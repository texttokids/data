quand Lola est contente , elle chante .
c' est sa façon d' être gaie .
aujourd'hui , Lola a envie de chanter : le premier jour dans sa nouvelle classe s' est bien passé .
dans les rangs , Lola rencontre Lulu .
" dis moi , Lola , comment ils t' appellent , tes parents ? " demande Lulu .
" ça dépend " répond joyeusement Lola : " mon bébé , mon petit coeur ou ma petite fée . "
à ces mots , Lulu et les autres pouffent de rire .
Lola n' a plus envie de chanter .
" pourquoi se moque t on de moi ? je croyais que tout le monde avait des petits noms ... il faut que je vérifie ! "
" excusez moi , Monsieur l' Agent , ils vous appelaient comment vos parents , quand vous étiez petit ? "
" mon gentil poussin , pourquoi ? " répond le policier .
" pour rien " dit , Lola .
Lola pose la même question au boulanger .
" ils m' appelaient ma petite boule de pain " , sourit il un peu gêné .
Lola croise une dame qui poussa un landau .
son bébé hurle et gesticule ...
" calme toi , mon ange " , lui dit doucement la maman .
" drôle d' ange ! " pense Lola .
Lola recommence à chanter .
elle se sent rassurée : tout le monde a un petit nom !
à l' arrêt du bus , Lola entend rire derrière elle .
" Hououou , la fée ! coucou , le bébé ! "
Lola ne veut plus voir Lulu et les autres ...
elle rentre chez elle à pied , sans siffler , sans chanter .
" bonjour , mon bébé " dit Papa en lui ouvrant le porte .
" je ne suis plus un bébé , je suis une géante ! " crie Lola .
" bonjour ma petite fée " dit , Maman .
" je ne suis pas une fée , je suis une sorcière ! "
inquiets , Papa et Maman s' approchent : " que se passe t il , mon petit coeur ? "
" le petit coeur , c' est moi ! " s' écrie Lola en sautant dans leurs bras .
elle ne résiste pas à ce petit nom là .
le lendemain matin , quand Lola retourne à l' école , elle ne pense plus à Lulu la moqueuse .
dans la cour , Lulu arrive droit sur elle .
" pardon pour hier . j' étais jalouse " , dit elle .
" chez moi , on ne dit pas des mots doux comme ça . "
" oh ! c' est triste ! " s' écrie Lola .
" rassure toi " sourit Lulu .
" j' en ai parlé à mes parents et ça s' est arrangé . "
" chouette ! et alors , comment t' appellent ils ? "
" ça dépend " , répond fièrement Lulu : " mon bébé , mon petit coeur ou ma petite fée . "
" ah non ! crie Lola , " pas ces noms là !
ils sont à moi , rien qu' à moi !

en classe , Lola boude très fort .
" le petit coeur , c' est moi , pas elle ! "
puis , elle réfléchit et arrête de bouder .
les petits mots doux sont à tout le monde , après tout !
" hé , Lulu " , dit Lola , " tu sais quoi ? j' ai envie de t' apprendre à chanter ! "
maintenant , quand Lola et Lulu sont contentes , elles chantent .
c' est leur façon à elles d' être gaies .