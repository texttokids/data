la commission présidentielle publie le neuf juin dix neuf cent quatre vingt sixun rapport volumineux de deux cent vingt cinq pages qui détaille les problèmes techniques et les erreurs managériales ayant conduit à la catastrophe .
le rapport se termine par un ensemble de neuf recommandations sévères douze :
les joints toriques , éléments techniques responsables de l' accident , doivent être intégralement refaits , aussi bien au niveau de la conception que de la fixation .
ils devront être mis au point sous le contrôle de la NCR avec des tests réalisés dans les conditions se rapprochant le plus des conditions réelles .
la NASA doit revoir son organigramme , en particulier la structure du management pour le programme des navettes en encourageant l' intégration d' astronautes au poste de direction et en créant une commission de sécurité en volume
les critères de fiabilité et les composants critiques doivent être réétudiés l' objectif est que cette étude permette l' identification des composants à améliorer afin d' assurer la sûreté des missions .
la NCR vérifiera le bien fondé de l' étude .
la NASA doit établir un Office de la Sécurité , de la Fiabilité et du Contrôle de qualité .
cet Office aura l' autorité en matière de sécurité sur l' agence spatiale et devra remonter à la direction tout problème relatif à la fiabilité des volumes
beaucoup d' énergie doit être dépensée à éliminer la tendance des managers à décider dans l' isolement .
cela passe par un changement des personnes , de l' organisation et par la formation du personnel et par l' établissement d' une règlementation complète de lancement .
les réunions de revues des aptitudes en vol et de l' équipe de management devront être enregistrées .
la marge de sécurité d' usage du train d' atterrissage des orbiteurs doit être améliorée , y compris en cas d' atterrissage d' urgence sur la piste d' Edwards en cas de conditions climatiques défavorables à Kennedy .
la NASA doit réaliser tous les efforts pour mettre en place une procédure d' atterrissage d' urgence ou un système de survie pour l' équipage lors de la première phase de décollage pour qu' il devienne possible de tout arrêter .
la NASA doit mettre un terme à la politique du tout navette .
cela signifie qu' elle ne doit plus dépendre d' un seul type de lanceur et doit prévoir un calendrier de vol cohérent avec ses possibilités .
un plan d' inspection des opérations de maintenance doit être mis au point et mis en place et le programme des pièces de rechange doit être restauré .
ces recommandations sont lourdes de conséquence , en particulier la recommandation numéro huit .
la NASA avait toujours souhaité maintenir un contrôle sur les activités spatiales .
l' objectif initial de l' engin réutilisable de lancer un tir par semaine et de démocratiser l' espace se voyait définitivement condamné .
désormais , de nouveaux lanceurs jetables reviendraient sur le marché : le lanceur Titan quatre , dont la conception avait déjà commencé avant la catastrophe de Challenger pour servir de complément à la navette , récupèrera finalement la majorité des missions prévues , de même que les fusées Ariane dont le succès inattendu aura été favorisé par l' échec des objectifs initiaux de la navette et la catastrophe de Challenger .