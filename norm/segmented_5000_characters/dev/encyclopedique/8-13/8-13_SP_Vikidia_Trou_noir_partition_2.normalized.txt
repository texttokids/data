quand un objet va s' approcher d' un trou noir , il sera attiré de plus en plus à cause de sa forte gravité .
cependant il est possible pour l' objet de s' en échapper .
en effet , il existe une limite au delà de laquelle il est impossible de s' échapper du trou noir : on l' appelle l' horizon des évènements .
à partir de cette limite il faudrait , pour s' échapper de la gravité du trou noir , une vitesse supérieure à la vitesse de la lumière , qui est impossible à dépasser .
cependant , cet horizon dépend de la masse du trou noir .
sa masse a donc différentes conséquences sur l' objet qui s' en serait trop approché :
elle peut déformer les objets qui s' en approchent : sur Terre , la gravité diminue avec le distance , ce qui engendre une force de gravité différente entre le haut et le bas d' un objet ( le haut de l' objet étant un peu plus loin de la Terre que le bas de l' objet ) .
mais dans le cas d' un trou noir , la gravité est tellement grande que cette différence de force est elle aussi très importante !
au final , l' objet sera attiré beaucoup violemment sur le côté rapproché du trou noir que sur le côté lui tournant le dos , ce qui va l' étirer comme un spaghetti .
on appelle donc ce phénomène la " spaghettification " ( dite aussi " effet de nouilles " ) .
le fait qu' un objet soit plus attiré d' un côté que de l' autre par un astre est appelé " effet de marée " ( son nom provient du fait que c' est l' effet qui est à l' origine des marées de la mer sur Terre ) .
plus le trou noir est massif , plus l' horizon est grand et moins cet effet de marée sera puissant .
elle peut aussi permettre au tour noir d' émettre des particules .
pour cela , il faut qu' un couple de particules antiparticules se forme dans l' Univers , ( ex : proton antiproton ) .
ce couple doit également être à proximité de l' horizon des évènements d' un trou noir .
dans ce cas , il est possible qu' un élément du couple soit absorbé par le trou noir .
l' autre élément sera rejeté dans l' espace , après avoir " surfé " sur la limite de l' horizon .
pour un observateur , il aura l' impression que le trou noir émet une particule , on appelle cela le rayonnement de Hawking .
pour le trou noir , sa masse diminue car la particule qui tombe dedans se désintègre avec une autre particule du trou noir .
on appelle cela l' évaporation du trou noir .
peut on créer un trou noir ?
en théorie , des trous noirs peuvent être créés dans des accélérateurs de particules .
mais ces trous noirs là sont vraiment très petits et leur durée de vie extraordinairement courte , tellement courte qu' ils s' évaporent avant même d' avoir rencontré d' autre matière .
( cette évaporation s' appelle le rayonnement de Hawking .
) il est donc impossible en pratique que de tels trous noirs puissent gagner suffisamment de masse pour devenir un danger pour la Terre .
des trous noirs de ce type sont aussi créés dans la haute atmosphère de la Terre ou sur la Lune à cause des rayons cosmiques .
les seuls trous noirs dont l' existence ne fait quasiment aucun doute ont une masse au moins trois fois supérieure à celle du Soleil .
et ceux là , l' Homme ne peut pas les créer !
les trous noirs sont des objets assez mystérieux dus au fait qu' ils sont très compliqués à observer et qu' ils semblent défier les lois de la physique que nous avons l' habitude de voir .
de ce fait , ils font travailler l' imagination de chaque personne qui essaye de comprendre , mais cette imagination mène souvent à de mauvaises interprétations .
voici donc quelques idées reçues que nous allons clarifier :
aussi connu sous le nom d' " aspirateurs galactiques " , les trous noirs sont , dans l' imaginaire collectif , des objets qui aspirent tout autour d' eux comme un monstre qui mange tout ce qu' il voit .
en réalité , cette vision est décalée de la réalité scientifique .
un trou noir est un objet possédant une gravité comme toute autre étoile ou planète .
c' est cette gravité seule qui attire la matière , mais , de la même façon qu' un satellite orbite autour de la terre grâce à la gravité terrestre , un vaisseau ou une étoile peut très bien orbiter autour d' un trou noir .
vu que la gravité ne dépend que de la masse de l' astre , si on remplace notre Soleil par un trou noir qui a la même masse , toutes les planètes du système solaire vont continuer à tourner comme si de rien n' était ( mis à part qu' il fasse un peu plus sombre ) .
de même , si un vaisseau spatial s' approche d' un trou noir , il a largement le temps de le contourner et même de tourner autour sans prendre de risque .
les trous noirs portent assez mal leur nom .
d' une part parce que ce ne sont pas des trous , mais aussi parce que , d' après les dernières études ( concernant le rayonnement de Hawking cité plus haut ) , ils ne sont pas vraiment noirs .
il ne s' agit que d' une accumulation de matière compressée dans un espace minuscule ( plus petit qu' une tête d' épingle ) .
il faut plutôt les considérer comme un puits sans fin .