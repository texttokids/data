pour les Grecs de l' Antiquité , le char du Soleil était tiré par des chevaux pour son parcours quotidien dans le ciel .
phaéton , fils d' Hélios ( nom grec du Soleil ) , veut diriger le char pour égaler son père .
comme les chevaux ne reconnaissent pas leur conducteur habituel , ils quittent leur trajectoire , ce qui serait catastrophique pour les habitants de la Terre .
Zeus ( appelé aussi Jupiter ) met fin à cette folie en précipitant Phaéton dans un fleuve .
les centaures sont des chevaux avec un torse d' homme .
le plus célèbre est Chiron , auquel était attribuée l' invention de la musique et de la médecine , ainsi que l' éducation des héros , comme Achille et Ulysse .
à sa mort , Jupiter lui fera une place parmi les étoiles du ciel .
parmi les signes du zodiaque , c' est lui le Sagittaire .
mais les autres centaures ne sont pas aussi sages .
le peuple des Lapithes leur fait la guerre car ils avaient tenté de leur enlever leurs femmes .
hercule tue le centaure Nessus qui voulait lui enlever sa femme Déjanire .
pégase , cheval ailé , n' accepte sur son dos que les poètes .
la licorne est un cheval portant une corne sur le front .
une série de tapisseries du Moyen Âge en a fait un personnage de La Dame à la licorne .
le cheval de Troie est une ruse de guerre des Grecs .
gigantesque cheval de bois , il est abandonné par les guerriers grecs devant les portes de la ville de Troie qu' ils tentaient en vain d' assiéger .
les Troyens s' en emparent et l' introduisent dans leur ville , sans se rendre compte qu' il contient des soldats ennemis qui , la nuit , en ouvriront les portes à l' armée grecque qui pourra ainsi s' emparer de Troie .
dans une épopée du Moyen Âge , le cheval Bayard , aux bonds prodigieux , emporte ensemble sur son dos les quatre fils Aymon .
le cheval Spleinir ou cheval à huit membres qui selon la légende serait très timide et n' apparaîtrait aux gens au coeur pur mais partirait à tout jamais si on ne s' occupe pas de lui .
parmi les chevaux qui figurent dans les oeuvres de fiction , quelques uns sont particulièrement connus :
crin blanc , le cheval camarguais du roman et du film éponyme ,
flicka , la jument du roman et du film Mon amie Flicka ,
Lucky , cheval de Mélanie dans la collection le ranch de la pleine Lune
Sheltie , le Shetland , héros du livre dont le titre est son nom .
certains chevaux sont devenus célèbres , soit pour leurs performances sportives , soit pour leurs particularités ou encore parce que leur cavalier était un grand homme .
Bucéphale est un cheval indomptable que le fils du roi de Macédoine , Alexandre , alors âgé de quinze ans , parvient à faire obéir .
devant cet exploit , son père lui prédit alors un avenir de conquérant .
devenu adulte , Alexandre conquerra effectivement un immense empire ,
Incitatus , qui fut nommé consul par son maître , l' empereur romain Caligula ,
serko , cheval qui a effectué la plus longue randonnée de tous les temps , ayant traversé neuf kilomètre dans l' Empire russe en deux cents jours et qui a inspiré un roman et un film ,
Old Billy , le plus vieux cheval , né en mille sept cent soixante et mort le vingt sept novembre mille huit cent vingt deux , à l' âge de soixante deux ans ,
Hans le Malin qui a vécu au début du XXème siècle en Allemagne parvenait à répondre à des questions de calcul , de lecture ...
en interprétant la gestuelle de la personne qui lui posait des questions .
général du Lupin , qui a gagné cinquante et un épreuves pour un total de deux deux cent quarante cinq cent trente huit euros sur cent vingt cinq courses
général du Pommeau , qui a gagné les plus grandes courses de trot françaises et européennes .
Ourasi , célèbre pour ses fulgurantes remontées dans les courses hippiques de trot attelé .
Seabiscuit , cheval américain de course dont les victoires inattendues ont redonné l' espoir à des millions de personnes durant la crise économique de mille neuf cent vingt neuf .
Zakarva , jeune jument qui a étonné plus d' un parieur !
Jappeloup de Luze , médaillé d' or de saut d' obstacles aux Jeux olympiques d' été de mille neuf cent quatre vingt huit à Séoul avec pour cavalier le français Pierre Durand .
Milton , cheval de John Whitaker , considéré par beaucoup comme le meilleur cheval de saut d' obstacles de tous les temps .
Huaso , pur sang chilien , qui a battu le record du monde de saut d' obstacle avec un saut de deux mètre quarante sept , le cinq février mille neuf cent quarante neuf.
" Persik " , ( pur sang arabe ) qui a gagné énormément de course d' endurance .
( notamment les cent soixante kilomètres de Florac Lozère )
Hickstead , cheval d' Eric Lamaze , champion olympique à Pékin Hong Kong , mort en piste à Vérone en deux mille onze .
" Baloubet du Rouet " cheval de Rodrigo Pessoa , triple champion du monde en mille neuf cent quatre vingt dix huit mille neuf cent quatre vingt dix neuf et deux mille , deuxième en deux mille un , troisième en deux mille deux et deuxième en deux mille trois , médaille d' or aux jeux olympiques d' Athènes en deux mille quatre