l' élément technique ayant entrainé la catastrophe est un joint torique censé assurer l' étanchéité de la jonction inférieure du propulseur d' appoint à poudre droit qui n' était pas adapté au froid .
fragilisé , durci et dépourvu d' élasticité par des températures bien inférieures à la limite quatre , quatre degrees Celsius , il a brûlé et cédé à l' allumage .
au décollage , des petits résidus de la combustion en aluminium se sédimentent à la place du joint brûlé et comblent le trou ce qui évite la catastrophe sur le pas de tir .
près d' une minute après le décollage ( cinquante huit secondes ) , Challenger est heurtée par un puissant flux d' air latéral qui éjecte le bouchon de résidus .
la brèche est réouverte et le propergol jaillit à grande vitesse en tapant le support reliant le booster au réservoir .
soixante cinq secondes après le décollage , le bas du réservoir externe , attaqué par la fuite dans le booster , est percé et l' hydrogène liquide fuit à son tour , ce qui est visible par le changement de couleur de la flamme .
soixante dix secondes après le décollage , le support reliant le booster au réservoir externe est rompu .
toute la structure est déséquilibrée .
le booster droit est entrainé par la fuite de propergol et commence à tourner par rapport au support avant , toujours fixé au réservoir .
le dôme à l' arrière du réservoir est éjecté ce qui consume le carburant à grande vitesse .
soixante treize secondes après le décollage , l' aile de la navette est arrachée par la rotation du booster droit , le nez de celui ci vient percuter l' avant du réservoir externe et l' accélération ( d' une intensité de dix grammes ) provoquée par la chute du dôme arrière désintègre la navette et le réservoir externe ( cinq grammes de résistance maximale ) alors que les deux boosters y résistent et volent librement ( vingt grammes de résistance maximale ) jusqu'à leur autodestruction commandée à distance .
le compartiment de l' équipage tombe en chute libre dans l' Océan Atlantique , faisant subir aux astronautes une décélération fatale de deux cents grammes .
les astronautes ont pu avoir perdu conscience ou non pendant les deux minutes et demi de chute libre neuf .
si les joints toriques ont été identifiés comme les éléments techniques ayant conduit à la catastrophe , Richard Feynman , dont la démonstration en pleine audience télévisée a prouvé la responsabilité des joints toriques , est très critique à l' encontre de la culture d' entreprise de la NASA en matière de sécurité dix .
de nombreuses erreurs de prise de décision ont été relevées à l' encontre de la NASA par la commission présidentielle .
ces erreurs ont été l' objet de nombreuses analyses .
la NASA avait prédit que la navette spatiale réutilisable serait capable de réaliser soixante vols par année , soit plus d' un vol par semaine .
cependant , la navette était un engin expérimental qui a généré de nombreux problèmes techniques et qui n' a jamais pu dépasser le rythme de neuf vols par année .
cette situation a engendré une très forte pression interne .
la structure s' est efforcée d' accepter des risques pour respecter le calendrier de volume Aucun des risques acceptés n' a eu de conséquence grave lors des premiers vols , ce qui a conduit à son acceptation complète comme faisant partie du processus .
la sociologue Diane Vaughan a appelé ce phénomène la " normalisation de la déviance " .
en se livrant à ses pratiques , la NASA a installé en son sein une culture du risque , en repoussant toujours plus les limites onze .
au cours de l' enquête , Richard Feynman a demandé séparément aux ingénieurs et aux managers quelle est la probabilité qu' un accident survienne en vol avec la navette spatiale .
les managers ont estimé que la probabilité est d' un cent mille , soit un accident tous les cent volumes Chez les ingénieurs , cette évaluation donne un chiffre très supérieur de plus de un cent , donc plus d' un accident tous les cent vols dix .
le rapport d' enquête de la commission présidentielle est publié en juin dix neuf cent quatre vingt six, soit quatre mois après la catastrophe .
il accuse la NASA d' avoir omis de nombreux signaux d' alarme qui auraient dû éveiller ses soupçons depuis plusieurs années au sujet de la fiabilité des boosters .
avec ses méthodes directes , Richard Feynman menace directement de retirer son nom du rapport officiel si la commission n' accepte pas de publier ses propres constats , qui sont finalement publiés dans l' annexe F dix avec sa conclusion finale restée célèbre : " for a successful technology , reality must take precedence over public relations , for nature cannot be fooled . "