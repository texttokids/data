en mer , le seul prédateur du phoque moine était le grand requin blanc : ce requin ne se nourrit quasiment que de mammifères marins , principalement des phoques et des otaries .
dans les régions tropicales , où les otaries sont rares , le phoque moine des Caraïbes était sa principale source de nourriture .
comme tous les phoques , le phoque moine des Caraïbes était un animal très lent , et maladroit , une fois à terre : contrairement à leurs cousines les otaries , les phoques sont mieux adaptés à la vie aquatique : ils nagent mieux , mais se déplacent moins bien à terre .
heureusement , le phoque moine des Caraïbes n' avait aucun prédateur à terre : il pouvait nager très vite pour échapper aux requins , et se réfugier à terre pour se protéger .
c' est là , notamment , que les femelles donnaient naissance à leur bébé .
cependant , après la découverte de l' Amérique , au XVIème siècle , l' homme a commencé à explorer la mer des Caraïbes , ainsi que les différentes îles .
ces marins qui venaient par bateau de très loin avaient besoin de nombreuses ressources , notamment de la nourriture , et de l' huile pour leur lampes , afin de s' éclairer .
or , pour son malheur , le phoque moine des Caraïbes fournissait beaucoup de viande et d' huile , et , surtout , il était très facile à chasser , puisque , une fois à terre , il ne se déplaçait pas très vite .
les hommes ont donc entrepris de chasser le phoque moine en grande quantité , et , à mesure que l' exploration des Caraïbes et de l' Amérique progressait , le nombre de phoques a commencé à diminuer .
à la fin du XIXème siècle , c' était déjà devenu une espèce rare .
en plus de cela , la disparition de son habitat , à mesure que l' homme colonisait les plages où il avait l' habitude de se réfugier , a sans doute joué un grand rôle dans sa disparition .
au début du XXème siècle , seuls quelques groupes ou colonies de phoque moine des Caraïbes survivaient , éparpillés un peu partout dans les Caraïbes .
la dernière grande colonie vivait près du Yucatán , et a été quasiment exterminée en mille neuf cent onze , quand plus de deux phoques ont été tués par les pêcheurs .
dès lors , il est devenu très difficile de les observer , les rares survivants formant de tout petits groupes , souvent très éloignés les uns des autres .
le dernier phoque moine capturé a été tué en mille neuf cent vingt deux , en Floride .
le dernier groupe de phoques moines encore vivants a été vu pour la dernière fois en mille neuf cent cinquante deux , près de la Jamaïque .
depuis , plus personne n' a jamais revu un phoque moine des Caraïbes en vie .
des chercheurs américains ont effectué des recherches , pour tenter de retrouver les survivants des derniers phoques moines encore en vie , après plusieurs expéditions , en mille neuf cent soixante treize et mille neuf cent quatre vingt treize , sans avoir pu observer aucun phoque moine , ils en ont conclu que l' espèce avait disparu , et le phoque moine des Caraïbes a été déclaré officiellement éteint en mille neuf cent quatre vingt seize .
le phoque moine des Caraïbes n' est pas la seule espèce de phoque moine .
il en existe deux autres espèces , qui vivent également dans les régions chaudes : le phoque moine de Méditerranée , et le phoque moine de Hawaï .
ces trois espèces se ressemblent beaucoup .
le phoque moine de Hawaï et le phoque moine de Méditerranée sont tous les deux en danger critique d' extinction .
le phoque moine de Hawaï ( Monachus schauinslandi ) vit dans le Pacifique , notamment , comme son nom l' indique , dans l' archipel d' Hawaï .
selon les derniers recensements , il ne reste qu' environ un deux cents phoques individus vivants dans le monde .
le phoque moine de Méditerranée ( Monachus monachus ) vit en Méditerranée .
on pense qu' il n' en existe pas plus six cents à sept cents dans le monde , répartis en tous petits groupes très éparpillés .