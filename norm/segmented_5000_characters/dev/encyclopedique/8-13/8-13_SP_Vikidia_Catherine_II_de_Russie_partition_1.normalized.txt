pour consolider son trône , Catherine se fait couronner en septembre dix sept cent soixante deux à Moscou , dans la cathédrale de la Dormition , au coeur du Kremlin , le palais des tsars .
elle devient Catherine le deuxième de Russie .
un matin , dit on , elle se réveille et fait appeler ses médecins , leur ordonnant de la saigner afin qu' elle n' ait plus que du sang russe .
le ministre de Pierre le troisième , Nikita Panine , est placé à la tête de la Pologne .
mais , quand il s' avère que le contrat qu' il a fait signer précédemment , instaurant que la Russie s' entendait avec certains pays du nord de l' Europe , ne tiendra pas longtemps , il est remplacé par l' ambassadeur Stanislas Auguste Poniatowski , qui est envoyé gouverner le pays pour plus de sécurité .
Catherine a également fait de la Russie une puissance orientale , notamment à travers des guerres contre l' Empire ottoman , et contre les multiples puissances possédant des territoires sur la Mer Noire .
elle gagna aussi la Crimée , les forteresses d' Azov , de Taganrog , de Kinburn et d' Izmaïl , et engagea une guerre contre la Suède .
finalement , elle offre à la Russie cinq cent dix huit kilomètre second territoire supplémentaire .
encouragée par les philosophes , Catherine refuse de créer un parlement , mais se choisit des ministres qui la conseillent .
elle nomme des fonctionnaires pour administrer les territoires et tente de lutter contre la corruption .
ces réformes touchent toutefois peu les campagnes .
aussi Catherine doit elle faire face à une immense révolte menée par Pougatchev .
elle finit par mettre fin à cette révolte en emprisonnant et en faisant exécuter ce rebelle .
Catherine se refuse à condamner les autres rebelles , mais refuse d' abolir le servage .
Catherine déteste les révolutionnaires français , la démocratie , la république , car cela instaurait une grande anarchie dans des contrées peu surveillées de la Russie .
lorsqu' éclate la Révolution française , Catherine a soixante ans .
elle se sent seule et triste .
tout son soutien est mort et son fils Paul Ier de Russie la déteste , lui reprochant d' avoir fait tuer son père .
Catherine le lui rend bien , ne voyant en lui qu' un incapable et un dégénéré .
au matin du dix sept novembre dix sept cent quatre vingt seize , Catherine s' effondre .
la tsarine s' éteint après avoir régné sur la Russie pendant plus de trente ans .
pour sa succession , elle avait tout prévu .
dans son testament , elle déshérite Paul et transmet le pouvoir directement à son petit fils Alexandre .
mais Paul découvre le testament et le brule .
puis il fait ouvrir le tombeau de son père Pierre le troisième , le couronne et installe sa mère à côté .
Catherine repose ainsi à la cathédrale Pierre et Paul de Saint Pétersbourg , près de ce mari qu' elle a toujours détesté .
la tsarine n' est pas seulement célèbre pour ses victoires militaires , mais aussi pour sa culture .
seule dans son enfance , elle lisait tous les romans en français qu' elle trouvait .
finalement , elle affirme sa préférence pour les livres historiques , et plus tard , elle crée l' académie des Trois arts nobles .
elle se lie d' amitié avec Diderot , D' Alembert et Voltaire , leur demande des conseils sur la société et les invite en Russie .
le sujet qu' elle aborda le plus , durant ses longs entretiens avec les philosophes français , fut celui de l' éducation , problème majeur de la Russie , considérablement en retard sur ce point .
c' est donc poussée par les idées des Lumières qu' elle réforme le pays .
elle leur confirme également sa sympathie et son amour pour la culture en achetant les bibliothèques des Philosophes .
Diderot tente de convaincre Catherine de créer un parlement , mais la tsarine refuse nettement .
il ne faut pas se méprendre : si Catherine admire les Philosophes , elle ne prend pas parti pour une république .
elle pratique la censure et se montre épouvantée par la Révolution française .
cependant , à l' époque , la quasi totalité de la population de l' Empire russe demeure illettrée , et cela ne dépendait pas d' une seule personne , même d' une reine .
c' est malgré tout sous elle que l' éducation des jeunes russes ( des classes aisées peu nombreuses ) gagne le plus .
le personnage de Catherine le deuxième apparait dans quelques films , dont :
l' Impératrice rouge de Josef von Sternberg , film américain un , biographie romancée de l' impératrice .
le rôle de Catherine le deuxième est joué par Marlène Dietrich .
dans le film Casanova réalisé en dix neuf cent vingt sept par Alexandre Volkoff .
Catherine le deuxième de Russie est interprétée par Suzanne Bianchetti .