on trouve en Europe de nombreux lézards , comme le lézard vert : il en existe en fait plusieurs espèces , qui vivent dans des régions différentes d' Europe , et qui se ressemblent beaucoup .
il est assez difficile de les reconnaitre .
le lézard des souches est un proche parent du lézard vert .
les mâles se ressemblent beaucoup , et sont également de couleur verte , alors que les femelles sont beaucoup plus brunes .
le Lézard ocellé est le plus grand lézard d' Europe , et peut mesurer jusqu'à soixante dix centimètres .
le lézard des murailles est nettement plus petit , et mesure environ vingt centimètres .
il a de nombreux cousins , qui lui ressemblent beaucoup , notamment dans les îles de Méditerranée , comme le lézard des ruines , qui vit en Sicile , le lézard d' Espagne , le lézard de Crète , ou le lézard de Tauride .
le lézard vivipare vit dans le nord de l' Europe et dans les montagnes du sud de l' Europe , comme les Pyrénées , par exemple .
il présente une particularité originale pour un lézard : selon son milieu de vie , les femelles pondent des oeufs ou mettent au monde des petits déjà vivants .
les orvets sont des reptiles sans pattes , mais ce ne sont pas des serpents .
selon les scientifiques , il pourraient s' agir de proches cousins des lézards , ou de proches cousins des serpents .
comme les lézards , les orvets peuvent se casser leur queue pour échapper à leurs prédateurs : c' est ce que l' on appelle l' autotomie .
l' orvet fragile est ainsi appelé pour cette raison .
on le surnomme aussi " serpent de verre " .
il est très commun .
l' orvet doré , son cousin , vit en Grèce .
ils peuvent mesurer tous les deux une quarantaine de environ .
l' orvet géant est un cousin éloigné , beaucoup plus grand ( comme l' indique son nom ) , qui peut atteindre un , trente Monsieur On le confond facilement avec un serpent .
on trouve dans le sud de l' Europe certains reptiles qui vivent plutôt dans les régions plus chaudes d' Afrique et d' Asie , comme les geckos ou les caméléons .
la tarente de Maurétanie est un petit gecko , le seul d' Europe , que l' on trouve notamment en Espagne , au Portugal , en France et en Italie .
le caméléon commun est le seul caméléon d' Europe .
il vit en Afrique du Nord , mais aussi en Espagne et en Italie .
un lézard vert de l' Est , en République Tchèque
le lézard ocellé est le plus grand lézard d' Europe
la tortue d' Hermann , ou tortue des Maures , est une tortue terrestre , présente dans le sud de l' Europe , notamment en France , et dans les Balkans .
la tortue grecque , sa cousine , lui ressemble beaucoup .
la tortue bordée a des écailles sur le bord de la carapace qui forment une sorte de jupe .
elle vit en Grèce et en Albanie .
mais on trouve aussi des tortues aquatiques : la cistude d' Europe , également appelée tortue des marais , est la plus connue .
elle est malheureusement en voie de disparition .
on la trouve dans presque toute les régions d' Europe .
sa cousine , la cistude de Sicile , vit uniquement dans cette île .
l' émyde lépreuse vit principalement en Espagne et au Portugal .
elle est très rare en France .
la tortue de Floride était autrefois une espèce appréciée comme animal de compagnie , avant qu' elle ne soit interdite dans de nombreux pays d' Europe .
originaire d' Amérique du Nord , les petites tortues de Floride étaient élevées dans des aquarium .
mais , quand elles devenaient trop grandes , les gens les relâchaient dans la nature .
malheureusement , les tortues de Floride se sont rapidement multipliées en Europe , et elles ont chassées les cistudes et les émydes , qui du coup sont en voie de disparition .
les grenouilles vertes sont répandues dans toute l' Europe .
il s' agit en fait d' un groupe d' espèces , et de leurs hybrides .
il est assez difficile de les distinguer les unes des autres .
la grenouille rieuse est la plus grande , mais il existe d' autres espèces , avec lesquelles elle peut s' hybrider , comme la petite grenouille verte , la grenouille de Berger en Italie , et la grenouille de Perez en Espagne .
de nombreuses espèces de grenouilles vertes vivent dans différentes régions de la Méditerranée .
la petite grenouille de Karpathos est minuscule , c' est aussi la plus rare , car elle ne vit que dans la petite île de Karpathos , en Grèce .
la grenouille de Chypre ne vit qu' à Chypre , et la grenouille de Crète ...
qu' en Crète .
la grenouille épirote vit en Grèce , et en Albanie .
les grenouilles brunes sont leurs cousines .
il en existe aussi plusieurs espèces : la grenouille rousse est la plus connue , mais il en existe beaucoup d' autres , comme la grenouille agile .
la plupart apprécient les milieux forestiers , et les mares cachées dans les bois .
certaines espèces de grenouilles brunes ne se rencontrent que dans certaines zones de l' Europe , comme la grenouille ibérique , la grenouille italienne , la grenouille grecque , ou la grenouille des Pyrénées .