l' ancêtre du cheval est l' Eohippus , animal qui vivait dans la forêt , il y a environ soixante millions d' années en Amérique du Nord .
sa taille était de trente à cinquante centimètres .
l' Eohippus va progressivement évoluer et grandir , pour donner naissance à deux autres espèces qui sont sont le Mesohippus il y a quarante à cinquante millions d' années , et le Mérychippus il y a vingt à vingt cinq millions d' années .
ce dernier continue son évolution vers un animal mieux adapté à la vie dans une plaine .
les dents se modifient afin de brouter l' herbe , la vision s' élargit pour détecter les prédateurs dans toutes les directions .
les membres évoluent vers un sabot unique ce qui permet de courir sur un terrain difficile et complexe avec beaucoup de cailloux .
l' ancêtre des chevaux et poneys actuels est l' Equus caballus , un ancêtre des chevaux qui est apparu il y a moins d' un million d' années sur le continent Europe Asie .
l' Equus caballus va ensuite migrer vers les Amériques via vers l' Afrique .
son évolution a donné naissance au zèbre et à l' âne .
l' espèce a disparu du continent américain il y a environ huit ans .
trois sous espèces d' Equus caballus sont apparus à la suite de l' évolution de l' Equus caballus :
le cheval des plaines d' Europe centrale , ancêtre des chevaux actuels ,
l' Equus caballus Prjewalski appelé aussi cheval de Przewalski , dernière espèce de cheval sauvage qui vit actuellement .
son territoire d' origine est la Mongolie ,
l' Equus caballus Gmelini , dont descend le Tarpan , aujourd'hui disparu .
le cheval porte des noms différents suivant son genre ( mâle ou femelle ) , son âge ou ses capacités à se reproduire .
adulte femelle utilisée pour la reproduction : poulinière
adulte mâle utilisé pour la reproduction : étalon
mâle sans organe reproducteur ( châtré ) : hongre
noms anciens : monture , palefroi pour un messager , destrier pour une monture de guerre , haquenée cheval de tempérament doux monté par les dames : roussin ou rocin cheval monté par les pages , le sommier ou bidet est le cheval de bât ( transport des marchandises ) .
chez les indiens d' Amérique , le cheval est parfois désigné sous le terme de " grand chien " .
les mauvais chevaux sont dénommés : haridelle , rosse , bidet , bourrin , canasson
parmi les nombreuses manières de classer les chevaux , les principales concernent leur séparation par rapport à leur taille , leur utilisation et en fonction de la race .
s' ils sont de " sang chaud " ( chevaux de selle ) ou des " sang froid " ( chevaux de traits ) ou des poneys ce sont les trois catégories d' équidés
la grande diversité des races équines tient son origine de la sélection et des croisements opérés par l' homme sur le cheval domestiqué , mais également par la forte capacité d' adaptation de cet animal face à son environnement .
shetland , petit poney d' origine britannique et qui est possédé par presque tous les poney clubs de France ,
pur sang anglais , cheval de course ( le plus rapide )
Fallabella , plus petit cheval au monde ( moins d' un mètre au garrot )
pur race espagnol cheval utilisé pour la doma vaquera ou la corrida
le shire , le plus grand cheval de trait du monde ( deux mètres en moyenne )
le Comtois , cheval de trait originaire de Franche Comté , utilisé notamment pour en tant que cheval de trait , cheval de loisirs ( promenade , .
) mais aussi cheval de boucherie caractérisé par sa robe principalement Alezan crins lavé
le cheval a été domestiqué par l' homme il y a un peu plus de cinq cinq cents ans .
au départ , le cheval servait de nourriture mais l' homme a découvert qu' il était bien plus utile pour ses travaux .
le cheval a donc été utilisé dans l' histoire à de nombreuses tâches :
se déplacer : le cheval est capable d' emmener un homme sur son dos .
il s' agit de l' équitation .
le cheval est aussi capable de tirer des voitures et carrioles pour emmener plusieurs personnes en même temps ,
transporter des marchandises , du matériel ou du courrier ,
les deux principales utilisations du cheval sont soit d' être une monture pour un cavalier ou soit d' être un animal utilisé pour tirer des charges .
ainsi , est venue la distinction du cheval de selle et du cheval de trait .
le cheval de selle est fait pour avoir un cavalier sur son dos tandis que le cheval de trait est mieux adapté à tirer des lourdes charges .
une race de cheval est généralement spécialisée dans une de ces deux activités , mais la distinction n' est pas toujours nette .
certaines races peuvent être utilisées pour les deux travaux .
suivant le caractère et l' entraînement du cheval , ce dernier peut effectuer des travaux qui n' étaient pas prévus à l' origine pour sa race .
modèle : Clear
de nos jours , le cheval est principalement utilisé pour le loisir et le sport .
les sports équestres les plus pratiqués sont le saut d' obstacles et le dressage .
en sport hippique , cet animal permet la pratique des courses de galop et de trot .
de nouvelles utilisations du cheval ont vu le jour ces dix dernières années :
équithérapie : le cheval aide les personnes malades ou en difficulté pour guérir ,