les concombres de mer , ou holothuries , sont des cousins des oursins .
ils vivent près du fond , et se nourrissent de particules qui y tombent .
les bernard l' ermite vivent dans les coquilles vides des coquillages , comme les bigorneaux ou les bulots .
le bernard l' ermite commun vit le long des côtes européennes de l' Atlantique .
il est totalement absent de Méditerranée , mais cela n' empêche pas d' y trouver d' autres espèces .
le grand pagure vit en Méditerranée et en Atlantique .
comme son nom l' indique , c' est l' un des plus grands bernard l' ermite européens .
on le trouve souvent avec une anémone de mer , l' anémone parasite , sur son dos .
en réalité , cette anémone n' est pas un parasite , elle vit en symbiose avec le bernard l' ermite : elle le protège , et lui la nourrit en échange .
le tourteau est un gros crabe présent en Atlantique , il est recherché pour sa chair .
le crabe vert , ou crabe enragé , est le plus courant sur les côtes de l' Europe .
on le rencontre depuis la Norvège jusqu' en Mauritanie .
en Méditerranée , il est remplacé par une autre espèce , qui lui ressemble beaucoup , le crabe vert de Méditerranée .
on a longtemps cru qu' il faisait partie de la même espèce , jusqu'à ce que des analyses génétiques révèlent qu' il s' agit en fait d' une espèce à part .
pour l' araignée de mer , c' est le contraire : la grande araignée de mer ne vit qu' en Méditerranée , même si on a longtemps cru qu' elle vivait aussi en Atlantique : il s' agit en fait d' une espèce à part , qui lui ressemble beaucoup , l' araignée de mer Atlantique .
la petite araignée de mer vit principalement , elle aussi , en Méditerranée .
elle ressemble beaucoup aux deux autres , mais grandit beaucoup moins .
le homard européen , ou homard breton , vit dans l' Atlantique .
c' est un crustacé très apprécié pour sa chair .
contrairement à ce que l' on croit souvent , le homard n' est pas rouge , mais ...
bleu !
il ne devient rouge que dans la casserole , quand on le fait cuire .
un grand pagure , dans la coquille d' un coquillage , avec deux anémones parasites sur son dos .
une petite araignée de mer , en Adriatique , près des côtes italiennes .
la petite et la grande cigale de mer sont des cousines des langoustes .
très appréciées pour leur chair , elles sont devenues rares en Méditerranée , et sont à présent protégées .
la crevette grise est abondante sur toutes les côtes d' Europe .
on la trouve en Atlantique , mais aussi en mer du Nord , en mer Baltique , en Méditerranée , et en mer Noire .
la crevette nettoyeuse rouge , ou crevette barbier de Méditerranée , vit en Méditerranée , bien sûr , mais aussi dans le sud de l' Atlantique : on la rencontre jusqu' en Bretagne .
elle vit sur les rochers , où elle attend les poissons , qui la repèrent grâce à ses couleurs vives .
ils s' approchent d' elle , pour qu' elle puisse les débarrasser de leurs parasite , qu' elle capture avec ses pinces fines , et dont elle se nourrit .
les crevettes roses , ou " bouquet " , comprennent en fait plusieurs espèces , comme la grande crevette rose , la petite crevette rose , et le bouquet des posidonies , par exemple .
cette dernière vit , comme son nom l' indique , dans les herbiers de posidonies .
la minuscule crevette améthyste vit en Méditerranée , dans les tentacules des anémones de mer .
l' anémone de mer verte est une grosse anémone de mer , qui vit en Méditerranée .
dans certaines régions , on la mange , sous forme de beignets .
l' actinie rouge vit en Atlantique .
à marée basse , elle referme ses tentacules , et ressemble à une grosse boule , ce qui lui a valu son nom de " tomate de mer " .
on trouve aussi des actinies de couleur rouge en Méditerranée qui leur ressemble beaucoup , mais on ne sait pas exactement s' il s' agit de la même espèce ou bien d' une espèce à part .
l' anémone encroûtante jaune n' est pas une vraie anémone , mais un zoanthide , un cousin des anémones de mer .
elle vit en Atlantique et en Méditerranée .
d' autres cousins des anémones de mer incluent les coraux , et les gorgones , mais aussi les méduses .
le corail rouge vit en Méditerranée .
il a fait l' objet d' une pêche très importante , car son squelette rouge est utilisé pour faire des bijoux .
il est aujourd'hui protégé .
contrairement à ce que son nom indique , ce n' est pas un corail , mais une gorgone , un cousin des coraux .
on trouve d' autres gorgones en Europe , comme la gorgone jaune , qui vit , elle aussi , en Méditerranée .
une colonie de corail rouge , au plafond d' une grotte sous marine , en Italie .