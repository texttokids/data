la tour de sauvetage utilisée sur Saturn le cinquième faisait dix mètres de long et un diamètre de zéro , soixante dix mètre , elle pesait plus de quatre tonnes , la tour de sauvetage avait une poussée d' environ soixante dix tonnes .
sa poussée était plus forte que celle de la fusée Mercury Redstone qui avait propulsé Alan Shepard lors de la première mission spatiale américaine .
le moteur seul mesure trois , trois mètres de haut pour trois mètres de diamètre .
le prolongateur de tuyère porte sa longueur à six mètres pour un diamètre de quatre Monsieur cette tuyère est constituée de cent soixante dix huit tubes d' acier de deux , vingt cinq centimètres de diamètre convenablement cintrés entre lesquels , dans la partie intérieure , cent soixante dix huit autres tubes sont insérés , tous soudés avec un alliage contenant cuivre , argent et or .
dans ces tubes , le combustible circule avant son introduction dans la chambre .
les moteurs sont fabriqués par Rocketdyne à Canoga Park en Californie .
le moteur J deux équipe les deuxième et troisième étages du Saturn V C' est lui qui est chargé de la satellisation autour de la terre et d' envoyer le module lunaire vers la Lune .
successeur du RL dix qui équipe les Saturn un , il développe quatre vingt dix tonnes de poussée dans le vide contre quarante dans l' air .
il fait trois , quatre mètres de haut pour deux , un mètre de diamètre .
le premier étage fonctionne pendant deux minutes et trente secondes en brulant deux tonnes de carburant .
lorsque le deuxième étage prend le relais , la fusée se trouve à une altitude d' environs soixante kilomètres et sa vitesse est de huit six cents kilomètres par heure .
la séquence d' allumage du premier étage débute huit secondes avant le lancement .
le moteur F un central s' allume en premier , suivi par les quatre moteurs extérieurs avec un décalage d' environ un seconde .
une fois que les ordinateurs ont confirmé que les moteurs ont atteint leur puissance maximale , la fusée est relâchée en deux étapes : les bras qui maintiennent la fusée se déverrouillent pour la libérer puis , tandis que le lanceur commence à s' élever au dessus du sol , des fixations métalliques accrochées à travers des fentes à la fusée se déforment progressivement jusqu'à relâcher complètement le lanceur .
cette phase du lancement dure une demi seconde .
lorsque la fusée est complètement relâchée , le lancement ne peut plus être interrompu même si un moteur a un fonctionnement défectueux .
il faut environ dix secondes à la fusée pour s' élever au dessus de la tour de lancement .
Saturn le cinquième accélère rapidement , atteignant la vitesse de cinq cents mètres par seconde à deux kilomètres d' altitude .
or la poussée du moteur F un n' était pas variable .
six secondes après avoir éteint le moteur central , alors que la fusée a atteint une altitude de soixante kilomètres le premier étage se sépare du lanceur avec l' aide de huit petits rétrofusées à poudre qui empêchent le premier étage toujours propulsé de rentrer en collision avec le deuxième étage qui n' avance plus que sur son inertie .
le deuxième étage prend le relais du premier , il va fonctionner pendant six minutes .
il va permettre d' atteindre une altitude de cent quatre vingt cinq kilomètres , une vitesse de vingt quatre six cents kilomètres par heure ( six , cinq kilomètres par seconde ) .
les moteurs du second étage sont allumés en deux temps .
après la séparation , des fusées à propergol solide impriment durant quatre secondes .
puis les cinq moteurs J deux sont allumés .
environ trente secondes après la séparation avec le premier étage , la jupe située entre les deux étages , qui sert également de support aux fusées de tassement , est larguée pour alléger le lanceur .
cette manoeuvre de séparation demande une grande précision , car il ne faut pas que cette pièce qui entoure les moteurs et qui en est distante de seulement un mètre percute les moteurs .
au même moment , la tour de sauvetage , qui est fixée au sommet du vaisseau Apollo pour arracher celui ci au lanceur en cas de défaillance , est larguée .
dès que des capteurs au fond des réservoirs captent qu' il n' y a plus d' essence , les ordinateurs séparent le deuxième étage .
puis quatre rétros fusées éloignent le troisième étage du deuxième .