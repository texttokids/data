le Kurdistan ( en kurmandji : Kurdistan ou Kurdewarî , en sorani : ) est une région du Moyen Orient , en Asie .
cette région se situe à cheval sur quatre pays : la Turquie , la Syrie , l' Irak et l' Iran .
elle est historiquement et culturellement la région des Kurdes , peuple indo européen qui n' est ni turc , ni arabe .
beaucoup de Kurdes cherchent à obtenir leur indépendance vis à vis des États qui occupent leur région historique , et veulent que le Kurdistan soit reconnu comme un pays à part entière .
le mot Kurdistan signifie par ailleurs Pays des Kurdes .
à la fin de l' occupation ottomane dans les années mille neuf cent vingt , les accords Sykes Picot conclus par le Royaume Uni et la France démantèlent l' Empire , créent les frontières de la Syrie et de l' Irak , et proposent l' établissement d' un Kurdistan indépendant :
la guerre d' indépendance turque mettra cependant fin à ce projet .
les différents conflits de la fin du XXe siècle et du début du XXIe en zone kurde ( guerre Iran Irak , guerre d' Irak , guerre civile syrienne ) vont permettre aux Kurdes d' affirmer leurs ambitions d' indépendance , et de prouver leur habileté militaire face à leurs adversaires .
les soldats kurdes ont d' ailleurs combattu l' État islamique en Irak et en Syrie entre deux mille quatorze et deux mille dix huit .
les Kurdes en faveur du Kurdistan ont également connu un nombre important de répressions au cours de l' Histoire contemporaine , surtout représentées par l' opération Anfal en Irak sous Saddam Hussein en mille neuf cent quatre vingt huit .
le Kurdistan a pour capitale Diyarbakir ( en kurde : Amed ) en Turquie , et le kurde détient le statut de langue majeure au sein de la région .
la majorité des Kurdes sont musulmans sunnites .
l' Histoire du Kurdistan remonte aux débuts de l' Antiquité , dans les régions montagneuses de Zagros et de Taurus au sud du Caucase un .
durant cette période , les Kardukhiens ( ancêtres revendiqués des Kurdes ) habitent la région de Gordyène , au nord de la Mésopotamie deux , il s' agit de l' une des régions de l' Urartu , royaume arménien datant du IXe siècle avant Jésus Christ trois .
à la suite de la défaite de l' Empire néo assyrien qui dominait la région au VIIe siècle avant Jésus Christ , les Mèdes parviennent à conquérir une grande partie du Moyen Orient , y compris le territoire kurde quatre .
la région du Kurdistan sera successivement dominée par les Mèdes , l' Empire achéménide , l' Empire d' Alexandre , les Séleucides , l' Empire parthe , l' Empire romain cinq et les Sassanides six .
au Moyen Âge , la région est marquée par l' implantation de l' islam , lors des conquêtes musulmanes des Omeyyades du VIIe siècle , qui agrandissent le territoire conquis par le prophète Muhammad en Arabie , de la Libye à l' Afghanistan sept .
la dynastie des Omeyyades sera renversée par les Abbassides en sept cent cinquante , qui prendront à leur tour le contrôle du territoire kurde .
au cours du IXe et du Xe siècle , les Kurdes disposent d' une certaine autonomie au sein du califat .
ils sont répartis en quatre principautés : les Chaddadites , les Hassanwahides , les Banou Annaz et les Merwanides huit .
ce seront ensuite les Seldjoukides , puis la dynastie des Ayyoubides , dynastie kurde fondée par Saladin neuf au XIIe siècle qui domineront le Kurdistan .
les Kurdes sont également confrontés à l' Empire mongol , qui envahit une partie du territoire du Kurdistan avec la prise de Bagdad en cent vingt cinq mille huit cent dix .
plus tard , au XIVe siècle , les troupes de l' Empire timouride , dirigées par Tamerlan , prennent le contrôle du Kurdistan onze .
le Kurdistan , suite à l' effondrement des Timourides et à l' instauration des Séfévides au début du XVIe siècle , se trouve partagé entre deux puissances impériales : d' une part les Séfévides ( qui correspondent géographiquement à l' Iran actuel ) qui contrôlent l' est , et d' autre part l' Empire ottoman qui contrôle le reste du territoire kurde .
lorsqu' un conflit éclate entre les deux puissances en mille cinq cent quatorze avec la bataille de Tchaldiran , ville du nord de l' Empire séfévide , les Kurdes ont plus tendance à soutenir les Ottomans , qui leur garantissent une certaine autonomie sur leur territoire douze , sans pour autant leur garantir l' indépendance totale .
le traité de Qasr e Chirin , signé en mille six cent trente neuf , met fin aux guerres entre Ottomans et Séfévides en établissant une frontière claire entre les deux Empires , et scinde de manière officielle la région du Kurdistan treize .