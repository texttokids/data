le nom d' origine de la ville avant le cinq octobre mille neuf cent soixante dix, date de l' actuel sultan , était Bandar Brunei .
le premier terme , Bandar dans la région via les langues indiennes , pour désigner une sanskrit : le premier est l' équivalent de terme Sri , titre de vénération donné aux dieux hindous , qui avait à la base une signification d' émanence , de radiance , de diffusion lumineuse , le second procède du terme bhagav N ( ) signifiant " dieu " .
Sri Bhagwan peut donc être traduit par " le béni " , ainsi " Bandar Seri
bâtie sur l' eau .
aujourd'hui les quartiers modernes sont construits sur la terre ferme et la ville est devenue une mosaïque bigarrée où les maisons sur pilotis côtoient autoroutes et buildings en béton .
au delà du fleuve Brunei , vers le sud , s' étend la cité lacustre de Kampong Ayer , qui empiète d' environ cinq cents mètres sur la mer .
Bandar est habité depuis le VII E siècle , sur l' île de Kampong Ayer , coeur historique actuel de la capitale .
au XVI E siècle , alors que le Brunei occupe une place importante sur l' île de Bornéo , la
en mille neuf cent six , la ville s' établit autour de Kampong Ayer , sur les conseils du premier résident anglais au Brunei , McArthur .
la ville devient officiellement la capitale du pays en mille neuf cent vingt et le gouvernement de
Brunei passe sous possession japonaise pendant la seconde guerre mondiale , une grande partie de la ville a été détruite par les bombardements alliés .
la prospérité de l' État permet une reconstruction
en mille neuf cent cinquante six , la superficie officielle est de douze , quatre vingt sept km deux , ce qui correspond
la ville , qui s' appelait Bandar Brunei ou Brunei Town , est rebaptisée en mille neuf cent soixante dix Bandar Seri Begawan en l' honneur du père du sultan , Sir
en deux mille sept , le sultan Hassanal Bolkiah réorganise les limites de la ville faisant ainsi passer sa superficie à cent , trente six km deux .
la ville comporte des industries textile , de fabrication de meubles et d' exploitation du bois .
le principal marché de la ville est celui de Tamu Kianggeh situé sur le fleuve homonyme dans le centre ville .
on y trouve essentiellement des étals d' artisanat ainsi que des marchands de crevettes , de fleurs , de fruits et de légumes .
le complexe Yayasan est le plus grand centre commercial du pays .
il est situé juste à côté de
d' après le dernier recensement officiel d' août deux mille un , la ville était peuplée de vingt sept deux cent quatre vingt cinq habitants .
à la suite de l' extension des limites de la ville jusqu'à l' aéroport en deux mille sept , un certain nombre de villages ont l' ouest , Madewa au sud , Subok , à l' est ou encore Manggis au nord .
la ville de Bandar Seri Begawan compte désormais cent quarante habitants , tandis que son agglomération comprend environ deux cents habitants .
la ville de Bandar Seri Begawan s' étend sur douze mukims ( districts ) : Kianggeh , Gadong B , Kilanas , Berakas À , Berakas B , Kota Batu , Sungai Kedayan , Tamoi , Burong Pingai Ayer , Peramu , Saba et Sungai Kebun .
les mukims de Sungai Kedayan , Tamoi , Burong Pingai Ayer , Peramu , Saba et Sungai Kebun correspondent à la cité historique de Kampong Ayer ainsi qu' aux rives du fleuve Brunei et Kedayan .
Kampong Ayer est peuplé
un dôme géant doré .
grandiose , elle est bâtie avec les plus beaux et les plus couteux matériaux du monde : les marbres viennent d' Italie , les granits roses de Shanghai et les vitraux de Londres .
jusqu' en mille neuf cent quatre vingt quinze , elle était la seule mosquée au monde à présente des expositions sur l' architecture des villages sur
Bandar Seri Begawan possède un aéroport international ( code AITA : ) .
l' aéroport est desservi par l' autoroute Lebuhraya Sultan Hassanal Bolkiah , d' une longueur de onze kilomètres , qui relie le