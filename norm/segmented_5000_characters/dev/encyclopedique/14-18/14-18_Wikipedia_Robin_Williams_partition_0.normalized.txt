robin McLaurin Williams nait à Chicago , dans l' Illinois puis grandit à Bloomfield Hills dans le Michigan et dans le
son père , Robert Fitzgerald Williams , d' ascendance anglaise , galloise et irlandaise , est le responsable de la région du Midwest pour le constructeur automobile Ford .
sa mère , Laurie McLaurin , d' ascendance française , est native de La Nouvelle Orléans .
son demi frère , nommé Robert Todd Williams la suite d' une opération de chirurgie cardiaque effectuée un mois
après de courtes études de sciences politiques , il part à Juilliard School pour y étudier le théâtre , il y rencontre à
en mille neuf cent soixante quatorze , Robin Williams est remarqué par le réalisateur Garry Marshall lors d' une audition pour la série télévisée Happy Days .
celui ci lui confie le rôle de l' extra terrestre Mork , après que Williams a impressionné les producteurs par son féroce sens de l' humour quand il se met sur la tête pour s' asseoir sur un siège .
pour ce personnage , il improvise la plupart des dialogues et la mise en scène , en parlant avec une voix haut perchée et nasale .
son apparition dans la série est si appréciée , qu' on décide d' en faire une série dérivée .
il reprend à partir de mille neuf cent soixante dix huit dans la série télévisée Mork and Mindy , jusqu' en mille neuf cent quatre vingt deux .
l' écriture du scénario s' accommode de ses improvisations .
ce rôle lui vaudra en mille neuf cent soixante dix huit un Golden Globe du meilleur acteur dans une série télévisée musicale
toujours en mille neuf cent soixante dix huit , il sort un album de musique , Reality ...
What a Concept , puis en mille neuf cent quatre vingt huit et mille neuf cent quatre vingt treize , deux contes musicaux pour le label Rabbit Ears , Pecos Bill , et The Fool and the Flying Ship , sur des musiques de
en mille neuf cent quatre vingts , Robin Williams fait ses débuts au cinéma dans Popeye de Robert Altman suivi par Le Monde selon Garp un de George Roy Hill adapté du roman du même nom de John
robin Williams à la soixante deux E cérémonie des Oscars en mille neuf cent quatre vingt dix avec la
par la suite , son interprétation dans Good Morning , Vietnam , dans lequel il joue un animateur de radio arrivé de Crète à Saïgon , lui vaut sa première nomination à l' Oscar du meilleur acteur en mille neuf cent quatre vingt huit .
elle sera suivie de deux autres nominations en mille neuf cent quatre vingt dix pour Le Cercle des poètes disparus , dans lequel il interprète le professeur de lettres , John Keating , qui transmet à ses élèves le gout de la poésie et de la transgression , et en mille neuf cent quatre vingt douze pour Le Roi pêcheur , où il donne la réplique à Jeff Bridges dans cette fable autour de la
il remporte la statuette lors de la cérémonie de mille neuf cent quatre vingt dix huit , pour le Meilleur acteur dans un second rôle dans Will Hunting .
dans ce film , il interprète Sean Maguire , un psychiatre qui doit soigner les troubles du comportement de Will , un génie des mathématiques et un délinquant chronique .
ce film révèlera l' acteur Matt Damon dans le
en mille neuf cent quatre vingt huit , il joue à Broadway en compagnie de Steve Martin dans En attendant Godot de Samuel Beckett .
la même année , il participe au clip vidéo de la chanson Don' t Worry , Be
en mille neuf cent quatre vingt douze , les studios d' animation Disney s' inspirent de Robin Williams pour créer le personnage du " génie " dans le film Aladdin et l' acteur développe le personnage , lui donnant sa voix dans la version originale mais aussi en créant de nombreux gags animés par la suite .
toutefois , il demande à Disney de ne pas utiliser sa voix pour commercialiser le film , ce qui se produit pourtant .
une dispute débute alors entre le studio et l' acteur .
robin Williams ne donne pas sa voix pour les suites d' Aladdin , mais participe en mille neuf cent quatre vingt dix neuf au film L' Homme bicentenaire de Touchstone Pictures .
la situation s' arrange par la suite et , en deux mille neuf , l' acteur reçoit la distinction Disney Legends .
fin mille neuf cent quatre vingt treize , selon une source anonyme au sein du studio , le succès au cinéma de l' acteur est dû à Disney , à la suite des films produits par Touchstone Pictures , Good Morning , Vietnam et Le Cercle
en mille neuf cent quatre vingt quatorze , l' acteur est récompensé par un Golden Globe du meilleur acteur dans un film musical ou une comédie , un American Comedy Award et un MTV Movie Award pour son rôle dans Madame Doubtfire .
dans ce film , il interprète un père de famille en instance de divorce qui , pour être au contact de ses trois enfants , se grime et se travestit en gouvernante pour se faire engager par son ex femme .
entretemps , il enchaine des films grand public comme Hook de Steven Spielberg , dans lequel il incarne le célèbre Peter
l' important succès commercial de Madame Doubtfire en mille neuf cent quatre vingt treize fait de lui un des acteurs les plus populaires d' Hollywood .
il accepte ensuite de tenir le rôle du politicien homosexuel Harvey Milk dans la biographie que prépare le réalisateur Gus Van Sant , mais des désaccords avec la production font que Van Sant abandonne le projet pour plusieurs années .
c' est finalement Sean Penn qui interprétera
en mille neuf cent quatre vingt quinze , il apparait pour le petit rôle de John Jacob Jingleheimer Schmidt dans le film Extravagances .
il y aide Vida ( Patrick Swayze ) , Noxima ( Wesley Snipes ) et Gigi ( John Leguizamo ) à partir de New York pour aller à " Foliland " , alias