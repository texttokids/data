dans l' Europe du XVII E siècle , deux gentilshommes unis par une amitié indéfectible , Don Lope de Villalobos y Sangrin , loup espagnol , hidalgo téméraire et impulsif , et Armand Raynal de Maupertuis , renard français poète gascon , se lancent dans une aventure épique en quête du trésor des îles Tangerines .
durant leur périple , qui les mène aux confins du monde , et même au delà , ils
outre cette troupe hétéroclite , ils rencontrent également , sans pour
tout au long de l' histoire , se multiplient les références à la littérature classique , à des évènements historiques ou à la culture
les noms des deux personnages principaux sont des références au Roman de Renart .
don Lope , le loup , se nomme Villalobos y Sangrin .
on reconnait là le nom d' Ysengrin , le loup compagnon de Renart dans le roman .
quant à Armand , le renard , le nom qu' il porte , Maupertuis , est aussi celui de la forteresse du goupil , toujours dans le même roman .
on peut également noter que le nom Lope de Villalobos ( litt .
loup de la Ville aux loups ) est une référence à Ruy López de Villalobos , un explorateur espagnol notamment au service d' un vice roi mexicain nommé
enfin , de multiples références aux grands classiques de la littérature , en trop grand nombre pour être toutes répertoriées ici , émaillent tout le récit : Molière ( le début de la série est une mise en abyme de la pièce Les Fourberies de Scapin , on peut également retrouver une citation de L' Avare lorsque Séléné retrouve ses parents ) ,
l' une des grandes sources d' inspiration des répliques en vers d' Armand est Cyrano de Bergerac , qui compose une ballade en se battant .
cet amour de la versification combative se retrouve dans la pratique de la " rixme " , le duel physique et verbal en vigueur dans les bouges de la Lune .
plus généralement , la " rixme " peut être reliée à la tençon , joute verbale traditionnelle des troubadours occitans .
les albums se déroulant sur la Lune ( tomes six des États et Empires de la Lune de Cyrano de Bergerac , dont est notamment tirée l' idée que , sur la Lune , les poèmes font office de monnaie .
Cyrano lui même finit par apparaître dans le tome huit , sous les
un autre personnage secondaire des albums quatre à six , Sabado ( " samedi " en espagnol ) , est visiblement inspiré de Vendredi , intervertissant les stéréotypes de l' homme blanc et du sauvage issus de Robinson
certains passages font également allusion au cinéma contemporain .
par exemple , dans le tome un , lorsque don Lope et Armand se préparent à attaquer la chébèque , les gros plans des torses , la pose du maquillage noir sous les yeux et le " clic clac " des boucles de ceinturon et des armes rappellent la scène où John Rambo se prépare manifeste le désir d' envoyer son frère dans l' espace profond " pour qu' on ne l' y entendît point crier " , ce qui pourrait être une allusion course poursuite à malte dans le tome deux emprunte aussi certains codes
les différents véhicules utilisés par les protagonistes pour se rendre sur la Lune font références à des engins modernes : ainsi que l' équation de Schroedinger apparaissent au détour de l' une de ses démonstrations ) .
il utilise le moteur à réaction à plusieurs reprises , mais donne une explication de son fonctionnement complètement farfelue .
il construira également une fusée lunaire pour le Capitaine Mendoza et Cénile Spilorcio , Raïs Kader atteignent la Lune est en fait une montgolfière ( qui n' est inventée qu' à la fin du XVIII E siècle ) .
Bombastus ignorait d' ailleurs son principe , et est très surpris , à la fin du tome cinq , de la voir décoller avant que la pierre de lune soit jetée
d' Apollo onze .
on peut remarquer également que la forme du véhicule que les héros empruntent rappelle celle de la capsule lunaire , qui se sépare en deux avant l' alunissage .
courageux voire téméraire , emporté , irréfléchi , colérique et excellent bretteur , ce qui fait de lui quelqu' un à ne pas contrarier .
c' est le meilleur ami d' Armand , qu' il a rencontré à la guerre , et il a la particularité d' appeler ses amis par une forme hispanisante de leur prénom : " Armando " pour Armand , " Eusebio " pour Eusèbe , et caetera Le cri de guerre de sa famille est " carne y Sangre " ( " chair et sang " en espagnol ) .
il cache une phobie des rats , qu' il combattra à grand peine et parviendra à surmonter face à des chimères avec l' aide de ses amis .
il tombe amoureux dans l' acte un de Donya Hermine , une belle bohémienne , mais tente de cacher ses sentiments .
hermine lui fera la cour pendant un temps , mais , agacée par les refus de Lope , se disputera régulièrement avec lui .
très pieux , il met un point d' honneur à se confesser et méprise les musulmans , qu' il juge païens .
il deviendra néanmoins , après quelques disputes et la promesse d' un duel à mort , ami avec le Raïs Kader .
de son père il hérita d' une botte secrète , qu' il transmettra à Armand .
il a un passé tragique : fiancé a une belle louve , Donya Inès , elle fut emportée