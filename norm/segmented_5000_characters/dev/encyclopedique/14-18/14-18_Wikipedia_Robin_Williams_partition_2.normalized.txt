le prénom de sa fille , Zelda , est inspiré du jeu vidéo The Legend of Zelda , jeu auquel jouait beaucoup Zachary et dont Robin Williams appréciait le prénom du personnage de la Princesse Zelda .
robin Williams a participé à soixante seize films ( dont trois étaient encore en production après sa mort en vingt cent quatorze ) , la plupart du temps en tant qu' acteur principal .
il a été également l' acteur principal dans deux séries télévisées et a fait plusieurs apparitions en tant
année Titre français Titre original Réalisateur Rôle Observations
mille neuf cent soixante dix sept can I Do It ...' Til un Need Glasses ?
deux noeuds Robert un
mille neuf cent quatre vingt deux le Monde selon Garp The World According to Garp
mille neuf cent quatre vingt trois les Survivants The Survivors Michael Ritchie Donald
mille neuf cent quatre vingt huit portrait of a White Marriage Harry Shearer inconnu Les Aventures du baron de Muenchhausen The Adventures of Baron Münchausen Terry Gilliam le roi de la Lune crédité sous le nom de
mille neuf cent quatre vingt neuf le Cercle des poètes disparus Dead Poets Society Peter Weir le professeur John Keating ( Patrick Floersheim )
l' Éveil Awakenings Penny Marshall D R Malcolm Sayer mille neuf cent quatre vingt onze Dead Again Kenneth Branagh Doctor Cozy Carlisle The Fisher King : le Roi pêcheur The Fisher King Terry
Hook ou la Revanche du capitaine Crochet Hook Steven
professeur de mime crédité sous le pseudonyme de Marty Fromage
Toys Barry Levinson Leslie Zevo ( Patrick Floersheim ) Les Aventures de Zak et Crysta dans la forêt tropicale de FernGully FernGully : The Last Rainforest Bill Kroyer Batty ( voix
Aladdin Ron Clements et John Musker le génie et le colporteur ( voix originales ) ( Richard Darbois et Bernard
mille neuf cent quatre vingt treize les Mille et une vies d' Hector Being Human Bill
madame Doubtfire Messieurs Doubtfire Chris Columbus Daniel Hillard
neuf mois aussi Nine Months Chris Columbus D R Kosevich Extravagances To Wong Foo Thanks for Everything , Julie Newmar Beeban Kidron John Jacob Jingleheimer Schmidt ( non crédité )
jack Francis Ford Coppola Jack Charles Powell ( Dominique
l' Agent secret The Secret Agent Christopher Hampton
Aladdin et le Roi des voleurs Aladdin and the King of Thieves Tad Stones le génie ( voix originale ) ( Richard Darbois ) mille neuf cent quatre vingt dix sept La Fête des pères Fathers' Day Ivan Reitman Dale
Harry dans tous ses états Deconstructing Harry Woody Allen
Will Hunting Good Will Hunting Gus Van Sant Sean Maguire Flubber Les Mayfield le professeur Philip Brainard mille neuf cent quatre vingt dix huit Au delà de nos rêves What Dreams May Côme Vincent
mille neuf cent quatre vingt dix neuf Jakob le menteur Jakob the Liar Peter Kassovitz
l' Homme bicentenaire Bicentennial Man Chris Columbus Andrew
deux mille un AI Intelligence artificielle Artificial Intelligence :
deux mille deux photo Obsession One Hour Photo Mark Romanek Seymour
crève , Smoochy , crève !
Death to Smoochy Danny DeVito Rainbow
le Prince de Greenwich Village House of D David Duchovny
robots Chris Wedge et Carlos Saldanha Fender ( voix
deux mille six Happy Feet George Miller Ramón et Lovelace ( voix
camping car Runaway Vacation ou RV Barry Sonnenfeld Bob Munro Une voix dans la nuit The Night Listener Patrick Stettner
la Nuit au musée Night at the Museum Shawn Levy Theodore
deux mille sept permis de mariage License to Wed Ken Kwapis le
deux mille huit August Rush Kirsten Sheridan Maxwell " Wizard "
deux mille neuf la Nuit au musée deux Night at the Museum : Battle of the
World' s Greatest Dad Bob Goldthwait Lance Clayton ( Éric
deux mille onze Happy Feet deux Happy Feet Two George Miller Ramón et
deux mille treize un grand mariage The Big Wedding Justin Zackham le
deuxième chance à Brooklyn The Angriest Man in Brooklyn Phil
un foutu conte de Noël À Merry Friggin' Christmas Tristram Shapeero deux noeuds Virgil Mitchler sortie à titre posthume La Nuit au musée : le Secret des Pharaons Night at the Museum :
deux mille quinze Absolutely Anything Terry Jones Dennis ( voix )
année Titre français Titre original Rôle Oeuvres Observations mille neuf cent soixante dix huit mille neuf cent soixante dix neuf Happy Days Mork , l' alien série télévisée
mille neuf cent soixante dix huit mille neuf cent quatre vingt deux Mork and Mindy Mork série télévisée inconnu mille neuf cent quatre vingt quatorze Homicide Robert Ellison série télévisée ( saison deux , mille neuf cent quatre vingt dix sept Friends Tomas série télévisée ( saison trois , épisode vingt quatre ) deux mille huit New York , unité spéciale Law and Order : special Victims Unit Merritt Rook série télévisée ( saison neuf , épisode dix sept ) ( VF :
deux mille onze The Legend of Zelda : ocarina of Time ( Nintendo trois DS ) The Legend of Zelda : Skyward Sword ( Nintendo Wii ) lui même publicités avec sa fille Zelda pour la sortie des jeux vidéo ) deux mille douze Louie lui même série télévisée ( saison trois , épisode six )
Wilfred le thérapeute série télévisée ( saison deux , épisode un ) encore
deux mille treize deux mille quatorze The Crazy Ones Simon Roberts série télévisée
Blyth , une attraction des parcs Disney ) : voix de Timekeeper
information icon with gradient background .
Sauf indication contraire ou complémentaire , les informations mentionnées dans cette section peuvent être confirmées par la base de données
source et légende : version française deux noeuds sur RS
l' étoile de Robin Williams sur le Hollywood Walk of Fame .
empreintes de Robin Williams devant le Grauman' s Chinese Theatre
une série télévisée musicale ou comique pour Mork and Mindy .