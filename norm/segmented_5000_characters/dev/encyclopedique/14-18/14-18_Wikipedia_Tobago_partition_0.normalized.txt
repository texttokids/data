cette section est vide , insuffisamment détaillée ou incomplète .
l' Île , fertile , attira l' attention des anglais dès le XVI E siècle anglais , néerlandais , français et même par le duché de Courlande .
pour certains , Tobago demeure , dans la littérature , l' île de Robinson Crusoé , oeuvre écrite par Daniel Defoe et
les anglais et français furent les premiers à tenter la colonisation des Petites Antilles , dont deux tentatives éphémères à
anglais .
de mille six cent vingt huit à mille six cent soixante dix huit , les néerlandais considéraient Tobago comme leur colonie sous le nom de Nieuw Walcheren .
lors de l' arrivée des néerlandais , l' île avait été octroyée ( ainsi que l' ensemble des Antilles ) plusieurs fois par le roi Jacques I er d' Angleterre , dont une fois en mille six cent dix à Jakob Kettler , fils du duc de Courlande .
pour les néerlandais , Tobago demeura durant cette période , un site de
jan de Moor demanda à Jacob Maerssen pour qu' on lui octroie la permission de coloniser Tobago sous l' auspice de la Compagnie néerlandaise des Indes occidentales ( WIC ) fondée en seize cent vingt et un et détenant un monopole colonial et commercial sur toutes terres à l' ouest du Cap de Bonne Espérance .
une expédition de colons s' y établit seulement en seize cent vingt huit et fut ravitaillée en seize cent vingt neuf par l' amiral Pater .
en seize cent trente deux , un second contingent de colons néerlandais quitte le port de Flessingue et s' y établit la colonie comporte maintenant plus ou moins deux cents colons .
aux débuts des années seize cent trente , certaines escadres de la compagnie eurent comme instructions de s' arrêter à Tobago pour ravitailler les colons .
entre seize cent trente quatre trente sept , la colonie fut éradiquée sans qu' on en sache exactement les causes .
il est probable qu' une expédition espagnole depuis l' île de la Trinité ait attaqué la colonie .
les néerlandais auraient été traités comme des criminels de guerre , contre toutes conventions établies entre les deux nations durant la trêve de seize cent neuf vingt et un .
les néerlandais répliquèrent à Trinité et mirent à sac San
au même moment , Willem Usselincx puis Charles I er d' Angleterre tentèrent d' intéresser Friederich Kettler , duc de Courlande Jakob en mission à Amsterdam , il déniche le capital nécessaire pour financer une première expédition de deux cent douze colons vers l' île .
les colons , pour la plupart zélandais , ne s' acclimatèrent pas et désertèrent rapidement .
une seconde entreprise courlandaise en seize cent trente sept connut probablement le même sort .
les années seize cent trente , semble t il , sont assez difficiles à décortiquer étant donné les nombreuses
en seize cent trente neuf , des anglais de la Barbade tentent de nouveau d' occuper l' île sous la direction du capitaine Massam mais les attaques continuelles des Amérindiens Caraïbes de l' île voisine de Saint Vincent découragent toute occupation permanente .
une troisième tentative en seize cent quarante deux dirigée par le capitaine Marshall de la Barbade ne porte fruits que quelques années avant d' être poussé à émigrer vers le Suriname par les affrontements autochtones incessants .
à ce moment , Jacob Kettler , qui avait succédé à son père à la tête du duché de Courlande , décida de retenter sa chance et de financer une nouvelle tentative de colonisation à Tobago .
éconduits par les Caraïbes , les colons courlandais trouvèrent refuge aux établissements zélandais du
après avoir tenté d' intéresser les néerlandais dans une entreprise coloniale conjointe et essuyé des refus , Jakob décide de s' y reprendre seul en seize cent cinquante trois alors que les néerlandais et les anglais s' affrontent pendant la Première guerre anglo néerlandaise .
le vingt mai seize cent cinquante quatre .
une expédition de quatre vingts familles dirigée par un capitaine hollandais engagé par le duc jette l' ancre à Tobago et renomme l' île Nouvelle Courlande .
rapidement , les Courlandais construisirent le fort Jakobus .
en seize cent cinquante quatre , des Juifs fuyant la Nouvelle Hollande repassée aux mains portugaises tentent de convaincre les États généraux de leur octroyer Tobago , mais les héritiers De Moor , les frères Lampsins , d' éminents Zélandais , cherchaient au même moment à remettre sur pied la
les Zélandais reprenant contrôle de la colonie , la première expédition zélandaise , avec un bateau de cinquante Juifs de Zélande , arrive à Tobago en septembre seize cent cinquante quatre , quatre mois après les Courlandais et fonde une colonie du côté opposé de l' île , nommée Nieuw Walcheren .
Becquard , chef de l' expédition néerlandaise , visite Saint Eustache afin d' y recruter des colons potentiels .
lorsque Becquard revint à Tobago , la présence courlandaise était maintenant connue et un traité de partition fut ratifié .
la dotation aux Lampsins a été rédigée de manière que les États généraux néerlandais conservent une partie