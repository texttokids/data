conseils :
pourquoi parle t on de " saison des amours " ?
au printemps , le corps des chattes est programmé pour se préparer à faire des bébés .
les chatons naissent alors deux mois plus tard , quand il fait beau et chaud !
c' est mieux qu' en plein hiver dans le froid , non ?
les chattes qui vivent en appartement perçoivent moins les changements de saison et peuvent faire des bébés toute l' année !
les chats ne sont pas amoureux pour toute la vie , mais juste le temps de se reproduire .
les femelles amoureuses Pendant la saison des amours , ta minette n' a qu' une idée en tête : trouver un matou pour faire des bébés avec lui .
elle miaule nuit et jour , veut sortir , se frotte contre tes jambes ou contre les meubles , se roule par terre ...
on dit qu' elle est' elle ne s' est pas accouplée , ça recommence tous les mois pendant plusieurs jours !
ta chatte miaule toute la nuit ?
ton chat fait pipi partout ?
inutile de les gronder , c' est leur instinct de reproduction .
les mâles amoureux Avec tout ce cirque , les mâles tombent amoureux !
mais ils entre eux , car seul le plus fort gagne le droit de faire des bébés avec la femelle en chaleur du territoire !
morsures , griffures , on dirait des tigres !
certains passent aussi leur temps à faire pipi partout , pensant que l' odeur de leur urine va attirer les femelles ...