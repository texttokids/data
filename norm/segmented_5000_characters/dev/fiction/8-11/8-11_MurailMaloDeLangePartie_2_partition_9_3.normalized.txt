j' ai connu un petit gars , un petit comme Mouchique , que les coups de bâton I z' y ont ouvert le dos jusqu' aux poumons .
il en est mort ?
demandai je , tremblant d' effroi dans le noir .
ah bah , ça , un peu plus qu' un peu !
rigola mon voisin .
l' avis général était qu' au delà de vingt coups les chances de survie allaient s' amenuisant .
et si c' était Lapierre qui donnait la correction , Mouchique pouvait déjà être considéré comme un homme mort .
dormez , gueula Lapierre , ou faites semblant !
cette nuit là , je fis semblant .
la bastonnade devant être publique , nous fûmes rassemblés le lendemain à six heures du matin .
je voulus me mettre dans les derniers rangs afin de ne rien voir , mais Lapierre vint me chercher pour me placer devant :
un ami , il faut le soutenir dans l' épreuve , me dit il avec , sur les lèvres , le rire du bourreau .
il tenait à la main la corde goudronnée avec laquelle il frapperait Mouchique .
et maintenant , imaginez quatre mille bagnards semblablement vêtus et attachés deux à deux , attendant la punition d' un des leurs .
certains , avides de sensations , avaient les yeux grands ouverts , mais la majorité gardait la tête baissée .
le commissaire , qui avait le gout du spectacle , fit battre le tambour tandis que deux gardes faisaient avancer Mouchique , torse nu , vers le lieu de son supplice .
jamais il ne m' avait paru si jeune ni si fluet .
je fixai des yeux le petit dragon tatoué sur son épaule qui , dans un instant , au lieu de feu , cracherait du sang .
Mouchique fut étendu à plat ventre sur un banc , quatre forçats désignés d' avance vinrent le tenir par les bras et par les jambes .
Lapierre s' avança , la corde à la main .
un murmure courut parmi nous :
vingt coups , ils ont dit vingt coups .
au premier coup , je fermai les yeux .
je n' entendis que le cinglement de la corde .
Mouchique voulait sans doute passer pour un héros .
mais au cinquième coup , il n' y tint plus et se mit à crier .
puis il ne fut plus que hurlements , injures , blasphèmes ...
jusqu' au silence et à l' évanouissement .
ce fut le commissaire qui dut arrêter le bras du bourreau , car Lapierre aurait continué jusqu'à la mise à mort sous nos yeux .
on emporta le corps de Mouchique au dépôt de Pontà Lezen , où les soeurs soigneraient ses plaies avec du vinaigre et du gros sel .
et je n' entendis plus parler de lui .