les trois hommes n' avaient pas cessé de boire et de fumer .
Paluchard , qui était originaire de Normandie , sortit une bouteille de Calvados , qu' il avait tenue cachée jusque là , et emplit les verres à quatre reprises pour le gloria , la partante , la rincette , la surrincette ...
et le coup de pied au cul !
annonça t il en avalant un cinquième verre .
je guettais mon père du coin de l' oeil .
ce n' était pas une outre à vin comme Paluchard , mais ce soir là il buvait autant que lui .
quant à Orson , il était ivre mort et ses yeux étaient injectés de sang .
il prononça quelques mots dans une langue inconnue puis il glissa ses deux couteaux dans sa ceinture .
il était prêt .
tout le long du chemin que nous fîmes à pied , Orson et Paluchard fumèrent leur pipe .
pensaient ils aux meurtres qu' ils allaient commettre ou à l' or dont ils rempliraient leurs sacoches ?
abrutis par l' alcool comme ils l' étaient , ils avaient peut être cessé de penser .
mon père m' avait expliqué que les criminels et les soldats s' enivrent pour ne plus éprouver rien d' humain , ni peur ni pitié .
on exagère beaucoup la difficulté qu' il y a à tuer , comme disait le bourreau à la tête qui venait de tomber dans le panier .
nous y voilà , fit Paluchard devant le mur à escalader .
Orson tapa le fourneau de sa pipe contre la semelle de son soulier pour la vider , puis il la rangea dans sa poche , prit un couteau à sa ceinture et le plaça entre ses dents , tout cela bien tranquillement .
comme il tuerait dans un instant .
il grimpa le premier , tout de suite suivi par Paluchard .
tu restes là , me dit mon père .
quoi qu' il se passe , quoi que tu entendes , tu restes là et tu m' attends .
fais attention , papa , murmurai je .
alors , t' arrives ?
l' appela Paluchard au moment de sauter dans le jardin .
en deux mouvements , mon père se retrouva à califourchon au faîte du mur , et c' est alors que j' entendis des cris , des bruits sourds de combat , puis un coup de feu , deux , trois ...
et je vis Jean Saint Just dégringoler du mur dans le jardin .
papa !
la police avait tendu un piège aux grinches , mais mon père venait aussi d' y tomber .
abandonnant mon poste , je courus à l' angle des rues d' Enghien et d' Hauteville et allai coller mon visage contre la grille d' entrée .
je cherchai à deviner ce qui se passait dans le jardin de l' autre côté .
on entendait des jurons , des râles , mais on ne voyait rien .
si j' appelais , j' allais peut être me faire tirer comme un lapin .
mais mon père , mon père , où était il ?
était il mort , était il seulement blessé ?
avait il eu le temps de se faire reconnaitre ?
les minutes passèrent , la nuit redevint calme .
un calme atroce ...
pendant toute cette journée , j' avais été parfois nerveux , tendu , inquiet .
mais je me sentais protégé .
soudain , j' étais seul .
sans mon chef , sans mon père , sans cet homme qui m' avait tiré du bagne , sauvé de la guillotine , sans ce monsieur Personne auquel je pensais si peu quand je vagabondais dans les rues .
appuyant mon front à la grille , je me mis à sangloter .
mais qu' est ce que tu fais là ?
gronda une voix .
je hurlai tout à la fois de terreur et de joie .
papa !
mais papa ...
quoi , papa ?
je ne t' avais pas dit de m' attendre derrière le mur ?
mais tu es mort .
bougre d' âne , je me suis laissé tomber et je me suis un peu égratigné les mains .
avant d' être emmené par les gendarmes , Paluchard a eu la satisfaction de voir mon cadavre .
il a même prononcé mon oraison funèbre , il a dit : " c' était un fameux zig . " il fallait que Jean Saint Just fasse une belle fin , une fin dont les grinches se souviendront .
allez , ouste , marchons !
la nuit est fraiche , cela me fera du bien .
j' ai tellement bu aujourd'hui que je sue l' alcool par tous les pores de la peau .
j' aurais voulu que mon père m' ouvre ses bras , j' aurais voulu me serrer contre lui , les yeux fermés , et oublier toute cette damnée journée .
je me mis à marcher à ses côtés , mais au bout de quelques pas , mes jambes se dérobèrent sous moi et je dus me raccrocher à son bras .
julien , es tu certain que ce métier est fait pour toi ?
je ne répondis pas .