les jours suivants , je retournai à mes activités de lieutenant de la Sûreté .
mon père pensait que Zina et Orson n' avaient pas quitté Paris , j' allais donc les rechercher .
et puisque tout avait commencé par une conversation que Gaby avait surprise entre deux hommes au Lapin Volant , je rendis visite à sa patronne , la mère Gargoton .
que veux tu boire , mon ange ?
je suis en service .
le verre de l' amitié , ou je vais me fâcher !
nous trinquâmes donc au passé et à tous nos souvenirs communs , aux morts et aux vivants , à Craquelin et Bourguignon , à Janvier et La Bouillie .
que veux tu savoir ?
me demanda l' ogresse en s' essuyant les lèvres de la manche .
je lui fis la description d' Orson et Zina .
la mère Gargoton était très observatrice :
le gars à la peau de mouton , il est déjà venu une fois ici .
c' était un jour de grand froid .
je l' ai remarqué parce qu' il était bras nus .
c' était le jour où Gaby l' avait surpris , préparant son mauvais coup .
il n' était pas seul , dis je .
te souviens tu de l' homme avec lequel il était ?
la mère Gargoton chercha dans sa mémoire , puis secoua la tête .
un détail me revint alors en mémoire :
un homme qui bégayait .
Quiqui ?
c' était le nom que mon père avait également prononcé .
je ne l' ai pas vu depuis longtemps , me dit l' ogresse .
je crois qu' il est en prison .
l' autre piste qui menait à Orson et Zina , c' était Caramel .
mais mon père découvrit bientôt que la fille avait quitté la galerie Valois sans laisser d' adresse .
pour ne négliger aucune piste , mon père plaça Frédo en sentinelle à la fourrière pour le cas où Orson aurait l' imprudence de venir y chercher d' autres chiens .
ce qu' il ne fit pas .
et les jours passèrent , et les nuits .
mars , avril .
j' étais Tortillard ou Quinquet ou Rodolphe Lequeu .
j' étais surtout le lieutenant de Lange , sans avenir , sans horizon , sans amour .
je ne prenais plus de plaisir à vagabonder dans les rues , la fumée des tavernes me brulait les yeux , l' eau d' aff me tordait les boyaux .
Gaby était peut être en train de perdre sa jambe .
mais moi , je perdais mon temps .
le premier lundi du mois de mai , toujours en compagnie du Gladiateur mourant , j' attendis Nini Guibole , qui était mon seul lien avec Léonie .
midi sonna .
puis midi et demi .
enfin , je la vis qui arrivait tout essoufflée , le chapeau de travers .
m' sieur Malo , oh , je suis désolée .
on m' a renvoyée .
renvoyée ?
monsieur de Bonnechose a dit que je " dressais sa fille contre lui " .
mais le plus pire , c' est que Trimbaldière est revenu dans la maison et qu' il a l' air d' être le maître !
nom d' unch !
il faudra vraiment que je le tue !
ah , mon Dieu , mademoiselle Léonie a tellement peur d' un duel !
j' avais plutôt pensé à lui loger une balle dans la tête .
j' ai une vision simple des rapports humains .
tiens , Nini , prends ça , dis je en lui tendant un louis d' or .
et trouve toi une autre place .
qu' est ce que vous allez faire , m' sieur Malo ?
me conduire en homme .
j' allai tout droit à la galerie Véro Dodat demander conseil à papa .
il y a quelqu' un avec Personne , m' avertit le libraire .
mon père écoutait un restaurateur du quartier qui se plaignait de ce que ses couverts en argent disparaissaient mystérieusement .
cela fait déjà plusieurs fois que cela se produit .
c' est plus fort que les tours de magie de monsieur Wizzard !
que se passe t il exactement ?
le questionna monsieur Personne , le sourire aux lèvres .
eh bien , voilà .
des gens arrivent , par exemple un couple un peu âgé , bien mis , qui commande à diner .
ils paient leur addition , mais le garçon qui vient débarrasser la table s' aperçoit à ce moment là que les couteaux en argent ont disparu .
bien sûr , malgré leurs protestations , nous fouillons les clients .
et comme vous ne trouvez rien , vous leur présentez vos excuses , compléta monsieur Personne .
le restaurateur soupira .
cher monsieur , vous êtes victime de ce que nous appelons à la brigade le " grinchissage à la cire " .
après le départ de votre couple respectable , d' autres personnes tout aussi respectables entrent dans votre établissement et s' installent à l' endroit précis où le couple vient de manger .
oh , ne vous en faites pas , ceux là vous laisseront vos couverts sur la table .
ils se contenteront de récupérer ceux qui ont été collés sous la table avec un emplâtre de cire ...
oh , les bandits , oh , les brigands !
s' étouffa le restaurateur .
monsieur le chef de la Sûreté , il faut absolument arrêter cette bande de coquins !
je fis signe à mon père de se hâter de mettre le bonhomme à la porte .
il l' y reconduisit en promettant de lui envoyer un de nos agents .
que t' arrive t il ?
tu as l' air hors de toi ...
papa , il se passe quelque chose d' incompréhensible chez les Bonnechose .
ils reçoivent de nouveau Côme de la Trimbaldière .
comment ça ?
monsieur de Bonnechose s' est fâché avec lui ...
c' est bien pour cela que je vous dis que c' est incompréhensible !
on frappa de nouveau à la porte et mon père me fit signe qu' il allait se débarrasser du visiteur .
mais au lieu de cela , il se mit à bredouiller :