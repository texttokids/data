Mouchique était là , assis contre le mur , à demi caché par l' armoire .
il se releva et fit semblant de se couper la figure avec le pouce depuis le front jusqu' aux lèvres , en passant par le côté du nez .
après quoi , il cracha à terre .
c' est ce qu' on appelle " faire l' arçon " en termes de métier , et c' est le signe de reconnaissance entre voleurs .
nous nous serrâmes les deux mains .
comment m' as tu retrouvé ?
dans tous tes mensonges , il t' arrive de laisser échapper la vérité .
je savais que tu avais habité au six rue Bridaine , je m' y suis rendu .
il y a là bas une fille ...
Nini ?
c' est elle qui m' a donné ton autre adresse .
lui avait elle dit aussi que je travaillais dans la brigade de monsieur Personne ?
Mouchique n' y fit pas allusion .
tandis que nous parlions , je l' observais discrètement .
il était à bout de forces , efflanqué comme un chat sauvage , et vêtu de haillons .
nous allons fêter notre liberté , dis je .
je vais chercher à manger .
n' oublie pas de revenir , fit Mouchique sur un ton bizarre .
tu n' as pas confiance en moi ?
bien sûr que si .
mais au cas où tu penserais me vendre à la police , je te signale que j' ai enfermé la fille .
Nini ?
tu as enfermé Nini ?
m' affolai je .
et le bébé , qu' est ce que tu en as fait ?
garde ton calme , Quincampoix .
la mère et l' enfant se portent bien .
si tu veux que je les libère , rapporte moi à boire et à manger .
pour les vêtements , je m' arrangerai avec les tiens .
mon père avait raison : Mouchique était un monstre .
je courus jusqu'à la taverne du Lapin Volant , de l' autre côté de la cour , et j' en rapportai un plat de ragout fumant et un pichet de vin .
Mouchique se jeta sur la nourriture , se bourra de pain , se lécha les doigts , renversa le verre au dessus de sa tête pour en boire la dernière goutte , le tout sans dire un mot .
enfin , il poussa un soupir de bête repue et me nargua d' un sourire :
je n' ai pas enfermé la fille .
et je ne savais même pas qu' elle avait un bébé .
il me laissa digérer l' information , puis ajouta :
où as tu caché le Golconde ?
je n' ai rien volé .
je suis innocent .
il éclata de rire .
il m' exaspérait , j' avais envie de lui cracher à la figure que j' étais un agent de la Sûreté et que j' avais pour consigne de renvoyer les gens comme lui au bagne à perpétuité .
je te dis la vérité , Mouchique , c' est la maîtresse du duc qui a fait le coup .
elle l' a rejoint dans sa chambre en pleine nuit , et elle a attendu qu' il s' endorme pour enlever le Golconde de son écrin .
elle pensait m' accuser du vol le lendemain matin .
comment s' appelle cette femme ?
me demanda Mouchique , qui n' était qu' à demi convaincu .
Moïra de Feuillère .
elle se fait passer pour baronne .
où est elle ?
je fis un geste d' ignorance .
les héritiers avaient dû la chasser de la maison du duc .
il faut interroger les domestiques , mais nous ne pouvons y aller , ni toi ni moi , me dit Mouchique .
nous n' avons qu' à y envoyer Nini .
elle prétendra qu' elle est la soeur cadette de la baronne , qu' elle a un message pour elle ...
Mouchique s' était assis sur mon lit et parlait sur un ton d' autorité .
après tout , je pouvais bien m' associer avec lui pour un temps .
mon père ne faisait pas autrement quand il employait des voleurs dans sa brigade .
j' avais tout de même une objection :
Nini n' a que deux robes , et aucune ne lui donnera l' air de la soeur d' une baronne .
tu as déjà volé à la détourne ?
me demanda Mouchique en se rallongeant nonchalamment sur mon lit .
non , et je n' ai pas l' intention de m' y mettre .
voilà ce que tu vas faire , poursuivit Mouchique , exactement comme si je n' avais rien dit .
tu iras Au Vieil Elbeuf , le magasin de nouveautés qui se trouve rue de la Michodière .
tu demanderas à voir les pièces de tissu qui sont dans les casiers et tu les feras étaler sur le comptoir .
quand il y en aura beaucoup , Nini entrera .
elle aura plein de cartons à la main , comme si elle venait de faire des achats .
bien sûr , les cartons seront vides ...
Mouchique , avisant un carton à chapeau en haut de mon armoire , me pria de le descendre .
puis il me montra comment le ficeler sur le dessus tout en laissant un fond mobile au dessous .
il me parut nécessaire de repréciser à Mouchique que je ne volerais rien , ni Au Vieil Elbeuf ni ailleurs .
parce que je ne vole jamais .
mais , ajoutai je , il m' arrive d' emprunter certains objets et d' oublier de les rendre .
Quincampoix , tu es le plus grand embrouilleur que je connaisse .
et surtout , j' ai un gros défaut .
j' agis d' abord , je réfléchis après , comme disait le monsieur qui venait de se tirer une balle dans la tête .