vous êtes le nouveau ?
s' enquit monsieur Barbote , qui avait une voix haut perchée je vous croyais chez monsieur de Rechinchat .
dépêchez vous de vous installer à cette place .
nous étions dans Le Cid .
et tenez , puisque vous êtes debout , profitez en pour nous dire ce que vous savez de Corneille .
entre autres qualités , j' ai une mémoire remarquable :
corneille ?
il peint les hommes tels qu' ils devraient être et Racine les peint tels qu' ils sont .
monsieur Barbote marqua un temps de surprise .
hum ...
oui , en effet .
c' est une belle phrase de La Bruyère .
non , c' est de Louis Philippe .
les petits se mirent à glousser , et je vis Barbote devenir tout rouge .
j' appris à la récréation suivante que c' était un professeur qui se faisait chahuter .
ah , monsieur aime plaisanter , fit il , le ton aigre .
eh bien , vous me ferez cent lignes .
les poings commençaient à me démanger .
un bon direct dans le nez et je lui faisais gicler le raisiné , à ce bouffeur de Corneille .
il dut voir quelque chose dans mes yeux , car il se dépêcha de retourner à ses affaires .
pendant toute une heure , il nous lut des morceaux de son livre .
ce n' était guère mieux que du latin , Barbote appelait cela des alexandrins .
de temps en temps , il soupirait , en cherchant je ne sais quoi au plafond :
ah , messieurs , voyez comme c' est admirable !
mais à part les mouches , on ne voyait rien .
" cette obscure clarté qui tombe des étoiles " ...
n' est ce pas admirable ?
lange , au lieu de rêvasser , dites nous si vous avez reconnu cette figure de style .
je le regardai , les yeux ronds :
la figure de qui ?
ce fut une explosion de joie dans la salle .
la colère souleva Barbote de son siège .
en retenue , monsieur , en retenue , dimanche !
et je veux faire savoir votre comportement à votre père , à monsieur le proviseur , à ...
au roi , si vous voulez .
barbote se laissa retomber sur sa chaise .
un roulement de tambour montant de la cour nous annonça l' heure de la récréation .
tandis que les petits , me jetant au passage un regard craintif , s' élançaient vers l' escalier , je sentis une main qui se glissait dans la mienne :
vous êtes comme le Cid , me souffla Montpensier .
vous portez " sur le front une mâle assurance " !
peut être , dis je , mais tout cela est bien embêtant .
allons jouer !
Baudelaire m' a promis une place dans son équipe .
hélas , Tenant de Latour nous arrêta au pied de l' escalier .
toto devait prendre un cours particulier .
le pauvre petit !
m' exclamai je , indigné .
il est resté assis des heures à écouter des sornettes .
il va tomber malade .
oh , oui , pitié , pitié !
fit Toto en joignant les mains .
je sais par coeur mes déclinaisons .
il se mit à réciter à toute vitesse :
Civis , civis , civem , civis , civi , cive ...
c' est bon , c' est bon , grommela son précepteur , qui était brave garçon .
mais il est sous votre responsabilité pendant la récréation , monsieur de Lange .
Baudelaire avait tenu parole .
avec d' autres collégiens de son âge , il m' attendait devant les cabinets .
il jeta un regard méfiant à Montpensier :
le Petit Sa Majesté joue avec nous ?
il s' appelle Toto , répondis je , et il est avec moi .
comment joue t on à ton jeu ?
quoi ?
fit Baudelaire , avec un peu de pitié dans la voix comme monsieur de Rechinchat , tu n' as jamais joué aux barres ?
j' ai joué à d' autres jeux , dont tu n' as pas idée .
tu es un drôle de pistolet , s' amusa le jeune Baudelaire .
il en a même un dans son cartable !
s' écria Montpensier .
tais ta gueule , Toto .
pour mes lectrices , qui n' ont surement jamais joué aux barres à leur pensionnat , je vais rappeler quelques règles du jeu .
on sépare le terrain en deux par une ligne tracée à la craie .
il y a une dizaine de joueurs dans chaque camp et le but est de faire prisonniers les joueurs de l' équipe opposée .
alors , voilà , c' est moi qui donne la soupe , m' expliqua Baudelaire .
il tapa trois fois dans la main d' un collégien du camp opposé en prononçant une sorte de formule magique : " barre , baron , barrette . " à la troisième tape , il se sauva , poursuivi par son ennemi .
l' autre va essayer de le toucher pour en faire un prisonnier , m' expliqua Toto .
pris !
hurla le poursuivant .
Baudelaire fut emmené en prison dans le camp adverse .
toto ne tarda pas à le rejoindre et dut lui donner la main pour faire la chaîne .
délivre moi !
me lança t il .
il suffisait de toucher la main d' un prisonnier pour libérer toute la chaîne .
ce que je fis , parce que personne ne courait aussi vite que moi et que j' avais sacrément envie de délivrer tous ces forçats .
je me doutais que tu étais fort , me complimenta Baudelaire .
alors , comment trouves tu ce jeu ?
cela me va .
c' est moitié grinche , moitié cogne .
je vis au regard de Baudelaire qu' il ne parlait pas ce latin là .
allons , me dit il , c' est à toi de donner la soupe !
mais les choses se gâtèrent peu à peu .
Montpensier n' avait jamais été mêlé aux autres à l' heure de la récréation .