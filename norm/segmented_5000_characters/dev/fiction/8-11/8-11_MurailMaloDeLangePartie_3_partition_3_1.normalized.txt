disant ces mots , il écarta sa chemise puis , du même geste , il écarta les côtes de sa maigre cage thoracique .
" du froid qu' il fait , pensai je , lui qui a les poumons fragiles ! " c' est là que je m' aperçus que l' enfant avait une tête de caniche et que je décidai de me réveiller .
j' étais au lit , les pieds gelés .
ma chandelle brulait toujours .
minuit sonna à l' église Saint Gervais .
d' un coup d' oeil , je m' assurai que j' avais bien placé une chaise devant ma porte .
j' allais souffler ma chandelle quand , de nouveau , la peur m' étreignit .
la porte de mon armoire de déguisements était entrebâillée .
de deux choses l' une , ou je l' avais mal fermée tout à l' heure , ou l' enfant était venu reprendre son coeur sur l' étagère .
on verra demain matin , marmonnai je .
quand il fait jour , la probabilité que les fantômes existent diminue considérablement .
quelle horreur !
s' écria Gaby quand je fis miroiter le bocal dans un rayon de soleil .
comment peux tu garder ça ?
je ricanai .
ce coeur en conserve n' était pas la preuve qu' il existe un au delà , mais qu' il y avait eu un assassinat .
d' une manière ou d' une autre , le docteur Pelletan y était mêlé et je devais en avertir monsieur Personne .
mais il n' allait pas être content que j' aie agi sans ordre .
aussi , dès que j' entrai dans son bureau , c' est moi qui fis semblant d' être furieux :
vous auriez pu me dire que vous enquêtiez chez ma fiancée !
je voulais que mon père reconnaisse ses torts avant de lui avouer les miens .
mais je n' en eus pas le temps , car on frappa à la porte .
entrez !
la personne qui entra était la dernière que je souhaitais voir .
je ne sais pas si vous vous souvenez de moi , monsieur le chef de la Sûreté ?
oh , j' aurais dû vous faire confiance , l' autre jour ...
c' était le docteur Pelletan .
il m' adressa un signe de tête avant de s' effondrer sur la chaise .
on m' a volé hier .
la porte a été forcée et la fenêtre de la cuisine a été brisée .
la porte ET la fenêtre ?
s' étonna mon père .
et que vous a t on volé ?
le meurmouimette ...
pardon ?
cette fois , le docteur Pelletan se résigna à articuler :
le coeur de Louis le dix septième .
mon père hocha la tête pendant dix longues secondes avant de répéter sur un ton interrogatif :
le coeur de Louis le dix septième ?
dans un bocal .
c' est toute une histoire , monsieur .
je ne voudrais pas abuser de votre temps ...
du tout , du tout .
allez y !
fort heureusement , personne n' avait fait attention à moi pendant cet échange .
je devais avoir l' air le plus stupide qu' on ait jamais vu à un lieutenant de la Sûreté .
louis dix sept ?
le petit garçon qui m' avait réclamé son coeur , c' était ...
louis dix sept ?
louis dix sept , balbutiai je , c' est ...
c' est le fils de Louis le seizième ?
mon père me regarda , l' air encore plus stupéfait que moi .
il a été guillotiné , non ?
demandai je , en fronçant les sourcils dans un effort de mémoire .
mon père se prit la tête entre les mains en marmonnant : " mais quelle buse ! "
ah non , ça me revient !
il s' est échappé de la prison , caché dans un cheval de bois !
c' était une histoire que me racontaient mes tantes quand j' étais petit garçon .
hum ...
hum ...
je crains que ce ne soit une fable , me corrigea doucement le docteur Pelletan .
dans la réalité , le petit dauphin a été emmuré vivant au deuxième étage de la prison du Temple , il n' avait pas neuf ans , et il est resté ainsi dans les ténèbres , la crasse et la solitude pendant six mois , nourri de pain et de lentilles qu' on lui passait par un guichet .
quand enfin on a eu pitié de lui , il était trop tard .
il ne tenait presque plus debout , il ne parlait plus , il avait des tumeurs aux genoux et aux poignets ...
d' un moulinet de la main , mon père lui fit signe de passer les détails .
on a fait venir deux médecins pour le soigner , et l' un d' eux était mon père , comme moi même chirurgien en chef à l' Hôtel Dieu .
le cinq juin dix sept cent quatre vingt quinze , il a été conduit dans la tour du Temple et il y a trouvé celui qu' on appelait " le fils Capet " .
il me l' a décrit plus de cent fois , il en a été hanté jusqu'à la fin de sa vie .
le petit roi avait toujours son beau visage , ses cheveux blonds , ses yeux si bleus .
mais il était difforme , détruit par la maladie , et presque imbécile .
mon père n' a rien pu faire pour lui .
trois jours plus tard , le petit était mort .
le docteur Pelletan laissa passer quelques instants avant d' ajouter :
et mon père a procédé à son autopsie .
l' autopsie ?
releva monsieur Personne .
pour quoi faire ?
le comité de Sûreté générale ne voulait pas être accusé d' avoir fait empoisonner le petit prisonnier .
mon père a donc examiné les viscères et découpé la calotte du crâne .
l' opération a duré près de cinq heures et mon père a conclu à une mort par maladie scrofuleuse .
pendant qu' il recousait le corps , ses collègues médecins , le concierge et l' officier se sont éloignés près de la fenêtre pour causer .
la voix naturellement basse du docteur Pelletan devint plus basse encore , et je dus tendre l' oreille pour saisir cet aveu qui me glaça le sang :