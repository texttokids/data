ce seul mot me terrassa .
Bicêtre .
en un éclair , je revis le concierge , le cachot , la cour , les interrogatoires , la visite , le ferrement , la longue marche .
et le bagne de Brest .
non , jamais je ne supporterais de devoir tout revivre !
je suis innocent !
criai je .
je suis un agent de la Sûreté .
je n' ai pas volé le Golconde !
vous raconterez tout cela au juge d' instruction , ricana le commissaire .
vous êtes , de toute façon , accusé d' un crime plus grave .
je me suis évadé parce que j' étais injustement condamné , protestai je .
oh , je ne parlais pas de cela ...
que voulez vous dire ?
vous êtes accusé de meurtre sur la personne du duc d' Écourlieu .
mais c' est son valet de chambre qui l' a tué !
c' est lui , le voleur !
c' est lui , l' assassin !
allons , gardez votre salive pour le juge d' instruction ou je me verrai forcé de vous faire bâillonner .
mon père , mon père , pensai je , ne me laissez pas repartir pour Brest !
et soudain , je compris que ce n' était pas ce dont j' étais menacé .
si le crime d' Antonin retombait sur moi , j' aurais le même sort que le beau Guédon .
la guillotine !
papa , sauve moi !