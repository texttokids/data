de retour dans mon cachot , je fis le compte , qu' à raison de trois cent soixante cinq jours par an , multipliés par vingt , j' avais devant moi sept trois cents jours de captivité .
le temps finit par vous paraitre long , surtout les années bissextiles , comme disait Mathusalem en soufflant ses neuf cents bougies d' anniversaire .
ayant trouvé un clou dissimulé dans ma paillasse par un autre prisonnier , je m' en servis pour graver un bâton dans le mur chaque fois que le jour se levait .
au sixième bâton gravé dans le mur , j' eus une surprise en ouvrant ma boule de pain noir .
elle avait été creusée et il s' y trouvait un petit papier plié et un gros sou .
sur le papier , il était écrit : " tu vas partir pour le bagne de Brest . tiens bon jusque là bas . " c' était l' écriture de mon père , il ne m' avait pas abandonné !
j' avalai le papier par petits bouts tout en examinant le gros sou .
comme je m' y attendais , il s' ouvrait , il était creux et contenait un louis d' or .
je le glissai dans mon soulier .
le lendemain , au point du jour , j' entendis une rumeur étrange qui montait de la cour .
en me hissant sur la pointe des pieds , je pus voir par la fenêtre ce qui se préparait en bas .
la porte de la cour avait été ouverte à deux battants pour livrer passage à une charrette dont le chargement faisait un bruit étrange de cliquetis .
des hommes , dans des uniformes sales avec des restes d' épaulettes rouges , entrèrent à leur tour .
je compris qu' il s' agissait là des argousins , ces gardiens misérables qui escortent les forçats jusqu' au bagne .
ils commencèrent le déchargement de la charrette et je vis bientôt , alignées sur le pavé , six énormes chaines coupées de loin en loin par des chaines plus petites auxquelles pendait un triangle de fer .
je m' aperçus que je n' étais pas le seul à guetter ces préparatifs .
à toutes les fenêtres qui donnaient sur la cour , les prisonniers s' étaient agglutinés et ils se mirent à lancer des injures , des menaces , des plaisanteries , chacun selon son humeur .
les clameurs redoublèrent quand des messieurs en redingote et haut de forme entrèrent dans la cour .
journalistes ou simples curieux , ils avaient une permission spéciale du préfet de police pour assister au ferrement des forçats .
la porte de mon cachot s' ouvrit à ce moment là :
parait que c' est ton jour !
me fit gaiement le concierge .
et sais tu pas ?
c' est aussi le jour où ton ami Guédon s' est fait raccourcir !
il cligna de l' oeil dans ma direction pour que je savoure bien ma chance , celle de n' être condamné qu' à vingt ans de travaux forcés .
l' abbé Lornière est venu le voir hier , reprit le concierge , pour lui demander s' il se repentait de tous ses crimes ...
le beau Guédon y a sauté dessus pour l' étrangler .
le concierge s' étouffa de rire à ce souvenir , puis soupira , la mine attendrie :
tout de même , c' était un homme .
et qu' est ce qu' il aurait pas fait avec deux mains !
un coup de sifflet , venant du dehors , le rappela à ses devoirs :
dépêche toi , Quincampoix , c' est la visite .
quelle visite ?
ah , ah , " quelle visite " !
s' amusa le concierge , qui pensait que j' étais toujours dans mon rôle d' idiot .
une fois dans la cour , il me poussa vers les prisonniers qui étaient regroupés au centre .
malgré le froid terrible de janvier , ils étaient en train d' enlever leurs vêtements et je ne compris pas pourquoi .
mon père m' avait parfois raconté ses souvenirs de forçat , mais il ne m' avait jamais parlé de la visite .
qu' est ce qui se passe ?
demandai je à mon plus proche voisin .
l' homme me regarda pesamment sans me répondre .
c' était la plus parfaite face de brute que j' aie jamais vue .
à présent , tous les prisonniers étaient nus , ils jouaient à faire saillir leurs biceps ou disaient des choses sales en poussant de gros rires .
ils allaient ...
ou plutôt nous allions passer la visite médicale , car moi aussi , je devais me dévêtir .
ce que je fis , tout en essayant de cacher certaines parties de ma personne , tantôt avec une chaussure , tantôt avec mon bonnet .
je m' aperçus que les messieurs en redingote et haut de forme s' approchaient prudemment de nous , portant parfois un lorgnon à leurs yeux pour mieux voir les coeurs et les ancres tatoués sur la peau des prisonniers ou , plus effrayante , la fleur de lys sur leur épaule .
j' en remarquai un qui discourait au milieu d' auditeurs attentifs et qui me désigna du bout de sa canne .
il connaissait sans doute les détails de mon histoire car j' entendis le mot de Golconde voler sur les bouches .
en rang , en rang !
crièrent les argousins en distribuant des coups de bâton au hasard pour nous faire obéir .
nous étions près de cent cinquante à claquer des dents , nus comme des bêtes , et une buée montait du troupeau .
trois médecins nous firent défiler devant eux , nos vêtements à la main .
ils jetaient à peine un regard sur le prisonnier qui se présentait , lui posaient parfois une question , et lançaient invariablement :
bon pour le bagne !
un vieillard au bord de la tombe ?
bon pour le bagne !
un poitrinaire finissant de cracher ses poumons ?
bon pour le bagne !