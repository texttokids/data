je me chargeai de donner quelques leçons de français à Nini , le plus important étant de lui apprendre la forme interrogative .
tu ne diras pas : " Ousqu' elle est , ma frangine ? " , tu diras : " qu' est devenue ma soeur ? "
Ké dev' nue !
ah , ah , ké dev' nue !
un matin , j' escortai Nini jusqu' aux abords de l' hôtel du duc d' Écourlieu .
puis , un peu inquiet , je la laissai s' éloigner vers la porte des fournisseurs .
elle y frappa trois coups avec le heurtoir avant de disparaitre à ma vue .
j' attendis son retour sous le porche où le faux manchot avait fait le guet , la nuit où le Golconde avait été volé .
Nini m' y rejoignit bientôt , l' air contente d' elle .
voilà .
j' ai causé avec la cuisinière et avec le groom .
ta baronne , elle est repartie en province , chez sa tante .
il y a longtemps ?
la nuit d' après la mort du vieux .
Moïra n' avait guère attendu avant de filer .
Nini , les sourcils froncés , semblait hésiter à me dire autre chose .
j' ai causé avec la femme de chambre .
mais parait qu' elle est un peu fêlée ...
on t' a dit qu' elle était somnambule et qu' elle inventait des choses ?
tout juste !
et je crois ben qu' elle m' a fait des batteries , comme quoi la baronne était folle et qu' une nuit deux gendarmes l' ont emmenée à la clinique du docteur Lablache .
j' étais prêt à parier que la femme de chambre n' inventait rien .
comme elle était somnambule et errait dans les couloirs , il lui arrivait de surprendre des secrets .
mais ké dev' nue ma soeur ?
me demanda Nini , la voix rigolarde .
ma tête à couper , elle est chez les fous !