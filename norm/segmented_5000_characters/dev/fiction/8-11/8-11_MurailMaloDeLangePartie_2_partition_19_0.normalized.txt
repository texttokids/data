la petite porte d' entrée des témoins venait de se rouvrir et quelqu' un était entré .
qui êtes vous , monsieur ?
qui vous a permis de troubler le déroulement de ce procès ?
je n' ai pu arriver plus tôt , monsieur le président , et je vous prie d' accepter mes excuses .
mon nom est Eugène Donnadieu .
mais on me connait mieux sous le nom de monsieur Personne .
une rumeur parcourut la salle .
monsieur Personne , c' était monsieur Personne !
cet homme redouté des voleurs , haï des assassins , méprisé de la police officielle , et auquel les gens de la bonne société refuseraient de serrer la main .
un paria , mais un paria redoutable , sachant les secrets des puissants comme ceux des misérables .
avancez , monsieur , lui ordonna le président .
que venez vous faire ici ?
dire la vérité , toute la vérité , je le jure .
il a été dit ici que Malo de Lange est un agent de ma brigade , c' est exact .
que je lui avais confié la mission de veiller sur les biens de monsieur le duc d' Écourlieu , c' est exact .
qu' il a failli dans sa mission et qu' il a volé le Golconde , c' est faux .
mon père parlait avec une telle autorité que personne ne songea à l' interrompre .
aussi poursuivit il :
le voleur du Golconde est Antonin Meulière .
engagé en tant que valet de chambre grâce à de fausses lettres de recommandation , il a opéré comme vous l' a décrit le commissaire Jacquot .
dès le lendemain du crime , il a quitté l' hôtel du duc sans même donner son congé et il est venu se réfugier chez un ami , un saltimbanque comme lui .
connu dans les foires comme " le cannibale du Gabon " , il s' appelle en réalité Jean François Ledoux , et il est prêt à témoigner .
jean françois Ledoux n' est en aucune façon un complice d' Antonin Meulière .
mais dans le milieu des saltimbanques , on s' entraide .
aussi a t il accepté de prêter des vêtements à Antonin , qui portait toujours sur lui sa livrée de domestique .
Antonin espérait pouvoir se fondre dans la foule de la grande ville .
ce n' est pas à la police qu' il voulait échapper , puisque celle ci croyait avoir arrêté le coupable .
ceux qu' il fuyait , c' étaient ses propres complices avec lesquels il n' avait pas l' intention de partager le butin .
mais ils l' ont finalement retrouvé et torturé à mort sans pour autant le faire parler .
plusieurs personnes ont vu le corps d' Antonin à la morgue et pourront attester qu' il s' agissait bien de lui .
mon père s' arrêta à ce moment là et croisa les bras sur son ample redingote , qui le faisait paraitre plus ventru qu' il n' était .
votre histoire est passionnante , fit maître Chanterelle , qui s' était ressaisi .
mais elle présente un inconvénient .
rien ne vient prouver qu' elle est vraie , sinon la parole de quelques canailles dont on peut fort bien acheter les témoignages .
je peux vous montrer la veste que Ledoux a prêtée à son camarade et qu' il portait le jour de sa mort .
l' avocat haussa une épaule :
est ce une preuve que cela ?
je peux vous montrer la livrée de domestique qu' Antonin a laissée chez Ledoux .
j' avais du mal à suivre le débat tant ma cervelle était embrumée .
mais il me sembla que mon père était en train de proférer un mensonge pour me sauver .
ce n' est pas non plus une preuve , fit maître Chanterelle , un peu moins sûr de lui .
dans ce cas , je peux aussi vous montrer le Golconde , qui se trouvait dans une poche de cette livrée ...
mon père écarta les pans de sa redingote et se défit de sa bedaine postiche : c' était un paquet entouré de papier brun et solidement ficelé .
il le tendit à un huissier pour qu' il le remît au président .
toute la salle s' était figée comme dans le conte de La Belle au bois dormant .
des ciseaux , chuchota le président .
le greffier lui en tendit une paire .
la ficelle fut rompue , le papier écarté , et le président , aidé d' un de ses assesseurs , déploya la livrée de domestique qui avait appartenu à Antonin .
il la fouilla et sortit d' une poche un gros caillou qu' il éleva dans la lumière .
c' était le diamant bleu qu' un lord anglais avait rapporté des Indes , ce diamant de cent quarante carats qui valait à lui seul douze millions de francs et qui avait couté la vie au duc d' Écourlieu .
un mot , un seul mot fut répété de bouche en bouche .
le Golconde .
le Golconde , enfin , c' était lui !
des applaudissements éclatèrent comme à la fin d' un spectacle .
monsieur Personne ayant prouvé devant des centaines de témoins qu' Antonin Meulière était le voleur et l' assassin , je fus très vite remis en liberté .
et un mardi ensoleillé je me rendis galerie Véro Dodat , où je retrouvai tout à la fois mon chef et mon papa .
julien Donnadieu !
m' accueillit il .
filou , voleur , tête de bois !
il me serra dans ses bras .
allons , assieds toi , mon garçon .
tu n' as pas trop mauvaise mine .
moi , je crois que tu m' as fait perdre dix kilos .
il avait l' air fou de joie .
après tout , il devait m' aimer joliment .
alors , me dit il en riant , es tu fier de moi ?
n' as tu pas un père admirable ?
oh , si , fameux !
vous avez retrouvé l' homme au chapeau noir et vous lui avez repris le Golconde ?