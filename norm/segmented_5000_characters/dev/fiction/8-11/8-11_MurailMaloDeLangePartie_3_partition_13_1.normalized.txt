on vous réveillera demain à cinq heures .
bonne chance , lieutenant .
le sommeil tarda à venir dans mon petit réduit .
on y entendait toutes sortes de craquements , grincements , couinements .
soudain , le bruit d' une respiration toute proche me força à rouvrir les yeux .
je savais déjà qui j' allais voir .
c' est un rêve , pensai je , le rêve où on rêve qu' on se réveille .
l' enfant du Temple était là , il ressemblait tellement à Montpensier que j' eus l' espoir que c' était lui :
toto ?
mais l' enfant portait les vêtements anciens qu' il avait sur le portrait .
veillez , me dit il .
veillez , lieutenant , nous sommes en grand danger .
ce fut un domestique qui me secoua par l' épaule au petit matin .
il posa près de moi un plateau sur lequel il y avait des choses que je n' avais pas mangées depuis longtemps , de la brioche et une tasse de chocolat chaud .
dites moi , monsieur , demandai je , savez vous s' il y a des fantômes aux Tuileries ?
oh , il n' y a que ça .
d' ailleurs , moi aussi , j' en suis un .
il m' indiqua que je trouverais dans le cabinet de toilette voisin ma tunique de collégien et mon cartable .
je devais être prêt dans la demi heure .
le général Sébastiani m' avait prévenu que je passerais aux yeux des autres pour un élève ordinaire .
rien ne devrait signaler mon appartenance à la Sûreté .
ma tunique étant très serrée , je glissai mon pistolet de poche dans mon cartable entre deux cahiers puis je vérifiai que mon poignard était bien lacé contre mon mollet .
je fis la connaissance d' Aumale au pied du grand char à bancs bleu et jaune qui devait nous transporter au collège .
lui ne portait pas l' uniforme du collégien .
grand pour ses treize ans , il avait l' air distingué et lointain d' un gentleman anglais .
il me salua du bout des lèvres puis s' assit à côté de son précepteur , monsieur Cuvillier Fleury , un homme encore jeune au grand front dégarni .
Montpensier s' assit à côté de son propre précepteur , monsieur Tenant de Latour , mais il passa tout le trajet la tête dévissée de mon côté , et dans un état d' excitation que rien ne pouvait calmer .
ce fut le proviseur en personne qui vint nous accueillir .
je lui fus présenté comme le fils d' une amie de madame Adélaïde et je prétendis avoir treize ans .
vous serez donc dans la classe d' Aumale , me dit le proviseur au grand désespoir de Montpensier .
je n' avais jamais vu une salle de classe de toute ma vie .
quand j' entrai , les élèves étaient déjà tous à leur pupitre , une soixantaine de garçons alignés sur trois rangées et mis deux par deux comme dans la chaîne des forçats .
debout , dit le professeur en faisant claquer sa règle sur le bureau .
assis , dit le proviseur .
jeunes gens , je vous présente votre nouveau camarade , monsieur de Lange .
je vis quelques sourires , j' entendis quelques chuchotements .
mon nom amusait .
on me désigna une place située juste devant le bureau du professeur .
tenez , me dit monsieur de Rechinchat ( c' était le nom du professeur ) , je vous prête mon Cicéron .
lisez nous ce paragraphe du De amicitia .
je ne suis pas trop mauvais pour la lecture , sauf si les phrases ont des rallonges .
mais je m' aperçus au premier coup d' oeil que ce monsieur Cicéron écrivait n' importe quoi .
eh bien , lisez , s' impatienta monsieur de Rechinchat .
je fis la grimace et commençai :
Cum ...
sa ...
saepe mmulta ?
j' interrogeai le professeur du regard .
mais enfin , allez plus vite , monsieur , nous n' allons pas y passer la matinée !
on dirait que vous n' avez jamais lu de latin de votre vie .
ah , c' est du latin !
je me disais aussi que cela ne ressemblait à rien .
quelques élèves se mirent à rire .
silence , messieurs !
s' écria Rechinchat en faisant claquer sa règle .
blaise Pascal a dit que qui veut faire l' ange fait la bête .
je vois , Lange , que vous faites la bête à merveille .
cette fois , les rires se déchainèrent .
je regardai autour de moi , un peu ahuri .
ce n' étaient que des momacques .
au pistolet , au couteau , à la boxe , à la savate , ils ne valaient rien .
et pourtant , ils se moquaient de moi !
monsieur , je ne sais pas d' où vous sortez , me fit le professeur sur un ton de pitié , mais je ne vais pas pouvoir vous garder dans mon cours .
vous devez redescendre en sixième .
je le regardai , encore plus ahuri .
c' était un maigrichon que je pouvais envoyer dans le mur d' une taloche .
malheureusement , je n' étais pas chez Papa Guillotin .
Baudelaire , accompagnez ce garçon chez monsieur Barbote .
un élève au teint blafard de petit Parisien et à l' air sournois quitta son banc dans le fond de la classe .
il me fit arpenter de longs couloirs sombres et puants qui me rappelèrent la prison de Bicêtre .
tu dois être meilleur aux barres qu' en version latine , me dit Baudelaire au moment de nous séparer .
je te prendrai dans mon équipe .
il frappa du poing à une porte puis se sauva quand retentit le " entrez ! " de l' autre côté .
j' entrai donc chez les petits , mais comme ils étaient au moins soixante , je n' aperçus pas tout de suite Montpensier .