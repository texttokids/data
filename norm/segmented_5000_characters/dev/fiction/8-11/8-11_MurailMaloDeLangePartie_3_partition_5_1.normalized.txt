c' est juste qu' un chien .
pourquoi c' est ti qu' on s' occupe de chiens ?
voulut il savoir .
parce que quelqu' un qui tue des chiens peut un jour trouver amusant de tuer des gens , lui répondit posément monsieur Personne .
puis il se tourna vers moi :
vous pouvez vous dispenser de m' accompagner , lieutenant .
il pensait m' être agréable , mais je refusai d' un hochement de tête .
je voulais voir , je voulais savoir .
cependant , les lectrices qui ne souhaitent pas retourner à la morgue peuvent se dispenser de lire le paragraphe suivant .
vous allez être contents , nous dit le morgueur .
il est frais du jour .
le chien se détachait , blanc sale sur le marbre noir .
oui , il ressemblait à Fiston .
c' était un bâtard à tête noire et aux pattes crottées .
je vis alors mon père sortir de sa poche son lingriot .
( canif )
il a déjà été ouvert , m' expliqua t il en désignant le chien de la pointe du couteau , il a été ouvert et recousu .
il fit sauter quelques fils puis , penché au dessus du cadavre , il se mit à renifler comme quelqu' un qui cherche à identifier une odeur .
jamais senti ça , marmonna t il .
il plongea la main dans la bête et en retira un chiffon maculé de sang .
l' odeur vient de là , dit il en me tendant la guenille .
je ne pus faire autrement que de m' avancer et de renifler à mon tour .
papa , c' est l' odeur du théâtre !
c' était cette liqueur qui rend léger comme un ballon , l' éther éthylo ...
je ne trouvais plus le mot .
mon père m' attrapa brusquement par le bras et me chuchota :
évite de m' appeler papa en public .
il me repoussa tout aussi brusquement :
lieutenant , vous surveillerez ce monsieur Wizzard .
je n' avais pas envie de soupçonner le magicien d' être le benk .
mais il avait tous les défauts du diable : il était élégant , spirituel et mystérieux .
les jours suivants , nous prîmes nos renseignements , Gaby et moi .
le magicien avait bien signalé au commissaire Jacquot la disparition de Monsieur Jones .
le singe avait été mal attaché après un spectacle et il s' était sauvé .
par ailleurs , j' appris que l' inimitable Wizzard s' appelait Jean Eustache Bertin , qu' il était marié et père de deux enfants , dont le petit Ernest qui remplaçait désormais le singe savant .
toute la famille Bertin vivait au dessus du théâtre , au cent soixante trois de la galerie Valois , qui , comme le cent soixante et un , appartenait au marquis Victor de Montval .
rien de diabolique dans tout cela .
mais puisque mon chef m' avait recommandé de surveiller le magicien , j' allai deux soirs de suite au spectacle .
mon père avait beau me dire que la scène était truquée , que tout n' était que trappes et doubles fonds , je ne m' expliquais toujours pas comment une jeune fille , enfermée dans un cercueil d' où dépassaient sa tête et ses pieds , pouvait se laisser scier en souriant .
le troisième soir , alors que j' étais déjà installé dans mon fauteuil de velours , j' eus la surprise de voir entrer , avec accompagnement de bruit de bottes et de sabres , une escouade de militaires .
ils encadraient une dame au teint rougeaud dont j' appris , grâce aux chuchotements de l' assistance , qu' il s' agissait de madame Adélaïde , la soeur du roi .
je la regardai avec la curiosité qu' on peut avoir pour les gens qui ont traversé de grands malheurs .
son père avait été guillotiné , deux de ses frères , prisonniers pendant la Révolution , étaient morts de maladie , elle même avait vécu toute sa jeunesse dans l' exil et la pauvreté .
je savais qu' elle était restée célibataire , mais qu' elle adorait ses huit neveux et nièces .
on la disait plus intelligente que tout le conseil des ministres , mais elle était gracieuse comme un soldat de Napoléon à qui on aurait passé une robe .
madame Adélaïde fut accueillie par le propriétaire du théâtre , un grand bonhomme sec et agité , qui s' empressa de la conduire au premier rang en bégayant d' émotion :
c' est un no ...
un no ...
un honneur !
l' inimitable Wizzard se surpassa ce soir là .
il ajouta même à son programme un numéro que je n' avais encore jamais vu .
il ligota les pieds et les mains de son fils Ernest , puis le plaça sous un énorme gobelet noir .
après avoir prononcé quelques formules magiques , il tira en l' air un coup de pistolet qui mit sur le qui vive tous les militaires .
le gobelet se renversa , l' enfant n' était plus là .
ohé !
fit une voix .
je me retournai .
le petit Ernest était au fond de la salle .
mon père avait beau dire , il n' y avait à ce prodige aucune explication .
le tour s' appelait " l' enfant transporté " .