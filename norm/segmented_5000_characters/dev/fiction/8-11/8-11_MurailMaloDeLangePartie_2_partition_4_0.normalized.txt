je courus jusqu'à la rue Cloche Perce fouiller dans mon armoire .
j' y trouvai mon déguisement féminin , la robe et le chapeau que je possédais depuis deux ans .
malheureusement , on ne peut pas toujours s' empêcher de grossir , comme disait le bouton qui était devenu une pustule .
quand je passai la robe , elle craqua dans le dos .
mon premier mouvement eût été de me précipiter chez Léonie .
mais une demoiselle de Bonnechose ne s' habille pas comme une fille du peuple .
à qui demander une robe ?
eh mais , à Nini la farceuse !
quand je repris place à mon poste d' observation derrière la cloison , la nuit était tombée sur la rue Bridaine et toute la famille Guibole était assemblée autour du diner .
il y avait des côtelettes , des pommes de terre , du chou , un beau morceau de fromage , et des confitures pour le bébé .
je compris que Trois Guiboles avait fait la carouble dans la journée et qu' il avait été payé .
lanturlu avait maintenant une clé pour entrer chez le duc d' Écourlieu .
servez moi un peu de vin , c' est du bon !
réclama Trois Guiboles en essayant de prendre un ton enjoué .
du vin de voleur , vrai , t' es pas dégouté , grommela la Louise .
malgré tout , elle mangeait de bon coeur de la viande de voleur , et les patates avec .
le bébé , qu' on appelait Mouzette , avait de la confiture jusque dans les sourcils .
il te reste ti un peu d' argent , papa ?
demanda Nini d' une petite voix .
pourquoi c' est faire ?
je voudrais voir un docteur pour Mouzette .
faudrait lui donner quéque chose pour lui fortifier les jambes .
trois guiboles sortit une pièce d' or de sa poche et la plaqua sur la table d' un geste sonore .
tiens , v' là pour ta Mouzette !
de l' or de voleur , marmonna la Louise , impressionnée .
elle savait bien que son mari le paierait de sa vie .
quand la Mouzette marchera , tu seras pas contente , peut être ?
répliqua Trois Guiboles .
Nini fit disparaitre la pièce dans la poche de son tablier et se leva pour embrasser son père .
le pauvre gars en fut tout réconforté :
va donc nous chercher un peu d' aut vin !
Nini attrapa son châle pour aller jusqu'à la taverne du bout de la rue .
je décollai mon oeil du trou de surveillance et je descendis l' escalier quatre à quatre pour l' attendre dehors .
mademoiselle ...
mademoiselle Nini !
elle tourna vers moi ses gros yeux d' un vert marécageux et sa bouche molle de poisson mort .
n' ayez crainte .
je suis votre voisin de palier , Malo .
j' aurais voulu vous dire deux mots .
mon attitude polie et ma figure angélique ( soit dit sans me vanter ) la disposèrent à m' écouter .
je sais que votre père a fabriqué une fausse clé pour un nommé Lanturlu .
Nini se recula d' un pas avec effroi .
vous avez entendu parler de monsieur Personne ?
ajoutai je en faisant un geste vers elle pour l' empêcher de fuir .
elle fit signe que oui .
monsieur Personne était celui qui avait valu quinze ans de bagne à son père .
je suis dans sa brigade , mais je veux vous aider .
lanturlu est un assassin .
et il tue tous ceux qui savent ce qu' il fait .
la pauvre Nini poussa un gémissement et les coins pendants de sa bouche se mirent à trembler comme si elle allait pleurer pour de bon .
je la pris par le bras et nous marchâmes jusqu'à la taverne .
je lui expliquai la mission que m' avait confiée le chef de la Sûreté : m' introduire dans la domesticité du duc d' Écourlieu et veiller sur son hôtel jour et nuit .
mon projet de déguisement en femme de chambre tira de Nini un sourire .
quand les coins de sa bouche remontaient , elle pouvait être jolie .
vous êtes plus grand que moi , monsieur Malo .
je rallongerai ma robe .
elle n' en avait que deux , celle qu' elle avait sur le dos et celle qu' elle allait me prêter .
au moment de la quitter , je lui glissai mes derniers sous dans la main :
je crois que Mouzette aime les confitures ?
l' effroi la reprit :
vous savez tout de nous z' aut !
les murs ont des oreilles , mademoiselle , et certains ont même des yeux .
le lendemain , l' intendante du duc d' Écourlieu parut surprise en recevant Hortense à l' office :
c' est incroyable ce que vous ressemblez à votre frère !
c' est toujours ce qu' on nous dit , répondis je en faisant une petite révérence du meilleur gout .
vous semblez mieux élevée que lui .
ce n' est pas dans ma nature de rougir quand on me fait un compliment , comme disait Blanche Neige au miroir qui la trouvait mieux que sa belle mère .
mais je baissai les yeux avec modestie .
la fille qui a été renvoyée était l' habilleuse de madame , m' apprit l' intendante .
avez vous déjà servi d' habilleuse ?
jamais , répondis je , toujours avec modestie .
un silence suivit mon aveu .
eh bien , ma petite , vous avez intérêt à apprendre vite , conclut l' intendante .
madame la baronne n' est pas connue pour sa patience .
Moïra de Feuillère , ma patronne , était au lit .
les rideaux des fenêtres étaient encore tirés et seul un petit amour en bronze doré répandait la douce lumière d' une veilleuse dans la chambre à coucher .
mon Dieu , est ce possible , il fait jour ?
dit une voix mourante au milieu des oreillers .