l' opération de police de la rue d' Enghien n' avait pas été un succès .
Orson avait réussi à s' échapper en poignardant au passage un gendarme .
les recherches entreprises pour le retrouver n' avaient rien donné .
il n' était pas retourné dans le campement bohémien , et Zina avait également disparu .
Paluchard avait été blessé à l' épaule .
interrogé par le commissaire Jacquot , il avait reconnu être un faux Louis le dix septième et un vrai forçat évadé .
ainsi se termina la carrière de monseigneur le duc de Normandie .
monsieur de Bonnechose , furieux d' avoir été trompé , se fâcha avec Côme de la Trimbaldière .
mais , sa situation financière ne s' étant pas améliorée , il se mit à chercher un mari pour sa fille parmi les banquiers et les financiers .
il y avait un certain monsieur Péreire de quarante cinq printemps qui avait l' air de trouver Léonie à son gout .
mon père m' avait dit que , si mademoiselle de Bonnechose était ruinée , mes chances de l' épouser augmenteraient .
c' était faux , car je n' avais rien à lui offrir .
mes quinze ans ?
mon taudis de la galerie Véro Dodat ?
mon maigre salaire de lieutenant de la Sûreté ?
tu vas avoir de la promotion , me dit mon père .
on demande à te voir au château .
quel château ?
aux Tuileries .
quelqu' un a parlé de toi à madame Adélaïde .
c' est la soeur du roi .
je sais qui est madame Adélaïde .
je l' ai même vue au théâtre du Palais Royal .
c' est elle qui veut me voir ?
le général Sébastiani est un de ses grands amis .
c' est le militaire qui était l' autre jour dans le bureau de Gisquet et qui t' a écouté raconter l' attentat .
il n' a pas dit grand chose , mais je pense qu' il a apprécié ton courage et ta simplicité .
je rougis de satisfaction .
quelqu' un allait il enfin reconnaitre que je m' étais conduit héroïquement ?
mon père avait reçu l' ordre de m' accompagner au pavillon de Flore , là où madame Adélaïde recevait ses invités .
mon seul effort de toilette pour me présenter devant une altesse royale fut de m' habiller en Rodolphe Lequeu .
mon père dans sa redingote à la mode me fit une pénible impression .
elle n' était pas taillée pour ses grosses épaules et son dos trop large .
on aurait dit un lion déguisé en chien de salon .
nous entrâmes aux Tuileries entre deux piquets de gardes nationaux à cheval et nous fûmes tout de suite accueillis par deux laquais à perruque poudrée , qui nous escortèrent jusqu' aux appartements de madame Adélaïde .
monsieur Personne , comme c' est aimable à vous de me rendre visite , fit son altesse la soeur du roi .
elle avait l' air d' oublier que nous avions été convoqués .
il y avait là le général Sébastiani , très raide , et un autre monsieur qui ne pouvait quitter son siège car son pied , pris par la goutte , devait rester posé sur un tabouret .
pendant que tout ce monde échangeait des politesses , je regardai autour de moi .
le salon de madame Adélaïde était aussi froid que ses occupants .
des livres , des journaux , une table de travail surchargée de paperasses .
nom d' unch , quel endroit ennuyeux !
c' est vous , mon garçon , qui avez sauvé la vie de votre roi ?
je sursautai .
la question m' était adressée et c' était madame Adélaïde qui venait de me la poser .
mon Dieu , qu' elle était laide avec son gros nez bourgeonnant , ses yeux ronds et son menton rengorgé !
oui , madame .
enfin , je crois .
vous n' en êtes pas sûr ?
insista madame Adélaïde de cette voix si peu féminine qui lui sortait du nez .
si , si !
je compris qu' on attendait de moi le récit de l' attentat , mais l' émotion me fit dire un peu n' importe quoi :
comme je ne voyais rien à cause des shakos , j' ai pensé à Zachée et je suis monté dans un arbre pour voir passer Jésus , je veux dire , Louis Philippe .
c' était un beau spectacle , les chevaux , les soldats , j' allais crier " vive le roi ! " quand ...
vous l' aimez bien votre roi , n' est ce pas ?
me coupa madame Adélaïde , attendrie .
heu ...
oui .
mais sur une barricade , je crierais " vive la République ! " .
c' est une question d' ambiance .
le monsieur goutteux éclata d' un rire sec .
mon fils a fait son devoir , rien de plus , intervint mon père , qui avait hâte de s' en aller .
oh , c' est votre fils ?
s' étonna madame Adélaïde .
quel âge a t il ?
quinze ans , votre altesse .
c' est un bel enfant , je vous en fais mon compliment .
sa voix avait changé .
elle s' écria soudain :
ah , général , général !
ils me tueront mon frère ou l' un de mes neveux !
mais non , chère amie , protesta le militaire .
nous veillons sur eux .
vous veillez !
vraiment ?
mais si ce petit garçon n' avait pas été là , nous n' aurions plus de roi !
j' aurais préféré ne pas être traité de petit garçon , mais l' intention était gentille .
on entendit alors un gratouillis au bas de la porte .
oh , monsieur Personne , soyez assez aimable pour ouvrir à ma chienne , le pria madame Adélaïde .
une jolie petite bête entra , tout de suite suivie par un jeune garçon blond comme les blés , en pantalon blanc et veste de velours bleu .
toto !
s' écria madame Adélaïde .