monsieur Personne laissa s' écouler quelques secondes de silence .
" il a deviné , pensai je , il sait qu' elle ment . " j' attribue parfois à mon père des facultés extraordinaires , alors qu' il est un homme d' une intelligence médiocre .
lieutenant , au rapport .
j' obéis en prenant à mon compte le récit de Gaby .
tu me dis que tu n' as pas vu ces bandits , mais tu les as entendus , remarqua mon père .
n' y avait il rien de particulier dans leurs voix ?
je secouai la tête mais , dans le même temps , Gaby s' écria :
si !
un des hommes bégayait !
mon père la regarda en silence et elle se mit à rougir , puis à bafouiller :
c' est ...
c' est Malo qui me l' a dit .
tu ...
tu te rappelles , Malo ?
c' est exact , il bégayait .
un éclair bleu partit des yeux sombres de monsieur Personne :
je connais un habitué du Lapin Volant qui bégaie .
on l' appelle Quiqui .
c' est un venternier .
je vais demander à Frédo de le surveiller à partir de demain .
et si le meurtre a lieu cette nuit ?
le questionna Gaby .
on arrêtera les assassins .
on ferait mieux d' empêcher le crime !
pourquoi ne pas surveiller la rue des Prouvaires cette nuit ?
quelle maison ?
quel étage ?
quel appartement ?
Gaby , même si elle avait peur du chef , lui tenait parfois tête :
la rue n' est pas très grande .
Malo et moi , on pourrait ...
Blam !
monsieur Personne tapa des deux poings sur son bureau , nous faisant sauter en l' air , Gaby , moi et l' encrier .
tu ne discutes pas mes ordres !
et tu ne quittes pas ton poste pour aller te réchauffer !
en fait , mon papa est vraiment quelqu' un d' extraordinaire .
et toi , me dit il , couche toi de bonne heure .
tu as une sale mine .
nous nous quittâmes , Gaby et moi , à l' entrée de la galerie .
elle me jeta un regard de chien battu :
je te paie un verre , veux tu ?
non , je rentre chez moi .
ma mauvaise humeur me tient compagnie .
il n' aime personne .
qui ça ?
le chef ?
elle eut un triste sourire :
personne n' aime personne .
je regardai s' éloigner ce pauvre moineau de Gaby , sautillant entre les flaques de boue .
savait elle seulement où elle allait passer la nuit ?
moi , en revanche , j' avais un projet , un projet qui me mena tout droit rue des Prouvaires .
Gaby avait raison : cette rue n' était pas longue .
en passant devant le volet baissé d' une boucherie chevaline , je pensai soudain à ce conte où le chasseur doit rapporter à la méchante reine le foie et les poumons de Blanche Neige comme preuves de sa mort .
où vivait l' enfant dont on voulait arracher le coeur ?
dans cette maison bourgeoise ?
derrière cette fenêtre éclairée ?
j' avais décidé de monter la garde jusqu' au matin .
comme la neige s' était remise à tomber à gros flocons , je m' abritai sous un porche en face de la boucherie .
les longues stations solitaires dans la nuit et le froid , ce n' est pas le meilleur aspect du métier , comme disait l' Éventreur du Châtelet .
je songeai à la tiédeur du Lapin Volant , au tintement amical des verres , au bon souper de la mère Gargoton , et je me décollai du mur .
bah , une nuit pareille , même les assassins restent chez eux .
comme je m' engageais rue de la Petite Truanderie , je fus bousculé par une femme en châle qui semblait surgie de nulle part .
si je ne l' avais pas entendue venir , c' étaitussi incroyable que cela parûtarce qu' elle marchait pieds nus .
Ouch !
c' est des façons avec les dames !
s' écria t elle en se frottant l' épaule .
mais c' est plutôt vous ...
à la lueur du réverbère , je vis tout de suite à qui j' avais affaire .
une bohémienne , des anneaux d' or aux oreilles .
viens , viens , me dit elle en me tirant par le bras , je vais te lire les lignes de la main .
ouais , c' est ça !
tu veux me faire les poches , répliquai je en la repoussant .
allons , ne fais pas le méchant , je te dis l' avenir contre un verre d' eau d' aff .
j' avais froid , le Lapin Volant était encore loin , et les bohémiens peuvent vous apprendre beaucoup de choses .
c' est ce que mon père prétend .
voyant que j' étais tenté , la fille me reprit le bras :
viens chez Polidori , c' est à côté .
il a du parfait amour , c' est bon comme si les anges te pissaient dans la bouche .
la drôlesse savait trouver les mots qui touchent .
cinq minutes plus tard , nous étions attablés chez Polidori devant un verre d' une étrange liqueur violette .
j' en pris une lampée et je fis claquer ma langue .
fameux , le parfait amour .
une boule de feu qui vous descend jusque dans le ventre .
la fille qui me faisait face était une vraie bohémienne au teint cuivré , pouilleuse et effrontée .
lavée et correctement habillée , elle aurait pu être belle , mais elle aurait toujours senti l' enfer .
je lui tendis ma main droite .
en voilà une main de paresseux !
bien douce , bien lisse !
je suis un employé de bureau , ricanai je .
de la main gauche , je m' envoyai une autre lampée dans le gosier .
ta ligne de coeur est longue , me dit la fille .
elle suivit la ligne du bout de son ongle sale et je ne pus m' empêcher d' imaginer que Léonie de Bonnechose m' attendait au bout de ce sillon .
tu es fidèle en amour , c' est une bonne chose .
malgré moi , je tressaillis .