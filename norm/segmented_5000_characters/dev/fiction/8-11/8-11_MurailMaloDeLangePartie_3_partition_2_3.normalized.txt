pour lui changer les idées , je parlai à Gaby de l' apparition de l' enfant sous le réverbère et du portrait qui lui ressemblait chez le docteur Pelletan .
il n' y avait qu' à Gaby que je pouvais raconter certaines choses sans me sentir gêné .
je ne sais pas s' il y a un rapport , marmonna Gaby , le menton dans la main , mais Frédo a trouvé cette nuit dans un tas d' ordures un chien crevé à qui on avait ouvert les côtes .
eh bien , figure toi qu' on lui avait arraché le coeur !
d' un geste brusque , j' écartai la main de Gaby :
répète un peu ce que tu viens de dire !
un chien , quel genre de chien ?
heu ...
un petit chien .
blanc ?
un caniche ?
peut être bien .
pauvre Joliette , soupirai je .
un peu plus tard dans la journée , je me fis confirmer par Frédo qu' il s' agissait bien d' un caniche blanc .
sa cage thoracique avait été ouverte à la scie et le coeur prélevé plutôt qu' arraché .
du travail soigné , un travail de chirurgien , n' est ce pas , docteur Pelletan ?