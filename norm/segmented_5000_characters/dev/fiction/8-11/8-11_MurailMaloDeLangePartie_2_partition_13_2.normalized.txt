je l' embrassai sur le nez et allai déposer le message dans son panier .
un sifflement terrible traversa la rue .
Mouchique m' avertissait d' un danger .
je me précipitai vers la fenêtre et regardai au dehors .
la rue semblait toujours paisible , mais mieux valait ne pas s' attarder .
je descendis à la va vite au risque de me rompre le cou .
eh bien , quoi , qu' est ce qu' il y a ?
rien , me répondit nonchalamment Mouchique .
je n' aime pas faire le guet .
Mouchique était ainsi .
à prendre ou à laisser , comme disait le boucher à saint Nicolas , en lui proposant du saucisson de petit garçon premier choix .