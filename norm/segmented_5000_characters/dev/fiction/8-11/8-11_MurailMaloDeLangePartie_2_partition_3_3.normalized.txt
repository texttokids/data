monsieur Personne s' inclina et disparut aussi discrètement qu' il était apparu .
dix fois pendant cette entrevue mon coeur avait bondi tandis que mon père avalait les insultes .
après quelques pas dans la rue de Jérusalem , je pus enfin déverser ma colère :
cet homme ne fait aucune différence entre nous et les bandits que nous arrêtons !
mon père s' immobilisa sur le trottoir et j' eus un instant l' espoir qu' il allait retourner boxer monsieur le préfet .
il se contenta d' inspirer largement .
bien , conclut il en soufflant .
je vais devoir mettre fin à la carrière de Dubuc .
quant à toi , Pégriot , devine qui va t' employer ?
je le regardai , décontenancé .
n' avait il pas affirmé à l' instant que je serais son lieutenant ?
allons , réfléchis : Pigrièche a quitté le service du duc d' Écourlieu ...
je me fais embaucher à sa place ?
mais les choses ne furent pas si simples .
sous le nom de Malo Quincampoix , je me présentai devant l' intendante du duc d' Écourlieu avec d' excellentes lettres de recommandation , parfaitement fausses .
je regrette , me dit elle , mais il y a déjà un groom dans la maison .
et pour être valet de pied , vous êtes trop jeune .
une idée me traversa l' esprit :
ma soeur ainée a pas de travail non plus .
Not' père est aveugle et not' maman paralytique ...
c' est bon , c' est bon , envoyez la moi , m' interrompit l' intendante .
madame la baronne de Feuillère a besoin d' une femme de chambre .
la baronne , qui était la maîtresse du duc d' Écourlieu , vivait sous son toit , au grand scandale de la bonne société .
comment s' appelle votre soeur ?
voulut savoir l' intendante .
Hortense ...
vous verrez , nous nous ressemblons beaucoup .
j' espère qu' elle est sage .
nous avons dû chasser la précédente , qui était tombée enceinte .
oh , ça , c' est quelque chose qui n' arrivera jamais à ma soeur .