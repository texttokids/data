c' est la bonne vieille méthode des chauffeurs pour délier les langues .
lanturlu !
pensai je .
mais pourquoi Lanturlu aurait il torturé ce pauvre Antonin ?
où sont ses affaires ?
demanda mon père au morgueur .
je vas vous montrer ce qu' il portait , mais c' étaient pas ses vêtements à lui : ils étaient ben trop petits .
le père Chaud Froid nous conduisit dans la salle où il suspendait les habits en attendant de pouvoir les revendre .
c' était un drôle de vestiaire où se mêlaient la redingote du bourgeois , le châle de la prostituée et la blouse du charretier .
monsieur Personne décrocha les vêtements de MangeMisère et les palpa .
après avoir passé la main sur le col de la veste , il regarda sa paume d' un air de dégout :
du noir de suageur , marmonna t il .
je ...
je vous attends dehors , dis je à mon père , car l' odeur de cette garde robe moisie me portait au coeur .
je sortis de la morgue en chancelant .
je soupçonnais monsieur Personne de m' avoir emmené en ces lieux pour me dégouter du métier .
mais un peu d' air frais suffit à me ranimer .
" réfléchissons , me dis je . Antonin faisait partie de la bande de Lanturlu et on sait que Lanturlu a la mauvaise habitude de tuer ses complices ... oui , c' est cela , Lanturlu a tué Antonin . mais pourquoi lui a t il brûlé la plante des pieds ? eh mais , oui ! pour lui faire avouer où il avait caché le Golconde . " Antonin alias Mange Misère était le voleur du diamant .
mais au lieu de le rapporter à son chef , il avait voulu le garder pour lui .
alors , fit une voix dans mon dos , cet homme au chapeau noir , sais tu qui c' était à présent ?
je me retournai vivement vers mon père :
c' était Antonin ...
Antonin , dissimulé sous la cape et le grand chapeau , avait traversé le vestibule à l' heure où la femme de chambre souffrait d' insomnie .
il était entré chez le duc qui dormait profondément .
comme il savait où son maître rangeait le Golconde , le vol ne lui avait pris que quelques secondes .
mais que s' était il passé après ?
après ?
me dit Moïra que je retrouvai rue ClochePerce .
après , il a tué le duc .
mais je ne voyais ni comment ni pourquoi .
et maintenant , le Golconde est entre les mains de Lanturlu , soupira t elle .
peuh !
lanturlu n' a rien à voir là dedans , intervint Mouchique , qui comme d' habitude était allongé sur mon lit .
mange misère n' a jamais fréquenté les chauffeurs d' Orgères !
c' était un avaleur de sabre .
il ne connaissait que des monstres de foire , la femme à barbe , l' homme tronc et le cannibale du Gabon ...
Mouchique n' avait jamais autant parlé et il parut tout de suite le regretter .
et le noir de suageur sur sa veste ?
lui objecta Moïra .
c' est la pommade que les chauffeurs se mettent sur la figure , non ?
mais Mouchique ne voulut rien nous dire de plus .
tu as déjà volé à la venterne ?
lui demandai je pour changer de sujet .
non , mais je peux apprendre .
les voleurs à la venterne vont par deux .
l' un grimpe tandis que l' autre fait le guet .
Mouchique savait siffler très fort , ce qui est un atout pour un guetteur .
les Bonnechose habitaient dans une rue tranquille .
quand les beaux jours revenaient , la fenêtre de la chambre de Léonie restait entrouverte .
c' est au deuxième étage , dis je à mon collègue , une fois que nous fûmes rue de la Tour des Dames .
la porte du numéro sept s' ouvrit à ce moment là pour laisser le passage à monsieur de Bonnechose et à mon rival , Armand Furme d' Aubert .
ils semblaient les meilleurs amis du monde et montèrent ensemble dans un fiacre .
nous avions juste eu le temps de nous dissimuler sous une porte cochère .
une rue bien tranquille , hein ?
grogna Mouchique .
pour me faciliter la tâche , il se mit dos au mur et croisa les mains pour me servir d' appui pied .
il me souleva de terre , puis je montai sur ses épaules et attrapai un rebord de fenêtre .
la gouttière me vint ensuite en aide pour me permettre d' atteindre la fenêtre du deuxième étage .
Léonie n' était pas dans sa chambre .
il était huit heures du soir et elle dinait sans doute avec sa mère .
mais je devais me méfier de Margote , qui pouvait surgir à tout moment .
quelque chose me disait qu' elle n' était plus mon alliée .
j' avais préparé un message demandant une fois de plus un rendez vous sous la statue du Gladiateur mourant .
où le mettre ?
sur le lit ?
sur le secrétaire ?
quelqu' un risquait de le lire avant Léonie .
miaou .
je fis un bond .
miaou .
je fouillai la chambre du regard .
un petit chat noir et blanc sortit de son panier , s' étira avec élégance et vint se frotter contre moi .
morpion !
elle t' a gardé .
non seulement elle l' avait installé dans sa chambre à coucher , mais elle lui avait aussi passé un collier avec une médaille .
un nom y était gravé .
attends , Morpion , que je regarde ...
oh , oh , tu t' appelles Malo , mon garçon !