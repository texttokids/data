mes premières déclarations furent accueillies par des éclats de rire : les gendarmes ne voulaient pas croire que j' étais un agent de la Sûreté .
je demandai à ce qu' on aille chercher mon chef à son bureau de la galerie Véro Dodat .
le brigadier qui s' y rendit en revint seul .
le libraire lui avait dit qu' il n' y avait personne chez Personne .
enfin , le commissaire Jacquot , averti qu' un témoin ou un complice de l' attentat avait été arrêté , me fit sortir de ma cellule et me reconnut .
je lui racontai ce qui s' était passé : que nous avions remarqué , Gaby et moi , un individu aux allures louches , que je l' avais un instant perdu de vue , mais que j' étais intervenu à temps pour sauver le roi .
sauver , sauver , bougonna le commissaire Jacquot .
à cette distance , le roi n' aurait pas été touché .
je compris qu' il ne voulait pas m' attribuer un trop grand mérite .
est ce que le terroriste a crié " vive Louis le dix septième " avant d' être tué ?
me demanda t il .
je n' ai rien entendu .
est ce qu' on connait son identité ?
le commissaire Jacquot était passé à la morgue et il avait fouillé le cadavre .
rien ne permettait de l' identifier .
après m' avoir fait répéter dix fois mon témoignage , il accepta de me relâcher .
comme je sortais du commissariat , je me heurtai à mon père qui venait me chercher .
tu as le génie pour te trouver toujours aux bons endroits , grommela t il .
je dus raconter une fois de plus mon aventure .
il a crié " vive Louis le dix septième " ?
m' interrompit mon père .
oui , mais je n' en ai rien dit à Jacquot .
mon père regarda derrière lui comme quelqu' un qui s' assure qu' il n' est pas suivi , il plongea la main dans la poche de son manteau et en sortit un papier froissé .
Frédo m' a averti qu' un cadavre avait été porté à la morgue .
je l' ai fouillé avant tout le monde .
ce papier était sur lui .
je lus : à mort L' USURPATEUR !
la famille d' Orléans fait depuis trop longtemps le malheur de la France .
louis philippe et les siens doivent PÉRIR Du premier au dernier !
pourquoi vous ne donnez pas ce papier à Jacquot ?
parce que monsieur de Bonnechose a fait imprimer ce torchon .
je le sais , il m' en a donné un exemplaire quand j' étais à l' une de ses soirées , déguisé en Liechtemberg .
il faut que le père de ta fiancée cesse de comploter .
je ne pourrai pas éternellement le protéger .
un jour ou l' autre , il y aura une victime .
c' est déjà le cas .
le terroriste n' a eu que ce qu' il méritait .
je parlais de Gaby .
le visage de mon père se décomposa :
quoi Gaby ?
comment ?
vous ne savez pas ?
il m' attrapa par les épaules et me secoua en disant :
je ne sais pas quoi ?
mais ...
c' est ...
Gaby ...
qui ...
a pris la balle , répondis je , mes dents s' entrechoquant tandis que mon père continuait de me secouer .
nom de Dieu !
elle est ...
blessée .
à la jambe .
personne héla un fiacre et nous fit conduire à l' HôtelDieu .
il ne dit pas un mot pendant le trajet , mais parfois je voyais ses lèvres remuer comme s' il priait Dieu ou murmurait des malédictions .
peut être les deux .
il faisait nuit quand nous arrivâmes à l' Hôtel Dieu .
le chirurgien Pelletan , qui avait extrait la balle de la jambe , était reparti chez lui , rue des Prouvaires .
mon père demanda à parler à soeur Jeanne Marie , la religieuse qui rendait parfois service à notre brigade .
vous venez pour la pauvre jeune fille ?
elle a perdu beaucoup de sang .
elle se repose à présent .
je veux la voir , je ne la fatiguerai pas , promit monsieur Personne .
Gaby était dans la salle commune , mais isolée par des rideaux blancs , que soeur Jeanne Marie écarta :
voyez , elle dort .
mon père poussa la religieuse sans grand ménagement et se pencha au dessus de Gaby .
elle ne dort pas , dit il en se redressant .
elle est sans connaissance .
il se mit à lui tapoter les joues en l' appelant : " Gaby , Gaby " , puis il se tourna vers la soeur :
des compresses d' eau fraiche .
il y avait quelque chose de si farouche dans sa voix que la religieuse ne chercha pas à le contrarier .
pendant quelques longues minutes , tandis que je rafraichissais le front de Gaby , mon père tenta de la faire revenir à elle :
Gaby , c' est monsieur Personne qui est là .
Gaby , ressaisistoi , ne te laisse pas aller !
en désespoir de cause , il s' écria :
agent Gaby , au rapport !
les paupières de Gaby se soulevèrent avec peine .
... chef ?
chuchota t elle .
oui , c' est moi , petit , et je t' interdis de mourir , tu m' entends ?
elle ferma les yeux d' épuisement et marmotta quelque chose .
que dis tu ?
répète ...
monsieur Personne mit l' oreille tout contre ses lèvres , puis il se redressa .
lentement , il repoussa le drap qui la recouvrait .
on lui avait passé une pauvre chemise .
sa jambe droite n' était qu' un bandage sanguinolent depuis le pied jusqu' au genou .
on ne t' a pas coupé la jambe , Gaby .
une ombre de sourire passa sur son visage .
elle pourrait remettre ses bas de soie .
c' était tout ce qui la tourmentait .
doucement , monsieur Personne remonta le drap .
puis il exigea du laudanum , qu' il fit boire à Gaby pour apaiser la douleur .
enfin , il l' embrassa sur le front :
dors , petit .
je dormis moi même d' un sommeil agité , ne sachant de quel côté tourner mon corps endolori .
aux premières lueurs du jour , je fus éveillé en sursaut par mon père qui tambourinait à ma porte :
tu es convoqué par Gisquet .
Gisquet était le nouveau préfet de police de Paris .
il souhaitait m' interroger dans son bureau , rue de Jérusalem , où mon père me conduisit .
Gisquet n' était pas seul , mais je le reconnus à sa manche de veste relevée et épinglée sur l' épaule .
il avait perdu l' avantbras gauche dans un accident de chasse .
l' autre homme était un militaire .
tous deux étaient assis , mais Gisquet ne nous proposa pas d' en faire autant .
il avait l' habitude de dire que " la police des voleurs était faite par des voleurs " .
donc il nous laissait debout comme des vauriens qu' on s' apprête à chasser .
voilà le héros de jour , ricana t il en me toisant .
racontez votre affaire , et vite , je vous prie .
j' ai autre chose à faire .
moi aussi .
cela fait vingt fois que je répète les mêmes choses .
mon père toussota tandis que Gisquet prenait un air de surprise exagérée .
sans me laisser impressionner , je racontai l' affaire à ma façon .
certains témoins disent avoir entendu l' homme crier " vive Louis le dix septième " .
pouvez vous le confirmer ?
me demanda le préfet .
je n' ai rien entendu , j' étais assourdi par le bruit de la détonation .
nous ne pouvons pas continuer à cacher la vérité , grommela mon père quand nous eûmes quitté l' hôtel de police .
si Gisquet s' en aperçoit , il me renvoie au pré .
( au bagne )
mais si monsieur de Bonnechose est arrêté , c' est la ruine pour Léonie !
écoute , Malo , je t' ai laissé vivre ta vie .
tu aimes qui tu veux .
mais ne vois tu pas le fossé qui sépare une demoiselle de la noblesse d' un fils de voleur ?
si c' est Léonie que tu aimes , et pas le joli nom qu' elle porte et la fortune qui va avec , tu devrais souhaiter sa ruine .
c' est la seule chance que tu aies de pouvoir l' épouser .
le malheur de Léonie ne fera jamais mon bonheur .
c' est ton dernier mot , bougre d' âne ?
oui .
dans ce cas , nous allons faire ce que ne font pas les gens bien .
nous allons nous salir les mains .
mon père m' apprit que Paluchard , repris par ses mauvais instincts , menait une double vie , Louis le dix septième avec Trimbaldière , mais ancien forçat chez Papa Guillotin .
il allait à la taverne pour s' enivrer et danser le chahut avec les filles .
chez Papa Guillotin , Paluchard a fait la connaissance d' un certain Jean Saint Just , me raconta mon père .
nous sommes devenus de grands amis , lui et moi , et il a promis de m' associer au prochain mauvais coup qu' il ferait .
je sais que sa future victime est un banquier qui a chez lui beaucoup d' or et d' argent .
certains soirs où il sort , il n' y a que deux personnes pour garder sa maison .
dès que je saurai où et quand le cambriolage doit avoir lieu , tu préviendras Jacquot et nous tendrons un piège .
quand revoyez vous Paluchard ?
demain midi chez Papa Guillotin .
je vous accompagnerai .
il n' en est pas question .
j' irai seul .
je suis votre lieutenant .
tu attires les catastrophes .
c' était une catastrophe de sauver le roi ?
et Gaby , tu l' as sauvée ?
c' est injuste !
je n' y suis pour rien .
nous reprîmes notre marche en silence .
ce sera dangereux , marmonna enfin mon père .
je me souvins alors de ce que Zina m' avait prédit : " tu mènes une vie dangereuse et tu devras en changer si tu ne veux pas mourir jeune . "
j' ai choisi cette vie , papa .
s' il m' arrivait quelque chose , vous n' y seriez pour rien .