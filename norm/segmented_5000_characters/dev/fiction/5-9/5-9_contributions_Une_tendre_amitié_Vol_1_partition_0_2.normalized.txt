le début d' une amitié Au cours de la soirée , à table , Bérangère raconta à ses parents la rencontre avec Tanguy .
" il est très gentil et poli , tu sais , maman , c' est dommage qu' il ne puisse venir lui même à la maison .
oui , c' est un handicap que les enfants ne devraient jamais connaître , dit le père , les adultes non plus , d' ailleurs !
ajouta t il .
si tu veux , dit la maman , je préparerai un gâteau pour votre gouter de demain .
chic , bonne idée !
répondit de façon quasi incompréhensible Bérangère , tout en mastiquant sa salade .
par contre , renchérit la mère , je préfère t' accompagner pour me rendre compte de l' endroit où tu vas passer l' après midi .
d' accord " , fit Bérangère à mi voix , un peu déçue , mais elle avait suffisamment de maturité pour comprendre que plus qu' un manque de confiance en sa fille , il s' agissait pour la maman d' un principe de sécurité destiné à la rassurer .
le lendemain , les deux enfants , ainsi que Marthe qui ne manqua pas d' y gouter , apprécièrent la saveur de la tarte aux pommes que Bérangère apporta .
les gamins jouèrent à divers jeux de société de lettres et de cartes , l' après midi s' écoulant avec une extrême rapidité .
Tanguy avait laissé tomber , pour l' occasion , ses livres et ses cahiers .
Molière attendrait ...
il essayait d' être gai et de ne pas laisser percevoir sa frustration , mais quelques faits inévitables lui rappelaient sa situation inconfortable .
ainsi , un pion tombant du jeu et roulant sous un meuble , suffisait à l' attrister car il était incapable de pouvoir le ramasser , et n' avait même plus le réflexe de se pencher .
bien que Bérangère ne lui montrait pas qu' elle s' apercevait de la difficulté de son handicap dans un cas comme celui ci , il se doutait qu' elle ne pouvait pas rester totalement indifférente à cet état .
" tu sais , un docteur doit venir m' examiner la semaine prochaine ! " s' exclama t il d' un coup .
la fillette parut toute surprise de cette phrase si spontanée et inattendue , elle sentit que Tanguy avait besoin de justifier sa situation .
sans doute voulait il ainsi lui démontrer que tout espoir de recouvrer l' usage normal de ses jambes n' était pas perdu .
" ah , bien ! fit elle , tu vas entreprendre une rééducation ? je ne sais pas encore , mes premiers efforts n' ont pas été concluants , mais j' espère que cela sera à nouveau possible . je suis encore très jeune , avec le temps et de la patience , j' arriverai peut être à marcher à l' aide de béquilles ou d' un appareil . je l' espère beaucoup pour toi " , affirma t elle .
un quart d' heure plus tard , les enfants se quittèrent , Bérangère devant réviser ses leçons .
" je suis très content de cet après midi , dit Tanguy d' une voix enjouée , quand reviendras tu ? samedi après midi , si tu veux bien ! entendu et merci encore pour le gâteau et les jeux ! "