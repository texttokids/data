Schmucke regarda cette femme , qui ne se doutait pas de sa barbarie , car les gens du peuple sont habitués à subir passivement les plus grandes douleurs morales .
monsieur , il faut du linge pour un linceul , il faut de l' argent pour un lit de sangle , afin de coucher cette dame , il en faut pour acheter de la batterie de cuisine , des plats , des assiettes , des verres , car il va venir un prêtre pour passer la nuit , et cette dame ne trouve absolument rien dans la cuisine .
mais , monsieur , répéta la Sauvage , il me faut cependant du bois , du charbon , pour apprêter le diner , et je ne vois rien !
ce n' est d' ailleurs pas bien étonnant , puisque la Cibot vous fournissait tout ...
mais , ma chère dame , dit madame Cantinet en montrant Schmucke qui gisait aux pieds du mort dans un état d' insensibilité complète , vous ne voulez pas me croire , il ne répond à rien .
eh bien !
ma petite , dit la Sauvage , je vais vous montrer comment l' on fait dans ces cas là .