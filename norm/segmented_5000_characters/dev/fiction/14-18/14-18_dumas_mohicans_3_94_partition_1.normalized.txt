et le jeune homme , d' un mouvement rapide , saisit la main dont Mina tenait le couteau .
en m' arrachant cette arme ?
dit Mina .
eh bien !
mais cette arme n' est qu' un moyen de mort , ce moyen ôté , il m' en restera dix autres .
n' y a t il pas l' étang qui est en face du château ?
ne serai je pas toujours libre de monter au second étage et de me jeter par la fenêtre sur les dalles du perron ?
oh !
mon honneur est bien gardé , je vous jure , car il est sous la garde de la mort .
mina , vous ne ferez pas ce que vous dites !
aussi vrai que je vous hais , aussi vrai que je vous déteste , aussi vrai que je vous méprise , aussi vrai que j' aime Justin , aussi vrai que je n' aimerai jamais que lui , je me tuerai , monsieur , au jour , à l' heure , à la minute où je ne serai plus digne de reparaitre devant lui !
après cela , vous êtes libre de me garder ici tant qu' il vous plaira .
soit !
dit le jeune homme , dont Salvator entendit les dents grincer les unes contre les autres , nous verrons qui se lassera le premier .
ce sera , à coup sûr , celui avec lequel Dieu n' est pas , répondit la jeune fille .
dieu !

murmura le jeune homme .
dieu !
toujours Dieu !
oui , je sais qu' il y a des gens qui n' y croient pas ou qui font semblant de ne pas y croire , à Dieu , et , si vous aviez le malheur d' être un de ces hommes là , monsieur , je vous dirais : " à ce rayon de lune qui nous éclaire tous deux , regardez moi , moi l' opprimée , moi la prisonnière , moi l' esclave , eh bien ! c' est moi qui suis calme et croyante , et c' est vous qui êtes plein de doute et de colère . il y a donc un Dieu , puisque ce Dieu permet que je sois tranquille et que vous soyez agité . "
mina , dit le jeune homme en se jetant à ses genoux , vous avez raison , il faut croire au Dieu qui vous a faite .
il ne me manque qu' une chose pour y croire , c' est votre amour .
aimez moi et j' y croirai .