gilles vit dans un appartement avec sa maman , son papa et son petit frère Paul .
il va à l' école où il aime jouer au foot avec ses copains .
il adore les pâtes à la sauce tomate mais déteste les brocolis .
il préfère regarder la télé plutôt que d' apprendre ses leçons .
bref , c' est un petit garçon de huit ans comme les autres , me direz vous .
mais ...
ne vous fiez pas aux apparences !
car si vous entrez dans la chambre de Gilles , vous serez émerveillés .
les murs sont couverts de posters de films d' extraterrestres ou de super héros .
ses étagères regorgent de livres sur les dinosaures , de figurines de robots et de monstres en tout genre .
mais la magie ne s' arrête pas là .
lorsque Gilles s' endort dans son petit lit en forme de soucoupe volante , ses rêves l' emmènent très loin .
dans des mondes inconnus , au fin fond du cosmos , pour combattre d' infâmes extraterrestres ou à des époques reculées , quand les dinosaures régnaient en maîtres sur la Terre .
parfois même , il a l' impression que son lit s' envole , pour de vrai !
à quoi rêve t il ce soir ?
allons voir !
sa soucoupe vient de se poser au milieu des arbres touffus d' une jungle très humide .
il fait nuit mais , en parfait aventurier , Gilles décide d' aller explorer les alentours .
gilles frissonne .
il a un peu froid .
ce n' est rien en comparaison de la peur qui le saisit lorsqu' il aperçoit , à quelques mètres de lui , un énorme tyrannosaure .
gilles recule en faisant le moins de bruit possible mais " patatras ! " il n' a pas vu la grosse pierre derrière lui et il s' étale de tout son long .
le tyrannosaure l' a entendu et il se lance à sa poursuite , tous crocs dehors , pour le dévorer !
la terre tremble sous les pieds du garçonnet qui court aussi vite qu' il peut pour échapper à son poursuivant .
il sent le souffle chaud du tyrannosaure contre ses mollets quand tout à coup , quelque chose l' agrippe par le bas de son pyjama et il s' envole , tête en bas , très haut dans le ciel .
" ouf ! sauvé ! " se dit il .
au bout d' un moment , la grosse bête volante le repose à terre , juste à côté de sa soucoupe .
gilles se retrouve , face à face , avec un autre dinosaure , un superbe ptérosaure .
" merci ! " dit Gilles avant que l' animal ne s' envole à nouveau .
" bon ... allez ! j' ai eu assez d' émotions pour aujourd'hui , je préfère rentrer à la maison ! " dit Gilles en grimpant dans sa soucoupe volante .
il met les moteurs en marche et il décolle .
les moteurs se mettent à faire un drôle de bruit " bip , bip , bip , bip ! bip , bip , bip , bip ... "
ce ne sont pas les moteurs de la soucoupe , mais le réveil matin de Gilles qui est en train de sonner !
il s' étire , baille et ouvre doucement les yeux .
sa maman entre dans la chambre , éteint le réveil et lui fait un gros bisou sur le front .
" bonjour mon ange . as tu bien dormi ? " " oh oui maman ! j' ai rêvé que j' étais poursuivi par un énorme tyrannosaure ! " sa maman sourit .
" quelle imagination mon fils ! allez , les fiers explorateurs , ont besoin de prendre des forces ! habille toi et viens prendre ton petit déjeuner ! "
gilles enlève son haut de pyjama mais quand il entreprend d' enlever le bas , il se rend compte qu' il a deux énormes trous au niveau des fesses !
comment cela est il possible ?
gilles sourit .
peut être n' a t il pas rêvé , au fond .
il s' habille puis il s' applique à bien plier son pyjama pour cacher les trous et il le range sur son lit .
" maman ne me croira jamais si je lui dit que j' ai troué mon pyjama avec les griffes d' un ptérosaure ! " quand je vous disais que Gilles n' est pas un petit garçon comme les autres !