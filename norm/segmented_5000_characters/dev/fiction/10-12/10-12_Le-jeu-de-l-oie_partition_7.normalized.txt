et puis , je suis allé à la cuisine .
je me suis assis sur un tabouret , devant la fenêtre , et j' ai attendu .
la température a baissé , degré par degré .
pas dans l' appartement , pas à l' extérieur : à l' intérieur de moi .
et bizarrement , je me suis senti bien .
j' ai compris : je n' avais plus peur .
je me suis rappelé : dans le jeu de l' oie , il y a une case que Michèle avait oublié de dessiner : la case cinquante huit , la case de la mort .
" qui ira à cinquante huit , où est une tête de mort , recommencera tout le jeu " .
sans savoir , je l' avais passée , cette case , et plus jamais je ne reviendrai en arrière .
dans ma chambre , la porte du placard a grincé .
bruits de tissus agités , de vêtements froissés .
j' ai vu Michèle passer devant la cuisine , sans s' arrêter .
un peu plus tard , j' ai entendu qu' elle farfouillait au salon .
sans bruit , je me suis levé et suis allé me poster dans l' entrée .
elle s' est figée quand elle m' a vu .
elle avait enfilé sa veste de cuir et tenait son sac à main par la lanière .
je l' ai fixée un moment .
c' est elle qui a détourné les yeux .
tu as oublié quelque chose , ai je dit .
ah oui , quoi ?
je suis allé chercher le jeu de l' oie au salon .
je l' ai replié à la hâte , j' ai jeté les dés et les figurines dans la boite et tassé tant bien que mal le couvercle par dessus .
et puis je lui ai fourré le tout dans les bras .
voilà !
ai je dit .
tu peux partir maintenant .
elle a poussé la porte , hésité un instant , et puis elle est sortie .
avant que la porte se ferme , je n' ai pas pu m' empêcher de demander :
tout à l' heure , c' était vrai ?
tu as réellement vu mon père ?
elle a ouvert la bouche , ses lèvres ont tremblé .
mais avant qu' elle ait le temps de répondre , j' ai claqué la porte à toute force .
non , je n' avais pas besoin de la réponse .
dix
voilà , c' était fini .
j' ai ramassé les feuilles de papier égarées dans tout l' appartement , j' ai rangé les chaises de la salle à manger , mis de l' ordre au salon .
pas de traces , surtout , pas de traces .
quand j' ai eu terminé , j' ai ouvert la fenêtre de ma chambre , laissant l' air frais entrer et effacer tout souvenir de ce qui s' était passé .
il ne s' était rien passé .
oui , tout à coup , j' avais l' impression d' avoir seulement rêvé .
j' ai sorti mon livre de math et j' ai commencé un exercice noté sur mon agenda pour le lundi suivant .
maman est rentrée tard , à dix heures passées .
je ne dormais pas .
Etendu dans le noir sur mon lit , je rêvassais vaguement , entre éveil et sommeil .
quand j' ai entendu la clé tourner dans la porte , je me suis levé aussitôt pour la rejoindre .
tu n' es pas couché ?
a t elle demandé , inquiète .
comment ça s' est passé ?
je me suis couché à neuf heures , mais je n' arrive pas à dormir .
et ça s' est très bien passé .
très , très bien .
je crois même qu' elle ne reviendra plus .
elle a voulu en savoir plus , mais je n' ai rien expliqué .
plus tard , ai je dit .
et toi , comment ça va ?
bien .
un peu fatiguée , mais ça va ...
je suis retourné me coucher .
elle m' a suivi , s' est assise au bord du lit .
dans la pénombre de la chambre , juste éclairée par la lumière du couloir , j' ai remarqué une lueur dans ses yeux et , sur son visage , une sorte de douceur inhabituelle .
elle a senti mon regard et a souri .
il faut que je te dise ...
Adrian vient manger demain soir .
tu te souviens de lui ?
oui , bien sûr ...
Adrian est médecin à l' hôpital où travaille maman .
il est roumain .
j' aime bien son accent , sa façon de prononcer les " R " , et la mélodie qu' il donne aux phrases .
il dormira ici ?
ai je demandé .
je suis sûr qu' elle a rougi , mais j' ai détourné les yeux pour ne pas voir .
j' étais content pour elle et je l' ai dit .
oui , j' ai dit : " je suis content pour toi . " ce n' était pas facile , j' ai dû arracher les mots coincés dans ma gorge , mais j' ai réussi .
elle a serré ma main .
et puis elle s' est penchée vers moi , elle a frotté son nez au mien et m' a embrassé sur le menton , sur le front et , léger , très léger , sur les lèvres .
comme on faisait , autrefois , quand j' étais un petit garçon .
il n' y a pas si longtemps , en fait .
j' ai compris que c' était la dernière fois .
c' est bien .
c' est très bien comme ça .