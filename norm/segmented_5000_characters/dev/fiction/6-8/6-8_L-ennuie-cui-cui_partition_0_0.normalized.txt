il n' y a rien de plus ennuyeux que de s' ennuyer , rien de plus embêtant que de s' embêter .
Hector en sait quelque chose , il est champion du monde de l' ennui : il sait tourner en rond , faire les cent pas , fixer le vide , s' asseoir sur toutes les chaises à tour de rôle , il fait éclater chaque bulle dans le papier bulle que sa mère n' a pas jeté , manger un gâteau , puis un autre gâteau , ( et puis encore un autre !
) , ouvrir la porte du frigo , la fermer , regarder l' écran de la télé sans vouloir l' allumer , même chose pour l' ordinateur , et aussi les jeux sur le portable que son père a laisser sur la table .
il ne veut rien et en même temps il attend que quelque chose lui arrive .
dimanche est le pire .
Hector guette un mouvement éventuel de la chambre de ses parents , mais ils dorment comme des morts .
sa grande soeur est éternellement dans la salle de bains .
il est pitoyablement seul .
il a l' impression d' être dans une cage .
" tiens ! " pense t il dans un éclair , " je pourrais préparer le petit déjeuner . " il vérifie le stock dans sa tirelire et sort toute sa maigre fortune .
il doit avoir assez pour acheter quelques croissants .
Hector est tout imbue d' une grande mission .
sur la pointe des pieds il sort délicatement et descend les six étages , direction boulangerie juste en bas de son immeuble .
devant la vitrine des croissants , il calcule qu' il a assez pour un seul .
ça ne suffit pas pour quatre personnes .
il achète alors une baguette .
il mange tout de suite le bout .
entre le rez de chaussée et le premier étage , il croque l' autre bout .
le long de l' escalier , par une magie mystérieuse , toute la sainte baguette disparait .
il perd l' envie de préparer le petit déjeuner .
dans sa maison , il décide de compter ses pas jusqu' au réveil de la famille .
Hector sait compter jusqu'à l' infini , mais il s' ennuie fort autour de vingt neuf cent dix huit pas .
il se rappelle de l' idée ( ennuyeuse ) de sa mère de le trainer dans un musée ( ennuyeux ) devant des rectangles sur les murs .
les gardiens des salles sont là comme des fantômes .
" ils doivent mourir d' ennui , " dit Hector à sa mère .
" mais non , " dit elle , " ils regardent les tableaux . "
Hector s' imagine roi sur son trône .
est ce que les rois s' ennuient ?
il s' assoit comme un roi sur le fauteuil du salon et il attend de voir s' il s' ennuie .
et bien oui !
l' ennui royal dans son royaume .
est ce qu' un peut vraiment mourir d' ennui ?
mourir , vivre .
Hector pense aux verbes et aux conjugaisons .
ce qui lui vient à l' esprit c' est pleurer , regarder , nager , rêver , construire , dessiner , rire , sauter , vendre , voler , danser , chanter , manger , dormir , écrire , écouter , téléphoner ...
lire .
son ami Sacha qui sait tout sur tout lui a dit qu' il y a douze mille verbes dans la langue française .
dans tout ça il devrait pouvoir trouver quelque chose à faire , n' est ce pas ?
le verbe exprime une action , n' est ce pas ?
mais il faut vouloir avant de pouvoir .
il est presque prêt à se jeter par la fenêtre quand sa mère arrive dans la cuisine en se frottant les yeux et active la machine à café .
" tu as assez dormi , Maman ? " dit Hector avec ironie .
jamais assez !
et toi ?
je n' ai rien à faire et je m' ennuie .
je vous attends pour manger depuis un siècle .
et bien , mets la table .
papa se rase .
sa soeur Héloïse arrive et s' assoit lourdement en boudant .
c' est son état normal , elle boude beaucoup .
sa mère explique ça par le fait qu' elle est une a do le scente .
Hector se demande si c' est une maladie .
il est jaloux parce qu' elle ne s' ennuie jamais .
elle passe des heures à se regarder dans le miroir ou à envoyer et à répondre à des SMS .
son meilleur ami est son téléphone portable .
il est toujours dans sa main .
Hector ne s' ennuie pas pendant le petit déj .
sa mère a fait du pain perdu .
il adore ...
avec du sirop d' érable .
il aide même à ranger la vaisselle dans la machine .
tu as des devoirs ?
non .
je n' ai rien à faire .
qu' est ce que je peux faire ?
tu peux appeler Paul .
pas envie .
tu peux lire !
bof !
tu peux te promener .
veux pas .
tu peux ranger ta chambre !
laisse tomber !
Hector aurait envie de partir à la mer ou aller au ski ou assister à un spectacle de cirque ou monter sur un cheval ou escalader une montagne ou partir à l' autre bout du monde en Australie ou marcher sur Mars , participer à un safari en Afrique du sud , monter en haut d' un gratte ciel à New York , nager avec les dauphins , flotter dans la mer morte , dormir dans un igloo , voir Venise d' un gondole , vivre dans une cabine dans un arbre , aller à Disneyland , traverser le désert à dos d' un chameau , faire voler un cerf volant , trouver la sortie d' un labyrinthe , jouer avec les vagues sur une planche , compter les étoiles dans un hamac , camper dans la nature , assister à un match , rouler dans l' herbe en bas d' une colline , construire un château dans la sable , adopter un chien .