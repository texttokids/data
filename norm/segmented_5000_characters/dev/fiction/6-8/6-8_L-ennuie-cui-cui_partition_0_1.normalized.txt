mais au lieu de tout ça ils vont déjeuner chez Tante Gertrude .
tante Gertrude vit dans une maison sombre où il y a tant de meubles qu' elle est obligée d' en mettre devant les fenêtres .
tante Gertrude n' a pas d' enfants , n' aime pas les enfants , ne comprend pas les enfants , est raciste contre les enfants .
elle pense qu' ils doivent rester à table sans parler jusqu'à leurs trente cinq ans .
personne n' aime y aller , mais Papa est le seul neveu de Tante Gertrude , la soeur de sa mère morte .
elle a quatre vingt dix ans et toute sa tête tordue .
elle vit seule , fait ses courses et sa cuisine révoltante qui consiste en de légumes bouillis et de compote .
on joue aux statues , lui chuchote Héloïse dans un rare moment de complicité .
on n' a pas le choix !
leur mère les regarde en les suppliant silencieusement de se comporter selon les règles de cette maison sinistre .
c' est seulement quand les poireaux flagada et le poisson poison et la compote de rutabaga sont difficilement avalés que tante Gertrude proclame dans sa voix autoritaire : " les enfants vous pouvez quitter la table . allez jouer au jardin . " elle les chasse avec un mouvement de la main comme on balaye des mouches .
Hector sait que maintenant il y aura enfin une conversation intéressante avec d' éventuels secrets de famille .
il reste dans les parages avec sa soeur pour écouter l' annonce : " j' ai fait les démarches pour vous laisser la maison , mon seul bien . "
c' est comme une flèche au coeur d' Hector .
est ce que cela veut dire que le jour où tante Gertrude décide de ne plus vivre , ils seront obligés de déménager dans cette horreur hantée ?
lui , il refuse !
c' est trop loin de son école , de son quartier , c' est dans une autre ville !
quel est son soulagement quand l' heure arrive enfin de partir et quel est son soulagement quand ses parents discutent de l' héritage qu' ils vont soit vendre , soit louer .
et quel est son soulagement à la fin de cette journée désespérante de retrouver son petit lit dans sa chambre , dans son appartement au sixième étage , dans son immeuble , dans son quartier et même à l' idée d' aller à l' école le lendemain .
l' école est meilleure que Tante Gertrude .
une fois à l' école , Hector se rend compte que s' il s' ennuie moins , il s' ennuie tout de même .
à peine installé à son bureau , il a faim et en plus il a envie de dormir , mais aussi de bouger .
il obéit en ouvrant le livre à la page soixante dix huit , et puis il décroche parce que c' est pénible d' écouter les vingt huit élèves lire la même chose vingt sept fois .
il s' évade de la monotonie en regardant par la fenêtre et en imaginant qu' il est un oiseau qui survol la surface de la terre .
est ce que les oiseaux s' ennuient ?
à son tour , Hector est quelque part au Pôle nord ou sud mais pas dans la classe .
la maîtresse qui lui pose une question à laquelle il ne répond pas l' envoie dans le couloir .
le principal passe par là et l' amène attendre dans son bureau .
attendre c' est une des pires formes d' ennui .
Hector sait que beaucoup de génies se sont ennuyés à l' école : Albert Einstein , Mark Twain , Oscar Wilde , Winston Churchill , Thomas Edison , Benjamin Franklin , Montaigne .
il a vu des dessins animés sur eux .
" je dois être un génie alors ! " pense t il en baillant .
le principal renvoie Hector dans sa classe avec " essaie de te concentrer " et sans punition .
Hector s' est bien concentré sur ses billes pendant la récréation .
il a gagné .
il s' est bien concentré aussi sur les macaronis à la cantine , il a tout fini .
il se concentre sur la nouvelle élève rousse qui s' appelle Abigaël .
il se concentre sur l' horloge accrochée au mur qui ne veut pas avancer plus vite .
il se concentre à faire sauter sa gomme , sur ses gribouillages , sur le bout de crayon qu' il mord .
ses oreilles se concentrent à aider à faire venir la sonnerie .
puis il se concentre pour quitter l' école et rentrer à la maison .
avant de partir , la maîtresse qui les quitte toujours en les laissant une pensée : " vous avez vécu huit ans dans cette vie qui est unique que l' on ne vit qu' une seule fois . "
d' une façon bizarre , ces mots retentissent dans son cerveau comme une cloche persistante : " cette vie ... une seule fois ... vie qui est unique ... on ne vit qu' une seule fois ... et le devoir : " pour demain faites un portrait de votre famille .

étrangement Hector aime cette idée .
sa famille : sa mère , son père , sa soeur .
mais il ne sait pas quoi écrire .
il décrit son père qui déteste se raser , qui court le samedi matin , qui se dévoue à son ordinateur .
qu' est ce qu' il en sait de plus ?
pas grande chose .
sa mère non plus .
à table il les bombarde de questions qui viennent subitement à l' esprit : où êtes vous nés ?
comment vous vous êtes rencontrés ?
quel est le rêve de votre vie ?
comment étaient vos parents ?
qu' est ce que vous pensez de la France ?
est ce que vous vous êtes ennuyés dans votre enfance ?
je ne connaissais pas l' ennui , dit son père .
on faisait des jeux avec rien : on construisait une tente avec un drap et campait sous la table de la salle à manger .