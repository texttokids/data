en latin de cuisine , le plus gouleyant et le mieux en rapport avec cette affaire restauratrice , on imagine assez bien la mâle consigne , non donnée , affirme t il , par le proconsul Bernard Bonnet aux centurions en ceinturon qui , eux , certifient l' avoir entendue : " Delenda est paillota ! " car ainsi va le ridicule du temps que la justice en est à se pencher sur ce cas pendable d' une République humiliée par les agissements incendiaires et noctambules de ses présumés serviteurs .
pour qui l' ignorerait encore , ce dont il faut douter , tant ce feu de paillote fit du bruit et de la fumée , résumons l' affaire à ses protagonistes principaux : à main gauche , un plagiste adepte de la restauration sauvage , et néanmoins coutumière , homards grillés , nuits étoilées , clientèle d' habitués , sous le barbecue , la plage en somme .
à main droite , un préfet de la République , nommé là et choisi pour sa capacité à être , citons le ministre de l' intérieur de l' époque , Jean Pierre Chevènement , " l' homme qu' il faut là où il faut " .
un préfet , arrivé , et là le mauvais comique allait succéder au tragique , pour assurer la relève de son prédécesseur , Claude Érignac tombé sous les balles des tueurs .
et donc , avec ferme consigne et grand souci de restaurer l' Etat de droit .
entre la restauration sauvage Chez Francis et la restauration , tout aussi sauvage , de l' Etat de droit , on sait ce qu' il advint de cette lutte jusqu'à l' incendie final .
un commando de gendarmes pyromanes s' en fut une nuit , sur ordre du préfet , mais le préfet le nie , mettre le feu à la cambuse illégale du sieur Féraud .
étant entendu , pour mémoire , que le sieur Féraud s' était vu intimer , par la justice , l' ordre de détruire sa guinguette des plages au motif que construite illégalement sur le domaine public maritime inaliénable , le fameux DPM .
décision de justice dont le restaurateur insulaire , brave garçon , mais têtu , se fit , si l' on ose dire , un chapeau de paille .
voilà l' affaire ainsi résumée .
à sa plus simple expression du ridicule lamentable .
et voici le procès , commencé hier à Ajaccio , et s' annonçant , à lire le compte rendu de cette première audience ( page treize ) absolument dans la continuité du chapitre précédent .
le préfet Bernard Bonnet , devenu un auteur prolixe , un héros pour certains , une victime du devoir pour d' autres , en tout cas le préfet le plus médiatique de tous les préfets présumés incendiaires , reste sûr de lui .
et sûr de son bon ( état de ) droit .
il dépose une gerbe , avant l' audience , à l' endroit où le préfet Érignac fut assassiné .
il dédicace ses oeuvres , pendant les suspensions d' audience .
il fait son vingt heures de France deux , dans la modeste échoppe d' un cordonnier .
et à la barre , au tribunal , il tient tribune .
souriant , volubile , séducteur , plaideur à en être tribun .
le préfet Bonnet en arrive même à caricaturer le préfet Bonnet dans une fumeuse envolée , du moins fumeuse pour nous , sur " les remugles de barbouzerie qui ne sont pas absents de cette affaire de paillotes " .
sur le supplément d' information qu' il souhaite " d' autant que des liens entre barbouzes modestes , locales et barbouzes , supérieures à Matignon , ne sont pas à exclure " .
diable , barbouze en feu !
le procès s' annonce chaud .