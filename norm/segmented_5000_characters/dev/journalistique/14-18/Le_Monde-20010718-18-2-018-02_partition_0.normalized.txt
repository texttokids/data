l' étape pontarlier aix les bains n' y a rien changé .
quatrième du classement général , le Kazakh Andreï Kivilev ( Cofidis ) restait , avec François Simon ( Bonjour ) l' un des plus sérieux prétendants au maillot jaune , mardi dix sept juillet à l' Alpe d' Huez .
accusant un retard de dix sept minutes trente cinq secondes sur François Simon , Andreï Kivilev , bon grimpeur , compte plus de douze minutes d' avance sur les grands prétendants au podium final à Paris .
" au départ de ce Tour , nous pensions qu' il entrerait dans les dix premiers , maintenant il peut faire dans les cinq " , assure Bernard Quilfen , l' un de ses directeurs sportifs .
âgé de vingt sept ans , ce coureur a débarqué à Saint Étienne en dix neuf cent quatre vingt dix huit chez les amateurs .
il a ensuite rejoint Festina , où il n' a pas été touché par les affaires de dopage .
après un passage en vingt cent zéro chez AG deux R Prévoyance , il a , en vingt cent un , rallié Cofidis , chez qui il a signé son premier succès professionnel , en s' adjugeant une étape du Critérium du Dauphiné libéré .
il a ensuite emporté la vingt cinquième édition de la Route du sud .