dans son logement troglodyte , Jean Luc Courcoult vit entouré de ses CD et de sa tireuse de photos numériques .
il accroche au mur les portraits qu' il prend des villageois .
ce " sage au caractère difficile " , selon la définition du chef du village , aime discuter théâtre .
il a demandé à Anne Marie Vennel , la nouvelle arrivée , de se joindre à nous .
elle a travaillé avec Farid Paya , Carmelo Bene , Philippe Adrien , et caetera C' est sa première expérience de théâtre en extérieur .
" être en permanence dehors n' est pas si facile , avoue t elle , après un mois de vie dans le village . comment retrouver ce temps d' intimité qu' on a dans les théâtres ? on crée en même temps qu' on découvre la Chine , le tout dans un contexte qui nous ébranle . j' ai le sentiment d' être acteur d' une grande aventure plus que d' un rôle . à partir du travail d' atelier , Jean Luc va construire , projeter , mettre en forme . c' est aussi dans la confrontation immédiate avec le public que se modèlent ces mythologies chinoises de son invention . on pressent que rien ne sera impossible . " jean luc Courcoult écoute .
il n' est pas né de la dernière pluie .
il y a plus de vingt ans , à Aix en Provence , il a fait ses trois ans d' études théâtrales en trois mois .
rien de ce qu' il imaginait n' étant dans les livres , seul , il décide de son destin .
il affronte la rue , fait la manche pour survivre .
vingt ans après , ses créations voyagent dans le monde entier .
royal de Luxe est un label de la culture française à l' export .
" accessible À tous " " qu' on appelle mon théâtre comme on veut ! mais c' est avant tout du théâtre populaire . accessible à tous . c' est un travail de longue haleine pendant lequel les participants doivent accepter sans se bloquer leur transformation intérieure . c' est ça qui se joue . je choisis des êtres pour leur capacité d' ouverture . on est jamais le même avant et après un spectacle . je n' aime pas l' acteur qui fait semblant d' exister , chez lequel on sent la psychologie , la phrase . le public , c' est lui qui dit qui on est " comment réinventer le monde ?
en partant de soi , de ses connaissances vacillantes , de ses faiblesses .
c' est ainsi qu' il réécrit en mille neuf cent quatre vingt dix l' Histoire de France ( La Véritable Histoire de France ) , et en mille neuf cent quatre vingt dix sept " son " Afrique ( Les Petits Contes nègres ) .
aujourd'hui , " sa " chine commence à prendre forme .
ce travail d' invention ne saurait se développer à Nantes , ville d' accueil de Royal de Luxe .
il faut à ce drogué de l' affectif un bain d' humanité qui le mette en péril .
" à l' étranger , dans une autre culture , il n' y a pas de temps à perdre . dans l' urgence , les choses sortent . qu' on ne me bassine pas avec l' histoire du collectif théâtral . c' est un collectif , certes , presque un phalanstère , mais qui ne remet pas en question mes décisions . " on s' en était aperçu .
être là où il faut , au bon moment , c' est là sa force , au moment où le gouvernement lance une opération nouvelle frontière en direction de l' intérieur , comprenant qu' il ne pourra longtemps laisser se creuser l' écart entre les villes côtières et la Chine rurale .
là , dans cette région où les artistes chinois partent à la recherche de leur culture dans le but de sauver ce qui en subsiste .
telle la chanteuse pékinoise Yang Yi collectant les vieilles chansons country auprès du paysan Li Shengcai , grande voix du Shaanxi ( Daily China du trente avril deux mille un) .
" des veines fertiles " la première confrontation avec le public de Jiao Kou a boosté le moral du chef de Royal de Luxe et désarçonné Anne Marie Vennel : " on vit des moments fous , dit elle . on se concentre , et puis on regarde le paysage . un paysan qui porte son mouton emporte notre esprit . le bruit , la chaleur , tout m' a déstabilisée . c' est un rapport autre à soi même qu' il faut trouver . il faut oublier l' extérieur , mais aussi c' est lui qui nous fait respirer large . il faut accepter de s' y perdre . " " nous n' en sommes encore qu' au début , reprend Jean Luc Courcoult . c' est cent idées qu' il nous faut trouver . des veines fertiles . peut être cette aventure ira t elle jusqu' en deux mille quatre , année de la France en Chine . qu' on pourra construire notre dragon géant , mettre en mouvement une folle parade . être ainsi dans un flot continu pour plusieurs années . la seule stratégie que je connaisse est l' efficacité . " et la gratuité des spectacles .
fin juin , Royal de Luxe pliera bagage pour retourner à Nantes .
la première de ce spectacle conçu en Chine aura lieu en septembre à Paris , à La Villette .