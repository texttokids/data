jeudi quinze février deux mille dix huitJeudi quinze février deux mille dix huit
jusqu' au mois de janvier , la Chine recevait une grande partie des déchets plastiques du monde entier .
mais c' est fini , et les autres pays sont bien embêtés .
depuis mille neuf cent quatre vingt quatorze , un tunnel sous la mer relie en trente minutes la ville de Douvres ( Angleterre ) à celle de Calai ( France ) .
mais l' idée de construire un pont de trente quatre kilomètres entre les deux pays est à nouveau d' actualité .
la Chine ne veut plus être une poubelle
un pont entre l' Angleterre et la France ?
l' Île plastique
le pont Haiwan en Chine mesure quarante et un , cinq kilomètres de long .
il est un des plus longs ponts au dessus de la mer du monde .
deux siècles de projets
personne ne vit sur cette île .
les quatre plus longs ponts au dessus de la mer se situent :
en Chine où le pont Haiwan ( quarante et un , cinq kilomètres ) relie la ville de Qingdi à l' île de Huangdao .
aux États Unis avec la chaussée du lac Pontchartrain ( trente huit , trois kilomètres ) .
elle traverse le lac Pontchartrain dans le sud de la Louisiane .
aux États Unis encore avec le pont du marais de Manchac ( trente six , sept kilomètres ) qui traverse le marai de Manchac , en Louisiane .
en Chine avec le pont de la Baie d' Hangzhou ( trente cinq , sept kilomètres ) qui relie Shanghai et Ningbo .
changement d' heure Vers la fin de l' heure d' été et d' hiver ?
depuis deux mille treize , la guerre déchire le Soudan du Sud , pays de l' est de l' Afrique .
les groupes armés qui s' y opposent n' hésitent pas à recruter de jeunes enfants pour en faire des soldats .
selon l' Organisation des Nations unies , qui est chargée de protéger la paix dans le monde , dix neuf enfants sont concernés .
Dorrefour à la vie .
l' Organisation négocie avec les groupes armés pour libérer ces enfants soldats .
la semaine dernière , plus de trois cents d' entre eux , dont quatre vingt sept filles ont ainsi été relâchés .
certains pourront retourner dans leurs familles , d' autres iront dans des centres d' accueil .
le retour à la vie normale est souvent très difficile pour ces enfants qui ont vécu des expériences très violentes .
l' Onu a besoin d' argent pour les aider à reprendre une vraie vie d' enfant , les aider à retourner à l' école et à oublier la guerre .
reçoivent des soins médicaux pour soigner leur corps , mais aussi les blessures qu' ils ont dans le coeur .
depuis mille neuf cent quatre vingt dix huit , la date du changement d' heure est la même dans tous les pays de l' Union européenne .
deux fois par an , il faut la changer le dernier dimanche de mars ( on avance d' L heure ) et le dernier dimanche d' octobre ( on recule d' un heure ) .
vers un abandon ?
les député ( qui votent les lois du Parlement ) , européen ont voté une résolution pour mettre fin au changement d' heure dans l' Union européenne .
des études ont prouvé que cela avait un effet négatif sur la santé .
maintenant , il faut convaincre les vingt huit États membres de la Commission européenne de faire appliquer cette décision .
en attendant , dans la nuit du samedi vingt quatre au dimanche vingt cinq mars , il faudra encore régler les montres .
pourquoi ?
parce que le Soleil n' est pas présent au même moment en été et en hiver .
en profitant plus longtemps de sa lumière , on fait des économies d' énergie .