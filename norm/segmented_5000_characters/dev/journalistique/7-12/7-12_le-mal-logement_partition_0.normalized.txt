numéro quatre vingt onze un au sept février vingt cent dix neuf
le mal logement
avoir un espace à soi pour faire ses devoirs ou pour jouer est très important .
c' est la même chose pour les plus grands .
pourtant , près de quatre millions de personnes sont aujourd'hui mal logées en France .
ça veut dire qu' elles n' ont pas de logement du tout ou vivent dans une habitation en très mauvais état ou beaucoup trop petite par exemple .
je t' explique tout dans ce nouveau numéro .
l' immeuble d' Inès et d' Adam menaçait de s' effondrer
dans l' appartement d' Inès , quatre ans , et Adam , deux ans , il y a des dizaines de photos de famille sur les murs .
des coussins moelleux sont disposés sur le canapé , et les enfants regardent des dessins animés à la télé .
rien d' anormal !
enfin presque ...
cet appartement situé à Coulommiers , une ville à l' est de Paris , n' est pas vraiment le leur .
ils y ont emménagé en catastrophe avec leurs parents et leurs deux chats , la veille de Noël .
l' immeuble où ils vivaient juste avant menaçait de s' effondrer ...
la famille était dans une situation de mal logement .
" il y a deux ans et demi , des fissures ont commencé à apparaître sur les murs de notre immeuble . petit à petit , elles ont grossi . à la fin , on pouvait passer nos bras à l' intérieur ! " raconte Jennifer , la maman d' Inès et Adam .
à cause de ces trous , il faisait toujours très froid et humide chez eux .
Adam tombait souvent malade : " on l' emmenait trois fois par mois chez le pédiatre " , affirme Djiad , son papa .
Inès aimait beaucoup sa chambre " violette et blanche avec des rideaux roses " .
mais elle se souvient des murs qui n' arrêtaient pas de faire " crac " .
ça lui faisait très peur .
" la maison allait tomber sur nous ... " murmure t elle en se blottissant sur le canapé .
pour protéger leurs enfants , Jennifer et Djiad ont fini par les faire dormir chez leur grand mère .
les habitants de l' immeuble ont demandé des dizaines , voire peut être des centaines de fois aux propriétaires de faire des travaux , sans succès .
cet automne , Jennifer a appelé l' association Droit au logement , qui aide les personnes dans ce genre de situation .
une bénévole a alors tout mis en oeuvre pour les sortir de là .
la maire de Coulommiers leur a trouvé un appartement de secours , dans lequel ils peuvent vivre gratuitement en attendant de trouver une solution .
même si leur combat pour trouver un nouveau logement est loin d' être terminé , toute la famille est déjà un peu soulagée .
Inès la première : " maintenant la nuit , on a bien chaud " , se réjouit elle .
Coulommiers , la ville où habite la famille