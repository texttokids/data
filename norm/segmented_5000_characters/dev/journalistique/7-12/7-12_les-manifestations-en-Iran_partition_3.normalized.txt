comment fonctionne ce pays ?
l' Iran est une République islamique .
la personne qui a le plus de pouvoir est un homme religieux .
on l' appelle le Guide suprême et l' actuel est Ali Khamenei .
il est élu à vie par d' autres religieux .
" il représente la supériorité de la religion sur le politique " , explique le chercheur Thierry Coville .
le Guide suprême contrôle l' armée , la justice , la radio et la télévision publiques et une partie de l' économie .
" le Guide contrôle tout mais ne décide pas forcément seul " , précise la sociologue Azadeh Kian .
en dessous de lui , il y a le président , Hassan Rohani , qui est un peu l' équivalent du Premier ministre en France .
il est élu par tous les citoyens pour quatre ans , tout comme les députés de l' Assemblée , qui votent les lois .
mais pour devenir député ou président , il faut d' abord que sa candidature soit retenue par le Conseil des gardiens , qui est composé de religieux .
" on vous laisse aller voter librement mais , en filtrant les candidats , on vous dit en quelque sorte pour qui aller voter " , remarque la sociologue .
la République islamique a été créée en mille neuf cent soixante dix neuf .
avant ça c' était un dictateur , le chah ( qui veut dire roi ) d' Iran , qui dirigeait le pays .
les Iraniens ont réussi à le chasser lors d' une révolution , et un homme , l' ayatollah Khomeiny , a pris le pouvoir .
" il disait qu' il voulait une démocratie mais il nous a menti " , se rappelle Azadeh Kian , qui a grandi dans ce pays , qu' elle a quitté il y a trente huit ans .
depuis mille neuf cent soixante dix neuf , les Iraniens doivent respecter des principes religieux pour ne pas être punis .
les femmes doivent par exemple se couvrir la tête avec un voile .
" avant , je portais des minijupes . on pouvait mettre ce qu' on voulait ! " , poursuit Azadeh Kian .
les femmes et les hommes n' ont pas le droit de se faire la bise .
l' alcool et le porc sont interdits ainsi que certaines musiques , comme le rock .
mais il existe des groupes de musique qui se cachent pour jouer ce qu' ils veulent et des jeunes font la fête dans des appartements ou dans des autocars , parfois en buvant de l' alcool .
" en Iran , il y a deux vies : les gens peuvent tout faire chez eux mais quand ils sont dans la rue , ils doivent respecter ces lois . quand j' étais jeune , c' était le contraire , l' espace public était plus libre qu' à l' intérieur de certaines familles " , remarque Azadeh Kian qui a soixante ans aujourd'hui .
le pouvoir est très religieux , " mais de plus en plus d' Iraniens sont laïcs , ça veut dire qu' ils aimeraient que l' Etat et la religion soient séparés comme en France " , ajoute t elle .
en Iran , la presse n' est pas libre : les journalistes ne peuvent pas tout dire et certains sont en prison .
de nombreux livres , films et sites Internet sont censurés : ça veut dire qu' ils sont en partie ou totalement interdits par le pouvoir .