qu' est ce que la Convention internationale des droits de l' enfant ?
chaque année , le vingt novembre , on célèbre la Journée internationale des droits de l' enfant .
mais cette année est un peu spéciale : on fête aussi les trente ans de la Convention internationale des droits de l' enfant ( Cide ) , adoptée en dix neuf cent quatre vingt neuf .
c' est un texte très important .
la preuve : c' est celui signé par le plus de pays au monde !
seuls les États Unis ne l' ont pas vraiment signé .
la Cide interdit de condamner à mort quelqu' un qui a commis un crime quand il était enfant et les États Unis ne sont pas d' accord .
cette convention a permis , pour la première fois , de reconnaitre que " les enfants aussi ont des droits , pas seulement les grandes personnes " , explique la juriste Marie Françoise Luecker Babel , autrice du Dictionnaire des droits de l' enfant : " comme c' est un sujet important , les États ont décidé de se mettre d' accord ensemble . donc ça concerne tous les enfants du monde . " la Cide dit qu' il faut toujours rechercher ce qui est le mieux pour un mineur , c' est à dire pour une personne de moins de dix huit ans .
l' opinion des plus jeunes doit être prise en compte dès qu' une décision qui les concerne est prise .
ça ne veut pas dire que les enfants sont les chefs et peuvent faire tout ce qu' ils veulent .
mais , par exemple , quand des parents se séparent , leurs enfants doivent pouvoir dire ce qu' ils pensent de la situation et s' ils préfèrent vivre avec leur père ou avec leur mère .
ça veut aussi dire que les adultes ne doivent pas forcer un élève de seize ans à devenir cuisinier s' il a envie de faire des études de dessin .
la Convention internationale des droits de l' enfant n' est pas juste un texte rangé dans un tiroir .
" tous les cinq ans , les États sont obligés de montrer ce qu' ils ont fait au Comité des droits de l' enfant . ça les oblige à faire plus " , affirme Marion Libertucci , responsable du service " plaidoyer et expertise " à l' Unicef .
si un État ne fait pas assez d' efforts , la Cide " sert à pouvoir intervenir auprès de son gouvernement en disant " ça , ça ne va pas " " , poursuit Marie Françoise Luecker Babel .
ce texte a permis de grandes avancées dans le monde : de plus en plus d' enfants sont vaccinés , mangent à leur faim , ont accès à l' eau potable , vont à l' école ...
que faire si tes droits ne sont pas respectés ?
la première chose à faire , c' est d' en parler à un adulte en qui tu as confiance .
ça peut être un parent , un enseignant , un médecin ...
tu peux aussi contacter le Défenseur des droits : c' est une structure spécialisée dans la défense des droits des citoyens et une partie de son travail consiste à défendre les enfants ( si tu es harcelée à l' école , si tes parents se séparent , si on refuse que tu fasses des études de coiffure parce que tu es un garçon ...

si tu es en danger , par exemple si tes parents ou d' autres personnes sont violents envers toi , appelle le cent dix neuf , c' est un numéro gratuit .