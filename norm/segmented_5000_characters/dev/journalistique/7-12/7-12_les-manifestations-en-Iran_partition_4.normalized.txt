comment l' opposition s' exprime t elle ?
en France , rien que dans la ville de Paris , plusieurs centaines de manifestations ont lieu chaque année .
on a le droit de se réunir en groupe pour critiquer quelqu' un ou quelque chose si on a demandé l' autorisation avant .
en Iran , c' est différent .
si on veut faire des reproches aux dirigeants et demander des changements , on a le droit de le faire , mais dans son coin .
" les personnes ont le droit de critiquer individuellement . ça peut être sur les réseaux sociaux , dans la rue , en écrivant des lettres , explique la sociologue spécialiste de l' Iran Amélie Myriam Chelly . mais si , avec vos critiques , vous réunissez beaucoup de personnes qui pourraient déstabiliser le pouvoir , c' est très réprimé . " en revanche , les personnes qui soutiennent les dirigeants peuvent manifester .
depuis quelques mois , des femmes montrent qu' elles ne sont pas d' accord avec l' obligation de porter un voile sur leurs cheveux .
mais elles savent qu' elles ne peuvent pas vraiment manifester , donc elles ont trouvé une solution : le mercredi , elles portent un voile ou un vêtement blanc .
c' est le symbole qu' elles ont choisi pour montrer leur mécontentement .
des hommes les soutiennent en portant un foulard ou un bracelet blanc .
des artistes résistent aussi à leur façon aux règles imposées en Iran .
c' est par exemple le cas du réalisateur de cinéma Jafar Panahi .
en deux mille dix , il a été emprisonné et interdit de faire des films pendant vingt ans , parce qu' on l' a accusé d' être opposé au régime et de critiquer la société iranienne .
mais il a continué en secret .
il a ainsi tourné un film dans son appartement et un autre dans un taxi , avec une minuscule caméra cachée dans une boite de mouchoirs posée sur le tableau de bord .