comment se déroule une élection ?
les français vont voter aux élections régionales le six et le treize décembre .
pour avoir le droit de voter , il faut être majeur ( avoir au moins dix huit ans ) et être inscrit sur les listes électorales , c' est à dire les listes sur lesquelles sont écrits tous les noms des citoyens qui ont le droit de voter .
l' élection se passe dans un bureau de vote , généralement installé dans une mairie ou une école .
chaque électeur doit aller dans celui qui est indiqué sur sa carte d' électeur .
dans chaque ville , le nombre de bureaux de vote dépend du nombre d' habitants .
par exemple , il y en a cent trente six à Bordeaux et soixante et onze à Amiens , qui est une plus petite ville .
il faut d' abord montrer sa carte d' identité et sa carte d' électeur .
ça permet de vérifier qu' on est bien inscrit sur les listes électorales et qu' on a bien le droit de voter .
on prend une enveloppe et des bulletins de vote .
ce sont les papiers sur lesquels sont écrits les noms des candidats .
on n' est pas obligé de prendre tous les bulletins , mais il vaut mieux en prendre au moins deux , car le vote doit être secret : si on n' en prenait qu' un , tout le monde saurait pour qui on vote .
on se rend dans l' isoloir , une sorte de petite cabine fermée par un rideau .
une fois qu' on est dedans , à l' abri des regards , on met le bulletin du candidat pour qui on vote dans l' enveloppe .
on montre une nouvelle fois sa carte d' identité et sa carte d' électeur .
puis on glisse l' enveloppe contenant le bulletin dans une boite , qu' on appelle l' urne .
il faut signer pour prouver qu' on a bien voté .
ça permettra ensuite de vérifier que le nombre de bulletins correspond au nombre de votants .
dans certains bureaux de vote , il n' y a ni bulletins en papier , ni enveloppes , ni urne car le vote est électronique : les listes de candidats sont présentées sur un écran et il faut appuyer sur celle pour qui on veut voter .
les personnes qui ne peuvent pas aller voter , par exemple parce qu' elles sont parties en vacances , ont le droit de faire une procuration , c' est à dire autoriser quelqu' un d' autre à aller voter pour elles .
le soir , quand le bureau de vote ferme , commence le dépouillement , c' est à dire l' ouverture de toutes les enveloppes pour compter le nombre de voix que chaque candidat a obtenu .
n' importe quel électeur qui sait lire et écrire peut dépouiller .
certains bulletins ne sont pas valables .
on dit alors qu' ils sont nuls .
c' est par exemple le cas quand quelqu' un a écrit sur le bulletin ou quand il a mis plusieurs bulletins dans l' enveloppe .
un électeur peut décider de ne pas mettre de papier dans l' enveloppe : on dit qu' il vote blanc , c' est à dire pour personne .
c' est une façon de dire qu' aucun candidat ne lui plait , mais qu' il s' intéresse quand même à l' élection .
quand tous les bulletins ont été dépouillés , le président du bureau de vote annonce les résultats .