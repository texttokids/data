qu' est ce que l' Iran ?
l' Iran fait trois fois la taille de la France .
quatre vingts millions de personnes y vivent , surtout en ville .
la capitale s' appelle Téhéran .
c' est un pays jeune : plus de la moitié des habitants ont moins de vingt ans ( c' est deux fois plus qu' en France ) .
l' Iran possède des richesses dans son sol : beaucoup de pétrole et de gaz .
mais il y a " un problème de répartition des richesses , remarque Thierry Coville , chercheur et spécialiste de l' Iran . le pays compte beaucoup de jeunes diplômés qui ne trouvent pas de travail correspondant à leurs études . ils sont obligés de faire des petits boulots ou de travailler dans un autre pays . "
la majorité des habitants sont persans et il existe aussi quatre vingts ethnies : kurdes , Turkmènes , Baloutches , Turcs , et caetera Les habitants parlent surtout le persan ( ou farsi ) , une langue qui s' écrit de droite à gauche ( alors que le français s' écrit de gauche à droite ) .
c' est la langue officielle du pays .
les mots français " sucre " , " pyjama " et " nénuphar " viennent du persan .
voici comment ils s' écrivent et , entre parenthèses , la façon dont on les prononce :
neuf Iraniens sur dix sont musulmans chiites .
l' islam , leur religion , se divise en deux grandes branches , comme un arbre : une sunnite ( la majorité des musulmans dans le monde ) et une chiite .
les chiites et les sunnites n' ont pas la même interprétation de l' islam .
en Iran , il y a aussi des chrétiens , des juifs et des zoroastriens , qui sont libres de pratiquer leur religion .
avant le XIXe ( dix neuvième ) siècle , l' Iran s' appelait la Perse .
c' était un très grand empire , fondé il y a deux cinq cents ans par Cyrus le Grand .
la capitale de la Perse s' appelait Persépolis .
c' est aujourd'hui un site archéologique , classé au patrimoine mondial de l' Unesco , que l' on peut visiter .
dans le pays , on peut aussi découvrir de beaux palais et de belles mosquées , comme dans la ville d' Ispahan .