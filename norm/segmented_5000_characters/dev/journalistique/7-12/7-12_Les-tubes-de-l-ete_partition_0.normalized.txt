numéro vingt trois juillet deux mille dix sept
les tubes de l' été
chaque été , on aime écouter en boucle la même chanson , la chanter à tue tête , danser dessus ...
et quand on retourne à l' école , on la réécoute en repensant aux vacances .
c' est ça , la magie des tubes de l' été , ces chansons qui connaissent un grand succès .
quel sera le morceau des grandes vacances cette année ?
d' où viennent les tubes de l' été ?
comment fait on un album et comment se fabrique un tube aujourd'hui ?
" le P' tit Libé " t' emmène en musique sur la route des vacances .
Erza , onze ans , chanteuse des Kids United
Erza a onze ans et elle est déjà une star !
elle fait partie des Kids United , avec quatre autres enfants âgés de dix à dix sept ans .
depuis son premier album sorti il y a deux ans , le groupe a beaucoup de succès et a déjà vendu plus de un , cinq million de disques .
en juin , les Kids United ont sorti une chanson , Mama Africa , qui pourrait bien devenir un tube cet été .
" je l' aime beaucoup parce qu' elle apporte de la joie et donne envie de danser , explique Erza . en plus , on a tourné le clip au Sénégal pendant quatre jours et on s' est vraiment amusés . "
la jeune chanteuse habite à Sarreguemines , une ville située dans le nord est de la France .
elle termine son année de sixième avec des félicitations .
elle va à l' école la semaine comme tous les autres enfants .
mais le soir , après avoir mangé et fait ses devoirs , elle répète souvent de nouveaux morceaux .
ça ne la dérange pas parce qu' elle " chante tout le temps " , partout où elle va et ça depuis l' âge de deux ans !
chaque week end et parfois le mercredi , elle retrouve les Kids United pour chanter dans des concerts , poser pour des séances photo , répondre à des interviews ...
" je ne pensais avoir ce succès au début . on me demandait des photos , des autographes , on me reconnaissait dans la rue ... maintenant je me suis habituée et ça me fait toujours plaisir . "
tout le mois de juillet , Erza se produit devant des milliers de spectateurs lors d' une dizaine de concerts en France , en Belgique et en suisse .
en août , elle pourra enfin se reposer et partira en vacances au Kosovo , le pays d' origine de sa famille , où habitent ses grands parents et ses cousins .
plus tard , Erza souhaite " toujours chanter tout en faisant des études pour devenir dentiste parce que j' aime bien ce métier " .
Sarreguemines , la ville où habite Erza