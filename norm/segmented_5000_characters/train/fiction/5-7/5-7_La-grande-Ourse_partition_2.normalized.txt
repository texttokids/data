Epilogue
aujourd'hui , c' est le dernier jour des vacances et je passe ma deuxième étoile .
il neige .
les autres enfants de mon groupe ont peur .
ils disent qu' on ne voit rien , que les bosses disparaissent quand le soleil se cache .
ils craignent de tomber .
moi je suis tranquille .
je sais que même si je tombe , une voix me guidera .
et je sais aussi que cette voix , c' est la voix de ma petite soeur qui naitra dans huit mois , selon les calculs que mon père a fait sur ses doigts .
je sais aussi que même si mon père est un savant et que ma mère réfléchit beaucoup , ils se trompent parfois .
je pense à Norah , la petite ourse , la soeur d' Orso et je me dis qu' on va devenir une grande famille .