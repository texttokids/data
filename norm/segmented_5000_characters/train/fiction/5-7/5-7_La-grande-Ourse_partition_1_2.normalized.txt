moi aussi je crois que c' est grâce à l' écho .
qu' est ce que tu en penses , papa .
toi qui es scientifique ?

mon père ne disait toujours rien .
il nous regardait en secouant la tête , comme s' il n' en croyait pas ses yeux de nous voir là .
" ma petite famille !
a t il dit , comme si c' était la réponse à ma question .
et il a répété encore :
ma petite famille .
plus si petite que ça , " a fait ma mère .
et soudain , j' ai tout compris , comme Galilée , comme pour le big bang .
j' ai compris à quoi ma mère réfléchissait et j' ai compris ce que mon père comptait sur ses doigts .
moi aussi j' étais un scientifique finalement .
on peut être champion de ski ET scientifique .
si c' est une fille , j' ai demandé , on l' appellera Norah ?
mes parents ont ri aux éclats et m' ont dit en coeur :
ce sera un garçon !