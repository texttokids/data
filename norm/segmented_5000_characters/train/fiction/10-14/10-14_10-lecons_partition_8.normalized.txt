mercredi .
c' est Pedro qui arrive le premier .
il apporte un gâteau enveloppé dans un torchon à carreaux .
c' est moi qui l' ai fait avec ma mère .
je le mets où ?
Simon réfléchit .
mets le dans l' armoire , si tu trouves de la place .
tu feras la surprise aux autres , d' accord ?
Pedro s' exécute , puis vient s' agenouiller près de Simon qui , comme chaque matin , est assis devant la table basse couverte de livres .
et lit .
Hector arrive peu après , dépose son sac à dos , enlève ses écouteurs , dit " salut " , tourne un moment dans la pièce , chantonne , les bras écartés .
puis , comme il ne se passe rien , il va s' asseoir près de Pedro , prend un livre , tourne les pages sans lire vraiment .
Kèv et Marion entrent ensemble .
salut , dit Kèv .
Pedro et Simon lèvent les yeux .
Simon fait un geste de la main .
Pedro sourit à Kèv , lui montre le coussin à côté de lui .
Kèv tend ses béquilles à Marion , se laisse tomber à côté de Pedro .
et attend .
marion est restée debout .
elle regarde la scène , méfiante .
elle pose les béquilles de Kèv contre une étagère , puis , à pas lents , elle contourne la table , examine les livres de haut .
en repère un : un livre de poche , couverture souple , couverte de gribouillages , tâches et ratures , comme un agenda d' écolier .
titre en lettres rouges : mon coeur a des dents .
elle s' en empare , va s' asseoir à sa place habituelle , sur le radiateur .
l' air concentré , maussade , elle retourne le livre , lit la quatrième de couverture .
puis , sans décoller du radiateur , d' un geste précis , elle renvoie le livre sur la table .
les autres arrivent en même temps .
comme le silence règne dans la bibliothèque , ils se taisent aussi dès qu' ils rentrent .
tour à tour , ils s' installent autour de la table basse , déplacent les livres , les ouvrent , les feuillètent , s' arrêtent sur une page , commencent à lire .
ça dure une dizaine de minutes .
jusqu'à ce que Noah demande :
qu' est ce qu' on fait ?
Lucie se lève d' un bond .
j' ai des fourmis dans la jambe ...
Hector s' étire .
Alice ferme le livre qu' elle était en train de lire .
etc .
Simon écarte les bras , montre la table et annonce :
aujourd'hui , on lit .
oh non , soupire Lucie .
hé si , dit Simon .
mais d' abord , j' ai apporté un cadeau à chacun .
un tout petit cadeau .
de sa sacoche , il tire une pile de carnets qu' il étale devant lui .
des carnets format poche , aux couvertures unies , rouges ou noires .
j' en veux un noir !
dit Marion .
remue ménage .
un peu de confusion .
et puis , les voilà tous en rond , un carnet dans les mains .
marion s' est assise à côté de Kèv , se décalant légèrement , quelques centimètres qui la maintiennent hors du cercle .
ce sera votre carnet de poésie , explique Simon .
vous pourrez recopier des poèmes qui vous plaisent , et bien sûr écrire vos propres textes .
pour commencer , je vous invite à une partie de pêche .
il fait silence , mais personne ne s' étonne .
marion tourne les pages blanches de son carnet , lentement , comme si elle y lisait des mots visibles à elle seule .
c' est simple , reprend Simon .
vous feuilletez les livres sur la table , et vous pêchez des fragments de poèmes qui vous émeuvent , vous surprennent , vous amusent ...
ce sont les poissons qui s' accrochent à votre hameçon .
vous les pêchez en les écrivant sur votre carnet .
et gentiment , vous les remettez en place , sur la table .
une dernière remarque : quand on pêche , on ne fait pas de bruit .
quand je vais pêcher avec mon grand père , on n' a pas le droit de parler , dit Noah .
Simon sourit .
eh bien , pour nous , c' est pareil .
surtout que nous n' avons que dix minutes chrono .
Kèv , on te passe les livres , ok ?
non , je me débrouille , je peux bouger , dit Kèv .
dix minutes donc .
ils se déplacent en silence , ouvrent , ferment les livres , chacun à son rythme .
marion continue à rêver sur les pages de son carnet .
Pedro lui met un livre dans les mains .
elle le prend , le rejette aussitôt , bouscule Lucie et Lila et s' empare de Mon coeur a des dents , note presqu' aussitôt quelque chose sur son carnet .
Simon jette un coup d' oeil sur sa montre .
à vos places , s' il vous plait .
bien .
maintenant , parmi les fragments que vous avez recopiés , vous en choisissez un .
un que vous allez offrir aux autres , en le lisant .
il les regarde , indécis .
Kèv s' est allongé .
il frotte sa jambe plâtrée .
tu as mal ?
demande Simon .
ça va , dit Kèv .
Simon reprend :
comment pourrait on faire pour offrir vraiment les morceaux de poésie que vous avez pêchés ?
qui a une idée ?
Alice a une idée .
on pourrait se mettre comme Kèv , couchés sur le dos , et lire notre poème en regardant le plafond .
avant même que les autres réagissent , elle explique :
c' est parce que ...
quand je dois apprendre par coeur un poème pour l' école ...
je le récite le soir dans mon lit ...
dans le noir ...
et je trouve ...
ça sonne mieux ...
super idée , dit Simon .
essayons .
je propose que vous vous disposiez en étoile , la tête vers le centre du cercle ...
il faut baisser les stores , dit Lucie .
tu as raison , dit Simon .
deux minutes de remue ménage plus tard , ils sont étendus sur le dos , dans la pénombre , formant une grande roue vivante .
marion est entre Alice et Kèv .
prenez votre temps , dit Simon .
prononcez lentement votre texte , essayez de le projeter jusqu' au plafond et attendez que les mots retombent sur nous .
je peux commencer ?
demande Noah .
commence qui veut , dit Simon .
il n' y a pas d' ordre .
préparons nous d' abord .
laissons le silence se faire .
vous allez voir : il épaissit peu à peu , et après , c' est comme si on le sentait sur soi .
ensuite , chacun donne son texte quand il est prêt .
et si deux personnes parlent en même temps , c' est très bien aussi .
il y a quelques rires , d' abord , du mouvement , et puis après plusieurs essais , ça marche .
ça donne à peu près ceci ( Textes à présenter sur une page entière , texte en blanc sur fond noir , ou en noir dans des bulles , sur fond noir .
