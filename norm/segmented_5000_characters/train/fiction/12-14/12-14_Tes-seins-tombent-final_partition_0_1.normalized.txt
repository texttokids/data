ses cheveux sont ramassés dans une queue de cheval qui bascule allègrement .
ce n' est pas difficile de remarquer qu' elle est une danseuse .
de talent !
pas besoin de lui dire " tiens toi droite ! " elle marche comme dans un ballet .
j' essaie de l' imiter .
un kiné m' a donné le secret : " dressez la tête , le reste suivra ! " de lumbago en sciatique , mon dos a une existence douloureuse .
" il faut travailler la verticalité ! " , me dit le kiné .
les épaules rondes , la tête en avant , le ventre bombé , je suis la gazelle .
nous avons pris le petit déjeuner sur la terrasse , la mer étalée devant un horizon aussi proche que le beurre sur la tartine .
le ciel bleu irréel , les oliviers argentés , on ne peut pas rêver mieux .
" que c' est beau ! " dis je inlassablement pour provoquer une réaction éventuelle , mais elle est aveugle à tout ce qui n' est pas lié à ce téléphone qui absorbe entièrement son attention .
" c' est beau , non ? " je me hasarde de nouveau .
oui , oui ...
" dit elle , blasée .
je ne suis jamais allée en Corse avec ma grand mère , je ne suis pas allée plus loin que le bout de sa rue à Brooklyn .
quand j' étais petite en été , toute la famille de ma mère , tantes , oncles , cousins , grands parents , nous déplacions aux Catskills , des montagnes ( collines ?
) à New York .
on remplissait la voiture de draps , serviettes et de casseroles qu' on déménageait dans le " coch alein " ( cuisinez vous mêmes ) .
chaque famille y louait une chambre .
dans la grande cuisine collective , il y avait dix frigos , dix gazinières et dix tables .
chaque famille faisait la cuisine pour son unité .
mon grand père organisait des expéditions et nous ramassions des seaux entiers de myrtilles .
mon père qui travaillait la semaine ne venait que les week ends .
il devait être heureux à la maison sans le harem .
si c' était la solitude et la sérénité que l' on cherchait , on ne pouvait pas la trouver ici à Divine Corners .
les tantes n' avaient que des critiques à notre égard , surtout envers mes soeurs .
les shorts étaient trop courts pour des filles aussi potelées , elles étaient trop maquillées .
" et c' est comme ça que vous vous coiffez ? ! " elles étaient adolescentes .
chaque bouton était passé en revue .
moi , j' étais petite , je sautais dans la piscine avec mes cousins , loin de la censure des tantes acerbes .
on y va ?
dit Yona , impatiente de se mettre à l' eau .
il faut marcher trois cents mètres , puis descendre une falaise avec une corde que Nicole , notre hôte , a plantée .
Yona y va comme Tarzan .
je suis paralysée de peur .
j' ai peur de glisser , j' ai peur de tomber , j' ai peur de me casser .
j' y vais tout doucement agrippée à la corde .
quand j' atterris , la sirène est dans l' eau .
Nicole a déjà ramassé avec son filet une flopé de méduses qui gisent sur la plage .
on la nomme " la plage Nobel " parce qu' elle est adjacente à l' Institut d' Etudes Scientifiques de Cargèse et souvent honorée par la présence de Prix Nobel en maillot .
un de ces Prix Nobel , ce grand bel homme avec son regard bleu on se croirait dans un film d' Hollywood quand on le voit est le voisin de Dan et Nicole , mes meilleurs amis qui ont acheté la maison d' un astronome .
je n' étais pas contente parce que cet achat me les enlevait tout l' été .
je refusais d' y aller , je les boudais .
dan me dit : " un jour tu viendras et tu regretteras chaque minute que tu n' as pas passé ici .
j' ai craqué .
il avait raison .
je viens tous les ans , mais demander une invitation pour cette môme a été dur , même à mes meilleurs amis .
on supporte les siens , pas ceux des autres .
mon voisin de plage Nobel est là avec ses enfants et ses petits enfants , ce qui fournit de la compagnie à Yona .
elle n' a aucun problème à s' intégrer dans le groupe de jeunes .
et avant même que je m' en rende compte , elle monte sur un bateau avec les autres et disparait , sans crème solaire , sans serviette , sans tee shirt , sans chaussures , sans me dire quoi que ce soit .
sa mère va me tuer !
j' ai élevé mes enfants aussi bien que j' ai pu en collant à la définition de Winicott pour être une mère " suffisamment bonne " .
est ce que JE les ai élevés , ou se sont ils élevés tout seuls ?
disons que j' étais le témoin amoureux de leur évolution .
j' ai adoré chaque étape .
c' était la plus grande aventure de ma vie .
je suis encore hantée par les soucis pour leur bien être , mais j' ai pris ma retraite .
je n' ai personne pour qui préparer le petit déjeuner et les autres repas .
je suis sans contraintes , sans obligations , libre !
mais je vis dans la nostalgie de l' enfance de mes enfants .
quelle période faste , stimulante , remplie de surprises , et de l' espoir en leur surpassement !
une de leurs ( multiples ) bonnes notes me transportaient .
j' avais un projet , une responsabilité , un but dans la vie .
j' avais une certaine autorité , possession , obsession .
j' avais aussi de la conversation , même si cela se réduisait à " range ta chambre ! " ou " mets ton pyjama ! " .