Elise soupire , assise à son bureau .
son père remarque que quelque chose ne va pas et l' interroge : " que se passe t' il ? pourquoi as tu l' air si ennuyée ? " " je dois trouver un sujet pour mon exposé mais je manque d' inspiration ... " " demande aux neuf muses de t' aider . " " les neuf muses ? "
son père s' installe près d' elle et commence à lui narrer l' histoire des muses .
" dans la mythologie grecque , on raconte que Zeus , le roi des Dieux et Mnémosyne , la déesse de la Mémoire ont eu neuf filles . chacune représentait un art et aidait les artistes en manque d' inspiration . "
" la première s' appelait : Calliope . c' était la muse de la poésie épique . elle portait une couronne d' or avec un livre , un stylet et une trompette . "
" la deuxième s' appelait : Clio . c' était la muse de l' Histoire . elle portait une couronne de laurier , tenait un livre et était accompagnée d' un cygne . "
" la troisième s' appelait : Erato . c' était la muse de la poésie lyrique et de la chorale . elle portait une couronne de myrte et de rose , un tambourin ou une lyre . "
" la quatrième s' appelait : Euterpe . c' était la muse de la musique . elle tenait souvent une flute ou une trompette . "
" la cinquième s' appelait : Melpomène . c' était la muse de la tragédie . elle portait une couronne de feuilles de vigne , une épée , un cor , un masque et un sceptre à ses pieds . "
" la sixième s' appelait : Polymnie . c' était la muse de la rhétorique . elle portait une couronne de perles et un orgue . "
" la septième s' appelait : Terpsichore . c' était la muse de la danse . elle portait une couronne de guirlande et un instrument à cordes , comme la lyre ou la viole . "
" la huitième s' appelait : Thalie . c' était la muse de la comédie . elle portait une couronne de lierre , une viole , un masque et un rouleau . "
" la neuvième s' appelait : uranie . c' était la muse de l' astronomie . elle portait une couronne d' étoiles , et tenait dans ses mains un compas et un globe . "
" avec neuf muses , il y en a bien qui devraient t' inspirer Elise . " " oui , je pense que je vais faire mon exposé sur elle et inspirer mes camarades de classe comme tu viens de le faire avec ton histoire . merci Papa ! "