et c' est ainsi que le deuxie me jour , le petit chien Séthi se rendit a l' E cole situe E au Nord du royaume accompagne de Rose , les bras charge S de fleurs aux couleurs E clatantes .
à leur arrive E , les enfants accueillirent nos amis chaleureusement .
pour la premie re fois de leur vie , ils allaient cre er des potions de couleurs .
chacun se mit a sa ta che .
pendant que Rose surveillait son chaudron , les enfants peignirent les bords de la rivie re .
mais la ta che E tait trop dure pour Rose .
elle ne disposait pas suffisamment de fleurs pour colorer tout le royaume .
à cet instant la fe E Gai a apparut .
elle avait dans ses bras un vieux grimoire .
elle lut a tous un paragraphe du vieux livre .
" quand l' animal domestique apre S grandes peines viendra parler . l' or et l' espoir associe S libe reront les couleurs abandonne es . "
Séthi n' en crut pas ses yeux : ce vieux grimoire parlait de lui .
He las la fe E Gai a E tait aussi porteuse d' une mauvaise nouvelle .
ses recherches E taient infructueuses car il n' y avait point d' or dans le royaume .
" peut e tre est il temps pour toi de faire usage de ta magie , sugge ra Gai a ? sans cet or , le male fice ne pourra E tre rompu . " alors Séthi prit une grande inspiration et prononc a sa formule magique : " Ruse comme un renard , fute comme un chien . qu' un simple regard montrera le chemin . " puis il observa autour de lui .
devant lui , un homme assis sur un banc feuilletait le journal des royaumes .
il s' approcha timidement et lut la dernie re page .
c' E tait la page des sports et des dates des prochains championnats .
de quoi sont faites les me dailles des champions , s' interrogea Séthi ?
elles sont faites a partir de l' or le plus pur , re pondit Gai a .
ainsi le lendemain matin , le petit chien Séthi se rendit dans l' E cole situe E au sud du royaume .
il E tait accompagne de la fe E Gai a .
elle avait dans la main sa baguette magique .
" mes chers amis , de clara Séthi . ce jour est un grand jour pour vous . il est grand temps de rompre le sort qui s' abat sur Petrosa . pour cela , nous allons vous aider a vous surpasser et de crocher le plus de me dailles d' or possibles . " Aussito T , Gai a brandit sa baguette magique et prononc a une incantation d' encouragement .
" par le pouvoir de la magie , tes re ves seront accomplis . travail et efficacite seront les cle S de ton succe S . et quand tu seras le meilleur , de l' or couvrira ton coeur . " à la sortie des classes , les parents n' en crurent pas leurs yeux .
leurs enfants portaient autour de leurs cous des me dailles d' or aussi grosses que leurs petits coeurs .
ce n' est pas possible !
c' est incroyable !
comment avez vous re alise cela ?
s' interrogea un parent .
c' est gra ce a la magie .
Re pondit fie rement l' enfant .
la magie existe toujours dans notre royaume .
il faut juste y croire .
à ces mots , les me dailles se mirent a briller telles des petites E toiles .
mais c' est de la magie !
se dit surpris un parent .
alors elle est toujours la !
les parents comprirent que sans leur action la magie ne pouvait fonctionner .
et dans un bref soupir , ils annonce rent de tout leur coeur que la magie existait dans le royaume .
Aussito T , la vieille pierre se re veilla .
elle se mit a briller de plus en plus fort .
les parents et les enfants furent vite E blouis et ferme rent les yeux .
lorsqu' ils les ouvrirent .
Petrosa avait retrouve ses couleurs .
la nouvelle circula rapidement dans les contre es lointaines .
les artistes revinrent de nouveau a Petrosa .
pour remercier nos amis de la fore T d' One ria , les parents et les enfants de cide rent de rendre hommage a la pierre .
et c' est ainsi que la pierre magique de Petrosa , fut entoure E de fleurs de couleurs resplendissantes mais nul connaissait leur provenance .
sauf les enfants qui en avaient une ide E .