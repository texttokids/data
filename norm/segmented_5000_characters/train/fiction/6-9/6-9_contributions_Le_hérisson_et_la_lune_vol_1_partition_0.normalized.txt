premier partie : le grand saut .
il faisait nuit , et dans le ciel dégagé , de nombreuses étoiles brillaient .
alors notre hérisson sortit de son logis .
il était heureux .
la pluie , qui tombait depuis des jours , avait cessé .
le ciel était , enfin , sans nuage et il pouvait la voir briller au dessus de sa tête , grande , ronde , blonde comme des épis de blés , et pleine , en plus .
il aimait tant la regarder .
des fois , elle ne lui laissait voir qu' un croissant .
d' autres fois , elle jouait à cache cache et disparaissait .
quand il en manquait un bout , notre porc épic attendait qu' elle soit pleine , c' est comme ça qu' il l' aimait .
et c' est comme ça qu' il voulait l' atteindre .
il voulait la voir de plus près , la toucher .
il voulait décrocher la lune ...
et la garder pour lui .
c' est normal , il l' aimait ...
pour ça , il était prêt à tout pour y arriver .
c' est sa rondeur qui l' attirait le plus .
alors , lui aussi , il se mettait en boule , pour montrer à la lune qu' il est rond , comme elle .
d' habitude les hérissons " se mettent en boule " quand ils sont en colère .
c' est de là que vient l' expression .
et dans cette position il la regarde et s' endort .
il rêve qu' il la rejoint ...
quand il a commencé à s' entrainer pour monter aux arbres , notre hérisson aurait pu se décourager , devant l' ampleur de la tâche .
mais il continuait à grimper de plus en plus haut , de plus en plus vite .
parfois , il tombait , mais il se relevait et continuait son entrainement .
il avait un but : atteindre le sommet du plus grand arbre de la forêt .
le tout , avant le lever du jour ...
il y avait beaucoup de spectateurs à ses tentatives et quand il décida de s' attaquer à l' ascension du plus grand arbre de la forêt .
un hêtre gigantesque , proche de la lune .
toute la forêt voulait voir ça .
il réussit son pari et escalada , en quelques heures , le grand arbre et atteignit son sommet .
mais il était déçu , elle était là , à portée , mais il fallait sauter , sauter haut et loin , et lui , le hérisson , ne savait pas sauter ...
il s' entraînait de longues heures , à la mare , avec les grenouilles .
il s' exerçait à sauter , avec elles et " atterrissait " le plus souvent , dans l' eau .
heureusement , il savait nager , sauter c' était moins évident , mais il faisait de son mieux ...
un soir , à la pleine lune , par beau temps , Il refit l' ascension du grand hêtre et arriva à sa cime .
il reprit son souffle et observait la belle , un peu plus loin dans le ciel .
alors , confiant , il prit son élan et sauta ...
dans le vide .
il tomba un peu plus bas , sur une branche bienveillante qui amortit sa chute .
il avait eu de la chance .
il resta un long moment sans bouger , à réfléchir .
elle était si proche qu' il l' avait presque touchée .
pour y parvenir , il lui faudrait voler ...
mais un hérisson ne vole pas ...
découvre la suite de cette histoire dans la deuxième partie !