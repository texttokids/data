un jour , un célèbre écrivain avait dit que la cité de Bénarès était plus ancienne que l' Histoire .
cette cité se trouve des deux côtés du Gange , la rivière sacrée , qui est , d' après les hindous : l' essence divine du dieu Shiva .
les saintes rues de la cité sont peuplées de pèlerins , et où que vous tourniez la tête , vous voyez soit un temple , soit un sanctuaire .
plusieurs siècles auparavant , il y avait un Roi à Bénarès .
ce roi n' aimait rien de plus au monde que le son de sa propre voix .
il avait vécu assez longtemps , vu maintes choses , lu beaucoup de livres , et rencontré beaucoup de merveilleuses personnes .
mais ses pensées radotaient , et son discours n' était jamais court .
lorsqu' un de ses généraux le questionnait sur une affaire militaire , il répondait de cette manière là : " que ferait Alexandre le Grand ? " .
et quand il avait terminé de raconter une anecdote de peu d' importance , il ne se souvenait plus de la question qu' on lui avait posé en premier lieu .
un jour , alors que le roi se promenait dans le jardin de son palace , il trouva une tortue allongée sur le chemin .
la pauvre créature était morte .
sa carapace était fracassée en plusieurs morceaux .
cette étrange découverte le rendit perplexe , car pour lui , c' était certainement un signe des Dieux .
cependant son esprit luttait au dedans de lui pour en comprendre le sens .
finalement , il appela le plus vieux et le plus sage homme de la cité de Bénarès , et lui demanda de lui interpréter le sens de cette découverte .
lorsqu' il vit la tortue brisée , le sage raconta l' histoire suivante ...
" maître .
il était une fois une tortue qui vivait aux bords d' un magnifique lac .
elle passait ses jours à l' ombre des grandes herbes , et à grignoter les herbes juteuses .
or une année , les pluies ne tombèrent point pendant la saison et le soleil brula la terre , qui devint de l' argile dure .
l' eau du lac diminua en petites flaques , puis disparut complètement .
les animaux qui étaient venus pour boire , maigrirent jusqu'à devenir des sacs d' os , et repartirent titubants , les pattes affaiblies .
la tortue avait des affinités avec certaines oies du lac .
un jour , un couple d' oies vint lui dire adieu , car la troupe d' oies avait décidé d' émigrer vers l' Himalaya , à la recherche d' eau et d' herbe fraiche .
" et qu' est que je vais devenir moi ? demanda la tortue . j' ai cent ans , ma carapace est lourde et j' ai des petits pieds . si je me mets à marcher à mon rythme toute la journée , je n' irais pas plus loin que le bloc de roche que vous voyez là bas . je vais devoir rester ici jusqu'à ce que je meure . " les deux oies eurent pitié de la vieille tortue et acceptèrent de l' aider .
voici leur plan : elles porteraient un bâton à deux en volant .
la tortue devra s' accrocher au bâton avec la bouche et c' est de cette façon inhabituelle qu' elles le transporteraient vers des régions plus froids et humides .
et c' est ainsi que la tortue se trouva en train de voler dans les cieux .
c' était une sensation très inhabituelle , qu' elle eut le mal de l' air .
les autres oies du troupeau trouvaient la vue assez hilarante .
" hé regardez , dit une oie , une tortue qui s' envole ! on aura tout vu ! " " mais non elle ne vole pas , dit une autre , elle mâche un bout de bois car elle a faim . " " je suis sure qu' elle serait plus à l' aise si elle abandonnait sa lourde carapace qui lui pend par derrière " , remarqua une troisième .
les remarques ne cessaient point , parce que les oies n' avaient rien d' autre à parler que de la tortue et de sa manière très étrange de voyager .
et la tortue finit par en avoir assez .
" regardez moi , dit la tortue , vous pensez que je me plais à faire ça ? " et c' est alors qu' elle ouvrit la bouche .
elle lâcha le bâton et tomba .
elle tomba , tomba et tomba , jusqu'à ce qu' elle atterrisse sur le sol de mon Seigneur , ici dans le jardin de votre palais .

c' était la fin de son histoire .
en réponse , le roi se mit à parler longtemps sur l' hibernation des oies .
le sage l' écoutait avec patience , en se disant que le roi n' avait toujours pas compris la morale de l' histoire .
quand il eut l' occasion de s' adresser poliment au souverain , sans qu' il eût besoin de l' interrompre , il se dirigea encore une fois vers le cadavre et dit ces paroles :
" et maintenant , maître puissant , retenez bien ceci . veillez à parler avec sagesse , chaque parole en son temps . la tortue est tombée dans le piège de la mort . elle parlait trop : voici la raison . " enfin , le roi comprit que les dieux lui avaient envoyé un signe .
dès lors , il veilla à ne dire que les bonnes paroles nécessaires .