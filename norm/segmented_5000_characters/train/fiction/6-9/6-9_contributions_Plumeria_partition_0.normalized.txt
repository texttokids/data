dans son royaume , le prince Iseult était adulé de tous .
avec ses longs cheveux bruns et ses beaux yeux verts , le prince ne comptait plus les compliments tant il en recevait .
mais la raison pour laquelle il était connu à travers le monde était sa passion et son talent pour les fleurs .
en effet , " aucun royaume ne possède de plus belles fleurs que Drysalia " criaient les villageois et se vantait le prince .
dans la vaste cour du château , des centaines de mètres carrés remplis de fleurs , toutes plus belles les unes que les autres .
des voyageurs du monde entier venaient admirer la beauté de ce jardin exceptionnel et tout le monde venait acquérir la fleur fétiche du prince Iseult , la fleur de Plumeria .
cette fleur magnifique aux nombreuses nuances de jaune , et de rouge n' avait que très peu d' égal et celles entretenues par le prince semblaient être plus imposantes et plus belles encore , comme si elles étaient sous l' emprise d' un charme .
à l' Ouest du jardin , onze allées étaient dédiées à cette fleur qui était considérée dans ce royaume comme un vrai bijou .
le joyau du prince .
alors qu' approchait la fête de Drysalia afin de célébrer ce joyau , le prince et la prospérité du royaume , les rumeurs avaient bon vent .
partout on entendait dire que le Prince allait enfin annoncer la date de son mariage .
ce serait en effet l' événement du siècle , l' un des jours les plus importants du jeune règne du Prince qui ressentait alors beaucoup de pression quant à l' annonce de cette date et de l' élu deux noeuds de son coeur .
les préparatifs pour la fête occupaient presque tout le royaume .
femmes , hommes et enfants avaient à leur charge la préparation des tables , des décorations , des banquets .
des milliers de petites mains travaillaient pour que cet évènement soit encore plus grandiose que celui de l' année précédente .
bien sûr , les fleurs provenant de la cour royale seraient omniprésentes dans la décoration afin que la beauté de cette fête fasse honneur au talent du Prince .
sur la place principale , des centaines de tables étaient disposées en rangées égales .
des milliers de fleurs étaient suspendues dans les airs , des plats chauds et fumants occupaient un gigantesque banquet sur tout le flanc Ouest de la place .
sur la pointe Nord s' élevait la table du Prince .
contrairement à son habitude , celle ci n' était pas préparée pour quatre personnes ( le prince , sa soeur et ses plus proches amis ) mais pour deux personnes .
l' attablée inhabituelle du Prince ne laissa personne indifférent , tous les membres du royaume savaient déjà pourquoi cette table n' était faite que pour deux personnes .
le Prince allait diner accompagné de la personne qu' il avait choisie pour partager le reste de ses jours .
c' est avec stupéfaction que tous les habitants de Drysalia observèrent l' arrivée du Prince car celui ci était accompagné d' un jeune homme aux traits absolument magnifiques .
sa peau couleur ébène , ses yeux gris et son regard profond ne laissèrent personne indifférent et son sourire semblait briller plus fort encore que le soleil lui même .
sa main qui semblait aussi douce qu' une brise de printemps tenait la main du Prince Iseult et ceux ci se stoppèrent devant l' assemblée visiblement très surprise .
" Drysaliens , c' est avec un immense honneur que je vous présente l' homme avec qui je vais partager le reste de ma vie , celui qui sera donc votre second Prince .
mes proches et ma soeur , ayant déjà refusé de partager notre table en signe de protestation contre notre union , j' invite quiconque s' opposant à notre amour à quitter immédiatement le royaume .
nul n' est digne de vivre parmi de si belles fleurs s' il ne saurait accepter l' amour sous sa forme la plus pure .
nous sommes amants depuis notre plus tendre enfance et avons toujours partagé l' amour des fleurs .
si tout le royaume pensait alors que la fleur de Pluméria tenait sa superbe de votre Prince , vous aviez raison .
il s' agissait cependant du Prince Masstan , le véritable joyau du Prince .

sur ces paroles , les deux Princes s' embrassèrent et firent hurler la foule de joie .
tous jetèrent leurs chapeaux en l' air et crièrent " vive le prince Masstan , vive le prince Iseult , longue vie aux princes " .