deuxième partie : le vol du hérisson .
pour apprendre à voler , il participa à un stage avec les jeunes oiseaux .
mais il avait beau battre ses petites pattes , il ne décollait pas d' un centimètre .
il lui fallait des ailes .
ce stage l' avait profondément marqué .
il avait des bleus et des bosses sur tout son corps et , pour une fois , il se sentait un peu découragé .
la prochaine pleine lune était loin et notre hérisson se remettait de ses blessures .
il gardait un peu espoir .
deux évènements allaient l' aider dans sa tentative .
d' abord une rencontre avec un être extraordinaire : un écureuil capable de grimper très vite aux arbres , et surtout de voler , enfin , de planer , plus exactement .
le rongeur était muni d' une membrane entre les pattes arrière et avant .
il pouvait ainsi se lancer dans le vide , et planer jusqu'à un autre arbre .
un rongeur pouvait voler , il l' avait vu ...
alors qu' il admirait le vol de l' écureuil , il s' était avancé , pour ne rien rater du spectacle , sur une branche un peu fragile .
la branche se brisa , il tomba sur le dos et se planta sur une feuille en forme de trapèze .
il eut un mal fou à se remettre sur ses pattes .
la feuille était accrochée à ses épines , et le gênait pour se retourner .
quand , enfin , il parvint à se remettre " sur pieds " , il pensa à l' écureuil .
il possédait maintenant un attribut qui devrait lui permettre de planer .
il grimpa sur un arbre assez haut , choisit une branche avancée , et prit pour cible un autre feuillu , à quelques dizaines de mètres .
il prit son élan , s' élança dans le vide et ...
plana ...
il rata son but et passa à côté de sa cible , mais atterrit un peu plus loin , en douceur .
il remonta sur un autre arbre et s' entraîna pendant de longues heures .
il arrivait , presque à tous les coups , à atteindre sa cible .
il arrêta quand la feuille , plantée au dessus de lui était trop déchirée .
alors il l' ota et alla dormir ...
il passa une bonne nuit , peuplée de rêves merveilleux , dont il était le héros .
quand il se sentira prêt et que les conditions de pleine lune et de beau temps seront réunies , il s' élancera du plus grand arbre de la forêt ...
le grand jour arriva , le hêtre monumental allait être le témoin d' un évènement particulier ...
à la tombée de la nuit , le hérisson se munit d' une " aile volante " qu' il avait choisie avec soin et cueillie directement sur un arbre .
l' escalade avec son fardeau ralentit son ascension mais il arriva quand même à temps , avant le petit matin , avant l' aube et le lever du soleil .
il reprit longuement son souffle et examina la situation .
la lune n' était pas très loin , à portée .
alors , il s' élança dans le vide et plana de longues minutes .
il volait avec une habileté , acquise par des semaines de pratique .
sa cible était en vue et il se dirigeait vers elle .
mais plus il avançait , plus la lune s' éloignait .
il se posa sur le hêtre , un peu plus bas , remonta jusqu'à la cime et s' élança à nouveau vers l' astre de nuit , qui le fuit à nouveau ...
il comprit alors , qu' il ne pourra jamais décrocher la lune , qu' il lui faudra , pour toujours , la partager avec les autres .
en attendant , il planait , au dessus de la forêt , et cette sensation que le vol lui procurait , le rendait heureux .
dans cette forêt , les animaux sont parfois surprenant ...