depuis une semaine , ou peut être deux , quelque chose a changé à la maison .
je dis une semaine ou deux , mais j' ai l' impression que ça fait depuis l' éternité tellement c' est horrible .
en plus , je ne sais pas vraiment ce que c' est .
personne ne sait .
ni moi , ni Marianne , ni François .
quand je leur ai demandé " mais qu' est ce qu' il a Frrintek ? " ils n' ont pas su me répondre .
Frrintek ?
a dit François , qui c' est ça , Frrintek ?
je veux dire Jasper .
ah , a soupiré mon père .
hum !
a fait ma mère .
Frrintek était dans sa chambre .
il ne pouvait pas nous entendre , alors j' ai insisté :
il est malade ?
Beueueueh , a fait mon père .
hum , a refait ma mère .
il va mourir , c' est ça ?
j' ai crié .
il est condamné par une maladie incurable que personne ne sait comment soigner et qui est plus grave qu' une fracture du crâne ?
François a éclaté de rire et Marianne aussi .
mes parents sont devenus fous .
il parait qu' on peut devenir fou d' angoisse , ou fou de chagrin , c' est Frrintek qui me l' a dit .
il parait qu' on peut aussi être fou de joie , mais Marianne et François , même s' ils sont parfois cruels , même s' il leur arrive de dire , " mais qu' est ce qu' on a fait au bon dieu pour avoir des monstres pareils ? " ne sont quand même pas assez méchants pour rire de la maladie mortelle d' un enfant .
je suis allé dans notre chambre .
Frrintek était sur son lit .
sans livre , sans rien .
je suis monté à l' échelle pour vérifier .
il avait les yeux fermés .
j' ai posé la main sur son front .
Frrintek ?
Frrintek , tu m' entends ?
qu' est ce que tu as ?
papa et maman sont devenus fous .
je veux dire , Marianne et François .
enfin Myriam et Hector .
ils rient dans la cuisine .
il n' a pas ouvert les paupières .
il n' a pas bougé d' un millimètre .
Frrintek , j' ai peur .
raconte moi une histoire .
je vais aller me mettre dans mon lit et on va faire comme si c' était la nuit , d' accord ?
tu vas me raconter une histoire .
je me suis allongé sur ma couette et j' ai attendu quelques minutes .
il ne se passait rien .
il y avait un grand silence dans la maison .
mes parents ne riaient plus .
Frrintek respirait à peine .
alors j' ai demandé :
tu as mal quelque part ?
pas de réponse .
tu dors ?
toujours pas de réponse .
tu veux que je te raconte une histoire ?
pas de réponse non plus .
mais , en un sens , ça m' arrangeait , parce que je n' aurais pas su quoi lui raconter .
moi , je ne suis pas comme Frrintek , je ne sais pas tout .
quand j' essaie de penser à quelque chose , de me concentrer , il ne se passe rien dans ma tête , un peu comme au temps des mites .
je n' arrive à penser qu' à nos secrets à Frrintek et à moi .
je pense à l' igloo en morceaux de sucres qu' on a construit ensemble sur la banquise .
à l' inventeur des fusées qui avait pris modèle sur les suppositoires mais n' osait pas le dire à l' académie des sciences .
je pense à nos chiens de traineau qui s' appelle Ours bouboule un , Ourse bouboule deux , Ourse Bouboule trois et ainsi de suite jusqu'à Ourse bouboule douze qui est le chef et la seule femelle .
je pense aux chaussons aux pommes , dont on avait trouvé la recette une année de famine et qu' on fabriquait , à l' origine , avec des vrais chaussons qui sentaient les pieds mais qui , une fois passés au four , grâce à un miracle chimique , avaient un gout de pomme .
je pense à l' aurore boréale que nos amis pingouins nous ont invités à regarder pour le jour de l' an et aux gaufrettes à l' eau de mer et au lait d' otarie qu' ils nous ont offertes en cadeau .
je pense que je vais peut être redoubler mon CP parce que depuis quelques temps Pascaline n' est pas très contente de moi et qu' elle a demandé à voir mes parents .
j' ai été convoqué chez la directrice et j' ai aussi vu la psychologue de l' école .
peut être qu' ils vont me renvoyer .
alors j' ai pleuré .
j' ai pleuré encore .
j' ai pleuré un peu plus fort .
mais Frrintek n' a rien dit .
il ne bougeait toujours pas .
ça faisait déjà plusieurs semaines , et même une éternité qu' il était comme ça .
qu' est ce que j' allais devenir si j' étais renvoyé de l' école , que mes parents étaient fous et que mon frère mourait ?
il fallait que je trouve une solution , une idée .
il fallait que je sauve mon frère .
une fois que Frrintek irait mieux , tout le reste suivrait .
même si j' étais renvoyé de l' école , il pourrait m' apprendre tout ce qu' il sait , et même si nos parents étaient fous , il pourrait m' élever , comme Ourse bouboule onze avait élevé ses dix frères et soeurs parce que sa mère , Ourse Bouboule douze était trop occupée à être le chef et qu' ils n' avaient pas de papa .
je me suis dit qu' il faudrait que je trouve un signe , un indice .
alors j' ai commencé à fouiller dans notre chambre .
j' ai ouvert les tiroirs de la commode .
j' ai sorti les chaussettes , les caleçons , les joggings .
j' ai retrouvé une figurine d' épouvantail .
ça m' a fait un tout petit peu plaisir .
j' ai pensé que c' était peut être un talisman , un objet magique , alors je l' ai frotté , j' ai soufflé dessus , j' ai récité une formule , mais ça n' a rien donné .
Frrintek était toujours immobile sur son lit .
j' ai sorti les livres de la bibliothèque , je les ai retournés .
une enveloppe est tombée de Mille et un contes sans queue ni tête .
elle portait le nom de mon frère : jasper Merigal , sept impasse du souffle d' or et encore au dessous , une série de chiffres et le nom d' une ville que je ne connaissais pas .
j' ai ouvert l' enveloppe et dedans il y avait une feuille pliée en deux avec un message écrit en grosse lettres : " mon COCO , C' est ton grand papi qui t' ecrit pour te souhaiter un bon anniversaire de deux ans . bisous . tu es mon arriere petit fils prefere . signe : grand papi prrintuk , roi DES ESQUIMEAUX . "
je n' avais jamais entendu parler de ce grand papi Prrintuk , roi des esquimaux .
c' était ça l' indice , le signe .
le secret de notre secret .
j' ai grimpé à l' échelle et j' ai secoué Frrintek .
Frrintek !
réveille toi .
j' ai trouvé la lettre .
Frrintek !
Frrintek a ouvert un oeil .
t' as fouillé dans mes affaires ?
il a ouvert l' autre oeil et s' est redressé sur son lit .
il avait l' air complètement normal tout à coup .
pas du tout mort .
pas du tout malade .
j' ai pas touché tes affaires , Frrintek .
arrête de m' appeler comme ça .
je m' appelle Jasper , OK ?
et j' en ai marre de ces trucs de bébés .
ces noms débiles et ce jeu débile .
t' avais pas le droit de fouiller .
cette lettre elle était pas pour toi , elle était que pour moi .
et mon cartable , c' est MON cartable .
tu mets pas les mains dedans !
j' ai pas touché à ton cartable , Frrint ...
heu , Jasper .
tiens , regarde .
je lui ai tendu son cartable , bien fermé .
il l' a pris , l' a ouvert et en a sorti une lettre .
une autre lettre , plus petite que celle du grand papi esquimau .
il l' a tenue trois secondes entre les mains et m' a dit .
pardon .
je me suis trompé .
pardon Grondouk .
tiens , tu peux la lire de toute façon .
il m' a tendu une jolie enveloppe rose avec une étoile dessinée dessus .
à l' intérieur , il y avait un papier vert plié en deux ou était écrit :
" ce n' et pas la pène de m' aimé ,
parse que moi , je ne t' aime pas .
signé : jacobine "
j' ai senti que c' était très important .
qu' il fallait que je comprenne très vite .
je devais absolument dire quelque chose à Frrintek , mais pour ça , il fallait que j' analyse la situation .
c' est une fille de ta classe ?
oui .
elle a un problème d' orthographe ?
non .
c' est ton amoureuse ?
il n' a pas répondu .
il s' est jeté contre son oreiller et s' est mis à pleurer .
c' était la première fois que je le voyais faire ça .
je me suis dit qu' il fallait que je demande de l' aide ailleurs .
peut être que mes parents se sentaient mieux maintenant .
peut être que leur crise de folie était passée .
j' ai laissé Frrintek tout seul Ça me faisait moins peur , maintenant que je savais qu' il n' était pas vraiment malade et je suis retourné voir mes parents à la cuisine .
qu' est ce qu' il a Jasper ?
vous êtes obligés de me le dire .
j' avais pris une voix spéciale , une voix un peu autoritaire .
il a des peines de coeur , a dit mon père .
un chagrin d' amour , a dit ma mère .
avec une fille ?
j' ai demandé .
mes parents m' ont regardé l' air étonné .
vous êtes sûr ?
j' ai insisté .
une fille ?
on ne s' est jamais intéressé aux filles .
ils ont hoché la tête , tous les deux .
le soir , après un diner où mon frère n' avait pas dit un mot et mangé deux raviolis sur douze , je me suis allongé sur mon lit et je lui ai raconté une histoire .