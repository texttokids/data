aussi loin que s' étend la mémoire de Samuel , c' est lui et son père dans les palais immenses et des nounous en série .
il ne se rappelle pas de sa mère qui est morte quand il était encore bébé .
Samuel ne sait pas comment ou pourquoi .
c' est un sujet tabou .
bien que pris par de grandes responsabilités , son père a toujours gardé du temps pour lui .
ils jouent de la musique ensemble , lui au piano , son père au violon .
ils passent au moins une heure par jour , pas exactement à parler , mais à faire de la conversation .
il y a trop de mots qu' ils ne veulent pas prononcer , trop de questions qu' ils n' ont pas le courage de poser , un équilibre qu' ils ne veulent pas secouer .
la paralysie et la peur de la vérité empêchent la parole .
même si son père a des diners diplomatiques , il reste à table avec Samuel avant de partir pour que son fils n' ait pas à manger seul .
cela l' arrange qu' Annabelle passe de plus en plus de temps chez eux et Samuel chez elle .
Sidney Shelbert n' aurait pas pu rêver mieux pour sa mission en France .
c' était son idée d' inscrire son fils à l' école publique après une scolarité privée , comme lui , aussi fils de diplomate .
Samuel , au début de son adolescence , lui causait des soucis à cause de sa solitude et de son renfermement .
il n' aimait pas l' école huppée du dernier poste au Qatar , préférant s' emmurer dans l' ambassade en dialoguant avec son piano .
de pays en pays , à la remorque de son père , Samuel ne savait pas dire où était sa maison .
certes ils passaient quelque semaines tous les étés dans leur résidence au Cape Cod et il se sentait bien là bas entre les photos de sa mère et un père plus décontracté , sans cravate , à faire de la voile , sur la plage et même à faire la cuisine comme des grands chefs .
son père sort avec des femmes élégantes , souvent intelligentes , des fois carrément sympathiques .
parfois elles passent la nuit ou c' est lui qui découche , mais rien ne dure .
le grand amour de sa vie , c' est Samuel .
le grand projet de sa vie , c' est Samuel aussi .
pour Samuel tout cet amour et ces espérances sont lourds à porter .
il ne se sent pas à la hauteur des ambitions de son père et surtout en France où il ne peut à peine demander où sont les toilettes .
il ne sait pas que la seule véritable ambition de son père pour lui est son bonheur , ou ce que son père estime être son bonheur .
dans les ambassades immenses il y a des bureaux , du personnel , des couloirs sans fin , des salons innombrables , des jardins impeccables , et Samuel doit se frayer un chemin vers son île , son chez soi au milieu du brouhaha .
dès fois , ils ont des hôtes , des vedettes américaines , des politiciens , des écrivains , des artistes et même le président des États Unis .
Annabelle a un laisser passer , mais il faut qu' elle soit vérifiée par les services de sécurité à chaque fois .
Samuel préfère étudier chez elle ou chez son père et son frère , deux êtres souriants et bien dans leur peau qu' il a rencontrés la semaine d' avant .
Anatole joue au piano aussi et ils aiment faire les clowns au clavier .
l' amitié fut immédiate .
Annabelle est violoncelliste .
elle embarque Samuel pour passer le chapeau quand elle joue flute et violoncelle avec Joséphine , place des Vosges .
ils ont gagné quatre vingt treize euros .
Samuel est heureux de se rendre utile .
et il n' a pas besoin de parler : il tend le chapeau et les gens comprennent .
tant de mots So many words