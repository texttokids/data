vers le mois de mai , nous étions devenus ce que les directrices et les directeurs d' école nomment avec horreur et mépris " un petit groupe " .
j' ignore pourquoi les petits groupes représentent un tel danger pour l' éducation nationale , mais j' ai remarqué à plusieurs reprises que l' ambition du personnel éducatif est systématiquement de les rompre en séparant les amis .
pour briser le nôtre , il aurait fallu quatre classes par niveau , or il n' y en a jamais eu que deux .
une année , ils ont réussi à isoler Fleur en CM un a , alors que Mon Commandant , Allison et moi étions en CM un B , une erreur dont l' école Curie n' est pas encore remise .
nous avons , bien sûr tenté de faire semblant de ne plus être amis , dans l' espoir de nous retrouver dans la même classe , mais la directrice n' était pas dupe .
quoiqu' il en soit , rien n' aurait pu nous séparer et , jusqu'à aujourd'hui , jusqu'à cette année de seconde où nous sommes de nouveau dans la même classe , j' avais cru que rien de nous séparerait jamais .
et pourtant , durant les trois jours que j' ai passés enfermée chez moi à frôler l' overdose de télé , seule Fleur est venue me rendre visite .
elle m' a photocopié les cours importants et m' a préparé une tarte à la rhubarbe pour que je connaisse ce gout avant de mourir , au cas où .
je ne peux pas manger ça , lui ai je dit .
c' est tout gluant , on dirait du céleri et ça sent l' acide .
mais imagine que tu meures .
il y a quand même certaines expériences que tu regretteras de ne pas avoir vécues .
pourquoi veux tu que je meure , j' ai même plus de fièvre .
je ne veux pas que tu meures .
je veux juste que tu goutes la meilleure tarte du monde .
fleur est comme ça , très gourmande et très têtue .
la nourriture est au premier plan de ses préoccupations , à égalité avec la hauteur des chaussettes ( jusqu' au genou ou plus , mi mollet ou cheville , c' est poubelle ) .
toi , si tu allais mourir , lui ai je demandé , tu chercherais quel plat il faut absolument que tu goutes avant d' y passer ?
non .
j' étais soulagée .
tu es comme tout le monde , finalement .
tu voudrais connaître le grand amour , passer une folle nuit de sexe et , éventuellement , avoir un enfant pour voir l' effet que ça fait de se faire appeler " maman " .
non .
ben alors quoi ?
ben alors je n' aurais pas besoin de chercher , pour le plat , parce que j' y ai déjà réfléchi .
j' ai même fait un genre de testament en cas d' AVC .
parce que , tu comprends , si je perds l' usage de la parole et que je m' achemine vers une mort certaine , j' aimerais que mes proches sachent à quoi s' en tenir .
et alors ?
steak tartare .
c' est révulsant , ça risquerait même de hâter mon trépas , mais j' y tiens .
" hâter mon trépas " , comment tu parles ?
c' est à cause de la mort .
quand je parle de la mort , mon vocabulaire s' élève .
c' est bizarre .
j' ai donc reçu la visite de Fleur et de sa merveilleuse tarte au crachat de martien , mais aucune nouvelle de Mon Commandant , ni d' Allison .
je n' arrivais pas à croire que Mon Commandant puisse être encore fâché à cause de cette histoire de playmobil pédé .
ça ne lui ressemblait pas .
quant à Allison , je l' imaginais dans sa lune de miel avec Liouba : place réservée à côté d' elle , échange de numéros de portable , de crayons de maquillage , de tee shirts .
j' ai demandé à Fleur de me faire un rapport circonstancié des activités scolaires et extrascolaires en mon absence , mais elle est tellement confuse que ça n' a pas donné grand chose .
un peu comme les cours qu' elle a photocopiés pour moi .
son écriture est illisible et tout est mélangé sur sa feuille .
ça donne à peu près ça :
l' argumentation est un système mis en place dans le but de persuader , de convaincre l' interlocuteur , le lecteur .
comment reconnaitre un texte argumentatif ?
caractéristiques et mots clés :
Michèle , ma belle sont des mots qui vont très bien ensemble .
les mots , les maux , Momo .
mon gâteau préféré : le Paris Brest
types d' arguments :
les rapports de cause à effet .
tel phénomène entraine tel autre selon le postulat du déterminisme .
ex : " fumer entraine des troubles gastriques , donne mauvaise haleine et perturbe l' odorat comme le gout " .
oui , mais ça rend cool .
j' ai l' air cool quand je fume .
je fume pour de faux .
ma mère fume trop , je la déteste .
l' ironie : l' ironie est une argumentation par l' absurde , qui tente de séduire le lecteur en faisant appel à son intelligence .
système de connivence .
risque d' être pris au premier degré .
récepteur complice , mais danger de malentendu .
allô , y a quelqu' un dans le tuyau ?