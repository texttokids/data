quinze .
gai !
marions nous
" qu' est ce qu' on va bien pouvoir se mettre ? " se demandaient tous les membres de la famille à court d' idées .
et voilà que Mia a fait livrer la veille une tenue de mariage pour chacun d' eux .
cinq paires de jeans et cinq chemises jaune fluo imprimées de deux noms : Mia et Maximilien .
l' Invitation est aussi simple : " rendez vous à la plage Beau rivage à onze heures . "
marie se débrouille plutôt bien avec son plâtre .
elle n' a pas fumé depuis son accident .
à la place , elle couvre les pages de son cahier blanc , de la main gauche .
elle devient de plus en plus ambidextre .
les jeans auraient été des robes féeriques en taffetas et en dentelle , des smokings avec noeuds papillons , les Blanc n' auraient pas été plus heureux .
c' est plus commode pour se rendre en tram au mariage mystère .
ça va être un pique nique , je le sens , dit Pablo .
ma soeur a peut être enfin opté pour la simplicité .
n' importe comment , à la fin de la journée , elle sera Madame Osterwalder , dit Bernard .
quatre syllabes pour une seule épouse , dit Charles .
attendons de voir , dit Angélique .
assez discuté , assez pronostiqué , des actes !
Mia est à la plage dans un jean blanc avec Maximilien également en blanc .
les deux familles se saluent .
albertine rejoint le groupe avec Georges .
ils voient les pédalos alignés sur la plage .
ils voient le photographe .
chacun a droit à un paquet surprise avec l' instruction formelle de ne pas l' ouvrir pour l' instant .
bon , dit Maximilien , vous prenez place dans les pédalos , nous prenons Pablo avec nous .
vous voyez ?
podolo pour honorer mon métier de podologue .
peaudalo pour honorer celui de Mia , dermatologue .
on va pédaler jusqu' au bateau que vous voyez là bas au loin .
les invités obtempèrent et montent comme les futurs mariés dans leurs embarcations respectives , dans l' excitation et l' hilarité générale .
personne ne s' attendait à ça .
et personne ne s' attendait à l' orage , à mi chemin entre le rivage et le navire .
ils arrivent trempés et épuisés , n' osant songer à un éventuel retour .
une fois à bord du luxueux yacht loué pour la journée , ils reçoivent d' autres instructions : ouvrir les paquets et aller dans leurs cabines se changer .
pour tous , des chemises sèches et habillées , de toutes les couleurs .
la pluie était elle prévue au programme ?
ils ont droit à une boisson fraiche pour se remettre de leurs efforts , après quoi ils suivent les mariés jusque devant le capitaine , pour la cérémonie .
là , pas de surprise : les deux mariés se disent oui .
Pablo leur tend leurs alliances , pénétré de l' importance de son rôle .
le déjeuner est élégant et informel .
délicieux !
tout le monde fait un discours ( spontané , Dieu merci , on ne lit pas de papiers ) pour exprimer sa joie .
Mia parle en dernier .
elle ne prononce qu' une phrase , attendrie , les yeux fixés sur sa soeur , et se tenant le ventre :
eh bien ...
c' est des jumelles .