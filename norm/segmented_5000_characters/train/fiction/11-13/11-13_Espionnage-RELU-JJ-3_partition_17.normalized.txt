cette fois Marie ne boude pas .
elle s' applique à garder bonne contenance , déterminée à approfondir la question du comportement de sa fille , mais c' est délicat car elle est censée ne rien savoir .
et sa fille a l' air tout à fait normale , pas la moindre séquelle de débauche charnelle dans son attitude .
au gouter , Marie pose la question la plus assommante que puisse poser une mère .
elle l' essaie d' abord sur Pablo , pour s' entraîner .
elle a acheté une tarte aux mirabelles qu' elle découpe avec entrain .
qu' est ce que tu as fait aujourd'hui , mon chou fleur au chocolat ?
j' ai fait semblant d' écouter la maîtresse qui nous a fait lire à chacun le même paragraphe .
quand ça a été mon tour , j' ai essayé de le faire aussi mal que les autres .
mais pourquoi ?

à Rome , on doit vivre comme les Romains .
sous le coup de l' hilarité , Charles et Angélique crachent leurs mirabelles .
et toi , mon Charles chérubin ?
j' ai embrassé ma bien aimée entre chaque cours .
c' est interdit !
dit Pablo .
est ce parce que nous t' avons toujours parlé comme à un adulte que tu vires au vieux con ?
demande Charles à Pablo .
l' entourage est contagieux .
et toi , mon Angélique , mon ange , ma fille qui tranche sur la grossièreté des garçons ?
oh , moi , la routine ...
rien que la routine .
sauf qu' une bonne femme est venue en classe nous parler de la contraception .
et toi , maman ?
je n' ai pas eu le temps de fumer .
bonne nouvelle .
à part ça ?
des soucis .
je n' ai le temps que de me faire du souci .
à propos de quoi ?
de tout .
de mes enfants , de mon mari , de ma mère , de ma soeur et de ce grand mariage ...
ça , ce n' est pas un souci , c' est une joie .
ils viennent tous diner samedi soir .
tu t' inquiètes du menu ?
non .
Mia a commandé un couscous pour vingt .
alors que nous serons huit !
elle veut que chacun de nous invite quelqu' un de son choix .
moi , j' invite Angélique et Charles , dit Pablo .
nous sommes déjà comptés .
alors j' invite ma maîtresse .
je ne pense pas que ça puisse se faire , dit Marie .
si tous les enfants invitaient la maîtresse , elle deviendrait folle .
elle l' est déjà .
tu n' as personne d' autre ?
si , Alice .
va pour Alice .
et toi , Charles ?
ce sera une invitée surprise .
on la verra enfin !
s' exclame Angélique .
oui , et je te prie de te montrer civilisée .
parce que , d' habitude , je suis une sauvage ?
parce qu' on n' est pas toujours maître de ses réactions .
tu crois que la vue de cette personne va me rendre méchante ?
c' est une éventualité .
j' ai déjà l' eau à la bouche , dit Angélique .
et toi , mon ange ?
je ne sais pas encore .
moi aussi , ce sera un ou une invité deux noeuds surprise , et j' espère que mon frère ainé se contrôlera .
et toi , maman , qui tu vas inviter ?
ma directrice .
elle est veuve et tellement seule .
mais tu ne l' aimes pas !
j' ai pitié d' elle .
et mamie ?
surprise aussi .
et papa ?
pas son patron , j' espère .
sa maîtresse , peut être , blague Marie .
pendant tout le gouter , Marie fixe sa fille en essayant de détecter sur son visage et dans ses gestes les symptômes d' un changement .
l' ambiance faussement joviale se poursuit au diner , sans Bernard qui a téléphoné qu' il rentrerait tard .
au menu : des pâtes , car c' est Charles qui a fait le diner .
il est d' excellente humeur .
il a repris des leçons de conduite et le moniteur lui a certifié qu' il n' aurait plus aucun problème .
après rangement de la cuisine , Angélique voit que sa mère s' apprête à sortir au jardin fumer autant de cigarettes qu' il faudra pour compenser sa journée sans .
elle l' embrasse et monte faire ses devoirs .
elle essaie d' imaginer l' invitée surprise de son frère et de désole de n' avoir elle même aucune idée de qui inviter .
quelle tragédie de ne plus avoir de meilleure amie , de ne pas être capable de se trouver un mec , de vivre par imagination et non pas réellement !

neuf .
les invités potentiels