cher cahier rose , un évènement digne d' un journal intime a eu lieu aujourd'hui .
la prof de français a lu mon dernier devoir à toute la classe .
( il faut dire que ma mère me relit toujours et corrige mes fautes !
) au lieu de m' attirer la haine des élèves , j' ai récolté plutôt de la bienveillance .
plusieurs sont venus me dire : " c' était super ! " ou " ça m' a vraiment amusé " .
si le fait d' écrire peut vous valoir un peu d' amour , je devrais peut être envisager ça comme futur métier , bien que papa dise que ça ne rapporte rien .
mais le top du top , c' est qu' Arthur lui même , avec la totalité de son mètre quatre vingt huit de chair et d' os ( et peut être de cerveau ) a daigné lâcher un moment la Brenda maudite pour joindre ses pas aériens aux miens ( de plomb ) .
il m' a regardée droit dans les yeux et m' a dit : " ce n' est pas facile , d' avoir de l' humour . c' est un don . " c' était l' heure du déjeuner .
il m' a suivie à la cafétéria , a fait la queue avec moi et s' est assis à ma table à côté de moi , sa cuisse contre la mienne .
il ne semblait pas gêné du tout .
moi j' étais en feu .
j' ai pivoté vers lui et mon sein gauche a effleuré le haut de son bras .
ce contact a déclenché une réaction instantanée en bas de mon corps , et de la chair de poule jusqu' aux cheveux .
chacune de mes cellules a fondu comme une soupe de chocolat chaud , sucrée à point .
j' étais incapable d' émettre un mot et encore moins de faire entrer une fourchette dans ma bouche .
j' ai dû oublier mon nom .
mon seul désir était de me glisser sous sa peau .
est ce un but dans la vie , pour une fille dont les ambitions sont en principe d' avoir des " très bien avec félicitations " ?
Arthur , démontrant qu' il n' est pas occupé uniquement de sentiments vagues , mais qu' il est un véritable athlète cérébral , s' est mis à commenter le contenu de nos assiettes : purée en poudre , steak haché graisseux , pas de légumes verts à l' horizon .
il a abordé le problème de l' obésité .
à vrai dire , je suis loin d' être obèse , sauf à l' endroit déjà mentionné .
mais existe t il quelqu' un qui ne se sente pas gros ?
même tante Mia se sent grosse .
maman , elle , n' arrête pas de fumer , de peur de grossir .
j' ai eu du mal à donner la réplique à Arthur , tellement j' étais intimidée .
mais corporellement j' osais .
mon sein restait cimenté à lui .
il parlait , je ne respirais pas .
et puis , ça a été l' heure de se lever de table .
j' aurais tout donné pour rester là , soeur siamoise éternelle .
je suppose que c' est l' occasion de servir le cliché : toute bonne chose a une fin .
pourquoi ?
je n' arrêtais pas de chanter dans ma tête What do un have to do to make him love me ?
What do un have to do to make him care ?
et j' ai décidé d' appeler tante Mia , séductrice professionnelle , pour avoir son avis sur la question .
j' ai téléphoné à son bureau et l' ai eue du premier coup .
elle travaille beaucoup et est presque toujours là .
écoute Tata , je suis effroyablement accro à un gars du lycée et je voudrais savoir comment le mettre dans le même état que moi à mon égard .
qu' est ce que je dois faire ?
Get physical !
réplique Tata Mia sans hésitation .
tu veux dire , je lui saute dessus ?
je veux dire , prends lui la main , le bras , entre en contact comme tu peux .
mais justement , je ne peux pas !
sers toi de cette poitrine pour laquelle je donnerais tous mes miles bonus .
mets la en valeur , au lieu de la cacher sous tes pulls immondes .
maintenant je te laisse , petit pois adoré .
j' ai une réunion .
tiens moi au courant .