tout part en ...
couilles , si j' ose dire .
et j' ose le dire .
ce genre de vocabulaire grossier est maintenant mon droit le plus absolu de mâle .
sérieusement , je ne sais pas si je vais tenir longtemps .
au bahut , pour commencer .
Karen n' arrête pas de me draguer .
j' ai encore trop de Pauline en moi pour ne pas m' en dépêtrer avec brio , mais quand même : quelle galère .
je ne parle pas d' Oliver , que j' aime encore en tant que séquelle de Pauline , mais pas du tout du même genre d' amour en tant que récent Paul à zigounette .
my God , comment m' en sortir ?
je ne veux froisser personne , mais pour l' instant mon cerveau n' est pas en adéquation avec mon corps .
vais je toujours conserver cette sorte de dualité , ou bien Pauline va t elle peu à peu s' estomper en moi au profit du soi disant cousin ?
mystère .
l' hologramme de mon père n' a rien révélé à ce sujet .
et d' abord , est ce que mon père connaissait toute la vérité sur ma condition ...
et sur la sienne ?
je me tourne et me retourne dans mon lit .
cela plus de deux heures que le sommeil me fuit .
je suis là à contempler le plafond de ma chambre rose va falloir vraiment faire quelque chose avec cette satanée couleur l' esprit torturé par ces centaines de questions qui m' arrivent en vagues successives pour venir s' écraser sur la plage déserte de mes incertitudes .
belle image pour signifier que je suis complètement paumé .
autre sujet d' inquiétude : mes pouvoirs .
quels sont ils vraiment ?
comment vais je les utiliser ?
j' ai une force hors du commun lorsque je le veux .
déjà , en fille , je balançais les garçons à l' autre bout du couloir : alors en garçon ...
le prof de gym n' en revient pas de la façon dont je cours , grimpe , saute .
je dois me surveiller , pour qu' il ne pense pas avoir découvert le nouveau Superman .
il y a au moins un avantage à ça , c' est que personne ne vient me chercher des crosses .
ma réputation de costaud a vite fait le tour du lycée .
bon , ça , à la rigueur , je gère .
mais mes autres pouvoirs , comment vont ils me tomber dessus ?
par hasard ?
comme cette mémoire d' éléphant , que je me suis découverte subitement la semaine dernière ?
tout à coup , ce matin là , alors que je végétais tranquillement en classe , l' esprit ailleurs , mes yeux se sont fixés sur le tableau où chaque élément chimique affiche sa masse atomique .
ça commence par l' hydrogène avec un et cela se termine par l' hassium avec deux cent soixante dix sept .
les trois quarts de ces noms sont des noms barbares impossibles à retenir .
quinze secondes après , je connaissais par coeur l' ensemble du tableau .
le choc d' étonnement a été rude .
c' était comme si , d' un coup , je venais de mettre en place un DVD enregistreur qui aurait avalé tout ce que je voyais , lisais , entendais , écrivais .
maintenant je me suis habitué à cette mémoire d' éléphant et j' y trouve des avantages , surtout dans le cadre de mes études .
je suis devenu imbattable dans les contrôles au point de susciter de nouvelles jalousies .
mais cela n' ôte rien à ma trouille de me voir me transformer au jour le jour en une espèce de monstre incapable de prévoir d' où va venir le coup suivant .
je regarde le radio réveil .
encore un petit quart d' heure avant qu' il se déclenche pour sonner l' heure du lever .
du coup , je ferme les yeux et tombe dans un profond sommeil ...
d' un quart d' heure tout juste .
au bout de quoi un morceau des Radiohead me tire du néant dans lequel j' étais plongé .
où suis je ?
ça met du temps à me revenir .
en tout cas , c' est le matin , et il faut que je me lève d' un bond pour me rendre à ce lycée qui prend la tête .
d' un bond , facile à dire .
il y a un handicap .
ce dernier petit somme m' a transformé en une sorte de chapiteau de cirque , moyennant un gros mât matinal bien raide et bien dressé qui soulève mon drap vers le plafond .
je dois me faire une raison .
ce genre de prodige un peu invalidant fait partie du packaging des garçons !
sur le chemin du bahut , je continue à cogiter .
cette mission dont a parlé mon père , quelle est elle ?
dois je me confectionner un costume de Spiderman et défendre les faibles et les damnés de la Terre ?
tout s' embrouille dans ma cervelle .
finalement , je décide de ne pas me torturer à l' avance , et d' attendre la suite des évènements .
pour couronner le tout , la journée débute par un contrôle de mathématique puis par un de physique .
à croire que les professeurs ont décidé de nous torturer tous le même jour .
je m' en sors haut la main , grâce à ma mémoire totale , mais n' en tire ni satisfaction ni gloire .
à midi , à voir la tête de mes camarades , je comprends tout de suite que pour eux , ce n' est pas la joie .
j' avais pourtant révisé hier soir , se lamente Oliver .
je me suis planté en maths comme en physique , j' ai séché comme une sardine en plein soleil .
je sens que ça va faire deux mauvaises notes à mon tableau de chasse .
mon père va me priver de voiture pendant au moins une semaine .
cela me fait penser que j' ai bientôt seize ans : il faut que je demande à ma mère qu' elle envisage de me prêter sa voiture , maintenant que j' ai l' âge pour la conduire .
au self , Karen accourt pour venir manger avec moi .
je surprends les regards de satisfaction qu' elle lance en direction de ses copines qui n' arrêtent pas de nous mater .
nous parlons de choses et d' autres .
enfin , je devrais dire : Karen parle de choses et d' autres .
un vrai moulin à paroles .
à la fin du repas , elle se lève , prend son plateau , contourne la table et vient me glisser à l' oreille :
ce soir , à la sortie de l' école , reste près des vestiaires , j' ai quelque chose à te montrer ...
puis elle me jette son regard le plus mystérieux , qu' elle accompagne d' un sourire énigmatique .
mon coeur se met à battre la chamade .
et si elle avait découvert ma situation ?
du coup , les cours de l' après midi me semblent interminables .
arrive enfin leur fin .
je lambine , alors que le gros de la troupe se précipite vers la sortie du lycée .
près des vestiaires , j' aperçois Karen , tout sourire .
qu' est ce que tu veux me montrer ?
tu vas voir , dit elle en me prenant par la main
elle m' entraîne dans la cour .
nous contournons le gymnase .
sans hésiter , elle m' entraîne vers le massif d' arbustes qui décore le fond du parc du lycée .
nous buttons contre une clôture .
avant que j' ouvre la bouche pour demander ce qu' on fait là , Karen tire sur un bout de grillage qui cède sans difficulté .
que penses tu de cette sortie bis ?
demande t elle d' un air malicieux .
pratique pour sécher certains cours , je réponds .
mais qu' est ce que tu voulais me montrer ?
ceci .
sans crier gare , elle prend ma main , la plaque contre un de ses seins , puis m' embrasse passionnément .
surpris et mal à l' aise , je ne lui rends pas son baiser et laisse ma main inerte .
Karen ne manque pas de s' en apercevoir :
je ne te plais pas , ou t' as jamais embrassé une fille ?
interroge t elle avec une pointe de défi .
et si la première réponse était la bonne ?
son visage se liquéfie .
elle perd toute sa morgue , toute son assurance .
paul , je t' aime , murmure t elle , les larmes au bord des yeux .
je m' en veux de ma brutalité .
le Paul qui est en moi , sans doute à cause de Pauline qui était son amie , ne trouve pas Karen désagréable , loin de là .
je la serre dans mes bras et lui souffle un mensonge :
laisse moi du temps , je t' en prie .
c' est un peu compliqué pour moi , en ce moment .
j' ai laissé un grand amour en Angleterre .
avec mes pouces , je sèche les deux larmes qui s' échappent de ses deux beaux yeux verts .
puis , tendrement , je dépose un baiser sur ses lèvres .
je comprends , dit elle soulagée .
je n' en suis pas si sûr .
moi même , j' ai beaucoup de mal à m' y retrouver dans mon comportement .
mais je suis content de l' avoir embrassée .
je peux même dire que j' en avais envie .
comme si rien ne s' était passé , nous franchissons la porte secrète et remettons le grillage en place , puis nous nous séparons pour rejoindre chacun son chez soi .
le coeur soudain plus léger , je me mets à siffloter .
cet accès de gaieté va tourner court .