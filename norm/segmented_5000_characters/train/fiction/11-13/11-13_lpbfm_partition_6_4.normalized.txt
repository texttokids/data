je ne sais pas comment Fleur arrive à passer chaque année dans la classe supérieure .
c' est un tel fouillis dans sa tête .
je crois que c' est à cause de sa frange .
je veux dire , c' est grâce à sa frange qu' elle finit toujours par s' en sortir .
sa frange est absolument parfaite , coupée très nette juste au dessus des sourcils , bien lisse , bien lourde , très convaincante , la frange , dans l' argumentation , ça compte aussi .
à chaque conseil de classe du troisième trimestre elle est convoquée .
tout le monde est d' accord pour dire qu' elle n' a pas le niveau , mais quand elle entre dans la salle , avec sa frange impeccable et son col en V sur sa chemisette blanche ( j' ai oublié de préciser que Fleur va au lycée en uniforme , comme une anglaise ) , les profs craquent .
elle a tellement l' air d' une bonne élève qu' on ne peut pas la faire redoubler .
avant de refermer le dossier de mes trois jours de convalescence , je me dis qu' il faudrait aussi , pendant que j' y suis , que je dise un mot du physique d' Allison et de Mon Commandant .
quand on décrit un personnage , je ne sais pas pourquoi , on a l' impression qu' il existe davantage .
c' est comme s' il pouvait se mettre à marcher , sortir des pages et se mêler aux vraies personnes .
vous me direz que Mon Commandant et Allison ne sont pas des personnages puisque ce sont mes amis .
dans la vie , oui , mais dans mon livre non .
dans mon livre , ce sont des personnages .
c' est la règle , ils sont comme aplatis entre les pages .
je vais donc leur donner un peu de substance avant de poursuivre .
mon Commandant est un garçon , donc beaucoup plus difficile à décrire qu' une fille .
c' est le plus petit de nous quatre .
il doit mesurer un mètre soixante deux .
il a des cheveux raides et bruns qui tiennent en l' air tout seuls , sans gel , sans rien , parce qu' il a énormément d' épis .
il a des taches de rousseur qu' il déteste , mais que moi j' aime bien , de très longs cils qui lui donnent l' air doux et des bagues aux dents qui le font un peu zozoter .
en relisant , je me rends compte que j' en ai fait un monstre alors que ce n' était pas du tout mon intention .
dans l' ensemble , il est plutôt mignon et surtout , j' aime bien sa façon de s' habiller avec des affaires un peu trop serrées , manches trop courtes , pantalons feu de plancher , vestes étriquées , on dirait Gavroche , dans l' album illustré des Misérables que j' avais quand j' étais petite .
il a deux petites soeurs jumelles de deux ans qu' il appelle les Bouboules .
il est très fort en maths et en histoire , nul dans les autres matières .
Allison ...
Allison , je lui en veux .
j' ai peur de ne pas être objective , mais je vais quand même essayer de faire son portrait .
c' est la plus grande de la bande .
elle est très mince , métisse , avec des yeux bleus .
elle a été adoptée quand elle avait trois ans et ne se souvient plus de rien avant ça .
elle adore un peu trop ses parents .
parfois , j' ai l' impression qu' elle se force .
sa mère est pompier et son père est éclairagiste sur des comédies musicales .
c' est vrai qu' ils sont sympas .
il y a toujours de la musique chez eux .
Allison danse hyper bien .
elle dit " c' est mon sang de noire " avec un accent africain génial .
Allison est la plus forte en classe du groupe .
elle veut devenir chirurgienne .
ça m' énerve parce que , à force de la décrire , j' ai envie de la voir .
tout à coup , elle me manque alors qu' elle nous a trahis .
elle est passée dans le camp de l' ennemi et d' après ce que m' a dit Fleur , ça ne s' est pas arrangé pendant mon absence .
Allison est allée au café avec Liouba , elles ont été vues au square , une rumeur court selon laquelle elles seraient allées au cinéma mardi soir .
et avec toi , elle est comment ?
ai je demandé à Fleur .
normale .
comment ça , normale ?
d' habitude c' est à côté de toi qu' elle s' assoit .
c' est avec toi qu' elle va au café .
elle s' est dédoublée ou quoi .
non .
parfois elle se met à côté de moi .
et quand elle est à côté de toi , c' est qui qui est à côté de Liouba ?
ben ...
ben quoi ?
fleur a passé la main sous sa frange , comme pour essuyer la sueur sur son front .
Étienne aussi ...
parfois , Étienne ...
qui c' est ça , Étienne ?
fleur n' a pas eu besoin de répondre .
il me restait suffisamment de neurones pour me souvenir que c' était le prénom de Mon Commandant .