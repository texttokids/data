angélique essaie d' oublier cette journée de misère .
le diner familial est glauque .
elle n' ouvre pas la bouche et quand qui que ce soit lui adresse la parole , elle entend Pablo dire : " laisse la tranquille , elle est en crise d' adolescence . " cet enfant là , pense Angélique , nous ferait comprendre ce que veut dire l' amour .
Charles se montre compatissant , lui aussi : il connait ces journées sombres et létales .
seuls sa mère et son père essaient de lui extirper quelques paroles , qu' elle se refuse à lâcher .
elle peut quand même débarrasser la table , ranger la vaisselle dans la machine bénie et s' enfermer dans la salle de bains avant la horde sauvage .
elle étale les crèmes que sa tante lui fournit en disant : " il n' est jamais trop tôt pour commencer . " et puis vient le meilleur moment , celui où elle se retrouve seule en compagnie de son journal intime , à naviguer dans ses pensées , espérant les petites révélations qu' elle se cacherait à elle même .
elle est reconnaissante à sa mère de l' avoir initiée à ce plaisir insoupçonné , de lui avoir acheté ce cahier rose à lignes horizontales sur ses feuillets blancs blancs comme est blanche la dénommée Angélique Blanc .
malheureusement , écrire n' est pas simplement écrire , mais aussi lire et se relire .
angélique commence toujours par relire ce qu' elle a écrit la veille .
cet exercice lui procure une gêne .
une chance que tout ça soit confidentiel .
elle ouvre le cahier à la page où elle ne parle que de ses seins et note avec honte que ça tourne à l' obsession .
dans la vie , on peut se mentir à soi même , mais on ne peut pas mentir au papier .
tout sort , noir sur blanc .
en tournant les pages , Angélique aperçoit de petites particules de cendres dans le pli des pages .
elle feuillette et refeuillette , elle trouve des cendres partout .
phénomène étrange .
elle referme le cahier et se met au lit , mal à l' aise .
une voix lui dit " vas y ! même si tu n' as rien à dire , tu peux quand même laisser courir ton stylo sur la feuille . " alors elle se relève et voici ce qu' elle fait : des gribouillages , des boucles comme à la maternelle , en travers de trois des pages de son journal .
car voici la vérité .
voici l' explication des choses .
voici la transposition parfaite de ce qui lui passe par la tête :
Oooiiyyyxxmmmppp
cinq .
une vie qui recommence tous les jours
elle se sent mieux , se remet au lit , se tourne et se retourne pendant une heure avant que le sommeil l' attrape dans son filet de rêves .
elle ne rêve pas d' Arthur mais de Maximilien Osterwalder jouant avec ses pieds .
le matin arrive , avec son dépôt habituel d' espoir .
elle s' habille à mi chemin entre son ancien et son nouveau soi même .
il commence à faire froid et elle enfile son unique pull en cachemire , rebut d' une Tante Mia à poitrine plate .
cette fois elle n' a pas " oublié " le soutien gorge .
elle sait , au fond d' elle même , que cette journée va être nouvelle et inhabituelle .