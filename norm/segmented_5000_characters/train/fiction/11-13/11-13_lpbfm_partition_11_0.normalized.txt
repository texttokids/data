je n' avais pas de plan précis .
je suis arrivé en retard au lycée parce que ma mère tenait à ce qu' on ait une discussion sérieuse au petit déjeuner .
elle avait écrit un mail à mon père pour organiser un déjeuner entre nous .
" comment ça ,' entre nous' ? "
" lui et toi , toi et lui , vous quoi . "
" seuls ? "
" tu veux inviter quelqu' un d' autre ? "
" ben , je ne sais pas . ça fait quand même cent ans que je ne l' ai pas vu . je ne me souviens même plus de sa tête . "
" la même que toi , en homme . "
" et je devrai l' appeler Papa ? "
" tu l' appelleras comme tu veux , ma chérie . ce que je sais , c' est que moi , je ne peux plus gérer ça toute seule . "
" gérer quoi ? "
" ton adolescence . ça m' épuise . "
" mais , comment tu peux dire ça ? je suis parfaite . "
ce n' était peut être pas exactement le bon mot , mais c' était celui qui m' était venu à l' esprit tout naturellement .
ma mère a tellement ri qu' elle a craché du café par les narines .
de son point de vue , j' étais peut être parfaite , mais très difficile à vivre ces derniers temps .
je posais des questions étranges .
j' étais distraite , lointaine , peu fiable .
je diffusais une sorte de mauvaise humeur poisseuse .
et là , on arrive à un point très délicat du roman : c' est quand un des personnages accuse le narrateur .
si le narrateur se défend , il peut donner l' impression d' être prétentieux , de s' envoyer des fleurs .
s' il ne fait rien , le lecteur ne sait plus à quoi s' en tenir .
étais je celle que je pensais être , ou ressemblais je au portrait que ma mère faisait de moi ?
à quel point est on capable de se voir , de s' étudier , de se comprendre ?
à la suite de cette discussion , j' ai décidé de me traiter comme un de mes personnages , de prendre de la distance , de m' examiner avec calme et humour , mais c' était extrêmement difficile .
une petite voix en moi n' arrêtait pas de répéter : " ma mère est une connasse , je la déteste " et ça m' empêchait de me concentrer .
l' idée de déjeuner avec mon père me donnait l' impression d' avoir été transplantée de force dans une série américaine .
on allait se dire les choses , se retrouver , et je redeviendrais celle que je n' aurais jamais dû cesser d' être .
il me dirait les mots justes , je pleurerais peut être un peu et il saurait me consoler en posant sa large main rassurante sur mon épaule .
pouah !
j' en avais des frissons d' horreur .
arrivée en salle cent cinq , juste avant que Madame Pitrebeau ne referme la porte , je me suis souvenue qu' on avait un contrôle de maths .
j' avais tenté de devenir une hyper bonne élève la veille , mais j' avais oublié que pour y parvenir il vaut mieux commencer par le travail urgent plutôt que de se précipiter sur ce qui peut attendre .
la seule chaise libre était à côté de Fleur , la plus grande tache en maths de l' univers qui , je vous le rappelle est en expansion ( c' est dire , si Fleur est nulle !

je me suis assise et elle m' a aussitôt demandé : " t' as révisé ? "
" non , mais on va se débrouiller . "
un détail m' avait toutefois échappé : c' était le premier contrôle d' algèbre depuis le début de l' année et j' avais un peu perdu la main en équations .
mais le vrai problème est apparu quand j' ai sorti ma trousse .
je veux parler de ma nouvelle trousse , celle que , dans mon délire nocturne de perfection scolaire , j' avais substitué à l' ancienne .
il est vrai que ma vieille trousse avait fait son temps .
je la trimballais depuis le CM un .
elle était à motif Elmer , l' éléphant à carreaux de toutes les couleurs et , avec le temps , le vert pomme avait viré au kaki , le blanc au gris et le jaune au verdâtre .
la fermeture éclair se coinçait sans arrêt et l' intérieur était constellé de taches d' encre .
la veille au soir , je m' étais dit : " vu le dix huit de moyenne générale que tu comptes atteindre , tu mérites quelque chose de mieux " .
du cuir plutôt que du plastique , du noir plutôt que du bariolé .
après avoir fouillé dans mes tiroirs pendant vingt bonnes minutes , j' avais trouvé l' objet idéal , ou presque .
c' était un genre d' étui pour manucure récupéré chez Sylvette un jour de grand ménage .
le grand ménage , chez Sylvette , ça veut dire : je me suis acheté plein de nouvelles choses avec mon astronomique retraite de prof d' Italien et je n' ai plus de place dans mon loft , alors venez vite me débarrasser de mes vieilleries .
je ne sais pas bien pourquoi on y va à chaque fois ni pourquoi , à chaque fois , on repart avec un sac rempli de babioles immondes , de bibelots tocards , de colifichets absurdes .
c' est comme ça , c' est un rituel .
en rentrant à la maison , ma mère et moi comparons nos butins .
quand nous sommes de bonne humeur , ça nous fait rire .
parfois ça nous énerve .
une fois ma mère a failli en pleurer .
la pochette dont j' avais hérité lors d' un de ces fameux vide greniers trônait à présent sur ma table et je contemplais sa noirceur avec découragement .
elle était plus belle , certes , plus mure et peut être même plus pratique que ma vieille trousse Elmer .
elle n' avait que des qualités , mais ce qui lui manquait risquait de nous faire passer , Fleur et moi du mauvais côté de la moyenne .