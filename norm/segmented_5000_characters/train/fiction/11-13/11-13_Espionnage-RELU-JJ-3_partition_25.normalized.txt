" chère Rosie , ma mère m' a offert ce cahier en me disant : " tu es muette à table , peut être seras tu plus bavarde par écrit .

c' est une très bonne idée .
si je ne dis rien à table , c' est que les autres parlent trop .
ma soeur , Dieu sait que je l' adore , ne me laisse pas de place dans la conversation .
si elle tenait ce journal intime , il serait plein de sons et de lumières , de gaffes et de bêtises .
moi je suis simplement ...
sage .
il faudrait que je m' invente une vie pour être une fille intéressante .
je ne le suis pas .
d' ailleurs , j' aimerais être écrivain pour créer d' autres vies que la mienne .
j' aimerais imaginer des personnages , des vilains , des anges , des mégères et des sorcières et même des meurtriers et des voleurs .
mais je sais , j' ai toujours su que je veux surtout avoir des enfants .
filles ou garçons , ça m' est égal , quoique non , ce n' est pas vrai , je préfèrerais une fille .
la mère qui a une fille a une amie pour la vie , une complice , une soeur ...
ma soeur !
qu' est ce que je serais sans ma jumelle ?
quand elle n' est pas avec moi , c' est comme si je manquais d' oxygène .
on est ensemble depuis notre conception .
on se comprend d' un regard , on a les mêmes pensées au même moment .
nous gloussons aux mêmes plaisanteries .
nous sommes de vraies jumelles et pourtant malgré tout , un peu différentes par les rêves et le caractère .
par exemple , elle trouve mon désir maternité d' une banalité affligeante .
mais avant d' en arriver là , il reste fort à faire : finir le lycée , faire des études , recevoir une formation , rencontrer l' amour , trouver un travail , démarrer dans la vie ...
depuis la mort de papa , maman se démène pour subvenir à nos besoins .
papa nous manque à toutes les trois mais nous vivons dans la gaieté , le rire , la stimulation .
grâce à maman .
oui , elle pleure en parlant de papa mais elle dit et répète que nous , on est vivantes , et que la vie est un cadeau grandiose , comme une feuille blanche sur laquelle on peut écrire son destin .
elle travaille comme plombier , va au théâtre et aux concerts ( avec nous ) , au cinéma , et toujours , en sortant , elle remercie les artistes , les écrivains , les musiciens de nous avoir donné tant de plaisir .
" dieu ne peut pas être partout à la fois , alors il a créé les mères . "
angélique lit toute la nuit .
de toute façon , elle n' aurait pas pu dormir et quand arrive le matin , elle ressent une sorte de soulagement .
elle ressent surtout une terrible impatience de retourner auprès de sa mère .
pour l' instant , elle commence à mettre la table du petit déjeuner , juste au moment où arrive sa grand mère apportant des croissants .
albertine n' a pas dû dormir beaucoup non plus .
ses poches sous les yeux lui occupent la quasi totalité des joues .
ses cheveux gris semblent encore plus gris sans leur brushing ni même le simulacre d' un coup de peigne .
et maintenant Mia les rejoint avec deux baguettes , et elles s' assoient en silence comme des veuves de tragédie grecque .
Charles descend ajouter sa part de silence funèbre à ce concert matinal , avant de déclarer : " il va falloir qu' on se secoue et qu' on reprenne nos esprits avant que Pablo ... "
Pablo surgit et demande : " avant que Pablo quoi ? "
avant que tu descendes et qu' on soit obligés de te laisser des croissants .
tu peux y aller , je préfère la baguette .
quelle fête , d' avoir mamie et tata si tôt le matin .
qu' est ce qui se passe ?
en l' absence de vos parents , nous sommes chargées de vous conduire à l' école .
ça tombe bien , dit Pablo , j' ai une idée pour le mariage .
faites ça à Disney world , je n' y suis jamais allé .
excellente idée , mon chéri , mais pas forcément pour le mariage .
max et moi t' y emmènerons avant mon accouchement .
" le bonheur veut tout le monde heureux " , dit mon ami Victor Hugo .
où as tu pris ça ?
demande Angélique .
c' est la citation du jour de la maîtresse .
" le bonheur est une habitude à cultiver " , c' était de la semaine dernière .
ça veut dire quoi ?
que même le bonheur est un travail , comme une plate bande .
je crains que tu sois le nouveau Bouddha !
je suis seulement l' ancien Pablo .
Bernard assiste au départ pour l' école depuis sa voiture garée à une certaine distance de la maison .
il n' a pas le coeur d' affronter les siens sans bonnes nouvelles à leur donner .
marie ne s' est pas réveillée .
il a averti sa mère et sa soeur qui vont le relayer un peu plus tard à l' hôpital , lui même ayant une réunion importante à son travail .
il se répète : " ils vont la réanimer , ils vont la réanimer . " les médecins ne voient pas tant de raisons pour qu' elle reste dans le coma .
et il y a beaucoup de raisons pour qu' elle en sorte : des raisons qui ont pour noms Bernard , Mia , Albertine et surtout Charles , Angélique et Pablo .
treize .
conversation avec une morte