j' habite déjà la petite maison toute déglinguée où je vis aujourd'hui , mais elle a l' air plus neuf .
mon père vient de quitter ma mère pour Marjolaine , une copine informaticienne qui m' offrait des pistolets et des épées parce que , disait elle , on lui avait pourri son enfance avec des baigneurs .
ce n' est pas un souvenir reconstruit , attention , personne ne m' a raconté ça , je m' en souviens vraiment .
nous habitons seules , ma mère et moi .
un jour , je me touche l' oreille .
ai je une otite ?
est ce pour jouer ?
ma mère reconnait ce geste et m' ordonne d' arrêter tout de suite .
elle a une voix crispée , je sens que quelque chose ne va pas .
je continue .
elle s' énerve , elle crie .
je trouve ça merveilleux .
ça met une ambiance terrible dans la petite maison .
depuis , pour passer le temps , pour énerver ma mère , pour me rappeler mon enfance , je me gratte l' oreille .
ça n' a rien de génétique .
est ce qu' on y croit davantage ?
je ne sais pas , mais moi , ça me convient mieux .