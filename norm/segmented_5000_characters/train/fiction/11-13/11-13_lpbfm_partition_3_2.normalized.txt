les affaires dans lesquelles c' est un enfant la victime .
tu n' as pas l' impression qu' il y en a de plus en plus .
non , c' est plutôt qu' on s' en occupe de mieux en mieux .
on est plus vigilant .
on en parle davantage .
vous ne ferez jamais dire à ma mère que quelque chose est pire qu' avant .
elle déteste la nostalgie , elle veut croire au progrès , aux vertus de la science , à l' éveil des consciences .
elle est tellement naïve , mais bon , je crois que si elle n' avait pas ces jolies idées auxquelles se raccrocher , elle ne pourrait pas travailler comme elle le fait , énormément , dans des conditions difficiles et en se confrontant chaque jour à des situations épouvantables .
je suis très différente d' elle , de ce point de vue .
j' ai tendance à être méfiante et pessimiste .
j' observe , et rien de ce que je vois ne me rassure .
par exemple , je suis persuadée qu' il y a un problème avec les enfants .
c' est un problème moderne , un problème grave dont personne ne parle alors que les indices sont flagrants .
par exemple , dans le train , les gens supportent très mal qu' un bébé pleure , mais trouvent complètement naturel de hurler dans leur portable pour dire : " oui , là , c' est bon je suis dans le train , donc j' arrive dans deux heures , si le train n' a pas de retard . et j' ai eu de la chance , figure toi , parce que le métro s' est arrêté entre deux stations et que j' ai cru que j' allais le rater . " dans ces cas là , je me demande toujours qui est à l' autre bout du fil .
qui a envie d' avoir une conversation pareille , franchement ?
mais je m' égare .
ça s' appelle faire une digression .
c' est important les digressions dans les romans .
je n' ai pas encore bien saisi pourquoi , mais j' ai quand même décidé d' en faire quelques unes .
donc , le train .
mais le restaurant aussi .
les gens trouvent que les enfants font trop de bruit au restaurant .
j' ai même lu un article sur des villages aux États Unis où les moins de dix huit ans ne sont pas admis .
en France , les trottoirs sont bondés d' adultes excédés hurlant sur des gamins en pleurs .
cette année , on a tué un grand nombre de bébés dans le monde , on en a mis dans des congélateurs , dans des coffres , dans les toilettes .
les enfants dérangent , c' est évident .
en recoupant tous ces faits , en les comparant , sans oublier d' y ajouter les histoires de ma mère , la mode du dog sitting et les croche pattes de l' anglais , je crois pouvoir déclarer officiellement que quelque chose ne tourne pas rond .
alors , vous me direz , il y a les pédophiles .
eux , ils adorent les enfants .
mais pas de la bonne manière .
c' est un extrême ou l' autre , en fait .
soit on nous aime trop , soit on ne nous aime pas assez .
je dis nous , parce que , même si j' ai quatorze ans , je considère que j' appartiens encore à la catégorie des enfants .
les ados , c' est comme des enfants en pire .
les adultes détestent .
ils nous trouvent mous , paresseux , impolis , insolents , idiots , bruyants , taciturnes .
là aussi la liste est longue .
et moi , j' aimerais comprendre .
j' aimerais savoir ce qui cloche dans notre merveilleux monde moderne .
j' aimerais quelqu' un me dise ce qu' on a fait pour mériter ça .