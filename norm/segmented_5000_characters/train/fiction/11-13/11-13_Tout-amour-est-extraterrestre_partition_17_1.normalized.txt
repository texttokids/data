sur ces entrefaites , ce que tout le monde prend pour Norman Master se transforme , se déforme à l' ébahissement général .
alors le vrai Don apparait .
cabotin , il se délecte de sa petite mise en scène .
nous venons délivrer nos camarades , annonce t il .
alors écartez vous et tout ira bien .
jeune présomptueux , je crois que vous surestimez vos capacités , ou tout du moins que sous estimez celles de la génération qui vous a précédés , lui répond la belle Rosewater .
son attaque mentale nous vrille le cerveau .
c' est insupportable !
j' ouvre la bouche pour crier , mais il n' en sort rien .
je tombe à genoux .
je me bouche les oreilles pour empêcher les ondes de pénétrer mon crâne .
geste puéril !
j' essaie de fermer mon esprit à ce flot ravageur mais je suis comme un poupon cherchant à fermer la lourde porte d' un énorme coffre fort .
soudain , je sens comme une main qui m' aide et qui pousse la porte avec moi .
Clac !
ma barrière mentale est enfin mise en place .
je revis , comme si on venait de retirer de mes épaules tout le poids de la Terre .
en me relevant , je jette un coup d' oeil en direction de Don , qui me sourit .
j' aurais dû m' en douter .
les autres avec nous , lance t il à pleins poumons .
ensemble , nous pouvons les battre !
alors s' engage une double bataille : physique et mentale .
les plus forts physiquement ne sont pas forcément des experts en combat mental , et réciproquement .
bref , chacun fait son maximum avec les pouvoirs qu' il détient !
quant à moi , je suis présentement bien trop occupé par le présent pour penser au passé , ou penser tout court .
mais je me dis que si les deux gars morts ont ressuscité , il va peut être être difficile de gagner une bataille contre les revenants , les spectres et les zombies .
on voit une nuée de pensionnaires s' abattre comme des mouches sur les adultes présents et tenter de les renverser .
cela a pour effet de les déstabiliser et de perturber leurs efforts à mettre hors de combat leurs adversaires par les seules forces de l' esprit .
en quelques minutes , la salle à manger si élégante est mise à sac .
les chaises volent en tout sens , couteaux , fourchettes , verres font de même .
impossible de savoir si quelqu' un les lance ou si ces objets sont projetés mentalement .
portes et fenêtres ne tardent pas à être de la partie et sont pulvérisées .
cette satanée Madame Rosewater et ses comparses ont plus d' expérience que nous .
mais le nombre joue en leur défaveur .
on a beau savoir que ces gens là sont , comme nous , des demi extraterrestres , rien ne nous réunit .
je sens , comme tous ceux de ma génération , émaner d' eux une haine immense que je ne comprends pas .
mais le moment est mal choisi pour y réfléchir .
il faudrait d' ailleurs se demander aussi comment il se fait et pourquoi Madame Rosewater est restée femme .
laisse tomber , conseille la voix de Don dans ma tête .
l' esprit de Don surnage au dessus de la mêlée .
mieux , il attire et aspire nos capacités psychiques , comme un trou noir la lumière .
un à un , nous nous rallions à lui pour ne faire plus qu' un avec lui en esprit .
cette force collective grandit à chaque seconde , se révèle de plus en plus forte .
elle fait barrage à la haine qui s' accumule contre nos défenses mentales .
la tension est énorme .
je crois m' évanouir tant elle est forte .
je serre les dents , le corps contracté au maximum , les muscles tétanisés .
et soudain , c' est l' explosion !
cette haine , cette méchanceté mentale adverse est retournée contre ses expéditeurs .
le coup est terrible .
une charge incalculable leur explose en pleine tête .
tels des voiles lâchés par des prestidigitateurs , ils tombent , vidés de leur substance , et atterrissent pareils à des pantins désarticulés .
la sublime Rosewater comme les autres !
nous nous retrouvons d' un coup sans adversaires en face de nous , nous regardant les uns les autres , un peu bêtement , indécis , à court de réactions et d' idées .
quelques uns d' entre nous se penchent sur leurs ennemis terrassés , puis relèvent la tête , le regard noyé du plus total désarroi .
ils sont morts , dit un garçon au bord des larmes .
cela ne me surprend qu' à moitié .
le choc qu' ils ont subi a été si énorme et si violent !
pas étonnant que cela leur ait grillé la cervelle .
don se dépêche d' intervenir pour empêcher un mouvement de culpabilité dans la troupe .
vous n' êtes en rien responsables de ce qui vient d' arriver .
c' est leur propre haine qui les a tués !
et , croyez moi , s' ils en avaient eu la possibilité , ils n' auraient pas hésité une seconde à vous éliminer !
mais pourquoi nous haïssent ils tant ?
nous sommes comme eux , après tout .
c' est l' éternelle question que posent toutes les guerres depuis le commencement des temps : nations contre nations , frères ennemis , humains contre humains .
c' est moi qui vous le dis , moi l' ex futur historien .
vu comme ça , la chose parait d' une limpidité cristalline , mais je ne comprends toujours pas cette réaction jusqu' au boutiste .
venez !
poursuit Don , nous devons partir d' ici très vite .