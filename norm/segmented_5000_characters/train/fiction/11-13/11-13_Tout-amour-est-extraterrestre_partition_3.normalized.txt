je n' étais pas bien vraiment pas bien .
imaginez vous seulement un truc tout simple et tout bête , comme de déménager .
vous concevez le bouleversement .
avoir à transvaser sa vie d' un endroit à un autre , tasser ses accumulations et amoncellements futiles dans des cartons , essayer de se débarrasser de toute la pacotille qu' on accumule au cours de sa vie quotidienne , quitter ses amis et devoir en mendier d' autres ...
et s' expatrier !
songez à ce que c' est que de quitter son pays , brader sa langue , résilier sa culture , abandonner sa façon de vivre ...
eh bien , croyez moi , comparé à un changement de sexe , tout cela n' est que broutilles .
changer de sexe et , en prime , s' attirer le mépris , et plus ou moins la hargne de ses trois mères simple , grand et arrière grand !
mes multiples mères , qui m' ont élevée dans la haine sacrée de ce que précisément j' allais devenir .
ma bonbonnière rose de chambre , qui me semblait déjà incongrue , m' est désormais insupportable .
et je n' ai rien d' autre à mettre que des robes cul culs et des fanfreluches funambulesques .
je voudrais m' enterrer à dix mètres sous terre , mais voilà : je suis dotée d' une furieuse envie de vivre .
et il y a Oliver ...
c' est dans cet état d' esprit que je me rends à l' école , toute de rose vêtue .
bon , il faut l' admettre , je n' ai jamais partagé le gout de mes mères et je me suis toujours sentie déguisée dans leurs choix de fringues .
mais je suis une bonne nature et j' ai toujours été d' avis qu' on doit réserver ses combats pour des causes nobles .
sauf que , maintenant , c' est insoutenable .
ça sonne .
il y a une bousculade pour faire la course aux salles de classe .
et si je sais une chose sur cette Terre , c' est bien que je ne suis pas faite pour la bousculade .
je rebrousse donc chemin et me réfugie dans les toilettes dames .
mes chères toilettes dames !
je m' enferme dans l' un des cabinets .
je baisse ma culotte , mais pas pour faire pipi , pour me toucher au point sensible de mon anatomie féminine .
je ne me suis jamais vraiment regardée de près à cet endroit précis : comment faire , à moins d' être une contorsionniste ?
c' est tellement secret et enfoui .
plus facile pour un homme !
à présent , je sors mon miroir de poche et le place stratégiquement .
je sais ce qui est quoi , parce que j' ai regardé des planches assez réalistes dans un livre .
j' identifie ces lèvres intérieures lisses de la vulve et celles , à l' extérieur , plus charnues et couvertes de poils .
au moins ceux ci sont les bienvenus ici , bien que beaucoup de femmes aujourd'hui se les fassent épiler .
je ne peux pas dire que je trouve l' image dans la glace d' une extrême beauté plastique .
c' est ridé , visqueux et d' une couleur indéfinie .
mais c' est chaud et doux , humide et agréable .
je ne sais pas comment ça marche mais j' aime me toucher .
je laisse courir ma main sur tout mon corps et je constate que mes seins , hier inexistants , commencent légèrement à enfler .
juste au moment où je vais devoir leur dire adieu !
je déambule dans le lycée comme un mouton égaré .
il y a d' autres élèves qui sèchent les cours comme moi .
on peut toujours prétexter une autre activité : journal , orchestre , théâtre .
au bout d' un couloir , je passe tout près de deux grands , percés de partout , qui rudoient une fille en la poussant contre un mur .
elle sanglote en suppliant : " lâchez moi ! " je me demande si dans quelques mois je serai comme eux .
je n' ai jamais eu ni grand courage physique ni grand courage tout court .
je n' aime ni la bagarre ni les conflits .
je m' écrase en général devant cette brute de monde immonde .
mais là , dans ma robe rose , avec mes boucles blondes , je m' entends dire : " foutez lui la paix ! "
De quoi je me mêle !
avant que j' aie le temps de m' approcher , ils sont tous les deux sur moi , et la fille peut s' échapper , et moi , devenue subitement Superman , je soulève le plus grand des deux types d' une seule main et le balance à l' autre bout du couloir .
c' est à ce moment que ça sonne de nouveau .
les deux gars se sauvent en me regardant d' un oeil mauvais et en grognant : " elle est bizarre , cette nana . "
tout ça , c' est trop pour moi .
je fonds en larmes .
je ne m' aime plus .
je n' aime pas cette force et cette violence qui ont surgi en moi , jaillies d' une source inconnue .
même si c' était pour la bonne cause , mon comportement n' a pas été ce qu' il aurait dû être .
c' est dans cet état de demoiselle en détresse qu' Oliver me trouve .
il me prend dans ses bras .
il a un centimètre de moins que moi mais je me tasse pour me conformer à l' image du couple idéal , où l' homme est censé être plus grand que la femme .
il m' embrasse sur les joues , sèche mes larmes avec un kleenex et dit : " on se barre ! "
je ne suis plus que de la compote , je vais achever de me liquéfier d' une minute à l' autre .
je m' accroche à sa main , qui est ma bouée de sauvetage .
je ne demande pas où on va , j' ai confiance en lui .
je me retrouve propulsée jusque dans sa chambre dans le silence de sa maison vidée de sa population de travailleurs et d' écoliers .
je m' affale sur le lit .
oliver s' étend à côté de moi .
nos bouches se joignent dans un baiser où je sens tout mon être se déverser dans le sien .
j' oublie mon chagrin , j' oublie la Terre , j' oublie l' univers .
j' oublie aussi le temps .
il n' y a rien , rien que nos deux bouches , nos deux langues et nos deux corps qui s' unissent dans ce choeur silencieux à deux voix .
je ne sais pas comment , mais d' un coup nous voilà nus .
peau contre peau , torse contre torse , géométrie biscornue qui produit une algèbre selon laquelle deux égalent un .
je n' ai jamais pareille fête , semblable réjouissance , un tel confort .
sa peau est un baume et , en même temps , elle me brule .
je suis apaisée et en feu .
sa bouche , ses mains explorent ce champ nouveau , alors que les miennes vont tout droit vers cet organe que j' ai toujours voulu voir de près .
et toucher .
je ne suis pas déçue .
c' est une baguette magique .
j' ai l' impression d' être toute lumière en le caressant .
Karen , qui a de l' expérience , m' a dit que sa première fois avait été une horreur .
et toutes les autres fois aussi .
elle a toujours eu mal , sans aucune récompense .
" franchement , m' a t elle confié , je ne vois pas pourquoi on en fait tout en plat . c' est nul ! "
non Karen .
tu as tort .
ou bien , tu n' as pas ce don que je découvre en moi , celui de me donner entièrement , de me livrer , captive et libérée .
je suis en exil de moi même et simultanément enfin chez moi .
ce sexe d' homme dans ma main , c' est un ami pour la vie qui remplit toutes les lacunes et soulage toutes les blessures .
Karen , pour me faire une blague lors de mon dernier anniversaire , m' avait offert un préservatif .
je l' ai gardé précieusement dans mon sac .
de temps en temps , je vérifie qu' il est toujours là au chaud avec mon rêve d' amour .
je pense que c' est le moment de le sortir .
qu' est ce qui te prend ?
demande Oliver paniqué .
pas de blague !
même si je suis sure de toi comme toi de moi , on nous l' a assez dit : il faut boucler sa ceinture .
oliver prend une mine de dégout mais se conforme aux instructions en vigueur .
je déroule le cylindre de caoutchouc sur son membre en alerte .
puis on l' oublie .
et je ne suis plus que contentement , délectation , et plaisir qui augmente , qui escalade , qui court , qui saute , qui vole .
oliver est dans le même état que moi .
oliver et moi .
oliver est moi , je suis Oliver .
ce n' est pas la paix , puisque tout mon être tend vers quelque chose .
ma respiration est rapide .
je suis presque à bout de souffle .
je ne sais pas où je vais , mais je sais qu' il faut y aller , y arriver .
je navigue sur une mer calme à bord d' un voilier qui tangue .
et puis je surfe sur une grande lame qui me submerge .
je suis en haut de la vague , entre ciel et mer , et je plane .
la vague se brise en milliards de gouttelettes qui m' aspergent de l' intérieur ...
plus rien n' existe sauf cette détente après l' orage heureux .
oliver n' arrête pas de m' embrasser partout .
tu es tout illuminée , me dit il .
il y a une lumière en toi .
quel lyrisme !
tu es poète .
sans rire , c' est comme si un néon était branché sous ta peau .
je n' ai jamais entendu parler de cet effet physique dans aucun des articles que j' ai lus sur le sexe .
tu es une luciole !
je la vois sous mon épiderme , cette lumière quasi électrique .
c' est effrayant .
ça va durer , tu crois ?
on va retourner en classe et je serai comme un phare ambulant ?
pourvu que tu ne t' éteignes jamais !
tu m' as donné ma première fois .
je n' ose pas lui dire qu' il m' a donné ma dernière fois .