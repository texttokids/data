que vous êtes beau , quand vous rugissez !
s' écria Catherine en se jetant à mon cou .
vous êtes mon pharaon superbe et généreux .
et accessoirement le roi des imbéciles quand Catherine fond dans mes bras .
j' aurai la peau de Jules Sampan !
qu' est ce qu' il vous a fait , ce malheureux gamin ?
me demanda Catherine , s' essuyant les mains à son tablier .
j' avais trouvé refuge dans les cuisines de SaintPrix auprès de la nouvelle cuisinière .
il m' a fait que je vais l' étriper .
j' ai voulu passer des diapositives tout à l' heure pour occuper ces satanés quatrièmes un et , bien sûr , quand j' ai branché la visionneuse , " banzaï ! " , les plombs ont sauté .
vous n' allez pas accuser Jules de ce que l' installation électrique est défectueuse .
je ricanai , l' oeil sombre .
j' aime votre candeur , Catherine .
et croyezvous que les tables de la salle cent quatre lévitent d' elles
Catherine haussa les épaules :
des enfantillages !
vous exagérez pour tout .
c' est comme votre description de madame Zagulon .
je m' attendais à un monstre ...
et c' est un monstre , dis je , sur le ton de l' évidence .
c' est une femme un peu forte qui se maquille trop .
à part ça , elle n' est pas déplaisante .
je fis un bond sur mon tabouret :
quoi !
cette ogresse ?
Catherine se tapa le front de l' index :
ça ne tourne pas rond là dedans , mon petit vieux .
faut vous soigner .
je passai à la contre attaque :
je suppose que vous trouvez monsieur Faure irrésistible ?
comment dites vous déjà ?
ah oui !
" sexy " .
il est gentil , dit Catherine du bout des lèvres .
il vous tourne autour .
ce midi , il passait son temps à aller chercher le pain et l' eau , à la cuisine .
il est serviable .
et puis ne m' énervez pas , mon petit vieux , ou je vous parle de votre Juliette .
vous lui donnez du Juliette toutes les deux phrases .
" vous voulez le pain , Juliette ? " " vous voulez l' eau , Juliette ? "
c' est que je suis serviable , mon petit chéri .
ah non , ne m' appelez pas " mon petit chéri " .
c' est ringard mortel , mon petit vieux .
je hurlai :
dans ce cas , arrêtez de me pousser dans la tombe avec vos " petit vieux " toutes les dix secondes !
Catherine plissa le nez et constata calmement :
ça défoule , un peu de psychodrame ...
je ne sais pas pourquoi , mais depuis que je suis dans ce fichu collège , j' ai les nerfs en pelote .
Catherine avait raison : il y avait dans l' air un je ne sais quoi d' inexplicable qui détraquait les gens .
peut être était ce tout simplement la neige qui nous exaspérait à tomber ainsi en flocons mollasses depuis la veille ?
le Char à voile , Nils , c' est un café dans le centre ville , m' apprit Catherine en raccrochant son tablier .
c' est le point de ralliement des jeunes et le seul endroit de Queutilly à garder un semblant d' animation après vingt et un heures .
il ne laissera pas filer un interne .
et après dix huit heures , le portail est fermé .
pour savoir le fin mot de l' énigme , nous convînmes de nous partager les rôles , Catherine et moi .
ma secrétaire irait au Char à voile prendre une tisane ...
et si j' ai envie d' un cognac ?
m' interrompit Catherine , d' un air de défi .
mais brûlez vous la gueule au schnaps si ça vous chante , ma petite vieille ...
donc , pendant que ma secrétaire , en sifflant du cognac , ferait le guet au Char à voile , je surveillerais les allées et venues des internes .
après vos cours , conclut Catherine , vous viendrez vous cacher dans ma cuisine jusqu'à ce que sonne l' heure fatidique .
parfait , camarade .
nous topâmes .
Catherine , je n' aime que vous .
confidence pour confidence , moi non plus .

fallait attendre que les internes se soient couchés et que le pion ait effectué sa ronde de surveillance .
le ronronnement de la machine eut finalement un heureux effet hypnotique .
quand je m' éveillai en sursaut , il était vingt et un heures passées .
j' avais entendu grincer une chaise métallique sur le dallage .
il y avait quelqu' un dans le réfectoire .
dans la brume du réveil , une phrase me troua la cervelle : " tu te fais toujours assassiner . " bien sûr , c' était absurde , mais c' était quand même désagréable .
je me redressai lentement .
avais je vraiment entendu le grincement ou avais je rêvé ?
jules Sampan était peut être derrière la porte de la cuisine ?
mais à nouveau , sans me demander mon avis , une phrase me traversa l' esprit : " ce n' est pas Jules Sampan . " à pas de loup , je m' approchai de la porte et j' écoutai .
deux minutes s' écoulèrent dans le silence le plus complet .
j' avais dû rêver ou bien j' avais entendu un chat , le pion ou un fantôme , rien qui justifiât ces battements de coeur affolés .
la neige ne me vaut rien , pensai je .
" ni les assassins " , ajouta en moi la petite voix qui parlait sans mon consentement .
le réfectoire était désert .
je le traversai et , dans ma hâte , j' accrochai un pied de chaise , produisant exactement le grincement métallique que j' avais entendu .
j' accélérai encore le pas et montai au premier étage .
en passant devant ma salle cent quatre , je crus apercevoir sous la porte un rai de lumière .
cela ne dura qu' une seconde .
j' avais fermé ma classe à clé en m' en allant .