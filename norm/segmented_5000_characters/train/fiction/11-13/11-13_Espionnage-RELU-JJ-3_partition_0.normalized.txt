angélique se lève toujours avec le même entrain .
son père met la table du petit déjeuner et dessine un visage avec des fruits sur chaque assiette .
elle a hâte tous les jours de voir l' expression qu' il a donnée au sien .
pour sa mère , c' est chaque fois la même figure : une bouche qui croque une cigarette .
s' il n' y a qu' un nuage dans cette famille , c' est un nuage de fumée : celui d' une mère idéale mais hélas tabagique .
le grand frère d' Angélique , Charles , sérieusement englouti dans son année de bac , se met à table en crachant inlassablement le même discours récité comme une prière : " ... la responsabilité du tabac est reconnue dans la survenue d' un cancer sur trois et d' un grand nombre de décès d' origine pulmonaire , cardiaque ou vasculaire . "
arrête tout de suite , Charles , dit sa mère .
je connais la chanson .
j' arrêterai quand tu arrêteras .
le tabac est la cause de soixante mille décès prématurés chaque année .
un fumeur sur deux mourra du tabac s' il a commencé à fumer à l' adolescence .
le tabac diminue l' espérance et la qualité de vie .
je ne suis pas d' humeur , lâche moi .
chaque paquet porte l' inscription " fumer tue " et ça n' a jamais empêché personne .
il faudra que le déclic vienne de moi .
sans parler des rides !
chuchote Angélique .
sans parler aussi des risques pour toute la famille .
la famille , en l' occurrence , c' est surtout Pablo , le petit frère , que ses parents ont conçu dix ans après la naissance d' Angélique , enfant tardif et surdoué qui a sauté la grande section de maternelle et s' ennuie en CP parce qu' il sait lire depuis longtemps .
il s' appelle Pablo parce que les parents ont fait un voyage à Madrid où ils ont vu le tableau de leur vie , le Guernica de Picasso .
angélique fut surprise et consternée par ce petit rouquin dès qu' il a surgi dans sa jeune vie , et Charles aussi .
elle avait fait plus ou moins la paix avec l' existence de Charles et lui se battait encore pour accepter la petite soeur , mais cette petite peste de Pablo était simplement de trop .
ils se battaient pour éviter de s' occuper de lui , le conduire à l' école , et fourguer à l' autre les nouvelles corvées associées à ce frère interlope .
mais Pablo ne s' en rende compte qu' il n' appartienne pas à leur " génération " .
et puis forcément , le temps faisant son travail , l' accoutumance s' activant , Pablo forge une petite place dans leur vie .
demain , j' arrête , déclare solennellement leur mère .
résolution qu' elle prend à peu près tous les jours .
le petit déjeuner est toujours expédié , néanmoins Angélique aime ce moment chaleureux qui ouvre sa journée de collégienne bien dans sa peau , ( presque ) bien dans sa famille , bien dans son collège , dans sa maison , sa ville , son pays bien dans son petit monde .
son monde .
le monde , c' est une autre histoire .
angélique est au courant du fait qu' elle est censée faire une crise d' adolescence .
mais elle ne la voit pas venir .
sa vie n' est qu' une fête qui se réactive à chaque lever du soleil , et du soleil , il y a en profusion dans sa ville de Nice .
même quand une minuscule déprime la frappe , suite à une remarque d' une copine ou d' un prof , à une note au dessous de dix huit ou à une dispute avec son grand frère , ou une vexation liée à Pablo , elle ne perd jamais de vue le bonheur qu' il y a à respirer , écouter et entendre , sentir et ressentir , renifler la vie à deux narines .
cela dit , Angélique n' en reste pas au stade de l' imbécile heureuse .
elle suit l' actualité et lit le journal que son père achète tous les jours .
son bonheur souffre des diverses tragédies qu' endure la planète Terre , si malade et si malmenée .
mais bon , on ne peut pas renier la chance qu' on a d' être née dans un pays démocratique , dans une famille harmonieuse , d' être une privilégiée , et la mauvaise conscience ne résout rien .
d' ailleurs , même chez les privilégiés , il y a des problèmes .
dans son cercle immédiat , son petit frère est allergique aux acariens , sa tante , soeur jumelle de sa mère , n' a jamais trouvé de mari , sa grand mère , qui travaille encore , souffre de rhumatismes croissants , et elle même , Angélique , est brouillée avec sa meilleure amie .
et puis , sa mère , comme il a été dit ci dessus , fume .
c' est ce souci qui tracasse le plus Angélique .
d' une intervention qu' a faite un médecin au collège , elle a retenu que fumer coute au fumeur quinze ans de vie .
et elle ne peut même pas envisager la mort précoce de sa mère sans avoir à lutter contre une montée de larmes .
ce médecin a donné le barème exact de la vie réduite :
boire du café égale moins un an et demi ,
manger du sucre égale moins quatre ans et demi ,
ne pas faire d' exercice physique égale moins trois ans .
sa mère a honte de fumer .
elle se cache à la cave , s' isole dans le jardin , s' enferme dans le grenier .
elle est consciente de son suicide plus ou moins immédiat .
elle sait que sa fumée empeste et la fait tousser .
elle se déteste mais semble incapable d' arrêter .
c' est à dire qu' elle s' arrête tous les jours .
elle crie victoire chaque soir .
elle a cru remporter la victoire des centaines de fois , et la rechute répétée a creusé une ride méchante à son front .
elle réussit assez bien à camoufler son échec , mais l' odeur , la fumée , la voix rauque , la mauvaise haleine le trahissent .
tout ça ne l' empêche pas de faire tout son possible , en tant que mère .
une mère qui applique la règle numéro un des mères : aimer ses enfants .
elle se comporte avec eux comme elle agit avec ses classes de l' école Saint Barthélemy , où elle est institutrice : paisiblement , sans élever la voix , affectueusement , avec beaucoup de tendresse .
sa seule bête noire , la seule faute qui l' énerve au point de lui faire perdre son calme , c' est la faute d' orthographe .
je ne comprends pas pourquoi on en fait de plus en plus , pourquoi il y a trente ans on savait écrire sans fautes , pourquoi maintenant on n' arrive plus à faire entrer la bonne orthographe dans la tête des élèves .
elle parle de ses élèves mais toute la famille sait bien qu' en fait elle vise avant tout l' orthographe accablante de sa grande fille Angélique .
maligne , elle lui offre pour son anniversaire un magnifique cahier relié , d' une superbe couleur rose fuchsia .
qu' est ce que tu veux que je fasse de ça ?
que tu y écrives , voilà tout .
mais j' écris quoi ?
tu tiens ton journal intime .
tu veux dire , je raconte mes secrets et tous mes trucs ?
en quelque sorte .
et toi , tu vas me corriger ?

pas du tout .
tu as tes secrets , tu te les gardes , mais au moins tu les mets noir sur blanc avec si possible la bonne orthographe .
je n' ai pas le moindre secret , maman .
alors tu te donnes le plaisir de l' expliquer .
comme presque toutes les idées de ses parents , Angélique trouve celle là intéressante .
elle aime écrire , elle écrit des poèmes et des nouvelles de temps en temps , quand elle a un peu de temps .
mais ce qu' elle aime avant tout c' est d' écrire sans penser aux règles de la grammaire .
toujours est il qu' elle attaque la première page de ce journal comme l' enfant sage et obéissante qu' elle a toujours été .
avant de se coucher , elle s' installe à son bureau .
elle a le trac .
qu' est ce qu' elle va bien pouvoir raconter à ces pages blanches reliées de rose ?
le cahier est tout neuf , tout propre , elle a peur de le salir .
elle aimerait laisser sa prose s' envoler mais pour ça il faut avoir quelque chose à dire .
et sa vie héroïque n' offre a priori rien qui puisse mettre en joie un journal intime !
pourtant , il faut bien commencer .