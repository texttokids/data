police !
que personne ne sorte !
on rallume .
l' inspecteur interroge tout le monde et il doit deviner qui est le coupable .
c' est un jeu qui fait très peur parce qu' on attend dans le noir .
on entend un grincement , une respiration .
quelqu' un vous frôle et puis c' est le coup fatal .
j' ai été tuée trois fois .
j' ai cru vraiment que j' allais mourir , tellement mon coeur battait fort .
en partant , j' ai remercié madame Maréchal .
je lui ai dit :
j' adore jouer à l' assassin .
ma copine a ri et elle m' a dit :
tu te fais toujours assassiner .
étrange , marmonnai je en reposant le devoir .
elle avait eu vingt vingt en chiffres de sang .
qui était Claire Delmas ?
quand je laissai aller ma tête sur l' oreiller , ce soir là , une phrase me vrilla le cerveau : " tu te fais toujours assassiner " et je ne pus l' en déloger .

un courant d' air glacial traverse de part en part le plateau de Queutilly comme un coup de poignard .
par la fenêtre de Saint Prix , on aperçoit la Doué qui branches nues des arbres .
l' hiver sera rigoureux , commenta monsieur Agnelle dans mon dos .
je me retournai .
je ne vous ai pas trop fait attendre , monsieur Hazard ?
asseyez vous ...
vous admiriez la vue ?
une fois derrière son bureau , le directeur de Saint Prix me dévisagea intensément .
je n' ai pas très bien compris , me dit il .
vous êtes de la police ou vous êtes de la faculté ?
mettons que je suis comme vous .
c' est à dire ?
un privé .
une sorte de sourire tordit la bouche du directeur .
les reliefs de son visage étaient si tourmentés qu' on s' attendait presque à voir les os se déplacer , sous la poussée d' obscures forces tectoniques .
bien sûr , reprit il , personne n' est au courant .
pour tout le monde , vous êtes le remplaçant de monsieur Copa , notre malheureux professeur d' histoire .
au fait , vous pourrez assurer ses cours ?
j' ignorais jusqu' au contenu des programmes .
tout à fait , dis je .
la bonté , monsieur Hazard , n' est pas le trait dominant de la jeunesse .
qu' ils viennent ou non de familles aisées , la plupart des jeunes de SaintPrix manquent de repères et de valeurs .
les derniers évènements m' ont peiné , mais pas vraiment surpris .
il faut s' attendre à cela et à pire .
il me regarda pour savourer l' effet de son préambule .
je lui fis un petit signe de tête encourageant .
fonce , Alphonse , tu m' intéresses .
je ne me serais pas résigné à faire appel à vos services , poursuivit il , si cette ...
ordure n' avait pas été glissée sous ma porte .
à bout de bras , il me tendit un papier sur lequel on avait écrit en lettres capitales :
agnelle , Tu t' en fous plein les fouilles .
tes jours sont comptÉS .
prends garde À TA COMPTA .
c' est anonyme , reprit Agnelle .
mais d' une certaine manière , c' est signé .
c' est le " style " des troisièmes un .
il froissa le papier dans son poing .
région ne tolèrent plus .
vous imaginez de quel rebut il s' agit .
il en parlait en faisant une lippe dégoutée .
les parents nous demandent de conduire ces jeunes jusqu' au brevet ...
il ricana :
jusqu' au brevet !
combien d' entre eux sont réellement récupérables ?
si sur vingt élèves , nous en sauvons dix , j' estime que nous aurons fait notre devoir .
pour les dix autres ...
il faut bien que le diable ait sa part .
n' est ce pas ?
fifty fifty , dis je tranquillement .
un brouhaha se fit alors entendre dans le couloir .
monsieur le directeur !
monsieur le directeur !
la porte du bureau s' ouvrit .
c' était Lucien , le concierge un peu simplet , suivi d' un surveillant .
un accident !
s' écria Lucien .
nous nous levâmes d' un même mouvement .
elle a sauté par la fenêtre , bredouillait le concierge .
c' est une chance .
elle s' est pas tuée .
dans la cour s' était formé un attroupement que le directeur fendit brutalement , en attrapant les gosses au collet et en les rejetant de côté .
une élève à se remettre sur pied .
celle ci poussa un cri de douleur .
c' est la cheville , dit monsieur Rémy .
entorse ou fracture .
je vais la porter à l' infirmerie .
il souleva sans effort la petite fille , une blonde aux cils presque blancs .
mais enfin , va t on m' expliquer ...
commença Agnelle , d' un ton rageur .
il ressortit des explications confuses du concierge que l' enfant avait délibérément sauté dans la cour depuis le vasistas des toilettes , au premier étage .
qu' est ce que je vais raconter aux parents ?
marmonna le directeur .
il se souvint de ma présence et ajouta :
excusez moi , monsieur Hazard , je dois aller téléphoner .
au fait , cette petite sera de vos élèves .
claire Delmas , sixième deux .
il me laissa au milieu de la cour , pris dans un tourbillon de pensées .
claire Delmas , vingt vingt , " on va jouer à l' assassin " .
l' infirmerie , s' il vous plait ?
sur les indications toujours embrouillées du concierge , je montai l' escalier de marbre jusqu'à
pourquoi tu as fait ça ?
disait une petite voix flutée .
parce qu' il était derrière moi , j' en suis sure , répondit une voix plus rauque .
mais quand même , t' es folle de sauter par la fenêtre !
j' ai pas réfléchi .
j' avais trop ...
il y a quelqu' un .