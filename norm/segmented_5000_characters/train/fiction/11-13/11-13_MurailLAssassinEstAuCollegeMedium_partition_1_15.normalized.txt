ainsi la ville de Queutilly et les parents d' élèves horrifiés apprirent ils ce jour là que leurs enfants avaient été confiés pendant deux ans à un ...
maniaque du crime .
Berthier m' a confirmé qu' avant d' être à SaintPrix , me rapporta Catherine , Agnelle était effectivement dans un collège de la Manche où les mêmes évènements se sont produits .
que voulez vous dire ?
m' étonnai je .
mais oui .
le coup des copies !
il l' avait déjà fait .
des copies d' élèves avaient été subtilisées et notées zéro vingt .
à l' encre rouge , cette fois .
je hochai la tête .
bien joué , marmonnai je .
le lendemain midi , l' inspecteur Berthier demanda aux professeurs et aux élèves de troisième un de se rassembler dans le réfectoire .
on convia aussi Lucien Renard et le surveillant Nicolas Arvet Dumillon .
mesdames , messieurs , commença l' inspecteur , il me revient la pénible obligation de vous apprendre ...
l' horreur , l' indignation , la stupéfaction , la révolte , la douleur se lurent sur tous les visages .
un à un , je les examinai .
l' un de ces visages mentait .
nous aurions dû éviter ce drame , Nils .
Catherine me regardait , désolée et un peu irritée aussi .
c' était évident que le directeur était fou .
vous ne vouliez pas en convenir .
il était le seul à pouvoir se promener librement à travers le collège .
forcer un casier , scier un barreau , limer un crochet , écrire sur les murs , tout était simple pour lui .
la nuit , il était le maître des lieux .
le passage par le soupirail ?
murmurai je .
ce n' est pas le directeur qui l' empruntait , d' accord .
mais Jules Sampan n' était pas le seul gamin capable de trouver le moyen de fuguer .
interrogez Boussicot , Alcatraz ou Marie Baston !
pourquoi le rap d' Axel a t il disparu ?
murmurai je à nouveau .
de quel pré Jules a t il voulu me parler ?
murmurai je encore .
je pris une feuille de papier sur laquelle j' écrivis les dernières paroles de Sampan : " pré AU PRÉ BALL MÉDECINE " .
" ball " c' est sans doute la batte de base ball qui l' a frappé , commenta Catherine , " médecine " , c' était plutôt " médecin " .
vous avez mal compris .
il réclamait un médecin .
je restai un moment dubitatif et mécontent .
pourquoi vous cassez vous la tête ?
me reprocha Catherine .
c' est trop tard , maintenant .
jules Sampan a payé d' avoir su la vérité avant nous .
comment l' a t il sue , à votre avis ?
mais la nuit où vous êtes allé le rechercher au bord de la Doué , vous l' avez bien laissé au pied du deuxième escalier ?
oui .
et alors ?
et alors ...
il a vu Agnelle errant au second étage avec ses yeux de fou .
puis , quand il vous a téléphoné , Agnelle était dans la salle des professeurs et il a tout entendu .
jules avait signé son arrêt de mort .
romans policiers .
c' est tout à fait le style .
je baissai les yeux sur mon papier et dans un éclair , je lus :
Catherine !
préau !
préau ...
jules parlait du préau et de " médecine ball " .
ces ballons de plusieurs kilos qui servent à la musculation ?
j' acquiesçai .
il y en a dans le préau , ajouta Catherine .
ils sont rangés dans un placard .
il faut ouvrir ce placard .
lorsque je me rendis au collège , le vendredi , Saint Prix avait déjà retrouvé un visage paisible .
les parents avaient d' abord cédé à la panique et téléphoné en nombre pour réclamer leurs enfants .
les professeurs s' étaient efforcés de les calmer .
le criminel étant sous les verrous , le danger était passé .
Faure assurait une sorte d' intérim à la direction .
Catherine , estimant son rôle terminé , s' était trouvé un successeur aux fourneaux .
Lucien était dans sa loge , fidèle au poste , presque à jeun .
heu ?
la clé ?
bredouilla t il .
la clé du ?

elle est sur le tableau , monsieur Hazard .
tenez !
je m' éloignai vers le préau .
qu' est ce que j' espérais trouver ?
un cadavre découpé en morceaux ?
je tournai la clé dans la serrure et j' ouvris un des panneaux .
aussitôt , un ballon , deux ballons roulèrent des étagères où ils étaient en équilibre .
j' en attrapai un qui allait m' écraser le pied et fis un bond pour éviter l' autre .
c' étaient des médecine balls de cinq kilos .
j' inspectai les autres étagères .
elles supportaient des haltères de différents poids .
pensivement , je remis les médecine balls en place , c' està dire en équilibre instable .
qu' est ce que Sampan avait voulu me dire ?
les haltères , me répondit Catherine .
jules vous a indiqué l' arme du crime .
mais pourquoi parler de médecine ball dans ce cas ?
Catherine haussa les épaules .
la question lui paraissait sans intérêt .
puisqu' on avait un coupable et une victime , le roman policier était terminé .
pourquoi donnez vous encore des cours dans ce collège ?
s' étonna Catherine .
je lui répondis par une autre question :
mais c' est un fou !
les fous ont des raisons qui échappent à la raison .
pourquoi Agnelle a t il manipulé sous mon nez mon coupe papier s' il l' avait volé dans ma poche ?
mais puisque ...
oui , oui , je sais !
m' exclamai je .
c' est un fou .
il n' y a que dans les mauvais romans policiers que l' assassin est un fou .