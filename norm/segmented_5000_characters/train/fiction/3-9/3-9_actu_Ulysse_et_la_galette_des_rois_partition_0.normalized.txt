les vacances de Noël sont déjà terminées , et Ulysse y repense avec nostalgie .
il ne veut pas retourner à l' école , car il voudrait faire encore la fête avec sa famille , manger de bons chocolats et des repas copieux ...
heureusement , il va pouvoir raconter tout ce qu' il a vécu pendant ces vacances à ses copains de l' école : les cadeaux , le sapin , les gourmandises ...
le jour de la rentrée est arrivé , Ulysse retrouve tous ses copains .
margot vient à sa rencontre : " bonjour Ulysse , mercredi , on fait la galette des rois , tu veux venir prendre le gouter chez moi ? " Ulysse répond : " la galette des rois ? qu' est ce que c' est ? " " c' est une fête qui suit Noël chaque année , on déguste une galette de frangipane ou aux fruits confis , dans laquelle est cachée une fève ! celui qui tombe dessus en mangeant sa part est sacré roi ! " Ulysse n' en revient pas .
il veut devenir roi !
il accepte aussitôt l' invitation de son amie .
le mercredi après midi , tous les amis d' Ulysse sont réunis chez Margot pour le gouter .
le papa de Margot est présent .
" connaissez vous l' histoire des rois mages ? " , demande t il .
" je vais vous la raconter . "
" il y a bien longtemps , le petit Jésus est né , un soir de Noël , dans une étable , entouré d' un boeuf et d' un âne . ses parents , Marie et Joseph étaient très heureux . trois rois mages , Melchior , Balthazar et Gaspard , venus chacun de différents royaumes , ont entendu parler de la naissance de celui qui était appelé' Le Sauveur' . "
" il partirent tous les trois à la rencontre de l' enfant . perdus dans la nuit , il virent une étoile briller plus fort que toutes les autres . elle semblait leur indiquer le chemin . cette étoile s' appelle l' étoile du berger , et elle les guida jusqu'à Jésus . "
" chacun des rois apporta un cadeau de naissance au petit Jésus : melchior , le plus âgé , offrit de l' or , qui représentait la puissance et la royauté . balthazar offrit de la myrrhe , une résine , qui signifiait la souffrance de Dieu pour les humains . Gaspard , le plus jeune , offrit de l' encens , que l' on offrait aux dieux . "
" chaque année , les chrétiens fêtent la visite des rois mages à Jésus . l' église a instauré la tradition de la galette plus tard . il fallait la partager avec toutes les personnes présentes . puis on ajouté une fève dedans . "
tous les enfants ont écouté , passionnés , l' histoire des rois mages .
il est maintenant temps de déguster la galette .
le plus jeune , le petit frère de Margot , Victor , se cache sous la table .
alors que leur papa découpe chacune des parts , les enfants se cachent les yeux .
personne ne doit voir où se trouve la fève !
Victor annonce un prénom pour chaque part .
Ulysse déguste sa part de galette des rois .
que c' est bon !
quand tout à coup , sa dent butte sur quelque chose .
il recrache ce petit bout dur : c' est la fève !
Ulysse est sacré roi !
il porte fièrement sa couronne dorée .
" et comme je suis le roi , j' ai besoin d' une reine ! margot , tu seras ma reine ! " margot rougit .
encore un bel après midi de fête , avec l' Epiphanie .