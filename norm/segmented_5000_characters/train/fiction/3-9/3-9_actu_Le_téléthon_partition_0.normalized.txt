Léa a beaucoup d' amis à l' école .
il y a Sonia , Maryam , Émilie , Zoé , Paul , Tom , mais il y a aussi Sophie .
Sophie est un peu différente de ses autres amis , car elle est en fauteuil roulant .
mais cela ne l' empêche pas de jouer avec Léa dans la cour d' école !
samedi , c' est l' anniversaire de Léa , et elle veut inviter tous ses amis .
elle va donc à la rencontre de son amie Sophie .
Sophie , je fête mon anniversaire samedi , tu viendras ?
c' est gentil Léa , mais je ne peux pas venir samedi , car c' est le Téléthon .
le quoi ?
le Téléthon ?
qu' est ce que c' est ?
comme tu le sais , j' ai une maladie , qui fait que mes muscles ne fonctionnent pas bien du tout .
j' ai une amyotrophie spinale .
Olala , c' est compliqué comme nom !
et tu ne guériras jamais ?
je ne sais pas , mais il y' a des recherches qui sont faites pour trouver des remèdes .
et le Téléthon aide les enfants comme moi .
chaque année , il y a une grande collecte nationale qui est organisée en France : le Téléthon !
grâce à l' argent que les gens donnent , les médecins et les chercheurs peuvent trouver des remèdes aux maladies , car c' est parfois très long et très cher .
pour inciter les gens à s' intéresser aux maladies , il y a des évènements organisés dans toute la France : des concerts , des spectacles , des animations , des courses ...
il y a même un marathon télé !
des chanteurs , des comédiens , des présentateurs , des humoristes viennent faire le show à la TV pendant tout le week end sans s' arrêter pour récolter un maximum de dons et rencontrer les malades .
Léa s' exclame : oh !
j' aimerai beaucoup t' aider à récolter des dons ce week end .
si tu veux , le Téléthon continue dimanche , tu peux venir au gymnase de la ville , il y a une course organisée , puis un concert .
oh oui avec plaisir , nous allons venir avec tous les copains dimanche .
ce sera l' occasion d' être tous ensemble et d' aider les chercheurs et les médecins pour te soigner .