ce sont les vacances de la Toussaint .
et que se passe t il pendant ces vacances là ?
halloween bien sûr !
et c' est aujourd'hui .
Lily se rend au magasin avec sa maman pour trouver un déguisement .
" maman " , dit la fillette , " j' aimerais bien trouver un déguisement de fée ou de princesse . " " mais ça ne fait pas très peur comme déguisement ma puce . " répond sa maman .
" mon déguisement doit faire peur ? pourquoi ? " demande la petite fille .
" halloween est une fête d' épouvante . " explique sa maman .
" les petits enfants se déguisent alors en sorcière , en fantôme , en monstre ou en citrouille pour effrayer les grandes personnes et récolter plein de sucreries . "
" ah c' est chouette ! alors il faut que je trouve un déguisement qui fasse vraiment peur pour avoir plein de bonbons ! " dit Lily .
elle cherche alors très attentivement dans les rayons .
" maman , j' ai trouvé ! " s' écrie la fillette .
elle montre un costume de diablotin , avec des cornes et une fourche .
sa maman trouve ça très drôle et l' achète à Lily .
le soir , la petite fille se prépare pour être la plus ...
effrayante !
elle enfile son beau costume de diable et demande à sa maman de la maquiller .
sa maman lui maquille le visage en rouge et dessine deux grands yeux noirs .
Lily est enfin prête pour partir à la chasse aux bonbons avec ses amis .
après une heure de porte à porte , la fillette revient , le sac chargé de sucreries .
" maman , j' ai fait peur à plein de gens avec mon déguisement ! " explique Lily , fière d' elle .
elle peut à présent savourer son butin avec tous ses copains .