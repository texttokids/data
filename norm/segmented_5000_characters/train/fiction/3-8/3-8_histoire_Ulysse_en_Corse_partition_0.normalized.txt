Ulysse va découvrir une île magique au coeur de la Méditerranée .
une terre française et pourtant tellement atypique .
la Corse .
accompagné de ses parents , il va entamer un voyage incroyable qui commence par l' avion .
à quelques minutes de l' atterrissage , Ulysse aperçoit deux enfants qui contemplent les aiguilles de Bavella .
il comprend alors pourquoi la Corse est appelée " l' île de beauté " .
les parents de son copain , Ange , sont venus avec un quatre par quatre pour réceptionner la petite famille .
" oh Ange , comment vas tu ? " demande Ulysse à son ami .
" ici tu es dans mon pays Ulysse , on dit salute ! et pour te répondre , mon ami je te dirais que je vais bien et toi ? " .
arrivés à la maison des parents du garçon , Ange propose à son ami d' aller se baigner .
Sampiero , le père du petit Ange , dit à son fils " nous irons tous ensemble à la plage , ce sera plus rigolo . " la famille part alors sur la route de Roccapina .
Ulysse découvre avec étonnement un lion de pierre qui se dresse fièrement au dessus de la mer .
" le seul lion que je connaisse est Arthur , le roi de la savane ! " s' exclame le petit garçon en riant .
arrivés sur la plage , la famille est frappée par la clarté de l' eau et la beauté des lieux .
ils s' amusent comme des fous .
la fin de l' après midi approche et le petit garçon dit à Ulysse " nous allons nous promener dans un endroit magique , les falaises de Bonifacio . " sur place , Ulysse comprend que le décor est somptueux .
" on dirait que la mer , les vagues et les rochers font sans cesse la même danse quand elles frappent les pierres " s' écrie t il .
la famille se promène sur le port et voit de beaux bateaux amarrés .
Ulysse imagine toutes sortes d' idées en voyant les recoins du port .
il s' imagine même en pirate , menant un combat sur l' un des bateaux .
avant de terminer sa journée , la famille se rend sur les plages somptueuses de Palombaggia , Rondinara , Santa Giulia , à quelques minutes de Porto Vecchio .
Ulysse , habitué à la vie citadine , n' a jamais vu de si beaux lieux .
ses yeux sont émerveillés lorsqu' ils regardent les plages , toutes plus belles les unes que les autres .
ange s' exclame " alors , elle est pas belle mon île ? " Ulysse , ne peut qu' approuver avec plaisir .
" allez , on va manger maintenant le sauté de veau de ma grand mère . "
à Sartène vit la grand mère du petit Ange .
sa maison est mitoyenne de celle des parents de Ange , la bonne odeur de la cuisine attire tout de suite Ulysse .
une dame , habillée en tablier , l' accueille .
" Fiddolu , que fais tu ici ? " Ulysse ne comprend pas , son ami Ange lui traduit " c' est du Corse ! Fiddolu " signifie enfant .
les deux garçons n' attendent pas longtemps pour tremper leurs lèvres dans le sauté de veau corse aux olives préparé par la grand mère .
" que c' est bon ! " le père d' Ulysse assiste à la scène et vient mettre un peu d' ordre .
" heu les enfants sortez de la cuisine et allez à table ! " dit il .
dès que les enfants ont le dos tourné , à son tour , le père prend la cuillère et goute le splendide sauté de veau .
mais les deux garçons , ne sont pas bien loin et rient en voyant que même les parents ne peuvent pas résister au plat de la grand mère .
le lendemain , Ange , emmène son ami dans les ruelles de la ville , ils jouent ensemble et se racontent des histoires .
" que font nos parents ? " demande Ulysse .
" t' occupes , ils dégustent un embrucciatta ( tarte au brocciu ) sur la place Porta . après ils iront chez Sampiero ou Thomas pour discuter " dit Ange
les deux amis s' amusent , Ange fait visiter la vieille ville et ses ruelles pittoresques à Ulysse .
" imagines que les rues où nous sommes ont plus de trois cents ans . " " mais tu sais comment retrouver nos parents demande Ulysse ? " " oui je connais les chemins de mon pays ! " répond Ange .
et en effet quelques minutes plus tard , la famille se retrouve à la cave Sartenaise , pour déguster le saucisson et le jambon de Claude .
Ulysse et Ange font tourner Claude en bourrique .
en effet , à chaque fois qu' il a le dos tourné les enfants viennent chipper des morceaux de jambon et de saucisson que Claude avait coupé pour ses clients .
il est temps pour la famille de partir , visiter les autres joyaux de la Corse , Ajaccio , les calanques de Piana , la citadelle de Calvi , Île Rousse , Saint Florent ...
" un été ne suffirait pas ! dit Ulysse à Ange " " non mais tu reviendras me voir " .
" une dernière chose pour la route " dit la maman d' Ulysse à la grand mère du petit Ange .
" je suis allée vous prendre un bouquet de fleurs chez Christiane et Paule , au magasin de fleurs sur la place . merci pour votre accueil " .
fin