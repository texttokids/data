allô , Hélène , alors comment vas tu ?
demanda le grand père , impatient , s' étant saisi du téléphone que Nanette lui tendit .
très bien papa , chez vous aussi apparemment , tant mieux !
oui , oui , les enfants sont très agréables .
et pour vous , pas trop dur ?
repartir a été difficile , Anna nous manque déjà .
mais nous avons beaucoup d' enfants malades à soigner ici .
le temps alloué à chacun est insuffisant pour les réconforter .
nous faisons de notre mieux .
patrice est avec toi ?
non , il vient de repartir car il a été appelé aux urgences .
il vous embrasse tous bien fort .
je dois vous laisser aussi , nous rappellerons dans quelque temps .

Anna avait déjà rejoint Anton , qui n' avait pas souhaité se joindre à la conversation et qui jouait avec la chienne .
elle était contente d' avoir reçu cet appel mais aurait préféré que ses parents soient avec elle pour ses dix ans .
les grands parents essayèrent de compenser cette absence en lui concoctant une belle journée d' anniversaire .
au menu de midi , un bon gâteau chocolaté surmonté de dix bougies remplaça occasionnellement le dessert type .
puis ils lui offrirent les cadeaux soigneusement cachés avant son arrivée .
en plus de ceux de Papy Germain et Nanette , la fillette reçut ceux que ses parents leur avaient confiés pour cette journée spéciale .
parents et grands parents avaient eu aussi une petite pensée pour Anton en lui achetant un petit cadeau afin qu' il ne se sente pas trop exclu de la fête .
le garçon exprima son bonheur par un large sourire et des remerciements encore un peu timides .
la journée se passa dans la joie et les enfants , autorisés à veiller un peu plus tard en cette fin de journée exceptionnelle , s' endormirent épuisés .
les vacances se poursuivaient toujours aussi agréablement et tranquillement .
les enfants allaient parfois cueillir des fruits murs dans le verger .
papy Germain en profitait alors pour leur faire connaître les diverses variétés existantes .
ils récoltaient aussi certains légumes du potager pour la préparation du repas du jour , sur demande de Nanette .
cette dernière en profitait pour leur donner des consignes tendant à les responsabiliser .
ils suivaient ses recommandations à la lettre .
ils prenaient soin de vérifier si , comme elle leur avait soigneusement expliqué , les plantes potagères étaient bien arrivés à maturité avant de les détacher ou de les extirper .
les deux enfants apprenaient beaucoup de l' expérience des grands parents sur la nature et sur la manière dont ils savaient observer celle ci .
la fillette les adorait et s' était toujours sentie bien avec eux .
elle leur témoignait une gentillesse sans faille .
cet été là , elle décida tout de même de profiter de la présence complice d' Anton pour faire ressurgir un peu sa nature espiègle .
bien sûr , cette tendance à l' espièglerie ne se voulait jamais méchante , juste taquine .
quelques jours après son anniversaire , elle entraina donc le garçonnet dans ses petites farces élaborées à l' intention de Papy Germain et de Nanette .
ainsi un jour , en début d' après midi , elle sut que son grand père avait prévu de se rendre un peu plus tard dans l' enclos de Canaille et Oisine .
il devait y terminer la réparation commencée tôt le matin .
elle prétexta alors aller cueillir des fruits avec Anton à qui elle demanda de la suivre .
ils se rendirent en premier lieu au poulailler .
à l' intérieur , elle fut contente de trouver deux beaux oeufs pondus récemment .
elle les prit délicatement et dit au garçon , resté hors de l' enclos :
" s' il te plait , peux tu m' ouvrir et refermer ensuite le portail ? on va bien rigoler tout à l' heure , viens ! "
Anton ne comprenait pas où elle voulait en venir .
tenant toujours dans ses mains les deux oeufs , elle entraina le garçonnet devant le parc des oies et lui demanda une nouvelle fois de bien vouloir lui ouvrir le portail .
le petit la regardait faire , silencieux .
une fois entrée , elle les déposa dans un recoin pour que les oies n' aillent pas les piétiner .
mais elle le choisit suffisamment visible pour rendre la chose la plus naturelle possible .
elle espérait ainsi que son grand père les trouve facilement .
le garçon saisit enfin ce que son amie réservait au grand père .
il se mit à rire en agitant une main devant lui , jugeant ainsi du degré de la nature de cette farce .
il s' y trouvait donc à présent associé malgré lui .
mais son esprit enfantin reprenant le dessus , il se sentit alors impatient de la voir aboutir .
avant de reprendre le chemin de la maison , ils firent une halte au verger afin d' y cueillir quelques fruits murs comme prévu .
au retour , ils croisèrent le grand père qui s' apprêtait déjà à reprendre son ouvrage du matin .
ils lui firent alors en passant un large sourire , contenant leur soudaine envie d' éclater de rire .
ils rentrèrent et attendirent discrètement , comme si de rien n' était .
moins d' une heure plus tard , entrant dans la cuisine où Nanette finissait de ranger ce qu' il restait du gouter des enfants , Papy Germain s' exclama :