enfin les grandes vacances !
ces deux derniers mois de scolarité avaient donc paru très longs à Anna .
de plus , ses parents s' étaient progressivement et méticuleusement occupés de la préparation de leur mission à l' étranger .
ils s' étaient mis à plusieurs reprises en contact avec leurs collègues établis depuis longtemps sur place , en Roumanie .
ils avaient rempli de nombreux dossiers et passé une grande partie de leur temps sur leur ordinateur .
entre les consultations médicales à l' hôpital en semaine et les préparatifs du départ pendant les week ends , la fillette commençait à appréhender leur prochaine longue absence .
elle se consolait en pensant qu' Anton allait bientôt être de retour .
les grandes vacances enfin là , les parents ne devaient rester qu' une seule journée après l' arrivée du petit garçon .
puis ce serait l' envol pour leur mission médicale d' une durée de huit semaines .
leur retour était normalement prévu pour la prochaine rentrée scolaire .
Anton revint donc , pour un troisième séjour , bien plus long cette fois , dans la famille d' Anna au tout début juillet .
il était maintenant habitué aux personnes qu' il retrouvait et aux lieux qui l' entouraient .
à son arrivée , son comportement semblait encore différent de celui adopté précédemment .
il paraissait plus avenant et vraiment heureux de retrouver sa famille d' accueil roussillonnaise .
il dormit une seule nuit chez Anna et le lendemain en début d' après midi , toute la maisonnée partit comme prévu en direction de la spacieuse maison des grands parents érigée dans la vallée du Conflent .
le coffre de la voiture était pleinement chargé , en dehors des valises , de jeux , de jouets et d' un petit stock alimentaire des denrées préférées des enfants .
bref , tout le nécessaire pour passer les meilleures vacances possibles !
de leur côté , les grands parents étaient très contents de les accueillir durant la totalité des vacances .
Nanette adorait Anna , leur seul petit enfant , qui avait le même prénom qu' elle .
cette similarité la comblait de bonheur et de fierté .
papy Germain aussi était heureux même s' il était beaucoup moins démonstratif que son épouse .
moins sujet à exposer ses émotions , il ressentait tout autant les sentiments .
l' été , pour lui , était synonyme de vie à l' extérieur et s' épanchait davantage dans ses moments là .
il se montrait plus enclin à exprimer ce sentiment de bien être .
la fillette le savait bien .
ils avaient en commun l' amour de la nature .
ils se comprenaient .
que d' espaces de jeux elle allait enfin pouvoir partager cet été dans leur propriété !
depuis toute petite , lorsqu' elle était chez ses grands parents , elle allait en leur compagnie nourrir les animaux dans leurs enclos , ainsi que récolter les fruits et les légumes qu' ils avaient plantés .
le couple aimait bien aussi faire des promenades dont elle connaissait le parcours habituel .
ils longeaient d' abord l' étroite route départementale jouxtant l' entrée de la propriété .
après , ils bifurquaient vers des chemins de traverse donnant sur des champs souvent abandonnés .
ils rapportaient , suivant les saisons , des petits cadeaux de la nature .
ce pouvait être de jolies fleurs des champs , de la bruyère , mais aussi du houx vers Noël , des asperges au printemps , des champignons ou des marrons tombées des arbres qu' il suffisait de ramasser , en automne .
la voiture se gara dans l' allée .
Nanette , qui s' impatientait déjà depuis un moment , trépigna de joie lorsqu' elle entendit la voiture .
papy Germain , qui guettait aussi leur arrivée tout en jouant avec Belle , la chienne de la maison , les rejoignit .
tandis qu' il descendait tranquillement l' allée à leur rencontre , la grand mère , s' étant subitement ruée hors de la maison , gesticulait , impatiente .
" les voici enfin ! " s' écria t elle .
les parents sortirent les premiers du véhicule et ouvrirent les portières aux enfants .
Anna , une fois libérée de sa ceinture de sécurité , descendit aussitôt .
elle se précipita vers son grand père et l' embrassa avant tout le monde .
Anton , qui était assis à son côté , avait fini par s' endormir , bercé par les balancements du véhicule .
la petite route menant à la propriété se montrait assez sinueuse sur la fin .
il fut donc doucement réveillé par les parents .
la petite ayant déjà filé rejoindre Nanette , Papy Germain se posta à côté de la voiture pour assister à la scène .
la maman détacha l' enfant de son harnais de sécurité puis le père , après l' avoir lentement extirpé du véhicule , le prit dans ses bras , encore à demi ensommeillé .
Anton mit un peu de temps à émerger de son état et à réagir .
" bonjour papa !
lui lança Hélène .
il n' a pas dû assez récupérer cette nuit après son long voyage , ajouta t elle en s' approchant de son père pour l' embrasser .
bonjour ma fille !
répondit le grand père en l' étreignant .
bonjour Germain !
dit le papa tenant toujours le garçonnet dans ses bras .
bonjour Patrice !
bonjour et bienvenue à toi !
" dit il à la suite à l' adresse d' Anton qui esquissa un léger sourire .