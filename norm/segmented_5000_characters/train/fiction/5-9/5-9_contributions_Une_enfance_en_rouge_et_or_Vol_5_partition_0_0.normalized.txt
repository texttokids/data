Anna gardait ce tempérament facétieux .
peut être représentait il dans son esprit une façon d' attirer l' attention sur elle .
ses parents prirent plusieurs fois des nouvelles par téléphone durant le mois de juillet .
ses grands parents les rassurèrent et ne mentionnèrent aucunement les pitreries répétées de leur petite fille .
quant à la fillette , elle avait profité d' un appel pour détailler à sa mère les deux soirées féériques des treize et quatorze juillet qu' ils avaient vécues .
depuis le terrain de la maison , ils avaient assisté en même temps à plusieurs feux d' artifice et spectacles pyrotechniques se tenant au dessus des villages avoisinants .
la propriété se trouvait sur un axe permettant de dominer une grande partie de la vallée .
Anton avait , lui aussi , été émerveillé par ce spectacle grandiose qui se jouait dans le ciel du Conflent .
Nanette avait au préalable pris soin de lui expliquer la signification de ces manifestations dont il ignorait l' existence .
Anna vivait des instants inoubliables en compagnie d' Anton .
il s' agissait des meilleures grandes vacances qu' elle avait passées jusqu'à présent .
sa famille avait vu juste en accueillant le garçonnet .
le bonheur était dédoublé .
ils le partageaient ensemble .
avec Nanette , fière de cuisiner les produits de leur propre récolte , ils apprenaient à confectionner des soufflés et des gâteaux .
elle leur montrait comment bien doser les ingrédients .
ce qu' ils adoraient par dessus tout , c' était préparer de succulents clafoutis avec les oeufs des poules de la maison et les fruits du verger .
quant à Papy Germain , il fabriquait le pain lui même .
durant la cuisson , la maison embaumait ce doux parfum qui s' exhalait , atteignant de façon exquise les narines de ses occupants .
Anna devait insister auprès de son grand père pour qu' il les laisse participer à la confection de ces magnifiques pavés de pain .
ces derniers n' étaient jamais semblables car le choix de la farine différait au gré de son humeur .
et pourtant , ils étaient tout aussi appétissants et délicieux les uns que les autres .
la fillette savait depuis toujours que ses grands parents étaient adeptes d' une vie simple et saine .
le cadre serein qu' ils avaient choisi s' y prêtait largement .
ils ne restaient toutefois pas coupés du monde .
ils s' informaient quotidiennement des nouvelles de l' actualité mondiale .
mais il n' exprimaient pas le besoin de tendre vers un mode de vie de surconsommation .
l' exposition de la propriété , dans cette belle région au climat tempéré , leur permettait même d' être à l' abri du vent ainsi que de profiter de la chaleureuse clarté du soleil .
le sol fertile de par sa nature leur facilitait les cultures .
néanmoins , s' ils se sentaient bien dans le havre de paix que représentait leur home privilégié , ils avaient su garder un sens aiguisé de l' hospitalité .
Anton en était le témoin direct .
ils aimaient aussi ouvrir leur porte aux amis proches et savaient recevoir .
il n' était pas rare que leurs invités passent tout un week end et même plusieurs jours chez eux .
cependant , cet été là , ils avaient décidé de se consacrer uniquement à s' occuper des deux enfants .
Anna regorgeait d' idées et avait encore plus d' un tour dans son sac à sortir avant la fin des vacances .
un après midi qu' elle entendit ses grands parents dire qu' ils allaient se délasser un moment sur leur fauteuil à feuilleter leur magazine préféré , elle s' empressa d' aller inverser les lunettes dans leur étui respectif .
" encore une idée farfelue signée Anna !
annonça Nanette en découvrant les lunettes du grand père dans son étui à elle .
oh , pas sûr !
tiens , voici les tiennes !
nous commençons à vieillir , ma chère , il faudra s' attendre à l' avenir à faire inconsciemment des bêtises !
peut être même pire que celles d' Anna !
" répondit il ironiquement en élevant volontairement la voix .
les grands parents se doutaient bien que l' enfant s' était cachée tout près afin de surveiller leur réaction .
la petite fille , les ayant écoutés du couloir , ne se montra pas .
ils ne lui firent ensuite aucune remarque personnelle et l' affaire passa sous silence .
mais elle débordait toujours d' imagination .
ainsi un matin d' août , elle eut subitement l' envie d' échanger en cachette les gamelles de Belle et de Tiky .
elle décida donc d' inverser les écuelles que Nanette venait de remplir et poser à leur place habituelle .
belle avait suivi de bonne heure Papy Germain qui était toujours en train de s' affairer dans l' atelier situé derrière la maison .
Tiky , habitué à partir chasser les nuits d' été dans la nature environnante , n' était toujours pas apparu .