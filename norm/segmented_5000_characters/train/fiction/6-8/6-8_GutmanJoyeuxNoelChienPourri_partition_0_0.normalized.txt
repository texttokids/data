c' est Noël dans la poubelle de Chien Pourri et Chaplapla .
le pauvre chat aplati se réchau E avec une bouillotte qui fuit et le toutou tout râpé a enfilé de vieilles chaussettes trouées .
malgré le froid et la faim , Chien Pourri attend avec impatience la venue de Chien Noël .
j' espère que nous allons être gâtés , Chaplapla !
oui , comme l' année derrière , avec une trottinette sans roues et un os sans moelle !
j' en ai marre des Noëls pourris , Chien Pourri .
chien Pourri sent son ami Chaplapla tout raplapla , alors pour lui donner du courage , il lui demande :
qu' est ce qui te ferait plaisir , Chaplapla ?
une nouvelle pompe à vélo pour me regonfler .
et toi , Chien Pourri ?
un collier antipuces et une boite de croquettes gout bacon .
pendant que Chien Pourri voit défiler les croquettes dans sa tête , Chaplapla sort son museau de leur poubelle .
oh , regarde , Chien Pourri , le caniche à frange a une nouvelle coupe de cheveux !
et le basset , un petit manteau d' hiver .
pourquoi personne ne nous aime , Chaplapla ?
parce que nous sentons la sardine , Chien Pourri .
nos deux amis ont le moral bien bas .
c' est alors que Chien Pourri a une idée :
Chaplapla , et si nous organisions le Noël des déshérités ?
pourquoi pas ?
comme ça , nous pourrions inviter tous les rats du quartier dans notre poubelle !
c' est vrai , tu es d' accord ?
chien Pourri , tu n' as aucune ambition !
moi , cette année , je vais passer le réveillon dans une maison de maîtres .
suis moi , si tu veux .
chien Pourri traine la patte dans le caniveau .
il pense que tant qu' il sentira la sardine , personne ne voudra l' inviter , mais Chaplapla le rassure :
tu feras ton regard de Chien Pourri .
un soir de Noël , ça devrait marcher .
mais le soir du vingt quatre décembre , trouver une maison accueillante pour deux pauvres bêtes revient à chercher un diamant dans une poubelle .
pourtant un écriteau donne un peu d' espoir à Chien Pourri .
chien Pourri prend son courage à deux pattes et gratte à la porte .
une petite fille répond :
mon papa n' est pas là et ma maman est partie !
mais Chaplapla insiste :
n' aie pas peur petite fille , nous ne sommes pas des bêtes ...
seulement un chien pourri et un chat aplati , poursuit Chien Pourri .
où sont vos maîtres ?
nous n' en avons pas , dit Chaplapla .
entrez , dit la petite fille .
chez les Noël , c' est tous les jours Noël !
youpi , dit Chien Pourri .
hourra !
crie Chaplapla .
mais à peine ont ils franchi la porte , que la petite fille s' écrie :
oh , mais vous êtes immondes !
remarquez , vous tombez bien , je n' avais pas de cadeaux pour mon petit frère !
chien Pourri n' en croit pas ses puces .
il va devenir le cadeau d' un enfant .
même dans ses rêves les plus fous , il ne pouvait s' imaginer pareille chance .
" il me lancera des ba balles rebondissantes , et je regarderai la télé en mangeant des mini saucisses .
c' est sûr , une gentille fée m' a transformé en labrador !
" , pense t il .
Chaplapla , en chat échaudé , ne partage pas son enthousiasme .
elle va m' O rir en frisbee ou en dessous de plat à son petit frère .
prétentieux , va , dit Chien Pourri .
noël noël Dans sa chambre , la petite fille prépare ses cadeaux tombés du ciel .
ça ne vous dérange pas que je vous emballe dans des sacs poubelles ?
je ne veux pas gâcher mon papier fantaisie , dit la petite fille .
non , non , on a l' habitude , dit Chien Pourri , tout ému .
au fait , je m' appelle MarieNoëlle et mon frère Jean Noël .
vous êtes de la famille de Chien Noël ?
demande Chien Pourri .
ne l' écoutez pas , dit Chaplapla , il raconte n' importe quoi .
le chat , si jamais mon frère te le demande , tu n' auras qu' à dire que tu es un hamster , il adore ces petits rongeurs .
chien Pourri et Chaplapla sont déposés au pied du sapin .
chien Pourri , enfermé dans son sac poubelle , ne peut s' empêcher de pleurer en pensant à tous les Noëls qu' il a passés dans sa benne à ordures .
tu te rends compte , Chaplapla , un enfant va me recevoir en cadeau !
ne t' emballe pas , Chien Pourri .
mais je suis déjà emballé .
la maman , Noëlle Noëlle , et le papa , Pierre Noël , invitent les enfants à ouvrir leurs paquets .
marie noëlle , Jean Noël , c' est l' heure !
mais Jean Noël n' est pas encore rentré .
que fait mon fils ?
demande Noëlle Noëlle .
excusez moi , dit Jean Noël , qui arrive essou é .
j' avais une dernière course à faire .
comme à chaque Noël , NoëlleNoëlle et Pierre Noël distribuent une encyclopédie et un atlas remis à jour à leurs petits chéris , puis les encouragent à s' O rir leurs cadeaux .
jean noël tire sur le fil rouge de son sac poubelle et s' écrie :
quelle horreur , une serpillière !
je suis un chien , dit Chien Pourri .
et lui , c' est quoi ?
un rat alors ?
je suis un hamster , dit Chaplapla .
vous me prenez pour un âne tous les deux ?
devant ses cadeaux et le rire de sa soeur , Jean Noël serre les dents et prépare sa vengeance .
ne fais pas la tête , dit PierreNoël , il faut accepter ce que ta soeur Marie Noëlle t' a O ert .
n' importe quoi , c' est ChienNoël qui apporte les cadeaux , dit Chien Pourri .