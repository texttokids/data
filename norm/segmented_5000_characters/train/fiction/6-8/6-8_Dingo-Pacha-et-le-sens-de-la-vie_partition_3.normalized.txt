il était une fois une bouse , et dans cette bouse , quatre petits .
quatre bébés linottes , qui s' appelaient Lourdaud , Solo , Costaud et Dingo .
ils avaient bien grandi depuis que Dingo était tombé du nid et ils avaient appris tout ce que les linottes doivent savoir .
c' est à dire pas grand chose .
ils savaient voler , capturer des bestioles pour le gouter , se baigner dans les flaques quand il faisait trop chaud et plonger dans les bouses de Vénus , la vache , quand ils avaient besoin de se réchauffer .
c' est quand même bizarre , les oiseaux , dit Pacha à Vénus , en regardant Dingo et ses frères patauger dans la crotte .
oh , tu sais , dit Vénus , qui avait autant de sagesse dans la tête que de paresse dans le corps , nous sommes tous un peu bizarres si on y réfléchit .
l' important , c' est ...
l' important c' est ?
demanda Pacha , impatient de savoir ce qui comptait réellement .
mais Vénus , la paresseuse Vénus , s' était endormie .
fatigué par tant d' aventures et de découvertes , Pacha le chat se roula en boule contre le ventre chaud de sa nouvelle amie et se dit que , finalement , il avait tout son temps pour découvrir le sens de la vie , bercé par de joyeux cuicui