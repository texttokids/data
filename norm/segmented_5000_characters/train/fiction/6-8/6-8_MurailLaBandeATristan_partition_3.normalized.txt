la boutique de farces et attrapes s' appelle Au crocodile bleu .
elle est super !
en vitrine , on voit des masques du Président et puis de Frankenstein .
il y a des fausses barbes , des faux nez , des faux ...
oh , là , regarde !
a dit Andrès , en me donnant un coup de coude .
il me montrait des faux ...
je n' ose pas dire quoi .
et alors ?
a dit Karine , j' en aurai moi , quand je serai grande .
des vrais seins comme Maman .
Andrès a ouvert de grands yeux .
je me suis énervé :
bon , on rentre ou quoi ?
la dame du Crocodile bleu est très gentille .
comme elle dit , elle connait bien les enfants .
elle répète tout le temps :
ne touche pas avec les mains .
tu me dis ce que tu veux .
nous , on voulait une crotte en plastique , des petits pétards et de la fausse encre .
on croit qu' elle tache et elle ne tache pas .
et une autre crotte , a dit ma soeur en sortant son argent .
pour quoi faire ?
j' ai une idée .
on est ressortis du Crocodile bleu et on a partagé .
Andrès aime surtout les pétards .
j' ai encore demandé à Karine :
qu' est ce que tu vas faire avec l' autre crotte ?
c' est mon idée .
tu verras .
non .
tu dis quoi ou je te tape .
c' est pour mettre dans la boite aux lettres de Pas de Chance .
comme ça , il avance la main pour prendre son courrier , et qu' est ce qu' il attrape ?
on a bien ri en revenant à la maison .
on imitait le voisin qui avance la main et qui attrape la crotte .
Andrès faisait trop bien sa tête .
moi et Karine , on est allés jusqu' aux boites aux lettres dans le couloir .
on riait et on avait peur en même temps .
attention , la concierge !
elle est passée devant nous .
bonjour , les enfants !
bonjour , madame !
elle est rentrée dans sa loge .
j' ai chuchoté :
bon , allez , vas y !
Karine a sorti la crotte de sa poche .
mais elle riait tellement qu' elle l' a fait tomber derrière les poubelles .
dépêche toi , j' ai envie de faire pipi !
alors , vite , Karine l' a glissée dans la boite , et il était temps parce que quelqu' un descendait l' escalier .
justement , c' était Pas de Chance .
il nous a regardés .
j' étais tellement affolé que j' ai dit :
bonjour , monsieur !
alors que d' habitude , on ne lui dit pas bonjour parce qu' on est fâchés avec .
il a souri et il m' a répondu :
bonjour , petit !
on a monté l' escalier quatre à quatre et j' ai couru jusqu' aux cabinets .
mais c' était un peu trop tard .
j' ai mal dormi pendant la nuit .
si mon maître , c' était Yvette , la maîtresse des CP , je n' aurais pas peur de lui faire une farce .
mais monsieur Languepin est trop sévère !
il frotte la tête des élèves avec son poing .
il appelle ça " passer un savon " .
à moi , il ne l' a jamais fait .
c' est Maman qui a ouvert la porte de l' appartement en premier , ce matin .
elle était en retard pour son travail comme presque tous les jours .
sur le palier , elle a poussé un cri .
on a couru , moi et Karine , pour voir ce qui se passait .
ah !
j' ai mis le pied dans la M ...
mais , c' est une fausse !
maman s' est baissée .
c' était notre crotte en plastique .
Karine m' a dit à l' oreille :
c' est de la magie .
moi , je crois plutôt que c' est le voisin .
pour mettre la crotte sur la chaise de monsieur Languepin , j' avais mon plan .
en fait , c' était le même que celui de Karine .
j' ai fait semblant d' oublier mon blouson et je suis remonté en classe .
je n' ai jamais passé une récréation aussi affreuse que celle là .
j' aurais voulu retourner dans la classe pour tout enlever .
Andrès s' est écrié :
qu' est ce qu' il va être en colère , le maître !
j' aurais voulu que la récréation ne finisse plus .
j' avais mal au ventre .
puis la cloche a sonné .
en rang , les CE deux !
on est retournés à notre place .
monsieur Languepin a dit comme d' habitude :
un peu de calme , maintenant .
la récréation est terminée .
il s' est approché de son bureau .
j' ai eu envie de crier : " non ! " je me suis mordu les lèvres .
le maître a tiré sa chaise pour s' asseoir .
il a fait des grands , grands yeux comme Andrès , puis il les a plissés , puis il les a rouverts .
il nous a regardés et il a dit :
qu' est ce que c' est que ça ?
il a mis la chaise devant tout le monde et il a répété très fort :
ça ?
il y a des enfants qui ont ri .
mais le maître a froncé les sourcils et il y a eu un grand silence dans la classe .
un silence de mort , comme on dit .
qui a mis ça ?
j' ai senti tous mes cheveux qui se redressaient .
Nathalie m' a regardé .
elle savait que c' était moi .
d' autres enfants se tournaient vers moi .
Nathalie leur avait surement dit .
elle est dégoutante , cette fille .
je ne l' aime plus du tout .
j' attends , a dit le maître .
ou le coupable se dénonce ou je punis toute la classe .
alors , je me suis levé .
j' avais les jambes toutes faibles et ma bouche tremblait .
je ne pouvais pas l' empêcher de trembler .
j' ai dit :
c' est moi .
le maître a eu l' air très étonné :
Tristan !
mais où allons nous si les bons élèves s' y mettent ?
il m' a enlevé quatre points de conduite et il m' a donné toute une page du livre de lecture à recopier pour demain .
j' ai un peu pleuré mais j' étais content de ne plus avoir mal dans le ventre .
ce qui m' a consolé aussi , c' est que le maître a dit que j' étais un bon élève .
j' ai commencé ma copie à la récré de la cantine en m' appuyant sur un banc .
Nathalie s' est accroupie près de moi .
elle m' a dit :
viens .
on va se cacher .
je vais t' en écrire la moitié .
je ne savais pas qu' elle était si gentille .
maintenant , c' est ma fiancée .