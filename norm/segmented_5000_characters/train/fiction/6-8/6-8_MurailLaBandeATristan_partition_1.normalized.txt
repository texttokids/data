cet après midi , j' ai fait plein de fautes à mon autodictée .
c' est cette histoire de liste de guerre qui me tourne dans la tête .
je ne sais pas quoi faire .
si j' étais dans la bande à Jujube , je pourrais me faire protéger .
c' est ce qu' il y a de bien avec les bandes .
quand on t' attaque , ceux de ta bande te défendent .
mais c' est difficile d' entrer dans la bande à Jujube , à cause des trois épreuves .
c' est Didier qui m' a expliqué , l' an dernier :
jujube te donne trois épreuves à faire .
c' est écrit sur un papier .
JeanFrançois m' a fait voir son papier pour la première épreuve .
c' était : " mettre une fausse crotte en plastique sur la chaise de la maîtresse . " jean françois n' a jamais passé la première épreuve .
marc a dû réussir les trois épreuves parce qu' il a la carte secrète de la bande .
elle est super , sa carte !
carte secrète om de code : Dazul Camembert Paris , trois octobre cette carte s' autodétruira dans cent ans .
au dos de la carte , il y a une tête de mort .
je voudrais bien faire partie de la bande à Jujube , mais je n' aurai jamais le courage de passer la première épreuve .
il est très sévère , monsieur Languepin .
j' ai toujours eu les maîtres sévères de l' école .
à chaque fois , dans l' autre classe , ils avaient la maîtresse gentille .
mon papa m' a dit :
c' est la faute à pas de chance .
c' est sa phrase quand j' ai un malheur .
quand j' étais petit , je croyais que Pas deChance , c' était le voisin du dessous , celui à qui on ne dit pas bonjour parce qu' on est fâchés avec .
pour rire , ma soeur et moi , on l' appelle toujours Pas de Chance et on met toutes les publicités qu' on reçoit dans sa boite aux lettres .
même , une fois , on a mis des papiers gras !
on s' amuse bien , moi et Karine .
elle n' est pas très jolie , ma soeur .
elle a un drôle de nez , mais ce n' est pas de sa faute .
j' aime mieux Nathalie comme visage .
mais comme caractère , je préfère ma soeur .
elle n' aime pas la bagarre .
alors , quand elle m' énerve , je lui dis : " tu arrêtes ou je te tape ! " elle s' arrête tout de suite .
heureusement , parce que je n' aime pas me battre non plus .
moi , j' aime bien jouer avec les filles et avec les petits du CP comme Andrès .
Andrès est amoureux de ma soeur .
il est fou !
moi , je ne suis pas amoureux .
olivier , c' est le fiancé de Nathalie .
c' est lui qui le dit mais je crois qu' il se vante .
en fait , moi , je suis amoureux de quelqu' un , mais je ne dis pas qui c' est .
ce soir , à la maison , Karine a bien vu que j' avais des ennuis .
elle m' a dit :
c' est à cause de ton amoureuse ?
j' en ai pas .
si !
tu l' as écrit dans l' escalier , à côté des poubelles .
non !
ça commence par un N .
comme dans Na ...
na ...
tu fiches le camp ou je te tape !
là , je crois que je l' aurais vraiment tapée .
en plus , ce n' est pas vrai .
je n' ai rien écrit du tout .
après le diner , comme c' est mardi , on a eu le droit de regarder la télé .
on voulait voir Les Incorruptibles .
c' est une histoire qui se passe aux États Unis .
il y a un chef de la police , Eliot Ness , qui chasse tout le temps les bandits .
il a un revolver sous la veste .
je vais essayer de m' en fabriquer un comme ça , avec une ceinture .
toujours de la violence , a dit Maman .
est ce qu' ils ont besoin de montrer toute cette violence ?
ça donne des idées aux jeunes ...
moi , j' ai pensé : " des idées de liste de guerre " , et ça m' a un peu gâché ma soirée .
c' est quoi , incorruptible ?
a demandé Karine .
elle demande toujours des trucs !
c' est pour ça qu' elle est intelligente .
incorruptible , a dit mon papa , c' est quelqu' un qu' on ne peut pas corrompre .
mon papa n' est pas très fort pour répondre aux questions .
maman dit qu' il n' écoute pas quand on lui parle .
mercredi , j' ai trouvé comment mettre mon revolver sous le bras .
j' ai un revolver super avec un silencieux .
au lieu de faire " pan ! " tout haut , on fait " paw ! " tout bas .
c' est bien parce que , quand on tue les gens , ils ne s' en aperçoivent pas .
demain , je vais jouer à Eliot Ness et ses hommes avec Andrès .
on tuera Olivier de loin .
on fera " paw , paw ! " très , très bas .
chapitre trois : il faut que je me venge !