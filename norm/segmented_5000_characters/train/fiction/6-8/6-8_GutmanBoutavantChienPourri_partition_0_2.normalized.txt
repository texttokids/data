et toi , vieux chien galeux , on te réserve un sort particulier .
ton odeur intéresse un client .
on va te presser comme une serpillière pour faire du jus de chien .
un parfum plus exactement , que nous appellerons Chien de Paris , ajoute un des bandits .
tu entends ça , Chaplapla ?
je vais devenir célèbre !
mais Chaplapla ne répond pas , écrasé entre un lama sans laine et un zèbre sans rayures .
la camionnette s' arrête à la lisière d' une forêt , devant :
tout le monde descend !
crie la petite fille .
le canard boiteux , l' âne bâté , le perroquet muet , le manchot unijambiste , la tortue sans carapace et le pigeon sans aile descendent les premiers .
chouette , un musée autorisé aux chiens !
apprécie Chien Pourri .
viens , Chaplala , il y aura peut être des croquettes et la Joconde !
mais Chaplapla se fait aussi plat qu' il peut pour ne pas se faire remarquer .
le premier bandit dit :
nous allons vous trier .
le deuxième bandit dit :
certains seront empaillés et finiront au musée .
le troisième bandit dit :
les autres seront répartis selon leur utilité .
avec mon physique , ils vont me clouer au mur ou me transformer en dessous de plat , dit Chaplapla .
et moi , en canard W C , dit le canard boiteux .
et moi , en tir au pigeon , dit le pigeon sans aile .
" empaillés " , c' est lorsqu' on boit à la paille ?
demande Chien Pourri .
à l' entrée du musée , Chien Pourri reconnait le caniche à frange et le basset à petit manteau .
bonjour les amis !
dit Chien Pourri .
comment va votre gentil maître ?
il nous a vendus comme des saucisses , mais on a réussi à s' enfuir .
je ne comprends pas , dit Chien Pourri , le restaurant n' était pas bon ?
ne faites pas attention à lui , dit Chaplapla .
en tout cas , méfiez vous des croquettes de la petite fille sans lacets .
elles vous assomment , dit le basset .
quelqu' un vous a tapé sur la tête ?
demande Chien Pourri .
moi , je ne reste pas là , dit Chaplapla .
viens , Chien Pourri , fuyons !
ah non , je ne pars pas sans mon parfum Chien de Paris !
Chaplapla n' a pas le temps de lui expliquer qu' il court un grand danger , que déjà les trois bandits les encerclent : le premier bandit dit :
je prends le chat aplati , je connais un collectionneur d' oeufs sur le plat .
le deuxième bandit dit :
et moi le perroquet muet , je connais un collectionneur sourd comme un pot .
le troisième bandit dit :
moi , je prends mon temps , je ne sais pas lequel choisir .
et moi , je voudrais m' occuper de Chien Pourri !
dit la petite fille aux souliers rouges .
aussitôt , les mouches qui tournent autour de Chien Pourri s' arrêtent de voler et tombent sur son museau .
bien sûr , presse le tant que tu veux , si ça peut te faire plaisir .
ah , les enfants sont incorrigibles !
et les trois bandits rient tous en choeur .
" que ces maîtres sont joyeux ! pense Chien Pourri . si un jour je trouve un os , je le rapporterai pour leur musée . " mais , une fois seul avec la petite fille , Chien Pourri est déçu : elle ne le presse pas comme un citron .
ai je fait quelque chose de mal ?
demande t il .
au contraire , gentil toutou .
tu vas m' aider à m' enfuir .
mais qu' est ce que vous avez tous à vouloir partir ?
j' ai été kidnappée par ces a reux bandits et si je ne leur obéis pas , je ne reverrai plus jamais mes parents .
ton histoire est très triste , petite fille , mais à ta place je continuerais à obéir à mes gentils maîtres .
ce ne sont pas mes maîtres , Chien Pourri , et je ne veux plus jamais faire de mal aux animaux !
mais comment retrouver tes parents ?
j' ai déjà du mal à retrouver ma poubelle !
j' ai du flair , Chien Pourri , je sais que derrière ton aspect de vieille moquette se cache un chien merveilleux .
bon , si je t' aide , tu m' apprendras à faire le beau ?
non , ça , c' est impossible .
alors j' aurai droit à un susucre ?
oui , Chien Pourri , à tous les sucres que tu voudras .
chien Pourri pleure de joie , car sous son pelage nauséabond se cache un chien sensible et courageux qui adore les su sucres .
" je vais tous les sauver ! " se dit il .
chien Pourri tombe dans un trou tel un maître de l' évasion , Chien Pourri creuse un tunnel pour ses compagnons d' infortune pendant que la petite fille prépare un cake empoisonné aux croquettes pour les bandits .
quand minuit sonne et que les bandits dorment , chien et chat se préparent à partir .
Chaplapla , sache que si cela tourne mal , tu auras été le chat le plus plat que j' aie jamais connu .
et toi , le chien le plus pourri .
les débuts dans le tunnel sont di ciles , le canard boiteux retarde tout le monde et le lama n' arrête pas de cracher .
et puis , Chien Pourri confond sa droite et sa gauche , sa tête et sa queue .
mais il se répète : " je suis l' ouvre boîte de la conserve , le berger des moutons , la coquille de l' escargot , et je sauverai mes amis ! " alors , dans l' obscurité , Chien Pourri sent , pour une fois , autre chose que la sardine , il sent que la ville n' est pas loin , et il gratte la terre pour apercevoir la lumière d' un réverbère et trouver la sortie !