Mister Gershwin
les gratte ciel de la musique
extrait un : un le piano .
trois
je vivais tranquille , beaucoup trop tranquille et solitaire .
on m' avait couvert d' une nappe brodée pour me protéger de la poussière .
je vivais donc dans l' obscurité depuis des années , mes cordes ne vibraient pas , mes marteaux ne frappaient pas , plus aucune caresse sur mes touches .
j' attendais que l' on me chatouille un peu .
extrait deux : deux le piano chatouille .
trois
un jour de juillet dix neuf cent dix, on vient me bousculer , me hisser , me déménager .
j' ai un peu peur et j' étouffe sous le monceau de couvertures .
mais enfin quelque chose m' arrive !
ils s' y mettent à quatre pour me soulever !
c' est que je pèse bien deux cents kilos !
on me juche sur un véhicule tiré par un pauvre cheval , ça cahote un bon moment , puis je sens que je suis suspendu entre ciel et terre .
quand enfin je suis posé sur le sol , on me déshabille , on me bichonne .
je suis dans une pièce minuscule qui doit servir à la fois de salon , de chambre à coucher et de salle à manger .
ça parle et ça parle !
c' est pour toi , Ira , mon cher fils , mon grand .
tu vas apprendre à jouer du piano , dit une femme avec un fort accent russe .
je le reconnais , cet accent : ma famille précédente , une famille juive qui avait fui les pogroms et la misère , avait le même .
le garçon , un adolescent d' environ quatorze ans , sans l' ombre d' un accent , l' air sérieux avec ses lunettes rondes , s' assoit sur le tabouret .
il se tortille , s' étouffe presque à la vue de mon bois massif , puis se lève en faisant la grimace .
ben , euh ...
un deuxième garçon , plus jeune qu' Ira , les cheveux en pagaille et l' air espiègle , me saute dessus .
maman , je peux essayer ?
non , George , s' écrie la mère .
je n' ai pas dépensé tout cet argent pour un petit morveux comme toi !
Ecarte toi de là et plus vite que ça !
et sur le palier , j' entends deux femmes , des voisines sans doute .
l' une chuchote à l' autre devant la porte ouverte :
madame Gershwin a de beaux enfants , de vrais petits américains !
mais qu' est ce qu' elle va faire de George ?
quel petit chenapan , celui là !
soudain , un homme échevelé à l' air fatigué entre en claquant la porte :
j' ai une surprise !
s' écrie celui qui s' avère être le père des enfants .
mais qu' est ce qui se passe ici ?
d' où vient ce piano ?
Morris , je t' avais prévenu , voyons !
une famille bien doit avoir un piano !
répond la mère avec autorité .
mais Rose , ça coute une fortune , un piano !
et moi , j' ai déjà un mal fou à nourrir cette famille !
il y a nourriture et nourriture .
il faut nourrir l' esprit , nous élever plus haut que terre , Morris .
ira !
ira !
reviens au piano !
George , descends immédiatement de ce tabouret !
pourquoi cries tu ainsi ?
pourquoi veux tu qu' il bouge du piano , puisqu' il y a un piano ?
comme moi , le père a bien vu que George semble être à sa place devant mon clavier , sous le charme de mes cinquante deux " dents " blanches et de mes trente six " tiges " noires !
avec jubilation , le garçon s' est mis à me tripoter et à extraire de mes tréfonds des sons tout à fait respectables , comme quelqu' un qui sait ce qu' il fait , comme si je faisais partie de lui .
extrait trois : prelude PIANO un
toute la famille s' est arrêtée de crier .
tous écoutent George avec stupéfaction .
et au bout d' un moment , des applaudissements nous parviennent de la rue .
c' est l' été , les fenêtres sont grandes ouvertes , et un attroupement s' est formé sur le trottoir pour écouter le prince qui a réveillé la belle au bois dormant .
je vis de nouveau .
ce petit est né rien que pour moi .
ensemble , nous allons chanter la vie .
et en avant la musique !
extrait quatre : Sweet and low down
le père , interloqué , demande à son fils :
comment tu sais tout ça , George ?
je ne sais pas ...
la musique coule de mes doigts ...
madame Gershwin , elle , a plutôt l' air inquiet .
elle s' est rapprochée de son mari :
au fait , Morris ...
quelle est ta surprise ?
rose chérie , mes enfants , j' ai un nouveau travail .
on va déménager !
encore !
oh , non !
et voilà Arthur , le frère cadet de George , qui se met à parler des amis qu' il va laisser , et Frances , la petite dernière , qui éclate en sanglots .
mais , finalement , j' ai comme l' impression que George et Ira , eux , sont vraiment excités à l' idée de changer de quartier !
j' allais bien les connaître , au fil des ans , les six membres de cette famille .
le père avait cette folie de vouloir à tout prix aller à son travail à pied .
et c' est qu' il changeait souvent de commerce , Morris Gershwin !
j' ai entendu Rose se plaindre un jour qu' elle avait dû déménager plus de trente fois dans sa vie entre Manhattan et Brooklyn .
heureusement , jamais ils ne m' ont oublié !
ils ont même eu la gentillesse de me faire accorder quand c' était nécessaire .
quant à moi , je me suis douté dès le premier jour que j' allais vivre de grands moments en leur compagnie .
extrait cinq : when do we dance .
trois