et maintenant ?
demande Chien Pourri .
il n' y a plus qu' à attendre un miracle , dit le gamin de Paris .
mais parfois , les miracles ne sont pas ceux qu' on attend .
au fond du bar , un petit monsieur à lunettes leur conseille :
vous devriez essayer Les Folies Ménagères , moi un jour , j' y ai retrouvé ma femme , elle y dansait le french cancan .
cette fois , le gamin de Paris sent briller à travers sa poche trouée son bout d' étoile de mer .
les Folies Ménagères Devant le célèbre cabaret parisien , un bonimenteur harangue la foule .
approchez ladies and gentlemen , mesdames et messieurs , chats et chiens , cats and dogs .
bienvenue , welcome dans le plus faymousse club de Parisse : les Folies Ménagères .
vous pourrez admirer le fameux lave bretelles , la machine à bigoudis et le légendaire stylo Biche !
nous cherchons une petite femme de Paris , dit Chien Pourri .
entre , brave toutou , on trouve tout aux Folies Ménagères !
ça m' a l' air bizarre , freine Chaplapla .
ne t' inquiète pas , c' est gratuit pour les chiens et les chats avant vingt heures , dit Chien Pourri .
le Poubelle Tour s' installe à une table en plein courant d' air , derrière un poteau et à côté des toilettes .
on est bien ici , se félicite Chien Pourri .
on ne voit rien , se plaint le gamin de Paris .
il n' entend rien non plus , car , à la table du Caniche Tour , Aristide Bruyant fait un vacarme épouvantable en tapant dans ses mains et en chantant .
And now , ladies et gentlemen , dit le bonimenteur , pour vous présenter la machine à laver , j' appelle la faboulousse Jambe Courte !
qu' est ce qu' il a dit ?
demande le gamin de Paris .
je n' entends rien , dit Chaplapla .
je ne vois rien , dit le gamin de Paris .
And now , sous vos encouragements , Jambe Courte va lever la jambe pour ouvrir la machine !
c' est une vedette , on l' applaudit bien fort !
malheureusement , le gamin de Paris ne peut pas voir sa maman sur scène , elle est cachée par le poteau et seule sa jambe dépasse .
alors qui veut de cette machine à laver ?
demande Aristide Bruyant .
moi , dit le labrador , je vais la rapporter à mes parents .
fayot !
crie le caniche à frange .
et maintenant , le lot suivant , une magnifique machine à bigoudis ...
ça m' intéresse , dit le caniche à frange en levant la frange .
le spectacle va bon train , le Caniche Tour est comblé .
jambe Courte regagne les coulisses pour mettre une nouvelle robe longue , mais derrière son poteau , le gamin de Paris se morfond encore :
j' ai perdu mon papa , j' ai perdu ma maman ...
il te faudrait une machine pour retrouver les parents , hurle Aristide Bruyant .
ha , Ha !
je vois que monsieur a entendu parler du stylo Biche qui retrouve tout .
il me faudrait un volontaire , demande le bonimenteur .
moi !
dit Chien Pourri .
" peut être que je pourrai retrouver ma chaussette . "
ce sera donc un retrouve toutou , plaisante le bonimenteur .
on l' applaudit bien fort !
vous voyez ce stylo ?
rien de plus normal .
et pour montrer qu' il n' y a aucun trucage , je vais demander à ce toutou d' égout de le toucher .
c' est un vrai stylo Biche , quatre couleurs , confirme Chien Pourri .
hier , j' ai perdu mon portefeuille et grâce à ce stylo Biche , je vais le retrouver !
allez , brave toutou , fais le tourner !
là où il s' arrêtera , le portefeuille apparaitra !
chien Pourri donne un coup de patte au stylo Biche , les spectateurs retiennent leur sou E .
stop !
fait le bonimenteur qui écrase le stylo avec son pied .
comme par hasard , le stylo Biche s' arrête pile devant le bonimenteur qui feint la surprise , en fouillant dans sa poche .
se pourrait il ...

mais oui , c' est mon portefeuille !
il était dans ma poche .
ah oui , vraiment ce stylo Biche retrouve tout !
" il doit y avoir un truc , mais je ne vois pas " , se dit Chien Pourri , perplexe .
pour une bouchée de pain , il sera à vous et retrouvera tout ce que vous voudrez .
cette fois , le gamin de Paris a bien entendu et il s' avance avec son tableau dans les bras et son bout d' étoile de mer pour l' échanger contre le stylo Biche .
" je vais retrouver mon papa et ma maman " , espère t il .
allons gamin , rentre chez ta mère et remballe ta camelote !
mais j' ai perdu ma maman ...
et maintenant , le spectacle continue , l' interrompt le bonimenteur .
pour vous présenter la gomme kangourou , j' appelle la célébrissime Jambe Courte dans sa nouvelle robe longue !
mais ...
mais , c' est ma maman !
hurle le gamin de Paris , en voyant sa mère .
c' est ça et moi , je suis ton papa ?
se moque le bonimenteur .
de toute façon , cette maman n' est pas à vendre !
mais rien ne peut interrompre les retrouvailles d' une mère avec son fils .
le gamin de Paris saute dans les bras de Jambe Courte sous les applaudissements de la foule .
je t' ai perdu dans la rue car j' avais les jambes trop courtes pour te rattraper , lui dit elle en pleurant .
cette fois , même le bonimenteur se laisse attendrir par ce spectacle .
tu diras à ton papa que les bonimenteurs ont du coeur , tu peux partir avec ta maman .
mais j' ai perdu mon papa ...