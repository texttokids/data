A l' automne les arbres changent .
les feuilles deviennent orange puis rouges .
paul est entré à l' école depuis peu de temps car les vacances viennent de se terminer .
le vent souffle plus fort et peu à peu le froid arrive .
paul doit mettre un manteau léger pour sortir .
une fois couvert , Paul part à la chasse aux escargots .
le temps des vendanges est venu , les raisins sont cueillis .
en automne , la nature déborde de fruits et légumes .
et la maman de Paul en profite pour lui cuisiner des champignons et des potirons .
les écureuils font des réserves de nourriture pour l' hiver .
les sangliers sortent pour déguster les glands et les châtaignes .
la nature se transforme .
pour Paul , c' est le meilleur moment pour aller faire des balades en famille .
quand viendra la fin de l' automne , il sera temps pour Paul de penser à Noël et de réfléchir à sa liste de cadeaux pour le Père Noël .