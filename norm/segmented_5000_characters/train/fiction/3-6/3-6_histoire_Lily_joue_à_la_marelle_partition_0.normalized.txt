C' est l' heure de la récréation , et Lily a envie de s' amuser .
elle sort dans la cour avec ses copines et décide de jouer à la marelle .
les petites filles se mettent à la queue leu leu pour passer chacune leur tour .
c' est au tour de Lily !
la petite fille jette son caillou et saute .
quand tout à coup , sa chaussure glisse et Lily tombe sur le genou .
" aïe aïe aïe ! " crie Lily en pleurant et tenant son genou blessé .
ses copines appellent la maîtresse qui prend la fillette dans ses bras et la conduit à l' infirmerie .
en arrivant à l' infirmerie , la maîtresse assoit Lily sur une chaise .
l' infirmière regarde le genou de la petite fille et lui demande : " et bien alors , comment t' es tu fait ça ? "
" en jouant à la marelle . " répond Lily en sanglotant .
" ne t' en fais pas Lily , on va désinfecter ton bobo et mettre un pansement " explique l' infirmière .
" ça veut dire quoi désinfecter ? est ce que ça va faire mal ? " demande la fillette inquiète .
" ça va picoter un petit peu , mais rien de méchant ! " réconforte l' infirmière .
" je vais prendre de l' eau et du savon pour nettoyer ton genou et enlever le sang et les petits graviers . " l' infirmière imbibe un coton d' eau savonneuse qu' elle vient poser sur le genou blessé .
la petite fille frémit en sentant le coton tout froid .
mais pas le temps de crier que c' est déjà fini !
le genou est nettoyé et ne saigne plus .
Lily a même le droit à un joli pansement avec des papillons dessus .
et pour retrouver le sourire , quoi de mieux qu' une bonne sucette !
Lily est rassurée .
elle n' a plus mal au genou et va pouvoir montrer son joli pansement à ses copines .
sans oublier la sucette de la gentille infirmière qu' elle va pouvoir grignoter .
avec tout ça , le petit accident est vite oublié !