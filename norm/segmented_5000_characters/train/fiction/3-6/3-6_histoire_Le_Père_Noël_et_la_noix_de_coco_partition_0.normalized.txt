le père Noël est prêt à recevoir les demandes des enfants du monde entier .
cette année , avec sa tablette magique , il a décidé d' être connecté au monde .
justement , Pierre , un petit garçon qui habite en France , lui envoie un message .
pierre a une demande plutôt étonnante .
il voudrait offrir à son Papa une noix de coco .
le petit garçon sait que son Papa en a mangé en allant un jour des les îles .
le Père Noël fait ses valises et établit un plan de vol , il va falloir aller dans de nombreux pays pour apporter des cadeaux aux enfants .
" allez mes rennes , Chico et Roberta , en avant ! " le Père Noël vole au dessus des maisons , passe à côté de la lune et des nuages .
enfin , lorsque le jour se lève , les nuages s' effacent progressivement et les rennes du Père Noël descendent dans le ciel .
une île apparaît soudain .
les rennes ne pouvant pas atterrir sur le sable , le père Noël décide de prendre une planche sur son traineau et de se mettre en maillot .
il surfe sur la mer , évite les poissons et saute sur le sable .
là , il découvre des cocotiers par dizaines .
mais ils sont trop grands .
" comment atteindre les noix de Coco ? " , se demande le Père Noël .
fatigué , le Père Noël essaie avec son bonnet en le lançant vers les noix de Coco .
quel dommage !
le bonnet est trop léger pour atteindre le haut de l' arbre , il flotte dans l' air si bien qu' un oiseau l' attrape avant même qu' il ne retombe .
le Père Noël est furieux , il tape des pieds tandis que les oiseaux rient en voyant ses cheveux hérissés sur la tête .
il se dit qu' il ne peut pas revenir bredouille car Pierre doit avoir le cadeau demandé pour son Papa .
le Père Noël demande aux oiseaux moqueurs de l' aider .
ils se mettent à taper sur les noix de coco et des dizaines d' entre elles tombent sur le Père Noël .
tant et si bien que l' une d' entre elles se casse sur sa tête et lui fait un nouveau bonnet .
il récupère son surf , met une noix de Coco dans son bonnet et repart vers la mer .
quelques jours plus tard , il livre ( toujours en short ) la noix de Coco au pied du sapin de Pierre .