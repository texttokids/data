cette après midi , le soleil est au rendez vous .
Peter le pirate a jeté l' ancre et décide de faire une sieste sur le pont de son bateau .
au bout d' un moment , Peter est réveillé par des pleurs .
il regarde dans la mer .
rien à l' horizon .
Peter se retourne et aperçoit une silhouette sur un rocher .
il lève l' ancre et navigue vers elle .
c' est une sirène qui pleure en tenant sa nageoire serrée .
" que vous arrive t il ? pourquoi pleurez vous ? "
la sirène prend peur et plonge dans l' eau .
" non n' ayez pas peur , je veux seulement vous aider . "
la sirène sort de l' eau et dit " les pirates n' aident pas les sirènes , regardez ce qu' ils m' ont fait " .
elle montre sa nageoire dans laquelle s' est pris un hameçon .
" je ne suis pas un méchant pirate " , explique Peter , " ce qui m' intéresse se sont les trésors , pas les sirènes . remontez sur le rocher je vais essayer de retirer ce vilain crochet " .
la sirène hésite et finit par remonter .
Peter attrape la nageoire et tortille l' hameçon qui finit par sortir .
" vous avez réussi , et vous ne m' avez même pas fait mal ! " s' étonne la sirène .
elle attrape son collier sur lequel se trouve une perle qu' elle tend à Peter .
" prenez mon collier , pour vous remercier " .
elle donne le collier et plonge aussitôt dans l' océan .
Peter a gagné un nouveau trésor , et la fierté d' avoir sauvé une si jolie sirène .