Tom ne comprend pas pourquoi il a toujours des chaussures trop grandes .
sa maman prend toujours la taille de son pied .
alors que se passe t il ?
pourquoi les autres enfants de son âge ne se posent pas la même question ?
il demande à sa copine Nina .
elle lui explique qu' il s' agit sans doute d' une histoire de lacets .
Tom a toujours ses lacets défaits , " c' est pour cette raison que je tombe souvent ? " se demande t il .
le soir même il raconte cette histoire à sa maman .
" je vais trouver une solution . " , dit elle .
le lendemain , Tom se rend à l' école , avec de nouvelles chaussures .
les élèves se demandent pourquoi il est de si bonne humeur .
tout simplement parce que Tom a mis des chaussures avec des scratches .
à partir d' aujourd'hui , les problèmes de lacets , c' est terminé !