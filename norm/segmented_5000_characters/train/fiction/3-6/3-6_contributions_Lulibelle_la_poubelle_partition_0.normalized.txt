Lulibelle était une jolie poubelle .
et même très , très belle , d' après Augustin le petit coquin .
cet enfant n' avait de cesse de la bichonner , l' astiquer , la rentrer ou la sortir du garage , afin qu' elle n' ait ni trop chaud l' été , ni trop froid l' hiver .
il aimait même lui parler !
" comme tu es belle ! " , lui disait il en la rangeant .
mais Lulibelle , elle , malgré tout l' amour d' Augustin , rêvait de voyager : la mer , les îles , les poubelles des pays chauds ou des pays froids ...
puis , un jour , ce qui devait arriver , arriva !
augustin n' en crut pas ses yeux .
plus de Lulibelle .
quel chagrin !
il fallut faire intervenir papa et maman pour effectuer des recherches .
mais qui se soucie d' une poubelle ?
en fait , Lulibelle avait attendu le passage de Maxou , le camion poubelle pour s' accrocher et se faire emmener discrètement .
mais tout ne se passa pas comme prévu .
dans la vraie vie , c' est plus difficile que dans les rêves !
point d' île , ni d' igloo , à l' horizon , mais seulement les box sales et puants de la déchetterie .
papa et maman l' ont retrouvée !
retour à la case départ pour Lulibelle .
mais finalement quel bonheur de revoir Augustin !
chaussé de bottes et gants de caoutchouc , c' est avec l' éponge savonneuse et le jet d' eau qu' il l' a accueillie , du haut de ses quatre ans .
une fois propre , il lui a expliqué combien il allait la chouchouter davantage .
c' est ainsi que le petit garçon prit soin à chaque saison de coller des autocollants décoratifs sur Lulibelle , de lui aménager un coin bien à elle pour la ranger et de lui préparer chaque mois des dessins colorés de tous les pays du monde qu' elle aurait voulu connaître .
Lulibelle , quant à elle , jura qu' on ne l' y reprendrait plus !
et puis ...
qu' est ce qu' on est bien à la maison !
et quant à Augustin , d' après vous , quel métier a t il choisi ?
ingénieur en environnement , pardi !