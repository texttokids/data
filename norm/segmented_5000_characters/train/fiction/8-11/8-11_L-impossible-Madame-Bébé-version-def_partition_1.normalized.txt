à la fin des vacances d' été j' avais très envie d' entrer à la primaire .
je voulais apprendre à écrire de petites lettres rondes toutes bien attachées , comme Lucile .
je voulais qu' on m' abonne à un magazine de karaté que je pourrais lire toute seule .
maman n' avait pas voulu que je m' attache un foulard de ninja sur le front le matin du grand jour , mais j' avais emporté un bout de tissu noir découpé dans un vieux déguisement de pirate pour m' en fabriquer un à la récréation .
dans la rue qui menait à l' école , je sentais la main de Lucile dans la mienne .
elle me serrait un peu .
ça ne me dérangeait pas .
lucile a toujours le trac à la rentrée , je ne sais pas pourquoi .
elle dit qu' elle a peur de ne pas avoir d' amis ou que la maîtresse ne l' aime pas .
ma soeur est folle .
c' est impossible de ne pas l' aimer , vu qu' elle est parfaite .
chaque année je lui explique .
ça ne sert à rien , elle a peur quand même .
la veille , en me disant bonne nuit , elle m' avait demandé si je pouvais essayer d' être sage maintenant que j' avais six ans .
" pour quoi faire ? " j' ai demandé .
" pour rien , " a t elle répondu d' une voix très douce .
" juste comme ça , pour voir quel effet ça fait . pour changer . peut être que si tu essaies , ça te plaira . "
" ça ferait deux sages dans la famille , " lui ai je fait remarquer .
" c' est monotone si tout le monde est sage . je crois que je préfère rester comme je suis . "
" nana , tu te souviens de ce que je t' ai raconté ? madame Bébé ? elle surveille l' entrée et la sortie et aussi la cantine et parfois la cour . elle est terrible . "
" est ce qu' elle t' a déjà punie ? "
" non , " a reconnu Lucile .
" mais elle a déjà puni Kylian . alors que Kylian ne parle jamais , ne court jamais , et dit tout le temps pardon . "
" Kylian ? celui qui a une tête de je vous jure que c' est pas moi' ? chaque fois que je le vois au jardin j' ai envie de lui donner des coups . je l' aurais puni , moi aussi , à la place de Madame Bébé . je suis sure qu' on va très bien s' entendre , elle et moi . on a exactement les mêmes gouts , en fait . "
" Nanabébé . fais un effort quand même , d' accord ? juste le premier jour . "
j' adore quand Lucile m' appelle Nanabébé .
je lui ai promis d' essayer et pendant que je lui faisais un bisou , j' ai pensé que la terreur de l' école et moi , on avait non seulement les mêmes gouts , mais presque le même nom .
madame Bébé et Nanabébé , c' est pareil .
en passant la double porte vitrée de la primaire , le lendemain matin , j' ai levé le menton bien haut et j' ai dit : " bonjour Madame ! " très fort , mais très poliment aussi .
le concierge , Monsieur Le brillant , que je connais depuis toujours parce qu' il s' occupe aussi des maternelles , a fait une tête de pourquoi j' ai reçu une goutte de pluie alors que le soleil brille .
ce n' était pas à lui que je m' adressais .
c' était à la petite dame avec un chignon blond très tiré , du rouge à lèvres foncé qui débordait un peu , des ongles longs peints en rouge et une chemise noire en dentelle fermée jusqu' au cou .
personne n' avait eu besoin de me dire que c' était Madame Bébé .
je l' avais reconnue au premier regard .
elle avait une tête de Si tu te crois maline , tu te trompes complètement .
elle a baissé les yeux sur moi en entendant mon " bonjour madame " impeccable .
sans sourire .
sans presque bouger .
et j' ai pensé qu' elle était comme une araignée , immobile dans un coin de sa toile .
aucun problème pour moi , j' adore les araignées .
les parents avaient le droit d' entrer dans l' école avec les CP , mais j' avais dit à mon père et à ma mère que ce n' était pas la peine .
lucile était là , et tous mes copains de l' année dernière aussi .
je me suis quand même retournée une seconde , juste après avoir franchi le seuil , pour leur faire un petit signe et je les ai vus , papa et maman , sur le trottoir qui nous regardaient , Lucile et moi , avec des têtes de pourvu que le dragon qui vient de les avaler ne les digère pas tout de suite .
ils avaient les larmes aux yeux .
ça m' a fait tout bizarre .
j' ai voulu leur faire un bisou de la main pour les consoler , mais une voix juste au dessus de ma tête à dit :
" pour qui te prends tu de stationner ainsi au milieu de l' entrée ? tu bloques le passage . cela t' amuse peut être de créer un embouteillage dès le premier jour ? mets toi sur le côté ! tout de suite ! pas de discussion ! "
" mais Madame Bébé , " a dit Lucile d' une voix tremblante .
" c' est une petite de CP , elle est perdue . "
madame Bébé a fait une tête de Tiens , voilà que mon chien sait parler .
elle a froncé ses petits yeux bleus perçants et a dit à Lucile :
" tu vas te taire , oui ? "
si , à cet instant , elle avait puni Lucile , je crois que je lui aurais fait un mawashi geri , coup de pied dans la figure , mais heureusement pour elle , elle ne s' en est prise qu' à moi .
chapitre trois : arrêter toutes les guerres dans le monde ou finir son assiette