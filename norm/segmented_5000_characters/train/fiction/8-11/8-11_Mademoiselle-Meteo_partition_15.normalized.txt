onze .
accalmie
axel est de bon conseil : " tu continues ton travail dignement et tu l' ignores . "
mais c' est la totalité du monde à l' extérieur de ma petite salle de classe .
tiens bon .
je ne sais pas si je suis assez forte .
cette exclusion , même du directeur qui a toujours été un ami , me mine .
et si on préparait un beau mariage ?
on ne se marie pas pour fuir une situation pourrie .
non , il faut que je résolve ce mystère .
mais j' aimerais que le mariage ait lieu en juin .
Alizée devrait lui dire qu' elle ne peut pas se marier comme un marché conclu .
elle a trop envie de se blottir contre lui , mais elle est trop timide pour prendre des initiatives ailleurs que dans sa classe .
elle ne dit rien à Axel de ses doutes .
pourquoi ne veut il la toucher ?
elle se plonge dans l' élaboration d' une demande pédagogique pour faire la classe de la mer .
bien sûr , il faut l' autorisation du directeur , des parents et même peut être de cette Béatrice Morel , mais elle est d' attaque pour faire tout ce qu' il faut pour promener ses élèves en dehors des murs de l' école .
quand Célia lui dit en pleurant que ses parents ne la laisseraient jamais partir à cause de son asthme , Alizée comprend l' étendu de la complexité de son projet .
mais elle le poursuit .
pour faire n' importe quoi dans la vie , il faut foncer .
elle dépose le dossier chez le directeur qui , comme tout le monde , ne lui parle plus .
elle ne sait toujours pas de quoi elle est accusée .
elle a enfin trouvé un météorologue pour venir parler à sa classe de son métier .
c' est grâce à Doina qui a rencontré ce jeune homme franco britannique à Londres .
le météorologue suisse d' Alizée avait répondu qu' il n' avait pas le temps en ce moment .
roman Johnson et Alizée sont mutuellement impressionnés .
il est aussi beau qu' elle est belle .
en plus il a ce charme chaleureux rare qui accompagne les fossettes , les yeux couleur du ciel à Nice et des cheveux aussi noirs que ceux d' Alizée .
votre nom est vraiment Alizée Tramontane ?
lui demande t il devant les élèves qui se sont aperçus du petit tremblement de terre entre leur maîtresse et le météorologue .
on va renverser les choses : je vous donne la réponse et après vous me dites la question à laquelle j' ai répondu .
le météorologue est un spécialiste des phénomènes atmosphériques .
il étudie les pressions , les vents , les températures et tous les mouvements de l' atmosphère , leurs causes et leurs effets .
c' est un scientifique de haut niveau .
il établi des prévisions .
c' est quoi un météorologue ?
disent les enfants en choeur .
le météorologue n' est pas le présentateur qui fait la pluie et le beau temps à la télé .
il y a les observateurs , techniciens supérieurs , qui travaillent dans une station météo et qui se chargent de transmettre les données aux ingénieurs des travaux qui les analysent .
quand aux ingénieurs de la météorologie , ils font de la recherche ou dirige des services .
une fois les informations traitées , elles sont diffusées au grand public : transports aériens , maritimes ou terrestres , pêcheurs , agriculteurs , tourisme , santé publique .
est ce que vous êtes à la télé ?
les techniciens et ingénieurs de Météo France sont formés principalement à l' Ecole nationale de la météorologie ( ENM ) basée à Toulouse .
ils sont recrutés sur concours .
quelle formation et éducation ont les météorologues ?
les techniciens passent un concours de niveau bac S : maths , physique , informatique , anglais principalement .
une trentaine de postes par an .
les enfants marchent très bien dans les formulations des questions .
ces scientifiques utilisent des équipements sophistiqués ( satellites , ordinateurs puissants ...
) pour observer et relever des données terrestres et atmosphériques sur leurs écrans et leurs graphiques .
les élèves trouvent toujours la question juste .
Y a t il une question que je n' ai pas traitée ?
demande l' invité .
quel est votre statut ?
demande Alizée .
les météorologues sont des fonctionnaires de Météo France ou travaillent pour l' armée .
en règle générale , ils sont en poste dans les centres de calculs ou partent en mission .
les conditions de travail peuvent être rudes .
le technicien météo doit pouvoir enchainer des horaires en continu ( mesures de nuit , jours fériés ) et parfois pendant de longues périodes d' isolement .
la question qu' Alizée meurt d' envie de poser mais n' ose pas est " est ce que vous pourriez vous dégager pour venir en classe de mer avec nous ? "
et puis elle trouve son courage et lui pose cette question .
roman promène ses yeux sur Alizée de la tête aux pieds en passant par ses longues jambes et lui dit : " oui , ça pourrait être intéressant pour moi aussi de créer un programme " mer et météo " . oui , alors , quelles sont les dates ? "
douze .
tempête