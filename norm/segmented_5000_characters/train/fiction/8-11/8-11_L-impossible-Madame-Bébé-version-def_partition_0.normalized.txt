l' impossible madame bebe
Agnès Desarthe
chapitre un : je n' ai peur de rien , ni de personne
je m' appelle Nadejda .
ça veut dire " espoir " en russe .
mais tout le monde m' appelle Nana .
ça ne veut rien dire , et ça me va bien .
nana , comme dans nananère .
ma grande soeur s' appelle Lucile , ça veut dire lumière en je ne sais plus quelle langue et ça lui va bien aussi .
personne ne l' appelle Lulu , parce qu' elle est très sage , très grande et qu' elle a de longs cils .
ma soeur est calme .
moi , non .
ma soeur fait de la danse classique .
moi , je fais du karaté .
ma soeur entre en CM un et moi , je suis en CP .
pendant l' été , Lucile est venue dans ma chambre et elle m' a dit :
" nana , il faut que je te parle . "
" les parents vont divorcer , c' est ça ? "
" non . "
" mamie est morte ? "
" non . "
" alors qu' est ce qu' il y a de si grave ? tu fais une tête de mauvaise nouvelle . "
lucile a souri .
ça m' a rassurée .
je n' avais pas envie qu' une tragédie éclate dans ma vie .
ma vie est très bien comme elle est
" c' est à propos de l' année prochaine , " m' a dit Lucile .
" de la rentrée . "
" je sais déjà lire , " ai je rappelé à ma soeur .
" je crois que je n' aurai aucun problème en CP . "
" ce n' est pas au travail que je pense , " a dit Lucile en se tordant un peu les mains .
elle avait l' air très gêné .
" c' est quelque chose à propos des cabinets ? " je lui ai demandé .
" pourquoi tu dis ça ? " a t elle fait , surprise .
" à cause de ta tête . tout à l' heure tu avais une tête de mauvaise nouvelle et là , tu as une tête de cabinet . "
lucile s' est mise à rire .
elle sait que je suis douée pour parler des têtes qu' ont les gens .
c' est une vraie collection : tête de pain rassis , tête de pizza froide , tête de cauchemar , tête de mon chien est plus mignon que le tien , tête de trois plus trois égale huit ...
et caetera
" ça n' a rien à voir avec les cabinets " a repris Lucile .
" mais c' est quelque chose qui se passe à l' école . quelque chose qui n' existait pas en maternelle . "
" les devoirs ? "
" laisse moi parler , Nana . pour une fois . "
c' est vrai que je ne laisse jamais Lucile parler .
ni personne d' autre d' ailleurs .
je préfère parler moi .
j' ai tellement de choses à exprimer que si je ne les dis pas tout de suite , j' ai l' impression que mon cerveau va exploser .
cette fois , j' ai fait un effort , parce que j' ai vu que Lucile souffrait .
elle avait une tête de chaussettes mouillées en plein hiver .
" cette chose dont il faut que je te parle , c' est en fait une personne , " a dit Lucile .
" une personne qui travaille à l' école mais qui n' est pas une maîtresse , ni une directrice . "
je mourais d' envie de deviner toute seule qui cette personne pouvait être : le jardinier , le cuisinier , la dame qui fait le ménage , celle qui aide à traverser la rue , mais j' ai mis ma main sur ma bouche pour empêcher toutes les réponses de sortir et laisser le temps à Lucile de terminer sa phrase .
" cette personne s' appelle Madame Bébé . "
" elle existe ? " je n' ai pas pu m' empêcher de crier cette question parce que c' est un nom tellement incroyable .
" oui , " a répondu Lucile .
" elle existe et elle est très très très sévère . "
lucile s' est mordu la lèvre après avoir prononcé ces mots , comme s' ils lui avaient fait mal .
" sévère comme Tata Marcelle , avec sa tête de touche pas à mes petites affaires bien rangées dans mon sac qui coute très cher ? "
" plus sévère . "
" sévère comme le moniteur de la piscine qui dit On ne court pas "
" beaucoup plus sévère . "
" je m' en fiche , " j' ai dit en croisant les bras .
" si tu dis je m' en fiche' devant Madame Bébé , elle te punira . elle te mettra au coin , elle appelle ça " le piquet " , et tu n' auras plus le droit d' en bouger jusqu'à ce que tu lui aies demandé pardon . "
j' ai haussé les épaules .
je m' en fichais vraiment .
je n' ai peur de rien , ni de personne .
je fais du karaté .
chapitre deux : Mawashi Geri