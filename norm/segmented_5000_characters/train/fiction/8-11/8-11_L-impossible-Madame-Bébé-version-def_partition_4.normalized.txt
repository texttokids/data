chapitre cinq : un poids sur la poitrine
au début , maman a pensé que j' avais peut être la grippe .
je ne mangeais plus rien , même pas mon gouter .
je me réveillais au milieu de la nuit à cause de cauchemars , je me sentais toute molle et j' avais les yeux brillants .
au bout de trois jours , elle m' a emmené chez le docteur Ben Larbi , qui est ma pédiatre depuis que je suis bébé et qui porte des boucles d' oreille en plumes et en paillettes .
le docteur m' a examinée la gorge , les oreilles , le nez .
elle a palpé mon ventre , écouté mon coeur .
elle a pris ma tension , m' a fait monter sur la balance , m' a mesurée , m' a fait marcher de long en large dans son cabinet puis s' est rassise derrière son bureau avec une tête Je ne me souviens plus des paroles d' Au clair de la lune .
elle a froncé les sourcils , a regardé ma maman , puis elle m' a regardée , moi .
" comment ça va , à l' école ? " a t elle demandé .
" bien , " j' ai répondu .
" les copains , les copines ? "
" ils vont bien aussi . "
" non , je veux dire , tu n' as pas de problèmes avec eux ? "
" je n' ai aucun problème , " j' ai dit .
" et avec la maîtresse , ça se passe bien ? "
" oui . "
" tu as des soucis , en dehors de l' école ? quelque chose qui t' inquiète ? " a t elle insisté .
" non . "
je disais la vérité , mais ma voix était tellement aigue que j' avais l' impression d' être quelqu' un d' autre .
" on dirait que tu as un poids sur la poitrine , " m' a dit très gentiment le docteur Ben Larbi .
je ne voyais pas du tout de quoi elle voulait parler et pourtant , dès qu' elle a prononcé ces mots , je me suis mise à pleurer très fort , d' un coup , sans pouvoir m' arrêter .
c' était une attaque surprise de chagrin .
normalement , je ne pleure jamais .
grâce à maître Lu , je sais maîtriser la douleur , mais aussi les émotions .
ma mère m' a prise dans ses bras et , par dessus son épaule , j' ai vu la tête de ma pédiatre , une tête de Je lis dans tes pensées , mais je ne comprends pas ce que je lis .
le problème , c' est que moi non plus je ne comprenais pas ce qu' il y avait dans mes pensées .
je pleurais et ça me faisait du bien , mais je ne savais pas pourquoi .
ma mère et Samia Ben Larbi se sont mises à parler entre grandes personnes , comme si je n' étais pas là .
ce n' était pas désagréable .
au contraire .
ça me donnait le temps de me calmer et j' aimais entendre leurs voix douces de femmes qui se font du souci .
" à la maison ? " a demandé le docteur " tout va bien ? pas de problèmes particuliers ? une séparation chez vos proches ? un décès ? "
" non , " a répondu ma mère .
" c' est plutôt une bonne période . mon mari a retrouvé du travail dans les équipements sportifs . sa boite l' envoie à Méribel pile pendant les vacances d' hiver . deux semaines au ski , c' est le grand luxe . les filles étaient folles de joie . surtout Nana . "
" c' est le début de la grande école , peut être . Nadejda a toujours eu besoin de bouger . rester assise du matin au soir à la même place , ça doit être difficile pour elle . "
je me suis arrêtée de pleurer d' un coup .
je suis descendu des genoux de ma mère et je suis allée me planter face au docteur .
je l' ai regardée dans les yeux et j' ai dit :
" vous avez trouvé , docteur .
c' est ça .
c' est exactement ça le problème .
vous m' avez guérie .
merci beaucoup .
bien sûr , c' était entièrement faux .
j' adore rester assise à lire et à écrire .
j' aime être au CP .
j' aime être grande .
mais quelque chose , dans la phrase qu' avait prononcée le docteur m' avait guérie .
ça c' était la vérité .
et à présent je savais exactement ce qui me restait à faire .
chapitre six : une pauvre petite fille en détresse