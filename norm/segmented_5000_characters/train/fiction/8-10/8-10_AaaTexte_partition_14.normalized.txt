Solène était seule à la maison .
ses parents ne rentraient du travail qu' à huit heures .
alors , elle passait son temps devant l' ordinateur à tchater avec ses copines Inès et Mira .
ce soir là , à dix huit heures trente cinq exactement , l' écran se bloqua .
elle redémarra l' ordinateur .
rien à faire : elle avait perdu la connexion Internet .
elle se précipita sur le téléphone , appela le service clients du fournisseur d' accès .
aussitôt , sans même une seconde d' attente , une voix moqueuse lui répondit :
hé , hé , hé , on est coupée du monde , pas vrai ?
déconnectée , isolée , c' est pas gai !
je vous en supplie , haleta Solène , aidez moi à rétablir la connexion .
à l' autre bout de la ligne , la voix devint sèche , cinglante :
tu n' a rien compris , ma fille , tu es punie , exclue , rejetée .
mais pourquoi ?
demanda Solène en éclatant en sanglots .
oh , il n' y a pas de raison particulière .
c' est tombé sur toi , voilà .
qui êtes vous ?
une fée au chômage , soupira la voix .
ma spécialité , c' était de transformer les citrouilles en carrosses , mais ça n' intéresse plus personne .
alors , j' ai dû me recycler .
j' ai suivi des cours d' informatique , et maintenant je m' amuse ...
ne me laissez pas comme ça !
implora Solène .
un court silence .
puis la voix reprit :
ok , je rétablis la connexion .
à une condition ...
laquelle ?
tu avaleras deux cents grammes de foie de veau avec de la purée de choux fleurs .
mais j' ai horreur de ça !
hé , hé , je sais bien !
débrouille toi , tu as dix minutes .
la mangeaille t' attend à la cuisine .
dès que l' assiette sera vide , tu pourras surfer à volonté sur Internet .
Clac , la communication s' interrompit .
Solène , désespérée , se traina jusqu'à cuisine .
sur la table fumait une assiette remplie de foie de veau accompagnée d' une répugnante purée de choux fleurs .
je ne peux pas avaler ça , c' est impossible !
gémit elle .
je vais t' aider , moi !
dit le chat Gaston .
et d' un bon élégant , il sauta sur la table .
mais tu parles , Gaston !
s' écria Solène .
oui , c' est l' odeur de choux fleurs qui m' a rendu la voix .
il se jeta sur l' assiette qu' il vida avec un bel appétit .
puis il se lécha les babines et dit :
pas mauvais , mais je préfère le foie cru .
Solène se précipita au salon , cliqua sur la souris et , oui , la connexion Internet était rétablie !
toute joyeuse , elle souleva le chat Gaston et l' embrassa sur le museau !
Zloup !
un éclair , un accord de violons et , dans ses bras , elle n' avait plus un chat , mais un beau gars !
qui continua , sans se gêner , à l' embrasser .
doucement , elle se dégagea et demanda :
Gaston , voyons , que fais tu ?
d' abord , je ne m' appelle pas Gaston , mais Simon .
et je te remercie de m' avoir délivré du sort jeté par la fée Microtix .
elle m' a transformé en chat parce que je ne voulais pas tchater avec elle sur MSN .
mais tu m' as embrassé et j' ai retrouvé mon aspect premier .
je te plais ?
ouais , ça va , répondit Solène qui , en vérité , trouvait Simon très joli garçon .
mais tu ne peux pas rester , mes parents vont rentrer .
pas grave , dit Gaston .
les miens vont être contents de me retrouver .
enfin , je suppose .
au revoir , Solène et continuons à communiquer .
oui , restons connectés .
on s' enverra des méls .
des SMS .
on tchatera ensemble .
on branchera nos Webcam .
on se donnera rendez vous sur des forums .
Waouh !
dit Solène .
miaou !
fit Simon .
et , comme il était encore un peu chat , il sauta sur la souris ( de l' ordinateur ) et l' avala .
vie buissonnière