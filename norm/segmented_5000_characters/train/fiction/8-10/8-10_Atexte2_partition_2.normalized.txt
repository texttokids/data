( note : tous les " madame " ont été prononcés comme il se doit pour ramollir Madame Croazic .
ça donnait à peu près : " ma daaame " .
mais tout élève bien constitué sait comment on s' adresse à une prof .

un peu nerveuse , Madame Croazic nous a demandé de prendre nos cahiers de texte .
elle nous a collé une recherche pour la semaine suivante : " quels sont les lieux de lecture dans votre voisinage ? "
ça veut dire quoi ?
( Zeynep )
c' est une recherche , alors vous chercherez !
( la prof )
c' est pas juste !
( Theo )
etc .
la tension est légèrement montée .
brouhaha général dans la classe .
rangez vos affaires et sortez !
a crié la prof .
tout le monde a obtempéré , bien sûr .
sauf Katia , toujours absorbée par sa lecture .
la prof l' a repérée , s' est approchée .
mais qu' est ce que tu fais ?
à contre coeur , Katia a relevé la tête .
je lis .
c' est pas le moment !
a dit la prof .
et elle lui a arraché son livre .
trois