ce soir là , ma soeur Léonie et son Cyril sont venus diner à la maison .
Katia aussi .
au fait , j' ai vu votre prof de français de l' année dernière , Madame Croazic , a raconté Léonie après l' entrée .
elle est passée à la librairie avec son compagnon .
elle a un mec ?
me suis je écrié .
je n' arrivais pas à imaginer qu' un type normal puisse vivre avec une nana qui passe son temps à lire !
c' est pourtant ce que fait Cyril avec ma soeur , qui est libraire .
mais lui , c' est un prof , donc pas un type normal .
alors ...
c' est un prof , n' est ce pas ?
ai je demandé ( pour vérifier si mon raisonnement était logique , comme dirait Miss Dulac ) .
je n' en sais rien , a répondu ma soeur .
il est plutôt beau garçon , mince et les cheveux en pétard , il a acheté une pile de mangas .
quant à Madame Croazic , elle est très contente dans son nouveau collège .
franchement , ça me saoulait , ces histoires de profs en plein repas .
en plus , Katia parlait d' Enguerrand avec Cyril .
de quoi me couper l' appétit .
machinalement , je jouais avec mon couteau et ma fourchette .
tout à coup , ma mère s' est écriée :
romain , arrête de jouer avec la nourriture !
surpris , j' ai regardé mon assiette : je venais de découper mon escalope en carrés , rectangles et triangles isocèles !
horreur , j' étais contaminé !
si je ne réagissais pas , j' allais avoir bientôt la tête farcie de chiffres , formules et théorèmes !
pour commencer , je me suis jeté sur le dessert , de la mousse au citron .
j' ai vidé ma coupelle en un temps record , battant à plate couture mon goinfre de beau frère .
ça m' a un peu remonté le moral .
satisfait , le ventre plein , je me suis à chantonner ...
soudain , Katia m' a donné un coup de coude .
hé , tu te rends compte de ce que tu chantes ?
non , pourquoi ?
c' est un air de l' opéra !
oh non !
c' était encore plus grave que je ne pensais : les maths étaient en train de me détraquer la cervelle !
le lendemain , deuxième jour du stage , j' étais bien décidé à me laver le cerveau de toute trace de math ou musique .
raté .
le matin , j' étais de nouveau en atelier " décor et costumes " , mais impossible de coudre un bonnet ou fabriquer un triangle sans calculer la surface en deux et le prix de revient en euros , dollars et en pataca ( c' est la monnaie de Macao ) .
à midi , j' avais l' impression que mon assiette de coquillettes était remplie de chiffres et symboles mathématiques .
seule consolation : l' apprenti boulanger est revenu , cette fois avec des croissants .
en partant , j' en suis sûr , il a fait un clin d' oeil au prof de musique .
Zorra l' a vu aussi .
elle pense que c' est Monsieur Gusi qui a commandé les viennoiseries à la boulangerie , pour faire la cour à Mademoiselle Dulac .
d' ailleurs , la prof de math a rougi quand Monsieur Gusi lui a offert un croissant .
l' après midi , pour me changer les idées , j' ai participé à l' atelier " instruments " .
j' ai joué du vélorole .
c' est l' instrument que j' ai fabriqué avec une roue de vélo , une casserole et deux tuyaux .
comme je n' avais que dix sept notes à jouer , je pensais que j' allais me reposer .
ouais , mais il fallait compter sans arrêt les pauses et les silences .
résultat : au bout d' une heure , j' avais la tête transformée en compteur électrique .
à quatre heures , on a eu une livraison de pains au chocolat .
Mademoiselle Dulac était tout embarrassée et Monsieur Gusi tout réjoui .
en fin d' après midi , on a une répétition de l' opéra .
disons : une tentative de répétition .
Mademoiselle Dulac a eu la mauvaise idée de demander :
eh bien , qui peut rappeler l' histoire de notre opéra ?
en résumé bien sûr !
là , il est apparu que la plupart avait complètement oublié de quoi il s' agissait :
c' est pas deux types qui se rencontrent dans un train pour jouer aux cartes ?
a proposé Oleg .
pas du tout !
l' a coupé Zeynep .
ils jouent avec des cerceaux sur une plage .
n' importe quoi , c' est deux voyageurs de l' espace qui construisent une fusée !
a braillé Coralie .
y' en a pas un qui ne noie en prenant son bain ?
a demandé Theo .
chacun a proposé son interprétation , ce qui a donné une discussion mouvementée .
la prof a dû nous menacer de Carmen en albanais et Don Juan en quechua avant qu' on se calme .
elle a résumé elle même l' opéra , mais ce n' était pas plus passionnant pour autant .
le seul moment intéressant , c' était la scène où les élèves assommés de théorèmes se révoltaient contre leur prof de math .
soyez naturels , a dit Monsieur Gusi .
et on lui a obéi .
on s' est jetés sur Roméo ( qui jouait le prof de math ) en hurlant " au chiotte , le prof " et autres gentillesses et on l' a à moitié assommé .
c' était vraiment naturel , je trouvais , mais les profs n' ont pas apprécié .
ils ont réécrit la scène .
finalement , il y aura un seul élève qui lèvera le doigt et dira :
c' est trop difficile , on n' y comprend rien .
passionnant !