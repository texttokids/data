j' ai eu une super note à mon interro de math .
surement parce que je portais le boxer offert par Katia .
je me suis demandé si elle n' en avait pas offert un aussi à Enguerrand , car le babouin musicien a eu dix vingt , un exploit pour lui .
il n' a pas cessé de chantonner pendant le cours , ce qui a fini par énerver Miss Dulac qui nous faisait répéter ( paroles et musique ) une nouvelle formule :
" factoriser une somme de termes , c' est la transformer en un produit de facteurs . "
après l' avoir menacé en vain de chanter La Traviata en chinois , finnois et suédois , elle a explosé et s' est mise à hurler ( chanter ) :
" factoriser une somme ... , etc . "
hou là là !
elle a une voix d' enfer , la prof .
elle a littéralement décoiffé tous ceux qui n' avaient pas une tonne de gel sur les cheveux .
après un temps de stupéfaction , chacun a commenté à sa façon :
c' est la Castafiore ?
( Zeynep )
madame , vous avez perdu un bouton de votre corsage !
( Coralie , c' était vrai , en plus .

elle va faire tourner mon yaourt au citron à zéro pour cent de matière grasse !
( Émilien , il est au régime .

c' est ça la Traviata en bas breton ?
( Roméo )
après avoir repris son souffle , la prof a marmonné :
non , c' était une improvisation personnelle , et je continue si vous ne vous calmez pas .
bien sûr , on lui a demandé de recommencer , mais vous connaissez les profs : ils ne tiennent jamais leurs promesses .
au contraire , la prof nous a fait chanter en choeur : " factoriser une somme de termes ... " , et caetera Comme j' étais à côté de Roméo et Coralie , on a chanté en version rock , en se planquant derrière le dos des copains de devant .
mais la prof nous a repérés .
tiens , tiens , il semble que certains aient une interprétation musicale originale , a t elle commenté .
qu' ils viennent la proposer au public intéressé !
on s' est donc trainés jusqu'à l' estrade .
on a utilisé le bureau comme batterie et on a fait notre numéro .
Coralie s' est déchainée .
elle a terminé à genoux aux pieds d' Emilien qui en a avalé de travers la biscotte qu' il grignotait .
moi , j' ai tapé tellement fort sur le bureau qu' il s' est renversé sur le premier rang .
cool !
la prof a tellement ri qu' un deuxième bouton de son corsage a sauté .
Katia m' a attendu à la sortie du cours .
c' était sympa , votre musique , a t elle dit .
tu as changé de voix , tu sais ?
comment ça ?
tu ne dérailles plus de l' aigu au grave .
tu as mué .
ah oui ?
je suis resté idiot un moment .
puis je l' ai regardée .
pour une fois , elle n' avait pas d' écharpe autour du cou , mais un pull à col roulé .
très moulant .
j' ai souri .
j' ai indiqué sa poitrine , et j' ai dit :
toi aussi , tu as mué .
ça te va bien .
elle a souri à son tour .
mais , juste à ce moment , elle a aperçu Enguerrand en train de se bagarrer avec un type deux fois plus gros que lui .
elle s' est précipitée à son secours .
dix
le deuxième " stage " a eu lieu début avril .
le premier jour , il y avait trois ateliers :
décors et costumes ,
pratique des instruments ( ceux qu' on avait fabriqués ) ,
atelier spécial pour ceux qui avaient un rôle soliste ( pas mon cas ) .
j' ai choisi l' atelier " décors et costumes " , imaginant qu' on allait bricoler et bien s' amuser .
c' était oublier la perversité des profs : Mademoiselle Dulac nous a distribué des plans à l' échelle et avant de toucher aux scies et aux marteaux , on a dû faire trois heures de calcul de proportionnalité !
" n' oubliez pas que le but est d' assimiler le programme " , a rappelé la prof .
heureusement , à la pause , une bonne surprise nous a remonté le moral : un apprenti boulanger est arrivé avec deux panières de brioches .
allez savoir pourquoi , Mademoiselle Dulac a rougi jusqu' aux oreilles .
elle nous a invités à nous servir et on s' est rués comme des affamés sur les brioches ( fameuses !
