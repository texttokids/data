martyre de saint ( par ex : Fra Angelico , Martyre de Cosme et Damien )
je suis puni .
privé de télé , DVD et jeux vidéo .
parce que j' ai regardé en cachette des films d' horreur .
l' éventreur de New York , pas mal , surtout quand le tueur découpe les yeux de la fille avec un rasoir .
et Le Manoir de la terreur .
celui là , bof , une histoires de zombies qui fait même pas peur .
mais j' ai pas le droit .
je sais pas pourquoi , c' est comme ça .
interdiction , ont dit les parents .
sinon , punition .
ils sont sadiques , mes parents .
des vrais tortionnaires .
ils ont inventé une punition atroce .
monstrueuse .
une visite au musée des Beaux Arts .
deux heures à me trainer devant des peintures nulles , des portraits de vieilles mémés et des trucs modernes qu' on y comprend rien .
je vais mourir d' ennui , je le sens .
ou peut être pas .
surprise
Grant Wood Spring In The Country Painting dix neuf cent quarante et un
ou :
Monsieur et Madame Rasmussen étaient assis dans leur jardin .
sur un banc .
sous un tilleul .
fin d' après midi et la chaleur était douce .
d' abord ils entendirent un léger craquement .
ou bien était ce un crissement ?
puis ils virent leur carré de gazon se déchirer et se former un monticule de terre fraiche et friable .
une taupe , dit Madame Rasmussen .
mais le monticule grandit encore et bientôt en sortit une tête ronde , chauve et rubiconde .
suivie aussitôt d' un torse rondouillard , de bras dodus et de jambes costaudes et trapues .
bonjour , Monsieur , dit Monsieur Rasmussen ( un peu sèchement ) .
bonjour , Madame , bonjour , Monsieur , dit l' apparition en secouant ses mains pleines de terre .
vous auriez l' heure , s' il vous plait ?
madame Rasmussen regarda sa montre et répondit poliment :
cinq heures moins cinq .
ah , l' heure du thé !
fit le petit homme chauve .
il sortit de son terrier un réchaud à gaz , une théière , une boite en fer , trois tasses , une nappe , un sucrier .
vous prenez du lait ?
demanda t il à Madame Rasmussen .
moi , non , mais mon mari , oui .
sans sucre , s' il vous plait .
ma femme vous a pris pour une taupe , dit Monsieur Rasmussen après un long silence .
et moi , je vous ai pris pour des humains , répondit le petit homme .
je suis désolé ...
Madame Rasmussen se mit alors à glousser et Monsieur Rasmussen sourit à pleines dents .
votre jardin est très beau , dit le petit homme , et je m' appelle Albert .
ce n' est pas grave , dit Madame Rasmussen , vous êtes quand même très sympathique .
la prochaine fois , vous vous essuierez les pieds sur le paillasson , dit Monsieur Rasmussen .
nous l' avons acheté en Écosse .
venez me voir un de ces jours , dit le petit homme .
je vous montrerai ma collection de nez de clown .
c' est émouvant .
oh , certainement , soupira Madame Rasmussen .
enfin , le petit homme prit congé .
il sortit par le portail du jardin et disparut au coin de la rue .
il est très onirique , dit Madame Rasmussen .
alcoolique , rectifia Monsieur Rasmussen .
ils se regardèrent en souriant .
puis Madame Rasmussen écarta les bras , les agita vigoureusement et s' envola .
elle alla se percher sur le toit de la maison d' en face .
Monsieur Rasmussen ouvrit très grand la bouche , secoua la tête et se mit à rugir férocement .
en trois bonds , il franchit le grillage du jardin , sauta sur le gamin des voisins et le dévora .