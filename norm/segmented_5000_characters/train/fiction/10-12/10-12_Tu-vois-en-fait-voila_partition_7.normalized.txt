notre première tache est d' écrire à Mademoiselle Laurent pour l' informer du nombre de fois qu' elle a dit " en fait " en quarante cinq minutes ( soixante sept fois !
) et lui décrire notre future application .
" nous estimons que les élèves devraient donner autant aux professeurs que les professeurs leur donnent . nous n' avons pas conduit notre expérience dans le manque du respect , au contraire c' est dans l' esprit d' améliorer le monde . il faut maintenant que nous trouvions une autre méthode qui ne dérange personne . vous avez toutes nos excuses . "
je ne lui dis pas quelle aubaine pour nous de pouvoir nous épanouir sans contraintes , sans devoirs et sans cours .
je ne suis pas contre l' école , c' est juste dommage que tous les profs ne soient pas des virtuoses comme Madame Gilli .
c' était imprudent , dit mon père .
et bête !
dit ma mère .
vous êtes donc contre les expériences scientifiques ?
quand elles sont grossières , oui .
je vais pouvoir reposer mes béquilles .
je suis tellement fatiguée et courbaturée .
je ne peux même pas te donner des corvées à faire , dit ma mère .
je pourrais si tu veux .
non , prends ça comme trois jours de repos et de réflexion .
c' était sans prendre en compte le message de Daniel que la femme linguiste de son collègue nous invite à son cours de linguistique à l' université demain .
" je sais que vous êtes au collège mais peut être vous pouvez demander aux parents de vous excuser pour la journée ? "
pas besoin !
je prends tous les renseignements et les instructions et j' informe Jules .
je n' ai pas cru à la nécessité de tenir mes parents au courant , mais la mère de Jules était libre pour nous conduire à l' université , m' épargnant le trajet dans les transports .
nous avons trouvé la salle et nous nous présentons à la Professeur Silvy qui nous indique deux places pour son cours sur les tics de langage qu' elle a programmé en notre honneur .
" c' est clair , bref , euh sont autant de béquilles qui squattent nos conversations et s' infiltrent dans nos phrases . ils sortent de nos bouches sans que l' on s' en rende compte . " hallucinant , c' est grave , juste pas possible " , la liste est longue et les tics changent selon l' âge , groupe social et ils prolifèrent dans notre monde des média où le silence n' est pas permis . la naissance et la mort d' un tic sont mystérieuses . une expression comme " c' est clair " illustre bien notre monde opaque et permet une langue de bois , tout comme " entre guillemets " tout comme les tics " c' est évident " ferment la porte à la discussion . "
madame Silvy parle pendant cinquante minutes et il n' y a pas une seconde d' ennui .
je pourrais l' écouter toute la journée ce qui me donne envie d' étudier la linguistique et la psychologie .
après ce cours , je comprends beaucoup mieux le phénomène des tics de langage .
elle nous invite pour un thé et du gâteau aux carottes fait maison .
nous lui parlons de notre projet .
elle le trouve drôle et intéressant : " peut être avec d' autres sons que des rots et des pets ! " je vais discuter avec mes collègues .
on peut aussi introduire des orthophonistes pour nous aider à corriger ces tics .

je me sentais bien dans cette atmosphère universitaire , presqu' adulte .
devrait on avoir hâte de devenir adulte ?
la journée aurait été parfaite si ma mère ne s' était pas rentrée tôt pour découvrir que sa prisonnière avait disparue .
je me fais de plus en plus mal vue , alors que j' essaie de faire ma contribution à l' humanité .
mais c' est le sort des précurseurs !
neuf .
retour au " normal "