lecteur ( totalement ) inconscient ,
malgré tous mes avertissements , tu as donc lu ce livre !
tant pis pour toi , je t' avais prévenu , le châtiment sera terrible !
tu trembles , j' espère , tu te demandes quel supplice je vais t' infliger ...
prépare toi à souffrir !
c' est encore pire que ce que tu imagines ...
non , je blague .
en vrai , je suis bien content si tu as lu jusqu' au bout .
parce que moi , même si on me promettait mon poids en Chocoloulou ou une année de vacances à Tahiti , jamais je ne lirai le journal intime d' un parfait inconnu .
mais chacun ses gouts , n' est ce pas ?
j' espère que tu ne t' es pas ennuyé , au moins .
si tu veux faire comme moi et écrire des livres , pas de souci , tu peux compter sur mon aide .
je suis quasiment devenu un pro .
je crois bien que je vais rédiger un manuel nul pour écrivains nuls .
tu te sens visé au moins ?
allez , sans rancune .
à la prochaine !
ben