voilà , j' ai dessiné cinquante six smiley .
ouf !
en vérité , je n' ai pas compté les personnes à qui j' ai fait risette .
cinquante six est un nombre vraisemblable .
j' avais mal aux muscles des joues , à force .
ça prouve que je ne souris pas assez , habituellement .
au début , ce n' était pas facile de regarder les gens dans les yeux , et puis , peu à peu , ça m' a semblé normal , nécessaire .
c' est cela , surement , qu' a voulu me faire comprendre Lukas .
seulement , j' imaginais qu' il suffisait de sourire pour rendre les gens souriants , mais c' est plus compliqué .
il faudrait qu' ils suivent tous des cours avec Lukas et apprennent à parler moins et regarder plus .
quand même , c' est une bonne journée : j' ai rencontré Milan et sa maman .
et Monsieur Demirel m' a donné son saxophone .
je vais apprendre à en jouer .
j' ai promis .
quatre mai
record de fausses notes
hier soir , quand on rentré de l' hôpital , j' ai harcelé maman pour qu' on aille chercher tout de suite le saxophone de Monsieur Demirel .
elle a fini par céder .
je l' ai trouvé sans problème , dans le placard du premier étage .
il était enfermé dans un étui un peu craquelé .
je l' ai rapporté à la maison et je l' ai frotté avec un chiffon spécial que m' a prêté maman .
il brille maintenant , il est magnifique .
avant de m' endormir , je l' ai posé sur une chaise à côté de mon lit .
je me suis réveillé tôt .
je voulais l' essayer sans tarder .
sur un internet , j' ai trouvé des cours de saxophone en ligne .
il faut d' abord apprendre à bien serrer l' embouchure avec les lèvres .
pour trouver la position juste , on s' entraîne à dire " fou " plusieurs fois .
on replie légèrement la lèvre inférieure pour former un " coussinet " sur lequel va reposer l' embouchure .
hou là là , pas simple !
et ensuite ?
on souffle !
c' est ce que j' ai fait .
de toutes mes forces .
un son perçant a résonné dans la maison , traversé les murs , mis en vibration les objets métalliques de la maison .
et déclenché à droite ( chambre des parents ) comme à gauche ( chambre de ma soeur ) des braillements synchronisés .
sans y prêter attention , j' ai soufflé à nouveau , en essayant de tenir le son le plus longtemps possible .
la porte de ma chambre s' est ouverte brusquement et le trio infernal ( père , mère , soeur , tous les trois échevelés et dépenaillés ) s' est précipité sur moi en hurlant :
arrête ça immédiatement , c' est horrible !
t' es dingue !
je vais le tuer !

ah bon ?
ai je fait , surpris .
vous n' aimez pas le saxophone ?
s' en est suivie une discussion assez mouvementée .
finalement , j' ai été autorisé à m' exercer chez Monsieur Demirel , ce qui n' est pas pratique parce qu' il n' y a pas de connexion Internet .
pas grave .
je me suis entrainé toute la matinée et j' arrive à jouer un air qui ressemble à " sur le pont d' Avignon " , à part quelques notes qui rappellent plutôt " maman les its bateaux qui vont sur l' eau " .
et quelques autres aussi , il faut être honnête , qui ne ressemblent à rien .
en tout cas , je mérite un diplôme .
je crois que personne n' a produit autant de fausses notes en si peu de temps .
" diplôme accordé à Ben Letourneux , le quatre mai mille neuf cent vingt .
, pour avoir établi le record de fausses notes émises un beau jour de mai sans avoir provoqué aucune perturbation météorologique notable .
le jury "
cet après midi , j' ai apporté le saxophone à l' école de cirque et j' ai joué ma version personnelle de " sur le pont d' Avignon " pendant l' atelier de clown .
les autres se sont bouché les oreilles .
mais pas Lukas .
à la fin du morceau , il m' a fait signe de lui prêter le saxo , et il a joué un air de danse tchèque .
ensuite , il est allé cherché d' autres instruments et on a improvisé tous ensemble un numéro sur le thème : " l' impossible orchestre " .
après le cours , Lukas m' a appris un air très facile avec trois notes seulement .
en partant , il m' a montré le saxo , puis a pointé sur le doigt sur son torse et a hoché la tête .
j' ai compris : il me proposait de me donner des cours de saxo .
j' ai dit oui , bien sûr , et même " oh ouais , super ! " .
en rentrant de l' école de cirque , j' ai croisé Milan et sa maman .
quand il a vu le saxophone , Milan est resté bouche bée .
il n' osait pas le toucher .
je lui ai montré comment on soufflait dedans , mais avec ses petits poumons , il n' en sortait que des " pouet pouet ! " riquiquis .
ça prouve qu' il faut un certain talent pour faire des fausses notes .
dix mai
record des bleus de toutes les couleurs