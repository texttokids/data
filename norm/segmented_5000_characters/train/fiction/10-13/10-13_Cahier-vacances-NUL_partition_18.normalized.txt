justine et Lukas ont invité Monsieur Demirel à déjeuner .
avec Tante Rosie .
et moi , bien sûr .
tu l' as invité pour faire baisser la moyenne d' âge ?
a demandé Monsieur Demirel à Justine .
juste pour faire la vaisselle , a répondu Justine en déposant sur la table un gratin aux courgettes bien cramé sur les bords .
et pour finir les plats , ajouté tante Rosie , c' est pratique avec lui , il n' y a jamais de restes !
après le café , j' ai accompagné Monsieur Demirel dans le jardin .
il s' est promené sans rien dire , retournant des cailloux ou des feuilles du bout de sa canne .
c' est ici , le trésor ?
ai je demandé quand on est passé près des restes de l' ancien puits .
le trésor ?
a fait Monsieur Demirel .
ah , oui ...
viens .
je l' ai suivi jusqu' au fond du jardin , vers un mur de pierre à moitié écroulé .
essoufflé , il s' est assis sur une souche d' arbre .
avec sa canne , il m' a montré une pierre couverte de mousse .
soulève la .
j' ai obéi .
elle était lourde et glissante .
j' ai dû en déplacer quelques autres aussi .
est apparue une dalle de béton , usée , brunâtre .
cherche bien , il doit y avoir une fente quelque part .
effectivement .
en repoussant quelques cailloux , j' ai découvert une fente ou plutôt un trou grossièrement taillé dans le béton .
Monsieur Demirel m' a tendu un bâton ramassé sous un buisson .
plonge le dans la fente et dis moi ce que tu en sors .
je me suis exécuté .
le bâton est ressorti enduit d' une substance noirâtre , liquide .
eh bien ?
a demandé Monsieur Demirel .
je l' ai regardé .
il me fixait d' un air grave , mystérieux .
du pétrole !
j' ai dit .
vous avez trouvé du pétrole dans votre jardin !
c' est ça , le trésor ...
le sourcil droit du vieux monsieur s' est soulevé .
sens !
j' ai senti .
je ne sais pas vraiment ce que sent le pétrole brut , mais certainement pas l' odeur que j' ai respirée en portant le bâton à mes narines .
pouah !
moi : ça sent ...
ça sent pas bon !
Monsieur Demirel : hé , non .
tu devines ce que c' est ?
moi : ça me fait penser à de la bouse de vache .
Monsieur Demirel : tu brules .
moi : je ne sais pas .
du jus de quelque chose fermenté ?
en tout cas , c' est pas un gisement d' eau de toilette .
là , il a éclaté de rire .
son corps en était secoué , j' avais l' impression d' entendre ses os s' entrechoquer .
j' ai eu peur qu' il se fracasse en mille morceaux .
je l' ai saisi par les épaules , j' ai balbutié :
Monsieur Demirel , ça va ?
ça va , vous êtes sûr ?
et lui riait , riait , et ses yeux en pleuraient .
de l' eau de toilette ...
tu n' es pas si loin , c' est de l' eau de toilette dans un certain sens .
là , je l' ai regardé avec mon plus bel air bête .
pas besoin me forcer , chez moi , c' est naturel .
il a respiré un grand coup pour chasser les derniers restes de fou rire .
c' est du purin , a t il finalement .
purée !
ai je lâché ( désolé , ça m' a échappé ) .
Monsieur Demirel : tu sais au moins ce que c' est , du purin ?
c' est le liquide qui s' écoule du fumier , essentiellement du pipi de vache , quoi !
moi ( réellement indigné ) : et c' est ça , votre trésor !
il m' a pris la main , l' a serrée dans la sienne .
d' abord , c' est un vrai trésor , d' une certaine manière .
parce que c' est un très bon engrais .
et je crois qu' on peut en faire du carburant maintenant .
mais ce n' est pas important .
il a lâché la main , m' a regardé d' une certaine façon ...
d' une façon qui m' a réchauffé le coeur , parce que j' ai compris à ce moment là qu' il m' aimait bien , Monsieur Demirel .
et que moi aussi , je l' aimais bien .
vraiment .
je ne sais pas depuis combien de temps il est là , ce purin .
depuis longtemps , en tout cas , c' est certain .
je sais seulement qu' il y avait une ferme autrefois sur le terrain , avant qu' on construise ma maison dans les années quarante .
j' étais bien plus âgé que toi quand j' ai découvert la dalle de béton , elle était déjà cachée par les herbes et les pierres .
eh bien , moi aussi , j' ai cru qu' il y avait un trésor enfoui là dessous ...
alors , pourquoi ce ne serait pas un trésor , après tout ?
je n' ai pas répondu .
je l' ai aidé à se relever .
on est allé retrouver les autres qui se reposaient à l' ombre du tilleul .
justine était assise sur les genoux de Lukas .
tante Rosie somnolait .
j' ai dit :
je vais faire la vaisselle .
: pas d' exercice aujourd'hui , j' ai assez travaillé , je pense .
ah si , ça :
" lecture suivie
lire " l' Île au trésor " de RL Stevenson .

vingt et un août
il est minuit et quart ( ou , autrement dit , zéro H quinze ) .
je viens de me relever parce que :
j' avais un petit creux ( de clafoutis aux cerises , trop peur que papa ne le finisse demain matin tout à l' heure au petit déjeuner ) ,
un moustique m' a piqué à l' intérieur de l' oreille droite ( celle que j' arrive à faire bouger , je ne sais pas pourquoi ça ne marche pas avec l' oreille gauche ) ,
il y avait un petit truc qui me trottait dans la tête , comme un petit grillon qui n' arrêtait pas de chantonner : " mon petit Ben , tu as écrit des choses compromettantes dans ton cahier de vacances , bien sûr , ça ne peut pas arriver , mais imagine que quelqu' un le lise ? "