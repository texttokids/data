ils se sont rencontrés au lycée dans le cours d' histoire en seconde .
ainsi l' histoire commença , une histoire d' amour , de bébés , de travail , de vie de tous les jours et de nuits sans fin .
quelle histoire !
Arthur se rappelle d' avoir vu Ariane avec ses longs cheveux noirs jusqu'à la taille .
ce qui a marqué Ariane c' était le sourire biscornu d' Arthur .
elle avait quinze ans et elle dit à sa mère : " j' ai rencontré l' homme de ma vie ! "
malgré les réserves de sa mère , ils se marièrent et eurent des enfants , BEAUCOUP d' enfants !
Arthur adorait les bébés .
Ariane aimait être enceinte .
elle aimait voir son ventre gonfler comme un ballon .
le premier à sortir de ce ballon était une fille , Anna .
ils étaient abonnés à la lettre A
pour leur deuxième fille , ils décidèrent d' avancer vers le B Elle s' appelle Bella .
et l' alphabet les a guidés pour leurs six filles : Anna , Bella , Cara , Dana , Élisa et Flavia .
s' ils ne commencent par un À , au moins ils finissent par le À qu' ils affectionnent tant .
ils ont décidé de fabriquer des bébés jusqu'à enfin donner naissance à un garçon , ce qui arriva avec leur septième bébé , Gabriel .
ouf !
et puis STOP .
car d' une façon ou d' une autre , il faut élever les enfants et gagner la vie , deux choses difficiles à concilier pour Arthur et Ariane qui ont des métiers qui les amènent loin ...
loin de leur famille .

bien sûr , ils ne se rendaient pas compte que non seulement il faut les faire naitre mais les enfants ont besoin de soins quotidiens et minute par minute .
Arthur , ingénieux ingénieur , inventait des dispositifs pour tenir les biberons sans l' aide d' un parent affectueux .
il fabriquait des berceaux électriques , mais n' arrivait jamais à concocter un système pour changer les couches sans les mains humaines .
pas de problème : Anna changeait les couches de Bella qui s' occupait de Cara , et ainsi de suite .
à vrai dire , c' est Anna , l' aînée qui s' occupe de tout le monde , y compris ses parents !
pendant que son père parcourt le monde dans une direction pour l' entreprise qui a commercialisé son logiciel et ses inventions et que sa mère , grand reporter pour une chaîne de télévision , parcourt le monde dans une autre direction , Anna se sent souvent seule avec ses cinq soeurs et son frère .
malgré la présence du jeune homme irlandais , Sean , qu' ils emploient pour assurer la survie de leurs enfants , Anna prend beaucoup sur elle , sans qu' on lui demande quoi que ce soit .
elle est pleinement marquée par son rôle d' aînée .
si Anna a l' impression d' être Cendrillon , c' est surtout parce que cette année elle est entrée en sixième et elle aimerait avoir plus de temps pour elle même , même si c' est seulement pour se faire belle !
par exemple , Anna est toujours la dernière des enfants à partir le matin alors que c' est elle qui a le plus long chemin .
Sean qui n' aime le matin et a du mal à se réveiller , donne volontiers ce boulot aux grandes qui doivent habiller , nourrir toute la fratrie .
cette première année de collège l' inquiète car en plus de ses propres devoirs , elle surveille ceux des autres enfants .
sauf Gabriel qui est encore à la maternelle .
Gabriel est allergique aux acariens .
il est toujours en train de s' éternuer , renifler , avec des yeux et le nez qui coulent .
il a du mal à respirer .
et Anna est toujours inquiète .
leur maison n' a donc pas de tapis , pas de bibliothèques , pas de tapisseries .
à la naissance de Gabriel , quand ils ont su pourquoi il toussait tout le temps , ils se sont débarrassés de tout ce qui n' est pas essentiel .
Gabriel dort dans une petite chambre , presque stérile , tout seul .
les six filles dorment dans une autre chambre " dortoir " les unes sur les autres .
il n' y a pas de place pour des bureaux .
ils font leurs devoirs sur la table de la salle à manger .
les parents ont leur propre " nid d' amour " , comme ils l' appellent .
et Sean a une petite chambre à lui .
Anna pense que c' est quand même plus facile maintenant qu' ils sont tous à l' école : Gabriel donc en grande section .
, Flavia en CP , Élisa en C E un , Dana , en C E deux , Cara en C Monsieur un , Bella en C Monsieur deux et Anna en sixième .
à leur famille seule , ils recouvrent presque toute l' éducation nationale .