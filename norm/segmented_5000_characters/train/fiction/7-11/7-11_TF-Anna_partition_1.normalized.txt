" on peut danser la polka
en pyjama
on peut manger la polenta
en pyjama
on peut faire la java
en pyjama
ou boire du cola
en pyjama
mais on ne peut pas voyager à Odessa
en pyjama
ce serait pas beau
il vaut mieux en pyjama
faire dodo .

cara récite une longue tirade de sa pièce .
Dana fait un tour de magie .
Élisa danse .
Flavia ne fait rien , à part manger du pop corn .
Flavia ne veut jamais rien faire .
et Gabriel , regarde ses soeurs , béat et admiratif .
Emma aimerait rester toute sa vie avec eux , même si elle n' aime rien de ce qu' ils mangent .
cara récite un nouveau passage d' une pièce que personne ne comprend :
" adieu , Camille , retourne à ton couvent , et lorsqu' on te fera de ces récits hideux qui t' ont empoisonnée , réponds ce que je vais te dire : tous les hommes sont menteurs , inconstants , faux , bavards , hypocrites , orgueilleux et lâches , méprisables et sensuels , toutes les femmes sont perfides , artificieuses , vaniteuses , curieuses et dépravées , le monde n' est qu' un égout sans fond où les phoques les plus informes rampent et se tordent sur des montagnes de fange , mais il y a au monde une chose sainte et sublime , c' est l' union de deux de ces êtres si imparfaits et si affreux . on est souvent trompé en amour , souvent blessé et souvent malheureux , mais on aime , et quand on est sur le bord de sa tombe , on se retourne pour regarder en arrière , et on se dit : " j' ai souffert souvent , je me suis trompé quelquefois , mais j' ai aimé .
c' est moi qui ai vécu , et non pas un être factice créé par mon orgueil et mon ennui .

au milieu de sa performance , les parents rentrent silencieusement comme des voleurs .
ils ne se sont pas pressés pour revenir .
mais maintenant ils sont fatigués .
ils regardent leurs enfants avec une tendresse énorme .
ils sont géniaux !
n' est ce pas qu' ils ont raison de faire confiance à leurs enfants ?