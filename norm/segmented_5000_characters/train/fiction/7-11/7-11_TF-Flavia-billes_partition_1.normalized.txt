dès que Flavia rentre chez elle , sa fratrie lui tombe dessus :
qu' est ce que tu as fait ?
questionne Bella .
qu' est ce qu' il y a dans ton sac ?
veut savoir Dana .
des billes !
oh , il y en a plein !
tu crois que tu pourras nous en donner ?
s' intéresse Gabriel .
non , c' est à moi !
fait Flavia .
pourtant , Grand Mère Léo lui en a offert tout un tas en lui disant :
comme ça , tu pourras en faire cadeau à qui tu veux .
mais Flavia veut tout garder pour elle .
c' est son trésor .
tu sais y jouer , au moins ?
demande Anna .
à l' école , tout le monde a des billes .
à la récré , c' est la folie dans la cour .
mais Flavia n' a encore jamais fait de partie avec les autres .
si tu ne sais pas , je peux te montrer !
lui propose gentiment Gabriel .
il ne manquerait plus que ça !
Flavia ne va quand même pas demander à son imbécile de petit frère !
n' importe qui sauf lui !
tu ne peux pas m' apprendre , toi ?
demande t elle à Anna .
j' ai un peu oublié les règles .
mais Gabriel est un vrai champion !
grr !
Flavia fait comme si elle n' avait pas entendu .
elle demande à chacune de ses soeurs , mais elles la renvoient toutes vers Gabriel .
Flavia n' a pas le choix ...
elle s' installe avec son frère par terre dans le salon .
tu sais qu' on jouait déjà aux billes dans la Grèce antique , commence Gabriel .
les Romains y jouaient aussi et ...
pas la peine de me raconter toute tes salades sur les billes , banane !
montre moi juste comment on fait .
heureusement pour Flavia , Gabriel ne se vexe pas facilement .
d' accord .
on va commencer par la tic , ou tiquette , c' est ce qu' il y a de plus simple .
tu places ta bille au milieu .
je vais essayer de la toucher avec la mienne et si j' y arrive , elle est pour moi .
c' est de la triche !
non , ce sont les règles du jeu .
après , je placerai ma bille et tu essaieras de la toucher .
mais d' abord , je vais t' apprendre les différentes techniques de tir aux billes .
pas la peine : j' inventerai une méthode à moi .
on ne peut pas faire n' importe quoi !
je te montre le plus courant : le pointage .
Gabriel place son pouce derrière l' index et le détend comme une gâchette pour envoyer sa bille vers celle du centre .
à toi d' essayer , dit il .
je n' essaie rien du tout , on joue .
mets ta bille au milieu .
Gabriel place sa bille que Flavia touche du premier coup .
son frère est impressionné .
le piano a dû lui dégourdir les doigts .
excellent !
à ton tour de placer ta bille .
Flavia choisit la bille la plus laide au cas où elle devrait la perdre .
elle a quand même du mal à s' en séparer , comme si la bille faisait partie de sa chair .
Gabriel tire et la rate .
Flavia joue de nouveau et gagne sa deuxième bille .
Beginner' s luck !
commente Billy en passant .
ce n' est pas la chance des débutants , rétorque Flavia .
c' est le talent !
Dana vient ensuite se joindre à eux avec ses billes .
Flavia les gagne toutes !
c' est bien la première fois que Flavia a hâte d' aller à l' école le lendemain .
elle se voit déjà remporter les billes de toute la cour .
le lendemain , elle entre à l' école un gros sac de billes à la main .
aussitôt , Quentin , l' homme de ses rêves , s' avance vers elle .
il est en CE deux , dans la classe de Dana , et il lui a déjà écrit une lettre , pas exactement d' amour , mais une lettre quand même !
Flavia rêve qu' il la demande en mariage , mais il lui demande juste si elle veut jouer aux billes avec lui .
non pas elle !
proteste Mouad , un ami de Quentin .
pas un bébé de CP !
elle a beau être en CP , elle joue très bien , prévient Dana .
si on l' accepte , tous les CP vont vouloir jouer avec nous , insiste Mouad .
et alors ?
l' important , c' est de jouer , déclare Quentin .
et Flavia l' aime encore plus .
mais quand il s' agit de gagner , Flavia ne fait pas dans le sentiment : dès la première partie , elle remporte toutes les billes .
quand la sonnerie retentit , elle entre en classe comme une condamnée qui doit attendre une éternité avant la récré .
mais quand il est enfin l' heure d' aller dans la cour , les grands garçons du CE deux ne veulent plus jouer avec elle !
ils ne veulent pas risquer les billes qui leur restent ...
Flavia parvient tant bien que mal à rassembler quelques nouvelles victimes et gagne de nouveau .
elle n' a plus assez de place dans son sac et ses poches débordent de billes .
aux récrés suivantes , plus personne ne veut jouer avec elle .
Flavia se rabat sur ses soeurs à qui elle est obligée de donner une partie de son butin pour faire une partie ensemble .
il ne lui faut pas longtemps pour récupérer toutes ses billes .
quel dommage que ce ne soit pas un sport olympique !
trois .