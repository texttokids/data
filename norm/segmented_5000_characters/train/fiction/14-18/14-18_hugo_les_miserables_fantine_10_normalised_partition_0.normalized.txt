il avait à sa fenêtre un antique rideau de grosse étoffe de laine qui finit par devenir tellement vieux que , pour éviter la dépense d' un neuf , madame Magloire fut obligée de faire une grande couture au beau milieu .
cette couture dessinait une croix .
l' évêque le faisait souvent remarquer .
comme cela fait bien !
disait il .
toutes les chambres de la maison , au rez de chaussée ainsi qu' au premier , sans exception , étaient blanchies au lait de chaux , ce qui est une mode de caserne et d' hôpital .
cependant , dans les dernières années , madame Magloire retrouva , comme on le verra plus loin , sous le papier badigeonné , des peintures qui ornaient l' appartement de mademoiselle Baptistine .
avant d' être l' hôpital , cette maison avait été le parloir aux bourgeois
de là cette décoration .
les chambres étaient pavées de briques rouges qu' on lavait toutes les semaines , avec des nattes de paille tressée devant tous les lits .
du reste , ce logis , tenu par deux femmes , était du haut en bas d' une propreté exquise .
c' était le seul luxe que l' évêque permit .
il disait :
cela ne prend rien aux pauvres .
il faut convenir cependant qu' il lui restait de ce qu' il avait possédé jadis six couverts d' argent et une grande cuiller à soupe que madame Magloire regardait tous les jours avec bonheur reluire splendidement sur la grosse nappe de toile blanche .
et comme nous peignons ici l' évêque de Digne tel qu' il était , nous devons ajouter qu' il lui était arrivé plus d' une fois de dire :
je renoncerais difficilement à manger dans de l' argenterie .
il faut ajouter à cette argenterie deux gros flambeaux d' argent massif qui lui venaient de l' héritage d' une grand' tante .
ces flambeaux portaient deux bougies de cire et figuraient habituellement sur la cheminée de l' évêque .
quand il avait quelqu' un à diner , madame Magloire allumait les deux bougies et mettait les deux flambeaux sur la table .
il y avait dans la chambre même de l' évêque , à la tête de son lit , un petit placard dans lequel madame Magloire serrait chaque soir les six couverts d' argent et la grande cuiller .
il faut dire qu' on n' en ôtait jamais la clé .
le jardin , un peu gâté par les constructions assez laides dont nous avons parlé , se composait de quatre allées en croix rayonnant autour d' un puisard , une autre allée faisait tout le tour du jardin et cheminait le long du mur blanc dont il était enclos .
ces allées laissaient entre elles quatre carrés bordés de buis .
dans trois , madame Magloire cultivait des légumes , dans le quatrième , l' évêque avait mis des fleurs .
il y avait çà et là quelques arbres fruitiers .
une fois madame Magloire lui avait dit avec une sorte de malice douce :
monseigneur , vous qui tirez parti de tout , voilà pourtant un carré inutile .
il vaudrait mieux avoir là des salades que des bouquets .
madame Magloire , répondit l' évêque , vous vous trompez .
le beau est aussi utile que l' utile .
il ajouta après un silence :
plus peut être .
ce carré , composé de trois ou quatre plates bandes , occupait Monsieur l' évêque presque autant que ses livres .
il y passait volontiers une heure ou deux , coupant , sarclant , et piquant çà et là des trous en terre où il mettait des graines .
il n' était pas aussi hostile aux insectes qu' un jardinier l' eût voulu .
du reste , aucune prétention à la botanique , il ignorait les groupes et le solidisme , il ne cherchait pas le moins du monde à décider entre Tournefort et la méthode naturelle , il ne prenait parti ni pour les utricules contre les cotylédons , ni pour Jussieu contre Linné .
il n' étudiait pas les plantes , il aimait les fleurs .
il respectait beaucoup les savants , il respectait encore plus les ignorants , et , sans jamais manquer à ces deux respects , il arrosait ses plates bandes chaque soir d' été avec un arrosoir de fer blanc peint en vert .
la maison n' avait pas une porte qui fermât à clé .
la porte de la salle à manger qui , nous l' avons dit , donnait de plain pied sur la place de la cathédrale , était jadis armée de serrures et de verrous comme une porte de prison .
l' évêque avait fait ôter toutes ces ferrures , et cette porte , la nuit comme le jour , n' était fermée qu' au loquet .
le premier passant venu , à quelque heure que ce fût , n' avait qu' à la pousser .
dans les commencements , les deux femmes avaient été fort tourmentées de cette porte jamais close , mais M .
de Digne leur avait dit :
faites mettre des verrous à vos chambres , si cela vous plait .
elles avaient fini par partager sa confiance ou du moins par faire comme si elles la partageaient .
madame Magloire seule avait de temps en temps des frayeurs .
pour ce qui est de l' évêque , on peut trouver sa pensée expliquée ou du moins indiquée dans ces trois lignes écrites par lui sur la marge d' une bible : " voici la nuance : la porte du médecin ne doit jamais être fermée , la porte du prêtre doit toujours être ouverte . "
sur un autre livre , intitulé Philosophie de la science médicale