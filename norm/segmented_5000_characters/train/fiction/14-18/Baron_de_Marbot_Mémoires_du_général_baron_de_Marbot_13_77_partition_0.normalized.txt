le stratagème des maréchaux Lannes et Murat ayant assuré le passage du Danube , l' empereur Napoléon avait dirigé son armée à la poursuite des autrichiens et des russes .
ici commence la seconde phase de la campagne .
Hollabruenn .
je remets à l' Empereur les drapeaux pris à Bregenz .
dangers d' un mensonge de complaisance .
le maréchal russe Koutousoff de Krems se dirigeait par Hollabruenn sur Bruenn , en Moravie , afin de s' y réunir à la seconde armée que l' empereur Alexandre conduisait en personne , mais , en approchant d' Hollabruenn , il fut consterné en apprenant que les corps de Murat et de Lannes étaient déjà maîtres de cette ville , ce qui lui coupait tout moyen de retraite .
pour se tirer de ce mauvais pas , le vieux maréchal russe , employant à son tour la ruse , envoya le général prince Bagration en parlementaire vers Murat , auquel il assura qu' un aide de camp de l' Empereur venait de conclure à Vienne un armistice avec l' empereur Napoléon , et qu' indubitablement la paix s' ensuivrait sous peu .
le prince Bagration était un homme fort aimable , il sut si bien flatter Murat , que celui ci , trompé à son tour par le général russe , s' empressa d' accepter l' armistice , malgré les observations du maréchal Lannes , qui voulait combattre , mais Murat ayant le commandement supérieur , force fut au maréchal Lannes d' obéir .
la suspension d' armes dura trente six heures , et pendant que Murat respirait l' encens que ce russe madré lui prodiguait , l' armée de Koutousoff , faisant un détour et dérobant sa marche derrière un rideau de monticules , échappait au danger et allait prendre , au delà d' Hollabruenn , une forte position qui lui ouvrit la route de Moravie et assurait sa retraite ainsi que sa jonction avec la seconde armée russe , cantonnée entre Znaïm et Bruenn .
napoléon était alors au palais de Schoenbruenn , près de Vienne , il entra dans une grande colère en apprenant que Murat , se laissant abuser par le prince Bagration , s' était permis d' accepter un armistice sans son ordre , et lui prescrivit d' attaquer sur le champ Koutousoff .
mais la situation des russes était bien changée à leur avantage , aussi reçurent ils les français très vigoureusement .
le combat fut des plus acharnés , la ville d' Hollabruenn , prise et reprise plusieurs fois par les deux partis , incendiée par les obus , remplie de morts et de mourants , resta enfin au pouvoir des français .
les russes se retirèrent sur Bruenn , nos troupes les y poursuivirent , et occupèrent cette ville sans combat , bien qu' elle soit fortifiée et dominée par la célèbre citadelle de Spielberg .
les armées russes et une partie des débris des troupes autrichiennes s' étant réunies en Moravie , l' Empereur , pour lui donner un dernier coup , se rendit à Bruenn , capitale de cette province .
mon camarade Massy et moi le suivîmes dans cette direction , mais nous avancions lentement et avec beaucoup de peine , d' abord parce que les chevaux de poste étaient sur les dents , puis à cause de la grande quantité de troupes , de canons , de caissons , de bagages dont les routes étaient encombrées .
nous fûmes obligés de nous arrêter vingt quatre heures à Hollabruenn , afin d' attendre que le passage fût rétabli dans ses rues détruites par l' incendie et remplies de planches , de poutres , de débris de meubles encore enflammés .
cette malheureuse ville avait été si complètement brulée que nous n' y trouvâmes pas une seule maison pour nous abriter !

pendant le séjour que nous fûmes contraints d' y faire , un spectacle horrible , épouvantable , consterna nos âmes .
les blessés , mais principalement ceux des russes , s' étaient réfugiés pendant le combat dans les habitations où l' incendie les avait bientôt atteints .
tout ce qui pouvait encore marcher s' était enfui à l' approche de ce nouveau danger , mais les estropiés , ainsi que les hommes gravement frappés , avaient été brulés vifs sous les décombres !

beaucoup avaient cherché à fuir l' incendie en rampant sur la terre , mais le feu les avait poursuivis dans les rues , où l' on voyait des milliers de ces malheureux à demi calcinés et dont plusieurs respiraient encore !

les cadavres des hommes et des chevaux tués pendant le combat avaient été aussi grillés , de sorte que l' infortunée cité d' Hollabruenn répandait à plusieurs lieues à la ronde une épouvantable odeur de chair grillée , qui soulevait le coeur !

il est des contrées et des villes qui , par leur situation , sont destinées à servir de champ de bataille , et Hollabruenn est de ce nombre , parce qu' elle offre une excellente position militaire , aussi , à peine avait elle réparé les malheurs que lui causa l' incendie de dix huit cent cinq , que je la revis , quatre ans après , brulée de nouveau , et jonchée de cadavres et de mourants à demi rôtis , ainsi que je le rapporterai dans mon récit de la campagne de dix huit cent neuf .
le commandant Massy et moi quittâmes ce foyer d' infection aussitôt que nous le pûmes et gagnâmes Znaïm où , quatre ans plus tard , je devais être blessé .
enfin nous joignîmes l' Empereur à Bruenn le vingt deux novembre , dix jours avant la bataille d' Austerlitz .