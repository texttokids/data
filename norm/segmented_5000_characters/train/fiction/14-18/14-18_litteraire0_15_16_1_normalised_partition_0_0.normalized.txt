
) à la vérité , j' aimais Autisme ( I ) , mon souverain maître qui sait ce qui m' est expédient ( ii ) , et m' avait fort bien appasté ( iii ) , m' offrant trois vies , et plus largement mille dans chacune de celles là .
je n' avais alors casement ( iv ) , allant comme mendiant , mon cheval , mon luth et mon or si bien qu' il me vint , avec ma fortune amassée aux pirouettes parlantes et caressantes , de bâtir ribon ribaine ( V ) La Folie , la jolie petite maison ci avant narrée en sa composition au Tome Premier .
il est bel à deviner ( vi ) qu' il s' y passa moultes joyeusetés , que sa renommée secrète se porta au delà des frontières .
mon père l' admira pour sa beauté et sa grâce , fier de me voir si bellement instruit d' architecture , ne sachant que s' y mussait , au delà de chansons , agapes et disputes sur l' art , de diécule à crépuscule , d' hardies nuitées , jusqu'à ce que mon frère sot copieur du Père Garasse , lui souffla quelques mots et le fit témoin par quelque espion .

je laisse le préambule , file au hasard au Chapitre six .

) à la minuit , dans les nuits de lune pleine , à la demande de Monsieur de S , je secoue vaillamment la trenquefile de la petite cloche accrochée derrière la tenture aux anges , la mignonne tinte vaillamment de sa voix menue , ou bien je tape vivement dans les mains quelques coups rapides et enjoués et des têtes se lèvent , ces messieurs et ces dames s' en courent tous nus les uns , empêtrés de quelque passementerie d' aucuns , en voilà qui se pressent en s' entrebaisant , en voici qui se fouettent et se chevauchent en titubant , c' est cris , grognements , clameurs , glapissements et regoulements .
point de vérécondie aux élus : c' est l' appel pour la descente au terrier .
on est en son avertin , on tressue , voilà la demi douzaine chaude et frétillante tout gaudissant ( vii ) qui me suit à la queue leu leu , moi l' orteil sur le bouton de jade , le portail s' ouvre , je me tire vitement en arrière , et soudain après que , Monsieur de S à la tête , on y court on y vole on y glisse , c' est la descente à la desserte , c' est la descente aux enfants , les pieds de la troupe heurtent le vide , les genoux fléchissent , les jambes s' envolent , les corps s' affaissent délicieusement et diablassent tout d' une tirade le long de la glissière et avant !
zou !
dedans mousselines , gazes , plumages , voiles lestes et légers qui emmitonnent et déposent en les draps de soie noire .
l' on est tombé en grappe dans l' alcôve où s' effraient deux ou trois petiots et petiotes enlevés au loin à quelque nid par l' entremise de mon bon Armand , de mignons Jésus et Marie effarouchés à qui apprendre la leçon , à échauffer tout dru , mignardiser et consoler tant ils appellent mère et père , à eux de s' échapper de l' assiduité lufre s' ils le peuvent par le fauteuil suspendu pour eux seuls et à s' en retrouver dans le jardin où il se pourrait bien que vieilles hahas ou lubres courtisans y revinssent sus .

c' est trop .
trop de forfaiture .
trop d' horreur .
le narré me tombe des mains et tombe du ciel pour Roch Henri qui se trouve à six pas .
et la noble Idalie de Serre par dessus .
oui , j' étais là haut , c' est mon érable à lecture et figurez vous que ce fut celui de mon aïeul !
roch henri ramasse le velin calciné et la liasse de feuillets décousus éparpillés à terre .
il s' en noircit les doigts , regarde la Chose qui encombre les mains et embarrasse l' âme , il regarde l' emballement de mon menton qui tremble .
je suis là , n' y suis plus , suis hors de moi et claque des dents .
délices et passementeries , Tome Deuxième !
une belle éloquence pour dire ses ...
ses ...
un vicieux !
des enfants , des enfants ...

j' ai pour aïeul un corrompu et un barbare !
la Folie , savez vous à quoi il destina La Folie ?
le commandant est parti auprès de Monsieur le comte , votre arrière grand père .
je vous conduis chez Madame Lévi .
je vais prendre le temps de lire ce document .
roch henri porte dans les mains la masse carbonisée comme il tiendrait un cadavre de tout petit surgi du tréfonds de la folie , il porte les résidus de tortures subies par des enfants dont j' entends sourdre les voix .
elles exigent que l' on prenne place dans les malheurs qui ont eu lieu et que le corps d' un lecteur les traverse pour en subir l' horreur .
en passer par là .
je fouille dans ma poche de robe , en tire mon portable , appelle mon père , tente un bref préambule , bafouille , résume , pleure sous les yeux de Silka qui m' a poussée dans son logis , là où aucun monte plat ne transportera nos dires dans tout le Château .
Ida , viens en au but , je suis en Chine .
monsieur notre aïeul Siméon écrivit en parlant du sous sol dans le Tome Premier de ses Délices une expression qui paraissait mystérieuse à nos regards aveugles et qui ne l' était point , énigmatique , si l' on voulait l' entendre , qui clamait net de quoi il retournait : la desserte aux enfants .
j' avais dix sept ans , Papa , je venais de dénicher le calepin dans la bibliothèque de l' atelier de Maman , je t' avais questionnée .