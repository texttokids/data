( Note trois cent quatre vingt quinze : peut être cet esprit inquiet , qui remuait tout , songeait il à réformer le clergé , du moins les moines .
dans une occasion , il reproche grossièrement aux prêtres : " leurs grosses grasses ribauldes . " Chastellain , C .
lxi , P cent quatre vingt dix .
de mille quatre cent soixante deux , il autorise son cousin et conseiller , Jean de Bourbon , abbé de Cluny , à réformer l' ordre de Cluny .
archives , registre cent quatre vingt dix neuf , N quatre cent trente six , déc. mille quatre cent soixante deux .

( Note trois cent quatre vingt seize : c' était Jehan de Foix , comte de Candale .
" d' autre part , Sire , Monsieur le cardinal , mon oncle , est en grant aage et tousjours maladif , mesmement a esté puis naguères en tel point qu' il a cuidé morir , et est à présumer qu' il ne vivra guère , je fusse voulentiers allé par devers luy pour le voir , et m' eust valu plus que je n' ay gaigné pieça ... je ne scay , Sire , si vous avez jamais pensé d' avoir Avignon en vostre main , lequel , à mon avis , vous seroit bien séant . et qui pourroit mettre au service de mondit sieur le cardinal , ou par la main de Monsieur de Foix , ou autrement , quelque homme , de façon qu' il fist résidence avec luy , ne fauldroit point avoir le palais , incontinent que ledit Monsieur le cardinal seroit trespassé . vous y adviserez , Sire , ainsi que vostre plaisir sera , nonobstant que je parle un peu contre conscience , attendu que c' est fait qui touche l' Église , mais la grant affection que j' ay de vous , Sire , me le fait dire . " trente et un aoust mille quatre cent soixante quatre .
lettre de Jehan de Foix au Roy .
Bibl .
royale , .
Legrand , preuves , C .
I .

louis XI se trouvait engagé dans une étrange voie , celle d' un séquestre universel , il y allait de lui même sans doute et par l' âpre instinct du chasseur .
mais quand il eût voulu s' arrêter , il ne l' aurait pu .
il n' avait pu élargir le duc d' Alençon , l' ami des anglais , qu' en s' assurant des places qu' il leur aurait ouvertes .
il n' avait pu s' aventurer dans la Catalogne qu' en prenant pour sûreté au comte de Foix une ville forte .
les Armagnacs , à qui il avait fait à son avénement le don énorme du duché de Nemours , le trahissaient au bout d' un an , le comte d' Armagnac , sachant que le roi en avait vent , craignit de sembler craindre , il vint se justifier , jura , selon son habitude , et , pour mieux se faire croire , offrit ses places : " j' accepte , " dit le roi .
et il lui prit Lectoure et Saint Sever .
il prenait souvent des gages , souvent des otages .
il aimait les gages vivants .
jamais ni roi , ni père , n' eut tant d' enfants autour de lui .
il en avait une petite bande , enfants de princes et de seigneurs , qu' il élevait , choyait , le bon père de famille , dont il ne pouvait se passer .
il gardait avec lui l' héritier d' Albret , les enfants d' Alençon , comme ami de leur père , qu' il avait réhabilité , le petit comte de Foix , dont il avait fait son beau frère , et le petit d' Orléans qui devait être son gendre .
il ne pouvait guère l' être de longtemps , il naissait , mais le roi avait cru plus sûr de tenir l' enfant entre ses mains , au moment où il irritait toute sa maison , livrant son héritage au delà des monts pour s' assurer à lui même ce côté ci des monts , la Savoie .
il aimait cette Savoie de longue date , comme voisine de Son Dauphiné : il y avait pris femme , il y maria sa soeur , il tenait près de lui tout ce qu' il y avait de princes ou princesses de Savoie , il fit enfin venir le vieux duc en personne .
des princes savoyards , un lui manquait , et le meilleur à prendre , le jeune et violent Philippe de Bresse , qui , d' abord caressé par lui , avait tourné au point de chasser de Savoie son père , beau père de Louis le onzième .
il attira l' étourdi à Lyon , et , le mettant sous bonne garde , il le logea royalement à son château de Loches .
au moyen d' une de ces Savoyardes , il comptait faire une belle capture , rien moins que le nouveau roi d' Angleterre .
ce jeune homme , vieux de guerres et d' avoir tant tué , voulait vivre à la fin .
il fallait une femme .
non pas une anglaise , ennuyeusement belle , mais une femme aimable qui fit oublier .
une française eût réussi , une française de montagnes , comme sont volontiers celles de Savoie , gracieuse , naïve et rusée .
une fois pris , enchainé , muselé , l' Anglais , tout en grondant , eût été ici , là , partout où le roi et le Faiseur de Rois auraient voulu le mener .
à cette française de Savoie , le parti Bourguignon opposa une anglaise de Picardie , du moins dont la mère était Picarde , sortant des Saint Pol de la maison de Luxembourg .
la chose fut évidemment préparée , et d' une manière habile , on arrangea un hasard romanesque , une aventure de chasse où ce rude chasseur d' hommes vint se prendre à l' aveugle .
entré dans un château pour se rafraichir , il est reçu par une jeune dame en deuil qui se jette à genoux avec ses enfants , ils sont , la dame l' avoue , du parti de Lancastre , le mari a été tué , le bien confisqué , elle demande grâce pour les orphelins .
cette belle femme qui pleurait , cette figure touchante de l' Angleterre après la guerre civile , troubla le vainqueur , ce fut lui qui pria ...
néanmoins , ceci était grave , la dame n' était pas de celles qu' on a sans mariage .