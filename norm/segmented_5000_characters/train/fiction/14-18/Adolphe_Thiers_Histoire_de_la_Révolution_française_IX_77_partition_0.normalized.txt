malte , le poste le plus important de la Méditerranée , appartenait à un ordre usé , et qui devait disparaitre devant l' influence de la révolution française .
malte , d' ailleurs , devait tomber bientôt au pouvoir des anglais , si la France ne s' en emparait pas .
Bonaparte avait fait saisir les propriétés des chevaliers en Italie , pour achever de les ruiner .
il avait pratiqué des intrigues à malte même , qui n' était gardée que par quelques chevaliers et une faible garnison , et il se proposait d' y envoyer sa petite marine et de s' en emparer .
" de ces différens postes , écrivait il au directoire , nous dominerons la Méditerranée , nous veillerons sur l' empire ottoman , qui croule de toutes parts , et nous serons en mesure ou de le soutenir ou d' en prendre notre part . nous pourrons davantage , ajoutait Bonaparte , nous pourrons rendre presque inutile aux anglais la domination de l' Océan . ils nous ont contesté à Lille le Cap de Bonne Espérance , nous pouvons nous en passer . occupons l' Égypte , nous aurons la route directe de l' Inde , et il nous sera facile d' y établir une des plus belles colonies du globe . "
c' est donc en Italie , et en promenant sa pensée sur le Levant , qu' il conçut la première idée de l' expédition célèbre qui fut tentée l' année suivante .
" c' est en Égypte , écrivait il , qu' il faut attaquer l' Angleterre . " ( lettre du seize août mille sept cent quatre vingt dix sept vingt neuf thermidor an V )
pour arriver à ces fins , il avait fait venir l' amiral Brueys dans l' Adriatique avec six vaisseaux , quelques frégates et quelques corvettes .
il s' était ménagé en outre un moyen de s' emparer de la marine vénitienne .
d' après le traité conclu , on devait lui payer trois millions en matériel de marine .
il prit sous ce prétexte tous les chanvres , fers , et caetera , qui formaient du reste la seule richesse de l' arsenal vénitien .
après s' être emparé du matériel sous le prétexte des trois millions , Bonaparte s' empara des vaisseaux , sous prétexte d' aller occuper les îles pour le compte de Venise démocratique .
il fit achever ceux qui étaient en construction , et parvint ainsi à armer six vaisseaux de guerre , six frégates et plusieurs corvettes , qu' il réunit à l' escadre que Brueys avait amenée de Toulon .
il remplaça le million que la trésorerie avait arrêté , donna à Brueys des fonds pour enrôler d' excellens matelots en Albanie et sur les côtes de la Grèce , et lui créa ainsi une marine capable d' imposer à toute la Méditerranée .
il en fixa le principal établissement à Corfou , par des raisons excellentes , et qui furent approuvées du gouvernement .
de Corfou , cette escadre pouvait se porter dans l' Adriatique , et se concerter avec l' armée d' Italie en cas de nouvelles hostilités , elle pouvait aller à malte , elle imposait à la cour de Naples , et il lui était facile , si on la désirait dans l' Océan , pour la faire concourir à quelque projet , de voler vers le détroit plus promptement que si elle eût été à Toulon .
enfin à Corfou , l' escadre apprenait à devenir manoeuvrière , et se formait mieux qu' à Toulon , où elle était ordinairement immobile .
" vous n' aurez jamais de marins , écrivait Bonaparte , en les laissant dans vos ports . "
telle était la manière dont Bonaparte occupait son temps pendant les lenteurs calculées que lui faisait essuyer l' Autriche .
il songeait aussi à sa position militaire à l' égard de cette puissance .
elle avait fait des préparatifs immenses , depuis la signature des préliminaires de Léoben .
elle avait transporté la plus grande partie de ses forces dans la Carinthie , pour protéger Vienne et se mettre à couvert contre la fougue de Bonaparte .
elle avait fait lever la Hongrie en masse .
dix huit mille cavaliers hongrois s' exerçaient depuis trois mois sur les bords du Danube .
elle avait donc les moyens d' appuyer les négociations d' Udine .
Bonaparte n' avait guère plus de soixante dix mille hommes de troupes , dont une très petite partie en cavalerie .
il demandait des renforts au directoire pour faire face à l' ennemi , et il pressait surtout la ratification du traité d' alliance avec le Piémont pour obtenir dix mille de ces soldats piémontais dont il faisait si grand cas .
mais le directoire ne voulait pas lui envoyer de renforts , parce que le déplacement des troupes aurait amené de nombreuses désertions , il aimait mieux , en accélérant la marche de l' armée d' Allemagne , dégager l' armée d' Italie , que la renforcer , il hésitait encore à signer une alliance avec le Piémont , parce qu' il ne voulait pas garantir un trône dont il espérait et souhaitait la chute naturelle .
il avait envoyé seulement quelques cavaliers à pied .
on avait en Italie de quoi les monter et les équiper .