à ce moment , tous réclamant Rouletabille , on vint le chercher et on rentra dans le salon .
Ivana s' aperçut immédiatement de l' état pitoyable dans lequel il se trouvait .
la Candeur dit rapidement à son ami : " surtout , toi , calme toi !
après tout , qu' est ce que ça peut te faire maintenant , Gaulow ?
parce qu' il a épousé , à la Karakoulé ...
tais toi donc !

eh !
un mariage dans ces conditions là , mon vieux , ça ne compte pas !

surtout un mariage musulman !

qu' y a t il ?
demanda Ivana , tout de suite inquiète .
mais rien , ma chérie , murmura Rouletabille ...
il faisait si chaud dans cette salle ...
j' admire que tu sois plus brave que moi .
tous ces jeunes gens sont si gentils .
ils t' aiment comme un frère , petit Zo .
moi aussi , je les aime bien , va ...
mais qu' est ce que c' est ça ?

demanda le reporter en voyant un groupe se dirigeant vers une table dans une attitude assez mystérieuse ...
depuis qu' il avait vu Monsieur Priski et qu' il l' avait entendu , tout était pour lui l' occasion d' un émoi nouveau ...
au fond de la salle , il y avait une dizaine de jeunes gens qui paraissaient porter quelque chose et le bruit courait de bouche en bouche : " une surprise ! ... une surprise ! ... "
quelle surprise ?

Rouletabille n' aimait pas beaucoup les surprises ...
et il allait se rendre compte de ce qui se passait , suivi d' Ivana , quand La Candeur accourut en levant les bras :
ça c' est épatant !

s' écriait il , le coffret byzantin !

le coffret byzantin !
s' écria Ivana ...
est ce bien possible ?

et elle claqua joyeusement des mains :
oh !
oui , c' est une surprise !

une bonne surprise !
c' est toi qui me l' as faite , petit Zo ?

non !
répondit Rouletabille ...
dont la vie sembla à nouveau suspendue , non , Ivana , ce n' est pas moi qui t' ai fait cette surprise là ...
et il s' avança avec courage , domptant la peur qui galopait déjà en lui , sans qu' il pût bien en connaître la cause , mais il sentait venir une catastrophe ...
la Candeur s' aperçut de ce trouble .
ne t' effraye pas , lui dit il , c' est certainement le père Priski qui a voulu te faire son cadeau de noces ...
tu te rappelles que nous avions laissé le coffret à Kirk Kilissé au moment de notre brusque départ !

il n' y a pas de quoi s' épouvanter ...
j' ai ouvert le coffret ...
il est plein de fleurs ...
ah !
murmura Rouletabille , qui recommençait à respirer ...
oui , ce doit être Priski ...
suis je bête ?

sûr !
fit La Candeur ...
venez , madame , continua La Candeur en entrainant Ivana ...
c' est un ami inconnu qui vous envoie des fleurs dans le coffret byzantin et elles sont magnifiques , ces fleurs !

ils s' avancèrent tous trois et se trouvèrent en face du coffret que l' on avait placé sur une table .
le couvercle en était soulevé et les magnifiques roses blanches dont il débordait embaumaient déjà toute la salle .
ce qu' il y en a !

fit La Candeur ...
ce qu' il y en a !

et sont elles belles !
dit Ivana en les prenant à poignées , et en plongeant ses beaux bras dans la moisson parfumée ...
tiens !

fit elle tout à coup , je sens quelque chose ?
qu' est ce qu' il y a là ?
et elle retira vivement sa main .
quoi ?
demanda Rouletabille , quoi ?
mais La Candeur avait déjà mis la main dans le coffret et en retirait un sac superbe et très riche comme on en voit chez les grands confiseurs aux temps de Noël et des fêtes ...
des bonbons !

jeta t il ...
des bonbons de chez Poissier !

il allait dénouer lui même les cordons du sac , quand Ivana le réclama .
il le lui remit et elle y plongea une main qu' elle ôta aussitôt en jetant un cri affreux ...
des clameurs d' horreur firent alors retentir la salle ...
aux doigts d' Ivana était emmêlée une chevelure ...
et elle secouait cette chevelure sans pouvoir s' en défaire !

et la chevelure sortit tout entière du sac avec la tête !

une tête hideuse , sanglante , au cou en lambeaux , aux yeux vitreux grands ouverts sur l' épouvante universelle ...
la tête de Gaulow !
hurla La Candeur .
la tête de Gaulow !
soupira Vladimir ...
la tête de Gaulow !
râla Rouletabille ...
la tête de Gaulow !
répéta la voix défaillante d' Ivana ...
et ils roulèrent dans les bras de leurs plus proches amis ...
cependant que les femmes , en poussant des cris insensés , s' enfuyaient ...
dans le logement du concierge , La Candeur et Vladimir , remis un peu de leur terrible émoi , faisaient subir un sérieux interrogatoire à Monsieur Priski et au groom .
Rouletabille était resté près d' Ivana qui avait perdu ses sens .
Monsieur Priski , encore sous le coup de la furieuse bousculade que lui avait imposée La Candeur et tout étonné d' être sorti vivant de sa terrible poigne , s' appliquait autant que possible , par ses réponses , à ne point déchainer à nouveau la colère du bon géant .
et il disait tout ce qu' il savait .
c' était lui , en effet , qui avait rapporté de Kirk Kilissé le coffret byzantin abandonné dans le kiosque par Rouletabille et Ivana dans le brouhaha de leur rapide départ pour Stara Zagora , où les attendait le général Stanislawoff .