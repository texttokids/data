un jour qu' elle etait au plus mal , on la vit avec etonnement se redresser tout a coup sur son seant , ecarter ses rideaux et mettre ses lunettes .
elle tenait a la main une lettre qu' on venait de lui apporter et qu' elle Deplia avec grand soin .
au haut de la feuille etait une belle vignette representant le temple de l' Amitie avec un autel au milieu et deux coeurs enflammes sur l' autel .
la lettre etait ecrite en grosse batarde , les mots parfaitement alignes , avec de grands traits de plume aux queues des majuscules .
c' etait un compliment de bonne année , a peu pres concu en ces termes :
" madame et chère marraine ,
" c' est pour vous la souhaiter bonne et heureuse que je prends la plume pour toute la famille , etant la seule qui sache ecrire chez nous .
papa , maman et mes freres vous la souhaitent de meme .
nous avons appris que vous etiez malade , et nous prions Dieu qu' il vous conserve , ce qui arrivera surement .
je prends la liberté de vous envoyer ci jointes des rillettes , et je suis avec bien du respect et de l' attachement ,
" votre filleule et servante ,
âpres avoir lu cette lettre , madame Doradour la mit sous son chevet , elle fit aussitot appeler Monsieur Despres , et elle lui dicta sa reponse .
personne , dans la maison , n' en eut connaissance , mais , des que cette reponse fut partie , la malade se montra plus tranquille , et peu de jours apres on la trouva aussi gaie et aussi bien portante qu' elle l' avait jamais et .
le bonhomme Piedeleu etait Beauceron , c' est a dire natif de la Beauce , ou il avait passe sa vie et ou il comptait bien mourir .
c' etait un vieux et honnete fermier de la terre de la Honville , pres de Chartres , terre qui appartenait a madame Doradour .
il n' avait vu de ses jours ni une foret ni une montagne , car il n' avait jamais quitte sa ferme que pour aller a la ville ou aux environs , et la Beauce , comme on sait , n' est qu' une plaine .
il avait vu , il est vrai , une rivière , l' Eure , qui coulait pres de sa maison .
pour ce qui est de la mer , il y croyait comme au paradis , c' est a dire qu' il pensait qu' il fallait y aller voir , aussi ne trouvait il en ce monde que trois choses dignes d' admiration , le clocher de Chartres , une belle fille et un beau champ de ble .
son érudition se bornait a savoir qu' il fait chaud en et , froid en hiver , et le prix des grains au dernier marche .
mais quand , par le soleil de midi , a l' heure ou les laboureurs se reposent , le bonhomme sortait de la basse cour pour dire bonjour a ses moissons , il faisait bon voir sa haute taille et ses larges epaules se dessiner sur l' horizon .
il semblait alors que les bles se tinssent plus droits et plus fiers que de coutume , que le soc des charrues fut plus etincelant .
à sa vue , ses garcons de ferme , couches a l' ombre et en train de diner , se decouvraient respectueusement tout en avalant leurs belles tranches de pain et de fromage .
les boeufs ruminaient en bonne contenance , les chevaux se redressaient sous la main du maître qui frappait leur croupe rebondie .
notre pays est le grenier de la France , disait quelquefois le bonhomme , puis il penchait la tete en marchant , regardait ses sillons bien alignes , et se perdait dans cette contemplation .
madame Piedeleu , sa femme , lui avait donne neuf enfants , dont huit garcons , et , si tous les huit n' avaient pas six pieds de haut , il ne s' en fallait guere .
il est vrai que c' etait la taille du bonhomme , et la mere avait ses cinq pieds cinq pouces , c' etait la plus belle femme du pays .
les huit garcons , forts comme des taureaux , terreur et admiration du village , obeissaient en esclaves a leur père .
ils etaient , pour ainsi dire , les premiers et les plus zeles de ses domestiques , faisant tour a tour le métier de charretiers , de laboureurs , de batteurs en grange .
c' etait un beau spectacle que ces huit gaillards , soit qu' on les vit , les manches retroussees , la fourche au poing , dresser une meule , soit qu' on les rencontrat le dimanche allant a la messe bras dessus bras dessous , leur père marchant a leur tete , soit enfin que le soir , apres le travail , on les vit , assis autour de la longue table de la cuisine , deviser en mangeant la soupe et choquer en trinquant leurs grands gobelets d' etain .