" je dis qu' il y a bien peu de temps , répondit il , qu' on a prêté serment au roi Louis le dix huitième , pour le violer déjà au bénéfice de l' ex empereur . "
" cette fois la réponse était trop claire pour que l' on pût se tromper à ses sentiments .
" général , dit le président , il n' y a pas plus pour nous de roi Louis le dix huitième qu' il n' y a d' ex empereur .
il n' y a que Sa Majesté l' Empereur et roi , éloigné depuis dix mois de la France , son État , par la violence et la trahison .
" pardon , messieurs , dit le général , il se peut qu' il n' y ait pas pour vous de roi Louis le dix huitième , mais il y en a un pour moi : attendu qu' il m' a fait baron et maréchal de camp , et que je n' oublierai jamais que c' est à son heureux retour en France que je dois ces deux titres .
" monsieur , dit le président du ton le plus sérieux et en se levant , prenez garde à ce que vous dites , vos paroles nous démontrent clairement que l' on s' est trompé sur votre compte à l' île d' Elbe et qu' on nous a trompés .
la communication qui vous a été faite tient à la confiance qu' on avait en vous , et par conséquent à un sentiment qui vous honore .
maintenant nous étions dans l' erreur : un titre et un grade vous ont rallié au nouveau gouvernement que nous voulons renverser .
nous ne vous contraindrons pas à nous prêter votre concours , nous n' enrôlerons personne contre sa conscience et sa volonté , mais nous vous contraindrons à agir comme un galant homme , même au cas où vous n' y seriez point disposé .
" vous appelez être un galant homme connaître votre conspiration et ne pas la révéler !
j' appelle cela être votre complice , moi .
vous voyez que je suis encore plus franc que vous ...
" ah ! mon père , dit Franz , s' interrompant , je comprends maintenant pourquoi ils t' ont assassiné . "
valentine ne put s' empêcher de jeter un regard sur Franz , le jeune homme était vraiment beau dans son enthousiasme filial .
Villefort se promenait de long en large derrière lui .
Noirtier suivait des yeux l' expression de chacun , et conservait son attitude digne et sévère .
Franz revint au manuscrit et continua :
" monsieur , dit le président , on vous a prié de vous rendre au sein de l' assemblée , on ne vous y a point trainé de force , on vous a proposé de vous bander les yeux , vous avez accepté .
quand vous avez accédé à cette double demande vous saviez parfaitement que nous ne nous occupions pas d' assurer le trône de Louis le dix huitième , sans quoi nous n' eussions pas pris tant de soin de nous cacher à la police .
maintenant , vous le comprenez , il serait trop commode de mettre un masque à l' aide duquel on surprend le secret des gens , et de n' avoir ensuite qu' à ôter ce masque pour perdre ceux qui se sont fiés à vous .
non , non , vous allez d' abord dire franchement si vous êtes pour le roi de hasard qui règne en ce moment , ou pour S Monsieur l' Empereur .
" je suis royaliste , répondit le général , j' ai fait serment à Louis le dix huitième , je tiendrai mon serment .
" ces mots furent suivis d' un murmure général , et l' on put voir , par les regards d' un grand nombre des membres du club , qu' ils agitaient la question de faire repentir Monsieur d' Épinay de ces imprudentes paroles .
" le président se leva de nouveau et imposa silence .
" monsieur , lui dit il , vous êtes un homme trop grave et trop sensé pour ne pas comprendre les conséquences de la situation où nous nous trouvons les uns en face des autres , et votre franchise même nous dicte les conditions qu' il nous reste à vous faire : vous allez donc jurer sur l' honneur de ne rien révéler de ce que vous avez entendu .
" le général porta la main à son épée et s' écria :
" si vous parlez d' honneur , commencez par ne pas méconnaitre ses lois , et n' imposez rien par la violence .
" et vous , monsieur , continua le président avec un calme plus terrible peut être que la colère du général , ne touchez pas à votre épée , c' est un conseil que je vous donne .
" le général tourna autour de lui des regards qui décelaient un commencement d' inquiétude .
cependant il ne fléchit pas encore , au contraire , rappelant toute sa force :
" je ne jurerai pas , dit il .
" alors , monsieur , vous mourrez , répondit tranquillement le président .
" Monsieur d' Épinay devint fort pâle : il regarda une seconde fois tout autour de lui , plusieurs membres du club chuchotaient et cherchaient des armes sous leurs manteaux .
" général , dit le président , soyez tranquille , vous êtes parmi des gens d' honneur qui essaieront de tous les moyens de vous convaincre avant de se porter contre vous à la dernière extrémité , mais aussi , vous l' avez dit , vous êtes parmi des conspirateurs , vous tenez notre secret , il faut nous le rendre . "
" un silence plein de signification suivit ces paroles et comme le général ne répondait rien :
" fermez les portes , dit le président aux huissiers .
" le même silence de mort succéda à ses paroles .
" alors le général s' avança , et faisant un violent effort sur lui même :
" j' ai un fils , dit il , et je dois songer à lui en me trouvant parmi des assassins .