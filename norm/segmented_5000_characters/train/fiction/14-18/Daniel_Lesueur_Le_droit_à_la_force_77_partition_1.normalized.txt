un .
F .
trois .
Limited right of replacement or refund if you discover a defect in this electronic work within quatre vingt dix days of receiving it , you can receive a refund of the money ( if any ) you paid for it by sending a written explanation to the person you received the work from .
if you received the work on a physical médium , you must return the médium with your written explanation .
The person or entity that provided you with the defective work may elect to provide a replacement copy in lieu of a refund .
if you received the work electronically , the person or entity providing it to you may choose to give you a second opportunity to receive the work electronically in lieu of a refund .
if the second copy is also defective , you may demand a refund in writing without further opportunities to fix the problem .