les étoiles n' avaient pas commencé de pâlir lorsque Scott , qui couchait dans une charrette vide , s' éveilla et se mit en silence à la besogne , il semblait inhumain d' éveiller à cette heure Faiz Ullah et l' interprète .
courbé , la tête près du sol , il n' entendit pas venir William .
il l' aperçut soudain debout à ses côtés dans sa vieille amazone roussâtre , les yeux encore lourds de sommeil et une tasse de thé avec une rôtie dans les mains .
il y avait par terre un bébé qui piaulait , couché sur un morceau de couverture , et un enfant de six ans risquait un oeil par dessus l' épaule de Scott .
allons , petit vaurien , dit Scott , comment diable veux tu avoir ta part si tu ne restes pas tranquille ?