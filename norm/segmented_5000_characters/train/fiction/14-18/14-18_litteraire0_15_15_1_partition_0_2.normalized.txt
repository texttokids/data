le petit frère de Dyhia obéit , quelque chose en lui obéit alors qu' il se voudrait ailleurs , là où une vérité méchante l' attend , un poinçon indélébile , oui , Dyhia est morte .
il pousse son auto .
il roule , pris dans une saisissante suspension , les joues mouillées , un pli au coin de la bouche , résigné à mes brèves indications comme si l' instant et le sens des choses tenaient à cela , se soumettre et escorter , trouer l' espace vacant , s' y évanouir en ne percevant pas ce que l' on y perd et ce que l' on y gagne , se fondre plus avant dans les ombres de la terre avec au dessus , son reversement , le ciel parcouru de tracés étoilés , vibrant d' une rumeur joyeuse et l' effarement de ne pas savoir ce que l' on est et même si l' on est Et puis il y a la réalité , les gestes à faire , les gestes qui se font , et les contingences Il faut faire ce qu' il faut .
une fois franchi le Chai , depuis le plateau , apparait à l' oeil un surprenant mouvement , des fourgons de gendarmerie grands comme des boites d' allumettes barrent la route dans le bas fond avant qu' elle ne monte en serpentant à travers la garrigue , dessinée par les véhicules de police toutes sirènes hurlantes , les gyrophares en astres de bravoure , et ce jusqu'à l' Estout dans un cortège serré , comme un déroulé de lettres annonciatrices de la loi alors que des autos et des motos dévalent bois et chemins dans la précipitation désordonnée d' une troupe en déroute .
Moha arrête sa voiture au premier barrage .
je descends du véhicule , lui saisis la main , l' entraîne , ne réponds pas aux injonctions des gendarmes .
roch henri apparait dans la lumière de phares , fait un signe de la main , nous courons vers lui et ensemble galopons jusqu' au domaine , entre les voitures de police qui interceptent des autos et des deux roues qui s' échappent et quelques piétons qui décampent , s' offusquent et jurent en anglais , je cours , je talonne mon voisin , je m' essouffle et dévide .
il y a l' homme au club , Roch Henri !
il faut le trouver .
il porte un pantalon de coton beige court , des chaussures de sport blanches , une marque à la jambe gauche .
l' entrée de la grande cour .
deux policiers en barrent le passage , nous ordonnent de reculer .
le commissaire obtempère .
je me précipite , Roch Henri me saisit le bras .
Moha me prend dans les siens .
un cordon d' hommes armes à la main est déployé tout autour du domaine , le long des hauts murs et au dedans dans l' immense patio .
il faut contenir la foule piégée .
un monde opaque d' une cinquantaine de spectateurs privés de fête trépigne et piétine dans la cour sous les lampes doucereuses , un reste de public qui n' a pu s' enfuir , qui gesticule et menace , une troupe de cris désordonnés , de vociférations discordantes , de grimaces que grossit l' ombre et que la lumière projette sur le sol empierré et les façades nues .
des portables sonnent et crépitent .
mon oeil ne peut supporter ce qu' il voit , c' est trop grand pour sa capacité .
une masse de corps se resserrent dans un tapage de désapprobation , un brouhaha de grenouilles , dirait Madame ma Grand mère reprenant Victor Hugo , des corps fiévreux du combat interrompu et qui se donnent à voir effervescents , qui se trémoussent , en sueur , traversés par plus puissant que les convenances , qui ont passé outre le bien et le mal , des corps débarrassés , et à vif la chose insoutenable de la voracité .
ça fait de la piaffe , prend de grands airs , crache ses piaillements , c' est sans dessus dessous , en cheveux , hors de sa peau , c' est virulent , quelqu' un a dérangé la table aux orgies , a interrompu la scène , quelqu' un a coupé court , les femmes se pressent à l' avant poste , relèguent les hommes vers l' arrière , se dressent en première ligne , leurs enfants érigés contre elles , de quoi a t on osé les priver ?
de jouer avec leurs fils ?
d' abord vivre , d' abord jouir , Enjoyment is good , vive la jouissance .
c' est cru , ça viole l' oeil , l' oeil qui se voit violé .
je secoue la tête , mes cheveux tombent , rideau sur les fracassés .
la petite figure de Melody Parmentier m' apparaît , et comment lors de sa garde à vue , elle disait le monde des détraqués de ses phrases cabossées , comment je l' écoutais .
là bas , au fond du commissariat , plus que ses mots , c' était ses lancers que Melody nous flanquait à la figure , ses jetés , ses jaillis , ses saillies ...
des cris en somme comme si la langue était morte , et sa douleur , son histoire , le monde qu' elle avait traversé , un monde qui gicle à nouveau du puits d' ombre , un monde à tête d' hydre de Lerné .
l' heure aux esclaves ne cesse de sonner , ici , là , d' une manière , d' une autre , crimes , tortures , attentats , suicides , viols .
et derrière , la figure de la fureur qui mastique la mort à pleine bouche .
jusqu'à quand ?