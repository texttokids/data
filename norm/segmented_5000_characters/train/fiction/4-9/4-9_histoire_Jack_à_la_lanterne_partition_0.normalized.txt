il y avait en Irlande un homme qui s' appelait Jack .
il était fort avare et fort cruel en plus d' aimer un peu trop l' alcool .
si bien que même le Diable en personne entendit parler de cet homme aux nombreux défauts .
un soir où Jack était à la taverne , comme à son habitude , le Diable se plaça au bar .
jack ne tarda pas à le bousculer .
le Diable proposa de lui offrir d' immenses pouvoirs en échange de son âme .
sur le point de se laisser convaincre , le malicieux Jack répondit : " très bien , j' accepterai ton pacte mais avant offre moi un dernier verre . " pour payer le tavernier , le Diable se transforma en pièce de six pence .
mais au lieu de payer son verre , Jack mit immédiatement la pièce dans sa bourse où se trouvait une croix en argent .
le Diable pesta car le contact avec la croix l' empêchait de reprendre sa forme initiale , il était prisonnier .
" libère moi tout de suite ! " cria le Diable .
" je te libèrerai à la condition que tu ne viennes pas réclamer mon âme avant dix ans . " lui répondit Jack .
n' ayant pas le choix , le Diable accepta .
dix ans après , Jack se promenait dans la campagne lorsque le Diable apparût .
" je viens chercher ton âme , je t' ai laissé dix ans comme convenu . " " d' accord , mais avant de te suivre peux tu cueillir une pomme sur cet arbre pour moi ? j' aimerais en manger une dernière fois . " le Diable n' y vit pas d' inconvénient et grimpa sur les épaules de Jack pour atteindre la branche .
le malin Jack attendit que le Diable touche l' arbre pour y graver une croix au couteau .
le Diable était à nouveau prisonnier car la croix l' empêchait de retirer ses mains de la branche .
" libère moi ! " tonna le Diable .
" je le ferai à la condition que tu ne prennes jamais mon âme . " répondit Jack .
le Diable bouillonnait de colère de s' être fait tromper par un simple humain .
mais n' eut d' autre choix que d' accepter la condition de Jack .
bien des années plus tard , Jack mourut .
comme il avait été cruel , avare et ivrogne , l' accès au Paradis lui fut refusé .
jack alla alors jusqu' aux portes de l' Enfer , mais le Diable lui refusa également l' entrée .
" rappelle toi la promesse que je t' ai faite , je ne peux pas prendre ton âme . "
" où vais je aller dans ce cas ? " lui répondit Jack .
" retourne d' où tu viens . " mais le chemin était froid et obscur car la nuit éternelle règne entre le monde des vivants et celui des morts .
jack demanda au Diable s' il avait de quoi éclairer la route .
le Diable se sentant généreux lui donna une braise de l' Enfer qui ne s' éteint jamais .
jack la plaça dans un navet qu' il trouva là , après l' avoir vidée .
depuis ce jour , l' âme de Jack est condamnée à errer au milieu des ténèbres , entre le monde des vivants et le monde des morts .
aujourd'hui on ne creuse plus des navets mais des citrouilles pour la Fête des Morts !
peut être apercevrez vous Jack avec sa lanterne pendant la nuit d' Halloween ?