depuis la rentrée , Sophie fait de la danse le mercredi après midi .
elle a vu à la télévision un ballet avec la magnifique étoile Sylvie Guillem , et a décidé qu' elle aussi deviendrait une grande danseuse !
avant de se mettre au lit , sa mère lui a lu Polina , une bande dessinée de Bastien Vivès sur une fille qui adore la danse comme elle .
d' ailleurs elle trouve qu' elle lui ressemble un peu !
après le cours de danse , dans les vestiaires , elle est avec les grandes : qu' elles sont belles et gracieuses !
Sophie est la dernière à sortir .
elle aperçoit un téléphone oublié dans un coin et le reconnait : c' est celui d' Eulalie !
Eulalie est la meilleure danseuse du groupe des grands .
vite , elle sort pour la rattraper .
elle la trouve en train de discuter avec des amis sur le trotoire .
" merci ! " lui dit Eulalie .
et elle lui fait un bisou pour la remercier .
aujourd'hui , Sophie a bien dansé , et elle s' est faite une nouvelle amie .