suite à la disparition de Luc Bondy le vingt huit novembre dernier , le spectacle Othello ne sera pas créé .
il est remplacé par la reprise de Tartuffe , spectacle mis en scène par Luc Bondy en mars deux mille quatorze, qui avait conquis le public .
on y retrouve Micha Lescot dans le rôle titre , au coeur d' une distribution en partie renouvelée .
marie louise Bischofberger et Vincent Huguet , collaborateurs artistiques de Luc Bondy , s' associent pour reprendre sa mise en scène .
depuis qu' Orgon a rencontré Tartuffe , sa piété tranquille est devenue fanatisme .
comment a t il pu succomber à son emprise , au point de lui faire don de tous ses biens et vouloir lui livrer sa propre fille ?
et jusqu' où devra aller Elmire , son épouse , pour lui ouvrir les yeux ?
explorant les mécanismes qui rendent possible le succès de l' imposture , la mise en scène de Luc Bondy nous offre , entre farce et terreur , le portrait génial d' un incroyable aveuglement .
" le style du suisse allemand Luc Bondy , c' est l' élégance mélancolique , la légèreté tragique , la profondeur romanesque désenchantée : on ne sait quel parfum viennois d' entre les deux guerres , un gout d' apocalypse joyeuse façon Schnitzler mâtiné de Freud .
il aime raconter des histoires avec des comédiens , faire théâtre avec des présences et des corps qu' il resculpte selon les intrigues .
ce n' est pas un théoricien , mais un charnel qui fait gouter la force de l' instant scénique , le métamorphose en espèce de fête .
en costumes modernes , dans un appartement digne des meilleures revues de design , le Tartuffe de Luc Bondy devient le portrait d' une grande famille bourgeoise d' aujourd'hui , suffisamment déglinguée et tourmentée pour que puisse s' y immiscer un aventurier arriviste , éperdument amoureux de la maîtresse de maison et deuxième épouse d' Orgon , qui boit trop de whisky , prend trop de cachets ...
à coup de détails visuels , Bondy épaissit le mystère du récit , devenu saga bunuélienne sur la famille et la bourgeoisie .

" de lourds rideaux de velours ouvrent et ferment les grands dégagements de cet espace élégant .
de beaux sièges , des tables , des chaises .
un crucifix , une vierge de céramique dans sa niche .
on est dans la demeure bourgeoise et cossue sans ostentation d' Orgon .
la scénographie forte et harmonieuse de Richard Peduzzi installe immédiatement une atmosphère et correspond parfaitement à l' esprit de Tartuffe .

on ne joue que le texte , strictement le texte de cette pièce puissante et grave .
et c' est au texte que s' en tient d' abord scrupuleusement Luc Bondy .
mais que d' imagination dans les gestes , les humeurs , les mouvements !

tout sonne juste , tout est juste .
les interprètes redonnent aux répliques toute leur pertinence .
c' est un homme de plateau qui a écrit Le Tartuffe ou l' imposteur .
chaque mot correspond à une action , chaque action est naturelle .
on en oublierait les vers et les rimes pourtant suivis avec rigueur .
jamais , et pourtant on en a vu , des Tartuffe , jamais le sentiment de la réalité , de la vérité n' avait été aussi saisissant .

" Luc Bondy électrise l' Odéon dans sa version glacée enfiévrée d' un classique de la littérature française . son Tartuffe s' inscrit avec une justesse confondante dans notre société minée par le pouvoir trompeur des postures et de la parole . l' aspect artificiel des sentiments orchestrés par Tartuffe trouve un écho pertinent dans la scénographie inhospitalière de Richard Peduzzi . Micha Lescot s' impose avec une classe machiavélique dans le rôle titre . "
représentations avec audiodescription simultanée diffusée par casque en collaboration avec l' association Accès Culture
un tarif de trente euros ( au lieu de quarante euros ) , incluant la place et le cout de la prestation , est proposé pour la personne aveugle ou malvoyante et son accompagnateur .