le dôme du Rocher , aussi appelé la coupole du Rocher , est une mosquée qui a été construite sur l' emplacement de l' ancien Temple de Salomon ( détruit une deuxième fois en soixante dix par les Romains de Titus ) , à Jérusalem .
il est considéré comme le troisième lieu saint de l' islam , après la Mecque et Médine , en Arabie saoudite .
selon la légende , c' est là que le prophète Mahomet aurait fait son voyage nocturne , puis son élévation vers les cieux .
c' est aussi sur ce rocher qu' Abraham aurait failli sacrifier son fils Isaac avant qu' un ange ne l' arrête .
la mosquée fut construite par le calife omeyyade Abd al Malik de six cent quatre vingt onze à six cent quatre vingt douze , c' est à dire soixante douze ans après l' Hégire .
elle est située sur le harâm al sharif , en face de la mosquée Al Aqsa , qui est la plus vieille mosquée que l' on connaisse .
après qu' Hélène , la mère de l' empereur Constantin , a découvert la croix de Jésus , les chrétiens laissèrent à l' abandon cette partie de la ville pour se concentrer sur sa partie occidentale .
il est reconnu que c' est à cet endroit que le roi biblique Salomon construisit au VIe siècle avant Jésus Christ le premier Temple , qui fut détruit par les Perses quelques siècles plus tard .
il fut ensuite reconstruit et agrandi par Hérode et détruit de nouveau en soixante dix ap .
j c
sur la terrasse , il y avait un temple romain consacré à Jupiter , restant en ruines afin de témoigner symboliquement de la victoire du christianisme sur les religions païennes .
il y a alors de très nombreuses églises chrétiennes dans la ville de Jérusalem .
les ruines de l' esplanade seront réutilisées afin de créer un symbole fort de la religion musulmane face au judaïsme et au christianisme , deux religions que l' islam ne considère pas comme justes .
le dôme fut restauré de nombreuses fois .
au IXe siècle , le calife abbasside Al Mamun effaça le nom d' Abd al Malik , le calife qui fit construire la mosquée , pour le remplacer par le sien .
au début du Xe siècle , la coupole , qui auparavant était en cuivre , est recouverte d' une feuille d' or .
plus tard , cette couverture est remplacée par du plomb .
au XIe siècle , le site , et particulièrement le dôme , subit deux tremblements de terre .
avec la prise de Jérusalem par les croisés en dix cent quatre vingt dix neuf , la mosquée est reconvertie en église , mais l' édifice est conservé tel quel .
le croissant est remplacé par la croix et on construit un autel en pierre .
pendant cette période , les juifs et les musulmans ont l' interdiction de pénétrer dans la ville .
à cause du symbole que représente le Rocher ( comme au Saint Sépulcre , il y a un rocher au centre ) , les croisés arrachèrent quelques bouts de terre afin de les vendre à des pèlerins .
pour empêcher ce commerce , les rois croisés firent installer une grille en métal , dont certaines parties sont encore visibles .
le dôme du Rocher est consacré comme église chrétienne en onze cent quarante deux .
cependant , on ne veut pas en faire un endroit plus important que le Saint Sépulcre ( bien que plusieurs évènements de l' Ancien et du Nouveau Testament lui soient associés ) .
au cours des siècles , de nombreux éléments de la mosquée ont été remplacés .
c' est le cas des mosaïques intérieures , où l' on trouve des restaurations mamelouks dans la coupole ou , par exemple , dans les plafonds peints dont les motifs datent du XIIIe siècle .
le décor extérieur est complètement remplacé par des carreaux de céramique ottomans sur l' ordre de l' empereur Soliman le Magnifique .
quatre campagnes de restauration ont été menées par la suite , à l' initiative des sultans ou des califes .
il y a également eu une restauration en dix neuf cent soixante .
en dix neuf cent vingt sept , une étude est menée sur les mosaïques par Marguerite van Berchem , qui en fait une description détaillée et conclut qu' elles ont été faites par des artistes syriens et non byzantins .
le dôme du Rocher a un plan octogonal , organisé autour du Rocher de la Fondation ( rocher au milieu de la mosquée ) , qui est l' endroit le plus élevé du mont .
il repose sur une plate forme artificielle rectangulaire qui possède huit escaliers .
le dôme est supporté par une arcade circulaire comportant quatre piliers et douze colonnes .
l' espace s' organise en un double déambulatoire , ce qui est étonnant , car cela ne fait pas partie des pratiques musulmanes , mais plutôt catholiques .
le premier déambulatoire est défini par la colonnade circulaire ( celle qui supporte la coupole ) et la deuxième colonnade .
le deuxième déambulatoire se situe entre la deuxième colonnade et les murs extérieurs , eux aussi octogonaux .
il y a quatre portes en direction des points cardinaux , celle orientée vers la mosquée Al Aqsa et donc vers La Mecque disposant d' un portique plus imposant .
il possède également deux mihrab , situés dans la grotte du Dôme , donc non visibles depuis l' intérieur de la mosquée .
une petite annexe , comprenant des colonnes à chapiteaux byzantins , abrite le bassin aux ablutions .
le Dôme ressemble beaucoup à l' église du Saint Sépulcre , probablement parce qu' il a des héritages architecturaux byzantins .