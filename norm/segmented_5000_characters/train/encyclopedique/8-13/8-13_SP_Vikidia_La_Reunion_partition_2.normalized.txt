l' une des premières espèces vivant sur l' île fut le dodo .
très longtemps , cette espèce a vécu sans prédateur .
l' arrivée des premiers colons sur l' île et une chasse intensive vont malheureusement conduire à l' extinction de l' espèce .
l' Île Bourbon est d' abord peuplée par des colons français , venus de métropole ou de Madagascar , qui amènent avec eux leurs familles et leurs serviteurs .
pendant un temps , l' île n' est qu' un port d' étape pour les bateaux qui voyagent entre l' Asie et l' Europe , mais bientôt , elle sera exploitée pour ses ressources agricoles : d' abord pour les plantations de café , puis de canne à sucre : l' agriculture locale a besoin de plus en plus d' esclaves achetés en Afrique .
les colons s' installent sur les côtes de l' île ( les bas ) , les plantations sur les flancs ( les hauts ) du massif du piton des Neiges .
les esclaves en fuite vont peu à peu former des villages ( nommés îlets ) dans les cirques , difficiles d' accès .
après l' abolition de l' esclavage ( vingt décembre mille huit cent quarante huit ) , les planteurs de canne recrutent des travailleurs venus d' Inde , puis de Chine .
tous ces gens ont fait souche dans l' île , et la Réunion est un endroit où la population est très métissée .
le créole réunionnais est couramment parlé , même si la langue officielle est le français .
chapelle dans l' église Sainte Anne de Saint Benoît à la Réunion .
les blancs créoles descendants des colons d' origine européenne .
ils forment environ vingt cinq pour cents de la population .
les métropolitains arrivés récemment , surnommés les Zoreils sont environ cinq pour cents de la population ,
les descendants des esclaves africains , appelés Cafres .
ils sont plus ou moins métissés avec les blancs .
ils représentent trente cinq pour cents de la population ,
les descendants des indiens engagés au XIXe siècle après l' abolition de l' esclavage .
on les appelle les Malbars .
ils forment environ vingt cinq pour cents de la population .
s' y joignent des indo musulmans du Gujerat ( environ trois pour cents ) surnommés les Zarabs .
les premiers habitants de La Réunion sont des Européens .
rapidement , ces colons importent , grâce à la traite des noirs , des esclaves provenant d' Afrique .
en mille six cent soixante cinq , on a la trace d' une vente d' esclaves .
en mille six cent soixante dix neuf , il y a sept cent cinquante habitants , dont près de la moitié sont des esclaves .
ces esclaves travaillent dans les plantations de café qui connaissent un grand essor au XVIIIe siècle .
en mille sept cent trente cinq , les esclaves sont sept cinq cents pour une population de colons de un sept cents personnes .
en mille sept cent quatre vingt quatorze , lorsque la Convention nationale abolit l' esclavage , il y a trente cinq esclaves et un personnes libres .
mais Napoléon Bonaparte rétablit l' esclavage .
en mille huit cent neuf , les britanniques s' emparent de l' île .
en mille huit cent onze , ils répriment sévèrement une révolte d' esclaves dans la région de Saint Leu .
en mille huit cent trente , la traite des esclaves est interdite par le gouvernement du roi Louis Philippe Ier .
mais il faut attendre mille huit cent quarante huit , et la proclamation de la Deuxième République , pour que l' esclavage soit aboli à La Réunion ( et dans les autres colonies françaises ) .
les esclaves s' enfuyait dans les hauts de l' ile comme Cilaos Mafate et Salazi
encore aujourd'hui , les hauts sont utilisés pour la culture de la canne à sucre .
mais dans les cirques , il existe des cultures locales bien développées : chouchou ( christophine ou chayote , dans le cirque de Salazie ) , lentilles , vigne ( cirque de Cilaos ) par exemple .
la canne à sucre forme l' essentiel des exportations de l' île .
située dans les tropiques , l' île possède de grandes plages et des lagons de corail sur une partie de son pourtour , qui intéressent les touristes .
il existe un important réseau de chemins de randonnées favorisé par l' histoire de l' île et sa géographie .
certains sont sur les côtes et d' autres , dirigés vers les pitons et l' intérieur des cirques , permettent d' y pratiquer la randonnée en montagne .
l' activité volcanique attire également , car elle offre un spectacle inoffensif , les éruptions du Piton de la Fournaise étant de type effusif .
la musique occupe une place majeure dans la vie de la population réunionnaise .
les deux styles emblématiques de l' île sont le maloya et le séga .
le maloya est une musique qui se caractérise par le blues ternaire et il a été créé clandestinement par les esclaves africains et malgaches pour transcender leurs souffrances et leurs douleurs .
le séga se danse , lui , en dandinant les hanches et en tournoyant sur soi et l' homme autour de la femme .
le séga est une musique plus entrainante et joyeuse que le maloya .
le maloya est classé au Patrimoine culturel immatériel de l' humanité de l' UNESCO depuis deux mille neuf .
la cuisine réunionnaise est savoureuse et variée .
la spécialité est le carry : il s' agit d' un plat composé de viande ou de poisson agrémenté d' épices , d' oignons , d' ail et le plus souvent de tomates .
il est servi accompagné de riz et de grains comme les lentilles , les pois ou les haricots .
on peut le manger avec du rougail qui est un condiment composé de piment , oignons et de légumes divers ( rougail tomate , rougail concombre ...
