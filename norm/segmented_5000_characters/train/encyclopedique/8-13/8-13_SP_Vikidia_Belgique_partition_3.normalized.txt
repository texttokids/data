la Belgique est célèbre pour ses frites , qu' on déguste avec des moules dans les friteries ( frietkot ou frituur en Flandre ) mais aussi des carbonades ( un ragout à la bière ) ou du steak , son non moins célèbre waterzooï ou ses boulets à la liégoise .
les belges sont également réputés pour leur production de bière , les gaufres ( bruxelloises ou liégoises ) et la qualité de leurs chocolats .
mais la cuisine belge ne se limite pas à cela .
c' est un pays producteur de fromages , et on y cultive beaucoup en serre des fruits comme les raisins .
les fraises cultivées en pleine terre sont réputées dans le monde ainsi que les célèbres chicons , appelés endives en France ( sauf dans le Nord !

endives ou chicons ?
les voici avec leurs racines !
be est le domaine Internet de premier niveau de Belgique administré par DNS Belgium .
les belges pratiquent beaucoup de sports , les disciplines les plus populaires étant le football , le cyclisme , le basket ball , la natation , la gymnastique .
le coureur de demi fond Ivo Van Damme a remporté deux médailles d' argent aux Jeux olympiques de mille neuf cent soixante seize à Montréal , mais il a trouvé la mort quelques semaines plus tard dans un accident de la route .
depuis , un grand meeting d' athlétisme , le " mémorial Van Damme " se tient chaque année à Bruxelles en sa mémoire .
aux Jeux olympiques de Pékin en deux mille huit , la sauteuse en hauteur Tia Hellebaut a gagné la médaille d' or .
l' heptathlon des Jeux Olympiques de Rio ont été remportés par Nafissatou Thiam .
elle remporta également le penthathlon indoor des championnats d' Europe deux mille dix sept .
en deux mille dix sept , elle remporte l' heptathlon au championnats du monde d' athlétisme .
les coureurs belges , depuis l' origine , ont obtenu d' excellents résultats .
la plus ancienne des classiques au monde est Liège Bastogne Liège , créée en mille huit cent quatre vingt douze .
de nombreuses autres courses ont lieu en Belgique , notamment le Tour de Belgique , le Tour des Flandres , les classiques ardennaises et les classiques flandriennes .
le premier belge à avoir gagné le Tour de France est Odiel Defraye , en mille neuf cent douze .
le champion cycliste belge le plus titré est Eddy Merckx ( actuellement , le meilleur de tous les temps ) : cinq Tours de France , cinq tours d' Italie ( Giro ) , un tour d' Espagne ( Vuelta ) , trois titres de champion du monde sur route , de nombreuses classiques ( Paris Roubaix , Milan San Remo , Liège Bastogne Liège ) et un record de l' heure sur piste .
dans les années deux mille et au début des années deux mille dix , deux joueuses belges de tennis ont atteint le rang de numéro un mondial : justine Henin et Kim Clijsters .
l' Équipe de Belgique de football , surnommée les " diables Rouges " ou " rode Duivels " en néerlandais , a connu son heure de gloire dans les années mille neuf cent quatre vingts , avec des joueurs comme Enzo Scifo ou Jean Marie Pfaff .
en mille neuf cent quatre vingt six , la Belgique termine quatrième de la Coupe du Monde de football .
l' Équipe nationale se hisse à la troisième place lors de la Coupe du monde de football de deux mille dix huit .
le Championnat de Belgique se joue depuis mille huit cent quatre vingt quinze : le club le plus titré est le RSC Anderlecht ( trente trois titres ) .
actuellement , il existe beaucoup de jeunes belges demandés par les plus grandes équipes d' Europe : éden et Thorgan Hazard , Thibaut Courtois , Kevin De Bruyne , Nacer Chadli , Dries Mertens et Romelu Lukaku
la Belgique est depuis le cinq novembre deux mille quinzepremière nation au classement mondial FIFA deux .