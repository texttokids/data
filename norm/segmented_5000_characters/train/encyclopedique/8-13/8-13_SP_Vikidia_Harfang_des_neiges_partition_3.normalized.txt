le harfang et les hommes préhistoriques durant la période glaciaire
autrefois , il y a bien longtemps , lorsque le climat était plus froid , le harfang des neiges vivait beaucoup plus au sud qu' aujourd'hui .
ainsi , durant la dernière glaciation , qui s' est terminée il y a environ dix ans , on trouvait des harfangs des neiges au sud de l' Europe , et notamment dans les Pyrénées .
la fin de cette période glaciaire correspond à une période appelée le Magdalénien , durant laquelle on sait que l' homme préhistorique connaissait déjà le harfang .
de nombreux os de harfang ont ainsi été retrouvés dans une grotte , la grotte de Bourouilla , dans le Pays basque .
on sait que l' homme préhistorique chassait le harfang , et qu' il utilisait sa chair , mais aussi ses os , même si on ne sait pas bien exactement pour quoi faire ...
les os de harfang ont pu servir à faire des outils , ou bien servaient ils à l' art , qui commence justement à bien se développer au magdalénien ?
l' homme préhistorique a peut être été subjugué par les magnifiques plumes blanches du harfang , mais , comme les plumes ne se fossilisent pas , il est impossible de le savoir .
il est en tout cas probable que l' homme préhistorique ait à la fois chassé , mangé et vénéré le harfang des neiges .
pour les indiens Oglalas , le harfang est un animal sacré , qui symbolise le nord et le vent du nord .
les meilleurs guerriers portent des coiffes faites de plumes de harfang pour symboliser leur bravoure .
le harfang est également un animal très présent dans le folklore inuit , où il est appelé Uppik , ou Upialuk .
il est très représenté dans l' art inuit .
les Inuits utilisent parfois un uniujaq , un morceau de cuir de caribou attaché au bout d' une longue ficelle , qu' ils font trainer dans la neige pour imiter un lemming en train de courir .
le harfang s' y trompe , et , croyant qu' il s' agit d' un vrai lemming , fonce sur le morceau de cuir pour le capturer trois .
l' huile de harfang des neiges est utilisée traditionnellement par les Inuits , sous forme d' onguent comme remède contre l' impétigo et les lésions .
quatre
Okpik ( du nom inuit du harfang ) est le nom d' un stage de préparation en milieu froid chez les Boys Scouts of America .
le harfang des neiges est représenté sur de nombreux timbres poste , notamment ceux des pays du nord de l' Eurasie et d' Amérique du Nord :
timbres des îles Marshall du onze mai deux mille huit, représentant des chouettes et des hiboux , dont le harfang des neiges .
chaque timbre vaut zéro , quarante deux dollars
premier jour cinq d' une série de timbres finlandais du quatre septembre mille neuf cent quatre vingt dix huit, montrant plusieurs chouettes et hiboux , dont le harfang des neiges .
le harfang n' est pas l' oiseau le plus fréquent du Québec , mais il est l' un des plus remarquables .
en mille neuf cent quatre vingt sept , il a été choisi comme l' un des trois emblèmes du Québec ( avec l' iris versicolore et le bouleau jaune ) six .
il symbolise la blancheur de l' hiver , la culture nordique et l' extension sur un vaste territoire .
le harfang des neiges était représenté sur l' ancien billet de cinquante dollars canadiens ( image )
dans la série Harry Potter , Hedwige , le hibou d' Harry , est un harfang des neiges .
dans les versions françaises , il y a une erreur de traduction , et Hedwige est décrite comme une chouette laponne , qui est en fait une espèce différente .
au cinéma , le personnage de Hedwige est interprété par trois harfangs des neiges différents .
bien qu' Hedwige soit une femelle , les oiseaux qui ont joué son rôle sont des mâles , plus petits , et donc plus faciles à porter sur l' épaule , ou le bras , par exemple .
dans la série Les Gardiens de Ga' Hoole , qui met en scène différentes espèces de chouettes et de hiboux , plusieurs personnages sont des harfangs des neiges :
Boron et Baran , étaient le roi et la reine de Hoole , jusqu'à l' arrivée de Coryn
miss Plonk est la Chanteuse du Grand Arbre , et la compagne de Doc Bonbec
Doc Bonbec est un célèbre traqueur , et deviendra le compagnon de Miss Plonk
le Forgeron Solitaire du Pays du Soleil d' Argent est la soeur de Miss Plonk .
ses plumes sont devenues noires , à cause du charbon
Tatie Finnie , était la Gardienne de Soren , à la pension de Saint Aegolius .