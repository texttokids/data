Rome , surnommée la " ville éternelle " , est la capitale de l' Italie , chef lieu de la région du Latium .
elle compte environ deux , cinq millions d' habitants .
c' est la plus grande ville d' Italie .
capitale de l' Empire Romain pendant l' Antiquité et siège du Vatican , Rome a exercé une grande influence dans l' histoire du monde .
le Risorgimento la fait devenir la capitale de l' Italie et le siège des institutions publiques .
Rome est une des grandes places économiques de l' Italie ( avec Milan ) et de nombreuses entreprises y ont leur siège .
c' est aussi un important centre médiatique du pays ( journaux , radios , chaines de télévision ...

Rome est visitée par près de douze millions de touristes chaque année .
Rome est située dans la région du Latium , au centre de l' Italie .
elle est traversée par le Tibre , à vingt deux kilomètres de la mer Tyrrhénienne .
sa superficie est d' environ un deux cent quatre vingt cinq kilomètres carrés , ce qui en fait la commune la plus grande d' Italie , dépassant Paris , Londres et New York en termes de superficie .
il existe vingt neuf communes limitrophes autour de Rome .
le site de Rome comprend sept collines : le Capitole , le Palatin , le Viminal , le Quirinal , l' Esquilin , le Caelius et l' Aventin .
les collines de Rome rassemblent encore tout le centre ville .
au début de la cité ( au VIe siècle avant Jésus Christ ) , elle comptait probablement dix sept habitants .
à l' apogée de l' Empire romain , Rome était la cité la plus grande et la plus peuplée au monde avec un million d' habitants ( environ ) .
cependant après les divers sacs suivant la chute de l' Empire Romain , elle traversa une période assez sombre .
au XVIe siècle , elle comptait cent habitants .
après le Risorgimento , la population passa les cinq cents habitants , puis ce nombre s' est élevé au long du XXe siècle .
des années mille neuf cent quatre vingts aux années deux mille , la population stagna et même recula un peu .
de deux mille deux à deux mille sept , la population augmenta , passant deux huit cents à deux huit cent quatre vingt dix habitants .
l' immigration ( particulièrement celle de l' Europe de l' Est ) a toujours constitué une grande communauté ( environ trois cent soixante étrangers " légaux " ) .
aujourd'hui , elle est la quatrième ville la plus peuplée en Europe après Londres , Berlin et Madrid .
selon la légende , Rome a été fondée en sept cent cinquante trois avant Jésus Christ sur le mont Palatin par Romulus , fils du dieu de la guerre ( Mars ) .
Romulus avait été recueilli et élevé , avec son frère Rémus , par une louve .
depuis , la louve est un des symboles de Rome .
entre le VIIe siècle avant Jésus Christ et le Ie siècle , l' État de Rome qui n' était au début qu' un regroupement de villages sur le site des sept collines de Rome , étend énormément son territoire jusqu'à devenir l' Empire romain , dont Rome reste la capitale .
son régime est une monarchie , puis une république et un Empire après Jules César .
sous l' Empire , la ville antique comptait à peu près un demi million habitants ( la plus grande ville du monde de l' époque ) .
de nombreux monuments sont bâtis : des temples , le Forum romanum , le Colisée .
partout dans l' Empire , les Romains construisaient des villes romaines imitant leur capitale .
le christianisme , qui se développe à Rome sous l' Empire , devient la religion officielle au IVe siècle .
avec les invasions des Barbares sur l' Europe , Rome est prise au Ve siècle avant Jésus Christ , ce qui provoque sa destruction partielle .
suivent mille ans durant lesquels l' activité et la puissance de la ville sont réduites .
la ville reste néanmoins le siège des papes , sauf de mille trois cent neuf à mille quatre cent vingt , quand les papes ont siégé en Avignon , deux ou trois personnes prétendant être le pape dans plusieurs villes en même temps .
par la suite , les papes redonnent à Rome une bonne partie de sa splendeur , faisant venir les plus grands artistes , comme Michel Ange ou Raphaël .
les grandes familles aristocratiques construisent des palais comme le Palais Farnèse ou le Palais Borghese .
Rome a été le centre majeur de l' Antiquité , et plus tard , durant la Renaissance , cette situation centrale réapparait grâce à l' installation du Vatican .
Rome est annexée par Napoléon Bonaparte au royaume d' Italie qu' il a créé .
après sa défaite , en mille huit cent quinze , Rome repasse sous le contrôle du pape .
au milieu du XIXe siècle , avec le Risorgimento , elle devient la capitale du nouveau royaume d' Italie puis de la république à partir de mille neuf cent quarante six .
elle est donc le siège des institutions de l' État .
le fascisme donne à Rome une place centrale .
de nouveaux bâtiments sont construits : Mussolini veut qu' elle devienne la capitale du régime fasciste .
les quartiers périurbains et les banlieues sont aménagés .
l' exposition universelle de mille neuf cent quarante deux , qui devait s' y dérouler , est annulée .
après la guerre , la ville continue à s' étendre avec la création de nouveaux quartiers .
Rome utilisa son exceptionnel patrimoine antique pour le mettre à profit aux évènements qui furent organisés dans la ville ( Olympiades , Coupe du monde de football , et caetera ) .