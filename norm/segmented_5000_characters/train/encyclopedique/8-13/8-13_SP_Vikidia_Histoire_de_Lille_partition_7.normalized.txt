à l' inverse de la Résistance , des mouvements de collaboration se développent dans le Nord dès mille neuf cent quarante .
la Fédération flamande de France ( dont le sigle VVF renvoie à son nom en néerlandais , Vlaamsch Verbond van Frankrijk ) insiste sur la proximité culturelle entre flamands et allemands pour justifier le rattachement de Lille à la Belgique .
d' autres mouvements , d' ampleur nationale , sont aussi influents dans le réseau de collaboration :
le Francisme , qui diffuse des tracts mais qui est en perte de vitesse durant l' occupation ,
le Parti Populaire français ( PPF ) , qui soutient l' action du maréchal Pétain et nourrit un anticommunisme , mais qui fait face à un manque d' argent et de militants ,
la Milice française , mise en place par le régime de Vichy , composée d' une trentaine de Lillois qui effectuent des arrestations , des contrôles d' identités et qui participent au marché noir .
à la Libération , les collaborateurs seront jugés et condamnés par les tribunaux .
trois neuf cent quatre vingt dix neuf personnes ont ainsi été sanctionnées par la Cour d' appel de Douai et par les tribunaux militaires .
mais parmi ces personnes condamnées , trois cent sept seront condamnées à mort .
la plupart des exécutions a touché des employés et des ouvriers , qui représentent soixante pour cents des exécutés , alors que seuls dix .
cinq pour cents des exécutés étaient des militaires ou des policiers .
en effet , le chômage , la misère et la sous alimentation dont ont été victimes ces personnes modestes ont favorisé les actes de collaboration ( qui leur permettaient d' avoir des avantages de la part des allemands ) .
les relations sont difficiles entre les Lillois et l' occupant allemand : les plus âgés qui ont vécu la Première Guerre mondiale ont encore de très mauvais souvenirs de l' attitude méprisante des allemands tandis que les plus jeunes sont effrayés par le régime nazi .
afin de favoriser l' intégration du Nord Pas de Calais dans un grand État flamand , les allemands tenteront de rendre les relations plus cordiales entre la population et eux .
ainsi , ils ouvrent des cantines , aident les personnes âgées et offrent des bonbons aux enfants .
la vie reste pourtant très difficile pour les Lillois : affamés , ils ne manquent pas de piller de nombreux commerces pour y trouver de la nourriture .
pour faire face à cette situation , le préfet décide de répartir le plus équitablement la nourriture .
ainsi , chaque Lillois est classé selon son âge et son sexe pour mieux répondre à ses besoins .
Lille est la première ville à mettre en place des cartes de ravitaillement , c' est à dire des cartes qui permettent à chaque personne ou chaque famille d' obtenir une quantité limitée de ressources , telles que le pain , le lait ou encore , pour se chauffer , le charbon .
tous les besoins des Lillois ne sont pas couverts : un marché noir s' organise alors pour les couvrir davantage .
des denrées , des produits chimiques , des médicaments , du charbon et beaucoup d' autres ressources sont vendues illégalement , souvent la nuit ( d' où le nom de " marché noir " ) , pour subvenir aux besoins des Lillois au delà de la limite des cartes de ravitaillement ( qui ne permettent de subvenir qu' à une partie de leurs besoins journaliers ) .
on estime que ce marché noir couvre jusqu'à soixante pour cents des besoins des Lillois mais les autorités punissent très sévèrement ces actions .
les marchés qui sont tenus chaque semaine à Lille sont désertés par les Lillois parce qu' il faut donner des tickets de rationnement pour tout achat .
néanmoins , les marchands de poissons sont ceux qui ont le plus de succès sur les marchés de Wazemmes et de la rue Solférino .
des files d' attentes se multiplient devant les épiceries et magasins d' alimentation .
l' État ne répond pas à tous les besoins .
des mouvements , notamment chrétiens , s' organisent pour venir à l' aide des populations les plus dans le besoin , tels que Action Catholique Ouvrière ( ACO ) , Ligue Ouvrière Chrétienne ( LOC ) ou encore , Secrétariat Social de Lille .
des soupes populaires , qui sont des soupes servies gratuitement aux personnes démunies , sont mises en place dès novembre mille neuf cent quarante. des repas populaires , à prix très bien , sont également mis à leur disposition , ainsi que des cantines scolaires dès mille neuf cent quarante et un .
des refuges chauffés les accueillent tous les après midis , sauf le dimanche , avec à leur disposition des salles de lectures , des infirmières ou encore , des machines à coudre .
le Secours national , qui avait vu le jour lors du premier conflit mondial , est de nouveau actif au cours de la Seconde Guerre mondiale , en aidant et sauvant les victimes de bombardements .