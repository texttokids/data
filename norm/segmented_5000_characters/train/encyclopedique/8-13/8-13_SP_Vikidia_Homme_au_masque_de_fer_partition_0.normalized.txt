l' homme au masque de fer est un mystérieux prisonnier anonyme emprisonné , en France , entre mille six cent soixante neuf et mille sept cent trois , année à laquelle il meurt un .
ses conditions de détention très particulières ( il était forcé de porter un masque ressemblant à un heaume , le fameux masque de fer ) , l' obstination avec laquelle le Roi de France Louis le quatorzième et ses geôliers voulaient le maintenir dans un isolement total , la longueur peu commune de son emprisonnement ( trente quatre ans ) , enfin surtout le doute qui plane autour de son identité deux ( il était connu sous le nom d' Eustache Danger ) et de son motif d' arrestation en font l' un des plus grands mystères de l' Histoire de France .
popularisé par des écrivains comme Voltaire et Alexandre Dumas , il fut aussi un symbole sous la Révolution française , considéré comme un martyr de l' Ancien Régime trois .
selon certaines versions , Danger s' appellerait en réalité Dauger .
pour des raisons de commodité , c' est Danger qui a été repris dans cet article .
en août mille six cent soixante neuf , Bénigne de Saint Mars , gouverneur de la citadelle de Pignerol dans les Alpes , reçoit un nouveau prisonnier , Eustache Danger , avec pour ordre de le maintenir dans le plus total isolement , sans aucune communication avec l' extérieur .
le prisonnier a alors pour compagnon de prison l' ex surintendant Nicolas Fouquet , qui vient de perdre les faveurs du roi et d' être condamné à l' emprisonnement à vie .
il se lie aussi avec le valet de ce dernier , un certain La Rivière .
au bout de quelques années , Saint Mars assouplit ses conditions de détention et autorise Danger à aider la Rivière dans son travail : il devient donc serviteur de Fouquet .
mais Fouquet meurt en mille six cent quatre vingts et Danger voit son régime carcéral durci .
ensuite , Saint Mars devient gouverneur du château d' Exilles , dans les Alpes .
il n' amène avec lui que deux prisonniers qu' il gardait à Pignerol ( " les deux de la tour d' en bas " pour reprendre l' expression du ministre Louvois ) : danger et La Rivière .
en mille six cent quatre vingt sept , Saint Mars devient cette fois gouverneur de l' île de Sainte Marguerite : il n' emmène avec lui que Danger , La Rivière étant décédé de maladie .
danger a le visage dissimulé sous un masque de velours fermé par une armature de fer , ce qui évoque un heaume .
le dix neuf novembre mille sept cent trois est enterré dans la plus grande discrétion un dénommé " Marchioly " au cimetière Saint Paul .
on sait de lui qu' il était emprisonné à la Bastille .
il semblerait que cette personne soit Danger , inhumé sous un faux nom afin de ne pas attirer l' attention .
c' est lors du transfert entre la prison d' Exilles et celle de Sainte Marguerite , en mille six cent quatre vingt sept , alors que Danger était déjà détenu depuis dix huit ans , que Saint Mars fait porter un masque évoquant un heaume à son prisonnier , le fameux masque de fer quatre .
c' est cette décision étonnante , ainsi que toutes les précautions prises par le gouverneur pour isoler Danger du reste du monde , qui a fait la légende du masque de fer .
danger n' a pas le droit de révéler son identité ( on peut alors supposer que Danger est un faux nom ) sous peine d' être tué sur le champ .
le fait que l' on ne puisse connaître ni son nom , ni son visage , peut laisser croire qu' il s' agissait d' un prisonnier important , dont il fallait à tout prix éviter qu' on le reconnaisse .
c' est sur cette idée que de nombreux romanciers sont partis pour écrire leur propre version de cette énigme .
une autre hypothèse moins romanesque , proposée par l' historien Jean Christian Petitfils cinq , avance que Saint Mars , après avoir perdu son plus fameux prisonnier Nicolas Fouquet , aurait tenté de retrouver un peu d' importance en entretenant le " mystère " autour d' Eustache Danger , en en faisant un énigmatique " homme au masque de fer " .
si cette hypothèse est véridique , alors Danger n' était autre qu' un prisonnier comme les autres et ne possédait en réalité pas du tout l' importance qu' on lui a supposée six !