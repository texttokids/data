après un long séjour à Rouen , la troupe de Molière revient à Paris en octobre mille six cent cinquante huit .
au palais du Louvre , devant Louis le quatorzième , il joue sans grand succès Nicomède , tragédie de Corneille , et sauve la représentation par une farce , Le Docteur amoureux .
c' est alors que débute la période faste .
Molière et sa troupe sont installés par le roi au théâtre du Petit Bourbon et ils y jouent en alternance avec la troupe italienne de Scaramouche .
Molière va alors créer des farces comme Les Précieuses ridicules et Sganarelle ou le Cocu imaginaire .
en mille six cent soixante et un , il inaugure son nouveau théâtre installé au Palais Royal en créant Dom Garcie de Navarre ou le Prince jaloux , sa comédie héroïque en cinq actes et en vers .
c' est un échec ( seulement sept représentations ) .
il renonce alors à jouer et à écrire des tragédies ( ce qui était alors sa plus haute ambition ) .
il va désormais se consacrer à la comédie et donner ses lettres de noblesse à ce genre théâtral jusqu' alors méprisé par les grands acteurs .
il va alors écrire L'École des maris et participe en août mille six cent soixante et un , avec sa pièce Les Fâcheux , aux somptueuses fêtes du château de Vaux le Vicomte que le surintendant des finances Nicolas Fouquet donne pour éblouir Louis le quatorzième .
en mille six cent soixante deux , il épouse Armande Béjart , fille ( ou soeur ) de son ancienne maîtresse Madeleine Béjart , qui a vingt ans de moins que lui et qui , se montrant coquette , va le rendre jaloux .
en mille six cent soixante deux , le succès de sa pièce L'École des femmes va être à l' origine d' une grande polémique avec les auteurs et les comédiens rivaux et des courtisans dont il s' était moqué dans Les Précieuses ridicules .
pour répondre aux critiques , même aux attaques contre sa vie privée , Molière va écrire La Critique de l' École des femmes ( juin mille six cent soixante trois ) et L' Impromptu de Versailles ( décembre mille six cent soixante trois ) .
il est soutenu par le jeune roi , qui accepte d' être le parrain du premier fils de Molière .
en mille six cent soixante quatre , il est nommé " responsable des divertissements de la Cour " et , en mille six cent soixante cinq , sa troupe est nommée troupe du roi .
devenu un des organisateurs des spectacles de la cour royale , Molière doit fournir sans cesse la matière de nombreux spectacles .
dans ce but , en mille six cent soixante quatre , il écrit Le Mariage forcé , puis il participe aux grandes fêtes données à Versailles , alors en construction , pour Les Plaisirs de l' île enchantée : ce sera La Princesse d' Élide et la reprise de pièces plus anciennes .
en mille six cent soixante cinq , il crée L' Amour médecin .
en mille six cent soixante six , en août c' est Le Médecin malgré lui , puis il produit pour les fêtes données par le roi au château de Saint Germain en Laye , Mélicerte en décembre , Pastorale comique au début janvier mille six cent soixante sept , Le Sicilien en février mille six cent soixante sept .
en octobre mille six cent soixante neuf , pour les fêtes royales du château de Chambord , il écrit Monsieur de Pourceaugnac .
puis pour de nouvelles fêtes à Saint Germain , en février mille six cent soixante dix , il place Les Amants magnifiques dans les spectacles des divertissements royaux .
en octobre à Chambord , en collaboration avec Jean Baptiste Lully , sur une commande du roi , il donne Le Bourgeois gentilhomme .
en mille six cent soixante et onze , ce sera Psyché en collaboration avec Corneille et Quinault .
en dehors des spectacles royaux , Molière doit aussi fournir du travail à sa troupe et remplir la salle de son théâtre .
en mille six cent soixante cinq , après l' interdiction du premier Tartuffe ( en mai mille six cent soixante quatre ) ( une pièce où il attaque les faux dévots en particulier ceux de la puissante compagnie du Saint Sacrement ) , il crée Dom Juan ou le Festin de pierre , comédie elle aussi vite interdite .
en mille six cent soixante six , il écrit et joue Le Misanthrope ou l' Atrabilaire amoureux une comédie sérieuse et Le Médecin malgré lui une farce .
en mille six cent soixante sept , il tente de rejouer une nouvelle version du Tartuffe , elle est vite interdite par la justice parisienne tandis que l' archevêque de Paris excommunie les éventuels spectateurs .
en mille six cent soixante huit , la troupe de Molière joue des pièces moins exposées à l' interdiction comme Amphitryon , George Dandin ou le Mari confondu et L' Avare ou l' École du mensonge .
cependant le roi qui vient d' interdire le mouvement puritain des Jansénistes , lui permet , en février mille six cent soixante neuf , de représenter une nouvelle version du Tartuffe .
ce sera un succès avec une cinquantaine de représentations dans l' année .
en mille six cent soixante dix , il donne Les Fourberies de Scapin et La Comtesse d' Escarbagnas .
en mille six cent soixante douze , ce sera Les Femmes savantes .
Molière connait alors des années difficiles .
il souffre de la tuberculose , son fils et son amie Madeleine Béjart meurent .
les soucis matériels s' accumulent .
le roi donne à Lulli l' exclusivité de la musique et des ballets .
le dix sept février mille six cent soixante treize , Molière meurt chez lui , quelques heures après la quatrième représentation du Malade imaginaire .
sur demande de la femme de Molière , le roi oblige l' archevêque de Paris à laisser faire des funérailles nocturnes et un enterrement dans la terre consacrée d' un cimetière .
en mille huit cent seize , son cercueil est transféré au cimetière du Père Lachaise .