la Sagrada Família est une basilique de Barcelone , consacrée en deux mille neuf par le pape Benoît seize .
elle a été construite d' abord par l' architecte Jean de Villar , puis reprise par Antoni Gaudi , lequel passa plus de quarante deux ans à la conception et la construction de ce qui allait être l' oeuvre de sa vie .
elle est , de par de ses dimensions , une des plus grandes églises au monde et un des exemples les plus flagrants du modernisme catalan , incarné par Gaudi .
la Sagrada Família n' est pas encore finie ( inauguration prévue horizon deux mille vingt six ) et financée par des fonds privés et des particuliers .
le XIXe siècle fut une période particulièrement difficile pour le christianisme .
toute l' Europe connut une profonde transformation sociale et économique qui changea la société .
en Espagne , l' Église perdit de son influence et , en mille neuf cent trente six , il fut décidé de vendre les terres et les biens de l' Église .
la Révolution industrielle modifia les habitudes , provoquant l' installation de nombreux ruraux dans les périphéries de ville , qui devinrent des ouvriers .
une crise spirituelle fut enclenchée à cause de ce processus d' urbanisation , mais le clergé espagnol le combattit avec l' oeuvre missionnaire .
en mille huit cent soixante six , l' Association des dévots de Saint Joseph s' engagea avec l' Église pour tenter de préserver le catholicisme conservateur .
en mille huit cent soixante dix huit , elle comptait un demi million de membres et son créateur , Josep Maria Bocabella , eut l' idée de construire un temple expiatoire consacré à la Sainte Famille et financé par les dons des fidèles .
il s' inspira du sanctuaire de Loreto ( qui conserve les restes de la Sainte Famille ) qu' il avait visité en Italie pour mettre en oeuvre le projet .
il voulut qu' il soit construit sur le quartier de l' Ensanche .
Barcelone fut enrichie de cette " industrialisation " : la ville connut un rapide essor économique , social et culturel .
ce dernier se manifesta dans le mouvement Renaixença , qui visait à rendre à la langue catalane sa condition de langue littéraire au Moyen Âge et qui permit le développement de la littérature , de l' art et de l' architecture .
ce mouvement fut encouragé par la bourgeoisie , enrichie par l' industrialisation , et plaça la revendication de la foi catholique au coeur de la moralité capitaliste de la Renaixença .
en effet , celui ci est un nationalisme modéré et conservateur d' origine chrétienne , qui défend l' idée que l' esprit de la nation catalane est dans la famille , la propriété ( elle se rapporte de ce fait au capitalisme ) et la religion .
l' Ensanche était un nouveau quartier , à l' époque : il est issu de la destruction des murailles et divisé en plusieurs îlots disposés en quadrillage .
il couvre plus de un cinq cents hectares .
en mille huit cent quatre vingt un , Josep Maria Bocabella achète un de ces îlots pour la construction de la Sagrada Família .
le premier projet de del Villar et l' arrivée de Gaudí
Bocabella proposa le projet à Francisco de Paula de Villar , architecte du diocèse , qui proposa ses services gratuitement .
Villar persuada Bocabella de construire un temple néogothique à la place de la réplique du sanctuaire de Loreto , voulue par Bocabella .
le néogothique était en vogue à l' époque : il consistait en une réadaptation des canons gothiques ( flèches élancées , arcs boutants , importance des vitraux à l' intérieur ) .
la première pierre fut posée le dix neuf mars mille huit cent quatre vingt deux avec la présence du pape .
le projet consistait en une église à trois nefs , en forme de croix latine , avec une abside à sept chapelles et une flèche de quatre vingt cinq mètres sur la façade principale .
il y avait également des contreforts et des rosaces d' inspiration médiévale .
ceux ci renforçaient l' impression d' " église gothique " .
l' ensemble était très vertical ( à cause de la flèche et des nombreux éléments verticaux tels que les pinacles et les colonnes ) .
les travaux commencèrent par la crypte et s' interrompirent un an plus tard .
cet arrêt était dû à un différend entre del Villar et Bocabella et son conseiller architecte Joan Martorell .
del Villar voulait réaliser les piliers de la crypte avec des pierres de grande taille , cependant Bocabella et Martorell souhaitaient que ces mêmes piliers soient remplis en matériaux de maçonnerie pour économiser de l' argent .
par la suite , del Villar et Bocabella acceptèrent cette solution , de peur de n' avoir plus d' argent pour financer l' entreprise .
Bocabella décida de confier la direction des travaux à Martorell , mais celui ci refusa et recommanda vivement Gaudi , son plus talentueux élève , qui avait trente et un ans à l' époque .
il accepta la commande en mille huit cent quatre vingt trois , en cette année , il avait ses deux premiers grands projets en construction : la Casa Vicens et le Caprice .
malgré son inexpérience ( à peine cinq ans dans l' architecture !
) , il voulut relever le défi et commencer l' oeuvre qui allait être celle de sa vie ( plus de quarante trois ans de travaux ) .