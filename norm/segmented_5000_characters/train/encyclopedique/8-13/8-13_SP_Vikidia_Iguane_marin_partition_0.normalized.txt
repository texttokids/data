l' iguane marin , ou iguane marin des Galápagos ( nom scientifique : Amblyrynchus cristatus ) est un iguane , qui vit sur les côtes des îles Galápagos .
c' est le seul lézard marin au monde .
l' iguane marin a un corps allongé , avec une longue queue , et une grosse tête , couverte d' écailles dures formant une sorte de casque .
il a aussi des épines qui forment une crête tout le long de son dos et de sa queue .
en mille huit cent trente cinq , quand Charles Darwin a débarqué aux îles Galápagos , il a décrit ainsi l' iguane marin : les rochers de lave noire sur la plage sont fréquentés de grands ( soixante à quatre vingt dix centimètres ) lézards , patauds et répugnants .
ils sont aussi noirs que les roches poreuses sur lesquelles ils rampent , et cherchent leurs proies dans la mer certains les appellent " lutins des ténèbres " un
en fait , les iguanes marins ne sont noirs que quand ils sortent de l' eau .
ils vont alors sur les rochers , pour se réchauffer au soleil , et leur peau devient rouge .
quand ils se sont bien réchauffés , ils retournent à la mer , pour se nourrir .
Darwin s' est en fait trompé : ces animaux ne sont pas carnivores , et s' ils vont en mer , c' est pour y trouver des algues dont ils se nourrissent , et qu' ils broutent sous l' eau .
d' ailleurs , il sont loin d' être " patauds " , et nagent même très bien .
quand ils nagent , ils gardent la tête hors de l' eau , et ne plongent que pour se nourrir .
l' iguane marin ne se nourrit que de certaines espèces d' algues , qu' il va chercher en mer , et broute sur les rochers , sous l' eau .
à marée basse , les iguanes marins peuvent également brouter les algues qui sont sur les rochers hors de l' eau , ce qui est beaucoup moins fatigant que de nager .
son intestin contient des bactéries qui vivent en symbiose avec l' iguane , et l' aident à digérer les algues dont il se nourrit .
les iguanes se nourrissent notamment d' algues vertes , comme les ulves qui poussent près de la surface de l' eau , mais ils préfèrent cependant les algues rouges , plus nutritives , mais qui poussent un peu plus profondément .
seuls les grands mâles plongent très profondément pour se nourrir , les autres iguanes restent plus près de la surface , ou bien attendent la marée basse .
lors des plus grandes marées , le niveau de la mer est si haut que les algues rouges se retrouvent trop profondément sous l' eau pour la plupart des iguanes , et ils sont obligés de manger des ulves , qui poussent plus près de la surface deux .
lorsqu' il fait plus chaud , l' été , l' eau se réchauffe , et les algues rouges sont moins nombreuses .
les iguanes sont alors obligés de manger plus d' ulves .
certaines années , à cause de El Ninyo , l' eau se réchauffe beaucoup , et même les algues vertes disparaissent .
elles sont remplacées par des algues brunes , qui sont beaucoup moins nourrissantes pour les iguanes .
sur certaines îles , les iguanes se nourrissent également d' une plante terrestre qui pousse sur la plage , la Batis maritima .
cette plante est beaucoup moins nourrissante que les algues , mais comme on en trouve tout le temps , quelle que soit la saison , les iguanes qui en mangent sont assurés de toujours trouver de quoi se nourrir trois .
les iguanes qui vivent sur ces îles et qui consomment ces plantes supportent donc mieux que les autres les années où El Ninyo réchauffe le climat .
l' iguane marin nage parfaitement bien , il est capable également de plonger , jusqu'à vingt mètres de profondeur , et de rester environ un H sous l' eau .
afin d' économiser du dioxygène , la fréquence cardiaque de l' iguane marin ralentit beaucoup lorsqu' il plonge .
son coeur peut même s' arrêter de battre pendant trois minutes !
une autre particularité de l' iguane marin , c' est que comme il ne se nourrit que d' algues marines , qui contiennent beaucoup plus de sel que les plantes terrestres , il mange en quelque sorte " trop salé " , ce qui pourrait être dangereux pour sa santé , s' il ne trouvait pas un moyen de l' évacuer de son corps .
en effet , comme d' autres animaux marins , comme les tortues marines , par exemple , les iguanes marins ont une glande à sel , un organe spécial qui leur permet d' éliminer le sel en trop dans leur organisme .
celle de l' iguane marin se trouve dans son nez , ce qui lui permet d' évacuer le sel par les narines , tout simplement en éternuant !