tintin , le héros de la série un .
après ses premières aventures en Orient , il doit partir en Chine où il va poursuivre sa lutte contre le trafic de stupéfiants tout en devant une nouvelle fois échapper aux autorités qui le pourchassent injustement .
il est encore accompagné de son chien Milou qui , comme toujours , suit son maître dans ses aventures avec un courage et une fidélité qui forcent l' admiration deux .
Monsieur Mitsuhirato est sans conteste le grand méchant de cet album trois .
intelligent , cruel , cupide et manipulateur , il tente à plusieurs reprises d' assassiner Tintin , qu' il fait également poursuivre par ses hommes de main ou arrêter par ses relations dans les autorités japonaises et britanniques .
espion du gouvernement japonais et actif dirigeant du réseau chinois de trafic d' opium , il a d' importants alliés qui le protègent jusqu'à son arrestation , après laquelle il se fait hara kiri .
Wang Jen Ghié est l' un des principaux alliés de Tintin dans cette aventure quatre .
vieil homme à la longue barbe blanche , ce chinois n' en est pas moins courageux et très agile intellectuellement .
admiré et respecté , il est le dirigeant de la lutte anti trafic d' opium en Chine et peut compter sur la fidélité de nombreux hommes de main qui sauvent souvent la vie à Tintin .
celui ci a un profond respect pour Monsieur Wang , à un point tel qu' il est prêt à partir à la recherche du professeur Fan Se Yang dans l' espoir de faire guérir son fils du poison qui rend fou ( voir ci dessous ) .
même capturé par Mitsuhirato , il fait preuve d' un sang froid et d' un courage irréprochables .
Didi , le fils de Monsieur Wang cinq .
au départ chargé de protéger le journaliste des attaques des hommes de Mitsuhirato , il est frappé par le poison qui rend fou , et tente à partir de ce moment de tuer Tintin , Milou et sa propre famille avec cette réplique devenue culte : " lao tzeu l' a dit : il faut trouver la Voie . moi je l' ai trouvée , il faut donc que vous la trouviez aussi . pour cela , je vais d' abord vous couper la tête ! " .
Wang et son épouse sont si désespérés par son état que Tintin décide de partir à la recherche du professeur Fan Se Yang , un spécialiste de la folie , pour le guérir , mais celui ci est enlevé par des hommes de main de Mitsuhirato six .
Tchang Tchong Jen sept est un jeune chinois orphelin , sans doute préadolescent , que Tintin sauve de la noyade lors d' une crue du Yangzi Jiang , un fleuve d' Asie .
méfiant vis à vis des Européens que son grand père a combattu dans l' armée , il perd ses préjugés en rencontrant le reporter , qu' il accompagne dans sa quête .
courageux et débrouillard , il noue avec Tintin une relation très forte qui lui fait plusieurs fois risquer sa vie .
la fin de l' album le voit à la fois triste de quitter Tintin et heureux de trouver un père adoptif en la personne de Monsieur Wang .
il retrouvera le journaliste dans l' album Tintin au Tibet dont il est l' élément central , Tintin partant à sa recherche dans les montagnes tibétaines alors que tout le monde le croit mort dans un accident d' avion .
le personnage de Tchang est inspiré d' un étudiant chinois , Zhang Chongren , qui avait aidé Hergé dans ses recherches et avec qui il connaitra une intense amitié devenue mythique dans l' histoire de la bande dessinée sept , huit .
Roberto Rastapopoulos neuf , le célèbre cinéaste rencontré ( dans des circonstances assez mouvementées ) par le héros dans l' album précédent .
en voyage à Shanghaï , il est surpris d' y retrouver Tintin qui lui demande une nouvelle fois de l' aide .
à la fin de cette aventure , le journaliste apprend avec surprise et consternation que Rastapopoulos n' est autre que le chef du trafic d' opium qu' il avait poursuivi avant de le voir tomber dans une falaise .
il finira par le faire arrêter , mais l' aventurier reviendra à plusieurs reprises dans les derniers albums de la série , devenant le pire ennemi de Tintin .
gibbons dix et Dawson onze sont deux Occidentaux , le premier étant un industriel américain et le second , britannique , le chef de la concession internationale à Shanghaï , c' est à dire les autorités britanniques présentes à Shanghaï .
racistes , corrompus et violents , ils ne reculent devant aucun moyen malhonnête pour soumettre tous ceux qui leur résistent , et le jeune reporter ne peut donc que leur déplaire , comme il déplait à un bon ami de Dawson : Mitsuhirato ...
ils sont régulièrement aidés par un commandant japonais qui , s' il est moins malhonnête qu' eux , se montre tout aussi vicieux et cruel .
les Dupondt , de vieilles connaissances de Tintin douze depuis l' album précédent , où ces deux policiers pourchassaient le journaliste soupçonné d' avoir pris part au trafic d' opium .
bien qu' ils aient de la sympathie pour lui depuis la preuve de son innocence , ils sont chargés de le poursuivre de nouveau par les autorités britanniques .
une fois de plus , même s' ils ne parviendront pas à leur but , ils lui donneront sérieusement du fil à retordre .