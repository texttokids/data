le loup est représenté par un assez grand nombre de sous espèces selon les régions .
ce sont notamment En Eurasie le loup d' Arabie , le loup des steppes , le loup de Mongolie , le loup de Sibérie , le loup des Abruzzes , le loup ibérique , et en Amérique du Nord le loup arctique ( Canis lupus arctos ) , le loup d' Alberta , le loup des plaines et le loup du Mexique .
le chien ( Canis lupus familiaris ) n' est pas vraiment une espèce différente du loup : ce sont en fait les différentes races d' une sous espèce de loup qui ont été domestiquées il y a environ cinquante ans .
les loups et les chiens font toujours partie de la même espèce et peuvent se reproduire entre eux .
le chien peut donc être considéré aussi comme une sous espèce du loup .
c' est aussi le cas du dingo ( Canis lupus dingo ) , un animal sauvage dont les ancêtres sont des chiens , eux même issus du loup !
le loup est un mammifère qui fait partie de la famille des Canidés , et , plus précisément , du genre Lupus .
d' autres espèces appartenant également à ce genre sont assez proches du loup :
certains animaux appelés " loups " ne font pas partie de la même espèce que le loup commun ( ou loup gris ) , bien qu' ils en soient très proches : c' est le cas par exemple du loup roux ( ou loup rouge ) , dont le nom scientifique est Canis rufus .
le coyote ( nom scientifique : Canis latrans ) est une espèce très proche du loup .
certains scientifiques pensent d' ailleurs que le loup roux serait en fait un hybride de loup et de coyote .
dans ce cas , le loup roux ne serait pas une espèce proprement dite , et son nom scientifique deviendrait Canis lupus X latrans .
certaines populations de loup , considérées comme des sous espèces , tels le loup des Indes ou le loup de l' Est , pourraient en fait être des espèces distinctes , très proches du loup .
le loup terrible ( nom scientifique Canis dirus , Dire Wolf en anglais ) est une espèce de canidé préhistorique , très proche du loup , qui a disparu .
il était beaucoup plus grand que le loup actuel .
le loup est présent dans les mythes d' un grand nombre de cultures et a toujours eu une place importante dans les légendes des hommes et dans la littérature .
dans la mythologie scandinave , Fenrir est un loup géant , fils du dieu Loki , qui doit tuer tous les dieux lors du Ragnarök ( la fin du monde ) .
Hati et Sköll sont les Managarn , des loups géants descendants de Fenrir .
ils poursuivent sans relâche la lune et le soleil pour les dévorer .
on leur attribuait les éclipses .
Geri et Freki , eux , sont les deux loups domestiques du dieu Odin , le roi des dieux .
dans la Rome antique , Rome a été fondée par Rémus et Romulus , deux jumeaux nouveaux nés qui auraient été élevés et nourris par une louve , attaquant normalement les élevages .
en Grèce antique , les lois de Solon instituent des primes pour tous ceux qui abattent les loups dangereux , poursuivant le mythe de la bête féroce .
dans l' Antiquité , voir un loup avant une bataille était un présage de victoire , le loup étant l' animal symbolique de la guerre et de la chasse .
chez les indiens d' Amérique ( Amérindiens ) , le loup est mieux vu : il incarne l' esprit , la réincarnation , la liberté .
le loup garou est une créature légendaire : il s' agit d' un être humain qui se transforme en loup à la pleine lune .
dans certaines versions , il s' agit d' un loup monstrueux , ou d' une créature mi loup , mi homme .
la figure du loup est présente dans la littérature à travers Le Roman de Renart .
le baron Ysengrin , oncle de Renart , est souvent la victime du renard malicieux .
il est décrit comme un seigneur brutal ( ou comme une bête féroce , selon le cas ) , et assez bête .
le loup apparait comme un prédateur sanguinaire et féroce à travers : des légendes : les loups garous , la Bête du Gévaudan ( monstre loup mangeant les hommes ) , des contes européens comme ceux de Charles Perrault ou des Frères Grimm .
c' est , par exemple , le grand méchant loup qui attaque le Petit Chaperon Rouge et sa mère grand , et est finalement tué par le chasseur , ou le loup des Trois Petits Cochons , conte du XVIIIe siècle , qui souffle sur les maisons pour essayer de les tuer .
les fables de Jean de La Fontaine comme Le Loup et l' agneau , ou Le Loup et le chien .
attention , dans ces fables , le loup peut aussi être trompé ( Le loup et la Cigogne ) , ou être pauvre , faible mais libre ( Le loup et le chien ) .
beaucoup plus récemment , dans Le Livre de la Jungle , de Rudyard Kipling , l' enfant Mowgli est recueilli et élevé par une famille de loups .
l' Oeil du loup est un roman pour la jeunesse de Daniel Pennac
d' autres livres dont le personnage principal et un loup :