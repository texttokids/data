saturne est une planète gazeuses et aussi Saturne est la sixième planète tournant autour du Soleil .
d' un diamètre neuf fois supérieur à celui de la Terre , Saturne est très connue pour ses anneaux spectaculaires composés de morceaux de glace .
c' est la plus lointaine planète visible à l' oeil nu , et la dernière connue jusqu'à ce qu' Uranus soit découverte au XIXe siècle .
en anglais , Saturne a donné son nom à Saturday , ce qui signifie samedi , le sixième jour de la semaine .
il s' agit d' une planète gazeuse , Saturne n' a par conséquent pas de surface solide : un astronaute ne pourrait donc pas y marcher .
le nom de Saturne vient du dieu romain du même nom .
pour les Grecs , Saturne s' appelle Cronos .
en raison de sa vitesse de rotation rapide ( dix heures ) , Saturne est légèrement aplatie au niveau des pôles .
son rayon équatorial est de soixante deux cent soixante huit kilomètres , alors que son rayon polaire est de cinquante quatre trois cent cinquante neuf kilomètres , soit près de six kilomètres de moins .
son volume est très important , sept cent soixante fois celui de la Terre mais sa masse seulement quatre vingt quinze fois .
la densité de Saturne est donc très faible , environ zéro , sept ( près de huit fois moins que celle de la Terre ) , la seule planète dont la densité soit inférieure à celle de l' eau .
son atmosphère est composée principalement d' hydrogène et d' hélium .
elle se divise en deux couches , la troposphère et la stratosphère : la stratosphère ressemble à celle de la Terre , sauf bien sûr qu' elle n' a pas la même composition .
la troposphère elle même contient trois couches :
la première couche est celle visible depuis l' espace .
ses nuages sont composés d' ammoniac , la température est de deux cent cinquante degrees Celsius et la pression atmosphérique se situe entre zéro , cinq et deux bars .
la seconde couche contient des nuages d' hydrosulfure d' ammonium , la température est de soixante dix degrees Celsius et la pression devient assez importante , de deux , cinq à neuf , cinq bars .
dans la troisième , la température peut atteindre zéro degree Celsius voire soixante dix degrees Celsius , et la pression est de dix à vingt bars .
l' atmosphère de Saturne est composée de quatre vingt treize pour cents d' hydrogène , cinq pour cents d' hélium , zéro , deux pour cents de méthane , zéro , un pour cent de vapeur d' eau , zéro , un pour cent d' ammoniac , zéro , cinq pour cents d' éthane et zéro , un pour cent d' hydrure de phosphore .
un
vue de loin , Saturne parait calme , sans signe particulier , à part ses anneaux .
en réalité , le climat de cette planète est très violent : il s' y produit beaucoup de phénomènes météorologiques .
tous les trente ans , une tempête géante appelée " grande tache blanche " apparait et fait le tour de l' équateur , avec des vents pouvant dépasser les un cinq cents kilomètres par heure .
cette tempête est précédemment apparue en mille neuf cent quatre vingt dix , la prochaine devrait se produire vers deux mille vingt .
il arrive par moment que des tempêtes formant une tache blanche apparaissent , mais sans faire le tour de la planète .
c' est ce qui est arrivé en deux mille onze , avec une tempête qui a atteint dix sept kilomètre de long .
cette tempête , tout comme celle qui se produit tout les trente ans , ne ressemble pas du tout aux cyclones sur Terre : en effet , ils ne présentent aucun oeil .
au pôle Nord de Saturne , il existe un étrange phénomène .
il pourrait s' agir d' une tempête en forme d' hexagone .
cette tempête hexagonale a été observée la première fois par les sondes Voyager un et Voyager deux .
elle ressemble beaucoup plus à une tempête terrestre , puisqu' elle possède un oeil comme les cyclones terrestres .
la nature exacte de cette tempête n' est pas encore connue .
il pourrait aussi s' agir d' une aurore polaire de type inconnu .
cette tempête est énorme , son périmètre étant de treize kilomètre .
au pôle Sud , il existe également une tempête , mais elle n' est pas hexagonale comme celle du pôle Nord .
elle ressemble donc beaucoup plus à une tempête terrestre .
sur Saturne , on peut aussi observer la formation d' orages , mais encore une fois , ce ne sont pas des orages comme sur la Terre .
ils sont extrêmement violents , le plus long a même duré huit mois .
c' est l' orage le plus long jamais observé .
ces orages peuvent s' étendre sur trois kilomètre .
ils se produisent généralement dans une région de Saturne nommée " allée des tempêtes " .
les éclairs de Saturne sont également des milliers de fois plus fort que sur notre planète .
vus de loin , ses anneaux donnent à Saturne ce profil particulier où ils semblent formés d' une matière continue .
en réalité , ils sont constitués de blocs de glace et de roches d' une taille variant entre celle d' un grain de sable et celle d' une montagne .
ces blocs se déplacent très rapidement autour de la planète , et une sonde spatiale qui traverserait les anneaux serait probablement détruite .
en vingt cent quatre , la sonde Cassini est passée à travers les anneaux , elle s' en est bien sortie , mais elle n' était pas totalement intacte .
les anneaux on des noms de lettre : de l' Anneau À jusqu'à l' anneau F
les anneaux de Saturne s' étendent sur plus de cinq cents kilomètre de large , mais sur moins d' un kilomètre d' épaisseur .