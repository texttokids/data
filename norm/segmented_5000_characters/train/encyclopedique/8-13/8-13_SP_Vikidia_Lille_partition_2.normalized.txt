l' autorité des États étrangers , une source de développement et de prospérité pour la ville
déjà , dès le Moyen Âge , Lille appartenait au comté de Flandre , ce qui a lui à permis de développer son économie , grâce au commerce et à la fabrication de draps de laine .
d' autres villes de la région connaissent aussi un développement à cette époque , comme Boulogne sur Mer , Arras et Cambrai .
d' autres villes du comté comme Valenciennes , Saint Omer , Gand , Bruges , Anvers et Douai connaissent un développement mais plus tardif .
on garde très peu de vestiges de cette période : les maisons étaient construites en bois et en torchis , des matériaux qui résistent mal avec le temps , sans compter les conflits qui ont détruit parfois entièrement certains quartiers .
il reste néanmoins quelques caves voutées qui datent du XIIIe siècle dans le Vieux Lille , ainsi que des ponts romans , qui ont été depuis recouverts par la chaussée .
à l' issue de la bataille de Mons en Pévèle en treize cent quatre , le roi Philippe le quatrième de France s' empare de Lille et jusqu' en treize cent soixante neuf , Lille sera sous le contrôle de la France .
française , elle deviendra bourguignonne ( la Bourgogne étant alors indépendante du royaume de France ) et restera sous l' autorité de la maison de Bourgogne jusqu' au XVe siècle .
c' est une période de prospérité pour la ville , qui prend de l' importance au niveau politique .
elle devient , en effet , avec Dijon et Bruxelles , une des capitales du duché de Bourgogne .
les vestiges qui restent visibles de cette domination bourguignonne sont ceux du Palais Rihour .
en quatorze cent soixante dix sept , le duc de Bourgogne , Charles le Téméraire meurt .
il n' avait qu' une fille unique , Marie de Bourgogne , qui devient alors héritière du duché .
mais la même année , Marie de Bourgogne épouse Maximilien d' Autrichie qui appartient à la Maison de Habsbourg .
le comté de Bourgogne revient dès lors aux Habsbourg et Maximilien d' Autrichien devient comte des Flandres et deviendra , en quinze cent huit , Empereur du Saint Empire romain germanique et ce , jusqu' en quinze cent dix neuf .
en quinze cent quarante neuf , Lille est rattachée aux Pays Bas après que Charles Quint promulgue la Pragmatique Sanction qui est un texte qui unifie toutes les provinces ( dont le comté de Flandres dont Lille fait partie ) des Pays Bas .
le XVIe siècle à Lille est marqué par la peste , l' essor de l' industrie textile , mais surtout par les guerres de religion qui la toucheront profondément .
Charles Quint , roi des Espagnes , succède en quinze cent dix neuf au titre d' empereur du Saint Empire germanique .
Lille sera donc sous l' autorité espagnole jusqu' en seize cent soixante sept .
de nombreux monuments d' inspiration flamande sont construits à cette époque : la Vieille Bourse , située sur la Grand' Place , en est l' exemple le plus marquant mais il y a aussi la maison de Gilles de la Boë , construite en seize cent trente six .
les tentatives françaises pour gagner la confiance de Lille et des Lillois
Lille est redevenue riche et prospère en seize cent soixante sept .
la ville est alors espagnole et le jeune roi Louis le quatorzième s' inquiète de voir la France encerclée de possessions espagnoles .
en effet , au nord de la France , la Flandre fait partie des Pays Bas espagnols et à l' est , la Franche Comté est également espagnole .
entre le vingt et le vingt sept juillet seize cent soixante sept , Lille est assiégée et prise par Vauban sous les yeux de Louis le quatorzième .
officiellement , Lille devient française en seize cent soixante huit par le traité d' Aix la Chapelle .
les Lillois accueillent très mal cette nouvelle car leur ville connaissait une prospérité sous l' autorité espagnole .
des travaux sont alors entrepris pour redonner confiance aux Lillois : la ville est fortifiée et , notamment , Vauban fera construire la citadelle qui porte aujourd'hui son nom .
en face de la citadelle , la rue Royale ( faisant ainsi référence à la prise du roi Louis le quatorzième ) accueillera une série de bâtiments influencés par le style français .
au XVIIIe siècle , Lille ne connaitra pas véritablement la Révolution française .
néanmoins , les autrichiens profitent que la France soit en révolution pour assiéger Lille .
les Lillois les repoussent et pour célébrer cette résistance , la Convention nationale ( qui était l' Assemblée qui a instauré la Première République ) décrète que " Lille a bien mérité de la patrie " .
la Colonne de la Déesse , érigée sur la Grand' Place illustre cette reconnaissance .
pour la première fois , les Lillois se sentent vraiment français et Lille est , à son tour , reconnue française .
au cours de la Seconde Guerre mondiale , les allemands joueront de ce passé mouvement de Lille , en rattachant Lille à la Belgique .
le gouvernement allemand considérait que la Flandre et l' Artois ( c' est à dire la grande partie du Nord Pas de Calais ) faisaient partie de " l' Europe germanique " .
les mouvements de collaboration insistent , eux aussi sur la proximité culturelle entre flamands et allemands pour justifier le rattachement de Lille à la Belgique .
mais avec le mouvement de résistance et l' affaiblissement de l' Allemagne , ce projet ne verra pas le jour puisque , le quatre septembre dix neuf cent quarante quatre, Lille sera définitivement libérée .