pour s' exprimer , en général , il aboie c' est à dire qu' il émet un son bref et souvent par saccades .
ses aboiements peuvent varier selon la taille ou l' âge : il glapit ( petits chiens ) , jappe ( jeunes chiens ) , et caetera Il gronde en signe de menace , avec les babines fréquemment retroussées .
et parfois , il hurle ( peut être de douleur , de colère ou de plainte ) .
les chiens communiquent entre eux avec leur queue , leurs aboiements , leurs grognements , et leurs sens .
les chiens ont des sentiments pour l' homme .
exemple : quand ils aboient , ils sont en colère , quand ils baissent les oreilles , c' est qu' ils ont peur , et quand ils remuent la queue , c' est qu' ils sont joyeux ...
le chien aime la présence de l' homme et l' assister : on parle d' ailleurs souvent de lui comme du meilleur ami de l' homme ou de son ami le plus fidèle .
il aide les chasseurs , les aveugle , les victimes d' un accident ( par exemple avec les secouristes de montagne ) , garde les maisons et les troupeaux , est élevé juste pour le plaisir , et caetera , d' où de nombreuses expressions courantes : chiens de chasse ( d' arrê .
note un ...
) , chiens guides , chiens sauveteurs , chiens policiers , chiens de berger , chiens de garde , chiens de compagnieNote deux , et caetera
son caractère et son comportement dépendent en grande partie de la vie qu' il mène , généralement avec son maître ou sa famille d' accueil ( affection , éducation , activités ...

par exemple , un chien dressé à attaquer ou sous alimenté risque davantage de se manifester comme un chien méchant .
on utilise l' image du chien parce qu' il partait de l' idée que le chien est une sale bête , un animal méchant et méprisant , et il n' est pas vraiment bien considéré selon les pays et les origines et les époques .
de nombreuses expressions sont en rapport avec le chien et tantôt elles ont une connotation positive et tantôt une connotation négative .
en s' installant en Australie , les hommes ont introduit de nouveaux animaux sur cette île .
le premier fut le dingo , sorte de chien sauvage .
on pense que le dingo a été introduit en Australie par des hommes venus du nord , il y a cinq ans .
les ancêtres des dingos qui ont été introduits étaient sans doute domestiqués et leur descendants sont redevenus sauvages .
et en Afrique subsaharienne , il y a le lycaon , un chien sauvage d' Afrique aussi appelé chien hyène .
en France , tout les chiens de plus de quatre mois doivent être identifiés et enregistrés dans un fichier national .
lorsque le propriétaire d' un chien déménage , il est obligé de le signaler .
par ailleurs le décès du chien doit aussi être signalé .
les chiens qui sortent de la France métropolitaine lors d' un voyage , doivent être obligatoirement vaccinés contre la rage trois .
les chiens de race doivent porter un nom qui commence par la lettre associée à l' année où il sont nés .
par exemple , la lettre de l' année vingt cent quatorze en France était J Tous les chiens de race nés en France en vingt cent quatorze ont donc un prénom qui commence par J quatre .
les chiens ne font pas des chats : pour dire que quelqu' un a le même comportement ou caractère que ses parents .
se regarder en chien de faïence : se regarder de travers , se bouder .
il fait un temps de chien : un très mauvais temps , pluvieux et maussade .
avoir une humeur de chien : être de mauvaise humeur .
l' origine de cette expression vient de la signification de de chien comme qualificatif désignant excès , comme dans une humeur de chien , un mal de chien , ou une vie de chien ( expressions familières et imagées ) .