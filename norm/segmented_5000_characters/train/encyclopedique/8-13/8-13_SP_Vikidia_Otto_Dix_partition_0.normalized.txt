Otto Dix , né le deux décembre mille huit cent quatre vingt onze à Untermhaus , près de Gera , en Thuringe et mort le vingt cinq juillet mille neuf cent soixante neuf( à soixante dix huit ans ) à Singen , près du lac de Constance , est un peintre allemand appartenant aux mouvements de l' expressionnisme et de la Nouvelle Objectivité .
sa vie a été profondément marquée par ce qu' il a vécu durant la Première Guerre mondiale : il en revient pour peindre les conséquences de la guerre et critiquer la société qui l' a provoquée .
dix était issu d' une famille ouvrière ( son père travaillait dans une mine de fer ) et rien ne le prédestinait à devenir peintre , si ce n' est l' intérêt de sa mère pour les beaux arts et la musique .
de mille neuf cent cinq à mille neuf cent neuf , il prend des cours de dessin et de peinture à Gera , mais ses professeurs ne voient pas en lui un avenir artistique .
de mille neuf cent neuf à mille neuf cent quatorze , grâce à une bourse , il étudie à l' École des arts appliqués de Dresde .
il s' essaie au futurisme , au cubisme et au dadaïsme .
quand éclate la Première Guerre mondiale , il se porte volontaire comme artilleur et effectue plusieurs missions ( notamment dans la Somme et en Champagne ) : il a alors vingt quatre ans .
il en ressort profondément bouleversé et choqué par les horreurs qu' il a vues : la guerre n' en finit pas de le poursuivre dans son inspiration artistique .
il adhère au mouvement satirique et réaliste de la Nouvelle Objectivité deux noeuds qui , au cours des années mille neuf cent vingt , critique violemment la société allemande .
en mille neuf cent vingt sept , Otto Dix devient professeur et enseigne l' art à l' université de Dresde .
mais lorsque les nazis prennent le pouvoir en Allemagne , en mille neuf cent trente trois , l' art et les artistes , surtout l' art moderne , sont persécutés .
considéré comme un " bolchévique " à cause de son style , il est renvoyé de son poste d' enseignant et fuit vers le Sud de l' Allemagne vers le lac de Constance , où il peint de nombreux paysages .
il fait partie de ce que les nazis appellent les " artistes dégénérés " : en mille neuf cent trente sept , de nombreux tableaux de lui sont brulés , tandis que d' autres sont mis de côté pour une exposition où les nazis montrent des " peintres dégénérés " ( souvent de la Nouvelle Objectivité , comme Grosz ) , mais aussi des tableaux d' artistes étrangers , comme Matisse ou Picasso , ainsi que des peintures de malades mentaux ( on veut ainsi prouver que les " peintres dégénérés " sont eux aussi des malades dont il faut se débarrasser ) .
de nombreux artistes quittent l' Allemagne , mais Otto Dix choisit de rester et de modifier son style .
en mille neuf cent trente huit , il est arrêté par la Gestapo pour complot contre Hitler , mais est ensuite libéré .
de mille neuf cent quarante quatre à mille neuf cent quarante cinq , il sert sur le front occidental .
il est envoyé en Alsace : fait prisonnier par les français , il ne pourra revenir en Allemagne qu' en mille neuf cent quarante six .
après guerre , il ne s' intéresse pas aux nouveaux mouvements qui apparaissent en Allemagne , mais reçoit de nombreuses récompenses des deux côtés du mur .
il meurt à Constance en mille neuf cent soixante neuf , des suites d' un infarctus .
Otto Dix a peint des centaines de tableaux , mais ses oeuvres sont encore soumises au droit d' auteur : on ne peut donc pas les reproduire sur Vikidia .
soleil levant un , réalisé alors qu' il n' était encore qu' un peintre amateur ( lien )
autoportrait avec un casque d' artillerie ( Selbstbildnis mit Artillerie Helm , mille neuf cent quatorze ) : Otto Dix s' est représenté lui même sur ce tableau , alors qu' il s' était engagé volontairement pour partir à la guerre en tant que soldat ( lien )
les Joueurs de Skat ( Die Skatspieler , mille neuf cent vingt ) et les " gueules cassées "
ce tableau représente trois vétérans allemands , mutilés après la guerre , en train de jouer au skat , un jeu de cartes très populaire en Allemagne .
il est exposé à la Neue Nationalgalerie , à Berlin .
Otto Dix a peint ce tableau après avoir vu trois vétérans allemands jouer aux cartes dans un café , il a donc décidé de reproduire la scène et l' a exagérée .
les soldats sont amputés , ils leur manquent des bras , des jambes , ou d' autres parties de leur corps , qui ont été remplacées par des prothèses .
peint seulement deux ans après la fin de la guerre , ce tableau en décrit les conséquences horribles .
il fait partie d' une série de quatre tableaux , peints cette année là , et représentant les " gueules cassées " , c' est à dire les soldats estropiés de la Première Guerre mondiale :
la Rue de Prague ( Prager Strasse lien ) , qui représente la Rue de Prague , à Dresde , après la première guerre mondiale .
on y voit deux personnes , l' une estropiée , et portant plusieurs prothèses , et l' autre cul de jatte ( c' est à dire qu' il lui manque les deux jambes ) , sur une planche à roulettes .
le Vendeur d' allumettes ( lien ) , qui montre un homme assis sur un trottoir , aveugle , amputé des deux bras et des deux jambes , en train de vendre des allumettes dans l' indifférence des passants , tandis qu' un chien lui fait pipi dessus .
portrait de la journaliste Sylvia von Harden , ou Portrait de femme ( Bildnis der Journalistin Sylvia von Harden , dix neuf cent vingt six )