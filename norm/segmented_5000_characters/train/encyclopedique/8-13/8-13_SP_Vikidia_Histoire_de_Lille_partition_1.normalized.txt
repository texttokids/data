au Moyen Âge , Lille n' appartenait pas à la France mais au comté de Flandre , qui connaissait alors un développement de son économie , grâce au commerce et à la fabrication de draps de laine .
certaines villes de la région comme Boulogne , Arras et Cambrai connaissent aussi un développement à cette époque .
d' autres villes du comté comme Valenciennes , Saint Omer , Gand , Bruges , Anvers et Douai se développeront plus tard .
la ville de Lille était organisée autour de la place du marché ( qui est l' actuelle place du Général de Gaulle , encore appelée Grand' Place ) et du Vieux Lille , qui était le castrum , un noyau urbain fortifié entouré de cours d' eau qui le protégeait des ennemis deux .
lors de la première mention de la ville dans les archives , en dix cent soixante six , Lille était déjà une petite ville fortifiée , avec son château fort .
un village voisin à l' est , du nom de Fins , possède lui aussi une église et sera rattaché à Lille au XIIe siècle .
Lille étant traversée par la rivière de la Deûle , ses rives sont très fertiles , on peut donc y faire de l' agriculture .
ainsi , on fit pousser du blé à Lille en très grande quantité .
la ville put ainsi se développer , grâce notamment à ses relations avec les autres villes du comté , elles aussi déjà bien développées .
à partir du XIIe siècle , Lille est connue pour sa foire aux draps .
Lille reste cependant , de nos jours , la ville la plus assiégée de France .
elle connait ses premiers sièges en onze cent vingt sept et mille millecent vingt huit .
en effet , en onze cent vingt sept , Charles Ier , le comte de Flandre , est tué et un conflit éclate entre son neveu Guillaume Cliton et Thierry d' Alsace pour la succession , chacun voulant devenir comte .
le roi de France Louis le sixième profite de ce climat de tension pour s' emparer de Lille , mais doit se retirer devant la menace anglaise .
en douze cent treize , la ville est assiégée par le roi Philippe Auguste , avant d' être reprise par Ferdinand de Portugal , comte de Flandre en septembre de la même année .
le comte , qui contrôlait alors de nouveau Lille , s' unit avec le comte de Boulogne , le comté de Hainaut , l' Angleterre et le Saint Empire romain germanique contre la France , dirigée par le roi Philippe Auguste .
la guerre s' achève en douze cent quatorze par la bataille de Bouvines .
le comte Ferrand de Flandre est alors emprisonné .
il revient à son épouse Jeanne de Constantinople de diriger le comté de Flandre et de Hainaut et donc , de gouverner Lille , qui comptait désormais dix habitants .
appréciée par les Lillois , c' est à son époque que furent créées à travers la région de nombreuses fondations charitables .
elle fonde l' hôpital Saint Sauveur et en douze cent trente sept , est fondée l' hospice Comtesse deux .
mais auparavant , en douze cent vingt cinq , Bertrand Cordel , un ermite artiste jongleur vivant dans la forêt entre Valenciennes et Tournai se fait passer pour Baudouin le sixième de Hainaut , le père de la comtesse , qui fut emprisonné , mais dont la date de la mort est incertaine ( on dit généralement qu' il est mort en douze cent six ) .
en effet , c' est après qu' un baron l' eut pris pour l' ancien comte de Flandre qu' il finit par jouer le jeu .
d' autres personnalités reconnurent en lui le comte Baudouin et il demanda que les pouvoirs qui étaient accordés à la comtesse Jeanne lui soient restitués .
acclamé à Valenciennes , Tournai , Bruges , Gand et à Lille , il régna à la place de la comtesse en avril mai mille deux cent vingt cinq .
celle ci put se réfugier à Mons , où elle avait encore des fidèles , et appela au jugement du roi Louis le huitième , en échange de vingt mille livres ( la monnaie de l' époque ) et la mise en caution des villes de Douai et de Lécluse cinq .
le roi démasque l' imposteur en lui posant des questions sur sa vie auxquelles il ne sait pas répondre : malgré une tentative de fuite , il est rattrapé , condamné à mort et pendu à Lille .
l' époux de la comtesse , Ferrand de Portugal , est libéré en douze cent vingt six par le roi de France , mais meurt en douze cent trente trois .
la comtesse rédige une charte par laquelle les mayeurs et les échevins ( c' est à dire respectivement l' équivalent du maire et de son adjoint ) sont choisis par quatre commissaires , désignés par le souverain .
la comtesse meurt sans descendance en douze cent quarante quatre et c' est à sa soeur , Marguerite , que revient le comté de Flandre et de Hainaut , puis au fils de cette dernière , Gui de Dampierre .
Lille a connu , entre mille deux cent quatre vingt dix sept et mille trois cent quatre , trois nouveaux sièges : le premier par le roi de France Philippe le quatrième , le deuxième par Jean Ier de Namur après la bataille des éperons d' or et le troisième par le roi Philippe le quatrième de France , à nouveau , après la bataille de Mons en Pévèle , en treize cent quatre .
après cette bataille et jusqu' en treize cent soixante neuf , Lille sera sous le contrôle de la France .
en effet , en treize cent soixante neuf , Lille est rattaché au comté de Bourgogne suite au mariage de Marguerite le troisième de Flandre ( la fille du comte de Flandre de l' époque , Louis de Male ) avec Philippe le deuxième de Bourgogne .
à ce moment , Lille appartint à la maison de Bourgogne jusqu' au XVe siècle .
c' est une période de prospérité pour la ville , qui prend de l' importance au niveau politique .
elle devient , en effet , avec Dijon et Bruxelles , une des capitales du duché de Bourgogne .
en quatorze cent quarante cinq , Lille compte vingt cinq habitants .
à cette époque , le duché est dirigé par Philippe le troisième de Bourgogne , un duc très puissant ( plus que le roi de France ) , il fait de Lille une capitale administrative et financière et pour loger sa cour , il fait construire en quatorze cent cinquante trois le Palais Rihour , qui abrite aujourd'hui l' office de tourisme deux .
Charles le Téméraire , le dernier duc bourguignon , meurt en quatorze cent soixante dix sept .
son héritière , qui était sa fille unique , Marie de Bourgogne , se marie avec Maximilien Ier .
Lille passe alors à la maison de Habsbourg , rejoint le Saint Empire romain germanique et partage donc l' histoire des Pays Bas espagnols , de Charles Quint à Philippe le quatrième d' Espagne , c' est à dire de quinze cent zéro à seize cent soixante sept .