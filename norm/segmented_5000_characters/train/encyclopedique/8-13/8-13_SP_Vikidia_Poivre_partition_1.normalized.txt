durant l' Antiquité et le Moyen Âge , les épices , et tout particulièrement le poivre , étaient des marchandises très rares et chères , importées d' Asie .
à cette époque , on ne disposait pas des moyens modernes pour conserver les aliments , surtout la viande .
les épices servaient non seulement à donner du gout à un plat , mais aussi à conserver les aliments , ou à masquer le gout d' une viande pas très fraiche .
au Moyen Âge , le poivre était si cher que l' on avait l' habitude de dire " cher comme poivre " , pour parler de quelque chose de très cher .
c' est également de cette époque que date l' expression payer en épices , qui s' est quelque peu modifiée pour devenir " payer en espèces " .
le poivre est , avec la cannelle , une des épices qui sont connues depuis le plus longtemps : le philosophe grec Théophraste en parle , au IVe siècle avant Jésus Christ , sous le nom de peperi .
selon une légende , Cléopâtre aurait séduit Jules César au moyen de plats riches en poivre , réputé pour ses vertus aphrodisiaques .
un
le poivre long a été une des premières épices importées en Europe .
son nom indien , pippali , est d' ailleurs à l' origine du mot peperi .
il sera peu à peu remplacé par les autres variétés de poivres , notamment le poivre noir , et oublié pendant longtemps : en effet , contrairement au poivre en grains , le poivre long n' est pas très facile à moudre ...
ce sont les marchands arabes qui vont , durant longtemps , contrôler la route des épices , par laquelle l' Europe et le Moyen Orient sont approvisionnés en poivre et autres épices , jusqu' au XIIème siècle , au cours des croisades .
au XVe siècle , les Portugais tentent d' ouvrir une nouvelle route pour l' approvisionnement des épices , par voie maritime , cette fois .
c' est l' époque des grandes découvertes maritimes , et des grands explorateurs , comme Vasco de Gama , qui atteindra l' Inde en quatorze cent quatre vingt dix huit , en passant par le cap de Bonne Espérance ( au sud de l' Afrique ) , et ouvrira une autre voie d' approvisionnement des épices .
en quatorze cent quatre vingt douze , Christophe Colomb cherche également à rejoindre l' Inde et ses épices , en empruntant une autre route , faisant le tour du monde .
il découvrira ainsi accidentellement les Antilles , et de nouvelles épices .
il ramène notamment une nouvelle épice au gout piquant , qu' il appelle poivre de Jamaïque ( bien qu' il ne s' agisse pas réellement de poivre ) , parce que le poivre est une épice très chère , à cette époque .
au XVIIe siècle , Colbert , ministre de Louis le quatorzième , crée la Compagnie des Indes Orientales , dans le but de permettre le commerce avec l' Asie , et notamment l' approvisionnement en épices .
au XVIIIe siècle , le botaniste Pierre Poivre , au nom prédestiné , parviendra à ramener et à cultiver des plants de poivrier , de giroflier et de muscadier sur l' île Maurice .
le gout du poivre est dû principalement à une substance , la pipérine , qui est contenue dans les vrais poivres , et quelques autres épices .
elle procure une sensation de brulure , de chaleur , et de piquant sur la langue .
l' échelle de Scoville , qui est utilisée pour mesurer la force des piments , peut également être utilisée pour le poivre .
le poivre est réputé pour ses vertus cicatrisantes , c' est pourquoi , autrefois , on en versait sur les plaies .
mais cette propriété n' a jamais été vraiment correctement expliquée .
en revanche , on sait que le poivre , en quantité raisonnable , est bon pour la santé : il permet notamment d' éviter certains cancers .
depuis l' Antiquité , le poivre est également réputé pour ses vertus aphrodisiaques , c' est à dire qu' il est sensé provoquer le désir amoureux .
on dit d' ailleurs que la Reine Cléopâtre utilisait beaucoup le poivre dans ses plats pour séduire ses amants , et notamment Jules César .
le poivre était aussi réputé pour lutter contre les maladies vénériennes .
le poivre est cultivé dans les pays tropicaux .
les principaux producteurs sont les pays point asie du Sud Est , d' Afrique et d' Amérique du Sud deux .
en vingt cent huit , les trois plus gros producteurs de poivre étaient le Vietnam , l' Indonésie et le Brésil , qui en ont produit plus de soixante milliards de tonnes .
ils sont suivis par l' Inde , la Chine et la Malaisie .
le poivrier étant une liane , on le fait pousser sur de grands tuteurs , dans des plantations .
poivre Blanc est une marque de vêtements , créée en dix neuf cent quatre vingt quatre à Marseille .
la salamandre géante du Japon produit , lorsqu' elle est menacée , une substance qui a la même odeur que le poivre , ce qui lui a valu son nom japonais de sansh uo , c' est à dire " poisson poivré géant " .
la menthe poivrée est une variété de menthe , très riche en menthol , ce qui lui procure un gout très fort .
son nom commun et son nom latin ( Mentha le dixième piperita ) sont inspirés de celui du poivre .