en raison de sa taille , et de son mode de vie , l' anguille électrique est un poisson difficile à élever en aquarium .
il lui faut un très grand aquarium ( au moins plusieurs centaines de litres ) , qui soit réservé aux anguilles électriques , puisqu' elles mangent les autres poissons .
en plus , les adultes s' entendent assez bien entre eux , mais les jeunes , eux , ont très mauvais caractère , et peuvent se battre entre eux , si bien qu' il faut les garder tous seuls dans l' aquarium .
l' anguille électrique est un poisson plutôt nocturne , qui a besoin de beaucoup de cachettes ( des pierres , des racines ...
) dans l' aquarium pour pouvoir se cacher et être à l' aise .
c' est en dix neuf mille millecent trente quatre que les premières anguilles électriques ont été importées en Europe pour l' aquarium .
l' anguille électrique reste cependant très rare dans le commerce , non seulement en raison de la difficulté de l' élever , mais aussi en raison du danger potentiel .
en effet , même si l' anguille électrique ne figure pas sur la liste des espèces officiellement considérées comme dangereuses en France cinq , elle reste un animal qui peut être dangereuse pour l' homme , en raison des chocs électriques qu' elle produit , et il faut donc y faire attention , en veillant notamment à ce que l' aquarium n' ait pas de parties en métal qui puisse conduire le courant .
en fait , même si certaines personnes peuvent avoir une anguille électrique chez elles , c' est un poisson que l' on trouvera plus facilement dans les grands aquariums publics , ou dans les zoos .
parfois , ces aquariums sont équipés de micros : les micros captent les chocs électriques et les transforment en sons , ce qui permet de les " entendre " .
plusieurs zoos et aquarium publics élèvent des anguilles électriques , on peut donc facilement en voir une :
à l' aquarium tropical du Palais de la Porte Dorée , à Paris