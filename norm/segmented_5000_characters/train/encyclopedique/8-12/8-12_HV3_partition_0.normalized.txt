il était une fois DISNEY
voilà cinquante ans qu' il a disparu et pourtant , son nom est toujours aussi connu des enfants !
le papa de Mickey a marqué l' histoire du dessin animé en créant son propre studio .
la nature , mais sa famille souviendra toute sa vie de déménage à Kansas City son enfance .
né en mille neuf cent un .
et son père oblige ses aux États Unis , il grandit enfants à travailler .
à dans le Missouri , au cinq heures , chaque matin contact des animaux de la avant l' école , Walt livre fermé .
il aime ce rapport à des journaux .
qu' il pleuve ,
qu' il vente ou qu' il neige !
cette éducation à la dure le marque profondément .
créer des films et des parcs d' attractions sera , devenu grand , un moyen de rattraper son enfance .
des contes de fées aux films Blanche Neige et les sept nains un d' autres adaptations de contes , comme est le premier long métrage réalisé La Belle au bois dormant ou Cendrillon en animation !
tiré du conte des frères de Charles Perrault , et de romans Grimm écrit au XIXe siècle , ce film est comme Le Livre de la jungle de Rudyard un pari fou de Disney .
en effet , il a Kipling et Alice au pays des merveilles réclamé plus de cinq cents dessins sur de Lewis Carroll .
même s' il symbolise lesquels ont travaillé sept cents artistes ...
cela l' Amérique , Disney s' est beaucoup couta une fortune aux studios , mais inspiré des histoires traditionnelles quel succès !
après ce triomphe , Disney et de la peinture européennes pour et son équipe de scénaristes réaliseront les merveilleux décors de ses films .
c' est ainsi que Walt racontait l' histoire de ses studios .
après un séjour en Europe durant la Première Guerre mondiale et des études d' art , il débute dans la publicité comme animateur , puis crée son studio d' animation en mille neuf cent vingt trois : les studios Disney .
mais il n' est pas très bon dessinateur .
son talent est plutôt d' avoir le flair pour s' associer avec des artistes exceptionnels .
ainsi , en mille neuf cent vingt huit , Walt et son ami , le talentueux dessinateur Ub lwerks , créent un héros qui devient célèbre dans le monde entier .
deux grandes oreilles , une tête ronde et un sourire taquin : Mickey Mouse ( " souris " en anglais ) est né !
sa première apparition dure sept minutes trente dans Steamboat Willie .
il sera rejoint par une grande famille : Donald Duck , Dingo , Pluto ...
un visionnaire au fort caractère Bourré d' énergie , Walt Disney est un infatigable innovateur .
il se soucie peu des questions d' argent , l' important étant de donner de beaux films aux spectateurs .
néanmoins , c' est un homme d' affaires !
en dix neuf cent cinquante cinq , en créant Disneyland , le premier parc d' attractions thématique , il propose aux familles de visiter un monde magique , inspiré en partie de son univers .
c' est nouveau pour l' époque !
Walt est aussi très entêté , il est parfois colérique et ne comprend pas ses collaborateurs .
pourtant , il considère ses employés comme une grande famille .
bref , il était un homme complexe aux idées géniales !
à sa mort , en dix neuf cent soixante six , les studios Disney mettront du temps