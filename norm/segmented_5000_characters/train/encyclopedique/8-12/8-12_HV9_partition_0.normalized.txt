un monde imaginaire .
à l' époque de Magellan , beaucoup de savants ont compris que la Terre était ronde .
mais d' autres personnes croient encore qu' elle est plate ...
certains s' imaginent que les navires qui s' aventurent jusqu' au bord de notre planète tombent dans le vide .
très peu d' Européens sont allés en Orient , c' est à dire en Inde , en Chine et dans les îles aux épices .
ceux qui en reviennent rapportent souvent des légendes qui n' ont rien à voir avec la vérité .
ils prétendent par exemple qu' il existe dans ces pays des hommes qui n' ont qu' un pied , mais si grand que lorsqu' ils se couchent par terre , ils le lèvent pour s' en servir de parasol !
ou des hommes aux oreilles qui pendent jusqu'à terre , si bien qu' ils peuvent se coucher sur l' une et se couvrir avec l' autre !
ou encore des plantes pouvant marcher , des oiseaux se nourrissant uniquement du coeur des baleines et toutes sortes d' autres
prodiges .
petit à petit , les informations rapportées par les explorateurs remplacent les légendes .
oui , la Terre est bien ronde et beaucoup plus grosse qu' on ne le pensait .
non , l' Amérique découverte par Christophe Colomb n' est
pas attachée à l' Asie .
oui , on peut faire le tour de l' Afrique en bateau pour aller en Inde .
et surtout , les gens qui vivent là bas ne sont guère différents
des Européens ...