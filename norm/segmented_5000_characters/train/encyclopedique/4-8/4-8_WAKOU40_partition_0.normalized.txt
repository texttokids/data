il construit son nid dans un grand arbre , avec des branches et des brindilles , et le tapisse de mousse pour plus de confort !
l' hiver est là .
mais le rouquin n' hiberne pas .
il ne s' endort pas dans un terrier comme sa cousine la marmotte .
quand le froid est trop piquant , il reste en boule au fond de son nid .
et quand il a faim , il sort déterrer un de ses trésors enfouis !
de quoi résister jusqu' au printemps où il pourra à nouveau s' activer , manger et sautiller dans tous les sens .
la sieste terminée , le temps des amours pourra recommencer !
mais c' est une autre histoire ...