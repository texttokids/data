conseils :
la baignade de ton chien
tous les chiens savent ils nager ?
c' est un réflexe : dès que les chiens sentent le contact de l' eau sous leur ventre , leurs pattes se mettent en mouvement toutes seules .
c' est rigolo !
certains sont d' excellents nageurs , comme les labradors , qu' on utilise parfois pour le sauvetage en mer .
d' autres sont moins doués , comme les bull dogs , qui se débattent dans tous les sens pour garder leur grosse tête hors de l' eau et ne pas couler .
il existe des cartes des plages où les chiens sont bienvenus , n' hésite pas à les consulter avant de partir !
quelle est leur baignade préférée ?
comme en promenade , ton chien aime se sentir libre d' aller où il veut , dans des coins sauvages .
une grande plage déserte , un lac ou un ruisseau dans la montagne , c' est génial !
les chiens qui adorent l' eau ne ratent pas une occasion de faire trempette : une piscine gonflable , un bassin plein de vase ou même une flaque d' eau , peu importe si c' est sale !
tu es un maître responsable , alors , après la baignade , empêche le
d' aller s' ébrouer sur les gens !
comment jouer dans l' eau ?
s' il n' y a pas trop de vagues ou de courant et que ton chien peut sortir tout seul de l' eau , alors plonge avec lui et éclabousser vous !