il y a dualisme entre matière et esprit .
l' une est solide , rigide , tangible et immobile de même que limitée , alors que l' esprit est fait de mélanger esprit et matière faisait partie du plan de Dieu .
l' homme est ainsi à la frontière entre le monde créé qui comporte les trois premiers règnes ( minéral , végétal et animal ) et le monde créateur .
or , de cette situation l' homme sur cette terre se voit attribuer une tâche , une finalité à savoir élever la matière , créer un lien entre le monde créé et le monde créateur , ceci afin que la Vie puisse circuler librement dans toute la création .
en d' autres termes , l' Esprit jouerait un rôle primordial sur la matière , selon John Eccles qui reçut le prix Nobel de physiologie et de médecine en
Teilhard de Chardin affirmait que nos pensées sont des énergies vivantes , des énergies psychiques qui imprègnent le milieu .
ainsi , mais de personne .
l' esprit n' est plus indépendant de la Matière , ni opposé à elle , mais émerge laborieusement d' elle sous l' attrait de Dieu
cette vision des choses expliquerait notamment les phénomènes de télépathie et l' affirmation selon laquelle les lieux possèderaient une mémoire , et qu' ainsi on trouve des lieux dits " maudits " de même que
Teilhard qui inventa le principe de noosphère , sorte de milieu d' énergies psychiques qui envelopperait la terre , affirme ainsi que selon la nature des pensées humaines bonnes ou mauvaises , ces dernières imprégnant le milieu , seraient , entre autres , capables de dégénérer les organismes de vie , viendraient alors les maladies .
cette thèse rejoint en fait la parabole du jardin d' Éden dont Adam et Ève furent chassés par leur péché .
dans ce cas , la matière n' est rien d' autre qu' une pâte création dans notre charge .
la matière , au sens propre comme au sens
dans l' économie moderne , on produit de plus en plus d' informations , qui sont stockées et diffusées sur des
les principaux supports d' information sont le papier , et , de plus en plus , les équipements électroniques qui stockent de l' information ( matériels informatiques , réseaux , bases de données , systèmes de gestion électronique des documents , systèmes de gestion de contenu ) ou la diffusent ( réseaux ) .
dans ce qu' on appelle quelquefois l' économie de l' immatériel , on fait souvent passer l' information du support papier au support que les termes " économie de l' immatériel " et " dématérialisation " sont peu appropriés , car en réalité on ne fait que changer le support de l' information .
le nouveau support , électronique , est lui aussi
on présente quelquefois la dématérialisation comme un avantage du point de vue du respect de l' environnement et du développement durable .
mais en réalité les choses ne sont pas si simples , car l' utilisation des deux types de supports d' information consomme de l' énergie et des ressources naturelles ( bois pour le papier , métaux pour les équipements électroniques ) et génère des déchets ( les vieux papiers et les déchets d' équipements
le bilan global n' est donc pas si simple à établir du point de vue de