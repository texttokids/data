la formation de l' île est datée de sept à quinze millions d' années .
l' Île est d' origine volcanique et encore de nos jours , on peut observer les traces de la grande caldeira à l' origine de sa formation .
l' Île ne compte plus de volcan en activité .
on y trouve cependant des cratères endormis dont le trou aux Cerfs , qui est devenu au fil des ans une des attractions touristiques .
celui ci se situe dans le Centre de l' île , à Curepipe .
l' Île Maurice couvre une superficie de un huit cent soixante cinq km deux .
elle mesure dans ses plus grandes dimensions soixante cinq kilomètres de longueur et quarante cinq kilomètres de largeur .
le point le plus haut est le Piton de la Petite Rivière Noire qui culmine à huit cent vingt huit mètres .
ses plaines côtières et un plateau central ont permis pendant longtemps la culture extensive de la canne à sucre et du thé .
l' Île Maurice est surtout connue pour ses beaux paysages .
la barrière de corail qui entoure l' île permet de protéger les lagons et les plages bordées de
articles détaillés : histoire de Maurice et Île de France
les Portugais furent les premiers Européens à visiter l' île , à une date comprise entre mille cinq cent et mille cinq cent treize .
ils l' appelèrent Cirné , nom du navire du capitaine d' expédition Diogo Fernandes Pereira .
cependant l' île demeura inhabitée jusqu'à l' arrivée des premiers colons hollandais , en mille cinq cent quatre vingt dix huit .
l' Île fut ensuite colonisée par les français de mille sept cent quinze à mille huit cent dix .
vinrent ensuite les britanniques qui l' occupèrent par la force avant que cette possession ne leur soit confirmée par le traité de Paris un .
l' établissement britannique dura jusqu'à l' indépendance le douze mars mille neuf cent soixante huit.
la population s' élève à environ un , trois million d' habitants et comporte plusieurs communautés .
la population mauricienne est très métissée avec la communauté créole , qui ne peut être définie par une seule origine et une couleur de peau spécifique ( puisque certains Créoles ont en grande partie des ancêtres africains et ou malgaches , quand d' autres Créoles ont en grande partie des ancêtres originaires d' Europe ou
travailleurs engagés , d' origine indienne , Il existe également une minorité de Sino Mauriciens et une minorité d' ascendance
les différents groupes ethniques et religieux à Maurice comprennent les descendants créoles d' esclave africains , les descendants hindous de travailleurs indiens sous contrat , les Tamouls du sud de l' Inde , les musulmans indiens , une communauté chinoise et une communauté plus
l' Île Maurice n' a pas de langue officielle mais l' anglais est reconnu comme la première langue du pays ( administration ) et le français comme seconde langue ( médias ) .
de ces deux dernières , le français est de loin la langue la plus parlée .
il est ainsi enseigné à partir de l' âge de quatre ans dans les écoles publiques .
le créole mauricien est parlé par la majorité de la population mais n' est pas reconnu officiellement par la constitution .
une langue asiatique ou le créole mauricien peut être choisi à partir de l' âge de six ans par les enfants à l' école primaire .
les langues asiatiques les plus parlées sont par ordre décroissant , l' hindi , l' ourdou , le
d' après le recensement effectué en vingt cent onze par Statistics Mauritius , l' hindouisme est la religion majoritaire à cinquante et un , neuf pour cents , suivi du
bouddhisme ( zéro , quatre pour cents ) .
les mauriciens pratiquant d' autres religions représentent zéro , deux pour cents de la population et les non croyants zéro , sept pour cents .
au début des années quatre vingt dix , un certain nombre de groupes islamistes se sont formés dans le but de réislamiser la communauté musulmane .
ces organisations sont toujours actives à Maurice et bénéficient de liens saoudite , créa le parti du Hezbollah dans le but de défier la domination hindoue créole au sein du gouvernement .
lors des élections municipales de dix neuf cent quatre vingt seize , le parti remporta cinq sièges à Port Louis , mais sa progression fut rapidement enrayée par une histoire sensationnelle sur fond de braquages de banque et de meurtres à motivation
en juillet vingt cent dix neuf, le gouvernement de l' île interdit l' entrée sur son territoire d' images pouvant offenser une religion ou la morale .
la décision aurait pour but de préserver la paix intercommunautaire et
selon le magazine Marianne , l' île Maurice serait touchée depuis quelques années par une augmentation du fanatisme musulman exporté
en vingt cent quinze , le premier campus de l' African Leadership University y est créé avec l' ambition de former l' élite intellectuelle africaine de
deux genres musicaux locaux sont présents sur l' île .
le séga et le séga ravanne ( ancien séga traditionnel ) , musique et
les chants et danses en langue bhojpuri sont aussi populaires .
d' autres courants musicaux font partie de la richesse musicale de l' île , on retrouve le seggae ( issu de la fusion du séga et du reggae ) avec des artistes très populaires tels que Kaya et Racinetatane et Ras Natty Baby et les Natty Rebels , et egalement le Sagga ( issu de la fusion entre le sega et le ragga ) avec le groupe