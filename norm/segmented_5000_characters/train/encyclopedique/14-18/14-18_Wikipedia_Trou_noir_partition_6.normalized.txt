une des premières méthodes de détection d' un trou noir est la détermination de la masse des deux composantes d' une étoile binaire , à partir des paramètres orbitaux .
on a ainsi observé des invisible .
le compagnon massif invisible peut généralement être interprété comme une étoile à neutrons ou un trou noir , puisqu' une masse du compagnon ( ou la fonction de masses , si l' angle d' inclinaison est inconnu ) est alors comparée à la masse limite maximale des étoiles à neutrons ( environ trois , trois masses solaires ) .
si elle dépasse cette limite , on considère que l' objet est un trou noir .
on considère également que certains trous noirs stellaires apparaissent lors des sursauts de rayons gamma ( ou GRB , pour gamma ray burst en anglais ) .
en effet , ces derniers se formeraient via l' explosion d' une dans certains cas ( décrits par le modèle collapsar ) , un flash de rayons gamma est produit au moment où le trou noir se forme .
ainsi , un GRB pourrait représenter le signal de la naissance d' un trou noir .
des trous noirs de plus faible masse peuvent aussi être formés par des supernovae classiques .
le rémanent de supernova SN mille neuf cent quatre vingt sept À est soupçonné d' être un trou noir , par exemple .
un deuxième phénomène directement relié à la présence d' un trou noir , cette fois pas seulement de type stellaire , mais aussi super massif , est la présence de jets observés principalement dans le domaine des ondes radio .
ces jets résultent des changements de champ magnétique à grande échelle se produisant dans le disque
l' objet trou noir en tant que tel est par définition inobservable , toutefois , il est possible d' observer l' environnement immédiat d' un trou noir ( disque d' accrétion , jets de matière .
) à proximité de son horizon , permettant ainsi de tester et vérifier la physique des trous noirs .
la petite taille d' un trou noir stellaire difficile .
en guise d' exemple , et même si la taille angulaire d' un trou noir est plus grande que celle d' un objet classique de même rayon , un trou noir d' une masse solaire et situé à un parsec ( environ trois , vingt six années lumière ) aurait un diamètre angulaire de
cependant , la situation est plus favorable pour un trou noir super massif .
en effet , la taille d' un trou noir est proportionnelle à sa masse .
ainsi , le trou noir du centre de notre galaxie a une masse , bien estimée , d' environ trois , six millions de masses solaires .
son rayon de Schwarzschild est donc d' environ onze millions de kilomètres .
la taille angulaire de ce trou noir , situé à environ huit , cinq kiloparsecs de la terre est de l' ordre de quarante microsecondes d' arc .
cette résolution est inaccessible dans le domaine visible , mais est assez proche des limites actuellement atteignables en interférométrie radio .
la technique de l' interférométrie radio , avec une sensibilité suffisante , est limitée en fréquence au domaine millimétrique .
un gain d' un ordre de grandeur en fréquence permettrait une résolution meilleure que la taille angulaire du trou noir .
le dix avril deux mille dix neuf, le projet Event Horizon Télescope publie les premières images de M quatre vingt sept , le trou noir supermassif se trouvant au coeur de la galaxie M quatre vingt sept .
ces restitutions sont obtenues grâce à un algorithme de reconstitution d' image , baptisé " chirp " ( Continuous High resolution Image Reconstruction using Patch priors ) , mis au point par la scientifique
ces images permettent de distinguer la silhouette du trou noir dans un
Cygnus X un , détecté en mille neuf cent soixante cinq , est le premier objet astrophysique identifié comme pouvant être la manifestation d' un trou noir .
c' est un système binaire qui serait constitué d' un trou noir
les systèmes binaires stellaires qui contiennent un trou noir avec un disque d' accrétion formant des jets sont appelés microquasars , en référence à leurs parents extragalactiques : les quasars .
les deux classes d' objets partagent en fait les mêmes processus physiques .
parmi les microquasars les plus étudiés , on notera GRS mille neuf cent quinze plus cent cinq , découvert en mille neuf cent quatre vingt quatorze pour avoir des jets supraluminiques .
un autre cas de tels jets fut détecté dans le système GRO J mille six cent cinquante cinq quarante .
mais , sa distance est sujette à controverse et ses jets pourraient ne pas être supraluminiques .
notons aussi le microquasar très spécial SS quatre cent trente trois , qui a des jets persistants en précession et où la matière se déplace par paquets à des vitesses de quelques fractions de
les candidats comme trous noirs supermassifs ont premièrement été les noyaux actifs de galaxie et les quasars découverts par les radioastronomes dans les années mille neuf cent soixante .
cependant , les observations les plus convaincantes de l' existence de trous noirs supermassifs sont celles des orbites des étoiles autour du centre galactique appelé Sagittarius A .
les orbites de ces étoiles et les vitesses atteintes ont permis aujourd'hui d' exclure tout autre type d' objet qu' un trou noir supermassif , de l' ordre de quatre millions de masses solaires à cet endroit de la galaxie .
par la suite , des trous noirs supermassifs ont été détectés dans de nombreuses autres galaxies .