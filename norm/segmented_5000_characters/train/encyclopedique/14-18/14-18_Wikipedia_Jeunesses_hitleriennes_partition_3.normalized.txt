pendant la bataille de Berlin , les Jeunesses hitlériennes constituent une part importante des forces allemandes et se battent avec fanatisme ( Alfred Czech ayant même été le plus jeune soldat décoré par Adolf Hitler ) .
le commandant de la ville , le général Helmut Weidling ordonne à Artur Axmann de dissoudre les unités combattantes des Jeunesses hitlériennes , cet ordre n' est jamais appliqué à cause de la confusion de la bataille de Berlin .
dissous les Jeunesses hitlériennes comme partie intégrante du Parti nazi .
des membres des Jeunesses hitlériennes furent accusés de crime de guerre mais , dans la mesure où l' organisation était constituée de mineurs , les efforts pour faire aboutir les poursuites
bien que les Jeunesses hitlériennes ne fussent jamais déclarées avait commis des crimes contre la paix en corrompant les jeunes esprits allemands .
de nombreux cadres de haut niveau furent jugés par les Alliés , à l' instar de Baldur von Schirach condamné à vingt ans