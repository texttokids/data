un mouchoir peut très bien servir d' objet de mode , autour du cou , sur la tête , notamment pour se protéger du soleil ou de la pluie , entre autres exemples , durant la saison chaude en Afrique ( le mouchoir est alors appelé moussor ) ou chez les travailleurs de bords de mer en France .
en effet les mouchoirs de tête flottants étaient très répandus autrefois chez les gens qui travaillaient sous le soleil , et a donné de nombreux dérivés , telle la pointe des comtadines , la quichenotte ( ou kissnot ) des ramasseuses d' huîtres des îles du littoral atlantique ( Oléron , Ré , et caetera ) ou de la côte
les limites entre le mouchoir et l' écharpe ou d' autres vêtements deviennent alors assez floues , même si la dénomination mouchoir de col
assortiment de costume : le mouchoir qui dépasse de la poche ,
par ailleurs , le mouchoir peut être le moyen d' une véritable ostentation non de richesse , mais de moeurs .
dans les années mille neuf cent soixante dix , mouchoir de costume pour indiquer l' homosexualité du porteur .
pour un regard initié , la couleur signalait les préférences sexuelles de la personne .
ce n' est qu' un aspect du mouchoir comme marque d' appartenance ont parfois recours à des mouchoirs de couleur pour indiquer ostensiblement l' équipe qu' ils soutiennent , même si les serviettes sont
mais le mouchoir peut cumuler un statut d' accessoire vestimentaire et une fonction rituelle , comme le montre son utilisation politique et partisane .
en effet le mouchoir passé au bras devient brassard et étalé sur le front fait office de bandeau .
l' improvisation est un aspect essentiel de certaines actions politiques , syndicales , et caetera ( par exemple une grève ) .
les partisans d' une cause populiste ont dans le passé aisément pris cet humble bout de tissu pour signe de ralliement , ainsi des membres actifs du Movimento dos Pioneiros au Portugal et leurs brassards rouges , à la base un mouchoir .
on comprend alors que le mouchoir , s' il reste un objet modeste , peut parfois devenir outil essentiel à des objectifs élevés et que c' est cette modestie elle même qui peut servir la cause échéante .
de tels signes de ralliement apparaissent souvent spontanément , à l' improviste : le mouchoir , ici encore , est objet par défaut .
mais dès lors que la banderole de mouchoirs improvisée devient un drapeau reconnu , on fabrique des drapeaux dans un format standardisé , à partir de tissu prévu à cet effet : le mouchoir retourne alors à son
ainsi cet objet ne pourra jamais occuper des fonctions plus nobles que par défaut , malgré le grand nombre de ses utilisations possibles .
rarement cependant , il peut occuper une place non par défaut mais par préférence : dans certains sports , les supporteurs d' une équipe agitent des mouchoirs pour marquer leur désaccord avec les dispositions de celles ci , au lieu de siffler par exemple .
dans une corrida , différentes couleurs de mouchoirs sont utilisées pour divers signaux du public au président de la corrida , du président au torero , et caetera Par exemple , le mouchoir blanc indique le soutien du public au torero , le mouchoir vert du président est le signe qu' il faut changer un taureau blessé ou défectueux .
récemment un , la couleur orange a été introduite , signifiant la grâce accordée par le président au taureau ( voir Corrida ) .
comme toute coutume , sans doute ces dernières sont elles spontanées à leurs origines , qu' elles ont peut être communes , provenant
la publicité utilise parfois des espaces inattendus .
à gauche , la réclame est imprimée directement sur l' enveloppe du paquet de mouchoirs .
à droite , un carton publicitaire est inséré entre les
d' une manière générale le mouchoir , malgré ses nouvelles fonctions , sera resté un objet censé être de première utilité , même s' il a pu n' être qu' un pur objet de mode .
l' appellation même de mouchoir le renvoie à sa basse extraction un objet banal au service d' un besoin physique basique et l' oblige à rester des plus communs .
le sème
le mouchoir , objet domestique et commun de grande diffusion au début du XX E siècle , a servi de support à une propagande nationaliste imagée .
ainsi , en France , on a pu voir des cocardes tricolores et autres symboles patriotiques sur des mouchoirs des
un autre exemple de récupération propagandiste , plus récent , se situe dans le Japon contemporain .
les entreprises font distribuer des paquets de mouchoirs gratuits dans la rue , cette distribution aux passants est très répandue .
sur les paquets de mouchoirs sont imprimées des publicités ( sans rapport avec les mouchoirs jetables ) .
le mouchoir sert ainsi de support d' une publicité polie ( offerte avec le