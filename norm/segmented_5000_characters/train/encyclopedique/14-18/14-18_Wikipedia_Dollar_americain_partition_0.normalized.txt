représentant soixante deux pour cents des réserves des banques centrales à travers
présente en deux mille dix neuf dans quatre vingt huit pour cents des transactions ( un chiffre inchangé par rapport à deux mille seize ) , contre trente deux pour cents pour l' euro ( source :
importants , même si cette prépondérance est en train d' être remise
enfin , certes pour des montants beaucoup moins importants , c' est une monnaie fiduciaire d' un usage très répandu dans le monde , et plus de la moitié de son encours en billets est en fait détenu hors de son
la valeur externe du dollar est du ressort exclusif du gouvernement fédéral américain , et c' est le Secrétaire au Trésor qui en a la responsabilité .
la banque centrale américaine , la Réserve fédérale des États Unis , en particulier , ne jouit d' aucun mandat pour communiquer sur la valeur de la devise .
l' appellation " dollar " est une déformation du nom d' une monnaie d' argent européenne , le thaler .
cette pièce avait connu plusieurs variations à partir du Joachimsthaler créé en Bohême au XV E siècle .
le thaler a été utilisé aux XVI E et XVII E siècles dans divers états allemands , jusqu' au thaler de Marie Thérèse frappé du XVIII E au XX E siècle .
c' est finalement la pièce de huit réaux de l' empire espagnol , inspirée du Joachimsthaler et appelée par les anglais spanish dollar , qui servira d' étalon pour établir le
les français appelleront ces pièces en argent , sensiblement de même titrage et poids , " piastre " , terme qui est resté chez les francophones d' Amérique du Nord pour désigner le dollar .
enfin , aux États Unis comme dans d' autres pays ayant une monnaie de même nom , l' usage familier préfère souvent au terme " dollar " celui de buck , qui désigne un cervidé mâle ( colloquialisme ) .
cet emploi , attesté dès dix huit cent cinquante six , vient peut être d' une abréviation de buckskin , " balle de peaux de cerf " , dont l' usage en tant qu' unité de valeur remonte au moins à dix sept cent quarante huit : en un temps où le numéraire était rare , une balle de peaux pouvait faire office de monnaie dans le commerce colonial entre Amérindiens et Européens , si cette pratique s' est progressivement éteinte à partir de l' introduction du dollar en dix sept cent quatre vingt douze , le nom serait resté , passant au nouveau moyen d' échange .
une autre origine possible est un terme de
le premier billet à avoir été imprimé sur le sol américain date de seize cent quatre vingt dix , par la colonie britannique du Massachusetts .
il était alors exprimé en livre sterling .
de dix sept cent neuf à dix sept cent cinquante cinq , les treize colonies imprimèrent différents types de billets , toujours en livres .
après le début de la guerre d' indépendance des États Unis , à partir de dix sept cent soixante seize , sont fabriqués sous l' égide du Congrès continental des billets exprimés en dollars continentaux , le Continental dollar excessive jusqu' en dix sept cent quatre vingt deux , et engendrant une quasi banqueroute puis
le dollar est finalement adopté par le Congrès de la Confédération
officielle des États Unis en vertu du Mint Act en dix sept cent quatre vingt douze .
le système décimal est alors choisi : un dollar égale dix dimes égale
la valeur faciale des premières pièces de un dollar frappés en dix sept cent quatre vingt quatorze est indexée sur l' argent : un dollar équivaut à vingt six , quatre vingt seize grammes d' argent au titre de huit cent quatre vingt douze mille E .
ce poids a été calqué sur la pièce espagnole de huit reales , appelée aussi pièce de huit ( peso de ocho ) pesant vingt sept , six cent soixante quatorze grammes d' argent au titre de huit cent quatre vingt seize mille E qui était couramment utilisée dans les échanges internationaux au XVIII E siècle et qui circulait sur le sol américain sous le nom de dollar espagnol ( Spanish
en dix huit cent zéro , par comparaison , la pièce de cinq francs français pesait vingt cinq grammes au titre de neuf cents mille , la pièce de un couronne britannique ( cinq shillings soit un quatre de ) pesait vingt huit , deux mille sept cent cinquante neuf grammes au titre de neuf cent vingt cinq mille .
en dix huit cent soixante et un , les premiers billets verts apparaissent .
la maxime " in God We Trust " ( " en Dieu nous croyons " ou " nous avons confiance en Dieu " ) apparait pour la première fois en dix huit cent soixante quatre sur la pièce de deux cents .
approuvée en dix neuf cent cinquante cinq par un acte du Congrès , elle est depuis dix neuf cent cinquante sept systématiquement imprimée sur tous les billets américains .
cette section est vide , insuffisamment détaillée ou incomplète .
les dernières pièces de un dollar en argent , dits peace dollar ayant véritablement circulé sont frappés en dix neuf cent trente cinq et pèsent vingt six , soixante treize grammes au titre de neuf cents mille E .
les sous multiples de un deux ( half dollar ) vingt cinq cents
le système monétaire mondial dit du Gold Exchange Standard Gênes en dix neuf cent vingt deux jusqu' en dix neuf cent trente trois ( Roosevelt décide de suspendre la convertibilité du dollar en or pour le dévaluer ) puis par les accords de Bretton Woods en dix neuf cent quarante quatre et donne une place prépondérante au dollar .
il repose sur deux piliers principaux :
si le système fonctionne correctement dans les années dix neuf cent cinquante , l' accumulation dans les années dix neuf cent soixante des déficits américains encore accrus par les dépenses afférentes à la guerre du Viêt Nam entrainait tout au début de la décennie dix neuf cent soixante dix de très fortes pressions sur la monnaie américaine .
or sa fonction de monnaie de réserve mondiale gênait fort peu les États Unis et ne les incitait pas