la future Catherine le deuxième , née Sophie Frédérique Augusta d' Anhalt Zerbst le vingt et un avril dix sept cent vingt neuf ( deux mai dix sept cent vingt neuf dans le calendrier grégorien ) , est l' aînée des enfants de Christian Auguste d' Anhalt Zerbst et de son épouse Jeanne Élisabeth de Holstein Gottorp .
lors de sa naissance , ses parents déplorent qu' elle
son père était militaire pour le roi de Prusse et , dès dix sept cent vingt neuf , commandant de Stettin , résidant dans le Château ducal de Stettin où sa fille est née , et ensuite élevée .
en dix sept cent quarante et un il devint gouverneur de la province de la Poméranie ultérieure et à partir de dix sept cent quarante deux prince
de son éducation protestante , austère , rigide , entourée de peu d' affection , une femme demeure en la personne d' une huguenote française , Babette Cardel , qui dirige son éducation et lui enseigne avec la langue française , manières et grâces de la société dont elle est issue .
elle lui donne en même temps le gout de la littérature française de son époque .
très vite , la princesse se tourne vers des activités spirituelles , ainsi que vers la lecture et les études .
introduite par sa mère dans les plus hautes cours d' Allemagne , elle se fait remarquer par son charisme .
la mère de Sophie , suivant les affaires de Russie , voit le futur Pierre le troisième bien disposé à succéder à sa tante Élisabeth Petrovna , et permet une union avec Sophie .
prenant soin d' envoyer des portraits de sa fille à la cour , ses manoeuvres portent leurs fruits et , en janvier dix sept cent quarante quatre , elle et sa fille sont conviées en Russie .
les intentions de l' impératrice sont claires , Sophie sera la future épouse de Pierre , pourtant son prestige est faible , et ce n' est ni l' or ni une alliance puissante qui pousse au choix de Sophie .
mais après les difficultés de succession créées par des revendications du trône de divers partis , Élisabeth est décidée à ne pas avoir de complications diplomatiques ou de revendications extravagantes .
de plus , Sophie est jeune et inexpérimentée en politique : elle ne représente apparemment aucun danger pour le trône
de son côté , Sophie , qui a alors quatorze ans , comprend ce qui se joue .
loin d' être ignorante du prestige et du pouvoir qui s' attacheraient à son futur statut , elle balaye les hésitations naissantes de sa mère vis à vis de cette union .
à leur arrivée en Russie , Sophie et sa mère sont accueillies par toute une grande procession jusqu'à Moscou .
elles rencontrent alors l' impératrice et son neveu Pierre .
dans ses Mémoires , Catherine parle de la grandeur d' Élisabeth , mais ne dit mot sur
l' ascension vers le statut de grande duchesse se fait presque sans heurt ( excepté une maladie qui la rapproche d' Élisabeth ) lors de sa conversion en grande pompe à la religion orthodoxe le vingt huit juin mille sept cent quarante quatre .
elle s' exprime clairement en russe devant un peuple qui l' adopte bientôt .
à cette date , elle prend officiellement le nom de Catherine
elle se fiance à Pierre le lendemain , devenant " grande duchesse et altesse impériale " .
conseillée dans ses lectures par divers intellectuels de passage , elle demande le catalogue de l' Académie des sciences où elle commande Plutarque , Montesquieu et d' autres
Catherine a alors quinze ans .
son fiancé , longtemps éloigné d' elle par une pleurésie , revient décharné et d' un aspect qui effraye la jeune Catherine mais cela n' ébranle pas sa volonté de
le mariage des deux adolescents a lieu à Saint Pétersbourg le vingt et un août mille sept cent quarante cinq ( un er septembre mille sept cent quarante cinq dans le calendrier grégorien ) , il est célébré au cours d' une somptueuse cérémonie , suivie de dix jours de fête .
questionnée le lendemain sur sa nuit de noces , Catherine ne trouve rien à dire .
diverses hypothèses présentent Pierre le troisième comme sexuellement immature , innocent , ou encore impuissant à cause d' un phimosis , à l' inverse de Catherine autour de
Catherine , convertie à l' orthodoxie , n' eut pas un mariage heureux d' autant qu' elle prenait le parti de l' opposition et lisait Machiavel , Tacite , Voltaire et Montesquieu , si bien qu' elle était en résidence surveillée au palais de Peterhof et que son mari menaçait de l' enfermer et de mettre sa maîtresse sur le trône
la nièce par alliance de l' impératrice Élisabeth I re qui voulait absolument un héritier lui proposa de prendre comme amant le prince Lev Alexandrovitch Narychkine ( mille sept cent trente trois mille sept cent quatre vingt dix neuf ) ou le comte Sergei Saltykov car Catherine n' avait toujours pas d' enfants après huit ans de mariage .
elle choisit finalement Saltykov et joua alors sur l' ambiguïté que le géniteur de son fils Paul I er né en mille sept cent cinquante quatre pouvait aussi
très à l' écoute des évènements qui se déroulaient dans son nouveau pays , Catherine , qui possédait l' affection du peuple russe , réussit à faire détrôner son époux avec la complicité de son amant Grigori Orlov et de quatre officiers de la garde impériale , frères d' Orlov lors du coup d' État du vingt huit juin mille sept cent soixante deux ( neuf juillet dans le calendrier