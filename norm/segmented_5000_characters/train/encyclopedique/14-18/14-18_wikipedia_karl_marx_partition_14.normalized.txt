la prolétarisation est la double conjonction de la transformation de l' Homme en prolétaire et de l' augmentation de leur nombre .
qu' est ce qu' un prolétaire ?
c' est un individu qui ne possède que sa seule force de travail , et pas les moyens de la production .
il est par conséquent obligé de vendre sa force de travail au capitaliste sous forme de salaire pour subvenir à ses besoins .
tout travailleur salarié est un prolétaire .
Marx avait très bien anticipé le développement du taylorisme à ce sujet .
la division du travail est en effet un mouvement constant du capitalisme .
il est dû à l' amélioration des techniques et notamment des machines , qui ont fait apparaître les ouvriers spécialisés .
il est également la conséquence d' une recherche de rentabilité accrue .
chaque salarié du système capitaliste ne devient capable que d' assurer une infime partie de la production .
son travail n' a pas de sens en lui même .
il n' est qu' un rouage d' un immense mécanisme .
il ne peut plus avoir de vie individuelle .
de plus , du fait de cette division continue du travail , et du développement des techniques , le chômage est appelé à se développer .
c' est l' " armée de réserve " , et celle ci , par sa simple présence , exerce une pression sur les salariés , qui ont peur de se retrouver au chômage .
le chômage empêche les travailleurs de se révolter .
les salaires ont donc une tendance continue à la baisse à long terme relativement aux possibilités qu' offre l' époque dans laquelle vivent les travailleurs , et la concentration du capital est aussi inéluctable .
la prolétarisation est donc la " corrélation entre l' accumulation de richesses et l' accumulation de misères " .
le prolétaire possède également d' autres caractéristiques telle que l' absence de propriété .
comment sortir de cette misère ( parfois matérielle , mais aussi surtout psychologique ) ?
il faut , selon Marx , que la société se libère du capitalisme par la révolution .
cette révolution doit libérer le prolétariat , mais aussi toutes les classes sociales , notamment les classes dominantes , qui sont également aliénées ( par l' argent notamment , comme on l' a vu plus haut ) .
c' est donc une révolution pour toutes les classes visant à abolir les classes elles mêmes ( société sans classes ) .
cette révolution doit être globale .