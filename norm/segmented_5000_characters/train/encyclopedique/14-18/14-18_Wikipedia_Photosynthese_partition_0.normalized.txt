arbre phylogénétique universel montrant les organismes photosynthétiques dans deux des trois domaines , selon Guillaume Lecointre et Hervé Le Guyader un et d' après Purificación López García et David Moreira un .
les organismes photosynthétiques sont photoautotrophes , ce qui signifie qu' ils sont capables de synthétiser leurs biomolécules directement à partir de composés minéraux le plus
l' énergie lumineuse reçue du soleil .
cependant , tous les organismes capables d' utiliser l' énergie lumineuse pour leur métabolisme ne sont pas nécessairement photosynthétiques : les organismes dits photohétérotrophes synthétisent leurs biomolécules
chez les plantes , les algues , les cyanobactéries et plusieurs taxons de bactéries ( bactéries sulfureuses vertes , bactéries non sulfureuses vertes ) , la photosynthèse libère de
la photosynthèse oxygénique .
bien qu' il y ait des différences dans la biosynthèse réalisée par ces organismes , les mécanismes généraux restent tout à fait semblables d' une espèce à l' autre .
il existe cependant des bactéries qui possèdent une photosynthèse
le dioxyde de carbone est converti en glucides à travers un processus appelé fixation du carbone .
il s' agit de réactions d' oxydoréduction endothermiques , de sorte que ce processus a
pour rendre ces réactions thermodynamiquement favorables .
la photosynthèse est globalement la réciproque de la respiration cellulaire , au cours de laquelle des composés organiques tels que le glucose sont oxydés en dioxyde de carbone et en eau afin de libérer de l' énergie et de produire des coenzymes réductrices .
cependant , ces deux processus impliquent des réactions chimiques différentes réalisées dans des compartiments cellulaires différents .
la photosynthèse se déroule en deux phases : lors de la première , les réactions dépendantes de la lumière captent l' énergie lumineuse et l' utilisent pour produire une coenzyme réductrice , le NADPH , et une coenzyme qui stocke l' énergie chimique , l' ATP , tandis que , lors de la seconde phase , les réactions indépendantes de la lumière utilisent ces coenzymes pour absorber et réduire le dioxyde de carbone .
la plupart des organismes photosynthétiques oxygéniques utilisent la lumière visible , cependant ils ne sont actifs qu' à certaines longueurs d' onde ( c' est à dire qu' ils absorbent ces longueurs d' onde par leurs pigments photosynthétiques ) pour la
bleus et les rouges ( longueurs d' onde autour quatre cent quarante et de six cent quatre vingts nanomètres ) , d' autres pigments absorbant d' autres longueurs d' onde comme la
caroténoïdes , jouent un rôle dans la photosynthèse , aussi certains organismes photosynthétiques utilisent le proche infrarouge ou , plus particulièrement , le rouge lointain , ( autour
on relèvera enfin que , si la photosynthèse est un processus biologique spécifique aux plantes , aux algues et à certains microorganismes ( dont des protistes et des bactéries ) , il existe également un animal , la limace de mer Elysia chlorotica , connu pour être le siège d' un processus de photosynthèse se déroulant dans des chloroplastes qu' il ne produit pas lui même mais qu' il absorbe avec les algues dont il se
l' équation globale de la photosynthèse peut s' écrire :
dans la photosynthèse oxygénique , l' eau est le donneur d' électrons , dont la dissociation libère de l' oxygène tandis que de l' eau est réformée à partir d' un atome d' oxygène du dioxyde de
on simplifie généralement l' équation en éliminant deux N
la photosynthèse anoxygénique utilise d' autres composés que l' eau comme donneur d' électrons .
ainsi , les bactéries pourpres sulfureuses
les bactéries sulfureuses vertes sont par exemple capables
soufre élémentaire S zéro comme donneurs d' électrons , tandis que les bactéries vertes non sulfureuses peuvent utiliser
microorganismes sont capables d' utiliser l' arsénite
parmi les donneurs d' électrons rencontrés chez les organismes photosynthétiques , on peut encore relever le fer ferreux Fe deux plus ,
des composés organiques tels que des acides et des
articles détaillés : chloroplaste et thylakoïde .
chez les bactéries photosynthétiques , les protéines qui absorbent la lumière pour la photosynthèse sont incluses dans des membranes cellulaires , ce qui représente l' arrangement le plus simple pour ces protéines .
cette membrane peut cependant être repliée en feuillets cylindriques appelés thylakoïdes ou
intracytoplasmiques .
ces structures peuvent remplir la majeure partie de l' intérieur d' une cellule , offrant ainsi une très grande surface membranaire pour permettre à la bactérie d' absorber