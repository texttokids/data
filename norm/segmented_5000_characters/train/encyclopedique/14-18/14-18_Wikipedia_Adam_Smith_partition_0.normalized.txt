Adam Smith est né le cinq juin dix sept cent vingt trois à Kirkcaldy .
dès sa naissance , Adam Smith est orphelin de père .
ce dernier , contrôleur des douanes , meurt deux mois avant la naissance de son fils .
à l' âge de quatre ans , Adam Smith est enlevé par des bohémiens , qui , prenant peur en voyant l' oncle du jeune garçon les poursuivre , l' abandonnent sur la route où il sera retrouvé , .
Adam Smith part étudier à Glasgow à l' âge de quatorze ans et y reste de dix sept cent trente sept à dix sept cent quarante .
il y reçoit , entre autres , l' enseignement de Francis Hutcheson , le prédécesseur d' Adam Smith à la chaire de philosophie morale .
Smith sera très influencé par Hutcheson , .
ayant obtenu une bourse , destinée en partie à former le clergé anglican écossais ( le statut de cette bourse l' université d' Oxford .
il ne se plait guère dans cette université .
plus tard dans son livre la Recherches sur la nature et les causes de la richesse des nations , il écrit : " il y a plusieurs années qu' à l' université d' Oxford la plus grande partie des professeurs publics ont abandonné totalement jusqu'à l' apparence même d' enseigner " .
il choisit lui même ses lectures , un choix qui lui vaut d' être menacé d' expulsion de l' université lorsqu' on découvre dans sa chambre le Traité de la nature humaine du philosophe David Hume , lecture
choisissant une carrière universitaire , Smith obtient à l' âge de vingt sept ans la chaire de logique à l' université de Glasgow et plus tard celle de philosophie morale .
le corps enseignant apprécie peu ce nouveau venu qui sourit pendant les services religieux et qui est de plus un ami déclaré de David Hume .
pourtant Smith devient relativement connu à Glasgow , où il participe à des cercles intellectuels , joue au whist le soir ...
il est apprécié de ses étudiants : ses manières et son allure peu commune lui valent d' être imité , et on voit même de petits bustes de lui dans
hochements de tête et sa diction maladroite dérivaient d' une maladie nerveuse dont il souffrit tout au long de sa vie .
au delà de son excentricité , la célébrité d' Adam Smith provient aussi de son travail et de la parution en dix sept cent cinquante neuf de la Théorie des sentiments moraux , oeuvre de philosophie qui le fait connaître en Grande Bretagne et même en Europe .
dans ce livre , il énonce les causes de l' immédiateté et de l' universalité des jugements moraux .
Smith affirme que l' individu partage les sentiments d' autrui par un mécanisme de sympathie .
Smith étend ce point de vue en évoquant un hypothétique spectateur impartial avec lequel nous serions en permanence en situation de sympathie .
on discute vite des thèses de ce livre un peu partout , et plus particulièrement en
Adam Smith , alors qu' il était professeur de logique , a écrit d' autres ouvrages qui ne seront publiés qu' après sa mort .
un des plus connus est son Histoire de l' astronomie .
l' histoire de l' astronomie à proprement parler ne représente qu' une petite partie de l' ouvrage , et s' arrête à Descartes , car en fait Smith s' intéresse davantage aux origines de la philosophie .
selon Smith , l' esprit prend plaisir à découvrir les ressemblances entre les objets et les observations , et c' est par ce procédé qu' il parvient à combiner des idées et à les classifier .
dans la succession des phénomènes constatés , l' esprit recherche des explications plausibles .
lorsque les sens constatent une succession qui rompt avec l' accoutumance de l' imagination , l' esprit est surpris , et c' est cette surprise qui l' excite et le pousse vers la recherche de nouvelles
objets isolés , s' efforce de mettre l' ordre dans ce chaos d' apparences discordantes , d' apaiser le tumulte de l' imagination , et de lui rendre , en s' occupant des grandes révolutions de l' univers , ce calme et cette tranquillité qui lui plaisent et qui sont assortis à sa nature .

les convictions religieuses d' Adam Smith ne sont pas connues avec précision , et il est souvent considéré comme un déiste à l' image de Voltaire qu' il admirait .
Ronald Coase a critiqué cette thèse et note que , bien que Smith fasse référence à un " grand architecte de l' univers " , à la Nature , ou encore à la fameuse surtout il explique que les merveilles de la nature attisent la curiosité des hommes , et que la superstition est la façon la plus immédiate de satisfaire cette curiosité , mais qu' à terme , elle laisse la place à des explications plus usuelles et donc plus satisfaisantes
l' ouvrage de Smith est remarqué par Charles Townshend , homme politique important et chancelier de l' Échiquier de dix sept cent soixante six à sa mort un an plus tard .
ce dernier avait épousé en dix sept cent cinquante quatre lady Caroline Campbell , veuve de lord Dalkeith , duc de Buccleuch , avec lequel elle a déjà deux fils .
Townshend cherche un tuteur pour le fils ainé de son épouse qui , comme tous les jeunes aristocrates anglais de l' époque , doit faire un Grand Tour , et propose à Smith