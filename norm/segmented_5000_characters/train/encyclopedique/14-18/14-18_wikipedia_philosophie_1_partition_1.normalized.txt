la philosophie s' est comprise très tôt comme une manière de vivre et non pas uniquement comme une réflexion théorique .
dit autrement : être philosophe , c' est aussi vivre et agir d' une certaine façon et non pas seulement se confronter à des questions abstraites .
l' étymologie du terme " philosophie " indique bien que le philosophe est celui qui tend vers la sagesse , qui cherche à vivre comme il le faut et plus particulièrement qui recherche le bonheur .
quinze la philosophie , entendue comme mode de vie , met l' accent sur la mise en application dans sa propre vie des résultats de la réflexion philosophique .
l' idée que la philosophie est une manière de vivre a aussi pu amener certains philosophes à imaginer que , pour cette raison , ils devaient guider les autres et les aider à mener correctement leurs existences .
la philosophie , d' éthique personnelle , pouvait se faire projet collectif voire politique .
ces ambitions " collectives " de la philosophie prennent différentes formes .
une véritable communauté de vie pouvait se constituer autour d' un philosophe .
ceci explique en partie la naissance dans l' Antiquité d' écoles philosophiques ( autour d' Épicure , de Platon ou d' Aristote par exemple ) .
depuis les présocratiques et surtout à partir de Socrate , toute une tradition a défendu cette conception de la philosophie comme un mode de vie .
citons entre autres les Stoïciens , Platon , Aristote , Épicure , Descartes , Spinoza , Sartre ou Russell .
mais ces derniers sont loin d' exclure l' idée que le philosophe s' intéresse à des problèmes théoriques .
la " sagesse " , ou plus exactement la sophia , que veut posséder le philosophe est aussi un savoir et une connaissance .
le philosophe , dans la lignée de la tradition fondée par Socrate , sait comment il doit vivre , il peut justifier ses choix et son mode de vie .
Socrate par exemple , dans les dialogues présocratiques de Platon , exige de ses interlocuteurs qu' ils soient à même de donner le logos de leur jugement de valeur et de leur choix , c' est à dire de les justifier rationnellement .
cette exigence de rationalité peut même amener à donner des fondements authentiquement scientifiques à la philosophie .
bien sûr la définition de la philosophie en tant que modus vivendi ( mode de vie ) ne peut prétendre être suffisante pour définir la philosophie dans son ensemble .
bien des philosophes ont compris la philosophie comme un travail intellectuel et non comme un mode de vie : c' est le cas dans le monde universitaire et de la recherche de nos jours .
il en va tout autrement , en Inde notamment .
le point de vue occidental ne peut s' appliquer aux concepts philosophiques en vigueur dans cette partie du monde , bien qu' il y eût tentative d' assimilation à l' époque romaine , en particulier avec Plotin .
l' on sait que lors des conquêtes d' Alexandre le Grand ( vers trois cent vingt cinq ) , les Grecs furent frappés par l' ascétisme hindou et le dénuement qui en résultait .
d' où leur appellation , fausse , de " gymnosophistes " ( de gumno , " nu " ) .
ces ascètes pratiquaient les préceptes des Upanishads .
à cette confrontation d' idées philosophiques intervient l' ethnophilosophie .
Maurice Merleau Ponty dans sa leçon inaugurale au Collège de France , intitulée Éloge à la philosophie , laisse entrevoir une conception de la philosophie comme mode de vie .
pour Pierre Hadot , dans La philosophie comme manière de vivre : " le vrai philosophe n' est pas celui qui parle , mais celui qui agit ( au quotidien ) " vingt deux .
" il y aurait place à nouveau dans notre monde contemporain , pour des philosophes ( sic ) , au sens étymologique du mot c' est à dire des chercheurs de sagesse , qui , ne renouvelleraient pas le discours philosophique , mais chercheraient ( ... ) une vie plus consciente , ( plus cohérente ( dit plus loin ) ) , plus rationnelle , plus ouverte sur les autres et sur l' immensité du monde . ( ... ) discours et vie ( philosophiques au quotidien ) sont inséparables " vingt trois .
" ( ... ) la concentration sur l' instant présent , l' émerveillement devant la présence du monde , le regard d' en haut ( concept qui lui est familier , et qu' il décline aussi en point de vue de Sirius ) porté sur les choses , la prise de conscience du mystère de l' existence " vingt quatre .
" ( ... ) s' efforcer à l' objectivité , à l' impartialité de l' historien et du savant , et aussi se détacher de son Moi pour s' ouvrir à une perspective universelle " vingt cinq .
" ( ... ) d' ouvrir notre coeur à tous les êtres vivants et à la nature entière dans sa magnificence " vingt six .
selon Georges Politzer , la philosophie du matérialisme scientifique , en devenant dialectique , s' identifie à une pratique au quotidien .
un des fondements de cette philosophie est la liaison étroite entre la théorie et la pratique .
c' est , pour lui , ce qui sépare le matérialisme des philosophes totalement idéel ( domaine de la pensée ) , non idéaliste autant que faire se peut , du matérialisme marxiste qui est aussi praxique ( dans le but de l' action , vie personnelle ainsi que vies sociale et politique ) .