en raison de ces traits particuliers , une distinction spécifique s' est lentement dessinée entre heavy metal et hard rock .
si les deux s' appuient sur la mise en avant des guitares et sur une structure à base de riffs , le heavy metal diffère tout particulièrement du hard rock dans le fait que les structures harmoniques et mélodiques du blues sont remplacées par des progressions modales et des relations tonales instables ( chromatisme , intervalles dissonants , progressions d' accords noyant l' orthodoxie
une autre particularité sont les célèbres power ballad .
typiquement , une power ballad s' ouvre sur un air doux et mélodique , à la guitare ou au synthétiseur .
plus tard dans la chanson , souvent après le refrain , intervient la batterie .
le volume sonore et l' effet dramatique qui l' accompagne augmente encore avec l' ajout d' une guitare électrique en distorsion .
la fin de la chanson peut
le hard rock aborde de nombreux thèmes tels que la science fiction , la fantasy , la libre pensée mais aussi des thèmes humanitaires , sociaux ou environnementaux ( cf par exemple Rush et Jimi Hendrix ) .
les chansons de Queen et de Led Zeppelin réemploient beaucoup d' airs qui traitaient de romance , d' amour non partagé ou de conquête sexuelle , des thèmes courants du rock , du pop et du blues .
certains textes , en particulier ceux de Led Zeppelin , ont été considérés comme misogynes .
beaucoup de chansons évoquent également la religion ( notamment celles d' Alice Cooper ) , la mort et la violence ( If You Want Blood ( You' ve Got It ) d' AC DC ) , la guerre ( Civil War des Guns N' Roses ) et les drogues , explicitement et implicitement comme
durant les années mille neuf cent soixante dix , à côté du hard rock , se développe un nouveau genre de rock dit " heavy metal " , représenté par Black Sabbath puis notamment Judas Priest .
en général , le hard rock s' inspire directement du blues et utilise des gammes pentatoniques mineures alors que le heavy metal s' oriente davantage vers des rythmiques lourdes et puissantes qui l' éloignent du shuffle .
certains groupes comme Judas Priest se sont radicalisés avec le temps , alors que leur musique au début de leur carrière pouvait parfois une confusion entre le hard rock et le heavy metal .
la distinction est parfois subtile .
beaucoup de groupes composent dans les deux genres comme Def Leppard et Moetley Cruee , tandis que d' autres comme AC DC et Aerosmith restent fidèles au hard rock .
la distinction entre hard rock et heavy metal est débattue depuis le milieu des années mille neuf cent quatre vingts : l' expression " heavy metal " est
Creem .
toutefois , cette distinction est sujette à controverses , Lester Bangs est décédé peu après avoir livré sa définition et n' a eu pas le temps de la défendre et de l' affiner .
selon lui , le hard rock se définit avant tout comme une musique de la période mille neuf cent soixante huit mille neuf cent soixante seize , le heavy metal , lui , se définit par un certain nombre d' attributs stylistiques ( look cuir , clous et badges ) et par les thèmes des chansons tels que l' horreur et la science fiction .
cependant , il existe des exceptions à la règle chronologique : guns N' Roses , groupe connu à la fin des années mille neuf cent quatre vingts , est généralement classé dans la catégorie hard rock .
le premier album de Black Sabbath , souvent considéré comme le premier album de heavy metal , sort dès mille neuf cent soixante neuf .
L Bangs lui même classe le groupe Scorpions dans le heavy metal alors que ses membres ne possèdent aucun des attributs qu' il évoque ( les membres de Scorpions préféraient clairement les foulards aux clous ) .
de ce fait , comme le précisent très justement Jean Marie Leduc et Jean Noël Ogouz , " la distinction entre hard
l' Encyclopedia of Rock et Roll de Rolling Stone ne fait pas de distinction entre hard rock et heavy metal .
seule l' expression " heavy metal " est définie et englobe les composantes hard rock et heavy metal en faisant référence à la chanson de Steppenwolf , Born to Be Wild un : " heavy metal thunder " .
l' encyclopédie de Rolling Stone journaliste chez Rolling Stone ) , sans en tenir compte .
l' expression
en France , le grand public ne connaissait jusque dans les années mille neuf cent quatre vingt dix que l' expression " hard rock " , " heavy metal " n' étant utilisé que par les connaisseurs .
depuis , le terme " metal "