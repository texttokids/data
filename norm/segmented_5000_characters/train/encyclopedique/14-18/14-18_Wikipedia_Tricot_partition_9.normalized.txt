tricoter des vêtements pour une distribution gratuite aux autres est de tradition courante dans les groupes de tricot .
les filles et les femmes ont tricoté des chaussettes , des chandails , des écharpes , des moufles , des gants et des bonnets pour les soldats en Crimée , pendant la guerre de Sécession , ou la guerre des Boers .
cette pratique a continué pendant les deux guerres mondiales , en Corée , et continue pour les soldats en Irak et en Afghanistan .
dans les entreprises historiques , les compagnies de laine fournissaient des patrons approuvés par les diverses armées .
les projets modernes impliquent usuellement des garnitures intérieures de casques .
ces garnitures doivent être à cent pour cents
un des projets modernes les plus réussis a été le projet chemo cap , démarré à Nazareth ( Pennsylvania ) en deux mille deux , en mémoire d' une femme décédée d' un cancer de l' utérus .
ce projet , comme d' autres tels que les headhuggers , jour sans cheveux , fournit des bonnets et autres couvre chefs pour les patientes en chimiothérapie .
comme pour les projets de garniture de casques , les compagnies de laine offrent gratuitement les recettes , la fibre doit être ici cent pour cents coton ou
il existe également des groupes de tricot qui confectionnent des modèles à destination des enfants prématurés ou mort nés , dont les parents ont généralement du mal à trouver des vêtements adaptés à leur
dans l' industrie , le fil métallique est aussi tricoté pour tout un ensemble d' applications , allant des filtres à cafetières aux convertisseurs catalytiques pour l' automobile , et encore bien d' autres usages .
ces étoffes sont généralement tricotées sur des machines à tricoter circulaires , analogues aux machines à chaussettes .
au Royaume Uni , une compagnie agro alimentaire a fait une campagne de publicité pour des céréales de petit déjeuner à la télévision en les présentant comme tricotées par des mamies .
la présentation mène le spectateur dans une usine mythique où chaque morceau de céréales est tricoté par une mamie .
puis elle explique que ce n' est pas facile de tricoter le bon gout du produit , parce que les céréales ont besoin du savoir faire des mamies .
entre les explications " techniques " qui sont données , des mamies qui bavardent sont admonestées par leur
en France et en suisse , c' est dans le cadre de l' opération " mets ton bonnet " que Phildar lance chaque année un appel au public pour que soient tricotés de minis bonnets destinés à orner les bouteilles de jus de fruit de la marque Innocent durant une période donnée : chaque bouteille vendue sous cette forme rapporte quelques centimes d' euros
dans ces dernières années , une pratique appelée Yarn bombing , ou bombage à la laine , utilisant de l' étoffe tricotée aux aiguilles ou au crochet pour modifier et orner le mobilier urbain ou un élément d' un décor champêtre , a émergé aux États Unis , et s' est répandue dans le monde .
les " Yarn bombeurs " se manifestent souvent dans les endroits symboliques de la culture graffiti ou du street art à cause de leurs renommées et d' une certaine exposition .
l' américaine Magda
dès les années mille neuf cent soixante , des artistes femmes américaines et européennes créent des oeuvres brodées , cousues , tissées , tricotées mettant en valeur un travail artisanal et créatif .
elles s' emparent de ces techniques comme moyen d' expression tout en affirmant leur identité .
dans les années mille neuf cent soixante , l' américaine Mary Walker Phillips deux noeuds utilise le tricot .
dans les années mille neuf cent soixante dix , Raymonde Arcier crée des objets tricotés représentants symboliquement l' asservissement des femmes à travers les tâches domestiques .
elle tricote ainsi un énorme pull en maille de
pensionnaires , une oeuvre qui présente un alignement de moineaux