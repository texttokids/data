pour sa première année à Oxford , Indira est une élève discrète mais appliquée qui ne participe guère à la vie sociale du collège : elle préfère rejoindre dès qu' elle en a l' occasion Feroze à Londres .
à l' été dix neuf cent trente huit , elle accompagne son père lors d' un voyage en Europe mais sa santé se détériore brusquement et elle est hospitalisée pour une pleurésie .
elle entreprend une cure de repos de plusieurs mois dans l' Himalaya puis dans les montagnes suisses , en attendant un retour à Oxford pour l' année scolaire dix neuf cent trente neuf dix neuf cent quarante .
mais elle est une nouvelle fois hospitalisée début octobre dix neuf cent trente neufet , sans poser de diagnostic définitif , les médecins lui recommandent un séjour dans un sanatorium pour malades de la tuberculose .
elle passe ainsi dix mois à Leysin dans les Alpes suisses , qu' elle quitte finalement , contre avis médical , fin octobre dix neuf cent quarantepour rejoindre Feroze à Londres en pleine Seconde Guerre mondiale ( elle prend d' abord la direction de Lisbonne , où elle reste coincée pendant deux longs mois ) .
les deux amants vivent ensuite ensemble dans la capitale britannique pendant trois mois avant d' embarquer pour Durban puis Bombay , qu' ils rejoignent
Dehra Dun , où son père est incarcéré , pour lui annoncer sa décision d' épouser Feroze Gandhi .
Nehru se montre réticent , mais ne s' oppose finalement pas au mariage , alors même que
l' indignation d' une partie de la société indienne .
le mariage est célébré le vingt six mars dix neuf cent quarante deuxen présence de Nehru , qui a deux mois au Cachemire .
un premier fils , Rajiv , nait en
des tensions apparaissent rapidement dans le couple .
Feroze est un mari volage qui , bien que menant une carrière journalistique et politique honorable ( il est élu député en dix neuf cent cinquante deux ) , supporte mal de vivre dans l' ombre des Nehru .
de son côté , Indira aurait eu une longue relation avec le secrétaire particulier de son père , M O Mathai deux noeuds .
le mariage est au plus bas lorsque Feroze est victime d' un premier accident cardiaque en septembre dix neuf cent cinquante huit , et la réconciliation qui s' ensuit est de courte durée .
lorsque , terrassé une nouvelle fois par un infarctus , Feroze meurt dans la matinée du huit septembre dix neuf cent soixante, Indira est à
de retour de leur voyage de noce , Indira et Feroze Gandhi prennent part au mouvement Quit India lancé par Nehru le huit août dix neuf cent quarante deux. la répression britannique est brutale : Nehru est arrêté et emprisonné dès le lendemain neuf août et Indira et Feroze un mois plus tard .
détenue sans être jugée , Indira est relâchée le treize mai dix neuf cent quarante troisaprès avoir passé huit mois derrière les barreaux .
sa santé et son moral connaissent des hauts et des bas , elle donne naissance à son premier fils le vingt août
la fin du conflit mondial marque le début du processus d' indépendance de l' Inde .
Nehru s' installe à Delhi , d' abord en tant que vice président du conseil exécutif il devient Premier ministre le quinze août dix neuf cent quarante sept, jour de l' indépendance de l' Inde .
Indira , qui vit à Lucknow où Feroze dirige un quotidien , rend de fréquentes visites à son père qu' elle aide à s' installer dans un premier temps dans un pavillon de York Road puis , après l' indépendance , à Teen Mûrti Bhavan deux noeuds , qui devient la résidence officielle du chef du gouvernement .
de plus en plus indispensable à son père , Indira joue notamment le rôle d' hôtesse lors des réceptions , alors que dans le même temps son mariage s' étiole .
elle finit par s' installer avec ses deux fils à Teen Mûrti Bhavan .
en dix neuf cent quarante neuf , Indira Gandhi se rend avec son père pour un voyage officiel aux États Unis , premier d' une liste de vingt quatre voyages effectués avec Nehru dans les dix années qui suivent .
lors des premières élections législatives organisées en dix neuf cent cinquante et un , elle soutient la candidature de Feroze qui est élu à Rae Bareli mais ne se porte pas candidate elle même , estimant être trop prise par son rôle auprès de son père et de ses enfants .
quatre ans plus tard , elle entre pourtant au comité exécutif du Congrès et participe à la conférence de Bandung avant de faire la preuve de ses capacités organisationnelles lors des élections législatives de dix neuf cent cinquante sept .
c' est sans doute cette même année qu' elle suit un traitement qui la guérit définitivement de la tuberculose .
le
Congrès malgré l' opposition de Nehru , mais elle choisit de ne
après une période de dépression et de deuil qui fait suite à la mort de Feroze , Indira retourne travailler auprès de son père , dont la fin approche .
la lutte pour la succession de Nehru au poste de Premier ministre est ouverte et le nom d' Indira commence à circuler .
lorsque Nehru décède en dix neuf cent soixante quatre , c' est finalement Lal Bahadur Shastri qui devient Premier ministre .
Indira accepte d' entrer au gouvernement comme ministre de l' Information , mais sa personnalité et son aura font bientôt d' elle un personnage clé du pouvoir .
elle se distingue notamment par ses interventions lors des la deuxième guerre indo pakistanaise la même année .