les chevaux sont des herbivores non ruminants .
ils disposent d' un seul estomac , capable de digérer les fibres végétales qui proviennent de l' herbe et du foin , grâce à une fermentation par micro organismes .
ce processus se déroule dans la partie du système digestif appelée caecum , et aboutit à la décomposition de la cellulose , principal composant des fibres végétales .
les chevaux préfèrent manger de petites quantités de nourriture de façon régulière tout au long de la journée .
cela n' est pas toujours compatible avec la vie dans les écuries , ni avec les plannings des humains , qui favorisent le nourrissage deux fois par jour .
le système digestif du cheval est délicat .
il est incapable de régurgiter sa nourriture , sauf depuis l' oesophage .
aussi , en cas d' excès de nourriture ou d' empoisonnement , vomir n' est pas
particulièrement long et complexe , l' équilibre de la flore intestinale dans le caecum peut être facilement bouleversé par des changements rapides d' alimentation .
ces facteurs le rendent sujet à des coliques , qui s' avèrent être la première cause de mortalité chevaline .
ils requièrent une nourriture propre et de grande qualité , fournie à intervalles réguliers , et peuvent tomber malades lorsqu' ils subissent un brusque changement de régime alimentaire .
les chevaux sont également sensibles aux moisissures et aux toxines .
pour cette raison , ils ne doivent jamais
article détaillé : cheval à sang chaud , Cheval à sang froid
les chevaux dits " à sang chaud " , comme ce Pur sang , sont fins ,
les chevaux étant des mammifères , ils ont toujours le sang chaud biologiquement parlant .
des termes tels que " cheval à sang chaud " , " cheval à sang froid " et " proche du sang " sont utilisés pour décrire le tempérament de l' animal .
le races dites " à sang chaud " sont surtout d' origine orientale
Turkoman ( maintenant éteint ) et les Pur sang développés à partir de ces derniers .
ils sont élevés pour leur agilité et leur vitesse , vifs , ils apprennent rapidement .
physiquement raffinés , leur peau est mince , leur silhouette longiligne , et leurs jambes longues .
ces races ont été importées en Europe depuis le Moyen Orient et l' Afrique du Nord lorsque les
la plupart des chevaux de trait , puissants et musclés , sont leur calme et leur patience , des qualités nécessaires pour tirer une charrue ou un lourd charriot rempli de passagers .
ils sont parfois surnommés les " doux géants " .
les races les plus connues incluent le trait belge et le Clydesdale .
certains , comme le Percheron , sont un peu plus légers et vifs .
d' autres , comme le lent et puissant Shire , sont créés pour labourer les champs aux sols lourds à base d' argile .
les chevaux à sang froid regroupent aussi quelques races de poneys comme le
le demi sang moderne est grand , mais agile et athlétique .
les demi sang ( ou Warmblood ) comme le Selle français , le Hunter irlandais , le Trakehner ou le Hanovrien , sont à l' origine des montures produites pour l' armée et issues du croisement de cheptels locaux à sang froid avec des chevaux à sang chaud , comme l' Arabe ou le Pur sang , afin d' obtenir un cheval ayant davantage de raffinement que le cheval de trait , mais aussi une plus grande taille et un tempérament plus calme que les chevaux de sang .
certains poneys demi sang ont été développés par croisement des cheptels locaux avec des chevaux de sang , par exemple le Connemara .
désormais , les termes " demi sang " et dominent les sports équestres olympiques du dressage et du saut d' obstacles depuis les années dix neuf cent soixante dix .
avant cette date , le terme français ( demi sang ) désignait tout croisement entre une race dite à sang froid et une race dite à sang chaud .
parfois , ce terme est utilisé pour faire référence à des races de chevaux légers autres
au début du XX E siècle , Hans le Malin le cheval
par le passé , les chevaux ont souvent été considérés comme des animaux stupides et incapables d' abstraction , soumis à leur seul instinct grégaire .
depuis le début du XX E siècle , des études ( et des faits comme l' affaire Hans le Malin ) ont mis en évidence leurs facultés cognitives dans la résolution d' un certain nombre de tâches quotidiennes , incluant la recherche de nourriture et la gestion de l' organisation sociale .
les chevaux sont également doués de bonnes habilités de visualisation spatiale .
ils sont capables de reconnaitre les humains qui les côtoient ( et de se reconnaitre entre eux ) à partir du simple son d' une voix ou des traits d' un