depuis le jubilé de la reine Élisabeth II un , la tour de l' Horloge se nomme la tour Élisabeth ( Elizabeth Tower ) .
c' est à tort que l' Elizabeth Tower est appelée parfois saint Stephen' s Tower .
durant le règne de la reine Victoria , les membres du Parlement tenaient leurs séances au saint Stephen' s Hall .
les journalistes citaient ou commentaient les comptes rendus du Parlement comme provenant de saint Stephen .
la dénomination s' était étendue au Parlement tout entier .
cet usage , qui trouvait sa justification au XIX E siècle n' a plus de raison d' être aujourd'hui .
quant aux termes de saint Stephen' s Tower , ils s' appliquent à saint Stephen' s Entrance , un bâtiment néogothique orné d' une tourelle à chaque angle et servant de hall d' accès ( saint Stephen' s Porch ) à la fois au Westminster Hall et au saint
manifestation sur la pelouse du jardin devant les houses of
quelques petits jardins entourent le palais de Westminster .
les jardins de la tour Victoria ( Victoria Tower Gardens ) , qui bordent la Tamise au sud du palais , font office de parc public .
le Black Rod' s Garden , appelé ainsi en référence à l' huissier de la Chambre des lords ( le Gentleman Usher of the Black Rod ) , est fermé au public et sert d' entrée privée .
la cour du Vieux Palais ( Old Palace Yard ) , face au palais , est pavée et recouverte par sécurité de blocs de béton .
d' autres espaces comme le Jardin Cromwell ( Cromwell Green ) sur le devant , la Cour du Nouveau Palais ( New Palace Yard ) et le jardin du Speaker ( Speaker' s Green ) au nord sont tous enclos et fermés au
le palais de Westminster compte environ un millier de pièces , une centaine d' escaliers et quatre , huit kilomètres de couloirs .
l' ensemble du bâtiment est organisé sur trois étages : le rez de chaussée abrite des bureaux , des salles à manger et des bars .
c' est le premier étage qui réunit la plupart des salles principales du palais , dont les deux chambres parlementaires , les vestibules et les bibliothèques .
on y trouve une perspective architecturale monumentale du fait de l' enfilade en droite ligne d' une longue série de pièces , à savoir la salle de Robe députés ( Members' Lobby ) et enfin la Chambre des communes .
le deuxième et le troisième étage sont occupés par les bureaux des
autrefois , en raison de son ancienne fonction de résidence royale , le palais était officiellement dirigé par le Grand Chambellan .
il a ensuite été décidé en dix neuf cent soixante cinq que chaque chambre parlementaire devrait pouvoir assurer le contrôle de ses propres pièces .
le speaker et le lord chancelier exercent ainsi leur autorité au nom de leurs assemblées respectives .
le Grand Chambellan , toutefois , conserve son emprise sur certaines salles de cérémonie .
la salle accueillant la Chambre des lords est située dans la partie sud du palais .
cette pièce , somptueusement aménagée , mesure vingt quatre , quatre
chambre , tout comme les autres meubles de la section du palais réservée aux lords , sont de couleur rouge .
la partie supérieure de la salle est agrémentée de vitraux et de six fresques allégoriques représentant la religion , la chevalerie et la loi .
d' or .
bien que le souverain puisse en théorie assister à n' importer quelle audience , il ou elle ne se déplace que pour les cérémonies d' ouverture du Parlement .
les autres membres de la famille royale présents à cette cérémonie utilisent des chaises d' État ( Chairs of State ) placées à proximité .
devant le trône se situe le Woolsack , simple coussin rouge rempli de laine sur lequel s' assied le Lord Speaker , président de la chambre .
le Woolsack des Juges ( Judges' Woolsack ) , un coussin rouge plus large occupé par les Law Lords lors des séances d' ouverture , et la table de la Chambre
les membres de l' assemblée siègent sur des bancs rouges répartis dans trois côtés de la salle .
les bancs situés à la droite du Woolsack forment le Côté spirituel ( Spiritual Sidé ) , et ceux de gauche le Côté temporel ( Temporal Sidé ) .
les Lords spirituels ( archevêques et évêques de l' Église d' Angleterre ) sont tous placés du Côté spirituel .
les Lords temporels ( membres de la noblesse ) , en revanche , s' organisent en fonction de leurs allégeances politiques : les membres du parti politique au pouvoir se rangent du Côté spirituel , tandis que les représentants de l' opposition s' assoient du Côté temporel .
certains pairs sans affiliation politique siègent sur les bancs du milieu de la salle , face au Woolsack .
ils sont de ce fait surnommés les cross benchers ( littéralement les " parlementaires de