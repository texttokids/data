le palais de Westminster ( à droite ) se situe au bord de la Tamise ,
en raison de sa situation privilégiée au bord de la Tamise , le palais de Westminster a revêtu une grande importance stratégique tout au long du Moyen Âge .
des bâtiments ont occupé ce site depuis au moins la période anglo saxonne : connu alors sous le nom d' île de Thorn ( Thorn Ey devenue Thorney Island ) , l' endroit pourrait avoir servi pour la première fois de résidence royale à l' époque de la
immédiatement à l' ouest de la cité londonienne et à peu près à la même époque que l' abbaye de Westminster voyait le jour ( entre mille quarante cinq et mille cinquante ) .
l' Île et ses environs prirent rapidement le nom de Westminster , en contraction des mots anglais West Monastery mille soixante six , Guillaume le Conquérant s' installa dans la tour de Londres , mais lui préféra vite Westminster .
il ne subsiste aujourd'hui aucune trace des bâtiments qui existaient à l' époque des Anglo Saxons et de Guillaume .
les plus anciennes sections subsistantes du palais , Westminster Hall et le Grand Hall , datent du règne du successeur de Guillaume le Conquérant , le roi Guillaume le deuxième le Roux .
le palais sur une carte de Londres réalisée en mille sept cent quarante six par John
le palais de Westminster fut la résidence principale des rois d' Angleterre jusqu'à la fin de l' ère médiévale .
beaucoup d' institutions publiques y virent le jour , en même temps qu' évoluait la nature du régime .
l' ancêtre du Parlement anglais , par exemple , le Curia Regis ( " conseil royal " ) , se réunissait à Westminster Hall , sauf lorsqu' il devait suivre le roi dans un autre palais .
quant au Parlement modèle ( Model Parliament ) , le premier parlement officiel d' Angleterre , il fut convoqué au palais par Édouard I er en mille deux cent quatre vingt quinze .
depuis lors , le palais a abrité , sauf à de rares
Westminster est resté la résidence londonienne principale des rois anglais jusqu'à ce qu' un incendie détruise une partie du bâtiment en
palais de York au détriment de son ancien propriétaire , le cardinal Thomas Wolsey , un puissant ministre tombé en disgrâce .
Henri rebaptisa l' endroit en palais de Whitehall , et l' utilisa par la suite comme résidence principale .
bien que Westminster reste officiellement un palais royal , il fut dès lors utilisé en tant que siège des deux chambres parlementaires et en tant que tribunal .
l' ancien palais de Westminster , par Thomas Malton le Jeune ( mille sept cent quarante huit
contenait aucune salle ayant vocation à accueillir les deux chambres : les cérémonies officielles telles que la cérémonie d' ouverture du Parlement se tenaient ainsi dans la Chambre peinte deux noeuds Chambre blanche deux noeuds ( White Chamber ) .
quant à la Chambre des communes , elle ne disposait d' aucune salle propre , ce qui la contraignait parfois à tenir ses débats à l' abbaye de Westminster , dans la salle capitulaire ou le réfectoire .
les Communes n' obtinrent un toit permanent que sous le successeur d' Henri le huitième , Édouard VI , lorsqu' on leur concéda l' usage de l' ancienne chapelle royale de Saint Étienne deux noeuds .
le Chantries Act de mille cinq cent quarante sept , passé dans le cadre de la Réforme protestante , avait en effet procédé à la dissolution de nombreux ordres religieux tels que celui des chanoines de Saint Étienne , ce qui permit aux Communes de trouver à se loger .
des aménagements furent ensuite réalisés dans l' ancienne chapelle pour satisfaire aux besoins de la chambre basse .
J M W Turner assista à l' incendie de mille huit cent trente quatre et réalisa plusieurs toiles sur le thème , notamment cet Incendie des Chambres du Parlement ( The Burning of the Houses of Parliament , mille huit cent trente cinq ) .
le seize octobre mille huit cent trente quatre , la majeure partie du palais disparut en fumée lors d' un incendie : l' incendie du Parlement .
seuls Westminster Hall , la tour des Joyaux , la crypte de la chapelle Saint Étienne et les cloitres échappèrent à la destruction .
une commission royale fut désignée afin d' étudier les options s' offrant pour la reconstruction , et parvint à la conclusion que le nouveau palais devrait être reconstruit sur le même site dans un style soit gothique , soit classique .
cette alternative ne fut pas sans provoquer de vifs débats publics .
les partisans du classicisme avancèrent que l' architecture gothique était trop crue , ou en tout cas peu appropriée à un Parlement .
beaucoup cependant , dont Augustus Pugin , soutinrent que le gothique représentait la plus authentique architecture chrétienne , allant jusqu'à comparer par contraste le classicisme avec le paganisme de la Rome et de la Grèce antiques .
l' art gothique était considéré par ailleurs comme typiquement national , à l' inverse du classicisme qu' on
en mille huit cent trente six , après l' examen de quatre vingt dix sept propositions rivales , la commission royale opta pour l' architecte Charles Barry et son projet de palais en style néogothique .
la première pierre fut posée en mille huit cent quarante , puis la Chambre des lords fut achevée en mille huit cent quarante sept et la Chambre des communes en mille huit cent cinquante deux , date à laquelle Barry reçut le titre de chevalier .
la plupart des travaux ont été réalisés avant mille huit cent soixante , mais certains éléments ne furent pas terminés avant la