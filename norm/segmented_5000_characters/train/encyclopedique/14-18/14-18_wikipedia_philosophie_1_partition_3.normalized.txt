malgré les difficultés que comporte cette entreprise , il est possible de distinguer certaines grandes caractéristiques positives de la méthode philosophique .
la philosophie se comprend comme un travail critique .
c' est une de ses définitions les plus courantes .
cette critique n' est cependant jamais purement et simplement négative .
elle a pour but de créer de nouvelles certitudes et de corriger les fausses évidences , les illusions et erreurs du sens commun ou de la philosophie elle même .
Socrate , par exemple , interrogeait ses contemporains et les sophistes afin de leur montrer leurs contradictions et leur incapacité à justifier ce qui leur semblait évident .
Descartes est à l' époque moderne le meilleur représentant de cette conception de la philosophie , car , selon lui , seul un doute radical et général pouvait être le fondement d' une pensée parfaitement rigoureuse et indubitable .
la philosophie est souvent caractérisée comme un travail sur les concepts et notions , un travail de création de concepts permettant de comprendre le réel , de distinguer les objets les uns des autres et de les analyser , mais aussi un travail d' analyse des concepts et de leurs ambigüités .
elle a très tôt reconnu les problèmes que posent les ambigüités du langage .
de nos jours la philosophie analytique donne elle aussi une grande place à ce problème .
en outre , à la différence des sciences , la délimitation des méthodes et du domaine de la philosophie fait partie de la philosophie elle même .
chaque penseur se doit d' indiquer quels problèmes il souhaite éclairer , et quelle sera la méthode la plus adaptée pour résoudre ces problèmes .
il faut en effet bien voir qu' il y a une unité profonde des problèmes philosophiques et de la méthode philosophique .
il ne faut donc pas voir l' instabilité des méthodes et des thèmes philosophiques comme une faiblesse de la discipline , mais plutôt comme un trait caractéristique de sa nature .
ainsi , la philosophie est une sorte de retour critique du savoir sur lui même , ou plus précisément une critique rationnelle de tous les savoirs ( opinions , croyances , art , réflexions scientifiques , et caetera ) , y compris philosophiques puisque réfléchir sur le rôle de la philosophie c' est entamer une réflexion philosophique .
Adorno et Horkheimer : deux représentants de la critique marxiste de la rationalité moderne .
enfin , la philosophie est une discipline déductive et rationnelle .
elle n' est pas simple intuition ou impression subjective , mais demeure inséparable de la volonté de démontrer par des arguments et déductions ce qu' elle avance : elle est volonté de rationalité .
c' est même la rupture des présocratiques avec la pensée religieuse ( mythologie ) de leur époque , et leur rapport aux dieux grecs qui est considérée traditionnellement comme le point marquant de la naissance de la philosophie .
ce souci de démontrer et de livrer une argumentation se retrouve au cours de toute l' histoire de la philosophie .
qu' on songe aux discussions éristiques durant l' Antiquité , à l' intérêt que portent les philosophes à la logique depuis Aristote , mais aussi , au Moyen Âge , au souci de donner à la philosophie la rigueur démonstrative des mathématiques ( comme chez Descartes ou Spinoza ) ou à l' importance qu' accorde la philosophie analytique de nos jours à la rigueur et à la clarté argumentatives .
malgré cette tendance profonde , la philosophie contemporaine a vu se développer une critique radicale de la raison , que ce soit chez Nietzsche , Heidegger , ou encore Adorno : la rationalité même s' est donc trouvée mise en débat par la philosophie .
les branches de la philosophie occidentale La philosophie est loin d' être un domaine de connaissances bien délimité au sens où les problèmes auxquels elle se confronte sont d' une extrême variété .
elle étudie de nombreux objets , certains proches , c' est pourquoi sa subdivision en différentes branches est problématique et relève de l' arbitraire .
de plus , si des pans entiers de la philosophie sont apparus au XXe siècle , certains domaines se sont détachés très nettement de la philosophie à l' époque moderne .
la physique , par exemple , était considérée comme appartenant à la philosophie jusqu' au XVIIIe siècle .
mais le détachement n' est pas toujours aussi net , ainsi la science politique , considérée comme une ancienne branche de la philosophie devenue autonome , entretient un dialogue permanent avec la philosophie politique ( qui n' est donc pas morte ) .
de même , la biologie , qui a longtemps été entravée par son appartenance à la philosophie avec les thèses finalistes , mécanistes , et vitalistes , revient par une porte dérobée .
en effet , au début du XXIe siècle le développement des biotechnologies a pour corollaire l' apparition d' un nouveau champ d' étude philosophique : la bioéthique .