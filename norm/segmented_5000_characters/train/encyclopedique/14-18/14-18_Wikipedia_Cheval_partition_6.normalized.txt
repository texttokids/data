il existe aussi des variations régionales dans l' ADN mitochondrial , dues à l' inclusion a posteriori de juments sauvages parmi des hardes
une autre conséquence de la domestication est une augmentation de la variabilité des robes , chez le cheval , notamment entre
mosaïque romaine d' une course de char , Sicile , III E IV E siècle .
statère en électrum de Zeugitane représentant un cheval
en Europe , les Grecs , Romains et Byzantins utilisaient le cheval pour la guerre , les communications , le transport mais aussi
vénéraient Épona , déesse des chevaux , dont le culte nous a romaines .
au Moyen Orient , certaines tribus Perses semblent avoir
chevaux du désert , robustes et élégants , ils inventent aussi le polo .
lorsque les Hyksôs envahissent l' Égypte au XVII E siècle avant notre ère , les égyptiens n' utilisaient les chevaux que pour des tâches civiles .
la cavalerie , qui fera la puissance des pharaons du Nouvel Empire , était alors du côté de l' ennemi et sera un facteur déterminant dans la défaite égyptienne .
en Afrique , la cavalerie numide est une unité importante des armées carthaginoises lors des guerres puniques tandis que la
en Asie , le plus ancien char hippomobile à nous être parvenu intact provient de la tombe de l' empereur chinois Wu Ding , mort en mille millecent dix huit avant J C Le cheval était peu utilisé comme animal de trait dans l' agriculture mais les chinois seraient à l' origine du collier d' épaule .
ils utilisèrent l' étrier au VI E siècle avant notre ère , la cavalerie formant le gros des troupes chinoises .
le cheval ( ) sert de moyen de transport et de communication ( coursier ) .
quand le jeu de polo perse arriva à la cour de l' empereur , tout le monde s' en éprit .
les chinois ne faisant pas d' élevage permanent des chevaux , ces derniers restaient un produit de luxe importé du
au Japon , le cheval sert d' animal de combat , de coursier et au transport de marchandises , mais dans ce dernier cas il est guidé par des hommes à pied , ce qui limite son potentiel .
des peuples d' Asie ont développé une unité militaire originale qui est
au Moyen Âge , des types spécifiques d' animaux sont développés .
le destrier est le plus connu , à travers l' image d' un énorme animal bardé de fer associé à son chevalier en armure complète , mais la réalité historique est plus nuancée .
les prestigieuses et puissantes montures de guerre portent le chevalier en armure , son
est également utilisé pour la guerre .
les chevaux de prestige et de parade , dits " palefrois " , sont réputés très coûteux , tout comme la haquenée , jument des dames fortunées .
le roussin , de moindre valeur , sert occasionnellement de monture aux chevaliers les plus pauvres ou de cheval de bât .
l' utilisation du cheval pour la traction est accrue par la diffusion du collier d' épaule en Europe au XII E siècle , permettant au cheval de trait de remplacer
agricoles .
les chevaux médiévaux sont nommés d' après leur lieu d' origine , par exemple " cheval espagnol " , mais ce terme se référait peut être à plusieurs races .
d' importants progrès technologiques , comme l' amélioration des selles , l' arrivée de l' étrier , du collier d' épaule et du fer à cheval permettent des changements capitaux dans l' équipement équestre , pour la
au Moyen Orient , les chevaux portent les cavaliers islamiques jusqu' en Espagne et des échanges culturels ont lieu à l' occasion des croisades et des invasions maures .
huit croisades , entre mille quatre vingt dix sept et mille trois cent , font se rencontrer deux cultures équestres radicalement différentes , les chevaliers chargeant lourdement et essayant de désarçonner leurs adversaires , les Bédouins cherchant à tailler
principale force des armées mongoles et tartares .
les académies d' équitation privilégient le dressage du cheval .
ici
canon entraine la fin de la cavalerie lourde et une nouvelle sélection du cheval de guerre .
des académies d' équitation sont créées , d' abord en Italie , pour obtenir des chevaux plus maniables .
l' école espagnole de Vienne est construite dès mille cinq cent soixante douze , et les Habsbourg fondent le haras berceau d' élevage
l' idée de mieux sélectionner les chevaux de guerre fait son chemin sous François I er , et le dix sept octobre seize cent soixante cinq , Colbert ordonne la création des haras nationaux .
au XVIII E siècle , la création de haras , d' écuries et d' écoles de dressage renforce la renommée des chevaux royaux , devenus plus légers et plus souples .
à la veille de la Révolution française , l' État possède quinze haras nationaux et près de sept cent cinquante reproducteurs .
ces haras sont supprimés par l' assemblée