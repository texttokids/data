pendant de nombreuses années , les circonstances et l' histoire de la mort de Billy the Kid n' ont eu pour source que le livre de Pat Garrett , The Authentic Life of Billy the Kid .
cette seule source controversée a alimenté des hypothèses selon lesquelles Billy the Kid n' aurait pas été tué par Pat Garrett .
ces hypothèses sont étayées par le fait que certaines personnes n' ont pas reconnu le corps du défunt et que celui ci présentait une forte pilosité ( alors que le " kid " était imberbe ) , .
John Poe , l' un des adjoints du shérif , que Garrett ne croyait pas à l' origine que le " kid " se trouvait à Fort Sumner .
d' après Poe , Garrett aurait reconnu le " kid " à sa voix et aurait tiré ensuite , et il est le seul à avoir certifié que le corps de la personne qu' il avait abattue était celui de Billy the Kid , puisque ni Poe ni Thomas McKinney ( l' autre adjoint ) n' avaient déjà vu le hors la loi .
le coroner n' a d' ailleurs jamais vu le corps et il n' existe aucun document officiel stipulant que le " kid " est mort .
certaines personnes ont prétendu par la suite avoir vu ou avoir été en contact avec Billy the Kid après le quatorze juillet .
il semblerait aussi qu' il existe deux preuves d' arrestations de Billy the Kid après la date de sa mort
de plus , il semblerait que Pat Garrett n' ait jamais touché la prime de cinq cents dollars , par manque de preuves sur l' identité de l' homme abattu .
en mille neuf cent quatre vingt trois , la fille de Pat Garrett a déclaré que son
en mille neuf cent cinquante , un certain Brushy Bill Roberts déclara être le vrai Billy
le trente et un décembre deux mille dix, Bill Richardson , gouverneur du Nouveau Mexique , a déclaré qu' il ne gracierait pas Billy the Kid à titre posthume , n' étant pas en mesure de s' assurer avec certitude des motifs qui avaient poussé son lointain prédécesseur Lew Wallace à
pendant de longues années , on a pensé que Billy The Kid était gaucher du fait que sur la seule photo reconnue par les historiens , il portait
néanmoins , cette célèbre photo était en fait la reproduction en veste ou la boucle de sa ceinture , qui sont orientés du mauvais côté .
cela accrédite le fait que Billy the Kid était
en mille neuf cent cinquante , un avocat de Floride nommé William Morrison a localisé un homme du nom de Ollie P Roberts , surnommé Brushy Bill , qui avoua être Billy the Kid ( il nia dans un premier temps ) et qui voulait faire une demande d' amnistie pour ses crimes .
il déclara ne pas avoir enterré à Fort Sumner .
selon l' opinion générale , cette option est peu probable mais toujours débattue car , sans William Morrison , Brushy Bill serait mort dans l' anonymat sans s' être expliqué .
la ville de Brushy Bill , Hico au Texas , a misé sur cette histoire en ouvrant un musée .
dans les années mille neuf cent trente , un autre individu , de Prescott en Arizona , affirma également être Billy the Kid .
le tourisme autour de Billy the Kid est en pleine croissance dans
Scenic Byway est une boucle routière située entre les villes de Ruidoso , Capitan , Fort Stanton , Lincoln , Hondo et San Patricio .
il est possible également de parcourir à cheval la même randonnée que Billy , au mois de juillet de chaque année .
photo ( controversée ) de Billy the Kid ( à gauche ) lors d' un jeu de
seules trois photographies de Billy the Kid adulte sont connues .
la première prise en mille huit cent quatre vingts devant un saloon du Nouveau Mexique alors qu' il avait vingt ans .
mesurant cinq centimètres sur huit , la photo est imprimée sur une plaque de métal .
payée à l' époque vingt cinq cents par Billy the Kid , elle a été adjugée le vingt cinq juin deux mille onzeaux enchères du
la deuxième achetée deux dollars dans une brocante a été mise en vente par une société de numismatique de San Francisco , qui a mis un an à l' authentifier .
sur la photo qui mesure dix centimètres sur treize , on peut voir le jeune bandit poser lors d' un jeu de croquet au Nouveau Mexique en mille huit cent soixante dix huit , avec des membres de son gang appelé les Lincoln County Regulators .
Billy the Kid apparait arborant un petit sourire , son maillet posé sur le sol .
il porte un chapeau noir , qui ressemble à une version plus courte du couvre chef de l' ex président Abraham Lincoln .
il se tient devant une maisonnette en bois , avec à sa gauche une femme en jupe longue tenant dans ses bras
la troisième a été achetée dix dollars par un homme qui voulait s' en servir pour décorer son gîte .
après une émission sur The Kid , il est pris de doutes et fait expertiser sa photo par le professeur Stahl .
après contre expertise , il est certain que le deuxième homme à partir de la gauche est bien Billy the Kid .
autre coup de chance , l' homme à droite sur l' image est Pat Garrett , son meilleur ami , et shérif responsable de sa mort .
elle est estimée à entre trois et cinq millions
il existe au moins deux autres photos de Billy the Kid enfant : l' une prise lorsqu' il est élève à l' école de Silver City et l' autre prise parmi six cow boys dans un ranch près de Roswell .
Miller et Frank Borzage un , avec Robert Taylor
avec Audie Murphy , Gale Storm , Albert Dekker , Will Geer Berké un , avec Don Barry ( Billy the Kid ) et Robert