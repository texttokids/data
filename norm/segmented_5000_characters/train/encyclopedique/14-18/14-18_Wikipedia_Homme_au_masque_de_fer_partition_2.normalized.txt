le prisonnier était arrivé à Pignerol le vingt quatre août seize cent soixante neuf .
dès le dix neuf juillet , Louvois avait écrit à Saint Mars à propos du prisonnier qu' il lui envoyait : " il est de la dernière importance qu' il soit gardé avec une grande sûreté et qu' il ne puisse donner de ses nouvelles en nulle manière et par lettre à qui que ce soit de faire en sorte que les jours qu' aura le lieu où il sera ne donne point sur des lieux qui puissent être abordés de personne et qu' il y ait assez de portes , fermées les unes sur les autres , pour que vos sentinelles ne puissent rien entendre . il faudra que vous portiez vous même à ce misérable , une fois par jour , de quoi vivre toute la journée et que vous n' écoutiez jamais , sous quelque prétexte que ce puisse être , ce qu' il voudra vous dire , le menaçant toujours de le faire mourir s' il vous ouvre jamais la bouche pour vous parler d' autre chose que de ses nécessités " .
en seize cent quatre vingt onze , lorsque Louvois meurt , son fils , Barbezieux , qui lui succède , écrivit à Saint Mars pour confirmer ces instructions : sous votre garde depuis vingt ans , je vous prie d' user des mêmes précautions que vous faisiez quand vous suiviez à Monsieur de Louvois .

le prisonnier a enflammé les imaginations .
en réalité , rien ne permet de penser que le prisonnier était constamment masqué .
il semble plus probable qu' il n' a été astreint à porter un masque que pendant les transferts , pour éviter qu' un passant puisse le reconnaitre .
l' hypothèse d' une prothèse a même été évoquée .
des scientifiques ont par ailleurs expliqué qu' il n' a pas pu porter ce masque constamment pour la bonne et simple raison qu' il aurait entrainé des maladies , comme une septicémie .
de plus il s' agissait d' un homme , donc la repousse des poils aurait eu lieu dans de mauvaises
encore le port d' un masque n' est il véritablement avéré qu' en seize cent quatre vingt dix huit , lors du transfert à la Bastille : il est mentionné dans le registre de Du Junca ( voir ci dessus ) ainsi que dans un récit ( publié dans l' Année littéraire le trente juin dix sept cent soixante dix huit ) de l' étape de Saint Mars dans son
gouvernement des Isles Sainte Marguerite à celui de la Bastille .
en venant en prendre possession , il séjourna avec son prisonnier à sa terre de Palteau .
l' homme au masque arriva dans une litière qui précédait celle de Monsieur de Saint Mars , ils étoient accompagnés de plusieurs gens à cheval .
les paysans allèrent au devant de leur seigneur , Monsieur de Saint Mars mangea avec son prisonnier , qui avait le dos opposé aux croisées de la salle à manger qui donnent sur la cour , les paysans que j' ai interrogés ne purent voir s' il mangeait avec son masque , mais ils observèrent très bien que Monsieur de Saint Mars , qui était à table vis à vis de lui , avoit deux pistolets valet de chambre , qui allait chercher les plats qu' on lui apportait dans l' anti chambre , fermant soigneusement sur lui la porte de la salle à manger .
lorsque le prisonnier traversait la cour , il avoit toujours son masque noir sur le visage , les paysans remarquèrent qu' on lui voyait les dents et les lèvres , qu' il était grand et avait les cheveux blancs .
Monsieur de Saint Mars coucha dans un lit qu' on lui avait dressé auprès de celui de l' homme au masque .

selon Émile Laloy , auteur du livre Le Masque de fer : jacques Stuart de la Cloche , l' Abbe Prignani , Roux de Marsilly un , Louis le quinzième est
connaissance de ce grand secret : louis seize l' ignorait complètement , son premier ministre , Malesherbes , fit faire des recherches dans les archives de la Bastille pour l' élucider , Chevalier , major de cette prison , en envoya le dix neuf novembre dix sept cent soixante quinze le résultat au ministre : il n' avait rien trouvé au delà de ce qu' on savait déjà .
d' après une tradition communiquée par M me d' Abrantès à Paul Lacroix , Napoléon aurait désiré vivement connaître le secret de l' énigme .
il ordonna des recherches qui restèrent sans résultat , ce fut en vain que pendant plusieurs années le secrétaire de Monsieur de Talleyrand fureta dans les archives des Affaires étrangères et que Monsieur le duc de Bassano appliqua toutes les lumières de son esprit judicieux
Michel Chamillart , ministre de la guerre en dix sept cent trois , connaissait aussi ce secret .
son gendre , le duc de La Feuillade , essaya de
le second maréchal de La Feuillade , son gendre , m' a dit qu' à la mort de son beau père , il le conjura à genoux de lui apprendre ce que c' était que cet homme , qu' on ne connut jamais que sous le nom de l' homme au masque de fer .
Chamillart lui répondit que c' était le secret de l' État , et qu' il avait fait serment de ne le révéler jamais .

selon l' historien Emmanuel Pénicaut , auteur d' une biographie de Michel Chamillart , " une tradition familiale veut que le secret ait été transmis de père en fils dans la famille Chamillart jusqu'à la mort du dernier porteur du nom , Lionel Chamillart , en dix neuf cent vingt six . "