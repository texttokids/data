Einstein est lié à de nombreuses causes pacifistes , car il se montre ouvert aux propositions multiples de soutien qu' il reçoit , et accepte souvent de s' engager pour les causes qu' il juge justes .
Einstein apporte un soutien marqué aux mouvements sionistes .
en mille neuf cent vingt , il accompagne ainsi le chef de file sioniste Chaim Weizmann aux États Unis au cours d' une campagne de récolte de fonds .
il se rend également en Palestine mandataire dans le cadre de l' inauguration de l' université hébraïque de Jérusalem à laquelle il lègue plus tard ses archives personnelles .
ses apparitions donnent un prestige politique à la cause sioniste .
à la suite d' une invitation à s' établir à Jérusalem , il écrit dans son carnet de voyage que " le coeur dit oui ( ... ) mais la raison dit non " .
selon Tom Segev , Einstein apprécie son voyage en Palestine et les honneurs qui lui sont faits .
il marque néanmoins sa désapprobation en voyant des Juifs prier devant le mur des Lamentations , Einstein commente qu' il s' agit de personnes collées au passé et faisant abstraction du présent .
ben Gourion lui propose en mille neuf cent cinquante deux la présidence de l' État d' Israël , qu' il refuse :
" j' ai passé ma vie à étudier des problèmes objectifs et je manque à la fois de l' aptitude naturelle et de l' expérience nécessaires pour traiter des problèmes humains et exercer des fonctions officielles . "
il a une vision clairvoyante de sa situation entre les deux guerres .
il écrit dans une remarque à la fin d' un article écrit pour le Times de Londres :
" je passe actuellement en Allemagne pour un savant allemand et en Angleterre pour un juif suisse . supposons que le sort fasse de moi une bête noire , je deviendrai au contraire un juif suisse en Allemagne , et un savant allemand en Angleterre . "
il reçoit des menaces de mort dès mille neuf cent vingt deux .
de violentes attaques ont lieu contre sa théorie de la relativité en Allemagne et en Russie .
Philipp Lenard , " chef de la physique aryenne ou allemande " attribue à Friedrich Hasenoehrl la formule E égale deux pour en faire une création aryenne .
Einstein démissionne de l' académie de Prusse en mille neuf cent trente trois , et il est exclu de celle de Bavière .
en mars mille neuf cent trente trois, en tant que président d' honneur de la Ligue contre l' antisémitisme , il lance un appel aux peuples civilisés de l' univers , tâchant " d' éveiller la conscience de tous les pays qui restent fidèles à l' humanisme et aux libertés politiques " , dans cet appel il s' élève contre " les actes de force brutale et d' oppression contre tous les gens d' esprit libre et contre les juifs , qui ont lieu en Allemagne " .
cette année là , Einstein est en voyage à l' étranger , et il choisit de ne pas revenir en Allemagne , où Hitler a pris le pouvoir en janvier .
après un séjour en Belgique , il décline une proposition de la France de l' accueillir comme professeur au Collège de France , et part pour les États Unis , à Princeton .
le deux août mille neuf cent trente neuf, il signe une lettre , rédigée par les physiciens Léo Szilard et Eugène Wigner , destinée à Roosevelt , qui aurait pu contribuer à enclencher le projet Manhattan ceci étant à l' opposé de l' intention d' origine de la lettre , qui ne se voulait que préventive des risques potentiels que les récentes découvertes scientifiques pourraient causer ( celles ci permettraient en effet la réalisation de " bombes d' un nouveau type et extrêmement puissantes " ) .
après la guerre , Einstein milite pour un désarmement atomique mondial , jusqu' au seuil de sa mort en mille neuf cent cinquante cinq , où il confesse à Linus Pauling : " j' ai fait une grande erreur dans ma vie , quand j' ai signé cette lettre ( de mille neuf cent trente neuf ) . "
après la Seconde Guerre mondiale , son engagement vis à vis des communautés juives et Israël est nuancé par ses opinions pacifistes .
il préface le Livre noir , recueil de témoignages sur l' extermination des juifs en Russie par les nazis pendant la guerre .
et en décembre mille neuf cent quarante huit, il cosigne une lettre condamnant le massacre de Deir Yassin commis par des combattants israéliens de l' Irgoun et du Lehi pendant la guerre de Palestine de mille neuf cent quarante huit .
pendant la guerre froide , il s' exprime contre la course aux armements et appelle , par exemple avec Bertrand Russell et Joseph Rotblat , les scientifiques à plus de responsabilités , les gouvernements à un renoncement commun à la prolifération des armes atomiques et à leur utilisation et les peuples à chercher d' autres moyens d' obtenir la paix ( création du Comité d' urgence des scientifiques atomistes en mille neuf cent quarante six , manifeste Russell Einstein en mille neuf cent cinquante quatre ) .
Einstein s' est exprimé sur ses convictions socialistes en mille neuf cent quarante neuf , en pleine période du maccarthysme , dans un essai intitulé Pourquoi le Socialisme , publié dans la Monthly Review :