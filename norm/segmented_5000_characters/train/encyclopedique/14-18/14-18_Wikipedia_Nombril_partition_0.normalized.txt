les deux mots " nombril " et " ombilic " sont issus du latin .
ombilic a été emprunté au latin classique umbilicus , et est passé par la forme embelic .
nombril a évolué depuis le latin populaire umbiliculus , et est passé par umblil un puis lonblil ( Chrétien de Troyes ) ou nomblil , par agglutination initiale de l' article dissimilation .
les appellations de bédille ( cordon ombilical ) et badine ( nombril ) , de même origine que boyau , boudin ( du latin bodellus ) étaient également utilisées anciennement .
le nombril est une cicatrice fibreuse située au niveau de la paroi antérieure de l' abdomen , formée par la chute du cordon ombilical
elle apparait à la suite de la chute du cordon ombilical chez les nouveau nés appartenant à la classe des mammifères placentaires .
c' est le vestige de la circulation foetale et ,
au niveau de la face externe ( antérieure ) de cette paroi , il est situé médialement , au milieu de la ligne blanche , entre les deux muscles droits .
au niveau de la face interne ( postérieure ) de la paroi abdominale antérieure , le nombril est relié en haut au foie par le ligament rond du foie , et en bas à la vessie par le ligament ombilical médian .
de chaque côté de ce ligament se trouvent les plis ombilicaux médiaux qui relient le nombril aux parois latérales du petit bassin .
le ligament rond est le vestige de la veine ombilicale , le ligament ombilical médian est le vestige de l' ouraque et les plis ombilicaux médiaux sont les vestiges des
le nombril est situé au niveau du disque entre les troisième et quatrième vertèbres lombaires .
en médecine , il constitue un point de repère important .
le nombril est innervé par la dixième paire de
chez l' humain , les nombrils sont extrêmement variés autant en taille , qu' en forme , qu' en profondeur ou encore qu' en apparence globale .
la cicatrice peut apparaître sous la forme d' une dépression ou d' une protubérance , d' un diamètre variant entre un et deux centimètres .
la
en tant que cicatrice , l' ombilic n' est pas un caractère héréditaire .
ainsi , le nombril est un trait de caractère permettant de
chez l' enfant , la hernie ombilicale est un défaut bénin de fermeture du nombril .
chez l' adulte , la paroi abdominale présente dans le voisinage du nombril une zone de faiblesse , pouvant être le siège
les anomalies de fermeture de la paroi antérieure telles que le laparoschisis et l' omphalocèle sont des pathologies plus
châtiment divin , mutilation qui nous a séparés d' une part de nous
par les théologiens juifs dès le Moyen Âge , Adam et Ève seraient dépourvus de l' ombilic car ils n' avaient pas de mères qui auraient accouché d' eux .
le peintre Jean Baptiste Santerre en l' Évangile de Barnabé , Jésus raconte que le nombril d' Adam est la trace du crachat de Satan sur la glaise dont Dieu se servit ,