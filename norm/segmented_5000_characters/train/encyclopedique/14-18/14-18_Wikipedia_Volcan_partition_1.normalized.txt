bombe volcanique sur un lit de tephras ( cendres et scories ) sur les pentes du Capelinhos aux Açores ,
le plus souvent , les matériaux volcaniques sont composés de tephras , ce sont les cendres volcaniques , les lapilli , les scories , les pierres ponces , les bombes volcaniques , les blocs rocheux ou basaltiques , les obsidiennes , et caetera Il s' agit de magma et de morceaux de roche arrachés du volcan qui sont pulvérisés et projetés parfois jusqu'à des dizaines de kilomètres de hauteur dans l' atmosphère .
les plus petits étant les cendres , il leur arrive de faire le tour de la Terre , portées par les vents dominants .
les bombes volcaniques , les éjectas les plus gros , peuvent avoir la taille d' une maison et retombent en général à proximité du volcan .
lorsque les bombes volcaniques sont éjectées alors qu' elles sont encore en fusion , elles peuvent prendre une forme en fuseau lors de leur trajet dans l' atmosphère , en bouse de vache lors de leur impact au sol ou en croûte de pain en présence d' eau .
les lapilli , qui ressemblent à de petits cailloux , peuvent s' accumuler en épaisses couches et former ainsi la pouzzolane .
les pierres ponces , véritable mousse de lave , sont si légères et contiennent tellement d' air qu' elles peuvent flotter sur l' eau .
enfin quand de fines gouttes de laves sont éjectées et portées par les vents , elles peuvent s' étirer en de longs filaments appelés " cheveux de
les matériaux émis proviennent d' un magma .
le magma est de la roche fondue située dans le sous sol et contenant des gaz dissous qui seront libérés lors de la progression du liquide et à cause de la baisse de pression qui en découle .
lorsque le magma arrive en surface
le magma a une consistance fluide à visqueuse .
il s' est formé à partir de la fusion partielle du manteau ou plus rarement de
roches conséquente à des mouvements tectoniques .
généralement , ce magma remonte vers la surface en raison de sa densité plus faible et se stocke dans la lithosphère en formant une chambre magmatique .
dans cette chambre , il peut subir une cristallisation totale ou partielle et ou un dégazage qui commence terrains qui le recouvrent deviennent insuffisantes pour le contenir , il remonte le long d' une cheminée volcanique ( où la baisse de pression due à la remontée produit un dégazage qui diminue encore la densité de l' émulsion résultante ) pour être émis sous forme de lave , c' est à dire totalement ou partiellement dégazé .
la présence d' eau dans le magma modifie significativement , voire complètement , le dynamisme volcanique et les propriétés rhéologiques des magmas .
elle abaisse notamment le seuil de mélange de près de deux cents degrees Celsius entre des magmas saturés en eau et son exsolution ( formation de bulles lorsqu' il remonte vers la surface ) entraine une réduction significative des viscosités .
les magmas terrestres peuvent contenir jusqu'à dix pour cents de leur poids en eau supercritique , du type amphibole ) et il y a , selon les modèles , l' équivalent d' un à sept océans terrestres dans le manteau , si bien que les volcanologues parlent de plus en plus
il existe plusieurs manières de classer les volcans mais leur diversité est tellement grande qu' il y a toujours des exceptions ou des
classifications les plus courantes distinguent des types de volcans suivant la morphologie , la structure et parfois le
hauteur en raison de la fluidité des laves qui peuvent parcourir des kilomètres avant de s' arrêter , le Mauna Kea , l' Erta
rapport à sa hauteur en raison de la plus grande viscosité des laves , il s' agit des volcans aux éruptions explosives comme le
croûte terrestre ou océanique par laquelle s' échappe de la lave fluide , les volcans des dorsales se présentent sous forme de
volcanique formé par l' accumulation et le refroidissement d' une
l' effondrement des roches au dessus d' une chambre magmatique :
explosions .
il n' y a pas de cône : Dallol .
lorsque que la dépression est remplie par un lac , on appelle cela un
comme toute classification de phénomènes naturels , beaucoup de cas sont intermédiaires entre les types purs : l' Etna ressemble à un stratovolcan posé sur un volcan bouclier , Hekla est à la fois un stratovolcan et un volcan fissural .
dans Volcanoes of the World , Tom Simkin and Lee Siebert listent vingt six types morphologiques .
si on considère des zones plus larges comportant souvent plusieurs
caldeira de Yellowstone , qui n' ont pas d' édifice volcanique , multiples édifices comme des cônes de scories édifiés chacun en une
cette classification simpliste à deux ou trois catégories , absente de la littérature scientifique , peut être utilisée pour une première approche pédagogique scolaire .
selon l' université de l' Oregon , une classification à trois catégories est " mauvaise " .
il faudrait six catégories pour englober plus de quatre vingt dix pour cents des volcans .
les volcans peuvent fréquentes , alors que l' histoire d' un volcan s' étend souvent sur plusieurs milliers d' années , avec des éruptions de types différents .
article détaillé : éruption volcanique Types d' éruptions