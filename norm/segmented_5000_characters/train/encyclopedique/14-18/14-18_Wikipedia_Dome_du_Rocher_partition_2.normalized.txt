on a souvent rapproché les mosaïques du dôme de mosaïques chrétiennes , comme celles de la Basilique de la Nativité de Bethléem , par exemple .
en effet , il est assez probable que leurs réalisateurs soient des artistes chrétiens ou musulmans récemment convertis , et formés dans des traditions chrétiennes ou juives .
les mosaïques tirent d' ailleurs leurs motifs de l' antiquité tardive .
toutefois , on remarque une adaptation au modèle musulman , notamment dans la disparition de la figuration , ce que l' on retrouvera quelques années
Damas .
Oleg Grabar note par ailleurs la naissance dans le dôme du Rocher de deux grands principes spécifiques à l' art islamique : l' utilisation de formes réalistes à des fins non réalistes variation infinie sur un même thème , en arrangeant différemment des motifs semblables .
de plus , contrairement aux bâtiments de l' antiquité classique , le décor du dôme n' est pas subordonné à l' architecture et ne cherche pas à mettre en valeur la structure du bâtiment , mais au contraire couvre tout le bâtiment , comme pour créer une atmosphère particulière , un lieu unifié sans architecture réellement
le dôme du Rocher constitue le premier bâtiment où se déploie un programme d' inscriptions murement réfléchi .
trois sont umayyades : une , longue de deux cent quarante mètres , se situe au dessus des arches de la colonnade octogonale extérieure , les deux autres se trouvant sur les portes est et nord .
il s' agit à chaque fois d' inscriptions religieuses issues du Coran , mis à part le nom du commanditaire , Abd al malik
lues depuis dix neuf cent zéro , elles font principalement référence à la grandeur et à l' unicité de Dieu , s' attardent sur les missions prophétiques et notamment le rôle de Jésus comme prophète , font allusion au paradis .
on peut y voir une affirmation de la grandeur de l' islam à la fois en direction des nouveaux
montré aussi les tenants eschatologiques afin d' appuyer sa thèse selon laquelle le dôme du Rocher est une préfiguration de la Jérusalem céleste .
karl heinz Ohlig contredit ces interprétations en considérant que les inscriptions du Dôme du Rocher montrent l' absence d' une religion musulmane indépendante aux VII e VIII E siècle , estimant au contraire qu' il s' agit alors d' une
cette section ne cite pas suffisamment ses sources ( août deux mille dix neuf) .
pour l' améliorer , ajoutez des références vérifiables ( comment faire ?
) ou le modèle Référence nécessaire sur les passages
architecture , plan , matériaux ( les colonnes , avec leurs chapiteaux et leurs bases , sont récupérés des ruines de l' esplanade ) , méthodes de construction et techniques de décor puisent grandement dans le vocabulaire de l' Antiquité tardive méditerranéenne , notamment à Byzance et à Rome .
il s' y ajoute déjà une influence persane sassanide , notamment dans la géométrisation rigoureuse , le jeu des surfaces planes , ainsi que dans le décor extérieur d' origine , comme les palmettes ailées .
cette association entre l' art byzantin et l' art persan sassanide formera la base de l' art islamique , inauguré par le
de ce fait , on remarque qu' aucun bâtiment paléochrétien ni byzantin ne ressemble exactement au dôme du Rocher .
celui ci puise certes son inspiration dans la tradition chrétienne préislamique , mais y ajoute d' autres influences et un vocabulaire formel propre pour arriver à un monument typique de l' islam .
ceci se remarque tout d' abord dans la géométrisation simplifiée et rigoureuse de l' édifice : les côtés mesurent autant que le diamètre de la coupole , et chaque point de l' édifice dérive du rocher central .
quelques éloignements sont à noter , notamment dans la disposition des colonnes , mais ils obéissent à une considération visuelle : l' observateur peut voir le Rocher à quelque endroit qu' il se trouve , son regard n' étant jamais bloqué par une colonne ni un pilier .
la conception du décor , aniconique , avec de longues inscriptions arabes et des bijoux est aussi nouvelle , même s' ils prennent souvent la place d' éléments anciens tels que les
enfin , il faut noter la grande richesse dans les coloris des mosaïques , dont il reste peu d' exemples de l' Antiquité tardive et du début de la période byzantine , comme celles des monuments de Ravenne , elles s' en différencient notamment par l' absence de représentation
il est difficile à l' heure actuelle de connaître précisément les facteurs qui poussèrent le calife omeyyade Abd al Malik à commander la construction du dôme du Rocher , dont la fonction précise n' est pas déterminée par sa forme ni par ses inscriptions .
plusieurs facteurs , à la fois politiques et symboliques , peuvent être cités .
dès le IX E siècle , on trouve dans les sources l' idée que le calife souhaitait détourner vers Jérusalem le hajj , pèlerinage rituel de La Mecque , alors occupée par un rival , Ibn az Zubayr .
cette hypothèse est mentionnée par l' historien d' Alexandrie , Eutychius ( mort en neuf cent quarante ) , qui s' appuient sur des sources visiblement différentes .
néanmoins , la plupart des