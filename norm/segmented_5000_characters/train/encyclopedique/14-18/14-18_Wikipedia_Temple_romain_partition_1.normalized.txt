l' inauguratio ( " inauguration " ) suit la locatio ou accompagne celle ci .
le rite de l' inauguratio est toujours célébré par un augur ( " augure " ) qui officie seul , à la demande expresse d' un magistrat ou d' un prêtre .
à la suite des travaux d' Isaac Marinus Josué Valeton , les auteurs admettent que la cérémonie comporte quatre phases successives .
la première est l' auspicatio Jupiter .
la deuxième est la liberatio par laquelle l' augure élimine les servitudes qui grèvent le sol .
la troisième est la délimitation rituelle du lieu .
la dernière est l' effatio par laquelle l' augure énonce les limites du
avant le début de la construction à proprement dite , le lieu choisi est délimité et consacré par un augure qui délivre l' espace de toute servitude divine .
l' augure prend les auspices afin de s' assurer l' aval de Jupiter .
l' endroit choisi est ensuite nettoyé et aplani .
puis l' augure , tenant son lituus de la main droite , invoque les dieux et marque dans le ciel l' espace sacré en traçant une ligne d' est en ouest .
ainsi , l' augure sépare l' espace de ce qui l' environne , le purifie et le sanctifie ( effatum et liberatum ) .
le bornage du futur temple avec des cippes est effectué à l' aide d' équerres et de cordeaux en suivant les mouvements de l' augure .
enfin , ce dernier prononce la déclaration inaugurale qui rend l' espace délimité inviolable .
la consecratio ( " consécration " ) du temple précède sa dedicatio .
le rituel de la consecratio est célébré par un pontife .
celui ci récite une formule des libri pontificales .
elle consiste en la création d' un aedes .
à l' origine , l' acte de consecratio consiste à tracer avec une charrue deux axes perpendiculaires qui définissent l' orientation du
la dedicatio ( " dédicace " ) conclut la fondation du temple .
lorsque le sanctuaire est public , le rituel de la dedicatio est d' ordinaire célébré par un magistrat cum imperio en exercice : effet , un magistrat cum imperio sorti de charge , un magistrat sine imperio ou encore un privatus ne peut célébrer la dedicatio d' un tel sanctuaire qu' après y avoir été expressément autorisé par les comices .
il consiste en la répétition , par le magistrat , de la formule dédicatoire solennelle que le pontife a
une fois la construction achevée et les lieux consacrés , les prêtres peuvent procéder à l' inauguration officielle du temple ( la dédicace ) .
ce jour ( dies natalis ) est célébré par des cérémonies annuelles .
l' administration des temples de Rome , et de tout ce qui s' y rapporte , est un privilège du collège des pontifes .
on appelle aeditui les hommes chargés de la gestion directe et quotidienne des temples .
en ce qui concerne la propriété des temples , il est établi que dans les temps anciens , un domaine foncier est attribué à chaque temple .
mais ces terres ne servent qu' à assurer la subsistance des prêtres .
les rituels publics ( sacra publica ) sont assurés aux frais de l' État .
ainsi on suppose que lorsque les dépenses pour l' entretien des temples , compensées par les tarifs sacerdotaux et les amendes , sont trop importantes pour permettre d' entreprendre des réparations , l' État se porte caution , à moins qu' un particulier n' assure la couverture de ces
maquette du temple de Jupiter Capitolin archaïque .
il semble n' y avoir eu que très peu de temples dans les premiers temps de Rome et , si dans bien des cas la vénération d' une idole y est attestée de temps immémorial , le premier temple de cette même idole n' est construit qu' à une époque historique relativement récente .
les lieux d' adoration des premiers Latins ne devaient être le plus souvent