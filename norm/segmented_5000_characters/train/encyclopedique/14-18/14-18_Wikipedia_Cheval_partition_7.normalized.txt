les anglais croisent des chevaux pur sang arabes et Barbes avec leurs espèces indigènes pour créer les Pur sangs , fameux chevaux de course .
les premiers colons espagnols réintroduisent le cheval Barbe et andalou dans les deux continents américains .
l' espèce y avait disparu depuis plus de huit millénaires .
en quinze cent dix neuf , Les conquistadores d' Hernán Cortés amènent avec eux onze chevaux et six juments qui deviennent les premiers ancêtres des mustangs .
les Amérindiens n' ayant jamais vu ces bêtes , les conquistadores remportent de nombreuses batailles en passant pour des divinités .
Cortez aurait déclaré : " nous devons notre victoire à Dieu et à nos chevaux " .
l' animal se répand rapidement , surtout en Amérique du Nord .
durant la conquête de l' Ouest , plusieurs centaines de milliers de chevaux sauvages peuplent le continent .
au XVIII E siècle , les Amérindiens élèvent de grandes hardes de chevaux dont le nombre total dépasse les cent cinquante
dressés émergent la plupart des races américaines .
les indiens Nez Percés opèrent des sélections à partir des mustangs pour