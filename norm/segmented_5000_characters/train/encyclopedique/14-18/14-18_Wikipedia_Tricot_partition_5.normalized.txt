divers ornements ponctuels peuvent être ajoutés au tricot pour l' aspect , ou pour améliorer la résistance à l' usure .
les exemples comprennent les divers types de pompons , de paillettes ou de perles .
de longues boucles peuvent aussi être tirées et fixées , donnant un aspect hirsute au tricot , ceci est appelé tricot à boucles .
des motifs additionnels peuvent être faits à la surface du tricot en utilisant la broderie , si cette broderie ressemble au tricot , elle s' appelle souvent reprise suisse .
diverses finitions sont rapportées sur les vêtements , comme les boutons ou galons .
les boutonnières sont généralement tricotées dans le vêtement , plutôt que coupées , ce qui
des ornements peuvent aussi être tricotés séparément et attachés en applique .
par exemple , les feuilles et pétales différemment colorés d' une fleur peuvent être tricotés séparément et appliqués pour former le dessin final .
des tubes tricotés séparément peuvent être appliqués sur un tricot pour former les complexes noeuds celtiques ou autres
le feutrage est une technique pour agglomérer les fibres de laine .
le produit fini est mis dans une lessive chaude , et agité jusqu'à ce qu' il soit rétréci .
le résultat final est significativement plus petit .
on peut feutrer des sacs , des moufles , des chaussettes ou des bonnets , par exemple .
ceci peut être souhaité pour améliorer la cohésion et l' imperméabilité du tricot , ou tout à fait désagréable , si cela arrive par accident , en mettant par mégarde une pièce en tricot
des fils non tricotés peuvent être introduits dans le tricot pour renforcer leur chaleur , comme on le fait pour ouater , en petits brins dont les extrémités , laissées libres à l' intérieur , sont feutrées , ou
les deux méthodes principales pour tricoter en jacquard .
beaucoup de vêtements tricotés finis n' utilisent qu' une seule couleur de laine , mais il y a beaucoup de façons de travailler en couleurs
on tricote avec plusieurs fils de couleurs différentes , d' habitude pour faire des dessins en couleur intéressants .
on appelle ces dessins en
il existe deux approches les plus courantes , appropriées à des dessins de style différent .
la figure représente schématiquement ces deux méthodes .
elle ne présente que le trajet moyen des fils de couleur , en négligeant les sinuosités des mailles .
il s' agit d' illustrer un rond
la première méthode est appropriée pour des motifs de grande dimension , avec peu de parties de couleurs différentes .
la méthode de tricot est à plat , donc en faisant l' aller retour sur chacune des régions .
elle est illustrée sur jacquard , haut .
elle consiste à tricoter chaque région avec son propre fil de la couleur souhaitée , le raccord entre régions étant fait en passant un fil sous l' autre à chaque rang .
le résultat est un tricot d' épaisseur simple , qui a les mêmes couleurs
dans une autre méthode ( jacquard , bas ) , plus complexe , les fils de toutes les couleurs utilisées , ici deux , courent tout au long du rang , l' un étant maintenu apparent ( trait continu sur la figure ) , l' autre étant passé derrière le tricot ( trait pointillé ) .
ici encore
d' inconvénient s' il doit flotter sur une petite distance ,
de plus les décors apparaissent à l' envers dans la couleur qui y est tricotée .
dans le cas où il y a plusieurs couleurs , les fils supplémentaires sont passés entre la maille tricotée sur le devant
ce deuxième ensemble de méthodes est plus appropriée pour faire des petits motifs , des bandes , des frises , voire des chandails entiers , ce qui représente un travail très important .
un autre système d' utilisation de la couleur est de s' abandonner aux variations de couleur du fil de laine : certaines laines sont teintes en panaché , en sorte que leur couleur change au hasard toutes les quelques mailles , ou auto zébrantes , qui changent de couleur plus lentement , tous les quelques rangs .
la laine peut être faite de fibres de différentes couleurs : en petites quantités , elles seront appelées
articles détaillés : laine , Fibre et Fibre textile .