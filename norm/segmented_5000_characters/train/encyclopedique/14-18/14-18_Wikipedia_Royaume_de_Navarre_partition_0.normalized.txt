la Navarre était peuplée par les Vascons .
cette contrée fut successivement envahie par les Romains , dont elle resta longtemps la fidèle alliée , par les Suèves , les Wisigoths , les arabes .
au VIII E siècle , la Navarre était sous le contrôle
l' avènement du premier roi de Navarre ou roi de Pampelune ne s' est pas fait sans heurts , tant sur le plan intérieur , en raison de l' opposition d' une partie de la population chrétienne Navarre étant menacée d' un côté par l' émirat de Cordoue ( en sept cent quatre vingt un ,' Abd al Rahm N I er s' était emparé de Pampelune ) et de l' autre par l' Empire carolingien , avec les interventions de Charlemagne d' abord , puis de son fils Louis le Débonnaire .
en sept cent soixante dix huit , Charlemagne la soumit ainsi que tous les pays voisins jusqu'à l' Èbre .
la Navarre s' étendait à cette époque sur les deux
le royaume de Navarre ( également nommé Royaume de Pampelune ) , est né d' une alliance entre les musulmans et les chrétiens qui ont désobéi à l' autorité religieuse pour défendre leur indépendance nationale .
il faut préciser que le Banu Qasi Musa ibn Musa , surnommé le troisième roi d' Espagne , était le demi frère et le gendre d' Eneko Arista ( Eneko Aritza en basque ) , premier roi de Navarre , et que d' autres mariages ont renforcé l' alliance des deux
louis le Débonnaire , alors roi d' Aquitaine , donna la Navarre au comte Aznar .
devenu empereur , il dut faire face à plusieurs soulèvements des Vascons .
en huit cent vingt quatre , les Vascons d' Eneko Arista troisième bataille de Roncevaux .
après cette victoire , Eneko Arista est proclamé roi de Pampelune .
son fils García Ínyiguez voit son titre de roi de Navarre confirmé en huit cent soixante .
l' indépendance de la Navarre est proclamée à la diète de Tribur un , et le titre de roi reconnu à García et à ses successeurs .
à la mort de Sanche le troisième le Grand un , ce royaume , qui comprenait alors tout le Nord Est de l' Espagne ( sauf les terres de Catalogne , qui seront rattachées à la couronne aragonaise en mille millecent trente quatre avec le mariage de Raimond Bérenger IV de Barcelone ) se partage en trois royaumes :
en mille soixante seize , Sanche le quatrième de Navarre est détrôné par Sanche Ramírez , roi d' Aragon , son cousin , qui réunit les deux couronnes et les transmit Navarre redevient un royaume indépendant avec la proclamation de
Sanche le sixième est entrainé dans la lutte entre les rois de France et d' Angleterre au XII E siècle , et y perd Bayonne et le Labourd .
en mille millecent soixante dix sept , Richard Coeur de Lion intervient contre les vassaux du roi de Navarre , en guerre contre lui .
Sanche le septième participe à la grande victoire des chrétiens sur les musulmans à Las Navas de Tolosa un , et meurt sans héritier .
en mille deux cent trente quatre , Thibaut de Champagne , fils de la soeur du dernier roi , Blanche de Navarre , commence une nouvelle dynastie .
il lutte contre les anglais sur sa frontière nord .
à la mort de Henri I er de Navarre , la régente Blanche d' Artois se réfugie en France , et le roi de France prend la régence , et soumet en
le mariage de Jeanne I re de Navarre , avec Philippe le Bel
succède sur le royaume de France .
mais la règle de primogéniture masculine ne s' appliquant pas à la Navarre , celle ci est restituée à la petite fille de Jeanne I re .
jeanne II de Navarre hérite du royaume , qui ne partage plus le même souverain avec la France : la maison d' Évreux ( capétienne ) , dont est issu le mari de Jeanne le deuxième ,
fermement les rênes du royaume .
l' époque de Charles le deuxième sera l' apogée militaire de la Navarre : il s' impliquera dans les guerres espagnoles , notamment entre la Castille et l' Aragon lors de la guerre des deux Pierre , et entrera en conflit à plusieurs reprises contre les rois de France Jean le deuxième et Charles V ce dernier finira par le
Charles le deuxième envoie même des troupes en Albanie , dont avait hérité
Charles le troisième ( roi de mille trois cent quatre vingt sept à mille quatre cent vingt cinq ) revient à la paix avec ses
la Navarre passe ensuite aux maisons d' Ivrée ( ou de
de mille quatre cent cinquante et un jusqu'à mille quatre cent soixante et un , une querelle successorale conduit à une guerre civile .
Charles le troisième meurt sans fils en mille quatre cent vingt cinq .
sa fille Blanche I re de Navarre est mariée à l' héritier d' Aragon Jean .
le contrat de mariage prévoit que les deux royaumes ne fusionneront pas et que le premier fils hérite du royaume de Navarre .
à la mort de Blanche I re en mille quatre cent quarante et un , Jean d' Aragon conserve la Navarre , spoliant ses enfants Charles , prince de Viane
Charles de Viane est soutenu par les Beaumont et les Luxe , qui s' opposent aux Gramont , alliés aux vicomtes de Béarn et aux vicomtes de Dax .
après la mort de Charles de Viane , la guerre est temporairement résolue par l' arbitrage de Louis le onzième de France et d' Henri le quatrième de Castille à l' entrevue du pont d' Osserain , en mille quatre cent soixante deux .
jean d' Aragon conserve la Navarre jusqu'à sa mort , ensuite , le royaume va à sa seconde fille Éléonore de Navarre , qui meurt la même année .
la couronne passe à la maison de Grailly ( qui possède
la solution ne satisfait que partiellement les deux partis , qui guerroient sporadiquement jusqu' au début du XVI E siècle .
armoiries royales de l' Espagne utilisées en Navarre ( mille cinq cent quatre vingts mille six cent soixante huit ) .