en deux mille dix , le collège André Honnorat de Barcelonnette ouvre un internat dit " d' excellence " , destiné aux élèves doués mais de condition sociale modeste .
cet internat est destiné à leur donner les meilleures conditions d' études , .
il occupe les locaux du Quartier Craplet , ancienne garnison du onze E bataillon de chasseurs alpins puis du Centre d' instruction et d' entraînement au
barcelonnette est classée une fleur au concours des villes et
une brigade de gendarmerie chef lieu de communauté est implantée à
barcelonnette est dotée d' un consulat honoraire du
en deux mille neuf , la population active s' élevait à un deux cent quatre vingt onze personnes , dont quatre vingt six chômeurs ( cent soixante dix sept fin deux mille onze ) .
ses travailleurs sont
principalement dans la commune ( soixante dix pour cents ) .
l' agriculture compte encore dix établissements actifs , mais aucun salarié .
l' industrie et la construction emploient onze pour cents des actifs , et les services et l' administration , un peu moins de quatre vingt dix pour cents .
le tissu économique est surtout composé de petites entreprises .
un grand nombre d' entreprises de Barcelonnette dépendent du tourisme .
au un er janvier deux mille onze, les établissements actifs dans la commune sont principalement des commerces et des services ( un vingt des
fin deux mille dix , le secteur primaire ( agriculture , sylviculture , pêche ) comptait dix établissements au sens de l' Insee .
le nombre d' exploitations , selon l' enquête Agreste du ministère de l' Agriculture , est tombé de neuf à six dans les années deux mille , après une perte de quatre exploitations de mille neuf cent quatre vingt huit à deux mille .
inversement , la surface agricole utile ( SAU ) a augmenté , passant de six cent quarante deux à huit cent soixante et un hectares de mille neuf cent quatre vingt huit à deux mille , et reste à ce niveau depuis .
l' essentiel de la surface est consacrée à l' élevage bovin , qui concerne trois exploitations , les autres se tournant vers l' élevage ovin et la polyculture .
fin deux mille dix , le secteur secondaire ( industrie et construction ) comptait soixante sept
le tourisme soutient le secteur immobilier et de la construction : la principale entreprise du secteur est la SACTP ( Société alpine de construction et travaux publics ) , qui emploie
une picocentrale hydro électrique est installée depuis l' automne deux mille treize sur la conduite d' adduction d' eau potable , en bas du captage des sources du Riou Guérin et des Aiguettes .
si le débit est faible permet d' avoir une pression de cinquante sept bars , et donc une bonne productibilité .
la puissance nette délivrée par la turbine Pelton est de deux cents W ( contre cent quatre vingt deux kilowatts estimés initialement ) .
la prévision de production annuelle de un , sept million de point wh devrait donc être dépassée .
l' exploitation est concédée à Veolia en délégation de service public pour une durée de
la société Andélia , qui commercialise des bornes publiques d' accueil à grands écrans tactiles fonctionnant avec point os , a reçu une Victoire de l' entreprise ( décernée par le conseil général ) en deux mille treize .
fin deux mille dix , le secteur tertiaire ( commerces , service ) comptait trois cent quatre vingt trois les cent dix huit établissements du secteur administratif , sanitaire et social et
d' après l' Observatoire départemental du tourisme , la fonction touristique est importante pour la commune , avec entre un et cinq touristes accueillis pour un habitant , l' essentiel de la capacité d' hébergement étant non marchande .
plusieurs structures d' hébergement à finalité touristique existent dans la commune :
classés deux étoiles , et deux classés trois étoiles , ayant au total
deux mille sept ) et un autre classé trois étoiles , d' une capacité
enfin , les résidences secondaires apportent un important complément dans la capacité d' accueil , avec mille cinq cent logements ( près la moitié des logements de la commune sont des résidences
en deux mille sept , le nombre total de logements est de trois cent soixante avec près de trente neuf pour cents de ménages propriétaires de leur résidence principale en deux mille sept .
ces logements se décomposent en trente pour cents de maisons individuelles et soixante cinq pour cents d' appartements .
presque cinquante pour cents de ces logements sont des résidences secondaires ( y compris les logements occasionnels ) , quarante trois pour cents des résidences principales et huit , six pour cents des logements sont vacants .
le prix moyen de l' immobilier neuf et ancien s' établit autour de trois cent quatre vingt dix sept euros le mètre carré .
le prix moyen a diminué ces dernières années et s' est rapproché de
carré en septembre deux mille dix. côté location , le mètre carré est de douze , vingt deux euros par mois .
la Place Manuel est le quartier le plus prisé de la ville .
articles connexes : histoire du recensement de la population en
en deux mille dix sept en diminution de un , trente sept pour cents par rapport à deux mille douze , la commune de Barcelonnette comptait deux mille cinq cent quatre vingt dix huit habitants .
à partir du XXI E siècle , les recensements réels des communes de moins de dix habitants ont lieu tous les cinq ans ( deux mille sept , deux mille douze , deux mille dix sept pour Barcelonnette ) .
les autres chiffres sont des estimations .
barcelonnette n' a pas connu d' exode