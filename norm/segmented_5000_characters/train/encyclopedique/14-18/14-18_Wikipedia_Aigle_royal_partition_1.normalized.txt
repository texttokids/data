l' aigle royal est l' un des prédateurs les plus puissants dans le monde aviaire .
bien qu' il montre localement de fortes préférences pour certaines proies , l' aigle royal est avant tout un opportuniste qui chasse tout animal de petite ou de moyenne taille qu' il peut rencontrer .
près de deux cents espèces de mammifères et d' oiseaux ont été signalées être des proies du rapace .
la sélection des proies est fortement déterminée par la disponibilité et l' abondance locale des espèces .
la plupart des proies capturées pèsent environ la moitié du poids de l' aigle , la fourchette moyenne de poids des proies étant de zéro , cinq à quatre kilogrammes .
cependant , cet aigle peut aussi voler avec des proies de son poids voire légèrement plus lourdes ( de quatre à sept kilogrammes ) .
en Amérique du Nord et dans la plupart de l' Europe , les proies principales sont les léporidés ( lièvres et
prairie et marmottes ) .
dans une étude nord américaine , les mammifères représentent quatre vingt trois , neuf pour cents du régime alimentaire de l' aigle
jaune ( Marmota flaviventris ) est largement prédominante , tandis qu' en Grande Bretagne et en Eurasie centrale et alpine , le lièvre variable ( Lepus timidus ) est beaucoup plus attrapé que toute autre espèce , .
parmi les autres mammifères régulièrement tués on compte les petits rongeurs , comme les souris , des mammifères de taille moyenne comme les renards ou les blaireaux et les petits d' ongulés ( cervidés , caprins et moutons ) .
l' aigle royal représente également l' un des plus grands prédateurs du petit du renne , et peut s' en prendre jusqu' aux phoques .
le cas d' une capture d' un jeune ours brun de moins d' un an est également connu .
pour les aigles juvéniles , en hivernage ou n' ayant pas réussi à se reproduire , il est moins important de pouvoir soulever et déplacer la proie qu' il ne l' est pour les oiseaux nicheurs devant la ramener au nid , et ces premiers sont donc plus susceptibles de s' attaquer à de grandes proies , qui peuvent être laissées sur place et sur lesquelles l' oiseau pourra retourner se nourrir à plusieurs reprises .
des aigles sauvages ont exceptionnellement tué des ongulés pesant jusqu'à trente kilogrammes ou même plus , comme des chevreuils adultes .
sur une grande partie de l' aire de répartition les reptiles sont rarement chassés , mais certains grands serpents semblent être des proies assez communes dans la partie méridionale de la distribution asiatique .
pendant les mois d' hiver , lorsque les proies sont rares , les aigles royaux consomment souvent des
diorama d' un aigle royal emportant un lièvre au muséum Field
après les mammifères , le second groupe de proies préférées de l' aigle royal sont les autres oiseaux .
des gallinacés , surtout des faisans et des tétras , sont les plus représentés parmi les proies .
cependant , pratiquement tous les oiseaux , de la taille d' un geai des chênes à celle d' un cygne , qui pèse environ le double du poids d' un aigle , sont des proies potentielles .
en Suède , les oiseaux sont les proies principales , l' espèce la plus chassée étant le grand Tétras ( Tetrao urogallus ) , tandis que dans les régions subarctiques on note une forte préférence pour le
royaux sont des oiseaux superprédateurs , les adultes en bonne santé ne connaissant pas de prédateurs .
on a vu des aigles royaux tuer et manger de grands rapaces comme le faucon gerfaut ( Falco rusticolus ) , l' autour des palombes ( Accipiter gentilis ) ou les buses du genre Buteo , que ce soit des adultes , des oisillons ou
les oiseaux des genres Falco , Stercorarius ou Buteo comme la buse pattue ( B lagopus ) , qui sont normalement des concurrents , peuvent se regrouper pour chasser les aigles royaux arrivant sur leurs aires de nidification .
un aigle royal volant près d' un nid de faucon pèlerin ( Falco peregrinus ) a été frappé et tué par un des parents qui l' attaqua en piqué , alors que l' aigle , beaucoup plus grand , est généralement dominant et un prédateur potentiel pour ces oiseaux .
généralement l' aigle s' en tire à meilleur compte , et se livre au cleptoparasitisme , en volant les proies d' autres rapaces .
bien que l' aigle royal ne soit pas aussi grand que certains vautours , il est généralement beaucoup plus agressif que ceux ci et capable de les chasser , ainsi que d' autres rapaces des charognes .
l' aigle royal entre le plus souvent en compétition interspécifique avec les grands pygargues du genre Haliaeetus blanche ) et , comme ces espèces sont de taille , de force et de ténacité comparables , l' issue dans de tels conflits dépend surtout de l' individu
cette espèce est monogame .
le mâle et la femelle sont parfois unis pour la vie , fidèles à leur territoire et construisant jusqu'à cinq nids qu' ils utilisent pendant plusieurs années .
les parades nuptiales comprennent des montées en spirale , des festons ( Quoi nid , l' " aire " , est bâti sur une falaise sous un surplomb , plus rarement dans un arbre .
sa construction prend de quatre à six semaines , et consiste en un enchevêtrement de branchages et brindilles , tapissé de végétation douce , d' herbes , d' écorce ou de feuilles mortes , de