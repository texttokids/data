le T trente quatre quatre vingt cinq a été le fer de lance de l' opération Bagration , opération en profondeur par excellence , à l' été mille neuf cent quarante quatre puis de toutes les grandes offensives de l' Armée rouge jusqu'à la bataille de Berlin .
excellent à la manoeuvre , sa grande polyvalence donna l' avantage à l' Armée rouge et permit d' exploiter dans la profondeur les dispositifs défensifs de la Wehrmacht au cours de l' année mille neuf cent quarante quatre , en Ukraine et en Biélorussie , puis en mille neuf cent quarante cinq à travers la Pologne , lors de l' Offensive Vistule Oder jusqu'à
par la suite , six cent soixante dix T trente quatre quatre vingt cinq constituèrent le bélier qui enfonça l' armée impériale japonaise en Mandchourie , au mois d' août
et l' invasion de la Mandchourie constituent des chefs d' oeuvre de l' art opératif de Toukhatchevski appliqués notamment par Joukov , Rokossovski et Vassilievski .
offensives au cours desquelles les T trente quatre quatre vingt cinq jouèrent un rôle fondamental dans l' exploitation
l' usage du T trente quatre ne cessa pas avec la fin de la guerre : il constitua le char de combat principal du Pacte de Varsovie jusqu'à l' arrivée en nombre du T cinquante quatre et fut employé lors de nombreux conflits comme la guerre de Corée , où il se révéla être à la hauteur face aux Sherman et Chaffee des États Unis mais deux cent cinquante six au moins sont perdus entre juillet et novembre mille neuf cent cinquante surtout à cause du manque d' entrainement des équipages .
l' URSS envoya aux révolutionnaires cubains près d' une centaine de T trente quatre quatre vingt cinq , ils ont servi en mille neuf cent soixante et un pour repousser avec succès l' invasion de la baie des cochons .
il a aussi été utilisé lors des guerres israélo arabes jusqu' en mille neuf cent soixante treize .
il a également servi dans plusieurs conflits africains , comme en Angola où les forces du MPLA et cubaines l' ont employé lors de la guerre civile angolaise Sud Africaine .
le char a servi lors de la guerre de Bosnie pendant les années mille neuf cent quatre vingt quatorze et mille neuf cent quatre vingt quinze , cinquante ans après sa mise en service .
selon le récit de Viktor Kutsenko , quelques T trente quatre quatre vingt cinq sont même utilisés par les afghans lors de la guerre d' Afghanistan contre les soviétiques lors des combats de Zhawar deux noeuds , mais ils y sont
pour l' anecdote , le six mai vingt cent quatorzependant la crise ukrainienne , un T trente quatre descendu d' un socle près d' un mémorial officiel puis remis en état de marche mais démilitarisé a été exhibé dans une rue de Louhansk
on fait état de leur utilisation dans la guerre du Yémen où au moins deux d' entre eux ont été détruits en vingt cent dix neuf par les
en janvier vingt cent dix neuf, l' armée russe à reprit trente T trente quatre à l' Armée populaire lao pour les remettre en état et participer au
les allemands s' intéressent de près au T trente quatre qu' ils réussissent à capturer intact et l' envoient à la Heeres Versuchsstelle Kummersdorf , au sud de Zossen , un centre de recherche de l' Armée , chargé d' inspecter et de tester le matériel de guerre ennemi saisi sur le champ de bataille .
le centre de Zossen reçoit les cinq premiers T trente quatre décembre dix neuf cent quarante et un. les chenilles et les barbotins d' un de ces T trente quatre sont remplacés par des chenilles et des barbotins prélevés sur un Panzer un Ausf .
F ou un Panzer le deuxième Ausf .
J Un autre servira au tournage d' un documentaire diffusé pour les troupes en dix neuf cent quarante deux , Nahbekaempfung russicher Panzer , consacré à la lutte contre les chars .
l' inspection des T trente quatre envoyés à Kummersdorf est remise dans un rapport signé par l' Oberst Dpl .
Ing .
esser .
d' après lui , les atouts du T trente quatre résident dans son canon F trente quatre , très efficace contre les Panzer le troisième et IV , et son blindage incliné qui le protège très bien des obus de la Panzerwaffe , l' un de ses autres atouts réside dans son moteur Diesel , combiné avec de larges chenilles qui lui offre une assez
a contrario , les défauts du T trente quatre relevés dans le rapport sont l' étroitesse de la tourelle qui ne peut accueillir que deux personnes , la boite de vitesses ainsi que l' embrayage qui sont défectueux , le filtre à air de mauvaise qualité , ainsi que des moyens de communications largement insuffisants , la mauvaise qualité de certaines pièces est également relevée .
cela n' empêche pas qu' Esser est impressionné par les chars soviétiques , le rapport d' Esser constitue probablement une des raisons de la conception d' un nouveau blindé inspiré du T trente quatre le Vk trois mille deux décibels , qui sera remplacé par le Vk trois mille deux man , le prototype du Panzer le cinquième , qui reprend lui aussi des
au début de l' opération Barbarossa durant l' été dix neuf cent quarante et un , l' Ostheer capture de nombreux T trente quatre , la plupart en bon état car abandonnés par leur équipage .
malgré les difficultés de leur nouveau propriétaire à régler les problèmes d' embrayage et de boite de vitesses , plusieurs de ces chars de prise ( Beutepanzer ) désignés sous la nomenclature T trente quatre sept cent quarante sept deux noeuds sont incorporés dans les un .
, huit .
, dix .
et
production des usines STZ de Stalingrad .
nombre d' exemplaires
greffe de poignées de maintien pour le transport de fantassins sur
trappes circulaires sur le toit , qui vaut au char le surnom de des obus antichars sous calibrés , tourelleau de type " allemand " pour le chef de char .
nombre d' exemplaires inconnu .
dite " Sormovo " contenant deux membres d' équipage .
environ trois cents
la tourelle " Sormovo " .
près de vingt trois unités produites de mars