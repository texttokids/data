chacune des robes de la star coutaient entre vingt et quarante dollars .
il n' y a pas de nom officiel donné aux robes de Dietrich , mais la presse , les proches ou la chanteuse elle même en surnommèrent plusieurs en fonction de leurs caractéristiques .
d' après Maria Riva , sa mère avait réfléchi avec la costumière Irène sur un fond de robe qui " contribuait à donner à Dietrich le corps sublime dont elle a rêvé toute sa vie " , notamment lors des tournages de des films Madame veut un bébé et Kismet , entre mille neuf cent quarante deux et mille neuf cent quarante quatre .
en mille neuf cent quarante quatre , le tissu est de la soie épaisse , mais à la fin de la décennie mille neuf cent quarante , les maisons de textile italiennes , telles Baranccini , perfectionnent leurs tissus , notamment ce que Dietrich appelle le ferme comme un canevas , utilisée par la chanteuse jusqu'à la fin de sa carrière en mille neuf cent soixante quinze .
la couleur est chair .
c' est un sous vêtement ultra fragile , qui existe en douzaines d' exemplaires d' après Maria Riva .
environ trois de ces corsets étaient prévus pour une seule robe , et la collection du Filmmuseum de Berlin renferme vingt et une robes de scène , donc une soixantaine de fonds de robe , sans compter les fonds de robe inachevés et tout le matériel nécessaire pour les situations
la mise en place de ce fond de robe est un travail qui requiert patience et rigueur , comme nous l' explique Maria Riva : " d' abord , elle enfilait le fond de robe par le bas , nous attachions la fine ceinture intérieure autour de sa taille , ensuite , elle plaçait le triangle pour minimiser la douleur causée par la tension inévitable .
elle se penchait en avant , les seins ballants , détachés du corps , glissant un bras dans l' une des emmanchures , puis l' autre .
ensuite , de la main , elle amenait ses seins tombants et les plaçait dans le soutien gorge incorporé , coupé dans le biais du tissu , prenant soin d' insérer chaque bout de sein exactement au bon endroit .
une fois que les seins étaient positionnés à sa convenance , elle mettait ses mains en coupe pour les soutenir , les maintenait en place ainsi que le fond de robe , se relevait très rapidement et nous remontions la fermeture Éclair du
ce fond de robe , quasi invisible , pouvait apparaître en deux endroits : Dietrich porte toujours un collier ras de cou ou des
robe .
cette fermeture était masquée par celle de la robe , qui la
ce fond de robe présente néanmoins des limites pour Maria Riva , puis que " une fois qu' elle était à l' intérieur de ce moule , bien en place , fermeture Éclair remontée , ma mère se transformait en statue , respirant
Marlène Dietrich en frac à Amsterdam le vingt huit mai mille neuf cent soixante
après son premier triomphe au Sahara Hotel de Las Vegas entre le quinze décembre mille neuf cent cinquante troiset le quatre janvier mille neuf cent cinquante quatre, les plus grands hôtels de la ville signèrent avec la star un contrat tous les ans jusqu' en mille neuf cent soixante deux .
chaque nouveau spectacle différait surtout par la tenue que portait la chanteuse , qui cherchait à créer l' événement , bien avant avec les chansons ou le show en lui même .
" chaque fois que Dietrich passait à Las Vegas , elle essayait d' inventer de nouveaux effets , espérant retrouver ce moment où , apparaissant sur scène pour la première fois , le public muet de stupéfaction retenait son souffle " .
l' année mille neuf cent soixante est une année particulière pour la " seule allemande connue à s' être publiquement opposée au régime nazi dans les années mille neuf cent trente et mille neuf cent quarante secondes , comme le rappelle sa fille Maria Riva en deux mille un . en effet , elle se produit pour la première fois en Allemagne depuis la fin du Second Conflit mondial . au mois de mai , elle est mal accueillie à Hambourg d' abord , avec des lettres anonymes de menaces et un succès mitigé de son spectacle , puis à Berlin où une partie du public la qualifie de " traitresse " , certains tenant même des panneaux " Marlene go home " devant le théâtre .
néanmoins , le futur chancelier Willy Brandt lui exprime toute sa sympathie , comme le compositeur Friedrich Hollander , le compositeur des chansons de l' Ange bleu , qui n' était d' ailleurs nullement fâché avec elle , ayant écrit pour elle les titres du film
la même année , elle se rend en Israël pour chanter à Tel Aviv puis à Jérusalem , où elle est notamment reçue par le dirigeant David Ben Gourion .
néanmoins , le producteur israélien lui demande de ne pas chanter en allemand , cette langue ravivant des blessures trop profondes pour nombre de spectateurs , Juifs expatriés d' Allemagne dans les années mille neuf cent trente , voire sauvés des camps d' extermination .
malgré tout ,
renard noir ( documentaire ) de Louis Clyde Stoumen : narratrice
rang d' officier en mille neuf cent soixante et onze , puis de commandeur en mille neuf cent quatre vingt neuf ,
source : musée de la mode de la Ville de Paris ( sauf
chaussures " Dietrich " , le look " Dietrich " .

Maximilian Schell lui consacre un documentaire primé au
Marlene , dans lequel l' actrice revient sur sa vie , sans pour
rend hommage à l' actrice avec une photographie réalisée en mille neuf cent trente deux pour la promotion du film Shanghaï Express .
coïncidence , elle meurt le six mai mille neuf cent quatre vingt douze, le jour précédent l' ouverture de cette