l' installation d' un modchip permettait aux PlayStation d' avoir des fonctionnalités avancées , et beaucoup d' options étaient disponibles .
vers la période de fin de vie de la console , presque n' importe qui pouvait , avec une simple soudure , réaliser une modification de la console .
ces modifications permettaient de jouer à des jeux NTSC sur une console PAL ( donc d' outrepasser la restriction du code régional ) , ou alors de pouvoir jouer à des copies de jeux illégales sans problème .
les modchips permettaient de jouer à des jeux gravés sur des CD ROMvierges .
cela a généré une vague de jeux créés avec des compilateurs GNU , ainsi que la reproduction illégale de jeux originaux .
avec l' introduction de tels dispositifs , la console devint très attrayante à la fois aux yeux des
les individus qui ont insisté à copier des jeux auxquels ils voulaient jouer comme les versions originales ont rencontré plusieurs problèmes à la fois , vu que les disques fabriqués par Sony n' étaient pas facilement copiables .
non seulement les disques originaux Sony ont une couleur noire spécifique , mais ils contenaient également des secteurs vides de données qui interdisaient leur copie .
la console était supposée ne pas
sans ces espaces de mémoire volontairement vierges .
parfois , les disques à face noire contenant des secteurs vides étaient impossibles à recopier avec les logiciels habituels , et les copies étaient inutiles sans modification de la console .
les logiciels de copies de CD ont bénéficié de nouvelles fonctions comme " ignorer les mauvais secteurs " , et combinés avec les modifications de consoles ont résolu les problèmes , et ont permis au grand public d' utiliser tous les disques voulus , à partir du moment où la puce de modification était installée dans leur console .
la création et la production en masse de ces puces à très bas prix ( douze C cinq cent huit ) , couplées avec leur facilité d' installation ( de quatre à huit fils à souder ) , marquèrent véritablement le
le premier successeur de la PlayStation est la PlayStation deux , qui est rétrocompatible avec son prédécesseur , dans le sens où elle peut lire presque tous les jeux PlayStation .
cela a été possible en intégrant la majorité des composants de la première PlayStation dans la seconde .
contrairement aux émulateurs sur PC , la PlayStation deux contient le processeur original de la PlayStation , permettant aux jeux de fonctionner exactement comme sur la première PlayStation .
pour les jeux PlayStation deux , ce processeur , appelé IOP , est utilisé pour les entrées et les sorties ( les cartes mémoires , le lecteur de DVD , l' adaptateur réseau , et le disque dur ) .
comme son prédécesseur , la PlayStation deux est basée sur des composants fabriqués par Sony .
la PlayStation Portable ( PSP ) est une console portable sortie en décembre deux mille quatreau Japon ,
deux mille cinq en Europe .
elle utilise le format UMD .
une partie du catalogue de jeux PlayStation est réédité sur la console en téléchargement sur le PlayStation Network .
depuis fin décembre deux mille six, un firmware spécial permet aussi aux utilisateurs de convertir leurs jeux PlayStation au format PSP EBOOT .
la PlayStation trois , est lancée en novembre vingt cent sixaux Europe .
la plupart des jeux PlayStation fonctionnent sur les modèles quarante et soixante gigaoctets de la PlayStation trois ( la rétrocompatibilité avec les jeux PlayStation deux est dépendante du modèle ) .
les jeux PlayStation et PlayStation deux restent zonés .
à l' instar de la PSP , une partie du catalogue de jeux PlayStation est réédité sur la console en
en vingt cent onze , la PlayStation Vita est annoncée , elle succède à la PlayStation Portable .
dotée d' un écran tactile et d' un pavé tactile à l' arrière , de deux sticks analogiques et de composants plus puissants , elle sort en décembre vingt cent onzeau Japon , et en
en vingt cent treize , la PlayStation quatre est annoncée , elle succède à la
le bloc laser du lecteur de CD intégré aux premières unités produites ( notamment la série des mille avec le KSM quatre cent quarante AAM ) était construit autour d' une structure en plastique qui glissait sur des rails eux aussi en plastique .
avec le temps , la friction provoquée par les déplacements répétés durant les accès au disque rongeait le plastique sur la partie la plus lourde du bloc optique .
s' ensuit au bout de quelques mois , voire années , une inclinaison horizontale souvent inégale du bloc optique .
visuellement , on peut s' apercevoir qu' il penche d' un côté .
le laser ne pointe plus ainsi perpendiculairement sur le CD mais légèrement de biais .
par conséquent les disques ne pouvaient plus être lus correctement .
Sony a résolu le problème en changeant le matériau des patins de la partie mobile en contact avec les rails de translation déplacement ( sled ) par du silicone nylon plus robuste .
le corps du bloc optique n' est plus réalisé en plastique lui non plus mais en métal sur modèles suivants , à