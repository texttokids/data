réutilisé , il est réapprovisionné en branchages , et d' années en années l' aire peut atteindre deux à trois mètres de diamètre , deux mètres de haut pour quatre vingt dix centimètres de profondeur .
un nid de six , un mètre de haut pour un diamètre de deux , cinquante neuf mètres est le plus gros connu chez l' espèce .
placé dans un arbre , il peut faire céder celui ci .
de petits mammifères ou de petits oiseaux trop petits pour intéresser l' aigle utilisent parfois aussi le nid , leurs prédateurs entrant souvent dans la gamme de
chaque année en mars ou avril , la femelle pond de un à trois oeufs , parfois quatre mais souvent deux , blanchâtres et tachetés de brun .
ils sont pondus à trois ou quatre jours d' intervalle , ce qui espace ponte du premier oeuf .
la durée d' incubation varie de trente cinq à quarante cinq jours , avec une moyenne de quarante deux jours .
la femelle assure l' essentiel ou la totalité du temps de couvaison , surtout dans les premières semaines , le mâle chassant pour la nourrir et défendant le
la plupart du temps un seul jeune survit : durant la deuxième quinzaine de vie , le plus fort ( souvent le premier éclos ) agresse et finit par tuer le plus faible sans que la concurrence alimentaire ne le justifie , on parle de caïnisme .
si le premier né est un mâle et le deuxième une femelle , généralement plus grosse qu' un mâle , la compétition est plus équilibrée et les deux jeunes peuvent parvenir à
son premier plumage .
il effectue en moyenne son premier vol vers dix semaines , mais reste au voisinage de l' aire .
les adultes lui tuent des proies , car le jeune mettra trente deux à quatre vingts jours supplémentaires pour acquérir son indépendance .
la maturité sexuelle n' est entièrement atteinte qu' entre quatre et sept ans .
l' espérance de vie d' un individu est
l' aigle royal qui niche principalement en falaise ( milieu rupestre ) se rencontre dans les montagnes d' Eurasie , comme
Maghreb et en Amérique du Nord : victime de persécutions , il a fui les plaines et est devenu presque exclusivement montagnard .
il occupe également le pourtour de ces territoires ( pré montagneux ) et les territoires ouverts des forêts boréales de Russie , des pays baltes et de Scandinavie .
une population niche également sur les hauts plateaux éthiopiens .
certaines populations sont plutôt migratrices , d' autres sédentaires .
le territoire d' un couple couvre généralement de vingt deux à trente trois km deux .
Carl von Linné décrit l' espèce dans la dixième édition de son Systema Naturae en mille sept cent cinquante huit , en la plaçant , comme tous les autres rapaces diurnes , dans le genre Falco , sous le protonyme de Falco chrysaetos .
la dénomination spécifique , chrysaetos , vient du grec rho ( khrysós ) pour " or " et ( aetós ) signifiant parfois retenue pour l' oiseau .
l' espèce est ensuite déplacée dans le
les plus proches parents de l' aigle royal sont l' aigle de Verreaux
d' après Alan P Peterson , cette espèce est constituée des six
du Nord , dans la Péninsule de Basse Californie et le
présente en Eurasie sauf dans la péninsule Ibérique et en
Kazakhstan jusqu'à la Mandchourie et au sud ouest de la
ibérique et l' Afrique du Nord , l' est de la Turquie ,
corée et dans le sud des îles Kouriles .
cette sous espèce est en net déclin , et sa population est estimée à cinq cents individus
orientale , de l' Altaï à la péninsule du Kamchatka .
cette section ne cite pas suffisamment ses sources ( janvier
pour l' améliorer , ajoutez des références vérifiables ( comment faire ?
) ou le modèle Référence nécessaire sur les passages
l' aigle royal bénéficie d' une protection totale sur le territoire français depuis l' arrêté ministériel du dix sept avril mille neuf cent quatre vingt unrelatif aux oiseaux protégés sur l' ensemble du territoire .
il est inscrit à
européenne .
il est donc interdit de le détruire , le mutiler , le capturer ou l' enlever , de le perturber intentionnellement ou de le naturaliser , ainsi que de détruire ou enlever les oeufs et les nids et de détruire , altérer ou dégrader leur milieu .
qu' il soit vivant ou mort , il est aussi interdit de le transporter , colporter , de l' utiliser , de le détenir , de le vendre ou de l' acheter .
plusieurs peuples d' Asie centrale , principalement les Kazakhs , Kirghizes et Mongols utilisent cet oiseau pour la chasse .
les
l' armée de l' air française a élevé et dressé des aigles royaux
article détaillé : symbolique de l' aigle chez les Amérindiens .
l' aigle royal jouit d' une excellente réputation : on le considère souvent comme le plus beau des aigles , et comme le roi des oiseaux .
depuis l' Antiquité , il est symbole de victoire , c' est pourquoi les Assyriens , les Hittites , les Perses le
romains , il était aussi messager des dieux , et oiseau de Jupiter .