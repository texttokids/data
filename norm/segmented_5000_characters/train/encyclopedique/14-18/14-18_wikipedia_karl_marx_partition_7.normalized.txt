malgré tout , le déterminisme marxien n' est que partiel .
en effet , si l' " être social explique la conscience sociale : dans la production sociale de leur existence , les hommes entrent en des rapports déterminés , nécessaires , indépendants de leur volonté , rapports de production qui correspondent à un degré de développement déterminé de leurs forces productives matérielles . " ( contribution à la Critique de l' Économie Politique ) , néanmoins l' être humain a son libre arbitre , ses passions , ses intérêts .
toutefois , pour Ernest Mandel les intérêts de classe sont prédominants sur les intérêts ou les passions individuelles .
il note : " le résultat de la collision de millions de passions , d' intérêts et d' options est essentiellement un effet de logique sociale et non de psychologie individuelle " .
la liberté humaine est donc limitée par la lutte des classes .
en fait pour Marx la liberté c' est surtout d' avoir du temps de loisir disponible pour développer ses talents , ses potentialités ce que peut faire la classe gouvernante .
le but pour lui est donc de libérer la classe ouvrière en développant assez les forces productives pour limiter le temps de travail des prolétaires et arriver à la société sans classe .
l' État pour Marx est " constitué de groupes de personnes séparés et à part du reste ( la majorité ) de la société " , c' est un instrument de maintien d' une certaine structure sociale et de classes données .
l' émergence de la société sans classe permet de s' en passer et d' arriver à une société d' auto administration .
toutefois avant d' atteindre cette phase ultime , il faut passer pour Marx par la dictature du prolétariat qu' il voit comme un État qui cherche à assurer sa propre dissolution .
au cours de l' Histoire , les progrès techniques permettent d' accroître la production .
après un certain temps , un conflit nait au sein de la société , où les rapports sociaux changent : la classe sociale qui détient les nouvelles techniques prend de l' importance sur la classe sociale dominante , fondée sur l' ancien modèle de production .
exemple : du système féodal où le suzerain possédait les terres et ceux qui la travaillaient , et le rôle du clergé sur la société , on est passé à une société dominée par la bourgeoisie au cours de la révolution industrielle du XVIIIe siècle .
ainsi , selon Marx , est née une nouvelle forme de l' économie : le capitalisme , qui suppose une nouvelle forme de propriété privée , garantie par une institution juridique nouvelle .
Marx , dans son oeuvre a résumé l' histoire humaine en quatre étapes ( la cinquième à venir étant , selon lui , la période socialiste ) , correspondant à des techniques et des modes de production différents : la communauté primitive , la société esclavagiste ( la société romaine ) , le régime féodal , le régime capitaliste .
Marx pense que le sens de l' Histoire est à terme inéluctable , et qu' elle aboutit toujours à cette troisième étape , critique , de restructuration sociale .
les rapports de production finissent tôt ou tard par être contestés , par ne plus être adaptés au développement , par être insupportables pour une part importante de la population : les structures de la société , qui paraissaient immuables , doivent alors changer .
la lutte des classes est l' opposition , au sein de la société civile , entre divers groupes de la population , qui se distinguent selon leur mode de vie et l' origine de leurs revenus .
aristocratie financière : banquiers , boursicoteurs , propriétaires ( terres , mines de charbon et de fer , forêts , chemins de fer , et caetera ) .
leurs revenus sont la rente et les intérêts .
bourgeoisie industrielle : propriétaires des usines , des manufactures et des machines , entrepreneurs .
leurs revenus sont les profits du capital .
classes laborieuses : ouvriers et paysans .
leur revenu est le salaire de leur force de travail .
selon la conception matérialiste de l' histoire ( appelée aussi matérialisme historique ) , les oppositions entre ces classes sociales constituent le fil conducteur qui permet de comprendre la succession des sociétés et des périodes historiques .
la théorie de la lutte des classes avance qu' exceptées les communautés primitives , toutes les sociétés historiques sont composées de classes en opposition constante ( homme libre et esclave , patricien et plébéien , seigneur et serf , patrons et ouvriers ) , cette opposition étant le moteur même de l' histoire .
l' idée selon laquelle la société n' est pas homogène , mais que ses membres ont des aspirations divergentes , et parfois contradictoires , n' est pas nouvelle .
le concept et l' expression de " lutte des classes " viennent d' historiens contemporains de Marx , notamment français , en particulier de François Guizot ( dont il possédait deux ouvrages , Histoire de la civilisation en France et Histoire générale de la civilisation en Europe , il recensa aussi le livre Pourquoi la révolution d' Angleterre a t elle réussi ?
discours sur l' histoire de la révolution d' Angleterre ) .
François Guizot écrit :