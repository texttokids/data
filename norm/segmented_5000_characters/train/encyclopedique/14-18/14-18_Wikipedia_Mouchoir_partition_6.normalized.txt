cette distribution a un succès relatif , d' un côté , les japonais utilisent peu les mouchoirs en public ( voir supra ) , d' un autre côté , l' aspect gratuit et le fait qu' on peut utiliser un mouchoir en papier pour n' importe quelle tâche hygiénique permettent malgré tout un certain succès à cette méthode publicitaire .
preuve en est , la diversification des publicités : avant les années mille neuf cent quatre vingt dix , on ne trouvait que des publicités pour des banques ou des téléphones roses .
aujourd'hui , le paquet de mouchoir est devenu un espace publicitaire
caption : le mouchoir : un objet d' une polyvalence extrême
utilisation prévue et affectant la forme ou la qualité du mouchoir Utilisation non prévue , à défaut d' objet plus adéquat
le mouchoir éponge : s' essuyer le front servir de gant de toilette Faire montre d' un certain confort ou d' une certaine richesse
militaires imprimées ou brodées Tout essuyage ( poussière , eau , et caetera )
filtrage physique ( protection contre la fumée les miasmes les
noeuds divers ( attache , garreau , ou aide mémoire )
si le mouchoir est grand ( surtout avant le XX E siècle ) :
articles détaillés : dentelle , Mode ( habillement ) , Histoire de la mode , Textile et Industrie textile .
entre mille sept cent soixante et onze et mille sept cent quatre vingt trois , l' Académie royale des sciences fait publier les Descriptions des arts et métiers , faites ou approuvées par Messieurs de l' Académie royale des sciences de Paris .
l' ouvrage contient des articles très précis sur la taille des mouchoirs ,
le mouchoir était de toute forme , ronde , carrée , triangulaire , toutes moucher .
cependant , au XVIII E siècle , Marie Antoinette fit remarquer que la forme carrée était plus adéquate , Louis le seizième , par la suite , imposa ce format au mouchoir par des lettres patentes en
la forme carrée est restée jusqu'à nos jours , bien que le mouchoir n' ait , du point de vue hygiénique , nul besoin de standardisation .
l' Encyclopédie répertorie plusieurs usages du mouchoir comme accessoire de mode ( voir tableau ci après ) .
du XVIII E siècle au XX E siècle , l' évolution de la forme du mouchoir dépendra de deux facteurs croisés : la relative standardisation d' une part et , d' autre part , l' utilisation de mode , qui se fait parfois ( assez rarement ) à partir de mouchoirs prévus dès l' origine de leur fabrication pour être des coiffes ou des accessoires de costume .
mouchoir , S .
Monsieur ( Gram .
et Écon .
domestiq .
) linge qu' on porte dans sa
mouchoirs de col , terme de Marchand de mode , ce sont des grands mouchoirs de soie qui ressemblent à du satin , mais qui n' a point d' envers , sur lesquels sont travaillés des desseins qui paroissent se servent de ces mouchoirs pour mettre sur leur col .
les Marchands de
mouchoir frisé , terme de Marchand de mode , ce sont trois rangs de gase brochée ou peinte , de blonde ou de dentelles , montés par étage sur un ruban de fil assez étroit , et qui sont fort plissés .
cet ajustement sert aux femmes pour mettre sur leur col , et peut être large en tout de
mouchoirs a deux faces , ( Soyerie .
) étoffe legere , façon de serge , dont un côté est d' une couleur par la chaîne , et l' autre d' une autre couleur
un mouchoir carré à rayures : le grand classique .
après Louis le seizième , la standardisation de la taille est principalement due avec elles apparurent en effet les réplications à grande échelle , surtout pour un objet aussi commun et répandu .
mais notons immédiatement un bémol : il s' est agi d' une standardisation de facto et non de jure , c' est à dire que chaque manufacture fabriqua des mouchoirs d' une taille unique , sans aucune règle commune avec les autres manufactures .
c' est encore le cas aujourd'hui : si l' automatisation des métiers à tisser permet une taille identique au millimètre près pour chaque modèle de mouchoir d' un même fabricant , en revanche chaque fabricant a ses propres patrons .
ce n' est pas le cas pour les chaussures , la longueur maximale des couteaux , la taille des ampoules électriques ou autre objets domestiques dont la standardisation est cruciale
texture sans accroc d' un mouchoir en tissu au microscope .
au XIX E siècle , le mouchoir aurait tendu vers une normalisation en trente par trente deux noeuds , plus souvent quarante par quarante , moins souvent cinquante par cinquante ( ces données sont forcément approximatives , ainsi qu' on vient de le voir ) , mais
standardisés : ainsi du mouchoir pour bébé en forme d' animal par exemple , attaché à un hochet ( ou autre ) , pouvant très bien servir de doudou , très répandu bien que discutable hygiéniquement .
l' automatisation a aussi apporté une plus grande rapidité et une meilleure efficacité dans la production de mouchoirs de qualité : à la main , avec le métier mécanique , un accroc est plus fréquent
impression à la planche de bois et peinture à la main , vers
un mouchoir non jetable est en général fait d' un tissu léger , doux , peu
au XVIII E siècle , de nouvelles qualités de coton apparaissent , provenant notamment d' Inde , par exemple le madras , du nom de la ville indienne .
madras a aussi donné son nom aux tissus à carreaux ,