pourquoi les terroristes ont ils attaqué ?
les terroristes peuvent avoir des buts différents .
certains veulent posséder un bout de pays déjà occupé par d' autres gens , d' autres souhaitent forcer tout le monde à suivre leur religion ou à avoir les mêmes idées qu' eux .
les terroristes de vendredi soir font partie d' une organisation qui s' appelle l' Etat islamique .
on dit parfois aussi " Daech " .
ce n' est pas un pays .
c' est un groupe de terroristes qui a été créé il y a dix ans dans un pays qui s' appelle l' Irak .
aujourd'hui , ils réussissent à diriger plusieurs villes en Irak et en Syrie .
les terroristes disent avoir choisi de commettre des attentats à Paris pour se venger de la France .
ils sont très fâchés parce qu' avec d' autres pays , la France envoie des bombes sur la Syrie .
là bas , c' est la guerre depuis plus de quatre ans , la situation est très compliquée parce que le gouvernement est aussi violent envers sa population .
la France , avec d' autres pays , essaie d' empêcher l' Etat islamique d' avoir des armes et de s' organiser .
ces terroristes ont une religion , l' islam .
on dit qu' ils sont musulmans .
beaucoup de gens ont la même religion , et ils sont très tranquilles , ils aiment la paix .
mais ceux qui ont fait les attentats appliquent l' islam d' une façon particulière et refusent que les autres gens ne fassent pas comme eux .
par exemple , ils veulent que les femmes soient entièrement voilées , ils ne veulent pas que les gens boivent de l' alcool ou écoutent de la musique .
pour imposer tout ça à l' ensemble de la société , ils utilisent la violence .
ces terroristes sont des jihadistes .
ils disent qu' ils se battent au nom de l' islam , mais leurs actes n' ont rien à voir avec cette religion .
vendredi soir , on a aussi parlé de " kamikazes " .
ce sont des terroristes qui se tuent en même temps qu' ils tuent d' autres gens .
ils font exploser les bombes qu' ils ont sur eux pour mourir , on dit qu' ils se suicident .
c' est pour cela que l' on parle d' attentats suicides .