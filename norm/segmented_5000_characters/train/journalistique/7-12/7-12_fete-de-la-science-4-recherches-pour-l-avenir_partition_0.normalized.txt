numéro cent vingt trois onze au dix sept octobre deux mille dix neuf
fête de la science : quatre recherches pour l' avenir
la science a toujours eu de sacrés défis à relever .
quels sont ceux de demain ?
préservation de l' environnement , lutte contre le changement climatique , meilleure connaissance de l' univers ...
la science nous accompagne et contribue aussi à imaginer les façons d' améliorer notre vie et notre futur .
à l' occasion de la fête de la science qui se déroule en octobre partout en France et dans le monde , suis moi pour découvrir de super recherches sur terre , dans la mer et dans l' espace ...
partout où les scientifiques viennent chercher des réponses .
c' est quoi la fête de la science ?
la fête de la science se déroule chaque année depuis vingt huit ans , toujours à l' automne .
elle se tient cette année en France du samedi cinq octobre au dimanche treize octobre .
et se poursuit jusqu' au dix sept novembre en Corse , en outre mer et à l' étranger .
à cette occasion , des milliers de chercheurs , enseignants et bien d' autres professionnels se retrouvent et partagent avec le public leur travail , leurs idées et les innovations scientifiques .
expériences , conférences , festivals , visite de laboratoires ...
il y a environ six évènements dans toute la France et c' est gratuit .
la science , c' est un mot qui impressionne parfois et pourtant il n' y a pas de quoi !
elle permet d' expliquer les phénomènes naturels ( comme le mouvement des planètes ou comment une pomme tombe d' un arbre ) , de produire des matériaux qui n' existent pas dans la nature ( plus résistants , par exemple ) , de développer des médicaments ou encore d' expliquer le changement climatique .
" sans la science , on ne pourrait que constater les choses sans pouvoir les expliquer , remarque Didier Leterq , chercheur et ingénieur au CEA , le Commissariat à l' énergie atomique et aux énergies alternatives ( organisme de recherche sur l' énergie , la défense et la santé ) . on serait aussi incapable de trouver des solutions . "
la recherche scientifique c' est comme une enquête policière , explique Didier Leterq : " on constate des phénomènes qu' on n' arrive pas à expliquer . donc on récolte des indices , on élabore des hypothèses et puis à un moment il y a un déclic : on comprend ce qu' il se passe et on émet une théorie , c' est à dire une réponse à la question qu' on se posait . "
retrouve la solution à la fin du coin lecture de ce numéro .
les scientifiques travaillent chacun dans un domaine : la chimie , la physique , l' informatique , la médecine ou la mécanique .
ce ne sont pas des femmes et des hommes enfermés dans leur laboratoire !
ils travaillent en grande majorité en équipe , se posent sans cesse des questions et " souhaitent contribuer au progrès et au bien être de chacun . c' est leur vocation " , souligne Didier Leterq .