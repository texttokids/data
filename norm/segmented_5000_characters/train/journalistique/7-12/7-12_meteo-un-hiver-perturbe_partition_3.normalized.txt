comment peut on prévoir le temps ?
sur la Terre , il existe des milliers de stations météorologiques qui enregistrent tout ce qui se passe dans l' atmosphère .
en France , il y en a cinq cent cinquante quatre .
dans chaque station , des appareils enregistrent les variations de l' air comme l' hygromètre qui mesure l' humidité de l' air ou la girouette , qui indique la direction du vent .
au sol , il existe aussi des radars .
on en compte une vingtaine en France .
chaque radar couvre un rayon d' une centaine de kilomètres et permet de voir où il pleut , où il neige et à quelle intensité .
dans l' espace , des satellites prennent des photos du ciel pour voir le mouvement des nuages et calculer la direction et la vitesse des vents .
certains surveillent toujours la même zone sur des millions de kilomètres .
dans l' air , on envoie aussi des ballons avec des capteurs à l' intérieur pour mesurer la pression , l' humidité ou la force des vents .
enfin en mer , certains navires étudient les courants et le temps qu' il fait .
toutes ces informations qui viennent de la Terre , de l' espace et de la mer sont envoyées en temps réel à un énorme ordinateur de Météo France situé dans son gros centre à Toulouse .
avec toutes ces données , ce super ordinateur donne chaque jour un scénario du temps qu' il fera aux prévisionnistes de Météo France .
les prévisionnistes de Météo France travaillent dans huit centres répartis dans le pays .
les premiers arrivent à sept heures et regardent d' abord la météo du jour car " pour prévoir le temps qu' il fera demain , il faut déjà connaître le temps qu' il fait aujourd'hui " , explique Frédéric Nathan , prévisionniste à Météo France .
ensuite , ils étudient le scénario du gros ordinateur de Toulouse et le comparent à d' autres ordinateurs européens ou américains .
tout le monde se met ensuite d' accord lors d' une grande réunion qui se tient à neuf heures .
enfin , les prévisionnistes qui travaillent avec les médias leur font un résumé et leur envoient des cartes .
malgré tout , les prévisions ne sont pas toujours bonnes .
certains phénomènes , comme le risque d' orages , ou d' averses , ne sont pas toujours faciles à voir .
" on sait qu' il va y avoir des averses dans une zone mais on ne sait pas où exactement " , explique Frédéric Nathan , de Météo France .
c' est comme quand on fait bouillir de l' eau .
on sait que des bulles vont apparaître mais on ne sait pas à quel endroit .