que se passe t il pour cette rentrée ?
à chaque rentrée , c' est la même histoire .
le coeur des élèves palpite à toute vitesse et les questions se bousculent dans leur tête : " est ce que je serai dans la même classe que mes amis ? mes nouveaux professeurs seront ils sévères ? "
l' angoisse de la rentrée n' épargne personne ...
pas même les professeurs !
le vendredi trente août , trois jours avant les élèves , les huit cent soixante et onze enseignants français ont retrouvé leurs établissements pour préparer la nouvelle année scolaire .
et il y a des changements importants .
l' enseignement ( à l' école ou à la maison ) est désormais obligatoire dès l' âge de trois ans , au lieu de six ans .
une grande majorité des élèves étaient déjà inscrits à la maternelle dès cet âge là , mais ce n' était pas une obligation .
dans quelques écoles des quartiers les plus défavorisés , le nombre d' élèves par classe est divisé par deux ( c' était déjà le cas pour les CP et les CE un l' année dernière ) : il ne pourra pas y avoir plus de douze élèves par cours .
les élèves de première inaugurent le nouveau fonctionnement du lycée .
ils n' ont plus à choisir entre plusieurs baccalauréats ( comme scientifique , économique , ou littéraire ) .
maintenant , tous les élèves ont un programme commun , et choisissent en plus des spécialités .
il est par exemple possible de faire à la fois des mathématiques , de la physique et de la philosophie .
au printemps vingt cent vingt et un , ces élèves seront les premiers à passer le nouveau baccalauréat imaginé par Jean Michel Blanquer , le ministre de l' Education nationale .
ils passeront toujours un examen à la fin de la terminale mais les contrôles qu' ils auront passés pendant l' année seront désormais aussi inclus dans leur note finale .
beaucoup d' enseignants sont en désaccord avec ces changements .
ils disent manquer de moyens pour bien les appliquer .
ils regrettent aussi de ne pas avoir été plus écoutés par le ministère de l' Education nationale , en particulier à propos de la réforme du lycée .
ils voudraient que leur travail soit plus respecté .
c' est la raison pour laquelle un important mouvement de grève a été organisé au moment du baccalauréat , en juin dernier .
ça n' a pas suffi à faire changer le ministère d' avis sur la réforme du lycée .
pour apaiser les tensions , Jean Michel Blanquer a annoncé il y a quelques jours que les professeurs allaient gagner trois cents euros de plus par an , donc trente euros en moyenne par mois .
mais les professeurs trouvent que ça ne répond pas à leur demande .
pour le moment , aucune grève n' est prévue pour la rentrée .
mais les relations entre les profs et le ministère restent tendues .