Yann gère la fabrication de boules de Noël
l' histoire raconte que les boules de Noël doivent leur existence à un triste évènement .
en dix huit cent cinquante huit , il y a eu si peu de pluie dans les Vosges du Nord , une région montagneuse du nord est de la France , que les fruits d' hiver n' ont pas poussé .
mais comment allait on donc habiller les sapins de Noël , qui étaient à cette époque décorés avec des pommes suspendues ?
un souffleur de verre a alors eu une idée : créer des boules en verre pour les remplacer .
voilà comment les boules de Noël seraient nées en France .
à Meisenthal , un petit village des Vosges du Nord , des artisans produisent encore des boules de Noël traditionnelles en verre soufflé grâce au Centre international d' art verrier ( Ciav ) .
dans l' atelier de fabrication , il peut faire très chaud , jusqu'à cinquante degrés en été !
chaque année , les verriers de Meisenthal font appel à un designer pour imaginer une nouvelle suspension de sapin .
cette année , Nathalie , une graphiste , a créé une boule en forme d' artichaut surnommée " Arti " .
les techniques des verriers représentent pour Yann un savoir faire régional précieux .
il a travaillé dur pour qu' elles ne soient pas oubliées .
" dans le village , il y avait à l' époque une usine de fabrication d' objets en verre , raconte Yann . avec l' apparition du plastique , les gens ont arrêté d' acheter du verre et l' usine a dû fermer il y a près de cinquante ans . "
des années plus tard , des élus politiques régionaux se sont mobilisés pour que l' art du verre continue d' exister à Meisenthal .
des artisans verriers sont venus de toute la France pour transmettre leur savoir .
grâce à eux , le centre a recommencé petit à petit à fabriquer des boules en verre soufflé .
aujourd'hui , il en produit jusqu'à quarante cinq par an .
mais décorer son sapin avec une boule de Meisenthal , ça se mérite : elles sont uniquement vendues dans la région !
Meisenthal , le village où sont fabriquées les boules en verre soufflé