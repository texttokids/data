pourquoi parle t on de ce peuple ?
depuis plusieurs mois , on entend régulièrement parler dans l' actualité d' un peuple de Birmanie , un pays situé en Asie : les Rohingyas .
plus de six cent quatre vingts d' entre eux ont en effet fui leur pays depuis la fin du mois d' août pour se réfugier au Bangladesh , un pays voisin .
les Rohingyas ont fui leur pays parce qu' ils ont été chassés de chez eux par des soldats , des policiers et aussi parfois par d' autres habitants , qui ne pratiquent pas la même religion qu' eux .
les Rohingyas sont musulmans , alors que la religion majoritaire en Birmanie est le bouddhisme .
ça fait très longtemps que les Rohingyas sont maltraités dans leur pays .
mais c' est la première fois que c' est si grave .
à la fin du mois d' août , des jeunes Rohingyas qui en avaient assez d' être rejetés ont attaqué des postes de police et ont tué dix policiers .
du coup , l' armée et la police ont voulu punir toute la population musulmane très sévèrement .
il y a eu des coups de feu , des morts , des blessés , des pillages , et beaucoup de villages ont été brulés .
sous la pression d' autres pays , le gouvernement birman a récemment promis de les faire revenir dans le pays s' ils prouvent qu' ils habitaient bien en Birmanie .
sauf que les Rohingyas n' ont pas de papiers d' identité depuis des années parce que le gouvernement refuse de leur en donner .
en plus , le gouvernement veut les rassembler dans des camps où ils n' auront rien pour vivre dignement .
au Bangladesh , six réfugiés rohingyas sur dix sont des enfants .
certains d' entre eux ont vu des scènes de violence , tous ont quitté leur maison et ont dû arrêter l' école .
les réfugiés se sont installés sous des bâches en plastique , dans les jardins de certains habitants et dans de très grands campements .
les enfants qui ont perdu leurs parents ont été regroupés dans une sorte d' orphelinat qui les protège et s' occupe d' eux .
les réfugiés ont monté une radio locale et un système de petites annonces pour que les familles puissent retrouver les enfants .