quel est le tout premier jeu vidéo ?
quand on s' intéresse aux origines du jeu vidéo , un nom revient toujours : Pong .
c' est un jeu de tennis sorti il y a quarante six ans , en mille neuf cent soixante douze .
les joueurs sont représentés par deux barres blanches de chaque côté de l' écran et ils s' échangent un carré blanc qui représente la balle .
mais est ce réellement le tout premier jeu ?
en fait , il y a plusieurs candidats à ce titre .
on peut par exemple parler du premier à avoir utilisé un écran .
il s' agit de OXO , un Morpion créé en mille neuf cent cinquante deux .
mais le jeu du Morpion , où il faut juste aligner trois ronds ou trois croix , ce n' est pas vraiment un jeu vidéo : il suffit d' une simple feuille de papier pour y jouer .
en mille neuf cent cinquante huit , un jeu plus travaillé encore est apparu : tennis for Two .
ce jeu de tennis avait été créé par un chercheur en physique sur un écran tout rond .
le filet était représenté par un petit trait au milieu de l' écran .
on se servait d' une petite roue pour choisir la direction dans laquelle on renvoyait la balle et on appuyait sur un bouton pour taper .
ça a été un très grand succès , mais le chercheur ne voyait pas en quoi son invention pouvait intéresser d' autres personnes , donc il n' a pas cherché à la faire connaître .
il a alors été vite oublié .
en mille neuf cent soixante deux , un étudiant américain en informatique a créé SpaceWars !
c' était un jeu de combat qui se passe dans l' espace .
on y jouait aussi sur un écran rond .
chaque joueur contrôlait un petit vaisseau en forme de triangle et pouvait tirer sur celui de son adversaire .
dix ans plus tard , en mille neuf cent soixante douze , la toute première console de jeux vidéo a vu le jour : c' était la Magnavox Odyssey .
problème : les jeux n' étaient pas très intéressants et on s' ennuyait vite .
personne ne voulait l' acheter .
mais un des jeux , présenté comme un jeu de tennis , a donné des idées à un certain Nolan Bushnell .
cette année là , en mille neuf cent soixante douze , il sort une borne d' arcade avec sa propre version de ce jeu , qu' il appelle Pong .
c' est le premier à avoir eu un vrai succès .
c' est pour ça qu' on considère souvent Pong comme le tout premier de l' histoire des jeux vidéo .
mais ce n' est pas pour autant qu' il faut oublier ce qu' il y a eu avant !