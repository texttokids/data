que se passe t il avec Parcoursup ?
des universités bloquées et des manifestations un peu partout en France : ces dernières semaines , de nombreux étudiants se sont mobilisés contre de nouvelles règles mises en place par le gouvernement pour entrer à l' université .
pour la première fois cette année , les universités classent en effet les dossiers des bacheliers des meilleurs aux moins bons .
avant , ce tri ne se faisait que pour l' accès à certaines formations après le bac .
le problème , c' est que tout le monde a le droit d' entrer à l' université .
c' est la loi qui le dit .
pour aller dans l' enseignement supérieur , les lycéens ont dû remplir plusieurs choix de formation sur Parcoursup , le tout nouvel outil informatique accessible par Internet .
le vingt deux mai , les premiers résultats de cette machine sont tombés avec trois types de réponses : " oui " , " non " ou " en attente " .
et là , ça a été un coup dur : la moitié des candidats s' est retrouvée sur liste d' attente .
ils doivent donc attendre que les élèves acceptés dans plusieurs formations fassent des choix et leur libèrent des places .
mais certains ont beaucoup moins de chance que Monsieur P' tit Libé parce qu' ils sont très loin dans la liste d' attente .
les lycéens déçus ont beaucoup réagi , notamment sur les réseaux sociaux .
le lycéen ci dessous est par exemple un soixante dix neuvième sur liste d' attente pour une formation qui comprend quatre vingt deux places ...
depuis les premiers résultats , des places se libèrent petit à petit .
la ministre de l' Enseignement supérieur , Frédérique Vidal , a promis que tout le monde serait pris quelque part et qu' il fallait être patient .
mais certains n' auront pas de réponse avant septembre , c' est à dire au moment où ils doivent normalement commencer leurs nouvelles études .
Parcoursup est tout neuf , c' est la première fois qu' on l' utilise .
comme il y a eu de nombreux élèves en attente et que certains disent avoir eu des problèmes techniques , les jeunes ( et leurs parents ) ont peur qu' il y ait des erreurs .
ceux qui attendent une réponse se connectent tous les jours sur Parcoursup pour voir s' ils sont remontés sur la liste d' attente .
c' est stressant pour ces lycéens qui passent leur bac le dix huit juin .