qui sont les députés ?
les députés sont des hommes et des femmes élus pour cinq ans par les citoyens français pour les représenter .
ils appartiennent en grande majorité à un mouvement politique .
pour devenir député , il faut avoir la nationalité française , être âgé d' au moins dix huit ans et être candidat aux élections législatives là où on le souhaite , même à un endroit où on n' habite pas .
chaque candidat se présente ainsi sur une circonscription .
pour bien comprendre , la France est découpée en cent un départements .
chaque département est divisé , en fonction de son nombre d' habitants , en plusieurs circonscriptions .
il y a cinq cent soixante dix sept circonscriptions , avec un député élu dans chacune d' elle .
il y a donc cinq cent soixante dix sept députés .
les circonscriptions de la Charente
les députés sont élus sur un territoire , mais ils représentent tout le pays .
ils font partie d' une assemblée , l' Assemblée nationale , qui est située au Palais Bourbon , à Paris .
les députés ont deux grandes missions :
mais un député ne travaille pas qu' à l' Assemblée nationale .
il va aussi dans sa circonscription pour rencontrer les habitants et les différents élus , comme les maires , qui lui font part de leurs préoccupations .
on dit souvent que les députés ne représentent pas bien les français .
ce sont surtout des hommes blancs qui ont déjà fait de la politique avant et ont en moyenne soixante ans .
il y a donc très peu de femmes , de Noirs , d' ouvriers et de jeunes , par exemple .
pour ces législatives , certains mouvements politiques veulent que ça change .
ils proposent donc des personnes qui ont un parcours un peu différent et qui n' ont pas fait de politique avant .