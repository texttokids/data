que se passe t il au Royaume Uni ?
quel bazar ...
au Royaume Uni , voilà des mois qu' on ne parle que de ça , et il faut bien l' avouer : les citoyens en ont un peu marre !
leur pays aurait dû quitter l' Union européenne ( on dit aussi " ue " ) fin mars .
puis la décision a été décalée au mois d' avril .
et finalement , on devrait être fixé fin octobre .
à moins que ce soit avant ?
ou après ?
le Royaume Uni a décidé , après un vote de ses citoyens , de quitter l' UE il y a trois ans .
c' est ce qu' on appelle le Brexit : ça vient des mots anglais " British " ( " britannique " ) et " exit " ( " sortie " ) .
l' Union européenne est un groupe de vingt huit pays , dont la France fait partie .
il fallait le temps de mettre tout le monde d' accord , c' est pour ça que le Brexit ne doit avoir lieu que cette année .
c' est comme dans un divorce , quand les parents décident qui garde la maison , chez qui vont vivre les enfants , et caetera Et un divorce à vingt huit , ça demande du temps !
après de nombreuses discussions , les dirigeants de l' UE et la Première ministre britannique , Theresa May , ont signé un accord fixant les règles à suivre pour que le Royaume Uni quitte l' UE .
c' est désormais aux députés britanniques de valider ce texte pour que le Brexit soit réellement mis en route .
problème : les députés votent contre l' accord à chaque fois , et ça bloque tout !
ils ne sont pas tous d' accord sur le type de relation qu' ils souhaitent entre le Royaume Uni et les pays de l' UE une fois que leur pays aura quitté le groupe .
certains veulent rester très proches ( pour vendre et acheter facilement des marchandises , par exemple ) , d' autres veulent que les membres de l' Union européenne soient considérés comme n' importe quel autre pays .
chaque fois que les députés rejettent l' accord , tout le monde s' interroge : quand est ce que le Royaume Uni quittera vraiment l' UE ?
quelles règles seront mises en place ?
la situation reste floue ...