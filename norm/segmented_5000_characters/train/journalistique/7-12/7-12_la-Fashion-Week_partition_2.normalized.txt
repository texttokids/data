quels sont les métiers de la mode ?
" travailler dans la mode , ce n' est pas uniquement être mannequin ou DA " , c' est à dire directeur artistique , prévient Karine Piotraut , directrice du recrutement et de la gestion des carrières à l' Institut français de la mode .
il existe des dizaines de métiers différents , qu' on peut ranger en cinq catégories .
Karl Lagerfeld , Hedi Slimane , Olivier Rousteing : ces trois hommes sont connus dans le monde entier car ils sont directeurs artistiques de grandes maisons de couture ( Chanel , Dior et Balmain ) .
" les DA donnent l' inspiration de saison , explique Karine Piotraut . ils ne travaillent pas seuls , ils ont toute une équipe autour d' eux . "
les stylistes , par exemple , imaginent et dessinent des vêtements en fonction des idées du DA .
on les appelle aussi des designers .
certains sont spécialisés dans les habits pour hommes , d' autres pour femmes , d' autres encore dans les accessoires , et caetera
une fois le vêtement ou le sac imaginé et dessiné , il faut en créer un prototype , c' est à dire le premier exemplaire .
c' est le travail des modélistes .
ensuite , d' autres personnes achètent les tissus , organisent la fabrication , cousent , et caetera
les personnes qui travaillent dans ce domaine font tout pour que la collection soit très vendue .
" le chef de produit fait des études de marché ( il regarde quels vêtements se vendent beaucoup , à quel prix ... ) , définit la collection , détermine le prix de vente " , énumère Karine Piotraut .
les commerciaux doivent donner envie d' acheter une collection .
ils peuvent présenter les vêtements soit à des boutiques , soit à ce qu' on appelle des acheteurs .
" l' acheteur s' occupe d' acheter les collections pour qu' elles soient revendues . il fait les bons choix sur ce qui va fonctionner , plaire dans tel territoire ou telle boutique . il faut se mettre à la place du client final " , précise Karine Piotraut .
il y a aussi la vente en magasin .
là , il faut " animer une boutique , savoir bien accueillir , développer une relation avec le client , gérer des équipes de vendeurs " , note Karine Piotraut .
quand une collection est créée et mise en vente , il faut la faire connaître .
pour ça , il faut " faire voir les produits dans les magasins , sur les réseaux sociaux , mettre en scène les vitrines et l' intérieur des magasins " , dit Karine Piotraut .
les merchandisers visuels aménagent les boutiques et les sites de vente sur Internet .
les attachés de presse informent les journalistes de la sortie d' une nouvelle collection , organisent leur venue à des défilés , et caetera
à toi de jouer !
toi aussi tu veux dessiner des vêtements et accessoires ?
télécharge et imprime la silhouette de P' tit Libé et entraîne toi à devenir un ou une super styliste !