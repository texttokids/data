pourquoi y a t il eu des manifestations contre l' antisémitisme ?
des milliers de personnes se sont rassemblées le dix neuf février dans plusieurs villes de France pour dire non à l' antisémitisme .
être antisémite , ça veut dire rejeter les juifs , c' est à dire les personnes appartenant à cette communauté construite autour d' une religion , appelée le judaïsme .
or , personne ne devrait être rejeté pour sa religion ou ses origines culturelles .
ces manifestations ont eu lieu parce qu' il y a eu plusieurs actes antisémites ces dernières semaines .
le jour même de la mobilisation , quatre vingt seize tombes juives ont été saccagées dans un cimetière en Alsace .
le philosophe Alain Finkielkraut a , lui , été filmé en train d' être insulté dans la rue parce qu' il est juif .
des arbres plantés en mémoire d' un jeune juif tué à cause de sa religion il y a treize ans ont été coupés .
des portraits de Simone Veil , une personnalité politique très importante en France , et qui était juive , ont aussi été recouverts de croix gammées , le symbole du nazisme , dans les rues de Paris .
en vingt cent dix huit , cinq cent quarante et un actes antisémites ont été signalés à la police .
c' est deux cent trente de plus que l' année d' avant .
" ça fait vingt ans que c' est comme ça , s' agace Alain Jakubowicz , avocat et ancien président de la Licra , la Ligue internationale contre le racisme et l' antisémitisme . on en parle quand ce sont des personnalités qui sont visées mais il y a des actes antisémites comme ceux là tous les jours . "
pour Marie Anne Matard Bonucci , historienne spécialiste de l' antisémitisme , ce n' est malheureusement pas étonnant " parce que l' antisémitisme a ressurgi tout au long de l' histoire . quand un pays va mal comme c' est le cas en France aujourd'hui , les gens ont besoin de boucs émissaires . l' antisémitisme fait toujours office d' explication très commode aux problèmes des sociétés . "