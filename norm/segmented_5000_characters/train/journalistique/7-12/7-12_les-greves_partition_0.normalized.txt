numéro cinquante trois treize au dix neuf avril deux mille dix huit
les grèves
des grèves à la SNCF ou à Air France , des salariés en colère , des syndicats qui négocient ...
mais qu' est ce que ça veut dire tout ça ?
qu' est ce qu' une grève exactement ?
à quoi ça sert d' arrêter de travailler quand il n' y a plus de dialogue avec son patron ?
cette semaine , je t' explique tout sur cette action dont on entend régulièrement parler dans l' actualité .
robin , vingt quatre ans , est en grève
robin est un conducteur de train de la SNCF qui vit à Marseille , dans le Sud Est de la France .
depuis un an et demi , il transporte les voyageurs dans toute la région provençale .
le quatre avril , il s' est mis en grève pour la première fois de sa vie avec d' autres collègues .
ça veut dire qu' il a décidé d' arrêter de travailler pour montrer qu' il n' est pas d' accord avec des décisions prises pour son entreprise .
robin a dû prévenir ses chefs " deux jours avant , pour qu' ils puissent s' organiser " , explique t il .
même s' ils ne travaillent pas , les grévistes se retrouvent très souvent sur leur lieu de travail , tôt le matin .
ils organisent un " piquet de grève " , c' est à dire qu' ils se rassemblent tous ensemble pour discuter et rendre la grève visible , ce qui ne serait pas le cas s' ils restaient chez eux .
" on peut par exemple organiser une manifestation ou distribuer des tracts aux gens pour expliquer pourquoi on fait grève " , détaille Robin .
pour lui , c' est important de se retrouver avec ses collègues .
" c' est un moment stressant , alors c' est sympa d' être ensemble pour se serrer les coudes . "
chaque fin de matinée , les grévistes organisent aussi une assemblée générale .
c' est une sorte de réunion géante où les représentants des grévistes , que l' on appelle les syndicalistes , donnent des informations .
tout le monde discute , et à la fin , il y a un vote pour décider si on continue la grève ou pas .
parfois , ce n' est pas facile de continuer : les grévistes ne sont pas payés les jours où ils ne travaillent pas .
" j' avais mis des sous de côté pour partir en vacances , je vais devoir y renoncer , confie Robin . mais j' ai de la chance , car ma famille m' aide . " robin espère que la grève sera utile .
il a décidé d' aller jusqu' au bout .
Marseille , la ville où habite Robin