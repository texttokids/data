pourquoi y a t il des élections européennes ?
du vingt trois au vingt six mai , les électeurs de vingt huit pays d' Europe ( âgés d' au moins dix huit ans ) vont voter pour les élections européennes .
ça représente plus de quatre cents millions d' électeurs !
parmi eux , il y a quarante cinq , cinq millions de français qui sont appelés à voter le dimanche vingt six mai .
lors de cette élection , les citoyennes et citoyens vont choisir leurs députés européens , qu' on appelle aussi des eurodéputés .
ce sont les personnes qui les représentent au Parlement européen , l' un des organismes les plus importants de l' Union européenne .
ces femmes et hommes politiques sont élus tous les cinq ans et sont chargés de prendre des décisions qui vont s' appliquer dans les vingt huit pays qui font partie de l' Union européenne ( on dit aussi UE ) .
cette année , sept cent cinquante et un députés vont être élus .
ce chiffre varie en fonction du nombre d' habitants des différents pays .
l' Allemagne est le pays le plus peuplé d' Europe et compte quatre vingt seize députés .
la France , elle , en a soixante quatorze .
les électeurs ne vont pas voter pour une seule personne mais pour une liste de candidats définie par les partis politiques de leur pays .
en France , comme soixante quatorze députés doivent être élus , chaque liste doit donc comprendre soixante quatorze noms .
mais cette année , c' est un peu compliqué avec le Royaume Uni : le pays a décidé de quitter l' Union il y a trois ans , sauf que ça prend du temps .
donc il est pour le moment toujours dans l' UE .
quand le Royaume Uni partira , il y aura soixante treize députés européens en moins .
certains seront donc redistribués à d' autres pays .
conséquence : les électeurs français voteront pour une liste avec soixante dix neuf noms , mais seuls soixante quatorze seront , pour le moment , vraiment élus .
la première personne de la liste s' appelle la tête de liste , c' est elle qui mène la bataille pour l' élection et qu' on voit dans les médias .
l' Union européenne s' est construite petit à petit .
au début , elle s' appelait la Communauté européenne du charbon et de l' acier ( CEE ) et comptait six pays , dont la France .
ces six pays ont petit à petit été rejoints par d' autres États et la CEE est devenue l' Union européenne en mille neuf cent quatre vingt treize .
aujourd'hui , l' UE compte vingt huit pays .
l' Union européenne intervient dans plein de sujets différents comme l' éducation , l' agriculture , le commerce ou encore la protection de l' environnement .
comme ça a un effet sur la vie quotidienne de millions de personnes , c' est important que les citoyens choisissent celles et ceux qui prennent des décisions qui les concernent .
c' est pour ça que les citoyens européens élisent leurs députés depuis quarante ans .
c' est plus démocratique .