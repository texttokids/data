comment choisit on le prix d' une oeuvre ?
début octobre , l' artiste britannique Banksy a surpris tout le monde .
un de ses dessins a été vendu aux enchères pour plus d' un million d' euros .
la vente à peine terminée , une alarme s' est déclenchée , le dessin s' est mis à glisser du cadre et la moitié en est ressortie déchiquetée .
Banksy a ensuite expliqué qu' il avait installé une déchiqueteuse dans le cadre du tableau .
il dit qu' il a fait ça pour empêcher que des gens s' enrichissent en revendant son travail .
mais c' est l' inverse qui risque de se produire : son tableau devrait valoir encore plus cher maintenant .
pourquoi ?
parce que " ça participe à un moment fort de l' histoire du marché de l' art " , analyse le galeriste Kamel Mennour .
jamais un artiste n' avait déclenché une broyeuse à distance lors d' une vente , ça marquera les mémoires et ça augmente donc la valeur de l' objet .
" la valeur financière d' une oeuvre est très subjective " , explique la journaliste et critique d' art Roxana Azimi .
on ne peut pas se dire " puisque le tableau fait cinquante centimètres sur trente , il vaudra un euros " , ni " cette sculpture est plus belle que celle là , elle vaut plus cher " , car tout le monde n' a pas les mêmes gouts .
en plus , ça dépend des modes .
un artiste déjà très connu vendra son travail beaucoup plus cher que quelqu' un qui débute .
" la question , c' est " est ce que des collectionneurs importants s' y sont intéressés ?
" , indique Roxana Azimi . une oeuvre prend de la valeur quand elle entre dans une collection , comme celle de François Pinault ( un milliardaire passionné d' art ) . les collectionneurs se regardent et veulent tous la même chose au même moment . " en gros , plus les gens vous aiment , plus ça va attirer d' autres gens !
le tableau le plus cher de l' histoire a été acheté pour trois cent quatre vingt trois millions d' euros .
il s' agit d' une peinture appelée Salvator Mundi , qui aurait été peinte par Léonard de Vinci .
" c' est le prix de la passion , de quelque chose d' unique . ça arrive sur des oeuvres exceptionnelles qu' on retrouve tous les trente , quarante , cinquante ans " , affirme Mathias Ary Jan , président du Syndicat national des antiquaires .
en effet , les tableaux de Léonard de Vinci sont déjà tous dans des musées et ne sont jamais mis en vente .
et puis celui ci avait disparu de la circulation pendant des dizaines d' années , alors sa redécouverte a été un évènement important .