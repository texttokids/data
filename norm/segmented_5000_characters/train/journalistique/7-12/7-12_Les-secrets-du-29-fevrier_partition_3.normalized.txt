le vingt neuf février , un jour à part ?
puisque le vingt neuf février est rare , il est lié à de nombreuses histoires et traditions originales .
naitre un vingt neuf février , c' est déjà rare .
mais avoir des frères et soeurs nés aussi un vingt neuf février , c' est exceptionnel !
et pourtant ça existe .
en Norvège , une maman a eu trois enfants , en mille neuf cent soixante , en mille neuf cent soixante quatre et en mille neuf cent soixante huit , et ils sont tous nés ce jour si particulier .
cette femme dit avoir un petit secret pour accoucher à cette date , mais elle ne veut pas le révéler .
c' est un record mondial , codétenu par une famille américaine : dans l' Utah , une mère a accouché trois fois un vingt neuf février , en deux mille quatre , en deux mille huit et en deux mille douze .
selon une vieille légende irlandaise , les deux patrons des catholiques du pays , sainte Brigitte et saint Patrick , ont un jour conclu un accord : les femmes auront le droit de faire leur demande en mariage aux hommes tous les quatre ans , le vingt neuf février .
traditionnellement , c' étaient les hommes qui faisaient leur demande .
c' est devenu une coutume au Royaume Uni .
pour que cela soit légal , une loi a même été adoptée en mille deux cent vingt huit en Écosse .
si un homme avait le malheur de dire non à une femme , il devait payer une amende ou , dans certaines régions , offrir une paire de gants .
la femme célibataire pouvait ainsi cacher le fait qu' elle n' ait pas de bague de fiançailles au doigt .
aujourd'hui encore , même si chacun fait comme il veut , on encourage les femmes du Royaume Uni à déclarer leur flamme le vingt neuf février .
selon un proverbe paysan , les années bissextiles sont mauvaises pour les récoltes : " en l' année bissextile , garde du blé pour l' an qui suit . " en Écosse , les superstitieux pensent qu' un enfant né le vingt neuf février aura la poisse ( un peu comme un vendredi treize ) .
et en Grèce , se marier ce jour là porterait malheur .
un journal humoristique ne parait que tous les quatre ans depuis mille neuf cent quatre vingts : la Bougie du sapeur .
son nom a été choisi en hommage au héros de bande dessinée le sapeur Camember , un soldat un peu bête né un vingt neuf février .
en trente ans d' existence , seuls neuf numéros sont donc sortis .
cette année , ce sera le dixième .
quand on travaille , on gagne un salaire chaque mois .
mais que le mois de février dure vingt huit ou vingt neuf jours , la somme d' argent ne change pas .
et le dessinateur français Lukino , qui fête son anniversaire tous les quatre ans , ne trouve pas ça normal : si on travaille un jour de plus , on devrait être payé un jour de plus !
il demande donc , avec humour , que le vingt neuf février devienne un jour férié .
bon , ce n' est pas gagné : sa pétition n' a recueilli que cent trente cinq signatures .
aux États Unis , la petite ville d' Anthony organise une grande parade dans les rues chaque vingt neuf février pour fêter les anniversaires de toutes les personnes nées ce jour là .
l' idée est venue de deux habitants de la ville qui voulaient une journée spéciale pour leur anniversaire .
des gens de tout le pays et du monde entier viennent y participer .
c' est un moyen de s' en souvenir : les Jeux olympiques d' été , qui ont lieu tous les quatre ans depuis mille huit cent quatre vingt six , tombent toujours sur une année bissextile .
cette année , les JO d' été auront lieu à Rio de Janeiro au Brésil du cinq au vingt et un août .
et cela tombe aussi en même temps que l' Euro de foot ( le championnat d' Europe de football ) , organisé tous les quatre ans depuis mille neuf cent soixante .
cette année , il se déroulera en France du dix juin au dix juillet .