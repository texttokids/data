pourquoi ça ne change pas ?
entre deux mille onze et deux mille seize , cent lois pour mieux contrôler les armes ont été proposées aux États Unis , mais aucune n' a été acceptée par les élus qui votent les lois .
l' ancien président Barack Obama a essayé de changer les choses , mais il n' a pas réussi .
le président actuel , Donald Trump , lui , aime bien les armes .
il pense par exemple qu' il faut en donner aux professeurs dans tout le pays , pour qu' ils puissent se défendre si quelqu' un veut attaquer une école .
le douze mars , il a écrit sur Twitter : " si les écoles sont des zones sans armes , la violence et le danger sont invités à y entrer . "
les partis politiques s' opposent beaucoup sur la question des armes .
les Républicains , le parti de Donald Trump , sont nombreux à être pour les armes .
les Démocrates , le parti de Barack Obama et d' Hillary Clinton , sont souvent opposés aux armes .
mais " personne ne cherche à interdire les armes aux États Unis . ils veulent juste enlever les plus dangereuses " , assure Jean Éric Branaa , professeur à Paris deux un .
les armes à feu font partie de la vie des américains et de l' identité du pays .
s' il est aussi difficile de changer les lois sur les armes , c' est en grande partie à cause d' un groupe appelé la NRA ( National Rifle Association en anglais , ce qui veut dire " association nationale des armes à feu " ) .
ce groupe défend les armes .
il existe depuis près de cent cinquante ans et a un pouvoir énorme dans le pays .
plus il y a d' armes , plus la NRA gagne de dollars , car ceux qui ont des armes la soutiennent .
l' association compte cinq millions d' adhérents : c' est plus que les habitants de l' Irlande !
la NRA donne de grosses sommes d' argent à des personnalités politiques et des entreprises .
en échange , ces personnes doivent parler des armes de façon positive .
" la NRA est très puissante , très active et très organisée , explique Jean Éric Branaa . aujourd'hui , pour un élu politique , résister à la NRA , c' est pratiquement perdre son élection . " par exemple , la NRA a donné vingt quatre millions d' euros à Donald Trump pendant la campagne présidentielle .
l' association a aussi plusieurs magazines et une chaîne de télévision .
elle donne une note à chaque personne élue dans le pays pour dire si elle défend bien les armes ou pas , pour que les américains qui aiment les armes sachent pour qui voter .