l' importance des repas , une spécialité française ?
les repas sont très importants en France , en particulier pendant les fêtes .
Éric Roux , le porte parole de l' Observatoire des cuisines populaires , nous explique pourquoi .
c' est un moment où on se retrouve en famille : le frangin qui est parti à l' autre bout de la France revient , la vieille tante qu' on n' a pas vue depuis un an est là ...
on cherche des choses un peu extraordinaires parce que , sans forcément s' en rendre compte , on se dit : " l' année s' est écoulée , une nouvelle va redémarrer , on va attaquer une nouvelle page . "
dans les régions à fortes traditions de Noël ( la Lorraine , l' Alsace , le nord de la France ) , il y a plein de choses .
par exemple , tous les petits biscuits de tradition germanique : le lebkuchen , le mannele .
en Provence , il y a les treize desserts : des figues et raisins secs , du melon , la pompe à l' huile ( un gâteau à l' huile olive ) , du nougat ...
le repas gastronomique des français est inscrit au patrimoine mondial de l' Unesco .
il s' agit du repas familial , entre copains , où on mange avec un rituel : l' apéro , l' entrée , le plat principal , le fromage , le champagne , les petits trucs qu' on grignote .
en France , on a des repas qui peuvent durer trois heures , tout le monde s' engueule , tout le monde rigole .
et il existe quelque chose de très français : pendant qu' on mange , on parle de ce qu' on a mangé la dernière fois et de ce qu' on va peut être manger dans un futur proche .
la gastronomie a été le moyen de s' inventer une identité nationale , d' avoir quelque chose qui permette de dire " nous sommes français " .
pendant la guerre de dix neuf cent quatorze dix neuf cent dix huit , des gens qui venaient de cultures régionales extrêmement fortes se sont retrouvés tous ensemble pour combattre , et ils ont pu gouter plein de choses .
les gars du Nord ont gouté à des tomates , les gars des tomates ont gouté à la choucroute , et caetera L' un des éléments qui provoquent l' unification , c' est la nourriture .