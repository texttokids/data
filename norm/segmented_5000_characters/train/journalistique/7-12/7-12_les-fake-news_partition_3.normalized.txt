pourquoi cela pose t il problème ?
dans une démocratie , il est très important d' avoir accès à des informations vraies , vérifiées , sinon ça peut changer la façon dont on vit ensemble .
prenons l' exemple de la politique .
lors d' une élection , on vote en fonction de ce que propose un candidat .
pour savoir ce qu' il propose , on peut lire son programme mais aussi des articles ou des messages sur les réseaux sociaux .
si on lit une fausse information disant que Madame P' tit Libé veut obliger les enfants à manger des choux de Bruxelles le mardi , on va peut être vouloir voter pour son concurrent , Monsieur P' tit Libé , alors que ce n' est pas ce qu' on souhaitait à la base .
il faut aussi faire attention aux fake news car elles peuvent amener à croire des choses fausses sur un groupe de gens en raison de ce qu' ils sont : leur nationalité ou leur religion , par exemple .
si on a vu sur le réseau social Instagram une image trafiquée montrant que des musulmans sont en train de préparer une révolution pour interdire à tout le monde de manger du porc , on peut en vouloir aux musulmans en général de ne pas laisser les gens manger ce qu' ils veulent , alors qu' ils n' ont rien demandé .
depuis quelques années , des journalistes font ce qu' on appelle en anglais du " fact checking " , c' est à dire de la vérification des faits , d' informations .
depuis l' année dernière , ils doivent vérifier de plus en plus de fake news .
ce n' est pas toujours facile de faire la différence entre une vraie info et une fausse .
les vérificateurs sont là pour aider les personnes qui lisent les informations sur Internet à savoir si elles sont vraies ou pas .