quand pense t on avoir vu des extraterrestres ?
les passionnés d' extraterrestres connaissent tous ces deux histoires .
en France et aux États Unis , des évènements ont pu laisser croire que les extraterrestres existaient , et qu' ils étaient venus sur Terre .
le premier juillet mille neuf cent soixante cinq, il y a plus de cinquante ans , un agriculteur a été le témoin d' un évènement mystérieux .
c' était à Valensole , une commune provençale du sud est de la France .
vers cinq heures du matin , alors qu' il commençait sa journée de travail , Maurice Masse a été surpris par un gros vrombissement .
il a alors découvert , ébahi , une chose métallique en forme de ballon de rugby , posée dans son champ de lavande .
près de l' objet se trouvaient d' après lui deux petites créatures avec des têtes chauves , beaucoup plus grosses que leur corps .
Maurice Masse affirme qu' il les a ensuite vues monter dans l' engin , qui s' est envolé avant de disparaitre .
les gendarmes se sont rendus sur son champ et ont vu des traces qui donnaient l' impression qu' un engin s' était posé .
à cet endroit , le sol était très dur .
une enquête a été menée , de grands spécialistes des ovnis ( objets volants non identifiés ) se sont rendus à Valensole pour tenter d' élucider le mystère .
mais cinquante trois ans après les faits , aucune explication n' a pu être trouvée .
des ovnis se sont ils écrasés sur notre planète ?
en juin mille neuf cent quarante sept, il y a plus de soixante dix ans , des américains ont affirmé avoir aperçu neuf engins brillants en forme de soucoupe traverser le ciel à toute vitesse .
quelques jours plus tard , à Roswell , une ville du sud des États Unis , un fermier a découvert sur ses terres des débris métalliques étranges .
l' armée les a récupérés et a pris l' affaire en main .
les autorités américaines ont alors assuré qu' il ne s' agissait pas d' un véhicule extraterrestre mais d' un " ballon sonde " , un appareil utilisé pour étudier la météo .
l' histoire ne s' arrête pas là .
en mille neuf cent soixante dix huit , il y a quarante ans , le premier militaire arrivé sur place a affirmé dans une interview télévisée que les matériaux récupérés à Roswell n' avaient rien à voir avec ceux qu' utilisent les humains .
dans le monde entier , des gens ont accusé l' armée américaine d' avoir menti .
des théories du complot se sont multipliées .
une vidéo d' un cadavre d' extraterrestre qui aurait été retrouvé à Roswell s' est même mise à circuler , mais il ne s' agissait que d' un canular .
dans les années mille neuf cent quatre vingt dix , des documents secrets ont été révélés et l' armée a donné une nouvelle explication : les débris retrouvés étaient ceux d' un programme d' espionnage contre les russes , les ennemis des États Unis à cette époque .
malgré de nombreuses explications , certaines personnes continuent de croire dur comme fer qu' un véhicule extraterrestre s' est écrasé à Roswell pendant l' été mille neuf cent quarante sept ...