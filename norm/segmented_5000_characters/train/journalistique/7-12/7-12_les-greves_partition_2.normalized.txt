qu' est ce qu' une grève ?
quand des salariés font grève , ça veut dire qu' ils arrêtent de travailler pour dire qu' ils ne sont pas d' accord avec leur employeur ou pour lui réclamer des choses .
on dit que ce sont des grévistes .
ils demandent en général à être mieux payés , à avoir de meilleures conditions de travail ou défendent tout simplement leur emploi si leur patron dit qu' il va tous les licencier par exemple .
personne ne fait grève par plaisir .
les salariés ne sont pas payés quand ils font grève .
mais quand ils n' arrivent vraiment pas à se mettre d' accord avec leur employeur , ils utilisent ce moyen de pression pour obtenir des choses .
parce qu' une entreprise ne peut pas fonctionner sans ses salariés .
comme tout le monde perd de l' argent , " les grèves sont , dans la très grande majorité des cas , de très courte durée en France , elles durent très souvent moins d' un jour " , remarque Baptiste Giraud , enseignant chercheur à l' université d' Aix Marseille .
lorsqu' elles durent longtemps , plusieurs semaines ou plusieurs mois , les gens peuvent soutenir les grévistes en leur donnant de l' argent via des cagnottes prévues à cet effet .
les salariés qui font grève sont protégés par la loi : leur employeur ne peut pas les punir pour ça , en les renvoyant par exemple .
il y a quand même des conditions :
lors d' une grève , les salariés se retrouvent souvent pour discuter , pour fabriquer des banderoles , réfléchir à ce qu' il faut faire pour convaincre les autres salariés de faire grève ou encore pour manifester dans la rue .
les grèves sont , le plus souvent , organisées par des syndicats , des organisations de salariés chargées de défendre les autres travailleurs .
pendant la grève , les syndicats cherchent à trouver un accord avec la direction .
on appelle ça des négociations .
les syndicats disent ensuite aux salariés ce qu' ils ont obtenu ou non .
si la majorité des salariés n' est pas d' accord avec ces conditions , la grève continue et les négociations recommencent .
si un accord est trouvé , tout le monde reprend le travail .