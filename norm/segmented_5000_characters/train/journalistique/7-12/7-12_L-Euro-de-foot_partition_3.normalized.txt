pourquoi le foot est il si populaire ?
on parle beaucoup de l' Euro parce qu' il s' agit d' une compétition très importante , comme la Coupe du monde , et qu' elle a lieu en France cette année .
ce n' est pas très difficile de jouer au foot : il faut un ballon et c' est parti !
on peut faire des buts avec deux pulls roulés en boule , jouer sur une plage , de l' herbe ou du béton .
du coup , de nombreux enfants , dans quasiment tous les pays du monde , jouent au foot avec leurs copains , leurs voisins , leurs camarades de classe ...
voilà pourquoi le foot a autant de succès : tout le monde a déjà tapé dans un ballon .
les compétitions internationales de football sont organisées depuis longtemps : la première Coupe du monde a eu lieu en dix neuf cent trente , en Uruguay ( en Amérique du Sud ) , c' est à dire il y a presque un siècle .
la finale de l' Euro vingt cent seize , le dix juillet , rassemblera quatre vingts personnes au Stade de France , à Saint Denis , au nord est de Paris .
c' est beaucoup , et il existe des stades de foot plus grands encore : celui de Barcelone , le Camp Nou , peut accueillir quatre vingt dix neuf spectateurs .
il y aura aussi des centaines de millions de téléspectateurs , partout dans le monde , qui regarderont cette finale en direct .
il y a deux ans , environ un milliard de personnes ont regardé la finale de la Coupe du monde entre l' Argentine et l' Allemagne : ça fait un Terrien sur sept !
aucun autre évènement , sportif ou non , ne rassemble autant de personnes en même temps .
le football n' est pas le seul sport très populaire .
dans certains coins de la planète , d' autres sports rassemblent un monde fou au stade ou devant la télévision .
par exemple , en Nouvelle Zélande , on adore le rugby .
en Inde et au Pakistan , on ne jure que par le cricket , un sport qui ressemble un peu au base ball : lors de la dernière Coupe du monde , l' an dernier , environ un milliard de personnes ont regardé le match Inde Pakistan .
aux États Unis , les cinq cents Miles d' Indianapolis , une course automobile mythique , peut rassembler jusqu'à quatre cents spectateurs autour du circuit .
quant au Super Bowl , la finale du championnat de football américain ( qui ressemble un peu au rugby ) , il rassemble chaque année environ cent millions de personnes devant le petit écran , c' est à dire un américain sur trois .