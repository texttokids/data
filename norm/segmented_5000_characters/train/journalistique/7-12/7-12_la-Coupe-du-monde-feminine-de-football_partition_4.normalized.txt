quelles sont les grandes figures féminines du football ?
elles sont joueuses , entraîneures ou commentatrices .
elles ont marqué et marquent encore l' histoire du football .
qui sont elles ?
Marta Vieira Da Silva , la star mondiale
âge : trente trois ans
nationalité : brésilienne
poste : attaquante
la brésilienne Marta Vieira Da Silva est une légende du football .
elle a été élue six fois meilleure joueuse du monde .
née dans une région pauvre du Brésil , elle a appris le football dans la rue et s' est fait repérer à quatorze ans .
la jeune joueuse a intégré trois ans plus tard l' équipe du Brésil puis a été recrutée par des clubs européens .
la meilleure buteuse de l' histoire de la Coupe du monde n' a , à son grand désespoir , jamais remporté la compétition .
Marinette Pichon , une modèle en France
âge : quarante trois ans
nationalité : française
poste : ex attaquante
Marinette Pichon a débuté le football très tôt , à l' âge de cinq ans , au début des années mille neuf cent quatre vingts .
à une époque où très peu de filles pratiquaient ce sport , elle a fait face aux remarques sexistes .
elle s' est ensuite envolée pour les États Unis où elle faisait partie des meilleures joueuses .
à ce jour , cette ex footballeuse détient encore le record de buts marqués en équipe de France .
Marinette Pichon est aussi l' une des premières femmes à avoir commenté des matchs à la télévision .
Ada Hegerberg , la première footballeuse en or
âge : vingt trois ans
nationalité : norvégienne
poste : attaquante
Ada Hegerberg n' a que vingt trois ans et elle multiplie les victoires .
avec le club de foot de l' Olympique lyonnais , elle a remporté trois titres très importants dans le monde du foot : un en Ligue des champions ( le championnat européen ) , quatre en championnat de France et trois en coupe de France .
l' an dernier , l' attaquante a décroché le premier Ballon d' or féminin , qui récompense la meilleure joueuse du monde .
même si la Norvège est qualifiée pour la Coupe du monde , Ada Hegerberg a annoncé qu' elle ne jouerait pas pour protester contre les inégalités de traitement entre les footballeuses et les footballeurs .
ces derniers sont notamment bien mieux payés que les femmes .
laure Boulleau , l' experte
âge : trente deux ans
nationalité : française
poste : commentatrice
ex joueuse du Paris Saint Germain ( PSG ) , Laure Boulleau n' évolue plus sur les terrains de foot mais sur les plateaux de télévision .
elle commente le football masculin dans l' émission du Canal Football Club et fait d' ailleurs partie des rares femmes à le faire .
elle travaille aussi pour l' équipe féminine du PSG : elle y recrute les futures joueuses , observe les matchs et fait le lien entre son équipe et la presse .
Corinne Diacre , la sélectionneuse
âge : quarante quatre ans
nationalité : française
poste : entraîneure
sélectionneuse de l' équipe de France depuis deux ans , Corinne Diacre est l' équivalent de Didier Deschamps , l' entraîneur de l' équipe de France masculine .
elle a d' ailleurs , comme lui , annoncé la liste des vingt trois joueuses sélectionnée pour la compétition lors du journal télévisé de vingt heures , en mai dernier .
une première pour le football féminin .
Corinne Diacre est aussi connue car elle est la première femme à avoir entrainé une équipe de football masculine et professionnelle .