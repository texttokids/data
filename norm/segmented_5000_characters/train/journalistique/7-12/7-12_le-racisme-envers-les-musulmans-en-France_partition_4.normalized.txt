comment ça se passe à l' école ?
en France , l' école publique est laïque .
les règles ne sont pas les mêmes pour les élèves , les enseignants et les parents .
depuis quinze ans , une loi interdit aux élèves des écoles , collèges et lycées publics de porter des signes qui montrent clairement leur religion , comme le voile musulman , la kippa juive ou une grosse croix chrétienne .
les élèves peuvent quand même porter des signes religieux tant qu' ils sont discrets , comme une petite croix ou une petite étoile de David .
à l' université , c' est différent : les étudiants sont plus grands et ont le droit d' afficher leurs croyances .
cette loi à l' école a été mise en place en vingt cent quatre pour " protéger les enfants et les adolescents des pressions entre élèves . mais aussi pour qu' ils puissent développer librement leur esprit critique et leur propre opinion au moment où ils apprennent la base des connaissances " , explique Nicolas Cadène de l' Observatoire de la laïcité .
les élèves peuvent tout de même parler de religion entre eux ( dire qu' ils vont à l' église le dimanche ou qu' ils ne croient pas en dieu , par exemple ) tant qu' ils ne passent pas leur temps à en parler et qu' ils ne forcent pas les autres à penser comme eux .
aucun élève ne peut refuser de suivre un enseignement ou une consigne sous prétexte que sa religion ou ses idées le lui interdisent .
les enseignants n' ont pas le droit de porter des signes ou des tenues qui permettent de savoir s' ils sont croyants , ni même de dire s' ils le sont ou pas .
ils ne doivent pas non plus poser la question aux élèves .
ils représentent l' Etat et pas leur simple personne et doivent donc être neutres .
ils peuvent quand même parler de religion " pour ce qui relève du savoir , c' est à dire ce que l' on sait de manière objective des religions , comme leur histoire ou quelles sont les différentes religions dans le monde " , poursuit Nicolas Cadène .
toutes ces règles sont rappelées dans un texte qui s' appelle la charte de la laïcité à l' école et qui est affiché dans les établissements scolaires publics .
le port des signes religieux dans l' espace public n' est interdit par aucune loi .
les parents sont donc libres de porter le voile , la kippa ou une croix quand ils vont chercher leurs enfants à l' école , comme quand ils se promènent dans la rue .
c' est la même chose quand ils accompagnent les enfants lors des sorties scolaires à l' extérieur de l' établissement , parce que ce ne sont pas des enseignants .
ils sont là pour aider .