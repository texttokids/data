numéro quarante sept deux au huit mars deux mille dix huit
la SNCF
la SNCF , l' entreprise qui fait rouler les trains en France , est en ce moment au coeur de l' actualité .
le gouvernement souhaite changer des choses dans la façon dont elle fonctionne , et des gens qui y travaillent ne sont pas contents .
qui sont ces " cheminots " dont on parle tant ?
comment fonctionne la SNCF et quelle est son histoire ?
pour tout comprendre , monte à bord du P' tit Libé Express !
jean philippe est conducteur de train
un peu avant dix huit heures , Jean Philippe commence sa journée de travail .
depuis dix ans , il conduit des trains .
avant de faire démarrer son véhicule , cet homme de trente huit ans se rend dans les bureaux de la gare de l' Est , à Paris , pour récupérer son emploi du temps .
son train ( appelé Transilien ) doit quitter la gare à dix huit heures vingt et un et rouler jusqu'à Château Thierry , une ville située à une centaine de kilomètres de Paris .
il a treize minutes pour préparer sa journée avant de rejoindre son engin .
" tout petit , je voulais conduire des trains . c' est un rêve de beaucoup d' enfants " , se souvient Jean Philippe , qui jouait avec un train électrique quand il était plus jeune .
il faut dire qu' il connaissait déjà bien cet univers : son père , son grand père et son arrière grand père , notamment , étaient cheminots .
" la ligne , on doit la connaître par coeur . on doit connaître les gares , les vitesses , les trains " , explique Jean Philippe .
son Transilien roule la plupart du temps à cent quarante kilomètres par heure , mais parfois la vitesse est moins élevée , par exemple quand il y a des travaux sur la ligne .
à cette vitesse et avec un véhicule aussi grand , il faut beaucoup de temps pour s' arrêter .
jean philippe doit commencer à freiner un kilomètre ( la longueur de dix terrains de foot ) avant chaque gare pour réussir à s' arrêter devant les voyageurs qui attendent .
pour savoir quand commencer à freiner , il a ses repères , comme une maison originale sur le bord de la route ou un tunnel .
si quelqu' un est trop proche du bord du quai , il déclenche un gros klaxon .
qu' est ce qui lui plait dans ce métier ?
" être autonome , répond Jean Philippe . et je ne fais jamais deux fois la même chose . " à chaque trajet , il transporte au moins un personnes , parfois deux .
ce jour là , il commence le travail à dix huit heures mais parfois c' est à trois heures , à midi ou encore à vingt heures .
ça change tout le temps .
huit à dix fois par mois , il ne dort pas chez lui mais dans la ville où il termine son trajet .
des logements sont prévus pour les conducteurs .
comment fait il pour avoir une vie de famille , avec sa femme et sa fille de vingt mois ?
" c' est compliqué , sourit il . mais je travaille souvent en soirée donc je passe toute la journée avec ma fille . " même si , dans sa famille , on est cheminot depuis plusieurs générations , Jean Philippe l' assure : ça ne le dérangera pas si sa fille ne suit pas ses pas .