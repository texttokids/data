d' où viennent les profs ?
avant d' arriver devant une salle de classe , les enseignants , eux aussi , sont sur les bancs de l' école .
pour devenir professeur , en primaire ou au collège , ils doivent d' abord obtenir une licence , le diplôme qu' on passe après trois années d' études à l' université .
en général , les professeurs de collège ont choisi une licence qui correspond à la matière qu' ils enseignent .
beaucoup de professeurs de français , par exemple , ont étudié la littérature à l' université .
après ces trois ans d' études , ils passent deux ans dans une école de professeurs , à l' université .
ils y apprennent à proposer des activités accessibles à des écoliers ou à des collégiens dans différentes matières , ou à gérer une classe .
par exemple , ils font des exercices de respiration : ils s' entraînent à parler fort sans pour autant crier .
c' est une habitude à prendre , d' ailleurs les jeunes professeurs n' ont parfois plus de voix à la fin de la journée !
à l' école , les futurs profs découvrent aussi comment la mémoire fonctionne .
on leur déconseille par exemple de donner trop d' informations en même temps , sinon les élèves n' arrivent pas à les retenir .
dans leur emploi du temps , il y a aussi des cours sur les valeurs de l' école , comme la laïcité .
parce que c' est un principe important en France : l' Etat n' interdit aucune religion et n' oblige personne à en pratiquer une .
l' école publique , qui est gérée par l' Etat , est donc laïque .
durant leurs études , les futurs enseignants vont aussi dans des classes pour voir comment se passent les cours .
au début , ils s' installent au fond de la salle pour observer un professeur face à ses élèves .
puis c' est à leur tour de faire cours aux élèves , qui ne savent pas qu' ils ne sont pas vraiment encore des professeurs .
cette fois ci , un tuteur les observe : c' est un enseignant qui les évalue et les conseille .
sans rien imposer , il peut leur donner des astuces , comme passer dans les rangs , regarder les élèves dans les yeux ou parler fort pour se faire respecter .
pour être diplômé , et donc devenir professeur , il ne suffit pas d' entrer dans cette école .
il faut réussir un concours à la fin de la première année , puis un examen à la fin de la deuxième .