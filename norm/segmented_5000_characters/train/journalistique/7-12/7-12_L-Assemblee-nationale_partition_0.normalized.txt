numéro vingt deux juin deux mille dix sept
l' Assemblée nationale
un peu plus d' un mois après avoir élu le président de la République , les électeurs français sont appelés à voter , les dimanches onze et dix huit juin , pour choisir les députés .
à quoi servent ils au juste ?
comment crée t on une loi ?
que se passe t il exactement à l' Assemblée nationale ?
et le Sénat , dans tout ça , à quoi sert il ?
" le P' tit Libé " vous explique tout .
Elsa , dix huit ans , veut devenir députée
Elsa a le droit de vote depuis cette année seulement et elle est déjà candidate aux élections législatives .
elle souhaite en effet devenir députée , c' est à dire représenter les citoyens à l' Assemblée nationale et voter les lois , autrement dit les règles de la vie en société .
cette étudiante à l' université de Grenoble est la plus jeune candidate de La France insoumise , le mouvement politique de gauche de Jean Luc Mélenchon .
Elsa veut être députée pour que les jeunes aient leur mot à dire à l' Assemblée nationale .
" les dix huit vingt cinq ans constituent dix pour cents de la population mais ils ne sont pas représentés parmi les députés . la moyenne d' âge des députés est de cinquante neuf ans , remarque t elle . je pense que ma jeunesse est une force . les jeunes sont capables de faire de la politique . j' en suis la preuve . "
quand on est candidat aux législatives , on se présente sur un petit territoire de la France qui s' appelle une circonscription .
Elsa se présente dans la première circonscription de l' Isère , où il y a presque quatre vingt dix électeurs .
ça fait beaucoup de gens à convaincre .
" j' ai été désignée candidate le dix neuf mai après un vote des militants de mon parti dans ma circonscription . depuis , je suis suivie de près pour tout préparer " , explique Elsa .
elle vient heureusement de terminer sa première année à l' université et peut se consacrer aux élections à temps plein .
pour convaincre les gens de voter pour elle , elle travaille avec une équipe d' une dizaine de personnes et de très nombreux bénévoles .
il faut aller à la rencontre des électeurs , distribuer des tracts sur les marchés et coller des affiches .
Elsa doit aussi répondre aux interviews des journalistes , intervenir dans des débats , réviser ses notes .
la jeune femme a commencé à militer pour La France insoumise en février .
avant , elle s' intéressait à la politique parce que ses parents en parlaient à la maison , mais elle n' était pas militante .
" si je suis élue , j' en serais très fière " , conclut Elsa .
l' Isère , le département où Elsa se présente