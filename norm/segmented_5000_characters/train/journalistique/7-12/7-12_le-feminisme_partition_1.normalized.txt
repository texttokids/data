que s' est il passé depuis l' affaire Weinstein ?
il y a un an , en octobre deux mille dix sept, un scandale a éclaté dans le monde du cinéma .
des articles de journaux ont révélé que Harvey Weinstein , un célèbre producteur de films américain , avait fait des choses très graves , punies par la loi , à des femmes .
en tout , une centaine de personnes , parmi lesquelles des actrices et des mannequins célèbres , l' ont accusé de les avoir agressées sexuellement ou de leur avoir très mal parlé .
on a découvert que ça faisait des dizaines d' années qu' il faisait ça .
beaucoup de gens étaient au courant , mais ils ont gardé le silence car Harvey Weinstein était très puissant dans le milieu du cinéma .
quand cette affaire a été révélée , des gens du monde entier ont été choqués et en colère .
ça a donné à de très nombreuses femmes , connues ou inconnues , le courage d' oser dire que des hommes leur avaient fait du mal à elles aussi .
beaucoup ont témoigné sur le réseau social Twitter .
elles ont accompagné leurs messages des hashtags balancetonporc et metoo ( " moi aussi " en anglais ) .
depuis , des femmes ont osé porter plainte , alors qu' elles se taisaient parfois depuis des années , par honte ou par peur qu' on ne les croie pas .
aux États Unis , des hommes accusés d' avoir fait du mal à des femmes ont perdu leur travail ( des animateurs de télévision , des hommes politiques , des acteurs ...

depuis le début de cette affaire , on parle plus de féminisme que les années précédentes .
le féminisme , " c' est deux choses , explique la militante Caroline De Haas . c' est d' abord se rendre compte que les femmes et les hommes ne sont pas égaux . la deuxième chose , c' est avoir envie de changer ça " .
les hommes comme les femmes peuvent être féministes .
certaines personnes trouvent que les féministes exagèrent , qu' il n' y a plus d' inégalités entre les femmes et les hommes , que les hommes devraient continuer à pouvoir séduire les femmes comme ils le veulent , que les femmes n' ont pas besoin de travailler autant que les hommes , et caetera Le féminisme crée parfois du rejet et donc des débats .
il y a un an , le P' tit Libé avait consacré un numéro à l' affaire Weinstein et au harcèlement sexuel .
profites en pour le lire ou le relire .