comment ce festival est il né ?
de mille neuf cent quarante sept à aujourd'hui , voici quelques grandes dates à retenir sur le Festival d' Avignon .
jean Vilar , comédien et metteur en scène , a créé le festival en mille neuf cent quarante sept , il y a soixante et onze ans .
il voulait rendre le théâtre plus accessible pour que tout le monde en profite .
" avant , c' était surtout pour les gens riches et les Parisiens . jean Vilar voulait des spectacles en dehors de Paris et pour tous " , raconte Olivier Py , le directeur du Festival d' Avignon aujourd'hui .
en septembre mille neuf cent quarante sept, trois pièces ont été présentées .
la première s' appelait la Tragédie du roi Richard le deuxième , une pièce d' un écrivain anglais très connu qui s' appelle William Shakespeare .
l' année suivante , l' événement a pris le nom de " festival d' Avignon " et il s' est déroulé en juillet comme aujourd'hui .
en mille neuf cent soixante six , il y a cinquante deux ans , le Festival d' Avignon a connu beaucoup de nouveautés .
il s' est ouvert à la danse et à des spectacles venus du monde entier .
c' est à partir de cette date que les spectacles ont aussi commencé à être un peu partout dans la ville .
peu de temps après , certaines pièces ont aussi été jouées sans demander l' accord du directeur du festival .
c' est le tout début du " off " d' Avignon .
en deux mille trois , le Festival a été annulé .
les gens qui travaillaient dans le théâtre , comme les comédiens et les techniciens , n' étaient pas contents car leurs conditions de travail devenaient très mauvaises et ils voulaient le faire savoir .
il y avait beaucoup de manifestations dans la ville , et finalement le directeur a annoncé qu' il n' y aurait pas de Festival .
" grâce à cette mobilisation , le statut d' intermittent a été sauvé " , explique l' historien Antoine de Baecque .
c' est la seule année où l' événement a été annulé .