qu' est ce que l' antisémitisme ?
l' antisémitisme , c' est quand quelqu' un porte un jugement à l' encontre des juifs ou des gens qu' il considère comme étant juifs .
c' est une forme de racisme .
ça va des préjugés , c' est à dire des idées toutes faites , qui supposent que les juifs sont tous pareils , à la haine des juifs .
l' antisémitisme est d' abord apparu pour une raison religieuse .
" les premiers chrétiens reprochaient aux juifs de ne pas avoir reconnu Jésus Christ comme l' envoyé de Dieu . ils les accusaient aussi d' être à l' origine de la mort du Christ " , raconte Marie Anne Matard Bonucci , historienne spécialiste de l' antisémitisme .
les juifs n' étaient pas du tout d' accord avec ça .
dès le Moyen Âge , ils étaient accusés d' empoisonner les puits ou de tuer les enfants , ce qui était complètement faux .
des juifs ont été chassés de leurs pays , n' ont pas pu exercer certains métiers ou ont été regroupés dans des ghettos , des quartiers où ils vivaient entassés et très pauvrement .
" à partir du XVIIIe ( dix huitième ) siècle et surtout au XIXe ( dix neuvième ) siècle , un antisémitisme très violent est né dans de nombreux pays d' Europe " , raconte Marie Anne Matard Bonucci .
d' abord , les juifs étaient accusés d' avoir provoqué la Révolution française de dix sept cent quatre vingt neuf parce qu' elle leur avait permis de devenir des citoyens comme les autres .
ensuite , les gens pensaient qu' ils étaient très riches et puissants .
la très grande majorité des juifs d' Europe étaient pourtant pauvres , " mais une toute petite partie avaient fait fortune dès le Moyen Âge dans la finance parce que les professions de commerce de l' argent étaient interdites aux chrétiens . c' est à ce moment là qu' est née l' une des plus grandes légendes de complot de l' histoire qui existe toujours : l' idée du juif monde , qui domine la planète " , poursuit Marie Anne Matard Bonucci .
l' antisémitisme était présent dans la littérature , au cinéma , dans les bandes dessinées ...
au XXe ( vingtième ) siècle , le dictateur allemand Adolf Hitler a repris toutes les accusations antisémites précédentes en disant que les juifs faisaient le malheur de l' humanité et que , pour que ça s' arrête , il fallait tous les tuer .
c' est à cause de ces idées horribles que les nazis ont exterminé six millions de juifs en Europe entre dix neuf cent quarante et un et dix neuf cent quarante cinq .
on appelle cet évènement la Shoah .
face à toutes ces persécutions et ces massacres , de nombreux juifs sont allés habiter loin de chez eux , notamment en Israël , dès la fin du XIXe ( dix neuvième ) siècle .
" comme ils ont vu qu' ils n' étaient jamais en sécurité nulle part , beaucoup se sont installés là bas après la Seconde Guerre mondiale . pour les juifs , Israël est la terre de leurs ancêtres , celle que Dieu leur a réservée . mais sur cette terre il y avait aussi un autre peuple , les palestiniens " , dit l' historienne Sandrine Mirza .
aujourd'hui , certains actes antisémites sont notamment liés à ce territoire que se disputent les Israéliens et les palestiniens .