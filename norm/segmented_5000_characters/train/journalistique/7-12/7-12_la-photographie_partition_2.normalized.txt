à quoi ressemble le métier de photographe ?
" être photographe , ce n' est pas juste faire une photo avec son smartphone , remarque Anne Fourès , responsable du pôle pédagogie des Rencontres d' Arles . il faut avoir quelque chose à dire . ce sont souvent des années de réflexion . il faut travailler son sujet , se documenter ... c' est un métier . "
un photographe peut décider lui même ce qu' il souhaite photographier ou effectuer ce qu' on appelle " une commande " .
ça veut dire qu' une entreprise le paye pour montrer quelque chose de précis .
un journal va par exemple commander des photos pour un reportage et une agence de publicité des images pour mettre en valeur un produit , comme des baskets .
ensuite , les photographes ont souvent une spécialité .
il y a des photoreporters , des portraitistes , des photographes de mode , des photographes culinaires ...
le P' tit Libé a donné la parole à trois d' entre eux .
" le photoreporter est avant tout un journaliste . il doit apporter une information , raconter et écrire avec l' image " , dit Albert , photographe depuis trente quatre ans .
il a travaillé pendant vingt cinq ans pour une agence de photos et est allé dans de nombreux pays , comme en Afghanistan ou au Rwanda .
désormais , il travaille en France pour le journal Libération un .
" je peux être envoyé à l' Elysée , aller dans un camp de réfugiés ... c' est très varié " , explique ce photographe qui aime par dessus tout " la liberté " que lui apporte son métier .
marie tire le portrait des gens depuis trois ans .
elle a par exemple photographié le styliste Jean Paul Gaultier ou le politicien Jean Luc Mélenchon .
" j' ai en général dix minutes avant de prendre la personne en photo . je dois donc vite réussir à comprendre qui elle est pour savoir comment la mettre en valeur " , explique t elle .
marie retouche ensuite un peu les photos pour modifier les couleurs ou enlever les petits défauts .
" il faut être passionné pour faire ce métier parce qu' on ne gagne pas beaucoup d' argent . mais ça apporte bien plus que ça . ça m' a permis d' aller vers les autres . "
Christine travaille depuis trente ans plus dans la savane africaine , au Kenya .
elle photographie des lions , des guépards , des éléphants ...
" la photographie animalière , c' est d' abord observer le comportement animal , remarque t elle . ce sont des heures de patience , d' attente , pour témoigner de ce qu' il se passe dans la nature . " elle prend des photos avec de très gros téléobjectifs , qui permettent de photographier de très loin les animaux , sans les déranger .
un Le P' tit Libé appartient au journal Libération .