pourquoi la lune nous attire tant ?
depuis mille neuf cent soixante douze , plus aucun humain n' a marché sur la Lune .
alors est ce qu' on peut dire que , depuis tout ce temps , les scientifiques se sont désintéressés d' elle ?
non , car de nombreuses missions ont ensuite consisté à l' explorer , avec des sondes envoyées en orbite , c' est à dire autour de la Lune .
aujourd'hui , la Lune continue d' intéresser les scientifiques et de nouvelles missions sont en cours .
en janvier deux mille dix neuf, les chinois ont réussi à poser un engin spatial pour la première fois sur la face cachée de la Lune .
ils préparent maintenant la mission Chang' e cinq , au cours de laquelle ils prévoient de récupérer des échantillons de sol lunaire .
les États Unis espèrent revenir sur la Lune d' ici deux mille vingt quatre .
c' est la mission Artémis menée par la Nasa .
objectif : envoyer un homme mais aussi , pour la première fois , une femme .
" il est très important de montrer que les femmes ont leur rôle à jouer dans cette conquête spatiale " , remarque Bernard Foing , astrophysicien à l' Agence spatiale européenne .
cette mission est aussi une première étape vers la conquête de Mars où plusieurs robots ont déjà été envoyés , mais aucun être humain .
enfin , plusieurs pays discutent afin d' installer une station de recherche sur la Lune .
pas besoin d' amener des briques depuis la Terre pour la construire !
des scientifiques de l' Agence spatiale européenne font des expériences pour voir si on pourrait utiliser le sable lunaire pour faire des murs , en le chauffant pour qu' il soit compact .
un peu comme on fait des châteaux sur la plage avec du sable mouillé !
" il s' agirait d' un avant poste de l' humanité . on pourrait y étudier la Lune , ses roches , ses paysages , en y installant des télescopes notamment " , explique Bernard Foing .
la Lune , un symbole de la théorie du complotSelon certains , Neil Armstrong n' aurait jamais marché sur la Lune .
c' est la plus célèbre des théories du complot , des histoires censées prouver que des personnes puissantes nous mentent et cachent des choses .
sur les images des premiers pas sur la Lune , on ne voit pas d' étoiles dans le ciel et le drapeau a l' air de flotter ( alors qu' il n' y a pas d' atmosphère sur la Lune ) .
pour les complotistes , c' est la preuve d' une mise en scène , comme au cinéma .
mais la Nasa a des explications très claires : le drapeau avait été accroché à une tige horizontale .
et on ne voit pas non plus les étoiles parce que le Soleil brillait fort et que son éclat masquait forcément le leur , beaucoup plus faible .