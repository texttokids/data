numéro trente sept vingt deux au vingt huit décembre vingt cent dix sept
les repas de fêtes
au moment des fêtes de fin d' année ( à Noël et au nouvel an ) on mange généralement des plats différents de d' habitude .
l' ambiance est à la fête , on retrouve des gens qu' on ne voit pas forcément souvent , donc on en profite pour préparer des repas exceptionnels .
d' où vient cette habitude de manger de la dinde et de la buche à Noël ?
pourquoi les repas sont ils si importants en France ?
comment préparer un super gouter de Noël ?
suis moi derrière les fourneaux !
jean , six ans , fait des biscuits de Noël
jean adore préparer des butter bredele .
ce sont des petits gâteaux de Noël alsaciens .
ce garçon de six ans habite en Bretagne mais sa mère vient d' Alsace , c' est elle qui lui a appris à les faire .
il en cuisine de temps en temps pour ses parents et sa grande soeur .
il en offre aussi à son maître en fin d' année , accompagnés d' autres types de biscuits de Noël alsaciens , comme les noisettins .
quand il sera grand , Jean aimerait bien être pâtissier .
" j' aime bien malaxer , mélanger , mettre les ingrédients " , précise t il .
à la maison , son père prépare les plats et sa mère les desserts .
et vu qu' il préfère les desserts , il copie sa maman ...
il ne mange pas toujours le même repas à Noël .
parfois il y a du chapon ( une grosse volaille ) , parfois des huitres , parfois de la buche faite par sa tante ...
le menu de cette année n' est pas encore choisi , mais c' est sa grand mère qui préparera la majeure partie du repas et lui les butter bredele pour toute la famille .
pour sa recette , Jean utilise :
" je mélange , j' étale la pâte et ensuite je fais des formes avec des choses en métal " , explique Jean .
il utilise des objets qu' on appelle des emporte pièce , en forme de sapin ou de père Noël .
" ensuite je mets du jaune d' oeuf dessus et on met au four , poursuit Jean . j' aime bien quand je peux manger la pâte . "
Ploemeur , la ville où habite Jean