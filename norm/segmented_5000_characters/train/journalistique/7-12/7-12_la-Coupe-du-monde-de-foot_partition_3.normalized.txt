que représente la Coupe du monde ?
la Coupe du monde de foot est la plus grande compétition sportive .
remporter cette compétition est quelque chose d' unique pour un pays .
quand l' équipe de France l' a remportée le douze juillet mille neuf cent quatre vingt dix huitface au Brésil , les français étaient descendus dans la rue .
ils klaxonnaient , ils criaient car ils étaient heureux .
quand on perd un match de Coupe du monde , ça peut être un cauchemar pour certains pays , comme le Brésil .
en deux mille quatorze , l' équipe du Brésil a été éliminée en demi finale ( c' est juste avant la finale ) par l' Allemagne sur le score de sept un .
dans ce pays , le football est une passion .
cette défaite a rendu les Brésiliens tristes , surtout que la Coupe du monde se déroulait chez eux !
ils pleuraient et déchiraient les tickets du match à la sortie du stade .
la Coupe du monde permet aussi à des pays moins puissants , moins riches , de battre ceux qui ont d' habitude plus de poids .
l' Uruguay , par exemple , n' est pas un grand pays .
pourtant , il a quand même gagné deux Coupes du monde ( en mille neuf cent trente et mille neuf cent cinquante ) , alors que la France , qui est plus puissante , en a gagné une seule ( en mille neuf cent quatre vingt dix huit ) .
mais en football , ce n' est pas parce qu' un pays est considéré comme moins important qu' il ne gagnera pas .
tout dépend des joueurs : est ce qu' ils sont en forme ?
mais surtout , est ce qu' ils arrivent à bien jouer ensemble ?
en plus , durant la Coupe du monde , les petits pays ont encore plus envie de gagner .
ils veulent montrer qu' ils existent , qu' ils sont capables de battre les pays les plus puissants du monde .
" cela crée de belles histoires , comme dans un conte " , explique Sylvain Kahn , professeur d' histoire et de géographie à Sciences Po Paris .
toutes ces histoires sont partagées et vécues par des milliards de personnes partout dans le monde , c' est pour ça qu' on dit du football qu' il est un sport universel .
pendant un mois , des Brésiliens , des russes , des marocains ou encore des français vont encourager leur pays .
si la Coupe du monde rassemble autant de personnes , c' est parce qu' il s' agit d' un sport excitant et captivant .
et puis , les règles sont simples , et on peut jouer au foot un peu partout .
il suffit d' avoir un ballon !