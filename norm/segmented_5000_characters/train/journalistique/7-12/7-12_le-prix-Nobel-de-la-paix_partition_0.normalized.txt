numéro vingt six six au douze octobre vingt cent dix sept
le prix Nobel de la paix
le prix Nobel de la paix a été remis ce vendredi six octobre à Oslo , en Norvège .
il récompense chaque année des gens qui participent à faire la paix dans le monde .
mais qu' est ce que c' est , le prix Nobel de la paix , et d' où ça vient ?
qui sont les gagnants les plus célèbres ?
et pourquoi le résultat ne fait il pas toujours plaisir à tout le monde ?
je t' explique tout .
Kehkashan , dix sept ans , a reçu un prix de la paix
Kehkashan vient des Émirats arabes unis , à plus de cinq kilomètres de la France .
aujourd'hui , elle vit au Canada et est élève en terminale .
en vingt cent seize , elle a reçu le Prix international de la paix des enfants , pour ses actions en faveur de l' environnement .
une sorte de prix Nobel de la paix pour les jeunes .
Kehkashan a commencé à protéger l' environnement très jeune , à huit ans seulement .
après avoir vu la photo d' un oiseau mort à cause de la pollution ( il avait mangé du plastique ) , elle a décidé de défendre la nature en plantant un arbre .
puis elle a encouragé les gens autour d' elle à protéger la planète .
ses actions ont été remarquées , et pas seulement à Dubaï , la ville où elle habitait : à l' âge de onze ans , elle a été invitée à un grand évènement organisé par le programme des Nations unies pour l' environnement .
à douze ans , elle a pris la parole lors d' une grande réunion internationale au Brésil .
elle était la plus jeune .
" notre futur était en train de se décider et même si nous , les enfants , étions les principaux intéressés , nous n' étions que des observateurs . j' ai décidé que ça devait changer " , raconte t elle .
la jeune Emiratie a ensuite créé sa propre association , Green Hope , désormais présente dans plusieurs pays , comme les Émirats arabes unis , Oman , le Népal , l' Inde et le Canada .
" je suis convaincue que les enfants sont tout à fait capables de faire ce qu' un adulte peut faire , assure t elle . j' ai l' impression que mon travail ne fait que commencer . "
elle a même écrit un livre pour les enfants , The Tree of Hope ( ça veut dire " l' arbre de l' espoir " en français ) .
il raconte l' histoire d' une petite fille qui plante des arbres dans le désert pour le transformer en oasis .
Kehkashan vient des Émirats arabes unis et habite au Canada