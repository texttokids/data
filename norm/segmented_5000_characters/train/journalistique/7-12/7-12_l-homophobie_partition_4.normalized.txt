comment lutter contre l' homophobie ?
pour lutter contre l' homophobie , il faut en parler dès l' école .
selon Joël Deumier , le président de l' association SOS Homophobie , il faut pour ça bien former les adultes qui encadrent les enfants pour identifier les cas d' homophobie et ne pas laisser les enfants seuls si ça leur arrive .
parce que les conséquences peuvent être graves : " la victime peut décrocher à l' école , avoir de moins bonnes notes , être moins attentive en cours , avoir moins confiance en elle " , explique Joël Deumier .
" l' homosexualité est souvent mal vécue à cause de l' homophobie , remarque Diane Winaver , gynécologue psychosomaticienne . beaucoup de personnes ont lutté contre leur homosexualité . elles avaient honte parce qu' elles sentaient que leur entourage ne l' aurait pas accepté , que ce n' était pas possible . les coming out arrivent souvent tard . "
Joël Deumier , le président de l' association SOS Homophobie donne différents conseils .
si on en est victime
pour que les agressions et les insultes s' arrêtent , il faut parler .
il ne faut pas avoir peur de le dire à des adultes .
il ne faut pas avoir honte de dire qu' on a été moqué , insulté ou frappé .
ce n' est jamais de la faute de la victime .
" il faut briser la loi du silence . se faire insulter , discriminer ou frapper pour son orientation sexuelle , c' est anormal et illégal " , rappelle Joël Deumier .
plusieurs associations viennent en aide aux personnes rejetées à cause de leur sexualité , comme le MAG jeunes LGBT .
on peut téléphoner à l' association SOS Homophobie au un quarante huit six quarante deux quarante et un , un numéro gratuit et anonyme , ce qui veut dire que tu n' as pas besoin de donner ton nom .
on peut aussi écrire sur leur tchat .
si on sent qu' on est gêné par l' homosexualité
il faut essayer de comprendre l' autre , d' être bienveillant .
il a le droit d' exister , d' être comme ça et c' est important de le respecter tel qu' il est Il n' y a pas qu' un seul modèle de personne , un seul modèle de couple , un seul modèle de famille dans la vie .
il faut apprendre à être tolérant avec ce qui est différent de nous .
si on embête et insulte une personne parce qu' on pense qu' elle est homosexuelle , c' est interdit par la loi .
si on est témoin
il ne faut pas se mettre du côté de la personne qui agresse en riant à ses moqueries ou à ses insultes , par exemple .
parce que ça revient à soutenir l' homophobie .
il faut au contraire intervenir pour montrer qu' on soutient la victime .
il faut lui parler , la rassurer , instaurer un dialogue avec elle pour qu' elle sente qu' elle n' est pas seule .
puis il faut en parler à des adultes pour raconter ce qu' il se passe parce que c' est grave .