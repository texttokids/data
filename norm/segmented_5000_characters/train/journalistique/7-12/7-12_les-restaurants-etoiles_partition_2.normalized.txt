qu' est ce que la haute gastronomie ?
ah la la , qu' est ce qu' un bon repas peut apporter comme plaisir !
des frites bien croustillantes , un steak fondant , un gâteau au chocolat sucré pile comme il faut ...
miam miam !
on peut se lécher les babines devant les bons petits plats de papy ou au restaurant du quartier : les lieux où bien manger sont très nombreux .
mais certains établissements essayent de faire encore mieux que les autres : ce sont les restaurants gastronomiques .
qu' est ce qu' on y trouve ?
" une assiette de très belle facture , avec un service et un cadre qui font qu' on va vivre une expérience particulière " , résume Franck Pinay Rabaroust , rédacteur en chef du site Atabula , spécialisé dans la gastronomie .
le chef trois étoiles Éric Frechon raconte qu' il a oublié certaines vacances qu' il a passées mais qu' il se souviendra toute sa vie de repas fabuleux qu' il a mangés .
c' est ça , la haute gastronomie !
parfois , les plats ont des noms compliqués , comme " l' effiloché d' ormeaux sauvages , légumes du Léon , beurre d' ail fumé au whisky tourbé " , un plat que l' on peut déguster dans un grand restaurant breton .
" sans bon produit , il n' y a pas de bonne cuisine , assure le chef Éric Frechon . ça va du produit de luxe au maquereau ( un poisson qui ne coute pas cher ) , mais on prend le meilleur . " on peut déguster dans ces restaurants des aliments qui coutent cher , comme la truffe ( un champignon ) ou le caviar ( des oeufs d' un poisson appelé l' esturgeon ) .
en plus , dans les restaurants gastronomiques , il y a une jolie décoration , de belles assiettes ...
et beaucoup de monde qui travaille pour offrir un beau moment aux clients .
" la main d' oeuvre peut aller parfois jusqu'à un cuisinier pour un client " , constate Franck Pinay Rabaroust .
tout ça fait que l' addition dans les grands restaurants peut être élevée , voire très élevée .
pour déguster un pot au feu de boeuf aux truffes noires à Épicure , le restaurant d' Eric Frechon , il faut par exemple payer cent vingt euros .
si on veut terminer le repas avec un succulent dessert , il faut ajouter entre trente quatre et soixante quatre euros .
recette tirée du livre les Recettes des grands chefs : vingt deux recettes gastronomiques expliquées aux enfants de Marie Caroline Malbec et Camille Loiselet ( Mila éditions , quatorze , quatre vingt quinze euros ) .
clique ici pour télécharger la recette à imprimer .