est ce que tous les professeurs enseignent de la même façon ?
certains enseignants travaillent dans des écoles primaires et enseignent différentes matières .
dans les collèges et les lycées , les professeurs , eux , sont spécialisés dans un enseignement .
il existe aussi des cours au fonctionnement un peu différent .
trois professeurs ont raconté au P' tit Libé leur quotidien dans ces classes pas comme les autres .
elle est professeure d' histoire géographie au collège à Buxerolles , dans le centre ouest de la France .
son établissement fait partie d' un réseau d' éducation prioritaire ( REP ) .
ça signifie qu' il accueille beaucoup d' enfants issus de milieux défavorisés .
" il n' y a jamais plus de vingt cinq élèves par classe . la plupart d' entre eux sont boursiers et sont originaires de pays différents . certains ont de grandes difficultés avec la langue française . pour les aider , on enseigne à travers des projets . les élèves sont motivés quand ils doivent fabriquer quelque chose de concret . exemple : on a travaillé sur un audioguide en quatre langues pour faire visiter le collège aux nouveaux élèves . "
elle est professeure des écoles à Saint Nazaire le Désert , un village du sud est de la France .
même s' ils n' ont pas le même âge , les enfants de son école sont si peu nombreux qu' ils sont regroupés dans une classe , la seule de l' école .
Irène enseigne à quatorze élèves , de la grande section au CM deux .
" les enfants sont regroupés par niveau et chaque groupe a son tableau . je ne peux pas être tout le temps avec eux , donc le matin , je leur donne le planning de la journée avec un code couleur : rouge ou bleu selon les groupes . je leur apprends très tôt l' autonomie et l' entraide . ainsi , quand je ne suis pas disponible , ils s' aident mutuellement avant de m' appeler ! "
dans une école à Saint Seurin sur l' Isle , dans le Sud Ouest , elle accueille dans sa classe douze enfants en situation de handicap , quelques heures par semaine .
on appelle ça une classe Ulis ( Unités localisées pour l' inclusion scolaire ) , ça permet à ces élèves d' avoir une aide pour mieux suivre les autres cours .
" mes élèves ont entre six et onze ans et ont du mal à faire fonctionner leurs cerveaux pour apprendre . mon métier consiste à les aider à surmonter leurs difficultés . certains ont beaucoup de mal à parler , d' autres à écrire ... je les observe avec attention pour comprendre leurs besoins . pour les faire progresser , je les mets dans des situations concrètes : une recette de cuisine pour leur apprendre à lire et à compter , ou des jeux de société pour les aider à s' exprimer . "