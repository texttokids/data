quelles sont les nouveautés des JO deux mille dix huit ?
de nouveaux pays , plus d' épreuves féminines , un champion absent et un pays tout entier exclu : les Jeux olympiques deux mille dix huit réservent quelques surprises .
deux pays africains seront présents aux JO pour la première fois : le Nigeria en bobsleigh et l' Erythrée en slalom et slalom géant .
des sportifs africains participent aux JO d' hiver depuis mille neuf cent soixante , mais aucun n' a remporté de médaille .
ces athlètes sont nés et ont grandi aux États Unis ou au Canada .
mais comme leurs parents sont nés au Nigeria ou en Érythrée , ils possèdent deux nationalités .
ils ont donc pu choisir pour quel pays ils souhaitaient concourir aux JO .
sur les six nouvelles épreuves ajoutées cette année aux JO , deux sont réservées aux femmes .
c' est le cas de l' épreuve de big air , qui consiste à réaliser des acrobaties avec un snowboard en s' élançant de tremplins .
deux autres sont mixtes , comme l' épreuve par équipes de ski alpin .
" il y a une volonté de permettre aux femmes d' être mieux représentées . mais on remarque qu' il y a quarante quatre épreuves féminines contre cinquante épreuves masculines , donc les épreuves féminines sont toujours moins nombreuses " , souligne Éric Monnin , spécialiste du mouvement olympique international .
pour la première fois depuis mille neuf cent quatre vingt quatorze , l' athlète le plus médaillé de l' histoire des Jeux olympiques d' hiver sera absent de la compétition .
le norvégien Ole Einar Bjoerndalen , âgé de quarante quatre ans , a déjà remporté treize médailles ( huit en or , quatre en argent et une en bronze ) dans son sport , le biathlon .
mais cette année , ses résultats en Coupe du monde n' ont pas été assez bons pour lui permettre d' être sélectionné aux JO .
la Russie a été punie pour avoir triché pendant les Jeux olympiques qu' elle organisait à Sotchi , il y a quatre ans .
l' Etat russe est accusé d' avoir aidé les sportifs russes à se doper .
c' est la première fois dans l' histoire des Jeux olympiques qu' un pays tout entier est exclu pour dopage .
les athlètes russes ayant prouvé qu' ils ne sont pas dopés ont le droit de participer aux épreuves , mais ils ne représentent pas leur pays .