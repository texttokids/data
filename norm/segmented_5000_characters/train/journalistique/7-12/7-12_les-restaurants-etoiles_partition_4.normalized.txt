qui travaille dans les restaurants gastronomiques ?
pour faire fonctionner un restaurant gastronomique et satisfaire les clients , il faut du monde .
et les équipes s' activent bien avant et bien après la venue des clients !
le P' tit Libé a poussé les portes du Violon d' Ingres , un restaurant parisien ayant une étoile au Guide Michelin , pour rencontrer une partie de l' équipe .
le patron de la cuisine , c' est lui .
il décide notamment des plats qui sont proposés sur la carte .
certains existent au Violon d' Ingres depuis longtemps , et Jimmy Tsaramanana les garde car les clients les aiment bien .
mais il en invente aussi .
il gribouille régulièrement sur un cahier ses idées de recettes , liste les ingrédients , dessine des plats .
c' est aussi lui qui dit aux cuisiniers quoi faire , choisit ceux qui travailleront avec lui , commande les produits ...
" c' est un métier de passion . quand je suis en cuisine , j' oublie tout ce qui se passe ailleurs , savoure le chef . j' aime la rigueur , le côté presque militaire . " ce qui lui plait le plus , c' est de prendre un produit brut ( un poisson entier , par exemple ) et le transformer en un plat .
puis le voir disparaitre à jamais de sa cuisine pour rejoindre la table du client .
le soufflé chaud au Grand Marnier , le mille feuille et la tarte tatin , c' est elle qui les fait .
Chihiro Takahashi prépare chaque jour les desserts du Violon d' Ingres .
certains sont à la carte depuis très longtemps , et elle reprend la recette .
mais elle en invente aussi .
cette cheffe japonaise a par exemple créé sa propre version du Mont Blanc , un célèbre gâteau français à la crème de marrons et à la meringue .
" c' est amusant " , dit simplement celle qui rêvait déjà d' être pâtissière à dix ans .
beaucoup de gens aiment accompagner leur repas d' un bon vin .
Anthony Girbes est là pour les conseiller .
qu' est ce qui irait le mieux avec le tartare d' huître ?
et avec la pièce d' agneau de lait des Pyrénées ?
il connait toutes les réponses .
" mon métier est de trouver une bouteille qui plaira aux gens , qui les accompagnera pour un moment convivial à table " , résume le sommelier .
faire en sorte que les clients mangent dans des assiettes propres , c' est son rôle : Sabapathy Suthaskaran lave la vaisselle du restaurant .
son rôle est aussi d' aider les cuisiniers , en préparant ce dont ils ont besoin : il ouvre les coquilles Saint Jacques , décortique les langoustines , épluche les carottes ...
auparavant , il a travaillé dans des brasseries et des fast foods .
" ici , tout est frais " , salue t il .
bien manger , c' est important .
mais si les plats sont apportés par un serveur mal aimable , ça gâche un peu l' expérience .
Christophe Verain veille à ce que " chaque client qui vient déjeuner ou diner au restaurant passe un très bon moment . je les accueille et je veille à ce que les maîtres d' hôtel , les sommeliers et les chefs de rang s' occupent bien d' eux " , détaille t il : " les clients payent très cher , donc ils sont à même d' attendre un service de qualité . " dans un restaurant gastronomique , on a des attentions particulières .
par exemple , les serveurs redisent toujours le nom du plat en entier lorsqu' ils apportent l' assiette au client .