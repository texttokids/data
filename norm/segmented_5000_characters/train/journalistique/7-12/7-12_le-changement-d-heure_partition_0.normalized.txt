numéro soixante dix sept vingt six octobre au premier novembre deux mille dix huit
le changement d' heure
dimanche vingt huit octobre , on change d' heure .
on passe à ce qu' on appelle l' heure d' hiver .
il faut reculer sa montre : à trois heures du matin , il sera deux heures .
ce sera peut être la dernière fois .
les personnes qui prennent des décisions pour les pays d' Europe ont en effet décidé qu' on arrêterait de changer d' heure deux fois par an .
pourquoi une telle décision ?
et puis à la base , pourquoi avait on décidé de changer d' heure ?
existe t il un maître ou une maîtresse du temps qui choisit l' heure pour tout le monde ?
tic , tac , tic , tac , ne tarde pas à découvrir ce numéro .
Vincent va habituer ses vaches à la nouvelle heure
dans un premier temps , le P' tit Libé avait publié le témoignage d' un autre éleveur , Daniel , qui avait choisi de changer son rythme à lui pour que ses vaches ne se rendent pas compte du changement d' heure .
un exemple qui pouvait être un peu compliqué à comprendre .
pour simplifier les choses , nous avons choisi de donner la parole à Vincent , un autre agriculteur , qui s' y prend différemment .
ce week end , Vincent va faire comme les autres habitants d' Europe : il va changer d' heure et reculer sa montre .
dans la nuit de samedi à dimanche , à trois heures du matin , il sera deux heures .
ça a l' air de rien comme ça , mais il va devoir adapter un peu son travail .
Vincent a cinquante ans et il est éleveur de vaches laitières .
il récupère leur lait pour le vendre .
son troupeau compte une centaine de bêtes .
il les trait tous les matins à six heures et tous les soirs à dix huit heures .
elles connaissent bien le rythme , elles ont l' habitude .
alors quand l' heure change , elles perdent un peu leurs repères .
mais Vincent veut éviter de les chambouler .
" on ne change pas d' heure tout de suite , on le fait progressivement sur huit jours , explique l' éleveur . on y va de dix minutes en dix minutes . "
mais le passage à l' heure d' hiver , ce n' est pas le plus embêtant aux yeux de Vincent .
" ce n' est pas trop grave dans ce sens là . là où c' est le plus perturbant , c' est au printemps ( lors du passage à l' heure d' été ) : au lieu de se lever à cinq heures , il faut se lever à quatre heures , précise t il . nous , on a du mal à s' y faire et les animaux surement aussi , même s' ils ne nous le disent pas . "
il se souvient aussi que , quand ses deux enfants étaient petits , ils avaient beaucoup de mal à se lever au moment du passage à l' heure d' été ...
il a hâte qu' on arrête de changer et préfère qu' on reste pour toujours à l' heure d' hiver !
Bagas , la ville où habite Vincent