numéro cent vingt six premier au sept novembre vingt cent dix neuf
vive les morts !
à cette période de l' année , les morts font parler d' eux à l' occasion de trois fêtes célébrées dans le monde : halloween , la Toussaint et le jour des morts .
depuis toujours , les humains prennent soin d' eux à travers différentes traditions .
d' où viennent les fêtes qui ont lieu en ce moment ?
comment s' occupait on des morts autrefois et comment le fait on aujourd'hui ?
quelles relations garde t on avec eux ?
cette semaine , je t' emmène pour un voyage dans l' au delà ( garanti sans cauchemar ) .
charlotte , trente et un ans , prend soin des morts
chaque jour , Charlotte travaille avec des morts .
elle n' est ni gardienne de cimetière , ni chasseuse de fantômes .
elle est thanatopractrice .
ça veut dire qu' elle s' occupe des corps des morts dans des entreprises spécialisées , appelées des pompes funèbres .
d' abord , elle fait en sorte qu' ils restent bien conservés .
" j' injecte un produit dans le corps pour que le processus de putréfaction s' arrête , explique t elle . je vide aussi le sang du coeur et des organes . "
elle prépare le corps du mort pour que ses proches puissent venir le voir et lui dire au revoir .
" ça les aide de garder une bonne dernière image de la personne décédée , parfois je les entends dire " on dirait qu' il dort " " , précise Charlotte .
elle opère le corps pour maintenir sa bouche et ses yeux fermés .
pour faire tout ça , Charlotte a notamment appris la médecine .
" je vais aussi le maquiller , le coiffer , prendre soin de ses ongles , ajoute t elle . si la personne était très coquette quand elle était vivante , je lui mets du rouge à lèvres par exemple . le but , c' est qu' elle ressemble le plus possible à ce qu' elle était avant . "
prendre soin des morts , c' est un métier plutôt surprenant .
mais il plait beaucoup à Charlotte .
" bizarrement , je suis hypersensible , confie t elle . avant j' étais ambulancière et c' était très difficile pour moi de voir les gens souffrir . alors j' ai changé de travail . mais ce n' est pas facile tous les jours , surtout quand on assiste à la tristesse des familles . "
c' est aussi compliqué parfois d' en parler avec les gens .
" ils ne comprennent pas ce que je fais exactement , affirme t elle . on me pose souvent plein de questions . et surtout , si j' ai déjà vu des choses paranormales . " mais pour le moment , elle l' assure , rien à signaler !
charlotte vit à Mésandans et travaille dans trois départements de la région de Bourgogne Franche Comté