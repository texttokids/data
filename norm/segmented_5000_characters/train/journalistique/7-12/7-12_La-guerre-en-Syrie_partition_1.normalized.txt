que se passe t il dans le pays ?
ça va bientôt faire six ans qu' une guerre a lieu en Syrie .
plusieurs groupes de personnes s' opposent et beaucoup de gens meurent ou s' enfuient de leur maison .
récemment , Alep était à la une de l' actualité .
cette ville située au nord ouest de la Syrie était l' une des plus peuplées du pays avant le début du conflit .
à l' été vingt cent douze , des personnes opposées au président syrien , Bachar al Assad , ont pris le contrôle de la partie Est d' Alep .
elles sont alors entrées en guerre contre l' armée .
au mois de décembre , les soldats du Président ont gagné les combats et réussi à reprendre le contrôle d' Alep .
beaucoup d' habitants ont fui la ville .
depuis le début de la guerre , en mars vingt cent onze, trois cents personnes sont mortes .
c' est compliqué d' avoir des chiffres précis parce que les associations qui aident la population ont beaucoup de difficultés à aller dans le pays .
et les journalistes ne peuvent pas s' y rendre librement .
les informations proviennent de gens qui vivent encore en Syrie , mais elles ne sont pas toujours très précises .
certains syriens envoient des messages , des photos et des vidéos à des journalistes pour les informer de ce qui se passe .
de nombreuses villes ont été détruites par les bombardements .
des habitations , des écoles et des hôpitaux ont été démolis , il n' y a plus de moyens de transport ni d' eau ou de nourriture dans certaines zones .
des syriens meurent à cause des bombes , d' autres parce qu' ils n' ont plus à manger ou ne peuvent pas être soignés .
des millions de personnes se sont enfuies de leurs maisons et sont allées vivre dans des endroits où elles avaient moins de risques de mourir .
certaines sont restées en Syrie mais ont changé de ville , d' autres sont allées dans d' autres pays .
à l' étranger , les syriens sont surtout installés dans des pays voisins , comme la Turquie , le Liban ou la Jordanie .
en Europe , c' est surtout l' Allemagne qui accueille des syriens fuyant la guerre .
la France en accueille quarante fois moins que l' Allemagne .
les réfugiés s' installent surtout à côté de la Syrie
nombre de réfugiés inscrits dans chaque pays ( mais certains n' ont pas encore été comptés )