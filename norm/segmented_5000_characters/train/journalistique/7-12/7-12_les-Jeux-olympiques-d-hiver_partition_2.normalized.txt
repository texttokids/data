que va t on voir ?
certains participants à ces JO d' hiver sont des stars , comme le skieur autrichien Marcel Hirscher , qui a remporté toutes les médailles d' or sauf celle de champion olympique .
les Sud Coréens suivent les exploits de Choi Min jeong , l' une des favorites en patinage de vitesse sur courte piste .
les espoirs des français reposent , eux , notamment sur Martin Fourcade au biathlon , l' athlète français le plus médaillé aux JO d' hiver , et sur le couple Gabriella Papadakis et Guillaume Cizeron en danse sur glace .
les sports pratiqués aux JO sont répartis en trois catégories :
les sports de neige , comme le ski alpin
les sports de glisse , comme la luge
les sports de glace , comme le patinage artistique