pourquoi est ce grave ?
le harcèlement n' est pas un jeu .
depuis deux mille quatorze , il est même puni par la loi .
les harceleurs de plus de treize ans risquent six mois de prison et sept cinq cents euros d' amende .
pour les victimes , les conséquences peuvent aussi être importantes .
certaines se replient sur elles mêmes , pensent que tout est de leur faute et ont honte de parler de leur problème aux adultes .
d' autres encore souffrent de dépression : elles n' arrivent pas à dormir , ne mangent plus et s' isolent .
dans les cas les plus graves , les victimes peuvent se blesser volontairement ou se suicider .
certains adultes , victimes de harcèlement à l' école quand ils étaient enfants , souffrent encore aujourd'hui de dépression .
mais " généralement , les victimes vont de mieux en mieux quand le harcèlement prend fin " , explique Carole Damiani , psychologue et directrice de l' association Paris Aides aux victimes .
parfois , d' anciens harcelés deviennent eux mêmes harceleurs pour se venger ou pour éviter d' être à nouveau victimes .
Noémya Grohan a été harcelée pendant ses quatre années de collège .
aujourd'hui , la jeune femme de vingt neuf ans va beaucoup mieux .
mais elle a mis longtemps avant de retrouver confiance en elle .
" plus que les coups , ce qui m' a blessée ce sont les insultes et les moqueries " , confie t elle .
même à l' âge adulte , elle a eu du mal à oublier les agressions de ses camarades .
" j' ai mis plusieurs années avant de dire qu' on me harcelait à l' école . ce qui m' a guérie , c' est l' écriture : à chaque fois que je rentrais de l' école , j' écrivais . ça m' a aussi fait beaucoup de bien d' écrire un livre où je raconte mon histoire . "
le harcèlement à l' école peut aussi être " une souffrance du côté du harceleur " , explique Noémya Grohan .
certains cachent leurs angoisses ou leur tristesse en embêtant un camarade .
" le père d' une de mes harceleuses venait de mourir , alors elle s' est défoulée et projetait toute sa tristesse sur moi " , raconte Noémya .
les conséquences de leurs actes peuvent aussi être importantes pour eux mêmes .
une étude a par exemple montré que les harceleurs avaient une vie plus difficile adultes avec un travail moins stable et des périodes de dépression .