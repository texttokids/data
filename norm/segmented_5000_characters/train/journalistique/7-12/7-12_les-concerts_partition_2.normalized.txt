que permettent les concerts ?
la musique en live , c' est à dire en direct devant des spectateurs , existe depuis toujours .
" il n' y a pas une civilisation dans le monde qui n' a pas développé la musique de cette manière , remarque Olivier Lamm , journaliste culture et critique musical à Libération un . avant qu' on invente l' album studio , qui est un art différent , la musique c' était du concert . ça ne pouvait pas être autre chose puisqu' on ne pouvait pas l' enregistrer pendant des siècles . "
aujourd'hui , les spectateurs peuvent voir des concerts dans des toutes petites salles ou au contraire dans des salles immenses comme La Défense Arena à Paris , la plus grande de France , qui peut accueillir quarante personnes !
ça peut même être le double quand un artiste se produit en plein air comme au Stade de France .
ce type de spectacle s' est développé en même temps que la technologie : il faut des grands écrans et du très bon son pour que chacun puisse profiter du concert .
quand on aime un artiste , le voir en live est une expérience .
ce n' est pas la même chose d' écouter sa musique sur disque ou en streaming .
d' abord parce qu' on peut voir le musicien , sa façon de bouger , le voir sourire ou pleurer .
on est entourés des autres spectateurs qui applaudissent , dansent , ressentent des émotions .
un concert est en général une nouvelle création , on n' entend pas la même chose que sur l' album .
ça peut aussi être une performance : " certains concerts sont à chaque fois uniques et liés au lieu , aux spectateurs présents , au temps qu' il fait , à ce qu' il s' est passé dans l' actualité ... le musicien est un être humain , très sensible , qui transmet ce qu' il ressent à son public " , dit Olivier Lamm .
des artistes ont parfois peur de la scène , n' aiment pas ça ou ne sont pas bons , tandis que d' autres ont au contraire besoin de partager leur musique .
certains sont même de véritables bêtes de scène !
de nombreux artistes ont besoin des concerts pour vivre .
les disques se vendent quarante pour cents moins qu' il y a quinze ans .
" de nombreux artistes gagnent de l' argent en faisant des concerts plus qu' en vendant des disques , ce qui n' était pas le cas il y a quelques années " , analyse Joran Le Corre , tourneur et directeur artistique de l' agence Wart .
aujourd'hui , un consommateur de musique sur deux écoute des morceaux en streaming .
résultat : le streaming rapporte plus d' argent à l' industrie musicale que le CD ou le vinyle .
en revanche , les artistes touchent à peine quelques centimes à chaque fois que quelqu' un écoute leur musique en ligne .
un Le P' tit Libé appartient au journal Libération .