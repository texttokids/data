qui a créé l' étranger ?
cette question a été posée par Karim , neuf ans , élève en CM un à Romainville ( Seine Saint Denis ) .
la philosophe Barbara Cassin , directrice de recherche au CNRS , y répond .
les philosophes ont une drôle de manie .
quand on leur pose une question , au lieu de répondre , ils posent des questions à la question .
dans ta question : " qui a créé l' étranger ? " , moi j' ai envie de questionner le mot " créé " .
si je ne commence pas par là , je ne vais pas savoir te répondre .
" créer " , quel drôle de mot !
qui crée d' habitude ?
qui est capable de créer ?
il me semble que c' est soit Dieu , le Dieu " créateur " , soit un artiste , un " créateur " comme on dit dans la mode .
un artiste , c' est quelqu' un qui produit quelque chose qui n' existait pas du tout avant .
comme Dieu , car quand on est croyant , on pense que Dieu a créé le monde , qu' il n' y avait rien avant que Dieu ne le décide .
créer est une manière bien particulière de produire .
ainsi , on ne dit pas que les parents " créent " des enfants , et pourtant les enfants n' existent pas avant les parents , avant que les parents ne les " fassent " ou ne les " aient " .
car créer , c' est faire quelque chose mais à partir de rien , et qui ne ressemble à rien de ce qui existait avant .
alors est ce qu' il en va ainsi pour l' étranger ?
est ce qu' il y a un " qui " , un " quelqu' un " qui le crée ?
qui l' invente à partir de rien ?
c' est la première question que je ne peux pas m' empêcher de poser à ta question .
et puis la deuxième question , on doit se la poser ensemble .
c' est : qu' est ce que c' est , un étranger ?
qu' est ce que ça veut dire ?
on entend " étrange " dans " étranger " .
la manière dont les mots sont fabriqués en dit quelquefois très long sur les choses .
quelque chose d' " étrange " , c' est quelque chose de bizarre , d' inhabituel , parce que ça vient du dehors , de l' extérieur .
le mot " extérieur " et le mot " étranger " ont la même racine : le mot latin extraneus , qui veut dire " de l' extérieur " .
l' Étranger est étrange parce qu' il vient du dehors , voilà ce que nous racontent les mots .
maintenant je commence à pouvoir répondre à ta question : qui a créé l' étranger ?
pour qu' il y ait de l' étranger , il faut qu' il y ait un dehors et un dedans .
celui qui vient du dehors parait étrange à ceux qui sont au dedans , c' est pourquoi ils le nomment " étranger " .
mais il y a beaucoup de dedans différents , et évidemment pour chaque dedans , il y a un dehors .
dedans , cela peut renvoyer à mon corps , ma famille , mon école , mon pays , à l' Europe , à la terre entière , au système solaire , au genre humain ...
au fond , c' est le dedans qui crée le dehors , qui définit son dehors .
ce sera ma première réponse : c' est le dedans , le groupe , la communauté , l' appartenance qui crée l' étranger .
mais cette première réponse est beaucoup trop générale .
l' Étranger , c' est d' abord celui que je ne comprends pas , celui qui ne parle pas la même langue que moi .
car celui qu' on ne comprend pas , c' est vrai qu' on a du mal à partager quelque chose avec lui .
chacun de nous crée l' étranger lorsqu' il ne comprend pas celui qui arrive du dehors , avec son autre langue , son autre culture , sa couleur de peau .
ce qui crée l' étranger , on peut dire alors que c' est l' ignorance qu' on a de l' autre .
voilà ma deuxième réponse : c' est l' ignorance qui crée l' étranger .
quand on comprend quelqu' un , ce qu' il dit , pourquoi et comment il le dit , il n' est plus un étranger .
le premier pas pour comprendre l' autre , c' est de savoir que la langue que l' on parle , sa propre langue , est une langue parmi d' autres .
on est soi même un " étranger " pour l' autre .
c' est très intéressant de se voir avec les yeux de l' autre , comme du dehors .
soi même comme un autre ...
comprendre que l' étranger lui aussi , à son tour , vous crée , cela s' appelle la tolérance .