d' où viennent les migrants ?
les migrants qui arrivent en Europe viennent surtout d' Afrique et d' Asie .
en ce moment , ils viennent particulièrement de trois pays loin de chez nous : la Syrie , l' Erythrée et l' Afghanistan .
l' an dernier , un migrant sur deux venait de Syrie et d' Erythrée .
les habitants de ce pays se font la guerre depuis plus de quatre ans .
au départ , le Président a été violent envers la population , et maintenant différents groupes de personnes se battent .
les combats sont terribles .
la moitié de la population a dû quitter sa maison .
quatre millions de syriens ont même fui leur pays .
le conflit a fait beaucoup de morts et de nombreux enfants ne peuvent plus aller à l' école .
dans l' actualité , on parle peu de ce pays très pauvre .
il est dirigé depuis vingt deux ans par un dictateur , un homme qui exerce le pouvoir tout seul .
les habitants n' ont aucune liberté et vivent la peur au ventre .
beaucoup sont envoyés en prison du jour au lendemain .
plus de trois cent soixante Érythréens ont réussi à fuir leur pays mais c' est très dangereux : la police a pour mission de tuer ceux qui partent et de punir leurs familles .
l' Afghanistan est un pays qui a déjà connu plusieurs guerres .
aujourd'hui , il est attaqué par les talibans , des religieux dangereux qui vivent dans le pays et veulent prendre le pouvoir .
beaucoup d' habitants se font blesser ou tuer .
ils sont deux , six millions à avoir quitté leur pays .