numéro cinquante sept onze mai au dix sept mai vingt cent dix huit
mai soixante huit
il y a cinquante ans , en mai dix neuf cent soixante huit, des millions de personnes ont arrêté de travailler et ont manifesté dans toute la France .
cet évènement s' appelle Mai soixante huit .
il a été lancé par des étudiants puis a touché toutes les professions .
ces personnes voulaient une vie meilleure et rejetaient le monde dans lequel elles vivaient : elles le trouvaient injuste .
que s' est il passé exactement ?
pourquoi en parle t on encore aujourd'hui et est ce que ce grand mouvement de révolte a changé des choses ?
cette semaine , je te fais revivre ce grand évènement qui a secoué le pays .
florence avait dix ans en mai dix neuf cent soixante huit
florence a soixante ans .
en mai dix neuf cent soixante huit, elle avait donc dix ans .
elle habitait à ce moment là avec ses parents et ses trois soeurs près de la tour Eiffel , dans le VIIe deux noeuds arrondissement de Paris .
à l' époque , il n' y avait pas Internet , pas de téléphone portable et peu de gens avaient la télévision .
florence allait la regarder chez ses grands parents qui habitaient deux étages plus haut , dans le même immeuble qu' elle .
c' est devant leur télé qu' elle a découvert ce qu' il se passait à Paris en mai dix neuf cent soixante huit. elle se souvient des images en noir et blanc ( il n' y avait que deux chaines à l' époque , une seule était en couleur ) qui montraient des étudiants parisiens affronter des policiers .
elle se souvient de la fumée , des manifestations .
" ça me faisait un peu peur , ça avait l' air brutal . "
ces affrontements se déroulaient surtout dans le Ve deux noeuds arrondissement de Paris .
ça lui paraissait loin : florence ne s' aventurait pas en dehors de son quartier .
" je ne me sentais pas menacée , parce que je ne me sentais pas concernée . autour de moi , la vie continuait normalement . " le discours de son père la rassurait aussi .
ce professeur en école d' ingénieur trouvait " formidable " ce qui se passait .
" il en parlait beaucoup . il trouvait ça bien que les étudiants cherchent à faire bouger les choses . "
en septembre dix neuf cent soixante huit, Florence est entrée en sixième .
c' est à ce moment là qu' elle a observé des changements , alors que les manifestations de Mai soixante huit étaient terminées .
" les rapports avec les enseignants ont changé : les élèves leur répondaient plus facilement , alors que tout le monde était très discipliné avant . "
en cours , les élèves devaient porter des blouses .
mais là , " les plus grandes ont commencé à ouvrir un bouton de leur blouse , puis deux , puis trois .
trois mois plus tard , plus personne ne portait de blouse .
pour moi , c' est vraiment une conséquence de Mai soixante huit secondes , remarque t elle aujourd'hui .
à l' époque , comme on peut le voir sur la photo ci dessus , Florence avait cours avec des filles seulement .
il y avait des écoles pour les filles et des écoles pour les garçons .
mais , un an après dix neuf cent soixante huit , des garçons sont arrivés dans le même collège que Florence .
pour elle , c' était " une révolution " !
paris , la ville où habitait Florence en Mai soixante huit