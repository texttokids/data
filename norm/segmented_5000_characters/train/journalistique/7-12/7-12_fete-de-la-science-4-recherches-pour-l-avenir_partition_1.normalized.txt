un Comment recherche t on des traces de vie ailleurs dans l' univers ?
la Terre a un petit surnom , la " planète bleue " , car elle est recouverte aux trois quarts d' océans et de mers .
vue de l' espace , elle est toute bleue ...
c' est grâce à cette eau qu' il y a tant d' espèces d' animaux et de plantes sur Terre : elle est indispensable à la vie .
mais s' il y a de l' eau sur Terre , y en a t il sur les autres planètes ?
les astronomes essaient de répondre à cette question depuis des dizaines d' années .
imaginons qu' on découvre des lacs ou des rivières sur une planète lointaine : si ça se passe comme sur Terre , elle pourrait bien abriter , par exemple , des algues et des truites extraterrestres !
on sait aujourd'hui qu' il y a de l' eau sur la Lune , Mercure et Pluton , entre autres , sous forme de glace .
il y a en a aussi sur Mars : dans les régions glacées du pôle nord et du pôle sud mais aussi un peu dans le sol , et sous forme de vapeur dans l' atmosphère .
il est plutôt facile de détecter l' eau sur nos planètes voisines : il suffit d' envoyer des satellites leur tourner autour .
mais les astronomes veulent aussi savoir s' il y a de l' eau sur des planètes beaucoup plus lointaines , en dehors de notre système solaire .
on les appelle exoplanètes .
on en connait plus de quatre .
elles ne tournent pas autour de notre Soleil mais autour de toutes les autres étoiles que l' on voit dans le ciel .
grâce aux télescopes , et en faisant beaucoup de calculs , les astronomes arrivent à savoir , en gros , quelle est la taille de l' exoplanète et à quelle distance de son étoile elle se situe .
si elle est trop proche , il fait très chaud sur cette planète , et toute l' eau qu' il pourrait y avoir doit s' évaporer dans l' atmosphère .
si la planète est trop loin , elle est très froide , et l' eau sera glacée .
par contre , si elle est juste à la bonne distance , comme la Terre par rapport au Soleil , bingo !
on dit que la planète est en " zone habitable " .
ça veut dire qu' il y a peut être de l' eau liquide , qui forme des rivières et des mers .
et donc peut être de la vie ...
au mois de septembre , une nouvelle a fait les gros titres : on a découvert de l' eau sur une exoplanète !
le télescope Hubble , l' un des meilleurs télescopes sur Terre , a fait des mesures détaillées de l' atmosphère de cette planète ( qui s' appelle K deux dix huit B , un nom pas très pratique à retenir ) .
les astronomes sont surs qu' elle contient de l' eau sous forme de gaz , presque comme de la vapeur .
c' est la première fois qu' on arrive à détecter de l' eau aussi loin de notre système solaire .
l' exoplanète K deux dix huit B en vidéo