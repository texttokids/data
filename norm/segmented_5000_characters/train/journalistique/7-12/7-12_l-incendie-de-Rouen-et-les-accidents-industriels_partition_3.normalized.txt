Y a t il déjà eu des accidents industriels comme celui là ?
il y a déjà eu d' autres accidents dans des usines de produits chimiques dans le monde , dont certains ont eu des conséquences particulièrement graves .
voici trois exemples .
en dix neuf cent soixante seize , il y a quarante trois ans , à la suite d' une explosion dans une usine de produits chimiques dans la ville de Meda ( en Italie ) , un nuage très toxique s' est répandu dans les communes alentour .
la plus touchée a été Seveso .
cette explosion n' a pas fait de mort sur le coup mais elle a eu de très graves conséquences .
des centaines de personnes , surtout des enfants , ont souffert d' une maladie de la peau .
plus de sept cents habitants ont dû quitter leurs maisons , plus de quatre vingts animaux sont morts et près de deux hectares de sols ont été plus ou moins pollués .
ils ont ensuite été traités pendant plusieurs années pour retirer la pollution mais certaines zones sont peut être encore contaminées .
après cet accident , les pays européens ont établi de nouvelles règles , sous le nom de " directive Seveso " , pour obliger les États et les entreprises à mieux surveiller les sites industriels dangereux .
en dix neuf cent quatre vingt quatre , il y a trente cinq ans , l' une des pires catastrophes industrielles de l' histoire a eu lieu à Bhopal , en Inde .
un grand nuage de quarante tonnes de gaz toxiques s' est échappé d' une usine de pesticides .
il a surtout touché des gens très pauvres qui habitaient juste à côté .
des dizaines de milliers de personnes sont mortes le jour même de l' accident , ainsi que les années qui ont suivi .
des centaines de milliers ont été intoxiquées .
les habitants ont aussi souffert plus tard de maladies graves .
l' entreprise américaine à qui appartenait l' usine a versé une somme ridicule à certaines victimes en dédommagement .
le site de l' usine n' a jamais été traité pour enlever la pollution .
en vingt cent un , il y a dix huit ans , une explosion s' est produite dans l' usine de produits chimiques AZF , à trois kilomètres du centre ville de Toulouse , dans le sud ouest de la France .
un nuage de substances toxiques s' en est échappé .
l' explosion a été aussi violente qu' un petit séisme .
un grand cratère s' est formé là où était l' usine .
trente et un personnes sont mortes au moment de la catastrophe et les jours qui ont suivi , et près de deux cinq cents ont été blessées .
des dizaines de milliers de bâtiments ont été détruits .
depuis , la pollution a été traitée mais à un endroit , il reste des polluants en profondeur .
après ce très grave accident , une nouvelle loi a été votée pour renforcer la surveillance des sites Seveso et mieux évaluer les risques pour la population .