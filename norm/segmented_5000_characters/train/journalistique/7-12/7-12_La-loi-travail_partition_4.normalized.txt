quelles ont été les autres grandes mobilisations ?
ce n' est pas la première fois qu' il y a des manifestations au sujet du travail en France .
les gens se mobilisent soit parce qu' ils pensent que leurs conditions de travail ne sont pas assez bonnes , soit parce qu' ils ont peur qu' une loi les rende moins bonnes .
parfois , les manifestants obtiennent ce qu' ils veulent , parfois non .
les ouvriers , c' est à dire les gens qui sont employés dans les usines ( là où on fabrique des voitures , par exemple ) , voulaient des améliorations de leurs conditions de travail .
ils ont décidé de le faire savoir au gouvernement qui venait d' être élu .
pour ça , ils ont fait grève ( ils ont arrêté de travailler ) un peu partout en France .
puis des personnes qui étaient employées dans d' autres types d' entreprises , comme des grands magasins , ont fait pareil .
puisqu' il y avait vraiment beaucoup de grévistes ( les gens qui font grève ) , le gouvernement a réuni les représentants des patrons et ceux des ouvriers pour discuter .
ensemble , ils ont trouvé plusieurs solutions , dont voici quelques exemples :
les salaires ont été augmentés ,
le temps de travail est passé à quarante heures par semaine , alors qu' avant les gens devaient travailler quarante huit heures ( aujourd'hui , c' est trente cinq heures ) ,
les congés payés ont été créés : les travailleurs ont eu le droit de partir en vacances pendant deux semaines chaque année tout en recevant de l' argent ( aujourd'hui , c' est cinq semaines par an ) .
au mois de mai , il y a eu de nombreuses manifestations en France .
cette période est surnommée " mai soixante huit secondes .
ce sont d' abord des étudiants qui se sont mobilisés pour critiquer plusieurs choses qui ne leur plaisaient pas dans la société .
puis des ouvriers se sont mis en grève , pour protester contre leur rythme de travail et pour demander à être mieux payés .
ensuite , des personnes qui travaillaient dans d' autres types d' entreprises ont fait pareil , partout en France , pour les mêmes raisons .
après ces manifestations , les salaires ont été augmentés .
mais surtout , les syndicats , c' est à dire des groupes de gens qui défendent les travailleurs , ont été autorisés dans les entreprises .
à partir d' un certain âge ( aux alentours de soixante ans ) , on a le droit de prendre sa retraite .
ça veut dire qu' on arrête de travailler pour toujours , mais qu' on gagne quand même de l' argent chaque mois .
pour recevoir cet argent , il faut avoir eu un emploi avant , parce que , quand on travaille , une partie du salaire que l' entreprise distribue chaque mois est mise de côté exprès pour payer les retraites .
pour avoir le droit de recevoir l' argent de la retraite , il faut avoir travaillé un certain nombre d' années et avoir un âge minimum .
plusieurs gouvernements ont voulu obliger les gens à travailler plus longtemps avant d' avoir le droit de recevoir l' argent de la retraite .
ça a notamment été le cas en dix neuf cent quatre vingt quinze , en vingt cent trois et en vingt cent dix .
ces trois années là , des citoyens n' étaient pas d' accord pour travailler plus longtemps , donc ils ont manifesté pour afin que les gouvernements retirent leurs propositions .
en dix neuf cent quatre vingt quinze , ce sont les manifestants qui ont obtenu ce qu' ils voulaient .
en vingt cent trois et en vingt cent dix , ce sont les gouvernements .
aujourd'hui , la plupart des gens doivent avoir travaillé quarante trois années et avoir au moins soixante deux ans pour recevoir tout l' argent de la retraite .
si on s' arrête avant ces limites , on reçoit de l' argent quand même , mais moins , sauf si on a commencé à travailler très jeune .
le gouvernement a voulu créer un nouveau contrat de travail , qui s' appelait le contrat première embauche : le CPE .
il devait permettre aux patrons de licencier les salariés de moins de vingt six ans sans leur expliquer pourquoi , au cours des deux premières années où ils travaillent dans l' entreprise .
les jeunes n' étaient pas d' accord et ont beaucoup manifesté .
finalement , le gouvernement a renoncé à créer le CPE .