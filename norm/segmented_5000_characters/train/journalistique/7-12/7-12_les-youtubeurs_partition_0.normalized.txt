numéro trente huit vingt neuf décembre deux mille dix septau quatre janvier deux mille dix huit
les youtubeurs
ils réalisent des vidéos et les publient régulièrement sur le site Internet YouTube : ce sont des youtubeurs .
certains , comme Norman , EnjoyPhoenix ou Cyprien , sont devenus de vraies stars et sont suivis par des millions de personnes ...
qu' est ce qu' un youtubeur ?
peut on gagner de l' argent grâce à ça ?
est ce un métier ?
cette semaine , je t' explique tout sur ce site sur lequel des milliards de vidéos sont regardées chaque jour .
adèle , seize ans , est youtubeuse
adèle a seize ans et est connue par de nombreux adolescents grâce au site Internet YouTube .
elle y poste des vidéos drôles dans lesquelles elle se met en scène .
chacune de ses vidéos est regardée par cinq cents à un million de personnes !
" j' ai commencé il y a trois ans parce que j' aime faire le show depuis que je suis toute petite . pour moi , Internet est un endroit formidable pour m' exprimer . j' ai choisi de faire rire parce que c' est ce que j' aime ! "
avant de se lancer sur YouTube , Adèle , qui habite à Nantes , avait déjà un certain succès sur Instagram alors qu' elle avait douze ans .
elle s' est fait remarquer en postant il y a deux ans une vidéo dans laquelle elle se moquait des youtubeuses qui parlent de beauté .
grâce à ce sketch , cinquante personnes se sont abonnées sur sa chaîne en à peine une semaine .
" j' espérais ce succès mais je ne m' attendais pas à ce que ça marche aussi vite " , se souvient elle .
elle dit que la réalisation de ses vidéos se fait très rapidement : " j' y pense la vieille , je la tourne le lendemain et je la poste deux jours après . "
adèle est aujourd'hui au lycée , en première , et gagne un peu d' argent ( mais ne dit pas combien ) grâce à ses vidéos .
comment ?
grâce aux publicités qu' il faut regarder pour accéder aux vidéos .
ceux qui s' occupent de la pub , qu' on appelle des annonceurs , envoient de l' argent à YouTube .
le site en garde une grosse partie mais donne le reste aux youtubeurs comme Adèle .
" tout est assez incroyable avec Internet . grâce à ça , j' ai notamment pu jouer dans le film Sous le même toit , sorti en avril . et j' ai aussi tourné dans un autre , qui devrait sortir en juin . " mais Adèle ne sait pas encore si elle veut devenir actrice plus tard , parce qu' elle est " intéressée par plein de choses " .
Nantes , la ville où habite Adèle