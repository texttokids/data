qui sont les spécialistes des dinosaures ?
quand il était petit , Florent Goussard adorait les dragons .
mais en grandissant , il a compris que les seuls dragons qui existaient vraiment étaient les dinosaures , alors il s' est intéressé à eux .
cette passion ne s' est jamais arrêtée : aujourd'hui , Florent Goussard a trente six ans et il est paléontologue , spécialiste des dinosaures .
il étudie les fossiles , c' est à dire les traces laissées par les dinos dans de la roche , pour mieux comprendre comment ils ont vécu .
le métier de paléontologue ressemble un peu à celui de détective .
" il faut d' abord trouver des fossiles " , explique Florent Goussard .
et pour ça , inutile de creuser dans son jardin !
les paléontologues fouillent dans des endroits où la surface de la Terre date de l' époque des dinosaures .
il y en a sur tous les continents .
florent Goussard est allé chercher des fossiles dans sept pays , comme au Maroc , en Pologne ou au Canada .
la première étape , c' est la collecte d' indices .
sur le sol , les paléontologues cherchent des traces de dinosaures , comme des bouts d' os .
plus ils trouvent d' indices , plus ils sont surs qu' un fossile important les attend en profondeur .
" quand on trouve enfin un fossile , on délimite un espace autour . on extrait ensuite ce bloc du sol pour l' amener au laboratoire " , précise Florent Goussard .
des produits spéciaux et des mini marteaux piqueurs permettent de nettoyer le fossile de son enveloppe de pierre sans l' abîmer .
une fois qu' il est " propre " , les paléontologues peuvent l' analyser .
florent Goussard raconte : " on l' observe et on le compare avec d' autres fossiles . c' est comme un livre sur la vie des dinosaures qu' il faut décrypter . "
grâce aux nouvelles technologies , les fossiles sont étudiés avec encore plus de précisions .
une machine permet de reproduire les fossiles en trois dimensions sur un ordinateur .
ils peuvent ainsi être observés sous toutes les coutures très simplement .
pratique quand les restes des dinosaures sont beaucoup trop lourds pour être manipulés !
quand le paléontologue a terminé d' apprendre tout ce qu' il pouvait apprendre sur son fossile , il commence à partager ses découvertes .
il peut écrire des articles ou des livres , réunir des gens pour leur expliquer ce qu' il sait , organiser des expositions ...
florent Goussard affirme que son métier " n' est pas facile " , mais ajoute qu' il est " tellement passionnant que ça en vaut la peine " .
tu veux imprimer ta carte des dinosaures ?
clique ici pour la télécharger !