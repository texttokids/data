numéro quarante huit neuf au quinze mars deux mille dix huit
les fake news
depuis l' élection de Donald Trump comme président des États Unis , en deux mille seize , on entend souvent l' expression " fake news " , qui désigne en général de fausses informations .
il y en a tellement qu' Emmanuel Macron , le président français , veut qu' une nouvelle loi interdise ces fameuses fake news .
les élèves de Taninges chassent les fake news
une longue ficelle traverse la salle de classe .
dessus , de nombreuses unes de journaux sont accrochées avec des pinces à linge .
au tableau , plusieurs unes de l' Equipe parlant des Jeux olympiques d' hiver sont affichées .
chaque lundi en fin de journée , neuf élèves de CM deux et un CM un de l' école de Taninges , en Haute Savoie , se retrouvent pour parler d' informations et de médias .
depuis le mois de septembre , ces dix enfants apprennent à être des chasseurs de fake news , de fausses informations , grâce à Rose Marie Farinella , une enseignante .
le thème du jour , c' est l' image .
" est ce qu' une photo est une preuve ? " , interroge la maîtresse .
pour ces élèves , pas de doute , la réponse est non .
romain bondit en levant la main : " tu peux faire des photomontages ! " autrement dit , il est possible de modifier une image .
rose marie Farinella leur montre une photo .
on y voit Isabelle , une autre enseignante , avec un couteau à la main , prête à couper les doigts d' un élève .
puis les écoliers découvrent que cette photo n' était pas entière .
elle a été recadrée , c' est à dire qu' on en a caché une partie .
isabelle était en réalité en train de couper un gâteau .
" et qu' est ce qui peut arriver si on croit que la maîtresse coupe les doigts des enfants ? " , demande Rose Marie .
la réponse des élèves tombe immédiatement : " des parents peuvent porter plainte et elle peut aller en prison . "
grâce à Rose Marie , les enfants apprennent à faire le tri parmi les informations .
" c' est pour faire attention sur Internet , explique Sarah , dix ans . des fois il y a des fausses informations , il ne faut pas tomber dedans . " et comment on fait pour ne pas tomber dedans ?
" il faut vérifier l' information sur plusieurs sites " , répond Mary Lou , dix ans .
" il faut voir quand ça a été publié " , poursuit Sarah .
" il faut voir qui a écrit l' article , et sur quel site ou journal , continue Lilian , dix ans . ça sert à ne pas dire de bêtises aux copains . "
à la fin de l' année , ces élèves de Taninges jureront sur la tête de leur souris d' ordinateur qu' ils ne partageront jamais une information sans l' avoir vérifiée .
ils obtiendront alors un diplôme de " hoax busters " , de chasseurs de fausses infos .
Taninges , la ville où les enfants vont à l' école