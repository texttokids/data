est ce la seule région à demander plus de pouvoirs ?
la Catalogne n' est pas la seule à réclamer son indépendance .
plusieurs autres régions des pays de l' Union européenne souhaitent plus d' autonomie .
voici quelques exemples .
l' Ecosse est considérée par tout le monde comme une nation mais ce n' est pas un État indépendant .
elle fait en effet partie du Royaume Uni et a son Parlement , son gouvernement .
lors d' un vote organisé en vingt cent quatorze , les habitants ont voté non à l' indépendance à cinquante cinq , quatre pour cents .
mais le gouvernement écossais veut leur poser une nouvelle fois la question depuis que le Royaume Uni a décidé de quitter l' Union européenne ( on appelle ça le Brexit ) alors que de nombreux Écossais veulent continuer à en faire partie .
ce territoire se partage entre l' Espagne et la France .
côté espagnol , comme ici sur la carte , le Pays basque est une région autonome dans laquelle il y a deux langues officielles : l' espagnol et l' euskara ( la langue basque ) .
cette région a beaucoup de pouvoirs : sa propre police , son Parlement , son gouvernement ...
certaines personnes réclamaient l' indépendance par la violence , regroupées dans l' organisation terroriste ETA , qui a fait huit cent cinquante victimes entre dix neuf cent soixante huit et vingt cent onze .
aujourd'hui , la majorité des basques souhaitent plus d' autonomie plutôt que l' indépendance .
c' est une région française différente des autres parce que c' est une île mais aussi parce que le gouvernement français lui a donné plus de compétences pour calmer les indépendantistes qui avaient choisi la violence ( le mouvement terroriste Front de libération national de la Corse , ou FLNC ) .
la Corse s' occupe donc par exemple des routes , des lycées , des collèges et des universités .
certains habitants communiquent en corse , une langue qui est aussi parlée dans quelques écoles primaires .
la majorité des Corses semble plus favorable à davantage d' autonomie qu' à l' indépendance .
ces deux riches régions situées dans le nord de l' Italie vont chacune organiser un référendum ce mois ci pour savoir si les habitants veulent que leur région soit plus autonome .
si la participation est importante , ça donnera plus d' importance au parti de la Ligue du Nord , qui dirige ces deux régions et qui est un parti xénophobe .
c' est l' une des trois régions de la Belgique .
c' est la plus peuplée et la plus riche du pays .
ses habitants parlent le flamand .
elle bénéficie d' une autonomie assez importante dans le pays avec un Parlement et un gouvernement .
certains partis réclament encore plus d' autonomie , sans forcément aller jusqu'à l' indépendance .