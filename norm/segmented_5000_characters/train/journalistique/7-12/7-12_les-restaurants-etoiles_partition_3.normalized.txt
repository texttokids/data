comment sont choisis les étoilés du Michelin ?
pour choisir qui mérite une étoile au Guide Michelin , il faut gouter ...
c' est le rôle des inspecteurs .
en France , ils sont une quinzaine .
" chaque jour , ils vont déjeuner et diner dans des restaurants et les noter . tel plat vaut une étoile , tel autre en vaut deux ... " explique Franck Pinay Rabaroust , rédacteur en chef du site Atabula , spécialisé dans la gastronomie .
ils font ça deux semaines de suite , puis font une pause , rentrent au bureau une semaine , et recommencent .
le tout pendant six mois !
il existe des milliers et des milliers de restaurants en France , et tous ne méritent pas une visite des inspecteurs du Michelin .
pour savoir où aller , ces experts se renseignent .
ils lisent la presse locale , demandent aux habitants du coin ce qu' ils en pensent , à l' office de tourisme ...
une fois dans le restaurant , ils ne disent pas qui ils sont .
il ne faudrait pas que les chefs fassent de plus grands efforts pour les impressionner qu' ils ne le font d' habitude avec leurs clients !
mais ils peuvent se présenter une fois le repas terminé , pour discuter un peu avec le chef .
" pour avoir une première étoile , il faut de bons produits et aucune erreur technique dans l' assiette : pas de plat froid s' il doit être chaud , pas de sur cuisson ou de sous cuisson ... " détaille Franck Pinay Rabaroust .
en gros , il faut que ce soit très bien , sans rien qui vienne déranger le palais .
pour accéder à la deuxième étoile , il faut que ce soit encore meilleur .
et pour la troisième , poursuit Franck Pinay Rabaroust , " il faut une cuisine ultrarégulière , une perfection dans les accords , dans les produits . il y a une grande identité dans l' assiette : on sait quasi immédiatement que c' est vous " .
avant de donner une troisième étoile à un chef , le Michelin veut être bien sûr de son choix .
alors le restaurant peut être inspecté sept , huit , neuf fois ...
il ne faut pas se tromper !