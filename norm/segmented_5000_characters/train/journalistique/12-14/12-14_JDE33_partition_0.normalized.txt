jeudi dix neuf octobre vingt cent dix sept
utilisé par les agriculteurs dans les champs pour se débarrasser des mauvaises herbes , le glyphosate est un produit suspecté de provoquer de graves maladies .
mais les autorités hésitent à l' interdire .
dis , tu peux me traduire ces hiéroglyphes ?
les hiéroglyphes sont ces petits signes et dessins utilisés par les égyptiens il y a plus de cinq mille ans pour écrire .
au début du dix neuvième siècle , ils ont été déchiffrés en partie grâce à Champollion .
mais ils recèlent encore bien des mystères .
Ubisoft , une société française qui fabrique des jeux vidéo , et le géant américain Google se sont associés pour lancer un nouveau programme informatique capable de mieux comprendre les hiéroglyphes et de les traduire plus facilement .
un peu comme ce que propose déjà Google pour les langues étrangères .
une chose est sure .
les produits : chimiques utilisés par les agriculteurs pour obtenir de meilleures récoltes se diffusent dans l' environnement .
et on les retrouve aussi dans nos assiettes .
récemment , une étude a montré qu' il y avait des traces de glyphosate dans des céréales de petit déjeuner par exemple , ou dans les pâtes .
même si ces traces sont minimes ( toutes petites ) , les consommateurs s' interrogent sur leurs effets .
les études ne sont pas encore capables de dire si c' est dangereux pour la santé .
près de neuf français sur dix aimeraient quand même que , s' il y a des traces de ces produits dans les aliments , cela soit mentionné écrit sur l' emballage .
il permet de débarrasser des mauvaises herbe .
les agriculteurs l' utilisent beaucoup pour que leurs plantes puissent pousser sans être embêtés par les mauvaises herbes .
Roundup contient du glyphosate .
c' est un produit chimique .
il y a deux ans , une étude l' Organisation mondiale de la té ( OMS ) classe le glyphosate comme une substance " probablement " cancérigène
la chaîne de télévision Gulli a décidé de ne plus diffuser de spectacles montrant des animaux sauvages .
et ce , dans tous les pays où Gulli et les autres chaines de son groupe sont diffusées .
des défenseurs de l' environnement collent des étiquettes dans un supermarché pour dénoncer la présence de pesticides dans la nourriture .
Gulli est la chaine numéro un auprès des jeunes téléspectateurs .
ses dirigeants estiment qu' ils ont une responsabilité envers ces enfants .
ils ont choisi de diffuser des programmes où on voit les animaux en liberté et où on explique leur vie dans la nature et les moyens de les protéger .
la voile qui rend les paquebots écolos .
environnement Selfie plus animaux sauvages égale mauvaise idée
c' est une première !
au Pays Bas , une autoroute a été construite avec des fibres de papier toilette .
d' une longueur de huit , huit kilomètres , elle relie Leeuwarden à Stiens ( au nord du pays ) .
comment ?
le papier toilette usagé a été récupéré dans les égouts , ses fibres extraites , nettoyées , stérilisées , blanchies et sechées .
le résultat obtenu est une masse cotonneuse de couleur grise .
pour des raisons d' hygiène , la loi interdit son utilisation dans les produits entrant en contact direct avec le corps humain .
alors l' ajouter à la chaussée a été un choix logique .
vif SUCCES
les autorités néerlandaises aiment tellement le concept que cette nouvelle matière a été utilisée dans la construction d' une digue ( sorte de barrage contre les vagues ) sur l' île d' Ameland et pour le nouveau revêtement du parking du zoo de Groningen .
elle intéressera aussi la ville d' Amsterdam , qui compte l' utiliser dans le revêtement de ses routes .
seasure
se prendre en photo avec des animaux sauvages est une pratique qui s' est développée avec les réseaux sociaux .
ces souvenirs de vacances originaux garantissent des likes à tous les coups .
mais un instant .
sur la photo , il n' y a pas que vous ...
l' animal sauvage , comment est il arrivé là ?
vous avez délogé de son arbre durant une balade ?
cette activité n' est donc pas sans conséquences sur les animaux sauvages .
les associations de défense de la nature tirent sonnette d' alarme .
l' une d' entre elles a appelé Instagram à prendre des mesures .
le réseau a assuré étudier des moyens d' informer ses utilisateurs des conséquences de ces photos .
l' ambition est de réduire la consommation pétrole des bateaux .