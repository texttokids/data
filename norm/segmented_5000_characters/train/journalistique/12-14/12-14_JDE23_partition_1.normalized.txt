même si les règles de fabrication des aliments sont strictes , de nombreux scandales ont éclaté ces dernières années .
l' Autorité des marchés financiers , chargée de surveiller la bourse , a rappelé que le bitcoin n' était pas un jeu .
elle a ainsi réagi à un message de Nabilla , la star de la téléréalité , qui se vantait d' avoir gagné de l' argent facilement grâce au bitcoin .
en tout , trente sept bébés sont tombés malades .
certains ont dû être conduits à l' hôpital .
un papa a porté plainte contre Lactalis et une enquête a été ouverte .
le gouvernement a aussi déclaré que des sanctions seraient prises contre l' entreprise , si des fautes graves sont prouvées .
certains ont été provoqués par des personnes qui ont vendu des produits dangereux pour la santé pour fabriquer des aliments , comme par exemple en dix neuf cent quatre vingt un , en Espagne .
de l' huile de colza , qui devait servir dans des usines , a été vendue comme une huile normale , à cuisiner .
elle a rendu vingt cinq personnes malades et en a tué près de dix cent zéro .
d' autres scandales sont liés à des produits qui ne devraient pas se trouver là , comme de la viande de cheval retrouvée dans des plats surgelés en vingt cent treize .
états unis Fausse alerte , vraie panique sur l' île d' Hawaï
il était environ huit heures samedi dernier lorsque les habitants de l' île ont reçu un drôle de message sur leur téléphone portable : " menace de missile balistique sur Hawaï . réfugiez vous immédiatement dans des abris . ce n' est pas un exercice . " ce message d' alerte est diffusé automatiquement par l' État à tous les habitants , pour les avertir d' un danger important ,
il existe des endroits en France où même le forfait " bras levé " ( lorsqu' on a le réflexe de leverle bras pour avoir du réseau ) ne marche pas .
dans cinq cent quarante et un communes exactement .
elles sont situées dans ce qu' on appelle les zones blanches .
c' est à dire des lieux où il n' y a pas du tout de réseau pour les téléphones mobiles .
souvent , c' est à la campagne .
sauf que ...
c' était une fausse alerte .
selon les autorités , elle a été déclenchée par un employé qui a appuyé par erreur sur un mauvais bouton .
le problème est qu' il a fallu trente huit minutes avant qu' un nouveau message ne l' annonce .
ce qui a laissé le temps aux habitants de penser au pire .
il y a même eu des scènes de panique .