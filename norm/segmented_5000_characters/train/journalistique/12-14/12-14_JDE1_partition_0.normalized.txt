hermien est enfin à la maison , après un chemin long , solitaire et stressant vers la liberté .
écosse Un robot renvoyé du supermarché Fabio le robot était employé dans un supermarché d' Edimbourg .
sa mission : répondre aux demandes des clients .
mais rapidement , Fabio ne s' est pas montré à la hauteur de sa mission , effrayant même un peu les usagers .
il a donc été remercié , mais devait retrouver rapidement du travail dans une autre entreprise .
hermien va pouvoir vivre des jours heureux .
la vache se cachait dans un bois au Pays Bas depuis deux mois pour échapper à l' abattoir .
devenue une vedette internationale , elle coule maintenant des jours paisibles dans une maison de repos pour bovins .
il y a , dans le monde , trois cent cinquante sept millions d' enfants qui vivent dans une zone de conflit .
c' est à dire dans un endroit où il y a la guerre .
c' est l' organisation Save the Children qui livre ce chiffre terrible , ainsi que les conséquences qui y sont liées .
des enfants tués , blessés , ou souffrant de troubles graves qui les empêchent d' avoir une vie normale .
Maëlys , neuf ans , a disparu dans la nuit du vingt six au vingt sept août dernier durant un mariage à Pont de Beauvoisin ( Isère ) .
depuis , on était sans nouvelles de la jeune fille .
un suspect avait été arrêté rapidement .
le quatorze février , il a finalement avoué avoir enlevé et tué la jeune fille dont le corps a été retrouvé près de chez lui .
Marius Ceraolo , neuf ans , ne sait pas faire de vélo .
aucun problème à la place .
à la place , il a appris à conduire un camion , une passion hors norme .
Marius a appris à conduire un camion en regardant son père , qui dirige une entreprise dans le Loiret .
pour conduire un camion , Marius devra attendre d' avoir vingt et un ans .
en attendant , il s' entraîne sur le parking .
les poules vont prendre l' air Fini les élevages de poules en cage .
à partir de deux mille vingt deux , tous les oeufs vendus dans les supermarchés devront avoir été pondus par des poules élevées en plein air .
pour célébrer le Nouvel An chinois , il est de coutume d' utiliser feux d' artifice et pétards durant quinze jours en Chine .
mais cela fait beaucoup de fumée et aggrave la pollution .
alors quatre cent quarante quatre villes les ont interdits .
Afrique du Sud Le président a démissionné Accusé d' avoir accepté de l' argent pour favoriser des entreprises ou d' avoir utilisé de l' argent du pays pour son usage personnel , le président d' Afrique du Sud Jacob Zuma a dû démissionner
éducation L'école le mercredi , c' est ( presque ) fini Neuf écoles primaires sur dix ont décidé de revenir à la semaine des quatre jours à partir de la prochaine rentrée .
autrement dit , les élèves n' auront plus école le mercredi ou le samedi matin .
un accord entre le gouvernement britannique et les autorités de l' Eglise permet désormais d' installer des antennes , notamment sur des églises .
celles ci sont souvent situées à la campagne , dans des zones où on ne capte pas bien Internet .
le plus vieux numéro un du tennis Roger Federer n' en finit plus de battre des records .
en remportant le tournoi de Rotterdam ( Pays Bas ) , le suisse est devenu , à trente six ans et six mois , le numéro un mondial le plus âgé de l' histoire .
de mystérieuses guirlandes de slips accrochées par des collégiens dans les rues de Poligny ( Jura ) ont provoqué la curiosité des habitants durant plusieurs jours .
les élèves travaillaient sur les menus pour enfants de McDonald' s devront contenir moins de calories et de sel .
le cheeseburger est donc remplacé par des nuggets , la taille des
trop jouer aux jeux vidéo devrait bientôt être reconnu comme une vraie maladie .
au Vietnam ( Asie ) , une école propose d' aider les jeunes à retrouver une vie plus classique grâce aux arts martiaux .
le vivonam ( nom de ce sport ) impose une grande maîtrise