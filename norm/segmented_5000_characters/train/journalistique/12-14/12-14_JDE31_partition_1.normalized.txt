l' Éducation Nationale interdit les photos individuelles .
justice À quoi sert le tilde ?
états unis Qui est Harvey Weinstein ?
des parents bretons ont dû changer l' orthographe du prénom de leur enfant .
la faute à un petit signe appelé tilde , qui n' existe pas dans l' alphabet français .
certaines de ces accusations portent sur des agressions très graves et la police a ouvert une enquête .
Harvey Weinstein est un producteur de cinéma américain .
son travail : trouver de l' argent pour réaliser des films .
il a beaucoup de pouvoir et peut donner des rôles aux acteurs et actrices de son choix .
c' est un journal américain qui a révélé l' affaire .
mais les agissements de Monsieur Weinstein étaient connus de beaucoup de monde .
la plupart de ces personnes avaient peur de parler .
ce scandale a lancé un débat important sur la façon dont les hommes qui ont du pouvoir s' en servent pour mal se conduire avec les femmes .
mais il semble que cet homme ait abusé de son pouvoir .
il est désormais accusé par des dizaines d' actrices de leur avoir demandé d' avoir des relations sexuelles avec lui en échange de son aide pour travailler .
cela pose parfois des problèmes .
le treize septembre dernier , les parents du petit Fanych , un prénom d' origin bretonne , ont ainsi dû retirer le tilde du prénom de leur enfant .
selon les juges , cela n' était pas compatible avec la langue française .
leur décision a provoqué des mécontentements .
si bien que la semaine dernière la région de Bretagne a décidé de demander à son tour au ministère de la Justice le droit d' utiliser ce signe particulier .
dans certaines langues comme l' espagnol , le portugais ou le breton , il existe un signe spécial qui ressemble à une lettre " S " couchée .
ce signe est appelé tilde .
utilisé comme un accent sur les lettres " N " , " a " ou " O " , il modifie leur prononciation .