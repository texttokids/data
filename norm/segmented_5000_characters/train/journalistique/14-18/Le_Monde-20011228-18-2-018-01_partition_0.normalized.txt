d' extases en effrois , le cinéma français s' est offert , en deux mille un , une année à grand spectacle .
peu d' industries françaises peuvent se flatter de connaître des résultats records , en hausse de douze pour cents , tout en prenant presque quinze pour cents aux américains sur un marché que les États Unis dominent depuis des lustres .
mais quelle industrie aussi peut être bouleversée du jour au lendemain à la suite d' une simple phrase lâchée au détour d' une conversation par l' un de ses acteurs , en l' occurrence Jean Marie Messier ?
pour annoncer que " l' exception culturelle française est morte " , le président de Vivendi Universal a choisi l' année où la vigueur de cette exception n' a jamais été aussi manifeste .
si l' on met de côté les lointains exemples coréen et indien , la France est le seul pays où le cinéma national fait jeu égal avec le cinéma américain , dans un contexte de fréquentation et de production en hausse .
vaillant petit soldat de cette campagne de l' an I , Audrey Tautou en est devenue , bon gré mal gré , le porte drapeau .
au printemps , elle était sur tous les murs de France , proclamant son intention de changer la vie des spectateurs .
ceux ci ont été assez nombreux pour que Le Fabuleux Destin d' Amélie Poulain finisse l' année en tête du classement des entrées , avec plus de huit millions de spectateurs , auxquels s' ajoutent neuf millions de billets vendus de par le monde .
le cinéma français est coutumier de ces histoires à succès la dernière en date s' appelait Astérix et Obélix contre César , en mille neuf cent quatre vingt dix huit .
on a moins l' habitude de voir le phénomène se répéter plusieurs fois dans l' année .
les efforts de dernière minute de Harry Potter auront défait la belle homogénéité du quatuor de tête qui a régné presque toute l' année sur le box office .
le film de Chris Columbus vient s' intercaler entre Amélie et La Vérité si je mens deux , d' une part , Le Placard et Le Pacte des loups , d' autre part .
n' empêche , le millésime aura été fameux .
déjà , dans ce peloton de tête , on distingue quelques uns des symptômes de cette bonne santé : la présence du Pacte des loups montre que les comédies n' ont plus le monopole des records d' entrées .
même si leurs publics se recoupent par endroits , on voit bien que ces quatre films s' adressent à des tranches d' âges , à des catégories socioprofessionnelles différentes .
plus loin dans le classement , cette hétérogénéité des genres et des publics se confirme , Tanguy et Yamakasi font jeu égal , tout comme Belphégor et La Tour Montparnasse infernale .
tous les genres bénéficient de cet appétit pour le cinéma français , les films d' auteur aussi .
dans quel pays un film féministe militant , tourné en DV ( Digital Vidéo ) , attirerait plus de spectateurs qu' un gros polar américain avec John Travolta ?
chaos , de Coline Serreau , a attiré un cent quarante spectateurs , contre sept cent cinquante à Opération Espadon .
ils étaient presque six cent soixante dix pour aller voir Sous le sable , de François Ozon , plus de deux cents pour Va savoir , de Jacques Rivette .
cercle VERTUEUX Tout indique que le cinéma français s' est retrouvé pris dans un cercle vertueux , dont la rotation a commencé il y a déjà plusieurs années .
d' un strict point de vue économique , le moteur principal en est le ralliement du public jeune .
l' achat d' un ticket pour un film français n' est plus le stigmate d' une irrémédiable ringardise .
des passerelles ont été jetées entre télévision et cinéma , sur le mode de l' actualité Éric et Ramzy ont installé en haut de La Tour Montparnasse ...
la notoriété que leur a valu la série H ou de la nostalgie , avec Belphégor et Vidocq .
cette croissance de la fréquentation est stimulée et organisée par des outils de marketing de plus en plus sophistiqués .
l' emploi de slogans publicitaires , naguère réservés aux productions hollywoodiennes , devient la règle pour les films français ( " la petite moutarde qui monte au nez " de Wasabi ou " elle va lui apprendre les bonnes manières . il lui apprendra les mauvaises " de Sur mes lèvres ) .
chaque grosse sortie s' accompagne de la création d' un site Internet , et les bandes annonces deviennent de vrais spots , projetés dans les écrans de publicité , en attendant l' autorisation espérée par certains et redoutée par d' autres de la publicité pour les films à la télévision .
dans les salles , on peut mesurer l' impact des cartes d' abonnement illimité , qui représentent six pour cents des entrées en France .
parmi leurs effets , on peut compter le niveau toujours très élevé du nombre des films français exploités en salles ( deux cent dix neuf en deux mille un , contre deux cent vingt deux en deux mille ) .
si le nombre global des sorties a baissé ( de cinq cent trente neuf à cinq cent onze ) , c' est exclusivement en raison de la diminution du nombre de films hollywoodiens distribués en France .
la hausse du nombre de spectateurs ne suffit pas à assurer la viabilité commerciale de tous ces films .
elle a profité aux grandes entreprises d' un secteur où l' intégration verticale prédomine .
les champions du box office ont été financés puis distribués , voire exploités , par UGC ( Amélie Poulain ) , Warner ( La Vérité ...
) ou Gaumont Disney ( Le Placard ) .
en deux mille un , le taux de recouvrement de l' avance sur recettes a nettement progressé , passant de dix pour cents à vingt pour cents , mais ce chiffre indique bien que l' investissement que représente un film ne sera pas rentabilisé en salles dans l' immense majorité des cas .
cette donnée de base est depuis des lustres prise en compte et assimilée grâce aux différents systèmes de financement , avances sur recettes , obligation d' investissement des chaines .
quel que soit son avenir , c' est cet appareil qui a , encore en deux mille un , permis une nouvelle augmentation du nombre de films produits , qui devrait dépasser deux cents , contre cent soixante et onze en deux mille .
parmi eux , une nette augmentation des budgets supérieurs à quinze millions d' euros ( l' ancienne barre des cent millions de francs ) , qui passent de sept à neuf .
mais aussi des petites productions , dont certaines sont présentées à l' avance sur recettes après leur tournage .
ces films sont donc réalisés dans une économie dont sont absents les acteurs traditionnels , organismes parapublics ou chaines de cinéma .
beaucoup d' entre eux sont tournés en vidéo digitale , certains bénéficient de nouvelles sources de financement , comme les régions , l' Europe ou les fondations internationales .
encore marginal , ce phénomène permet déjà de faire avancer le débat sur la réforme du système français de financement du cinéma .