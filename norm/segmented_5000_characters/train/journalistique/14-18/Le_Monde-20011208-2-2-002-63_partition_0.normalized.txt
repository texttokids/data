maître absolu de Kandahar , qu' il avait remise sans combattre aux talibans , le cinq novembre mille neuf cent quatre vingt quatorze, le mollah Naqib , âgé d' environ cinquante cinq ans , est un ancien commandant moudjahidin qui a fait le djihad contre les soviétiques dans les rangs du Jamiat e Islami , le mouvement du président Burhanuddin Rabbani .
commandant réputé , qui avait sous ses ordres environ deux cinq cents hommes mobilisés par les talibans à la reddition de leur chef , le mollah Naqib avait mis Kandahar en coupe réglée .
ce fils de famille pauvre s' est considérablement enrichi durant les années de guerre .
originaire d' Arandab , Pachtoune appartenant à la tribu des Alokzaï , il n' a pas quitté Kandahar sous le régime des talibans .
s' il a aidé ces derniers à leurs débuts , il est ensuite resté neutre et s' est contenté de faire fructifier ses affaires tout en arbitrant les différends tribaux dans sa région .
il a été gravement blessé lors d' une tentative d' assassinat , au motif non éclairci , survenue au cours de ces dernières années .
( Corresp .
