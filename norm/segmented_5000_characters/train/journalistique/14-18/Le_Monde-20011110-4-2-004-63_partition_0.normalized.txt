le général tommy Franks , qui commande l' opération militaire des États Unis en Afghanistan , a déclaré , jeudi huit novembre , à Washington , que " l' effort de guerre prendra autant de temps qu' il faut " .
confirmant que des " combats importants " ont lieu autour de Mazar e Charif , il a estimé que cette ville représente " une tête de pont " susceptible de servir de base logistique , notamment pour les soldats américains et l' acheminement de l' aide humanitaire dans le nord du pays .
le général Franks s' est déclaré " très satisfait des progrès réalisés en un mois " .
" nous n' excluons pas , a t il ajouté , un engagement de troupes terrestres en nombre " et " nous n' écartons pas la possibilité de recourir aux forces " offertes par d' autres pays alliés l' Allemagne , la Grande Bretagne , la France ou l' Italie qui , à ce jour , ont prévu de mobiliser quelque treize hommes au total .
( AFP .
, Reuters .
