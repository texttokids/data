dessiner une carte des régions de l' Union européenne et tenter d' expliquer la place qu' occupent les vingt deux régions françaises dans cet ensemble , telle est la démarche de Joëlle Jacquier et nnie Kirthichandra , dans l' étude publiée le dix sept octobre par l' Institut national de la statistique et des études économiques ( Insee ) , intitulée Les Régions françaises dans l' Union européenne en dix neuf cent quatre vingt dix huit ( N huit cent dix ) .
loin de vouloir établir un classement des régions , cette étude tend avant tout à localiser les aires où se concentre la richesse produite sur le territoire européen .
cette approche géographique met plus en avant le produit intérieur brut ( PIB ) que la richesse disponible par habitant .
" cependant , souligne Jean Philippe Grouthier , chef adjoint du département des statistiques régionales de l' Insee , il s' avère délicat d' établir des comparaisons sur l' évolution des régions sur les dix ans passés tant nombre de pays ont modifié les limites administratives de ces entités . " hormis la France , qui maintient imperturbablement le découpage de ses vingt deux régions , la longue liste des bouleversements régionaux en Europe affecte toute mesure comparative entre les régions .
en Angleterre , en dix neuf cent quatre vingt dix sept , la région de Londres a été scindée en deux espaces , Londres centre et Londres périphérique , modifiant par là même son poids réel dans l' économie .
en Allemagne , depuis la réunification de dix neuf cent quatre vingt dix , les régions ont subi des transformations .
par exemple , le Bade Wurtemberg se scinde entre les régions de Stuttgart , Karlsrhuhe , Fribourg et Tuebingen .
l' Irlande vient récemment de créer deux régions sur son territoire .
de plus , sur un espace de trois mille deux cent quarante trois millions de kilomètres carrés , où vivent trois cent soixante seize millions d' habitants , les deux cent onze régions européennes offrent une grande diversité .
si la région européenne moyenne possède une superficie de quinze kilomètre deux , compte un , huit million d' habitants et crée une richesse globale de trente six milliards d' euros , de grandes disparités existent entre chacune d' entre elles .
la plus grande région , celle d' Övre Norrland , en Suède , s' étend sur cent cinquante cinq kilomètre deux , tandis que Ceuta y Melilla , en Espagne , ne couvre que trente kilomètres deux .
disparités encore par leur population : ainsi les vingt six habitants de la région des îles Aland , en Finlande , s' opposent aux onze millions de l' Ile de France .
au sein de l' espace européen , l' étude de Joëlle Jacquier et Annie Kirthichandra démontre le rôle dynamique des régions capitales qui contribuent le plus à la formation du PIB .
sur les quinze régions capitales de l' Union , dix d' entre elles bénéficient de la réunion des principales fonctions économiques et politiques .
par ailleurs , la première région de l' Union est aussi une région capitale .
il s' agit de l' Ile de France , qui contribue à la formation de cinq pour cents du PIB de l' Union .
l' étude précisant que " l' Ile de France devance même en termes de richesse produite les deux tiers des pays membres et se place devant les Pays Bas , la Grèce , la Belgique , le Portugal , la Suède , l' Autriche , le Danemark , la Finlande , l' Irlande et le Luxembourg " .
forte CONCENTRATION Après l' Ile de France , la seconde région la plus dynamique de l' Union est la Lombardie .
seules dérogent à ce schéma des régions capitales celles d' Allemagne , d' Italie , d' Espagne , de Belgique et des Pays Bas .
dans chacun de ces cinq États , les premiers pôles économiques sont situés hors de leurs capitales .
ainsi , en Allemagne , Berlin n' occupe que le huitième rang , derrière les régions de Munich , Duesseldorf , Francfort , Stuttgart , Cologne , Dortmund et Karlsruhe .
ou encore en Belgique , la région d' Anvers dépasse celle de Bruxelles .
de plus , le paysage économique européen en dix neuf cent quatre vingt dix huit est marqué par une forte concentration .
cette année là , un tiers des régions européennes ont contribué à la formation des deux tiers du PIB communautaire .
la France , par sa position géographique , s' intègre tout naturellement dans ces vastes zones dynamiques .
les régions qui vont de la Bretagne au Nord Pas de Calais s' insèrent dans le couloir du Nord qui borde la Manche jusqu'à la Baltique .
ces régions couvrent quinze pour cents de la superficie de l' Union et produisent plus du quart de ses richesses .
à l' est , l' Alsace est englobée dans la " banane bleue " .
cette vaste zone qui s' étire de la mer du Nord à l' Italie septentrionale couvre le huitième de la superficie de l' Union , mais produit le tiers de ses richesses .
enfin , les deux façades maritimes jouent également leur rôle .
celle de l' Atlantique , de l' Ecosse au Portugal , couvre quinze pour cents du territoire et produit dix pour cents des richesses .
et la zone méditerranéenne , avec près du cinquième de la superficie communautaire , produit plus du dixième de ses richesses .
ainsi se dessine une carte économique de la France qui explique sa deuxième position dans la création du PIB de l' Union européenne , avec dix sept pour cents , juste avant l' Angleterre ( seize , cinq pour cents ) .
la première place revenant à l' Allemagne , avec vingt cinq pour cents .