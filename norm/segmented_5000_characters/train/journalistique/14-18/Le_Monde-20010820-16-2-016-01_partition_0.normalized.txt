août vingt cent un. brigitte Fontaine prend du repos en pays de Fréhel , à l' Hôtel de la Plage , pension complète , appétit d' oiseau .
Kékéland , son album , est sur le point de naitre .
elle observe les couleurs changeantes de la Bretagne de sa fenêtre , qui donne sur la mer , mais n' y touche pas .
elle craint le soleil , elle est sous antibiotiques , au restaurant on lui garde , numérotée , sa bouteille de Vichy Saint Yorre .
elle a amené son chat , une chatte nommée Slapette , " car elle s' la pète grave " , précise Brigitte Fontaine en se drapant , l' air chic , dans son gilet breton , un vrai , en velours , orné de broderies et de lacets , acheté il y a peu dans un vide grenier du bourg de Pléhérel .
Slapette la Parisienne ne fuit pas sur les toits , pourtant " elle calcule " .
soudain , les yeux de Brigitte Fontaine reflètent la géométrie du chat .
elle pourrait sauter .
pourquoi aimer Brigitte Fontaine ?
parce que c' est elle , parce que c' est nous .
fontaine est singulière , elle ne déçoit pas .
on l' aime , ou pas .
une journaliste japonaise amie , inconditionnelle , fan , admiratrice , fidèle , une de celles qui ont sauvé la chanteuse de l' étouffement des années dix neuf cent quatre vingts est venue passer quelques jours en sa compagnie , au bout des terres , entre Sables d' Or les Pins et le cap Fréhel ( Côtes d' Armor ) .
il y a la cousine germaine , et puis Maryse , la patronne de l' hôtel : tout un environnement .
totalement BRETONNE Brigitte Fontaine aime Fréhel , chanteuse réaliste , " la plus grande " , bretonne comme elle , née Marguerite Boulc' h en dix huit cent quatre vingt onze dans le Finistère , morte alcoolique et héroïnomane en dix neuf cent cinquante et un , et qui prit comme nom d' artiste celui de cette terre sauvage .
fontaine , une " néoréaliste " à coeur ouvert , aurait pu s' appeler Morlaix , tant elle a aimé cette ville du nord de la Bretagne où elle est née .
" c' est une ville superbe , j' y ai trouvé une chaleur , une chaleur venant de l' esprit . il y a un grand viaduc , du granit et de l' ardoise , la mer est à cinq kilomètres , mais c' est une ancienne cité de corsaires . " à l' évocation de ce mot rebelle , les yeux de Fontaine retrouvent la logique du chat .
bretonne , Brigitte Fontaine , égérie du rock ( de la chanson , du jazz ) alternatif des années dix neuf cent soixante cinq dix neuf cent soixante quinze ?
oui , bretonne , totalement bretonne .
des Côtes d' Armor , adolescente frustrée d' avoir déménagé à Brest , " si nis tre . bombardée , reconstruite , stalinienne . une grande ville avec plein de fils d' officiers de marine au lycée . " Brigitte Fontaine fume des Craven À , elle porte des sandales de raphia , carrées , à la japonaise , une tunique blanche , et le fameux gilet marron à fines rayures , et tient un sac Viahero , plastique transparent rose , très mode , très Saint Trop .
ainsi , elle ressemble au blason pastiche deux lions , des angelots , la tour Eiffel , la statue de la Liberté qui orne la pochette de Kékéland , avec cette mention : " Queen of Kékéland " .
" mais là , avec mon gilet , je suis Anne de Bretagne , ou bien la reine de l' île . " deux préceptes de base avec Brigitte Fontaine : un ) " il ne faut jamais dire de mal de la Bretagne devant Brigitte " , avait un jour précisé Areski , son compagnon .
deux ) quand elle parle de " l' île " , il s' agit bien entendu de l' île Saint Louis , à Paris , où elle vit et où elle concocte des aphorismes du genre : " ne prenez pas vos désirs pour des banalités " , " les coqs ne dorment jamais , ce n' est pas raisonnable . " elle a lu " au moins " quinze fois Les Liaisons dangereuses , de Choderlos de Laclos , et redit que Les Misérables , de Victor Hugo , sont un chef d' oeuvre absolu .
elle approuve à la défense du microclimat du Fréhel , et réprouve la mention " temps breton " qui sert à qualifier tout temps de chien à travers le monde .
" je suis chauvine . la Bretagne , c' est beau , très beau , la lumière est douce et je déteste le soleil violent . " à Fréhel , la lande est superbe : bruyère cendrée , bruyère ciliée , bruyères à quatre angles , rossolis et callune , " et les ajoncs du début de l' été " , que viennent parasiter la cuscute , aussi appelée " cheveux du diable " .
en la contemplant de sa fenêtre , et en s' aidant du dictionnaire d' anglais de l' hôtel , elle a écrit God' S Nightmare , le cauchemar de Dieu , " like a methyst moor , raped by wind and sea ... " .
ici , Brigitte Fontaine est parmi les gens normaux , les familles de la pension , les habitués du bar .
elle n' a jamais voulu s' extraire de l' humanité .
elle est fille , petite fille , arrière petite fille d' instituteurs .
en Bretagne , " cela veut dire quelque chose . les Bretons ont été les derniers christianisés , ils sont donc devenus farouchement religieux . l' instituteur laïque , c' était comme le diable " .
les Misérables , certes , mais aussi la Bretagne pauvre de l' intérieur , le pouvoir politique de l' Eglise , la lutte pour l' égalité républicaine tels que décrits dans Le Cheval d' orgueil , de Pierre Jakez Hélias , auraient ils davantage produit Comme à la radio , La Lettre au chef de gare de Latour de Carol que la révolution du free jazz et du Living Theater et Mai soixante huit réunis ?
les deux , mon capitaine , auxquels il faut rajouter la Kabylie d' Areski , le compagnon , le mari , le musicien , né en France de parents algériens .
donc , quand Brigitte Fontaine ponctue , invective , c' est en arabe ( " Inch' Allah , abdoullah " ) et en breton .
" ma grand mère maternelle parlait très bien le breton . ma un peu , mon père pas du tout . moi , je dis souvent des mots en breton , par exemple , quel coeuteuren ! quel gourbi ! , ou hrouiziquel , un frimeur , enfin des tas de mots . " à Morlaix , à la fin vingt cent zéro , Brigitte Fontaine enflamme la jeunesse locale avec ces mots là , et elle casse la baraque , elle qui a théoriquement dépassé l' âge des folies , mais dont le public ne cesse de rajeunir depuis Les Nougats , chanson hallucinatoire qui présida à son retour médiatique en dix neuf cent quatre vingt treize .
qu' est ce qu' ils lui trouvent les jeunes ?
" je ne sais pas , ils se reconnaissent dans mon envie de bouger , de changer , d' avancer , et ils aiment sans doute une présence sincère et en même temps un peu , qui leur plait . pour l' amour , personnellement , je préfère les vieux , plus de quarante ans , ils sont plus sexe . " les mots comme des bonbons Brigitte Fontaine aimerait aller en Écosse , mais " c' est loin , et personne ne veut venir avec moi " , et surtout aux Orcades , des îles au nord du pays frère en celtitude .
elle ne croit pas aux mystères , elle croit au mystère .
la Bretagne est belle , mais ce n' est pas tout : " l' héritage des Celtes est très important , ils ont laissé une mythologie , une mystique passionnantes . et j' y trouve la liberté des femmes . les trois religions monothéistes ne sont pas faites pour nous . or , dans ce domaine là , je suis pour la lutte armée . si j' habitais en Afghanistan ou en Iran , je prendrais les armes . " Brigitte Fontaine a lu les ouvrages de Jean Markalé , universitaire spécialiste du roi Arthur , d' Halloween , de Merlin , de Lancelot et du Graal .
un jour , en consultant des livres à la Bibliothèque publique du Centre Pompidou , à Paris , elle a trouvé cette définition druidique de Dieu : " dieu est un point de liberté où se font équilibre toutes les oppositions . " yeux plissés , regard perçant , mémoire d' éléphant , fragilité physique , Fontaine ponctue chaque mot d' un silence .
les mots donnent envie de les savourer " comme les bonbons " .
de les travailler .
" on a de la chance d' être artiste , dit elle . la musique est une grande joie , une sauvegarde , contre les douleurs , les vulgarités , la tristesse , et une merveilleuse façon de gagner sa vie . " et une manière de contre carrer la peur , " peur de tout , donc de rien " , que Brigitte Fontaine veut écarter , car " j' ai peur qu' elle ne soit contagieuse " .
Brigitte Fontaine ne se barricade pas , elle s' entoure .
de Jacques Higelin , d' Areski , toujours , hier du Chicago Art Ensemble , aujourd'hui de Sonic Youth , M , ou de Noir Désir .
mais qu' est ce qu' un " kéké " terme sans doute d' origine provençale ?
" un louf , un rigolo , c' est très large comme sens . un pote . je suis la reine de l' île ( Saint Louis ) , où il y a des bourgeois , oui , mais surtout beaucoup de kékés , l' île aux fous , où les gens ont tous un grain , un filet . " sur la lande de Fréhel , la lumière se voile , les campeurs sont au bain , Brigitte Fontaine se repose à l' intérieur .
saint hilaire , le patron de la commune , s' assoupit dans la pierre .
" y' a des zazous " , chantait Andrex , y' en a partout , y' en a encore , et des Demi clochardes prêtes à déclamer Rimbaud au Centre Pompidou , à chanter Je suis conne charge féministe a contrario au Festival de Cannes pour fêter la sortie de L' Amour à mort de son vieux copain Romain Goupil , " mais lui alors , il n' est pas kéké du tout , du tout " .
la reine a dit .