les deux minorités de gauche du PS , la Gauche socialiste et les amis d' Henri Emmanuelli , ne déposeront pas de texte alternatif au projet de Martine Aubry .
Monsieur Emmanuelli devrait proposer , au bureau national du dix huit décembre , des amendements sur le volet économique et social , notamment sur les " effets pervers de la prime pour l' emploi " .
la Gauche socialiste défendra un amendement sur le retour aux trente sept , cinq annuités de cotisations pour une retraite à taux plein .
elle devrait amender le projet sur l' Europe , en commun avec Monsieur Emmanuelli , pour réclamer une " Europe fédérale " .
certains amendements seront soumis au vote des militants .
d' autres seront intégrés au projet .
cette dernière démarche a les faveurs des amis de Laurent Fabius .
pour Henri Weber , le projet " s' inscrit dans la continuité " de l' action de Lionel Jospin et constitue " un bond en avant et non un pas de côté " .
mais il a déploré des " manques " , notamment sur la stratégie industrielle .