un vous êtes président du conseil d' administration et fondateur de Gemplus .
Etes vous en désaccord avec les mesures de restructuration prônées par le PDG , Antonio Perez ?
les restructurations et les licenciements ne sont pas dans la culture de Gemplus .
j' ai discuté des divers plans possibles .
mon rôle est d' expliquer aux managers qu' on ne peut pas agir en France comme aux États Unis .
pour eux , lorsqu' il y a un ouragan , il faut foncer dedans et baisser les voiles , je pense qu' il faut plutôt tenter de le contourner , faire le dos rond et accepter les pertes .
mais c' est comme si j' étais sur le banc de touche et je bous quand je vois les autres jouer .
deux quelle est la stratégie de Gemplus ?
l' idée n' est plus de creuser l' écart avec les concurrents .
c' est une stratégie plus prudente qui consiste à se concentrer sur le marché de la téléphonie mobile , des services financiers et du commerce électronique .
Gemplus était une flottille avec une dizaine d' activités , aujourd'hui , c' est un tanker avec trois activités de base .
mais Gemplus a racheté le vingt août une entreprise spécialisée dans les transactions via les téléphones mobiles .
trois comment réagissez vous aux critiques sur les " cadeaux " accordés aux dirigeants , et aux craintes d' une délocalisation des activités de Gemplus ?
ces " packages " proposés aux dirigeants l' ont été alors que le phénomène Internet était à son paroxysme .
les managers recevaient des centaines d' offres .
c' était nécessaire pour débaucher .
mais les accusations d' abus de droit sont complètement fausses .
pour les délocalisations , il faut trouver les solutions optimales .
la Pologne offre des couts de production intéressants mais rien n' est encore fait .
nous souhaitons maintenir les deux à trois emplois en France .
nous nous donnons deux ans pour reconvertir les salariés qui sont en production vers des emplois de services .
pour l' unité de Sarcelles ( Val d' Oise ) , nous négocions avec les élus .