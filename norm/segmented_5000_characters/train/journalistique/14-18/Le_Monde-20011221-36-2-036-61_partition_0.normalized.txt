l' argentine en faillite , ou quasi .
des émeutes de la faim .
des pillages , des morts déjà .
l' état de siège .
l' argent liquide qui dément son appellation et ne coule pratiquement plus .
la simple vie quotidienne devenue un enfer , un parcours du combattant .
les queues qui s' allongent .
les banques prises d' assaut .
les salaires qui n' arrivent plus .
les manifestations qui se multiplient .
les retraités sans retraite , les jeunes qui s' expatrient , le personnel politique déconsidéré et voué aux gémonies .
la récession qui galope .
ou plutôt grignote , creuse son trou , mois après mois , depuis trois ans .
l' Argentine à la rue .
financièrement et humainement .
à l' index aussi , au piquet comme cancre du FMI après en avoir été paraît il le bon élève .
ou pis encore , comme un pays interdit bancaire , incapable d' honorer ses dettes , ses débuts comme ses fins de mois .
l' Argentine , toute proche de la révolte sociale autant que du gouffre .
alors , bien sûr , laissons aux spécialistes le soin et la tâche d' expliquer comment tout cela est possible , réel , tragiquement et économiquement réel .
et contentons nous , ici , de dire comme notre imaginaire a du mal à appréhender cette réalité là , aussi radicale qu' un gigantesque rouge bancaire .
pour nous en effet , dans l' idée que nous nous en faisons , l' Argentine c' est , c' était autre chose .
un pays raisonnablement doté .
raisonnablement développé .
raisonnablement peuplé , éduqué et cultivé .
raisonnablement agricole et industriel .
pas du tout , a priori , un état en voie de développement , un pays sans ressources , ni avenir , oublié du progrès , menacé par la faim , contourné par le modernisme et plombé par ses handicaps et retards .
un pays raisonnablement moderne en somme dont on ne voit guère comment , tel un vulgaire délinquant financier , il peut se retrouver au grand tribunal monétaire international , en position de failli .
ou en dépôt de bilan et cessation de remboursements , comme république bananière qui aurait avalé la grenouille et dont les dirigeants auraient dilapidé les finances au grand casino mondial .
l' Argentine , ainsi présumée , et faussement présumée , ses malheurs le prouvent , nous paraissait faire partie , par son rang et sa nature , des pays hors d' atteinte d' un écroulement économique total .
parce que ni très riche , ni très pauvre , mais costaud , simplement costaud .
au lieu de quoi , et c' est ici qu' il nous faut ravaler notre sommaire vision de l' ordre économique des choses , ce qui arrive à ce pays nous ramène très fort à de vieux et vagues souvenirs de l' autre siècle , on parle du XXe .
l' Argentine en débâcle , et cette panique du lendemain , c' est pour nous ce qu' on nous racontait de l' Allemagne , jadis , de ces gens partant acheter trois kilos de pommes de terre avec pour ainsi dire des brouettes de millions de marks .
ou , encore , c' est cette histoire qui nous faisait rêver autant qu' elle nous intriguait , cette histoire économique antédiluvienne , quand il se disait qu' au Brésil la crise fut si grave que les locomotives à vapeur y fonctionnèrent un temps en brulant du café plutôt que du charbon .
l' Argentine , c' est la preuve que l' économie , aussi , est un éternel recommencement .
comme un dix neuf cent vingt neuf en deux mille un .