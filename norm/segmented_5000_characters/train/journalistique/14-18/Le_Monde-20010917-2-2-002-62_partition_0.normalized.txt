les réservistes rappelés par Monsieur Bush s' apprêtent à quitter leurs emplois civils pour renforcer les capacités de défense dans les ports et les bases aériennes et seconder les équipes de sauveteurs qui tentent de retrouver des victimes dans les ruines du World Trade Center .
l' armée évalue à trente cinq hommes ses besoins pour faire face aux suites des attentats : dix pour l' armée de terre , treize pour l' armée de l' air , trois pour la marine , sept cinq cents pour le corps des Marines et deux pour les garde côtes .
les réservistes rappelés par les garde côtes aideront aux opérations de sécurisation des ports , avec une attention particulière portée sur les cargos et les bateaux de croisière , susceptibles de se transformer en bombes flottantes ou en cibles mouvantes .
ils vérifieront les listes de passagers .
les réservistes rappelés par l' armée de l' air aideront quant à eux à faire fonctionner les vingt six bases où des avions de combat sont en alerte , prêts à décoller en quinze minutes .
( AFP .
