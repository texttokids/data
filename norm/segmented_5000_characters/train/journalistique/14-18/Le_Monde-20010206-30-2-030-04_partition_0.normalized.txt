une sensation fugace peut elle être prophétique ?
pourquoi le souvenir de mille neuf cent quatre vingt six me revient il en mémoire ?
la Fédération française de handball et Christian Picard organisaient le deuxième Tournoi des capitales .
à cette occasion , l' équipe de France avait pris les couleurs de Paris .
le Marseillais que j' étais encore , fraichement appelé à entrainer le sept tricolore , ressentit une intense émotion en pénétrant , pour la première fois , dans cette enceinte , à présent mythique .
l' idée folle ( à l' époque ) que , peut être , un jour nous pourrions y revenir pour disputer une vraie compétition me traversa l' esprit .
déjà , lors du Mondial B de mille neuf cent quatre vingt neuf , nous étions venus y mettre la dernière touche à notre initiation pour entrer dans la cour des grands .
plus tard , quand Jackson Richardson nous eut rejoints , nous y sommes venus régulièrement pour vous montrer nos progrès étayés par de bons résultats aux quatre coins du monde .
quand naquit l' intention d' organiser , dans un premier temps , le Mondial mille neuf cent quatre vingt dix neuf , nous savions pouvoir compter sur un écrin digne d' abriter les finalités .
plus besoin , comme en mille neuf cent soixante dix , de transformer un hall sommaire de la porte d' Ivry ( Paris , treizième ) en théâtre sportif adapté à un évènement planétaire .
restait à en obtenir la charge , ce qui fut fait pour l' édition deux mille un .
huit sites de province auront la responsabilité des deux premiers actes , rivalisant de ferveur populaire , de sens de l' hospitalité et d' amour pour ce sport .
l' Espoir de voir la France atteindre le dernier carré était , bien sûr , entretenu .
par la grâce d' un but miraculeux de l' inimitable Jackson Richardson , la prophétie de mille neuf cent quatre vingt six s' est donc réalisée .
quoi qu' il en soit , aujourd'hui , le handball français aura réussi son rendez vous avec l' histoire .
sans se travestir , en assumant ses paradoxes , il aura tenu toutes les promesses exprimées çà et là , et parfois au détriment de la prudence la plus élémentaire .
il reste , à présent , à ne pas avoir la mémoire courte .
souvenons nous et sachons dire : Bercy beaucoup !