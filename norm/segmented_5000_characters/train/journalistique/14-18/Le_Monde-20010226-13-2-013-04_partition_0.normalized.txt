derrière la liste , cherchez l' adresse .
le maire sortant , Michel Destot deux noeuds , grand amateur de montagne , genre piolet crampons , a choisi d' installer son local de campagne dans un ancien bar , le Café de Londres , haut lieu du rugby d' antan .
son adversaire de droite , Max Micoud , qui a pendant dix ans présidé aux destinées du FCG Rugby et cultive à plaisir son appartenance au monde de l' Ovalie , a élu domicile dans un garage , jouxtant un magasin spécialisé dans le vêtement Thermolactyl .
au risque de créer la confusion entre ses affiches , où cet homme de soixante huit ans apparait vêtu d' un blouson confortable et pratique , assorti d' un col roulé bien chaud , et les publicités du magasin qui vantent ce style de modèles .
enfin , tout en faisant liste commune , les écologistes et la gauche citoyenne font local à part : les premiers au bout de la très bourgeoise rue Voltaire , les seconds dans une artère de la Vieille Ville .
( Corresp .
