" un vrai talent . " le compliment est troussé par Alain Jardel , l' entraîneur de l' équipe de France féminine de basket ball .
il vise Cathy Melain , arrière ailière d' une formation tricolore qui , invaincue après avoir défait l' Espagne ( soixante dix soixante quatre ) lors du dernier match de poule , mercredi dix neuf septembre à Orléans , rencontrera la Slovaquie en quart de finale du vingt huitième championnat d' Europe des nations , vendredi vingt et un septembre , au Mans .
" elle incarne ce qu' est une star selon moi " , relève Alain Jardel , qui a fait de cette jeune femme ( vingt sept ans ) l' un des piliers de son groupe .
star , le terme peut sembler incongru lorsqu' on l' accole au nom de Cathy Melain , tant cette Bretonne née à Rennes affiche un " tempérament en retrait , non expressif , modeste " , comme le décrit Alain Jardel .
hors des parquets , celle qui va aborder sa septième saison en club à Bourges , est la discrétion incarnée .
" si elle s' exprime , c' est sur le terrain " , tranche Alain Jardel .
mais il ne faut pas s' attendre à la voir haranguer ses coéquipières à la manière de la capitaine , Yannick Souvré .
" mon leadership est plus dans l' action que dans la parole , convient Cathy Melain , je veux essayer de montrer l' exemple , être combative , parce que peut être que chacune se battra un peu plus . " mais il lui arrive aussi de " dire des choses sans diplomatie " à ses coéquipières .
" quand ça sort , ça pète . c' est sur des problèmes de combativité , de placement " , dit elle .
" elle démontre par sa simplicité qu' être une star c' est se conformer aux instructions quant aux habitudes du groupe et être forte au moment voulu " , poursuit Alain Jardel , selon qui , " s' il n' y avait pas eu Cathy contre la Pologne " , les Bleues auraient eu quelques soucis à refaire leur retard , puis à gagner .
avant qu' Edwige Lawson n' enflamme le dernier quart temps , c' est sous l' impulsion de Cathy Melain que les françaises avaient commencé à se remettre à l' endroit .
contre l' Ukraine , c' est déjà elle qui , blessée et ne devant pas jouer , avait demandé à entrer et avait remis d' aplomb des Bleues peinant à contenir leurs adversaires .
" une vraie championne de la dimension de Cathy Melain est toujours présente . c' est une joueuse magnifique " , savoure Alain Jardel à propos de cette fille unique , venue au basket ball par " besoin du partage " , après avoir pratiqué le judo .
défendre , attaquer , Cathy Melain sait tout faire .
sur cet Euro , elle est la meilleure intercepteuse de ballons , la deuxième rebondeuse et la troisième marqueuse de l' équipe de France .
elle sait tout faire Elle est aussi celle qui a provoqué le plus de fautes chez les adversaires .
désignée meilleure joueuse du championnat de France ces trois dernières saisons , elle a été élue meilleure joueuse européenne de l' année deux mille par un jury d' entraîneurs , joueuses et journalistes réuni par le quotidien italien La Gazzetta dello sport .
le titre suprême sur cet Euro , " j' y tiens " , affirme Cathy Melain , qui considère qu' en cas de victoire en quart de finale " tout est possible " .
ensuite , elle se verrait bien glaner de nouveaux titres de championne de France et d' Europe elle en compte respectivement cinq et trois avec Bourges , où elle est encore sous contrat pour un an .
mais , au delà , elle avoue " des envies d' aller voir ailleurs " .
hors de France .
" peut être en WNBA " , le championnat américain , glisse t elle , avouant avoir " jusqu'à présent repoussé les échéances " , en raison des JO , puis de l' Euro .
mais elle n' en fait pas une obsession : " tout cela viendra si ça vient . je n' ai pas envie de me dire : dans un an , je serai là . je n' ai jamais eu de plan de carrière . "