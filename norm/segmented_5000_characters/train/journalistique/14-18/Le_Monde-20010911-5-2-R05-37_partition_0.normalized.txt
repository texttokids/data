près d' un an après le déclenchement de l' Intifada al Aqsa et alors que la situation ne cesse de se dégrader , les conséquences sont lourdes pour la région .
les premiers touchés sont les palestiniens , dont l' économie est largement dépendante d' Israël dans toutes ses composantes : marché du travail , marché des biens et services , commerce extérieur , marchés financiers .
les prévisions d' une croissance de sept pour cents du produit intérieur brut ( PIB ) ont été revues à la baisse à la suite de l' effondrement de l' activité .
le chômage atteint des niveaux records dans la bande de Gaza et en Cisjordanie après le bouclage de ces territoires imposé par l' armée israélienne pour des raisons de sécurité .
selon l' Organisation internationale du travail ( OIT ) , les taux de chômage , pour le premier trimestre , sont de quarante huit , huit pour cents pour Gaza et de trente deux , cinq pour cents pour la Cisjordanie ( aujourd'hui , il est proche de quatre vingts pour cents à Gaza ) .
la pauvreté ne cesse de croître puisque les palestiniens , qui étaient plus de cent trente à se rendre en Israël pour travailler , n' ont plus accès à ce marché .
dix , sept pour cents des familles n' ont plus aucune source de revenus depuis le déclenchement de l' Intifada et quarante neuf , deux pour cents d' entre elles ont perdu plus de la moitié de leurs ressources .
le revenu mensuel moyen d' une famille est passé de deux trois cents shekels ( cinq cent quatre vingt seize euros ) à un deux cents shekels ( trois cent dix euros ) en avril .
le coordonnateur spécial de l' ONU pour le processus de paix au Proche Orient , Terje Roed Larsen , affirmait , fin juin , que l' économie palestinienne perdait de sept à dix millions de dollars par jour ( de sept , soixante et onze à onze millions d' euros ) en raison du bouclage israélien de la Cisjordanie et de Gaza .
si le tourisme est touché , l' agriculture l' est fortement aussi .
ce secteur représente seize pour cents du PIB .
de nombreux arbres fruitiers ont été arrachés , l' accès à l' eau est difficile et , conséquence directe du bouclage , les productions n' arrivent pas à être exportées .
d' après le ministère palestinien de l' agriculture , les pertes subies par le secteur agricole ont atteint près de deux cent quatre vingt huit millions de dollars ( trois cent dix sept millions d' euros ) entre octobre et mai .
il en va de même pour les entreprises qui n' arrivent pas à faire venir leurs matières premières et qui souffrent de problèmes de liquidités .
nombre d' entre elles , souvent des entreprises familiales , ont dû mettre la clé sous la porte .
les caisses sont vides car les autorités israéliennes bloquent le versement du revenu de taxes de douane transitant par son territoire et empêchent le transfert des avoirs des banques palestiniennes déposés dans les banques israéliennes .
facilité DE TRÉSORERIE Face à ces problèmes de liquidités , les donateurs internationaux se sont mobilisés , dont l' Union européenne deux noeuds , contributeur important .
une facilité de trésorerie de quatre vingt dix millions d' euros a été accordée sous forme d' assistance budgétaire à l' Autorité palestinienne .
la Banque islamique de développement ( BID ) , qui a été choisie pour gérer les fonds arabes de soutien à l' Intifada , a recueilli cinq cent trente huit millions de dollars ( six cent quarante cinq millions d' euros ) distribués sous forme de prêts et de dons pour des projets hospitaliers , éducatifs et d' infrastructures .
en Israël , l' Intifada a bien évidemment de fortes répercussions sur l' économie du pays , plus particulièrement dans le domaine touristique , où l' on enregistre une baisse des recettes de trente cinq pour cents .
la Fédération des chambres de commerce israéliennes a estimé les pertes liées au tourisme à un milliard de dollars ( mille millecent douze milliard d' euros ) au cours des huit premiers mois du soulèvement .
les halls des grands hôtels sont désertés par les touristes aussi bien à Tel Aviv , Jérusalem , Ramallah qu' a Amman .
mais la chute du NASDAQ , où sont cotées une centaine de sociétés israéliennes , a causé plus de dommages encore .
de nombreux investisseurs ont , en effet , rapatrié une partie de leurs capitaux pour faire face aux pertes subies aux États Unis .
néanmoins , avec la poursuite de la tension , les conséquences de l' Intifada sont de plus en plus visibles sur l' économie du pays : la confiance des investisseurs est mise à mal , et , on assiste à une reprise du chômage après trois ans de déclin continu .
l' armée , du fait du rappel des réservistes , a demandé une rallonge au gouvernement évaluée à sept cent vingt millions de dollars ( huit cents millions d' euros ) qui s' ajoute aux neuf milliards de dollars ( dix milliards d' euros ) du budget initial .
le niveau des investissements a baissé .
la Banque d' Israël a avancé le chiffre de un , sept milliard de dollars ( un , neuf milliard d' euros ) d' investissements directs étrangers ( IDE ) pour les six premiers mois de l' année deux mille un contre deux , deux milliards au cours de la même période en deux mille .
la croissance du PIB ne devrait être que de un pour cent pour deux mille un contre six pour cents l' année dernière .
les répercussions de la crise sont importantes sur la croissance jordaniennes ( elle ne devrait pas dépasser trois pour cents , contre quatre pour cents en deux mille ) , ainsi que sur le tourisme .
sur les six derniers mois , il semble que l' occupation des hôtels soit seulement de vingt pour cents .
autre élément important , l' arrivée en nombre important , sans qu' il soit possible de le quantifier précisément , de palestiniens dans le royaume hachémite qui ont profité de la période estivale pour quitter les territoires .
indispensables pour l' économie du royaume , le développement des zones économiques qualifiées ( QIZ ) pourrait être affecté par la tension .
définies lors de la conférence de Doha ( Qatar ) en mille neuf cent quatre vingt dix sept , alors que les avancées du processus de paix étaient significatives et qu' il était fortement question de développement économique régional , ces zones doivent permettre à des produits fabriqués dans certaines zones industrielles jordaniennes par des sociétés qui font appel à une valeur ajoutée israélienne , palestinienne ou jordanienne , d' être exportés ensuite vers Israël et les États Unis , en étant exonérés de taxes et en échappant à la contrainte des quotas .
au delà de ce premier cercle , les conséquences de la crise ont un impact sur le tourisme en Syrie , au Liban et en Égypte .
les touristes israéliens , qui étaient nombreux à se rendre dans le Sinaï , préfèrent aujourd'hui aller à Eilat .
les autorités égyptiennes se tournent vers les pays européens pour compenser la baisse d' activité .
dans ce contexte , l' idée selon laquelle seule la séparation entre les Israéliens et les palestiniens pourrait permettre de ramener la paix est une pure illusion car les deux économies sont fortement imbriquées .
les implications concernent le commerce palestino jordanien qui a chuté de vingt cinq , cinq pour cents au cours du premier semestre par rapport à l' année précédente , et les échanges palestino égyptiens car les flux de marchandises sont obligatoirement contrôlés par les services douaniers israéliens .
l' asphyxie des palestiniens par les Israéliens alimente leur désespoir , et le déséquilibre entre deux populations condamnées à vivre ensemble a atteint une dimension telle que les perspectives sont bien sombres .