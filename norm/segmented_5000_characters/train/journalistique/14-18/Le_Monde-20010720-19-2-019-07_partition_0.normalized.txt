avant de débuter , cette cent trentième édition du British Open aura fait onze fous de joie , des joueurs qui ont gagné le droit de participer à cette vénérable épreuve .
avec deux deux cent vingt cinq autres , ils s' étaient inscrits pour disputer un premier tour de qualification au début du mois sur seize parcours en Angleterre et en Irlande .
au terme des dix huit trous disputés simultanément , ils n' étaient plus que cent vingt quatre à avoir le droit de prendre part au deuxième tour de qualification .
en face d' eux , ils retrouvaient deux cent soixante joueurs , issus en majorité du circuit européen , qui n' avaient pas un accès direct à l' épreuve la plus importante du continent .
tout était donc à recommencer pour les premiers élus , avec une concurrence plus rude .
sur les joueurs alors en lice , trente quatre à peine gagneraient le " paradis " .
parmi eux , les onze inconnus , qui n' avaient pourtant fait que le plus facile puisque , à partir de jeudi dix neuf juillet , c' est avec les cent vingt deux meilleurs mondiaux qu' ils rivaliseront sur ce par soixante et onze , long de six deux cent quinze mètres .