pierrick sorin est un artiste vidéaste qui voyage en chambre .
né à Nantes en mille neuf cent soixante , il y vit , y travaille , et ça lui convient .
c' est là , à l' âge de douze ans , qu' il a commencé à utiliser la caméra super huit de son père .
deux ans plus tard , son père tenant la caméra , Sorin , admirateur de Sergio Leone , ne trouvait pas meilleur sujet que filmer son propre suicide , un sujet commode quand on n' a pas les moyens de se payer des acteurs .
vocation contrariée : Sorin sera d' abord instituteur puis , trop âgé pour entrer à l' Idhec , il fera l' Ecole des beaux arts de Nantes .
c' est à la fin de ces nouvelles études , en mille neuf cent quatre vingt sept mille neuf cent quatre vingt huit , qu' il réalise ses premiers autofilmages en super huit .
il passe au numérique en mille neuf cent quatre vingt dix .
premiers succès en mille neuf cent quatre vingt douze à l' ARC , en mille neuf cent quatre vingt treize au Salon Découvertes , à la Biennale de Venise , à la FIAC , et en mille neuf cent quatre vingt quatorze avec La Bataille des tartes , une création pour l' inauguration de la nouvelle Fondation Cartier , boulevard Raspail , à Paris .
depuis , on n' a plus arrêté de le voir .
c' est qu' il est très productif et qu' il plait beaucoup .
parce que tout le monde peut comprendre ce qu' il montre , qu' il peut faire rire , mais aussi attendrir .
d' aucuns disent de lui qu' il est le Keaton de la vidéo .
Sorin est partout dans ses films , tantôt lui même , le petit Sorin , tantôt dans la peau de Jean Loup , le frangin qu' il n' a pas eu .
Sorin est fils unique , il le dit et le redit , et a appris , enfant , à jouer tout seul avec des compagnons imaginaires .
puis à se filmer en vamp , pute , travelo , artiste , souffre douleur .
on l' a vu sous toutes les coutures , dans toutes les postures intimes , maladroit , débordé par un quotidien envahissant , gesticulant en arroseur arrosé ou en entarté aussi dramatique qu' un lapidé .
qui se cache derrière tous ces Sorin potentiels ou virtuels ?
un individu mal dans sa peau ?
pas si sûr .
l' histoire est passablement compliquée .
on y verrait bien Janus dans le rôle principal .
Pierrick Sorin est en tout cas un grand professionnel de l' image et de la magie , même en montrant comment il fait .