au quatrième trimestre vingt cent zéro , les fonctions de direction industrielle ( production , technique , recherche ) ont représenté onze pour cents des missions de recrutement des cabinets adhérents au Syntec .
une proportion en très légère augmentation par rapport au trimestre précédent ( dix pour cents ) .
la grande majoritÉ de ces recrutements est concentrée dans l' industrie ( quarante neuf missions , soit quatre vingt deux pour cents contre quarante cinq missions , soit soixante seize pour cents au troisième trimestre ) .
toutes fonctions confondues , ce secteur est d' ailleurs le principal recruteur avec trente neuf pour cents du total des missions , suivi de près par les services ( trente six pour cents ) .
cette TENDANCE favorable devrait se maintenir .
les entreprises indiquent ainsi vouloir continuer à accroitre leurs recrutements concernant la fonction " étude recherche projets " .
il faut y voir le signe d' un réveil de l' industrie après dix années de frilosité en matière d' emploi .