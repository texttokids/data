combien dénombre t on de Jacques Derrida ?
la réponse est incertaine .
quelques dizaines , au bas mot .
certains disent plus encore .
d' autres contestent la question même : il n' y aurait pas d' unité de compte , pas de mesure adéquate .
à son propos , faudrait il dire ce que Bertrand Russell affirmait des mathématiques : " on ne sait jamais de quoi on parle , ni si ce qu' on dit est vrai " ?
car ce qu' on croit concevoir bien , et pouvoir énoncer clairement , se trouverait toujours miné , transformé , multiplié .
par l' histoire , les textes et les contextes , l' inconscient .
entre autres .
essayons de préciser malgré tout , sans grande illusion .
parmi les Derrida visibles , on rappellera donc , par exemple , un jeune juif d' Algérie qui se vit privé par la France de la nationalité française , quand le régime de Vichy abolit le décret Crémieux de mille huit cent soixante dix , un penseur mondialement connu , auquel ont déjà été consacrés des centaines de volumes dans les pays les plus divers , un objet de culte pour groupes et groupies , de la Corée à la Californie en passant par l' Afrique du Sud , un philosophe déconcertant , jouant sur une multitude de registres , rebelle aux classifications , habile à défaire les conventions , un intellectuel engagé , arrêté en Tchécoslovaquie au temps du communisme , soutenant Nelson Mandela au temps de sa prison , aujourd'hui Mumia Abu Jamal ou les luttes des sans papiers , un fondateur d' institutions se voulant différentes , comme le Collège international de philosophie ou le Parlement des écrivains , un styliste dont l' écriture mélange les genres , souhaitant transgresser les frontières entre théorie et fiction , poésie et philosophie , un amateur d' art , un professeur , une star , un défenseur du droit de tous à la philosophie , un homme qui lit , écrit , écoute , parle .
sans arrêt .
il faudrait aussi mentionner l' extrême diversité des passions et des jugements suscités par cet inclassable et par ses parcours .
homme généreux et fidèle pour ses amis , celui que ses admirateurs considèrent comme un grand esprit ouvreur d' avenir passe pour un imposteur maniéré et incohérent aux yeux de ses détracteurs les plus acharnés .
l' atmosphère parait rarement sereine autour de cette figure .
elle suscite engouements ou résistances , enthousiasmes ou rejets .
il est rare que l' on se représente Derrida rangé ou terni .
élitiste pour les uns , démagogue pour les autres , sublime pour certains , il suscite peu l' indifférence .
le plus souvent , c' est en saint ou en diable qu' on l' imagine , comme si les attitudes le concernant ne se départaient pas de quelque religiosité .
sans entrer dans ces dédales , peut être faut il en retenir , ce qui n' est pas une mauvaise voie d' accès à ses textes , qu' avec ce penseur rien n' est jamais simple .
en tout cas rien de ce qu' on croit d' emblée , naïvement , pouvoir être simple .
une question toujours en cache une autre , tout est plus compliqué qu' on ne pense .
toujours .
cela pourrait constituer une première approche , minimaliste , de ce geste nommé " déconstruction " auquel Jacques Derrida a définitivement attaché son nom .
sans doute est il périlleux de vouloir définir en trois lignes cette vaste tâche , en un sens interminable , qui n' est pas une philosophie , ni simplement une méthode .
assumons sans vergogne ce que Derrida appelle le " simplisme journalistique " .
la déconstruction consiste à interroger les présupposés des discours , des disciplines , des institutions .
non pas pour les détruire ou les dissoudre , ce qui serait impossible ou insensé , mais pour en défaire les évidences et peut être la pesanteur .
ainsi Luther , souligne Derrida , parlait il déjà de " destructio " du christianisme .
il s' agit de soulever les sédiments , de démonter ce qui s' est ossifié ou appesanti , non de tout mettre à bas .
la déconstruction de la métaphysique , et les multiples voies qu' elle peut emprunter , serait donc une manière de rendre à la pensée ( discours , disciplines , institutions ) du jeu , du mouvement , voire un avenir .
cela vaut , au premier chef , pour l' Université , professe Jacques Derrida dans L' Université sans condition , conférence initialement prononcée en anglais à Stanford en mille neuf cent quatre vingt dix huit .
l' Université est à ses yeux , et par excellence , le lieu où doit s' exercer " une liberté inconditionnelle de questionnement et de proposition " , où doit être garanti le droit de tout dire et de tout publier .
cette capacité d' examen critique ne doit rien laisser à l' abri , " pas même la figure actuelle et déterminée de la démocratie " .
sans se préoccuper de ce que cette liberté inconditionnelle peut avoir , en certains cas , de liberticide , le philosophe dessine à grands traits la perspective où pourraient se développer les humanités de demain .
le programme de réflexion qu' esquisse Jacques Derrida pour ces humanités à venir devrait s' attacher notamment à " repenser le concept d' homme " , scruter les notions de démocratie et de souveraineté , analyser les idées de profession et de professorat .
la réflexion chemine cette fois encore sur une ligne où abondent les paradoxes .
si rien ne doit échapper à la critique , la souveraineté de l' Université doit elle aussi être mise en cause .
elle doit cependant être maintenue ...
pour que la critique se poursuive !
à force de vouloir ouvrir sans détruire , et déconstruire sans démolir , il n' y a presque plus moyen de distinguer ce qui est dedans de qui est dehors .
comble du paradoxe , cette conférence fort claire et bien conçue s' achève sur des doutes que l' on peut juger artificiels : " je ne sais pas si ce que je dis est intelligible , si cela fait sens . ( ... ) est ce de la philosophie , ou de la littérature , ou du théâtre ? " les dernières lignes sont traversées d' un souffle prophético énigmatique : " prenez votre temps , mais dépêchez vous de le faire , car vous ne savez pas ce qui vous attend . " du coup , évidemment , comme disait Karl Loewith sortant du cours de Heidegger , on se sent résolu , mais on ne sait pas à quoi .
c' est probablement plus compliqué , comme toujours .
mais les non initiés sont bien en peine de savoir à quelle hypothèse se vouer .
papier machine est d' un accès plus aisé , dans l' ensemble .
ce recueil groupe des analyses consacrées à l' avenir du livre , à l' informatisation du travail d' écriture , ainsi que des articles ou entretiens parus dans la presse française et internationale sur des thèmes divers , les uns politiques , les autres philosophiques et littéraires .
dans ces pages , on peut négliger les relations ambivalentes de Jacques Derrida avec la presse , qui n' ont rien d' original .
on s' attardera en revanche sur les jeux éminemment paradoxaux du livre et de l' ordinateur , qui modifient les places du papier et de la machine .
plus d' une fois , le penseur , qui n' a cessé de réfléchir aux questions liées à l' écriture , à la trace , à la rature , au palimpseste , éclaire de manière singulière l' arrivée des écrans dans l' espace du livre .
il analyse comment le livre se disperse , éclate dans le multiple sans pour autant disparaitre , et comment le traitement de texte bouleverse en secret l' attitude de celui qui écrit .
sans doute arrive t il à Jacques Derrida , ici comme souvent , de se laisser emporter par la lettre , s' accrochant à quelque tournure de langue pour en tirer des conséquences qui étonnent .
ainsi , le fait que l' informatique paraisse supprimer le papier le conduit à faire " résonner , sur plus d' un registre , littéral et figural , la question du " sans papier " " ...
reste cette évidence : une cinquantaine de volumes publiés en trente cinq ans , des cours et séminaires dans le monde entier , des lectures et des réécritures des oeuvres de Hegel , de Nietzsche , de Marx , de Freud , mais aussi de Ponge , de Blanchot , d' Artaud et de tant d' autres , un changement de regard et d' attitude marqué par la déconstruction , l' analyse de la différence , la critique de la métaphysique de la présence , des travaux sur la différence des sexes , l' amitié , l' hospitalité , le cosmopolitisme , le droit à la philosophie ( liste non limitative ) attestent que , pour Derrida , le paradoxe est fécond .