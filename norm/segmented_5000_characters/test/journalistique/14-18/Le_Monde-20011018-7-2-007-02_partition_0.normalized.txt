le remplacement des procédures d' extradition par un mandat d' arrêt européen unique prévoyant un mécanisme de transfert automatique a été à nouveau examiné , mardi seize octobre , à Luxembourg , par les ministres de la justice et de l' intérieur des Quinze .
nombre d' experts avaient suggéré qu' il serait plus facile de trouver un accord en décembre si le champ d' application très large prévu par la Commission était limité .
la présidence belge et la délégation française ont fait en ce sens deux propositions de compromis fort similaires .
la ministre de la justice française , Marylise Lebranchu , a proposé un système mixte , qui supprimerait la double incrimination pour une liste positive de treize infractions seulement , et la maintiendrait dans les autres cas .
il s' agit d' infractions déjà harmonisées au plan européen , comme la traite des êtres humains ou la contrefaçon de l' euro , et d' infractions graves n' ayant pas fait l' objet d' une harmonisation européenne , mais qui sont punissables dans tous les États membres de l' Union européenne , comme le meurtre ou le viol .
( Corresp .
