le représentant des Nations unies en Afghanistan , Fransesc Vendrell , a transmis , vendredi deux mars , aux talibans à Islamabad l' offre faite par le Metropolitan Museum de New York de conserver les statues pour les sauver de la destruction , a déclaré l' ONU à New York .
il devait aussi faire part de cette proposition au chef du régime militaire pakistanais Pervez Moucharraf .
le directeur du Metropolitan Museum , Philippe de Montebello , a proposé d' envoyer des experts en Afghanistan et d' emporter les pièces transportables pour les conserver dans le célèbre musée new yorkais .
Monsieur de Montebello a appelé le secrétaire général de l' ONU , Kofi Annan , qui a ensuite demandé à son représentant spécial de présenter cette offre aux talibans et aux dirigeants pakistanais , a précisé le porte parole de l' ONU .
( AFP .
