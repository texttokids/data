à quoi ressemble la vie après la prison ?
voilà quatre mois que Samy est sorti de prison .
" ça change tout . être assis à table avec mes parents , je préfère ça que d' être dans une cellule " , affirme cet homme de vingt six ans , qui a passé neuf années derrière les barreaux .
en sortant , " j' étais un peu perdu dans les papiers " à remplir , confie t il .
il a dû refaire sa carte d' identité , son permis de conduire , se créer une boite mail ...
il était incarcéré depuis ses dix neuf ans , alors tout à coup , devoir s' occuper de toutes ces choses qu' il ne connaissait pas bien , ça lui a semblé compliqué .
heureusement , il a été aidé par une association qui accompagne les anciens détenus , Wake Up Café .
rapidement , Samy a cherché un travail .
il avait trouvé une offre de laveur de carreaux , mais l' entreprise lui a demandé s' il avait déjà été condamné .
" quand ils ont vu pourquoi j' étais en prison , ils ont refusé de me prendre , raconte t il . je comprends . " Samy a été condamné pour avoir tenté de tuer quelqu' un et pour avoir fait du trafic de drogue .
souvent , les anciens détenus ont du mal à trouver un travail .
les entreprises n' ont pas très envie de leur faire confiance et puis , s' ils ont passé plusieurs années en prison , ils manquent d' expérience .
" ceux qui arrivent à s' en sortir , sont ceux qui ont une famille dehors qui s' occupe de leur chercher un travail , leur envoie de l' argent pendant la détention . ils auront un endroit où aller quand ils sortent . ça ne concerne qu' un petit nombre de détenus " , regrette François Bès , qui s' occupe du pôle enquête de l' Observatoire international des prisons .
c' était le cas de Samy qui a notamment été aidé par ses frères .
puisque , souvent , il y a beaucoup trop de détenus dans les prisons par rapport au nombre de places disponibles , les personnes dont le métier est de les aider lors de leur libération ne peuvent pas être là pour tout le monde .
résultat , plus de six anciens détenus sur dix retournent en prison dans les cinq ans après en être sortis .
c' est ce qu' on appelle la récidive : ils refont quelque chose d' illégal .
c' est notamment le cas pour les personnes qui vendaient de la drogue , parce que ça rapporte beaucoup d' argent .
" ils ont de fortes chances de replonger parce que ce sont les seules choses qu' ils savent ou peuvent faire " , constate François Bès .
" je veux pas retourner en prison , je préfère travailler " , assure Samy .
le jeune homme s' est inscrit à une formation pour devenir ambulancier .
son objectif est de créer sa propre entreprise d' ambulance .
" ça sera peut être dans dix ans " , dit il , mais il n' est pas pressé .