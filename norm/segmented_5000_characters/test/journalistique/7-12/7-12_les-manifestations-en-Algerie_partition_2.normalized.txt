pourquoi la jeunesse est elle très présente dans ce mouvement ?
dans les manifestations , il y a des hommes et des femmes , riches ou pauvres , de tous les âges et notamment beaucoup de jeunes .
l' Algérie compte quarante deux millions d' habitants et près de la moitié de la population a moins de vingt cinq ans .
les jeunes de vingt ans n' ont connu qu' Abdelaziz Bouteflika au pouvoir puisqu' il est le président du pays depuis leur naissance .
" c' est une jeunesse qui est hyperconnectée par rapport à un régime politique qui est totalement déconnecté des réalités du monde " , dit Kader Abderrahim , chercheur à Sciences Po , spécialiste de l' Afrique du Nord .
c' est d' ailleurs grâce aux réseaux sociaux que le mouvement contre le président a pris autant d' ampleur .
les jeunes se sentent enfermés dans leur pays et Internet leur offre une petite fenêtre sur le monde .
" avant , les algériens regardaient énormément la télévision qui est quand même très contrôlée par l' Etat . aujourd'hui , ils sont énormément sur les réseaux sociaux où il y a une très grande liberté " , ajoute Pierre Vermeren , professeur à la Sorbonne et spécialiste du Maghreb .
pour ces jeunes , Internet est aussi un lieu de drague dans un pays où les activités sont souvent séparées entre hommes et femmes : " la majorité des filles ne sortent pas à la nuit tombée et ne vont pas dans les bars , c' est très difficile de retrouver son petit copain ou sa petite copine , il faut se cacher . ce n' est pas très épanouissant . le seul lieu où les jeunes se sentent très libres c' est Internet et les stades de foot . c' est un lieu où les jeunes viennent se défouler plus que pour supporter leur équipe " , remarque Kader Abderrahim .
les jeunes ont le sentiment de vivre dans un pays qui ne leur offre pas d' avenir .
le chômage est très élevé et les touche particulièrement , même ceux qui ont fait des études .
ils veulent donc plus de travail , de liberté et de justice .