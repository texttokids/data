pourquoi les ministres veulent ils tous faire des réformes ?
en choisissant de changer le rythme des enfants à l' école , le ministre de l' Education nationale a fait ce qu' on appelle une réforme .
une réforme , c' est " une grande modification dans quelque chose qui est déjà mis en place " , explique Blanche Lochmann , spécialiste de l' éducation et de la formation des professeurs .
il peut y avoir des réformes dans différents domaines , comme le travail , la famille ou la santé .
beaucoup ont été faites à l' école .
elles ont lieu " environ tous les trois ou quatre ans " , précise Blanche Lochmann .
c' est un temps plutôt court , donc on ne sait pas si la réforme d' avant a vraiment été efficace .
s' il y a autant de réformes dans l' éducation , c' est parce que " chaque ministre a son idée de comment doit fonctionner l' école et comment doivent travailler les professeurs et les élèves . ils ont envie de mettre en place l' école de leurs rêves en quelque sorte " , affirme Blanche Lochmann .
les ministres n' ont peut être pas tous la même vision mais ils veulent la même chose : que l' école soit meilleure et que les élèves progressent .
mais en faisant des réformes , ils veulent aussi laisser leur nom dans l' histoire afin qu' on retienne que ce sont eux qui ont effectué des changements .
maintenant , les ministres sont encore plus poussés à faire des réformes car il y a beaucoup de communication autour d' eux .
sur les réseaux sociaux , comme Twitter ou Facebook , il faut qu' on voie que le ministre veut changer les choses parce que " c' est la preuve qu' il est en train de travailler . si le ministre ne fait rien , les gens vont se dire : mais où est il ? que fait il ? " raconte Blanche Lochmann .
le ministre de l' Education nationale est l' un des ministres qui a le plus de pression car les décisions qu' il prend vont avoir des conséquences sur la vie de beaucoup de personnes : les élèves , les professeurs , les personnes qui travaillent à l' école ...
et puis , l' école est un sujet très important puisque c' est là que sont formés les futurs citoyens .
" ils sont l' avenir de la France , il faut qu' ils apprennent beaucoup de choses " , note Blanche Lochmann .