ressources
chez moi , on a des solutions pour le climat !
de Philippe Godard , illustré par Guillaume Kashima ( Albin Michel , deux mille quinze , quinze euros ) .
dès huit ans .
ça chauffe ?
la bonne nouvelle , c' est qu' il y a une foule de solutions !
ici , ce sont les enfants eux mêmes qui en parlent .
au Bangladesh , Siraj raconte comment la mangrove protège des cyclones .
en France , Louise cultive son jardin et Manon troque tout un tas de trucs .
Helga l' Islandaise parle de géothermie , Xiaolin la chinoise d' énergie éolienne .
Horacio le Brésilien nous présente sa ville modèle , Curitiba .
au Mali , Dieyanaba attend un four solaire " magique " , qui libère les filles de la corvée de bois et leur permet d' aller à l' école ...
enthousiasmant , concret et bien documenté .
à nous l' écologie de Gilles Halais , illustré par Jacques Azam ( Milan , deux mille quinze , huit , quatre vingt dix euros ) .
dès huit ans .
c' est quoi un écologiste ?
c' est quoi les OGM ?
d' où vient la pollution de l' air ?
c' est quoi le changement climatique ?
que s' est il passé lors de la tempête Xynthia ?
pourquoi avons nous besoin des abeilles ?
comment se déplacer sans polluer ?
toutes les questions ou presque que petits et grands se posent , présentées avec clarté à l' aide de malicieuses petites vignettes .
sur quelle planète bleue ai je atterri ?
de Anna Alter avec Hervé Le Treut , illustré par Lucie Maillot ( Éditions Le Pommier , deux mille quinze , treize , quatre vingt dix euros ) .
dès sept ans .
comprendre comment fonctionne le climat n' est pas chose aisée .
océans , atmosphère , nuages , continents , hommes , qui agit sur son évolution et comment ?
que sait on ?
qu' ignore t on encore ?
alors qu' on entend beaucoup de bêtises sur le sujet , le ( grand ) climatologue Hervé Le Treut fait le point .
grimper sur ses solides épaules , c' est explorer les couleurs du ciel , démonter la mécanique de notre planète bleue comme une orange , s' aventurer dans son histoire brulante et glaçante , prendre sa température et réaliser qu' il faut à tout prix soigner son " petit début de fièvre " .
à mettre entre toutes les mains , de sept à soixante dix sept ans .
protégeons la planète !
de Jean Michel Billioud , illustré par Didier Balicevic ( Nathan , deux mille quinze , onze , quatre vingt quinze euros ) .
dès six ans .
les livres pop up de la collection Kididoc sont toujours ludiques et efficaces .
celui ci sensibilise les enfants à la préservation de l' environnement au sens large : océans , forêts , biodiversité , eau , climat ...
il y est aussi question d' économies d' énergie , d' énergies du futur et de tri des déchets .
l' eldorad' eau de Sandrine Dumas Roy , illustré par Jérôme Peyrat ( Les Éditions du Ricochet , deux mille treize , quatorze , soixante dix euros ) .
dès cinq ans .
ce très chouette album , aux textes et dessins pleins de finesse et d' humour , a déjà deux ans .
pourtant , il est d' une cruelle actualité , puisqu' il traite de la question des réfugiés climatiques .
en faisant réaliser que personne n' est a l' abri .
ici , le " migrant " est un pauvre cheval qui a dû quitter sa Normandie inondée par la montée des mers .
épuisé après un long et périlleux voyage , il arrive enfin en Tanzanie .
hélas , il est bloqué à la douane par des phacochères en uniforme de police , furibards : " dehors l' étranger , pas de vous chez nous ! " .
jusqu'à ce qu' un éléphant se souvienne de l' histoire de ce zèbre , qui au temps de la grande sécheresse , avait dû s' exiler dans un pays du Nord , où il avait lui aussi été persona non grata .
une histoire d' animaux bien proche de ce qui se passe parfois chez les hommes .
le climat à petits pas de Georges Feterman , illustré par Gilles Lerouvillois ( Actes Sud Junior , deux mille treize , douze , soixante dix euros ) .
dès neuf ans .
comprendre la différence entre météo et climat ( que bien des adultes n' ont toujours pas saisie ) .
apprendre à fabriquer un arc en ciel ou à reproduire l' effet de serre dans sa cuisine , avec deux gobelets et un saladier en verre .
observer les effets déjà concrets du changement climatique sur la nature .
savoir quels sont les risques pour demain et comment les éviter .
rigoler en regardant les dessins et terminer par un quiz .
complet et astucieux .
une sélection de Coralie Schaub