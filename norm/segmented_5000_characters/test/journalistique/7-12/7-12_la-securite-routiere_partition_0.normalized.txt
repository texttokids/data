numéro soixante quatre vingt neuf juin au cinq juillet vingt cent dix huit
la sécurité routière
dès dimanche , les voitures vont devoir rouler moins vite sur certaines routes en France .
le but : diminuer le nombre de personnes tuées dans des accidents .
cette nouvelle règle s' ajoute à d' autres qui doivent être respectées sur les routes pour que chacun soit en sécurité .
mais pourquoi limite t on la vitesse ?
comment protège t on les gens et à quoi doit on faire attention en voiture , à pied ou à vélo ?
prends la route avec moi !
Jordan , vingt quatre ans , a eu un grave accident de moto
Jordan avait deux ans et demi quand il a fait du ski pour la première fois .
très vite , ce sport est devenu une passion .
mais à l' âge de quinze ans , il a eu un grave accident alors qu' il conduisait une moto : " je ne me suis pas arrêté à un panneau stop , raconte t il . j' ai cogné une voiture et j' ai fait un vol de dix sept mètres . ma jambe s' est coincée entre la moto et la voiture . " les médecins ont dû couper la jambe gauche de Jordan en dessous du genou .
" quand j' ai appris à l' hôpital que j' avais été amputé , c' était un peu bizarre " , se souvient il .
" la première chose qui m' est venue à l' esprit , c' est de demander si je pourrais skier à nouveau un jour . " son médecin lui avait répondu qu' il pourrait skier sur ses deux jambes , grâce à une prothèse .
" ça m' a réconforté . grâce à ça , j' ai très vite accepté mon handicap . "
plus tard , il s' est inscrit dans un club de ski réservé aux sportifs handicapés , en équipant sa prothèse d' une chaussure adaptée .
puis il a rejoint l' équipe de France de ski paralympique et a participé aux Jeux paralympiques de PyeongChang , en Corée du Sud , en mars dernier .
" j' y ai passé des moments extraordinaires , magiques . je suis vraiment content d' avoir réalisé ce rêve . "
malheureusement , il n' a pas remporté de médaille .
mais il s' entraîne déjà pour en gagner une aux prochains JO , en vingt cent vingt deux .
" je n' aurais jamais pensé être un sportif de haut niveau sans mon accident . avec du recul , cet accident a changé beaucoup de choses dans ma vie . et je ne regrette pas du tout la voie que j' ai choisie " , assure Jordan .
Jordan habite à Caluire et Cuire ( Métropole de Lyon ) et s' entraîne au ski à Gruffy ( Haute Savoie )