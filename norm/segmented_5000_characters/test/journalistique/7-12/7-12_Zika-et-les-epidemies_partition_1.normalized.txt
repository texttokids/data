qu' est ce que le virus Zika ?
le virus Zika est transmis par des moustiques qui vivent dans les pays chauds .
il tient son nom de la forêt Zika , en Ouganda , un pays d' Afrique où il a été découvert en mille neuf cent quarante sept , sur un singe .
des humains l' ont ensuite attrapé .
Zika ne paraissait pas très inquiétant jusqu' ici , mais en mai dernier , il est arrivé au Brésil et s' est répandu très vite .
en quelques mois , un million et demi de personnes ont été contaminées dans ce pays , c' est à dire qu' elles ont attrapé le virus .
début mars , environ trente pays étaient touchés , surtout en Amérique latine ( les pays d' Amérique où on parle espagnol ou portugais ) .
des habitants des territoires d' outre mer ( la partie de la France qui n' est pas en Europe ) , comme la Guyane française et surtout la Martinique , ont aussi attrapé la maladie .
Zika est transmis par un moustique femelle qu' on reconnait à ses marques blanches sur les pattes .
quand ce moustique pique une personne atteinte par le virus , il s' infecte avec son sang et peut alors transmettre Zika à une autre personne qui n' est pas malade , en la piquant .
il se pourrait aussi que le virus se transmette en faisant l' amour .
une femme l' aurait attrapé de cette manière en France .
la plupart du temps , il n' y a pas de signes de la maladie , ce qui la rend difficile à détecter .
les symptômes , c' est à dire les effets , ressemblent à ceux de la grippe : fièvre , mal à la tête , courbatures , peau irritée ou yeux rouges .
ces effets mettent plusieurs jours à apparaître .
Zika peut aussi provoquer des problèmes plus graves .
certains malades ne peuvent plus bouger les bras ou les jambes , par exemple ( on appelle ça une paralysie ) .
par ailleurs , la mort de six personnes pourrait être liée au virus , mais on n' en est pas encore sûr .
les femmes enceintes doivent faire attention , car Zika peut toucher le bébé qui est dans leur ventre et empêcher sa tête et son cerveau de grossir .
on appelle ça la microcéphalie .
au Brésil , plus de trois bébés sont nés avec des têtes trop petites ces derniers mois , mais on n' a pas encore la confirmation que c' est à cause de Zika .
il n' existe pas encore de médicament ni de vaccin empêchant d' attraper le virus Zika , même si les scientifiques espèrent en trouver bientôt .
la seule solution , quand on a été piqué par un moustique infecté , est de prendre des médicaments pour avoir moins mal .
on déconseille d' aller en vacances dans les zones où Zika est présent , pour éviter que les touristes soient en contact avec des personnes malades sur place .
l' été prochain , les Jeux olympiques auront lieu au Brésil et on craint que les gens qui iront dans ce pays à cette occasion rapportent ensuite le virus chez eux .
le moustique qui transmet Zika ne vit pas en métropole ( la partie de la France qui est en Europe ) .
mais il en existe un autre de la même famille , le moustique tigre , qui dort l' hiver et pourrait transmettre ce virus quand il commencera à faire chaud .