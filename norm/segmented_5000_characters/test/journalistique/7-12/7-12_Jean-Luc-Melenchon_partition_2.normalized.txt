qui est Jean Luc Mélenchon ?
on entend beaucoup parler de lui depuis quelques mois parce qu' il s' est présenté à la dernière élection présidentielle .
il est arrivé quatrième , derrière Emmanuel Macron ( le Président actuel ) , Marine Le Pen et François Fillon .
il a été très déçu parce qu' il pensait pouvoir gagner .
au mois de juin , il a été élu député .
il fait donc partie des gens chargés de voter les lois .
résultats du premier tour de l' élection présidentielle
cet homme de soixante six ans fait de la politique depuis plus de quarante ans .
il est de gauche .
" son idée est que les vrais combats , les vraies luttes , sont au côté des gens " , explique le politologue Bruno Cautrès .
en dix neuf cent quatre vingt six , il est devenu le plus jeune sénateur de France : il avait trente cinq ans .
au début des années vingt cent zéro , il a été ministre de l' Enseignement professionnel .
il s' était déjà présenté à l' élection présidentielle en vingt cent douze et était arrivé quatrième , mais il avait obtenu beaucoup moins de votes que cette année .
pendant plus de trente ans , Jean Luc Mélenchon a été membre du Parti socialiste deux noeuds , le parti de l' ancien président de la République François Hollande .
mais , en vingt cent huit , il a décidé de le quitter parce qu' il n' était plus d' accord avec les idées défendues par le PS .
il a alors créé son propre parti : le Parti de gauche .
il en est toujours membre mais , début vingt cent seize , il a aussi créé La France insoumise .
" il a un parcours qui s' inscrit dans le modèle classique de la vie politique française et en même temps il a claqué la porte d' un parti , il a fondé une organisation et il a réussi à ce qu' elle soit , en quelques années , très forte . il a fait quelque chose d' assez spectaculaire dans la vie politique " , constate Bruno Cautrès .
jean luc Mélenchon a perdu l' élection présidentielle , mais il ne baisse pas les bras pour autant .
il n' aime pas les idées et les décisions du Président , Emmanuel Macron , et il veut réussir à réunir les gens qui s' opposent à lui .
pour cela , il organise notamment des manifestations .
son objectif est que de nombreux membres de La France insoumise soient élus lors des prochaines élections , les européennes , en vingt cent dix neuf , pour défendre les idées du mouvement .