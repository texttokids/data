numéro cinquante six quatre au dix mai vingt cent dix huit
le premier anniversaire du président à l' Elysée
il y a un an , le sept mai vingt cent dix sept, Emmanuel Macron devenait le nouveau président de la France , ou chef de l' Etat .
il avait trente neuf ans et devenait le plus jeune président de la République française .
que s' est il passé en un an ?
qui l' entoure dans son travail ?
que pensent les français de lui ?
dans ce nouveau numéro , je t' emmène à l' Elysée , où travaille le Président .
louise , vingt ans , a voté pour la première fois l' an dernier
louise a vingt ans et a voté pour la première fois l' an dernier lors de l' élection présidentielle .
c' était un grand moment pour cette étudiante à la faculté de Sciences politiques de Lille : " j' étudiais la politique depuis deux ans , sans pouvoir voter . ça représentait donc quelque chose pour moi qui suis ça depuis que je suis toute petite . " d' autant plus que c' était pour élire le Président , " qui a un rôle super important en France " , explique t elle .
lorsqu' elle a mis son bulletin dans l' urne , elle était sure de son choix .
mais elle a beaucoup hésité avant et a tout fait pour mieux comprendre les programmes des candidats à la présidentielle .
elle a par exemple lu leurs livres et est allée à quatre meetings de candidats très différents : " j' avais besoin d' écouter tout le monde pour tout comprendre et être sure de mon choix . "
louise a beaucoup aimé débattre avec ses amis pendant l' élection et en garde un très bon souvenir : " c' était super intéressant , on avait tous des avis différents . on regardait les débats entre les candidats à la télé et c' était très animé . " louise aime les débats d' idées parce qu' elle pense que c' est " la base de la démocratie " .
alors elle échange avec tout le monde , même ceux qui n' ont pas les mêmes opinions politiques qu' elle : " je ne veux pas rester dans ma bulle avec mes opinions . écouter les autres permet de comprendre pourquoi les gens votent pour telle ou telle personne . "
louise ne s' est jamais engagée en politique mais elle le fera peut être un jour " si je trouve un candidat qui me va . "
Lille , la ville où étudie Louise