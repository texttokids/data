quel est le rythme du corps humain ?
chaque fois qu' un ministre de l' Education nationale décide de changer le nombre de jours que les élèves devront passer à l' école , les adultes s' interrogent : qu' est ce qui est le mieux pour les enfants ?
selon les scientifiques spécialistes du sujet , quatre jours et demi c' est mieux que quatre jours .
et il vaut mieux aller à l' école le samedi matin que le mercredi matin .
pour savoir ce genre de choses , les scientifiques étudient le rythme du corps humain .
il est différent selon les âges .
par exemple , quand on est petit , on fait la sieste l' après midi , puis on arrête en grandissant .
les personnes âgées , elles , se lèvent généralement tôt le matin , sans avoir besoin de réveil .
" plus on est jeune , plus on dort " , résume François Testu , président de l' Observatoire des rythmes et des temps de vie des enfants et des jeunes .
" dans la journée , il y a des temps forts et des temps faibles pour apprendre " , explique t il .
chez les enfants , c' est le matin qu' on arrive le mieux à apprendre .
plus précisément durant la deuxième moitié de la matinée , avant le déjeuner .
puisqu' on est plus attentif vers dix heures ou onze heures , c' est là qu' il vaut mieux apprendre de nouvelles choses , conseille François Testu .
par exemple , le nom des couleurs en anglais .
l' après midi , quand on a un peu plus de mal à se concentrer , est un bon moment pour utiliser des connaissances qu' on a déjà , en faisant des exercices .
par exemple en décrivant , en anglais , les couleurs de nos habits .
les rythmes du corps humain ne s' observent pas uniquement sur une journée , mais sur toute l' année scolaire .
" il faut absolument préserver le " sept plus deux secondes : sept semaines de période scolaire et deux semaines de vacances " , affirme François Testu .
sinon , ça devient compliqué d' avoir de l' énergie et d' apprendre .
" ce qui serait idéal , c' est que la vie soit la plus régulière possible " , poursuit le spécialiste .
ça permet au corps et à l' esprit de rester bien en forme .
c' est pour ça qu' il est important de se coucher tous les soirs à la même heure , même si ça semble embêtant .
la preuve : à la fin des grandes vacances , on est un peu détraqué parce qu' on s' est couché plus tard que d' habitude et on a du mal à reprendre le rythme de l' école .