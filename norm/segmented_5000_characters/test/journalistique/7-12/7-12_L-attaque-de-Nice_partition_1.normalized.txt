pourquoi le conducteur a t il fait ça ?
pour l' instant , on ne sait pas pourquoi cet homme a foncé sur les gens .
des policiers sont en train d' enquêter pour savoir exactement qui il était , s' il a agi tout seul et quelles étaient ses motivations .
on sait qu' il s' agissait d' un homme de trente et un ans , né en Tunisie , qui habitait à Nice .
il était chauffeur livreur et avait été condamné au mois de mars parce qu' il avait été violent avec un automobiliste .
le président de la République , François Hollande , a affirmé que cette attaque était terroriste .
les terroristes sont des personnes violentes qui font du mal aux gens et espèrent leur faire peur pour imposer leur vision du monde .
malheureusement , il y a des attaques terroristes dans plusieurs pays du monde .
parfois c' est parce que des personnes veulent posséder un morceau de pays déjà occupé par d' autres gens , d' autres fois c' est parce qu' elles ne supportent pas que les gens ne pensent pas comme elles et veulent leur imposer leur vision de la religion ou de la société .
une organisation qui s' appelle l' Etat islamique ( ce n' est pas un pays , mais un groupe de terroristes ) a revendiqué l' attaque du quatorze Juillet : elle a écrit un texte disant que le conducteur du camion avait suivi ses appels à tuer des gens .
en France , il y a déjà eu plusieurs attaques terroristes , notamment l' an dernier .
en janvier vingt cent quinze, des hommes ont tué dix sept personnes à Paris , dont des employés du journal Charlie Hebdo et des gens qui faisaient leurs courses dans un magasin Hyper Cacher ( qui vend de la nourriture respectant les règles de la religion juive ) .
puis en novembre , d' autres hommes ont tué cent trente personnes qui se trouvaient à un concert et à des terrasses de cafés et de restaurants , à Paris et dans une ville à côté , Saint Denis .