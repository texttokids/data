numéro cent huit trente et un mai au six juin deux mille dix neuf
la prison
près de soixante douze personnes sont en prison en France .
pourtant , il n' y a pas assez de place pour tout le monde .
alors dans certains établissements , il y a deux fois plus de prisonniers que de lits .
à quoi sert la prison exactement ?
à quoi ressemble la vie à l' intérieur ?
et que se passe t il quand on en sort ?
cette semaine , je t' explique tout sur ce monde très fermé .
la mère de Mathis , quinze ans , est allée en prison
il y a quatre ans , la mère de Mathis est allée en prison .
il avait alors dix ans .
" c' est mon père qui m' a expliqué , je ne comprenais pas vraiment . pour moi , ce n' était que dans les films " , se souvient il .
sa mère a été condamnée par la justice parce qu' elle s' était fabriqué de faux documents d' identité et avait fait de faux chèques .
elle est restée trois mois en prison .
le premier soir , Mathis a attendu qu' elle rentre à la maison .
puis il s' est rendu compte qu' elle était vraiment absente .
" ça m' a fait bizarre , j' avais l' impression que ce n' était pas réel " , précise l' adolescent de quinze ans .
sa mère lui manquait beaucoup , surtout au moment d' aller se coucher .
il ressentait aussi son absence quand la famille regardait des films , car sa mère était d' habitude là .
chaque semaine , la famille allait lui rendre visite en prison , dans un lieu fait exprès pour ça , appelé un parloir .
Mathis a quatre frères et soeurs , mais seulement trois personnes avaient le droit de venir chaque semaine .
alors le garçon rendait visite à sa mère une semaine sur deux , avec son père et sa soeur .
ils passaient une heure ensemble , séparés par une sorte de muret .
avant d' entrer , " on devait retirer notre veste , notre gilet , nos chaussures " , détaille Mathis .
" je ne comprenais pas , j' étais tout petit . mon père m' a expliqué que c' était pour ne pas faire entrer d' objets interdits . "
lors des visites au parloir , " on parlait de tout , même de choses dont on ne parlait pas d' habitude , comme les programmes télé ! " s' amuse Mathis .
il lui parlait de ses notes à l' école , de ce qu' il faisait la semaine .
" elle nous demandait beaucoup ce qu' on faisait parce qu' elle s' inquiétait " , note t il .
depuis qu' elle est sortie de prison , Josina , la mère de Mathis , apprécie particulièrement ses relations avec ses enfants : " on est plus proches qu' avant . ils ne m' en veulent pas . "
Steenwerck , la ville où habite Mathis