il était une fois un vieux monsieur , une vieille dame , et un petit garçon .
un matin , la vieille dame fit du pain d' épices en forme de bonhomme .
elle ajouta du glaçage pour ses cheveux et ses vêtements , et des petites boules de pâte pour faire son nez et ses yeux .
lorsqu' elle le mit au four pour le cuire , elle dit au petit garçon : " tu vas surveiller le bonhomme en pain d' épices pendant que ton grand père et moi nous allons travailler dans le jardin " .
alors le vieux monsieur et la vieille dame sortirent et se mirent à cueillir des pommes de terre , ils laissèrent le petit garçon pour surveiller le four .
mais celui ci se mit à rêvasser .
soudain il entendit un bruit , et il leva la tête .
la porte du four s' ouvrit d' un coup et un bonhomme de pain d' épices sauta hors du four , et s' enroula jusqu'à la porte de la maison qui était ouverte .
le petit garçon courut pour fermer la porte , mais le bonhomme de pain d' épices était très rapide et roula en passant la porte , descendit les marches et continua sur le long du sentier bien avant que le petit garçon put l' attraper .
le petit garçon lui courut après aussi rapidement qu' il pût , criant à ses grands parents quin entendant le bruit , laissèrent leurs pelles et se mirent à courir aussi .
le bonhomme de pain d' épices les avait devancés d' un long chemin tous les trois et bientôt ils le perdirent de vue , tandis qu' ils devaient s' asseoir , à court de souffle , sur un banc pour se reposer .
et voilà que s' enfuit le bonhomme de pain d' épices .
sur le chemin il rencontra deux hommes qui creusaient un puits , ils levèrent la tête de leurs travaux et l' appelèrent : " où vas tu , bonhomme de pain d' épices ? " il répondit : " je me suis enfui d' un vieux monsieur , d' une vieille dame , et d' un petit garçon et je peux m' enfuir de vous aussi , tra la la ! " " tu crois que tu peux ? nous allons voir ça ! " dirent ils , et ils jetèrent leurs pioches et coururent après lui , mais ne purent l' attraper .
et bientôt ils durent s' asseoir au bord du chemin pour se reposer .
et ainsi s' enfuit le bonhomme de pain d' épices .
il courut et rencontra deux hommes qui creusaient un fossé .
" où vas tu , bonhomme de pain d' épices ? " dirent ils .
il dit : " je me suis enfui d' un vieux monsieur , d' une veille dame , d' un petit garçon , de deux hommes qui creusaient un puits , et je peux m' enfuir de vous aussi tra la la ! " " tu crois que tu peux ? nous allons voir ça ! " dirent ils , et eux aussi jetèrent leur bêches et se mirent à sa poursuite .
mais bientôt le bonhomme de pain d' épices les avait eux aussi devancé , et voyant qu' ils ne pouvaient l' attraper , ils laissèrent leur poursuite et s' assirent pour se reposer .
et voilà que s' enfuit le bonhomme de pain d' épices , et il rencontra un ours .
l' Ours dit : " où vas tu , bonhomme en pain d' épices ? " il dit : " je me suis enfui d' un vieux monsieur , d' une vieille dame , d' un petit garçon , de deux messieurs qui creusaient un puits , de deux messieurs qui creusaient un fossé , et je peux m' enfuir de toi aussi tra la la ! " " tu crois que tu peux ? " grogna l' ours .
" nous allons voir ça ! " il courut aussi vite que ses pattes lui permirent derrière le bonhomme de pain d' épices , qui ne s' arrêta même pas pour se retourner .
mais bientôt , l' ours était si loin qu' il se dit qu' il aurait dû arrêter la poursuite dès le début , alors il s' étira sur le bord du chemin pour se reposer .
et le bonhomme de pain d' épices s' en fut , et rencontra un loup .
le loup dit : " où vas tu , bonhomme en pain d' épices ? " il dit : " je me suis enfui d' un vieux monsieur , d' une vielle dame , d' un petit garçon , de deux messieurs qui creusaient un puit , de deux messieurs qui creusaient un fossé , d' un our , et je peux m' enfuir de toi aussi , tra la la ! " " tu crois que tu peux ? " hurla le loup .
" nous allons voir ça ! " et d' un bond il se mit à poursuivre le bonhomme de pain d' épices , qui s' enfuit très vite , si vite que loup comprit qu' il n' avait aucune chance de l' attraper , et lui aussi s' allongea pour se reposer .
et le bonhomme de pain d' épices s' en fut , et rencontra un renard qui se reposait tranquillement dans un coin de la barrière .
le renard l' appela d' une voix aigue , mais sans se lever : " où vas tu , bonhomme en pain d' épices ? " il répondit : " je me suis enfui d' un vieux monsieur , d' une veille dame , d' un petit garçon , de deux messieurs qui creusaient un puit , de deux messieurs qui creusaient un fossé , d' un ours , d' un loup , et je peux m' enfuir de toi aussi tra la la ! "
le renard dit : " je ne t' entends pas très bien , bonhomme en pain d' épices . peux tu te rapprocher un peu ? " tournant un peu sa tête d' un côté .
le bonhomme de pain d' épices s' arrêta dans sa course pour la première fois , s' approcha un peu , et appela d' un voix forte : " je me suis enfui d' un vieux monsieur , d' une veille dame , d' un petit garçon , de deux messieurs qui creusaient un puit , de deux messieurs qui creusaient un fossé , d' un our , d' un loup , et je peux m' enfuir de toi aussi tra la la ! " " je ne peux toujours pas t' entendre . peux tu te rapprocher un peu plus ? " dit le renard d' une voix faible , alors qu' il tendit son cou vers le bonhomme de pain d' épices et mit une patte derrière son oreille .
le bonhomme de pain d' épices s' approcha , et se penchant vers le renard , cria : " je Maître suis enfui d' un vieux monsieur , d' une Vieille dame , d' un PETIT GARÇON , De deux messieurs qui creusaient un puit , de Deux messieurs qui creusaient un fossÉ , D' un ours et d' un loup , et Je peux m' enfuir de toi aussi , tra LA LA ! " " tu crois que tu peux ? " cria le renard , et il en un clin d' oeil , il croqua le bonhomme de pain d' épices avec ses dents pointues .