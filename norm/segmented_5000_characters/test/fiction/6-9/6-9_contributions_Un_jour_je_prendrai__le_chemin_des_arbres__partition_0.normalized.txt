" il était une fois à La Romieu , mon petit village de la campagne gersoise , du temps où j' étais menuisier ébéniste ... " la voix usée tout à la fois douce et rocailleuse de Joseph , mon aïeul comme à l' accoutumé , commença ainsi le récit que j' ai eu l' envie de partager avec vous toutes et tous petits deux noeuds ou grands deux noeuds .
en ces instants aussi rares que précieux , tous mes sens étaient en éveil .
je retins mon souffle .
il commença : " l' arbre , c' est du bois chaud et de la sève au coeur .
les arbres me connaissent aussi bien que je les connais .
ma vie s' est déroulée en copeaux d' acajou , de charme , de sapin , de tilleul , de frêne , de châtaignier ou de chêne .
ma vie , c' est mon rabot , mon établi , mes gouges , mes chevilles , mes clous et mon bois .
toutes ces planches empilées là bas au fond sous mon appentis .
ma vie , c' est tenon et mortaise .
encore aujourd'hui , malgré mon grand âge , quand mon bois chante , je chante avec lui .
il se moque pas mal de ma voix éraillée , enrouée .
heureusement puisque c' est sa sciure qui me l' a cassé ma voix .
mon bois craque , il joue tout au fond de mon atelier , dans sa chaude poussière .
tiens , le vent va tourner , la pluie menace , ici l' humidité revient .
ces guetteurs , ces vieux dont je te parlais l' autre jour , surveillent les cieux et l' horizon lointain , les mains en visière , moi je regarde mon bois et je me dis père Joseph , tu vieillis , mais ton bois garde sa sève , lui .
il bouge , il te parle , il vit aussi sa vie , il se souvient .
eh oui , mon garçon !
le bois se souvient du vent , de la neige , de la pluie , du feu du soleil , du gel de l' hiver , des mauvaises intentions de certains hommes , comme du canif des enfants qui le marquent de deux coeurs entrelacés .
mais est ce encore de ce temps de marquer les troncs de deux coeurs entrelacés ?
crois moi mon p' tit , le bois se souvient du temps qui passe .
il nous dit des secrets que nous portions , sans le savoir , au fond du coeur .
ou des secrets que nous pensions avoir oublié .
cela nous aide à comprendre le sens de la vie .
cela nous rassure .
ah , mon garçon , j' en ai construit des lits , des tables , des armoires , des buffets , des berceaux , des vaisseliers , et des coffres , des étagères , des bancs et des tabourets .
tu sais de ces lits carrés , solides , bien calés dans un angle de la chambre .
de ces lits qui abritent au petit matin , la douce quiétude des enfants et même des grands , jamais rassasiés de caresses et de tendresse .
jamais désaltérés par tant d' embrassades .
tous ces ouvrages nés sous mes mains râpeuses , eux aussi , continuent de vivre .
même lorsque leurs usagers ont disparus , ils nous racontent leurs histoires , si nous savons y prêter l' oreille .
ils vous diront de ces hommes , de ces femmes , de leurs enfants et petits enfants , les petits secrets .
la couleur de leurs yeux et la saveur de leurs baisers .
ils vous diront tous leurs défauts , leurs joies , leurs bonnes ou mauvaises habitudes , leurs disputes , et leurs réconciliations .
leurs rires et leurs larmes .
en ce temps là mon petit , poursuivit Joseph , ma scie ronronnait , mon rabot faisait jaillir les copeaux .
petites spirales brunes ou blondes , tels des frisottis de jeunes enfants , tombant sur mes sabots .
eh !

tu es incrédule mais , il n' y a pas si longtemps , on portait encore des sabots !
les copeaux , spirales brunes ou blondes , comme une neige aux odeurs de septembre ou une Pâques aux senteurs de fagots .
l' odeur du bois , au début quand on n' est qu' un grouillot , un arpète , c' est comme l' ostensoir pour l' enfant de choeur distrait .
une sorte de saint Sacrement que l' on porte en soi presque sans y penser .
puis les années passent cela devient du sérieux , comme une deuxième naissance comme l' hésitation des petits pas chancelants d' un nourrisson l' émoi des premiers mots amoureux .

je fermais un peu les yeux .
je respirais .
je humais .
j' hésitais .
je cherchais dans la mémoire de mon nez .
je comparais , la mémoire de mes odeurs .
" comprends tu mon garçon ? " le silence seul répondit en acquiescement au conteur .
je n' ai pas osé lui dire quoi que ce soit .
je buvais ses paroles , comme on boit à une source pure et fraiche de la montagne .
après cette seconde de pause , légère comme une plume , la voix un peu blanche repris : " tout ce que je viens de te dire n' est qu' instinctif . c' est le bois qui est le maître enfin , le premier maître en second il y a eu le compagnon du devoir qui m' a pris à ses côtés sous son aile . ce Maître compagnon était tout à la fois bienveillant et terriblement exigeant . au début le bois ne m' a rien laissé découvrir de lui . ni de ses origines , ni de quel arbre il provenait , ni de quelle terre celui ci a été arraché . le bois ne m' a raconté aucun de ses secrets . devant lui je suis incliné et , patiemment , j' attendais que le maître me dise les bons gestes à effectuer . timidement , un jour , de peur de me tromper , car moi je ne sais pas , je me hasarde : " c' est du noyer , non ...
c' est du sapin !

oh oui !

au début , je me suis trompé souvent et j' en ai en rougi .
chaque fois le compagnon me reprenait , un sourire indulgent au coin de l' oeil .
l' exigence et la bienveillance incarnée en un seul homme .
c' est seulement ainsi que les Humains progressent , lentement sur les chemins escarpés de la sagesse , me disait il .
j' ai eu honte , tellement honte .
le bois , comprends tu , p' tit ne dis pas son nom .
le bois est vivant et fier , comme les Hommes .
le bois est noble .
au moins aussi vivant , fier et noble que la pierre .
lui aussi sait résister à la main .
il ne se commet pas avec n' importe quel premier venu .
il n' étincelle pas sous le premier vernis .
il ne fait pas semblant .
il n' est pas là pour briller , éphémère .
il est là , lui aussi pour durer .
il garde ses tabous , ses distances .
il se mérite le bois .
il s' apprivoise peu à peu , juste au bout des doigts .
il se laisse flatter du regard , du plat de la main , dans une lente et délicate caresse , longtemps .
il est plein de senteurs .
tel une fiancée , le bois se fait désirer .
quelquefois , il s' irrite , met des échardes aux paumes des mains de l' apprenti malhabile , que j' étais .
il est plein de reflets , de moisissures , de blondes veines de vagues d' écume enfermées .
des airs de chevelures féminines captives .
le bois te raconte la forêt , les chasseurs , le galop sourd et buté du sanglier qui charge hure baissée , le renard dont tu retrouves la couleur de la queue , à la surface d' une planche bien plane , là juste celle que tu viens de débiter .
imagine la queue d' un renard sous tes coudes appuyés à la table familiale .
il te fait entendre les milliers de chants d' oiseaux venus s' abriter dans sa ramure , du temps béni où il était encore fièrement planté au milieu de ses frères .
si tu prêtes une oreille attentive , tu entendras même leurs parades nuptiales .

après une courte pause , mon grand père reprit : " le bois mon garçon , le bois vient aussi bien du chaud que du froid .
le bois garde les souvenirs en lui .
les nôtres , ceux des vivants et ceux des morts .
ceux de toute la nature qui les a entourés , lorsqu' ils étaient dressés , grands et forts .
les arbres nous gardent , nous protègent , fiers , patients , fidèles et droits .
un jour , il y a très longtemps , entre eux et nous l' amour s' est gâté .
une sorte de divorce , un début d' injustice jamais réparé , une guerre qui n' a jamais cessé .
une cause de souffrance .
les arbres , eux , ont tout leur temps .
pas nous .
on s' agite , on court sans cesse .
sans jamais trop savoir , ni pourquoi , ni vers où .
une fuite en avant , éperdue et désespérée .
puis un jour , sonne l' heure de notre départ .
alors ils nous attendent .
toute notre vie , on s' impatiente .
les arbres , eux , patientent .
finalement , il nous faut , heureusement , bon gré , mal gré , quitter ce monde .
mais pour nous venger de ce départ auquel on ne peut se soustraire , cet embarquement au dernier quai du dernier port de la vie , on les amène , sots que nous sommes , avec nous en terre .
dernières marques de la vanité .
l' arbre est là tout autour de nous et ne dit jamais rien .
si les arbres pleuraient , appelaient au secours , nous traitaient d' assassins .
s' ils gémissaient un jour comme nous pouvons gémir sans cesse .
essaye d' imaginer , mon grand , le vacarme assourdissant que cela ferait .
un jour , mes copeaux retrouveront leur sève .
la sève , son arbre et l' arbre , sa forêt .
un jour , tous les lits , toutes les tables , tous les buffets , toutes les armoires , tous les berceaux que j' aimais pour les avoir vu naitre de mes mains , refleuriront en terre sauvage .
ce jour là , mes outils mon rabot , ma varlope , mes gouges , mes râpes , mon trusquin , mon pointeau refleuriront aussi " .
Joseph mon tout premier maître , fit une pause , comme pour reprendre son souffle , un peu fatigué .
" alors je te le dis mon garçon , j' accrocherai au clou la clé de mon atelier . je prendrai mon bissac , chausserai mes souliers ferrés . je suivrai le tremble , le chêne , l' orme , le frêne , le noyer et le sapin qui m' ouvriront tous la route . comme l' arbre va son chemin vers le ciel , les racines bien en terre , le faîte au vent . ce vent si grisant et si puissant que l' on nomme chez moi : l' autan . je prendrai moi aussi , enfin , " le chemin des arbres " , sans trembler . pour qu' ils m' expliquent , ainsi , chacun à leur manière , la véritable nature des Hommes . "
cette histoire je vous l' offre .
elle constitue la dernière chimère que j' ai écrite pour vous .
je vous la confie pour que vous en fassiez bon usage .
et comme on dit chez moi : " clic clac moùn counte ès aquabat ! ! ! Buon soun ! ! ! " ...
" clic clac mon conte est achevé ! ! ! bon sommeil ! ! ! " .