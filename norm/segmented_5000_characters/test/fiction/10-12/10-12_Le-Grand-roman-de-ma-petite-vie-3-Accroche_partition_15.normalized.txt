mademoiselle Laurent a réservé la cuisine de la cantine après l' école .
toute la classe est venue sous la pression de la prof , un peu trop à mon gout .
elle lit mon essai en disant qu' il lui a inspiré de former ce club et puis elle me donne la parole .
comme nous tous , je regarde ce que l' on m' envoie : des petits films sur youtube ou autre .
je suis une adepte des TED talks et je suis tombée sur Jamie Oliver , le chef anglais et le révolutionnaire de la cuisine .
il montre des films des enfants obèses et fait appel au bon sens du public pour essayer de les convaincre de cuisiner et de manger sainement pour échapper aux conséquences de la malbouffe .
il s' est engagé dans sa campagne en visitant les écoles et les familles .
essayez de l' écouter .
c' est lui qui donne le titre de notre club .
je sors de mon sac les salades que j' ai achetés et le citron et l' huile d' olive .
mais Bonnie , dit le perfide Carl , l' avocat du diable , on sait faire une salade en France .
c' est vrai que nous sommes mieux lotis .
Jamie Oliver montre des légumes à une classe du CP en Amérique et il n' y a pas un seul enfant qui peut identifier une aubergine , un poireau , un oignon ou même une pomme de terre .
les enfants pensent que les frites poussent dans la terre !
je voulais commencer par la plus simple pour vous dire qu' en cinq ou dix minutes on peut mettre la santé sur nos tables .
je vous demanderai Carl de suivre ces instructions faciles .
trop facile ?
tu vas couper le citron en deux et extraire le jus ( sans les pépins ) dans ce bocal à confiture .
tu ajoutes six cuillères à soupe d' huile d' olive , du sel et du poivre .
mets le couvercle et secoue le mélange .
tu peux gouter et corriger à ton palet .
Carl , cabotin , joue le rôle comme s' il était à la télé .
je prends les trois salades : la laitue , la laitue sucrine et le radicchio ( que j' ai lavé à la maison et les herbes ( basilic , persil et menthe ) et je les fourre dans le saladier .
je verse la moitié de la sauce et je mélange avec mes doigts .
je goute et j' ajoute le reste de la sauce .
puis j' invite chacun à déguster avec ses doigts .
les réactions sont élogieuses .
ce n' est qu' une simple salade et vous voyez qu' il n' y a même pas un tour de main .
vous allez pouvoir épater vos familles avec la salade en hors d' oeuvre ou après le plat .
on fera ainsi une dizaine de recettes de semaine en semaine et vous serez des pionniers dans vos familles pour une cuisine équilibrée .
bonne santé !
on reste à la cantine pour discuter et finalement Carl montre le film de Jamie Oliver sur son téléphone .
c' est mon fidèle assistant .
mademoiselle Laurent est contente .
Dorélie dit qu' elle va rentrer préparer sa salade puisque sa mère a renoncé à faire la cuisine .
Omar parle des repas au foyer qui n' a pas l' air folichons .
je pense que finalement ma vie avec Omama dans la cuisine a servi à quelque chose .
le club est lancé et qu' est ce que je ferai la semaine prochaine ?
on s' arrête au magasin bio pour acheter encore des salades .
Omama doit manger de la verdure .
décisions