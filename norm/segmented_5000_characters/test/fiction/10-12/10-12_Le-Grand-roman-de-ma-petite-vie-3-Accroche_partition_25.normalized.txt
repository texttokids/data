pourquoi tu as apporté du fromage ?
ce n' est pas prévu .
regarde quel fromage .
brillat savarin , je n' en ai jamais gouté .
c' est un , doux au palais , qui se mange jeune et frais .
il est fabriqué dans les régions de et de .
matière grasse ?
trente cinq pour cents de matière grasse , à partir de , c' est un fromage , d' un poids moyen de cinq cents grammes , qui se présente sous la forme d' un disque plat d' environ treize centimètres de diamètre et trois , cinq centimètres d' épaisseur .
tu as avalé Wikipédia ou quoi ?
je fais souvent ça , j' apprends un article par coeur et me voilà grand savant .
c' est juste une introduction au nom de notre école .
et je vois que tu as apporté son livre " la physiologie du gout " .
tu l' as lu ?
un peu ...
devant notre collège si ordinaire , si calme , si banal , il y a une foule , des camions de la télé , des journalistes comme s' il y avait un meurtre avant notre arrivée .
un homme que je reconnais parle au micro .
c' est Jamie Oliver .
il n' est pas venu seul !
mademoiselle Laurent vient vers moi .
" tu savais ? "
pas du tout .
l' Invitation était pour lui .
tout à fait intime .
il va falloir que j' aille parler avec le directeur .
il faut des autorisations , aie aie aie .
j' irai le prendre en charge avec Carl .
je vais vers la horde sauvage .
je me présente : " I am Bonnie Bonnet , I was the one who invited you . it is so nice of you to come speak to us . but in my head , it was a private little meeting . "
it grew !
is this a problem ?
The French administration may not allow this .
There is a state of emergency in France , Vigipirate , you understand ?
I' m sorry .
bien que le principal ne le connaisse pas , ni d' Eve , ni d' Adam , ce Jamie Oliver , il sort l' accueillir comme s' il était le roi de Siam .
il baragouine même quelques mots en anglais .
et il accepte la télé , la radio , et qui veut entrer dans son moulin .
il aurait pris des extraterrestres , des farfadets et des lutins s' ils étaient accompagnés de la télé .
Jamie Oliver est content que la cuisine soit occupée pour préparer le repas du midi .
il va de casserole en casserole .
il donne même des conseils aux cuistots .
les cameramen et women s' installent .
c' est toute une population voire une armée et ils vont mettre longtemps à prendre leurs marques .
et pendant ce temps Omama est à la chimio avec Josiane .
le directeur qui semble vouloir l' exclusivité de Jamie Oliver , envoie Carl et moi dans notre classe .
" il n' est pas encore prêt , on va lui laisser faire son travail . "
et nous on se tape les maths .
je vois que Carl apprend son discours sur Brillat Savarin et moi je tourne les pouces , je me gratte , je me peigne avec mes doigts , je triture ma montre Patek Philippe , je m' assois sur une fesse , puis l' autre , je tape des pieds , je regard à gauche et à droit , mais surtout pas le tableau .
la leçon est interminable et je pense à tout ce qui se passe dans la cuisine .
quand enfin nous nous y rendons , Jamie Oliver est en plein dans son film .
il dit qu' il est au collège Brillat Savarin à Paris où le déjeuner pour les élèves est en préparation .
il approuve le menu car il y a la salade , du poisson ( surgélé ) , des épinards ( surgelés ) .
" I even saw organic fresh fruit and vegetables . This school deserves to be called Brillat Savarin . "
ce que je pensais être une rencontre sympathique avec un Brillat Savarin moderne est devenu un voyage mégalomédiatique .
notre classe , selon les instructions de Mademoiselle Laurent , mange ensemble pour être filmé ensemble .
Jamie Oliver mange aussi et analyse le gout de chaque plat devant les caméras .
il trouve que nous sommes beaucoup plus sophistiqués que nos homologues américains .
merci !
le club va suivre le déjeuner .
on nettoie les tables et nous restons assis .
quand on a le feu vert , Carl se lève et récite son discours sur Brillat Savarin .
il est un orateur avec du charme et du savoir .
je traduis pour Jamie .
j' apprends qu' il ouvre un nouveau restaurant italien à Paris ( ainsi mon invitation lui tombe pile au service de sa pub ) .
ce qui n' empêche pas qu' il soit excessivement sympathique .
mademoiselle Laurent nous fait déménager dans la cuisine et lui passe les ingrédients pour son curry d' agneau .
on branche les téléphones , on débranche les cerveaux , ainsi on va apprendre du maître .
il commence par citer notre Brillat Savarin ( que je ne connaissais même pas il y a une semaine ) " You are what you eat ! "
il coupe , il fait dorer , il fait mijoter , ça sent bon , on vient de manger mais on a de nouveau faim , c' est de la magie .
pendant que ça cuit , il nous demande " if you are what you eat , what are you ? "
I' m chocolat !
dit Dorélie .
I' m pirogi !
je dis pensant à ma grand mère .
je suis frites !
je suis hamburger !
I' m ice cream , dit Carl qui est entièrement dévoué à la crème glacée .
je suis fraises tagada !
I' m chumous , dit Omar .
ainsi tout le monde annonce sa nourriture préférée .
Jamie montre sa consternation avec une expression de dégout .
il nous parle de l' obésité , du diabète , des artères , des maladies qui tuent , montre un de ses films sur son téléphone , parle des clés de la santé .
il réussit à convaincre .
le temps de sa plaidoirie et son curry est prêt .
il le met dans une assiette et nous passe des fourchettes .
inutile de dire qu' une bouchée ne suffit pas .
son attaché de presse nous distribue à chacun le nouveau livre de cuisine dédicacé par l' auteur Jamie Oliver .
c' est vraiment Noël et nous sommes tous ravis .
avant de dire au revoir , il me prend à côté pour me dire combien il apprécie mon invitation .
le film de la rencontre sera sur son site web et aussi au journal télévision à vingt heures .
il me tend aussi deux invitations pour l' ouverture de son restaurant .
j' irai avec Omama .
il part avec son armée .
mademoiselle Laurent est en tête à tête avec le principal .
je pars aussi avec Carl et Dorélie .
Omar nous rattrape .
je peux rentrer avec toi ?
demande t il à Dorélie .
on y va , répond elle .
on se sépare d' eux et Carl revient chez moi .