Omama ne se sentait pas d' attaque pour aller au mariage de Julie .
elle devait éviter la foule parce qu' elle n' avait pas beaucoup de défenses d' après ses analyses de sang , quelque chose autour des globules blanches et rouges et les plaquettes .
j' ai donc invité Carl qui , pour une raison que j' ignore , était propriétaire d' un smoking .
il est plus élégant que moi dans ma robe d' Omar .
personne n' est aussi belle que Julie ( dans ma robe !

c' est normal que la mariée soit la plus belle .
comme nous ne connaissons personne à part minimalement Julie , nous restons à la périphérie de la foule .
la salle est belle et fleurie , le buffet est appétissant et bien fournie .
les invités sont bien habillés et tout le monde semble heureux d' y être .
moi aussi !
c' est mon tout premier mariage .
je ne connais pas le rite , à part la mélodie de Mendelssohn pour accompagner les mariés vers l' autel .
c' est un mariage laïc dans lequel les mariés ont écrit le scénario avec des discours des uns et des autres : comment ils se sont rencontrés , comment ils se sont fait la cour , leurs espoirs pour l' avenir , leurs voeux .
de bonne humeur et enjoués , la salle entière sourit , car quoi de plus beau qu' un mariage d' amour ?
quoi de mieux que l' amour ?
je vois les rouages tourner dans la tête de Carl .
je m' imagine aussi dans le rôle de la mariée .
peut être me prêtera t elle la robe !
nous sommes certainement les plus jeunes de notre table de douze , mais tout le monde parle d' une façon animée .
les plats sont délicieux et entre deux nous dansons .
je n' ai jamais dansé avec Carl qui a bénéficié des leçons de danse du salon .
j' adore !
j' aimerais que cette soirée se poursuive jusqu'à ne jamais finir .
mais tout finit par finir .
tu aimerais un mariage comme ça ?
me demande Carl dans le métro qui ramène les deux Cendrillons chez eux .
c' était à la fois traditionnel et original .
il faut que j' aille une fois à un mariage juif pour découvrir .
et hindou et Musulman , Bouddhiste ?
il faut quand même que nous nous rattachions à une tradition plus proche à nous .
catholique ?
plutôt sans religion autre que le coeur .
et puis quel est l' intérêt de se marier quand tant de mariages finissent en divorce ?
tes parents , mes parents , Dorélie et la moitié de notre collège .
il faut être optimiste .
pour nous ça va être différent .
tout le monde dit ça .
mais tout le monde n' est pas prêt à faire le travail d' entretien .
Carl mon chéri , nous avons le temps d' en parler , de réfléchir , d' entreprendre , de changer .
on reverra tout ça dans dix ans .
d' accord mon amour .
je tiens bon !
il est tard après cette soirée fabuleuse .
je pensais que tout le monde sera couché dans un sommeil profond .
mais Omama m' attend en me disant " j' aime te dire bonne nuit . alors ? c' était beau ? "
je fais un récit rapide de la fête .
comment te sens tu Omama ?
bien , tout va bien .
je recommence la chimio cette semaine et j' espère que cela se passe aussi bien que le dernier cycle .
avec ton attitude époustouflante , ça se passera tout à fait aussi bien .
Josiane veut venir avec moi .
je n' aurai pas d' excuse alors pour sécher les cours .
et je n' aurai pas la culpabilité de t' empêcher de poursuivre ton éducation .
oh Omama !
j' apprends plus avec toi qu' à l' école .
aucune raison de te sentir coupable .
ni de s' inquiéter .
c' est génétique .
il n' y a une bonne raison de s' inquiéter : s' inquiéter parce qu' on s' inquiète trop !
je sais !
quelles grandes vacances ?