non seulement Carl veut programmer et planifier notre fête de mariage , il veut aussi parler des grandes vacances .
ce sont nos dernières vacances avant le lycée .
quelles vacances aimerais tu ?
avec Omama , auprès d' elle , avec des ballades avec toi .
j' espère que maman va prendre l' air aussi .
nous n' avons jamais été des grandes adeptes de vacances .
Carl a l' air déçu .
son père lui offre un voyage où qu' il veut pour le récompenser de ses bons résultats .
nous n' avons pas encore passé le brevet du collège .
ce que j' aime c' est trainer en pyjama , lire , écrire , faire des salades , les légumes d' été , me gaver de tomates qui ont du gout et peut être me rattraper en séries dont tout le monde parle .
j' aime Paris quand elle se vide de ses habitants qui sillonnent les routes bondées .
oui , mais c' est bon de changer d' air , de se recharger de nouvelles idées , de bouger .
le malheur c' est que tout le monde prend les vacances au même moment .
nous arrivons au cours de français .
ça a beau être la fin de l' année , Mademoiselle Laurent nous donne un devoir musclé : " racontez votre plus grand héros " .
j' en ai au moins deux , ou trois , ou quatre .
pendant toute la journée , j' essaie d' en choisir un .
à la cantine avec Carl , il me dit nonchalant : " Omar est parti avec ma mère en Italie . il a retrouvé sa famille à Lampedusa . "
quand ?
hier .
j' ai oublié de te le dire .
mais c' est énorme !
il a toujours ses béquilles ?
est ce qu' il sera de retour pour le brevet ?
je n' en sais rien .
ça fait un moment qu' il n' a plus besoin de béquilles ?
j' espère seulement que ma mère revienne !
je ne le vois plus .
il m' évite .
je me demande si Dorélie est au courant ...
elle le sera .
tu vas faire qui comme héros ?
je n' en sais rien , dit Carl au moment où Dorélie nous rejoint .
et toi Dorélie , ton héro ?
Omar !
quelqu' un qui a souffert , qui se bat , qui connait le prix de la liberté .
j' ai hâte de rentrer pour écrire .
Omama est au lit après sa journée de chimio .
je l' embrasse et je vais faire mes devoirs .
" franchement , je pense que chacun est héroïque à sa façon d' une façon ou une autre .
le bébé qui se bat pour sortir du ventre de sa mère , qui s' acclimate , qui grandit , qui parle et qui marche et qui lit .
l' ouvrier que fait son travail , les familles qui luttent pour rester en harmonie , le professeur qui canalise trente personnalités et essaie de transmettre et mille autres catégories d' êtres humains .
je voulais d' abord parler de mon voisin que j' ai trouvé mort chez lui , un résistant , un héros dans le sens grandiose du mot .
je voulais aussi désigner ma mère qui prend à coeur de nourrir sa mère et sa fille .
mais la seule que je voulais écrire en lettres en néon , c' est ma grand mère , non pas parce qu' elle accepte sans se plaindre ce cancer qui lui frappe , non pas parce qu' elle a gardé un secret empoisonnant pour nous protéger , non pas parce qu' elle considère chaque corvée comme un labeur d' amour .
disons que c' est à cause de sa disposition ensoleillée , son optimisme qui devient toujours la réalité , son appréciation du quotidien , sa sagesse , son humour , sa générosité .
Omama dit que la seule personne que tu peux connaître éventuellement est toi même .
et ça dans les meilleurs des cas .
je connais juste quelques petits faits : elle est née en Pologne avant la Deuxième Guerre Mondiale .
je n' ai pas une idée de ce qu' elle a vécu mais je ne désespère pas de lui extraire plus de détails .
elle s' est enfuit en Allemagne et puis en France où sa situation de réfugiée ne lui a pas permis d' aller à l' école puisqu' il a fallu qu' elle travaille dur dans les cuisines .
elle a connu un grand amour qui est mort avant la naissance de sa fille ( ma mère ) .
elle ne s' est jamais permise de pleurer sur son sort , seulement de faire face , d' assumer et dans la joie .
l' exemple de ma grand mère me pose quelques questions : pourquoi les uns se lamentent et pleurnichent alors qu' ils aient tout , comme on dit , pour être heureux , et les autres acceptent , positivent , font tout leur possible pour améliorer leur sort .
même quand elle est frappée par ce qu' on nomme " l' horrible crabe " , elle affirme que ce n' est pas si horrible , que cette maladie a mauvaise presse mais beaucoup de gens meurent de la grippe aussi .
elle minimalise la douleur en disant , qu' il ne faut pas que le cerveau entende des plaintes .
elle fait la pub auprès de ses voisines de chambre de chimio les convaincant du progrès dans la recherche et de l' atténuation de la souffrance .
et les malades autour d' elle ressortent plus fortes que quand elles sont entrées .
je ne connais pas beaucoup de gens qui apprennent à lire à son âge .
est ce que la maladie l' a poussé à ce sursaut de vie ?
est ce que c' est le traumatisme qui lui a forcé à faire des confidences ?
et pour nous consoler , elle dit " je suis et j' étais heureuse dans la vie . qui dit que je ne serai pas heureuse dans la mort ? "