en ce moment arriva l' infatigable courtier de la maison Sonet , suivi du seul homme qui se souvînt de Pons , qui pensât à lui rendre les derniers devoirs .
cet homme était un gagiste du théâtre , le garçon chargé de mettre les partitions sur les pupitres à l' orchestre , et à qui Pons donnait tous les mois une pièce de cinq francs , en le sachant père de famille .
ah !
Dobinard