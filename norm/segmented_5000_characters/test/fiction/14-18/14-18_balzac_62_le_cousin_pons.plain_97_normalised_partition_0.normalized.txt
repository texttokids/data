Schmucke n' écoutait pas , il était plongé dans une telle douleur , qu' elle avoisinait la folie .
l' âme a son tétanos comme le corps .
et vous feriez bien de vous faire représenter par un conseil , par un homme d' affaires .
ein home t' avvaires !
répéta Schmucke machinalement .
vous verrez que vous aurez besoin de vous faire représenter .
à votre place , moi , je prendrais un homme d' expérience , un homme connu dans le quartier , un homme de confiance ...
moi , dans toutes mes petites affaires , je me sers de Tabareau , l' huissier ...
et en donnant votre procuration à son premier clerc , vous n' aurez aucun souci .
cette insinuation , soufflée par Fraisier , convenue entre Rémonencq et la Cibot , resta dans la mémoire de Schmucke , car , dans les instants où la douleur fige pour ainsi dire l' âme en en arrêtant les fonctions , la mémoire reçoit toutes les empreintes que le hasard y fait arriver .
Schmucke écoutait Rémonencq , en le regardant d' un oeil si complétement dénué d' intelligence , que le brocanteur ne lui dit plus rien .
s' il reste imbécile comme cela , pensa Rémonencq , je pourrais bien lui acheter tout le bataclan de là haut pour cent mille francs , si c' est à lui ...
monsieur , nous voici à la Mairie .
Rémonencq fut forcé de sortir Schmucke du fiacre et de le prendre sous le bras pour le faire arriver jusqu' au bureau des actes de l' État civil , où Schmucke donna dans une noce .
Schmucke dut attendre son tour , car , par un de ces hasards assez fréquents à Paris , le commis avait cinq ou six actes de décès à dresser .
là , ce pauvre allemand devait être en proie à une passion égale à celle de Jésus .
monsieur est monsieur Schmucke ?
dit un homme vêtu de noir en s' adressant à l' Allemand stupéfait de s' entendre appeler par son nom .
Schmucke regarda cet homme de l' air hébété qu' il avait eu en répondant à Rémonencq .
mais , dit le brocanteur à l' inconnu , que lui voulez vous ?
laissez donc cet homme tranquille , vous voyez bien qu' il est dans la peine .
monsieur vient de perdre son ami , et sans doute il se propose d' honorer dignement sa mémoire , car il est son héritier , dit l' inconnu .
monsieur ne lésinera sans doute pas ...
il achètera un terrain à perpétuité pour sa sépulture , Monsieur Pons aimait tant les arts !
ce serait bien dommage de ne pas mettre sur son tombeau la Musique , la Peinture et la Sculpture ...
trois belles figures en pied , éplorées ...
Rémonencq fit un geste d' Auvergnat pour éloigner cet homme , et l' homme répondit par un autre geste , pour ainsi dire commercial , qui signifiait : " laissez moi donc faire mes affaires ! " et que comprit le brocanteur .
je suis le commissionnaire de la maison Sonet et compagnie , entrepreneurs de monuments funéraires , reprit le courtier , que Walter Scott eût surnommé le jeune homme des tombeaux
si monsieur voulait nous charger de la commande , nous lui éviterions l' ennui d' aller à la Ville acheter le terrain nécessaire à la sépulture de l' ami que les Arts ont perdu ...
Rémonencq hocha la tête en signe d' assentiment et poussa le coude à Schmucke .
tous les jours , nous nous chargeons , pour les familles , d' aller accomplir toutes les formalités , disait toujours le courtier encouragé par ce geste de l' Auvergnat .
dans le premier moment de sa douleur , il est bien difficile à un héritier de s' occuper par lui même de ces détails , et nous avons l' habitude de ces petits services pour nos clients ?
nos monuments , monsieur , sont tarifés à tant le mètre en pierre de taille ou en marbre ...
nous creusons les fosses pour les tombes de famille ...
nous nous chargeons de tout , au plus juste prix .
notre maison a fait le magnifique monument de la belle Esther Gobseck et de Lucien de Rubempré , l' un des plus magnifiques ornements du Père Lachaise .
nous avons les meilleurs ouvriers , et j' engage monsieur à se défier des petits entrepreneurs ...
qui ne font que de la camelotte , ajouta t il en voyant venir un autre homme vêtu de noir qui se proposait de parler pour une autre maison de marbrerie et de sculpture .