cinq ou six mille hommes qui , huit jours auparavant , ne faisaient pas même partie de la garde nationale , furent de la sorte armés et habillés .
tous ces hommes devaient obéir , non pas aux ordres de leurs colonels , mais au signal d' un chef carbonaro reconnaissable par eux seuls .
toutefois , comme les plus avancés ne croyaient pas encore l' heure de l' insurrection venue , il était ordonné , de la part de la vente suprême , de ne se porter à aucun acte d' hostilité pendant la revue .
de son côté , la police était sur pied et se tenait l' oeil au guet , l' oreille aux écoutes .
mais que faire contre des hommes qui s' empressent d' obéir aux ordres du roi ?
Monsieur Jackal incorpora dix hommes dans chaque légion , seulement , comme cette idée ne lui vint que lorsqu' il eut appris le mouvement qui s' opérait , il se trouva que les tailleurs de Paris avaient tant d' ouvrage , que la plupart des hommes de Monsieur Jackal furent bien armés le dimanche , mais ne furent habillés que le lundi .
c' était trop tard !
la revue du dimanche vingt neuf avril .
depuis le moment où l' ordre du jour annonçant la revue pour le vingt neuf avril avait été publié , jusqu' au jour de cette revue , on avait senti courir dans Paris un de ces sourds tressaillements qui précèdent et annoncent les orages politiques .
nul ne pouvait dire ce que présageait cette espèce de fièvre , ni même qu' elle présageât quelque chose , mais , sans savoir à quel vertige on était en proie , on se rencontrait , on se serrait la main , on se disait :
vous y serez ?
dimanche ?
oui .
je crois bien !
n' y manquez pas !
je n' ai garde !
puis on se serrait de nouveau la main les maçons et les affiliés aux ventes avec le signe de leur société , les autres tout simplement , et l' on se quittait en se disant chacun à soi même :
Y manquer ?
ah !
par exemple !
du vingt six au vingt neuf , les journaux libéraux ne firent que parler de cette revue , excitant les citoyens à s' y trouver et leur recommandant la prudence .
on sait ce que veulent dire ces recommandations venant de plumes ennemies du gouvernement , elles veulent dire : " tenez vous prêts à tout évènement , car un évènement est suspendu dans l' air , et saisissez l' occasion ! "
ces trois jours n' avaient point passé indifférents pour les jeunes héros de notre histoire .
cette génération , qui est la nôtre est ce un avantage ou une infériorité ?
avait encore , à cette époque , la foi , perdue non point par elle elle est restée jeune de coeur , mais par la génération qui l' a suivie , et qui est aujourd'hui celle des hommes de trente à trente cinq ans .
cette foi , c' est le vaisseau qui a fait naufrage dans les révolutions de mille huit cent trente et de mille huit cent quarante huit , lesquelles étaient encore cachées dans l' avenir , comme un enfant qui vit et qui tressaille déjà est caché dans le sein de sa mère .
chacun de nos jeunes amis avait donc senti l' influence de ces trois jours , les uns activement , les autres passivement .
Salvator , un des principaux chefs du carbonarisme , cette religion de l' époque , âme des sociétés secrètes , organisées non seulement à Paris , non seulement dans les départements , mais encore à l' étranger , Salvator avait , comme nous l' avons vu , contribué activement à renforcer les rangs de la garde nationale de cinq ou six mille patriotes qui , jusque là , n' en avaient point fait partie .
ces patriotes étaient habillés , avaient des fusils : c' était l' important , des cartouches , il serait facile de s' en procurer , à un jour donné , à un moment convenu , on se retrouverait avec un uniforme et des armes .
Justin , simple voltigeur dans une compagnie de la onze
légion , Justin , qui avait jusque là négligé ces relations superficielles qu' une nuit passée au corps de garde , que deux heures passées en faction nouent entre deux citoyens , Justin , depuis qu' il avait vu dans le carbonarisme un moyen de renverser ce gouvernement sous lequel un noble , appuyé d' un prêtre , pouvait impunément porter le trouble dans les familles , Justin s' était mis à faire de la propagande carbonariste avec une activité d' autant plus grande qu' elle avait été jusque là contenue , et , comme il était estimé , aimé , honoré même , dans son quartier , à cause de ses vertus de famille , si bien connues , il était écouté comme un oracle par des gens qui , au reste , ne demandaient pas mieux que d' être convaincus et qui allaient eux mêmes au devant de la conviction .
quant à Ludovic , Petrus et Jean Robert , c' étaient de simples unités , mais agissant chacune sur un centre .
Ludovic inspirait et dirigeait ses jeunes condisciples , les étudiants en droit et en médecine , dont il avait quitté les rangs depuis la veille à peine , Petrus , toute cette jeunesse d' atelier , alors pleine de flamme artistique et de foi nationale , Jean Robert , tout ce qui tenait une plume et qui , suivant un chef reconnu sur le terrain de l' art , était prêt à le suivre aussi sur tout autre terrain où il lui plairait de s' aventurer .
jean Robert faisait partie de la garde nationale à cheval , Petrus et Ludovic étaient lieutenants dans la garde nationale à pied .