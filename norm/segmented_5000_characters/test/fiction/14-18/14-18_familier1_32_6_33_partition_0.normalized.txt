le bruit de notre converse éveille la mère Pâquerette ( en l' eau cul rance ce serait plutôt un dahlia qu' elle évoquerait ) .
ma présence la trouble comme une goutte d' eau trouble le Ricard le plus pur .
elle se séante et ses vingt kilogrammes de nichons plouffent sur son ventre à replis .
du coup , les lunettes tatouées adoptent un regard de myope .
salut , bouffie , la salue je galamment , sois gentille : cache ta triperie !
le matin , les abats me portent au coeur !
elle relève un bout de drap qui trainait par là sur sa poitrine gélatineuse .
une docile .
plutôt une soumise .
la môme idéale pour devenir pute professionnelle .
elle se trouve dans l' antichambre de la prostitution , Pâquerette .
un pas de plus et elle met le pied dedans , comme toi dans une merde quand tu vas acheter le journal .
donc , reviens je à Ted , il s' agit d' une blague ?
tu m' as vu à la Rose d' Or .
tu te dis : " tiens , ce salaud de flic à qui je dois la belle cicatrice qui ajoute tant à mon charme , prend du bon temps avec une gerce , je vais lui jouer un tour " .
tu tires ton mouchoir , tu écris ce message mystérieux et inquiétant .
tu charges ta rombiasse de me le faire tenir sans m' alerter .
cette grosse charrette , pas sure d' elle , transmet le flambeau à la copine délurée qui vous accompagne .
et , effectivement , ta babille sur fil d' Écosse arrive à bon port .
O .
K ?
c' est la version qu' on enregistre , tout est bon , y a pas de virgule à changer dans le texte ?
bon , OK , alors saboulez vous , les deux , et suivez moi .
pas une partouze , rassure toi , ton brancard me ferait dégoder .
faut être english pour pouvoir s' embourber ce catafalque de bidoche pas nette !
j' ai débité la dernière réplique en anglais , pas désobliger la grosse .
tu connais ma galanterie légendaire ?
le couple se lève , sans trop de pudeur , et se loque avec mornitude .
Ted est blafard sous sa rouquinerie .
il a des cils de porc , comme ceux dont se servent les artistes chinois pour peindre sur un grain de riz la conquête de Pékin par les Mandchous en mille six cent quarante quatre .
en passant son jean , je le vois qui en palpe les vagues .
non , tu ne l' as pas perdu , lui fais je , c' est moi qui l' ai .
une qui ouvre des vasistas grand comme l' entrée principale de Saint Pierre de Rome en nous voyant radiner tous les trois , c' est ma miss Lola .
elle me questionne du regard .
ce sont les amis qui m' ont carré dans la braguette le message que tu sais !
du coup , la v' là qu' ébullitionne .
elle louche sur Pâquerette .
non , une de ses amies beaucoup plus souple .
commander à bouffer , ils ont un room service l' hôtel .
j' ai lu le menu dans l' ascenseur , je serais assez pour de la viande des grisons et des filets de perche meunière , pas vous , mes amis ?
c' est bon et léger pour le déjeuner .
ça ne vous abime pas l' après midi .
un coup de fendant pour arroser le tout et nous serons en pleine forme pour faire quelques parties de rami dans l' après midi .
Lola pige de moins en moins .
non , rassure toi .
on reste ensemble jusqu' au spectacle de ce soir .
nous irons à la Rose d' Or tous les quatre , et ensuite , si nous sommes encore vivants , nous nous séparerons .
ok , Teddy ?
il me regarde et hausse les épaules .
toujours a moi que ÇA ARRIVE !
ces dames refusant de jouer aux cartes , nous fîmes un poker , Ted et moi .
il trichait à la grecque , ce qui est rare pour un natif de la Grande Albion de mes fesses et m' épongea cinq cents francs .
et cinq cents vrais francs : pas des français ni des belges , des suisses .
tu avoueras qu' il n' est pas commun qu' un flic se fasse secouer sa fraiche par le malfrat qu' il surveille .
mais je ne suis pas n' importe quel poulet , tu l' auras déjà pressenti .
la journée se dérouta dans une torpeur un peu cafardeuse , sous un ciel où le soleil se laissait biter par des floconneries de nuages .
les cris acides des mouettes ajoutaient à la mélancolie ambiante .
j' avais déjà vécu des moments de ce tonneau au ( long ) cours de veillées funèbres consacrées à des gens qui ne me touchaient pas de trop près .
entre autres , après le décès de la mère Dunkerque , une voisine presque impotente qui se prénommait Rose ( car c' est la rose l' impotente ) .
m' man s' était occupée d' elle sur la fin de ses jours .
elle ressemblait à une baleine échouée sur la grève de son plumard , la mère Dunkerque .
des bajoues à n' en plus finir , des nichons plein le lit , un ventre qui foirait tout azimut .
elle matait sa téloche toute la sainte journée , en actionnant constamment , les boutons de la télé commande , sans jamais se fixer sur un programme .
une butineuse d' ondes hertziennes !
et puis elle était clamsée gentiment , un après midi d' automne ( c' était peut être le printemps , mais quand tu meurs , c' est toujours l' automne ) .
on l' avait veillée en compagnie d' un autre voisin serviable .