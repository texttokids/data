jeudi .
rose n' est pas encore arrivée .
elle a prévenu quelqu' un ?
demande Clara .
non , personne .
je ne sais même pas où elle habite , dit Marion .
la porte s' entrouve à cet instant .
rose entre maladroitement , les bras chargés d' un gros cageot rempli de légumes .
aidez la donc !
s' écrie Clara .
Noah se précipite , ouvre la porte .
Farid prend le cageot des mains de Rose et le dépose sur la table .
mais tu nous as apporté un jardin entier !
s' exclame Clara en se levant .
c' est mon père qui les a donnés , dit Rose .
pour l' atelier .
comme toujours , elle est vêtue d' une longue jupe informe , rose défraichi , et d' un tee shirt vert épinard mal assorti .
mais elle sourit .
elle sourit .
c' est cultivé en bio , ajoute t elle à mi voix , comme si elle s' excusait .
en tout cas , c' est parfait pour la leçon du jour .
je vous rappelle , aujourd'hui , c' est quiche et tarte salée .
Hector a apporté une recette de tarte à la tomate qui semble sympathique .
c' est mon tube , dit Hector .
l' avantage avec la quiche , c' est qu' une fois qu' on a compris le principe , on peut improviser avec les ingrédients qu' on a à la maison , reprend Clara .
pour commencer , on va préparer la pâte brisée .
pas question d' utiliser la pâte industrielle des supermarchés .
qui sait faire la pâte brisée ?
moi !
s' écrie Noah .
mais aussi Alice .
et Farid , Marion et Hector .
Pedro dit : " je crois " .
Kev et Rose ne disent rien .
après , c' est la confusion : chacun a une recette différente et chacun est convaincu que la sienne est la meilleure .
ma mère , elle met pas d' oeuf !
dit Hector .
la mienne non plus , le soutient Alice , mais elle ajoute une pincée de levure chimique .
non , il faut mettre un oeuf , dit Marion .
ma grand mère , elle faisait toujours comme ça .
pas l' oeuf entier , juste le jaune !
corrige Farid .
c' est mon oncle qui me l' a dit ...
la discussion s' anime .
Clara , les coudes sur la table , la tête dans les mains , les écoute sans réagir .
et puis Noah demande :
au fait , qui c' est qui a inventé la pâte à tarte ?
ils se taisent tout à coup et , dans un bel ensemble , leurs regards se tournent vers Clara .
bonne question , dit elle en souriant .
j' y pense souvent quand je fais la cuisine .
qui a inventé la mayonnaise ?
qui , le premier , a eu l' idée de séparer les blancs des jaunes d' oeuf et de battre les blancs en neige ?
étonnant , non ?
elle se redresse , caresse furtivement son ventre , continue :
on sait que les Grecs et les Romains , déjà , faisaient de la pâtisserie , des tartes notamment .
et la pâte feuilletée vient du Moyen Orient ...
elle raconte .
et pendant qu' elle raconte , Kev pose sa main sur le bras nu de Marion .
pas sûr qu' il soit conscient de son geste .
marion sent la chaleur courir dans son bras , gagner l' épaule et gonfler sa poitrine .
elle ne bouge pas , elle sait qu' elle ne doit pas bouger .
pour ne pas crever la bulle de bonheur qui l' a soudain enveloppée .
mais alors , c' est quoi , la bonne recette ?
demande Farid .
pourquoi y aurait il une bonne recette ?
répond Clara .
je propose que chacun essaye la sienne .
moi aussi , j' en ai une .
c' est un secret de famille .
je vous la donnerai , mais promettez moi de ne le diffuser , ok ?
à la cuisine , ils s' organisent par groupes .
rose et Kev s' associent à Marion ( recette avec l' oeuf entier ) .
Pedro hésite .
finalement , il se joint à Farid ( qui n' utilise que le jaune de l' oeuf ) .
Hector , Alice et Noah préparent ensemble une pâte brisée sans oeufs ( Alice a renoncé à la levure ) .
et Clara leur montre comment procédait sa grand mère alsacienne deux noeuds .
pendant que les pâtes reposent au frais , Clara forme de nouveaux duos : Noah et Rose , Kev et Pedro , Marion et Hector , Alice et Farid .
je vous demande d' inventer une recette de quiche en utilisant les ingrédients à votre disposition : les légumes apportés par Rose et tout ce que vous trouverez dans la chambre froide .
réfléchissez , discutez , et puis écrivez avant de réaliser .
voici la liste des ingrédients : tomates fraiches , tomates séchées , courgettes , carottes , miel , basilic , aubergines , menthe , coriandre , oignon frais , ail , jambon cuit , jambon cru , comté , ricotta , noisettes , cumin , citron , poivre , sel , oeuf , lait , crème , huile d' olive .
ils ont réfléchi , discuté , écrit et puis réalisé .
quatre quiches sont sorties du four ce matin là deux noeuds .
ils ont invité Sylvie et le personnel du centre à les déguster avec eux .
en dessert , bien sûr , ils savourent les charlottes aux framboises .
au fait , c' est quoi la blague de ton père ?
demande Farid .
c' est pas vraiment une blague , juste une plaisanterie , répond Alice .
quand on découpe la charlotte , elle a tendance à s' effondrer , alors mon père dit : " l' amour , c' est comme la charlotte aux framboises , ça s' écroule dès qu' on y touche . "
et tu trouves ça drôle ?
dit Marion .
oui , parce que ma mère répond à chaque fois : l' année prochaine , je ferai le gâteau au noix de ma grand mère , et celui là , il est dur comme du bois .
j' ai pas compris la blague , dit Noah en léchant religieusement sa cuillère .
moi , non plus , dit Farid , mais je me marierai bien rien que pour manger une charlotte aux framboises .