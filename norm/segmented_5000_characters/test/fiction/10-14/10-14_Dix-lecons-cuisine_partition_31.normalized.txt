il est près de six heures quand ils rentrent .
ils n' ont pas pris le chemin le plus court .
ils ont roulé côte à côte , d' un chemin à l' autre .
ils passent devant le cimetière , prennent le raccourci qui mène à la maison d' Annette .
Kev met la main sur le guidon de Marion , l' arrête .
ils posent leurs vélos par terre , s' approchent l' un de l' autre , face à face .
Kev prend la main droite de Marion entre ses deux mains , la porte à ses lèvres , embrasse les doigts un à un , du pouce à l' auriculaire .
et retour .
tu te souviens ?
dit il .
marion l' attire contre elle , lui souffle dans le cou .
et défait deux boutons de sa chemise .
il rit .
c' est plus bas que ça chatouille , dit il .
Cretino , dit elle .
oh oui , dit il , oh oui .
répète ...
Cretino , cretino , cretino ...