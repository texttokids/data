Noah , Rose et Alice sont les premiers à rentrer , à dix heures quarante deux .
Kev et Marion suivent à dix heures cinquante et un .
les derniers sont Pedro , Farid et Hector qui arrivent , un peu essoufflés , à onze heures pile .
marion et Kev ont rapporté une grande brioche que leur a donnée le boulanger .
Kev découpe des parts avec son couteau suisse .
on l' a regardé faire la pâte , dit Marion .
Kev sort son carnet de sa poche .
trois kilos de farine , soixante grammes de sel , un , deux kilo d' oeufs , un , deux kilo d' eau , énonce t il .
un , deux litre , tu veux dire , le corrige Alice .
non , il pèse l' eau sur sa balance .
et c' est au gramme près .
j' ai jamais mangé une brioche aussi bonne , dit Rose .
les autres la regardent , étonnés .
la brioche de Stéphane et ses pains au chocolat , eux , ils connaissent depuis l' enfance .
elle a raison , dit Kev .
même à Besançon , on n' en trouve pas une comme ça .
c' est vrai ?
dit Noah .
moi , je préfère la brioche du supermarché , dit Hector , en s' emparant du dernier morceau de brioche .
s' en suit une discussion animée , interrompue par Marion qui vient de recevoir un message de Clara sur son portable .
bonjour à tous .
je vais bien .
c' était une fausse alerte .
j' espère que les interviews se sont bien passées .
avez vous vu le panneau qu' a préparé Sylvie à ma demande ?
bon travail et à demain .
c' est Rose qui découvre le panneau punaisé au fond de la bibliothèque .
plutôt une longue bande de papier d' environ deux mètres de longueur sur un de large .
une seule phrase , écrite au feutre rouge :
" pouvez vous lister les mots nouveaux que vous avez découverts avec Paul , Aurore et Stéphane ? "
qu' est ce qu' on doit faire ?
demande Noah .
ben , c' est marqué , dit Pedro .
il prend un feutre dans un pot et , sans hésiter , écrit :
" Prézur "
c' est le truc pour faire cailler le lait .
aurore nous a expliqué .
je crois pas que ça s' écrive comme ça , dit Rose .
elle vérifie sur un dépliant que leur a donné Aurore et corrige :
" présure "
ça suffit à donner le mouvement .
à tour de rôle ils prennent un feutre , écrivent un mot , se concertent , se souviennent , et le panneau se couvre peu à peu de mots .
de dessins et de croquis aussi .
Pedro a compté : ils ont noté au total trente sept mots .
j' aurais pas cru , dit Farid .
pas cru quoi ?
fait une voix derrière lui .
c' est Sylvie qui les regarde travailler depuis un moment .
ben , qu' on avait retenu autant de choses ...
c' est surement parce qu' Aurore , Stéphane et Paul ont réussi à vous intéresser .
au fait , vous leur avez demandé leurs recettes préférées deux noeuds ?
n' oubliez pas de les donner à Clara .
elle sera là demain .
moi , ce soir , je vais manger à McDo avec mes cousins , dit Noah .
quel rapport avec ce que je viens de dire ?
demande Sylvie .
rien , sauf qu' il a faim , dit Marion .
et qu' il veut faire son intéressant , ajoute Alice .
Sylvie regarde sa montre .
onze heures cinquante neuf et cinquante cinq secondes .
tu as une horloge dans le ventre , Noah .
allez , disparaissez ...
( : insérer ici le tableau réalisé par le groupe .

interlude