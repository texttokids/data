plus tard , vers onze heures , Kev raccompagne Marion chez elle .
sa mère est déjà rentrée .
Alessandro et Léa les accompagnent .
après quelques dizaines de mètres , Alessandro et Léa les distancent .
ils avancent enlacés et leurs corps ondulent en accord , courbes qui jamais ne s' éloignent .
du bout des doigts , Alessandro joue avec les cheveux de Léa .
elle , elle a glissé un doigt dans un des passants de la ceinture d' Alessandro et , de temps en temps , l' attire contre elle , l' embrasse sur la joue ou lui mordille l' oreille .
Kev les regarde .
marion a cherché sa main et leurs doigts se sont entrecroisés .
après la fontaine , Alessandro et sa soeur continuent tout droit au lieu de tourner à gauche .
Kev ne les appelle pas , la nuit est trop douce pour qu' on la trouble .
il passe un bras autour des épaules de Marion et sent sa hanche trouver aussitôt sa place contre la sienne .
sous sa main , l' épaule de Marion est ronde , vivante , et la peau nue frissonne à peine .
devant la maison de Marion , ils se font face .
marion l' attire contre lui , l' embrasse .
c' est un baiser sérieux et apaisé .
et profond .
leurs langues se cherchent , se trouvent , se mêlent .
leurs respirations se fondent l' une à l' autre et le corps de l' un enveloppe celui de l' autre .
quand ils se détachent , un clocher égraine onze coups .
la lumière du réverbère vacille .
marion s' éloigne , se retourne quand elle est à la porte .
ils ne disent rien .
elle entre , referme doucement la porte derrière elle .
et lui , s' éloigne , étranger à lui même , soulevé par une sensation , un sentiment dont il ne connait pas le nom .
leçon dix
" ceux qu' une même nourriture réunit sont unis pour la vie . " ( proverbe malgache )
Clara a dit : " pas avant dix heures . il nous faudra le temps de tout installer " .
Farid arrive à neuf heures " pour donner un coup de main " .
rose se pointe à neuf heures vingt avec ses deux frères et un lourd panier d' osier .
Hector , Alice et Noah apparaissent deux minutes plus tard ( " c' est ma mère qui nous a déposés en allant au travail " , explique Noah ) .
Kev accompagne Léa et Alessandro qui doivent chacun animer un atelier .
à neuf heures vingt sept , il envoie un message à Marion :
" tu viens ? on t' attend . tout le monde est là , sauf Pedro . "
message retour :
" suis déjà là . j' aide Simon . "
Kev :
" où ? "
marion :
" cherche . "
il cherche .
et la trouve , près du potager collectif .
elle décharge des tables pliantes d' une camionnette municipale .
aide moi , dit elle .
on les met où , les tables ?
sous les arbres .
Clara et Sylvie dirigent les opérations .
il faut organiser l' espace , répartir le matériel , s' occuper de l' accueil .
le personnel du centre aéré a été réquisitionné et des parents se sont portés volontaires .
où est Simon ?
demande Kev .
il est allé chercher une rallonge , dit Marion .
il a dit qu' il voulait nous voir .
pourquoi ?
il a une proposition à nous faire ...
tiens le voilà ...
Simon approche en agitant les bras .
content de retrouver , Kev , dit il en lui serrant les deux mains dans les siennes .
Clara est tombée sous ton charme , tu sais , je suis presque jaloux .
embarras de Kev , tandis que Marion fait semblant d' être choquée .
et puis Simon en vient au fait :
vous animez un atelier , vous deux ?
non , répond Marion .
alors , j' aimerais que vous soyez les reporters de l' événement .
prenez des notes , des photos , et avec Clara nous vous aiderons à rédiger un article .
il pourrait paraitre dans une revue pour laquelle travaille Clara .
pourquoi tu ne le fais pas , toi ?
dit Kev .
d' abord , parce que ce serait mieux si c' était votre regard .
et puis , je veux aider Clara , elle doit se ménager un peu ...
vous avez de quoi écrire ?
oui , dit Marion , mais pour les photos ...
je vous prête mon appareil .
il est très simple .
allez y , je rejoins Clara ...
extraits du carnet de notes de Kev et Marion ( Marion a écrit , Kev a fait des croquis ) .
" plan du parc et liste des ateliers :
atelier beignets de légumes et de fruits ( Grand mère de Noah ) .
atelier fabrication fromage blanc ( Léa )
atelier pâtes fraiches ( Alessandro )
atelier jus de fruits et de légumes ( Farid et sa mère )
atelier " utiliser les restes " ( rose , sa mère et ses frères )
atelier petits gâteaux ( biscotti ) ( Hector , Alice et Stéphane le boulanger )
atelier : racontez votre recette préférée ( Clara , Simon et Martin ) "