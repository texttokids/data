samedi après midi .
Alessandro sert le café à Annette et Léa dans le jardin .
sur la table , les restes d' une tarte aux prunes .
marion pousse le portail qui pousse un miaulement aigu .
Kev n' est pas là .
on devait se retrouver ici , dit Marion .
à deux heures .
Léa regarde sa montre : deux heures exactement .
il est parti à vélo , il y a une demi heure .
il ne t' a pas laissé de message ?
marion vérifie sur son portable : non , rien .
si mon frère commence à poser des lapins à ses copines , je lui tirerai les oreilles , tu peux compter sur moi , dit Léa .
mais il n' a rien dit ?
demande Marion .
Alessandro se frappe le front du plat de la main .
ma che cretino !
ho dimenticato ...
il se lève d' un bond , court vers la maison , revient avec une enveloppe qu' il tend à Marion .
per te .
il y a un grand un tracé au feutre noir sur l' enveloppe .
c' est tout .
marion ne sait que faire : l' ouvrir devant les autres ?
attendre d' être chez elle ?
Annette comprend .
bon il est temps de débarrasser la table .
tu m' aides , Léa ?
Alessandro , va chercher un plateau s' il te plait .
marion s' éloigne , alors , sort du jardin , reprend son vélo appuyé contre le grillage .
elle fait quelques pas seulement et déchire l' enveloppe .
une feuille blanche , une seule phrase écrite en lettres d' imprimerie : " un Arbre pleure , viens LE CONSOLER . "
marion sourit .
facile .
elle a deviné , bien sûr : Kev l' attend près du vieux saule pleureur , à la sortie de Montrond , en direction du lac .
il lui faut moins de cinq minutes pour s' y rendre .
mais pas de Kev en vue .
elle s' assoit sur le banc défoncé à l' ombre du saule .
il va arriver par surprise , surgir d' un buisson peut être , un truc de gamin .
une sonnerie annonce un message .
sur l' écran du portable :
l' arbre a un creux .
je lui ai donné à manger .
Cretino , murmure Marion .
tout le monde sait qu' il y a un creux dans le saule .
elle en fait le tour , cherche la cavité , y plonge la main , sent une autre enveloppe .
marqué par un deux ( en rouge , cette fois ) .
message : " un Phare dans les pres , sauras tu Y GRIMPER ? "
un phare ?
marion ferme les yeux , se concentre sur le mot .
un phare .
un phare dans les prés .
mais oui !
elle enfourche sa bicyclette et après trois cent mètres tourne sur un chemin mal entretenu .
quelques minutes plus tard , elle l' aperçoit : le château d' eau , planté sur une maigre colline , au milieu d' une prairie .
elle doit laisser son vélo , passer sous un fil barbelé .
avant qu' elle atteigne le château d' eau , son portable affiche un message :
trois à droite , cinq en haut .
trois , c' est sans doute trois pas , ou trois mètres , mais à partir de quel repère ?
peut être la porte en fer qui marque l' entrée ?
et cinq , ce sont les rangées de briques ...

quatre , cinq ...
oui , il y a une brique ébréchée et une enveloppe pliée y est glissée .
numéro trois ( en vert ) .
" un Rideau vert tout au bout d' un chemin . si tu l' ecartes , j' y SERAI . "
elle comprend aussitôt .
mais comment se fait il qu' il connaisse l' endroit ?
elle ne lui en a jamais parlé , ils ne s' y sont jamais retrouvés .
elle n' est plus pressée maintenant .
elle continue sur le chemin , puis prend le sentier sous les arbres .
elle grimpe le raidillon , saute de la bicyclette sur le terre plein .
Kev a caché son vélo quelque part .
ah , ah ...
elle avance à pas de sioux , attentive à ne pas faire craquer une branche , à ne pas faire rouler un caillou .
délicatement , elle écarte le rideau de liane et de lierre .
il est là , assis sur une pierre .
il la regarde .
sans sourire .
elle hésite , comme intimidée .
alors il tend la main , et elle le rejoint , s' assied à côté de lui .
attend .
il lui caresse les cheveux , y fait courir ses doigts , approche son visage , pose ses lèvres la tempe gauche .
il y a encore un message sur ton portable , dit il .
elle déverrouille son portable , lit :
si tu m' as trouvé , embrasse moi .
elle prend le visage de Kev dans ses mains .
passe les pouces sur ses sourcils .
détaille chaque partie : front , pommettes , nez , joues , lèvres , menton .
et les fossettes , parce qu' il sourit , à peine .
lentement , très lentement , elle se penche vers lui , frotte son nez contre le sien , promène la pointe de sa langue sur ses lèvres .
et l' embrasse .
elle prend son temps , laisse le baiser éclore , éclater .
elle sent les mains de Kev masser ses épaules , son dos .
elle l' enlace , l' enserre .
il faut que le baiser leur coupe le souffle , les asphyxie .
pour respirer , ensuite , au même rythme .
doucement , elle se détache .
dit :
tu as mis ta chemise ...
oui .
les boutons sont jolis , non ?
ça fait un peu fille ...
alors , déboutonne les ...
elle lui pince la joue .
Cretino ...
avant , tu disais : " t' es con . "
elle ne répond pas .
elle replie les jambes , pose la tête sur les genoux .
à mi voix , elle dit :
c' était chouette .
quoi ?
tout .
les enveloppes , les messages , le jeu de piste ...
et maintenant .
il se tourne à moitié , prend le sac à dos qu' il a posé contre la paroi de la grotte , en tire une boite en plastique , la lui tend .
elle ôte le couvercle .
il y a une part de tarte aux prunes dans la boite .
elle lève les yeux vers lui .
c' est la recette ...
, dit il .
celle qu' on devait apporter pour l' atelier ?
oui .
goute .
c' est juste une tarte aux prunes , pense t elle .
comme on en fait quand c' est la saison des prunes dans les jardins .
la tarte de Kev est rustique deux noeuds .
ce n' est pas de la pâte brisée , mais une pâte levée , plus épaisse , comme de la pâte à pizza .
elle mord dans la tarte .
un peu de jus coule sur son menton .
dans sa bouche , un mélange de saveurs : acidité du fruit , douceur des sucres , arrière gout ( à peine perceptible ) de la levure , parfum de cannelle .
une autre bouchée .
et , brusquement , des images dans sa tête : Manou dans sa cuisine , le jardin de sa maison , le jour de l' enterrement ...
elle comprend .
elle dit :
c' est la recette de ta maman ?
il hoche la tête .
elle ne pose pas d' autre question .
elle finit le gâteau , prenant son temps entre chaque bouchée .
tu pars quand ?
demande t il enfin .
demain matin , vers six heures .
l' Avion est à dix heures et quelques .
c' est bête que tu t' en ailles , je reste encore une semaine .
on ne pouvait pas choisir la date .
elle referme la boite en plastique , la pose près d' elle .
c' est la première fois que je prendrai l' avion .
elle pose une main sur son avant bras .
demande :
tu es déjà allé au Portugal ?
il secoue la tête .
prend la main posée sur son bras , la tourne , l' ouvre entièrement , la porte à ses lèvres , embrasse la paume .
ça chatouille , dit Marion .
il relève la tête .
il veut dire quelque chose , mais ne sait pas comment .
il abandonne , reprend son sac à dos , y plonge la main , en tire un carnet fermé par une bande élastique .
c' est un carnet à spirale , d' un bleu un peu passé .
il le dépose dans la main de Marion .
c' est quoi ?
il ne répond pas .
elle retourne le carnet , puis fait glisser la bande élastique , feuillette .
pages manuscrites .
deux encres , deux écritures différentes .
sur la page de titre , deux noms : David et Isabelle .
le père et la mère de Kev .
tu peux lire , dit Kev .
elle ouvre au hasard .
lit quelques mots : " aujourd'hui , j' ai dessiné ton portrait de mémoire , comme tu étais quand on a fait le tour en barque , en octobre ... " .
elle referme le carnet , interroge Kev du regard .
alors , il raconte : David et Isabelle se sont rencontrés quand ils avaient douze ans .
elle était venue voir une de ses tantes à Montrond .
ils n' ont jamais perdu contact .
ils se téléphonaient , s' écrivaient , se voyaient pendant les vacances .
après leur premier été , Isabelle a eu l' idée du carnet .
un carnet pour deux .
celui qui l' avait écrivait .
et quand ils se rencontraient , il le passait à l' autre .
qui lisait et écrivait à son tour .
ils ont continué jusqu'à ce qu' ils s' installent ensemble .
marion a passé son bras autour des épaules de Kev , posé sa tête sur son épaule .
elle ouvre le carnet à une autre page .
c' est l' écriture de ton père ?
lis moi , s' il te plait .
il lit un long passage .
et puis s' arrête .
marion referme le carnet .
tu as tout lu ?
demande t elle .
oui , avec Léa , quand mon père nous l' a donné , après la mort de maman ...
marion se redresse , étend ses jambes .
c' est ...
elle ne trouve pas le mot , alors elle se tait .
ils se sont jamais quittés , dit Kev sans la regarder .
machinalement , il ramasse un caillou , le fait sauter dans sa main .
ça veut dire que , pour eux , c' était sérieux dès le début .
c' est tout ce qu' il peut dire .
ça suffit .
marion lui prend la main , entrelace leurs doigts .
oui , dit elle .
et elle répète , simplement :
oui .