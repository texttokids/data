quand ils ont terminé , Clara leur propose de faire une pause .
j' aurais juste besoin de quelqu' un pour m' aider à préparer l' activité suivante .
moi !
dit Marion .
je peux rester aussi ?
demande Rose .
non , dit Marion .
c' est moi qui décide , intervient Clara .
adoucissant la voix , elle ajoute :
rose , c' est gentil , mais une personne me suffit .
rose sort , en refermant lentement la porte derrière elle .
Clara ouvre un placard , en tire une glacière , la pose sur une chaise et en sort des boites en plastique .
marion , pourrais tu présenter joliment tous ces aliments sur les assiettes en papier qui sont dans mon sac ?
tu prends aussi l' enveloppe rouge , s' il te plait .
marion se met au travail .
elle ouvre une boite , en tire des petits gâteaux qu' elle arrange sur une assiette vert pâle .
la boite suivante contient de la viande en gelée .
une autre du poisson mariné .
qu' est ce que c' est ?
demande t elle en versant dans une assiette une crème où flottent des morceaux de concombre et des brins d' aneth .
c' est justement le jeu , explique Clara en ouvrant l' enveloppe .
j' ai préparé les noms de ces recettes sur des étiquettes .
ce sera à vous de gouter et de trouver le nom de chacune .
il y a même le kouign amann que tu ne savais pas prononcer .
tu devines ce que c' est ?
marion inspecte les nourritures étalées sur la table .
ça sonne oriental , comme mot , indonésien ou un truc ça .
c' est peut être cette espèce de sauce à l' huile ...
Clara sourit .
on verra ...
tiens , voilà les étiquettes .
on les met où ?
il y un tableau magnétique , là .
on n' a qu' à les accrocher ...
c' est ce qu' elle fait .
il y a huit étiquettes en tout :
Tzatzíki , Rollmops , Jambalaya , Kouign amann , Potjevleesch , Brutti ma buoni , Leckerli , Baba Ganoush
marion se recule .
admire son oeuvre .
sans transition , elle demande :
comment tu as rencontré Simon ?
sur un marché .
on faisait la queue devant l' étal d' un producteur de légumes et , je ne sais pas pourquoi , on a commencé à parler .
de tomates , je me souviens .
c' est bête , non ?
il t' a plu tout de suite ?
oui .
je ne m' en suis pas rendue compte sur le moment mais , oui , je suis tombée amoureuse de lui dès que je l' ai vu .
et lui ?
Clara a un petit rire clair , joyeux .
non .
le premier jour , il m' a trouvée un peu prétentieuse , paraît il .
mais on s' est revus une semaine après , et là ...
elle fait un geste de la main , gracieux .
il est temps d' appeler les autres , tu ne crois pas ?
je suis curieuse de voir comment vous allez vous débrouiller ...
( pour découvrir les plats présentés par Clara , voir le site : diecilezionisullacucina.com )
interlude