Clara est de retour .
ils lui font une haie d' honneur quand elle sort de sa voiture et se dirige vers l' entrée de l' école .
Farid lui porte son panier , Noah son grand sac en toile .
arrivée à la bibliothèque , Clara va directement vers le panneau punaisé dans le fond .
tantôt s' approchant pour déchiffrer une écriture maladroite , tantôt reculant pour apprécier l' effet d' ensemble , elle lit , commente , questionne .
et puis elle ramène la petite troupe vers la table de réunion .
encore désolée pour hier , mais je vois que vous vous en êtes très bien sortis sans moi .
tu avais quoi ?
demande Noah .
Clara , les regarde , hésitante .
c' est à cause du bébé ?
demande Rose .
Clara sourit .
et soudain , oui , elle est radieuse .
tu as deviné ?
rose rosit .
décidément , ça lui va bien .
oui .
je me souviens quand maman attendait mes petits frères ...
mais alors , tu vas avoir un bébé ?
demande Pedro , interloqué .
avec Simon ?
Clara se mord les lèvres pour ne pas rire .
c' est un garçon ou une fille ?
demande Alice .
vous allez l' appeler comment ?
etc .
Clara doit élever la voix pour rétablir un peu d' ordre .
maintenant que vous êtes au courant , on en vient au thème d' aujourd'hui .
les empanadas , dit Pedro .
oui , mais pas seulement .
tu expliques d' abord ce que c' est , les empanadas , et d' où tu as la recette ...
Pedro se redresse sur sa chaise .
ben , ça vient du Pérou .
c' est là que je suis né .
mais je m' en souviens pas , parce que j' étais trop petit quand mes parents sont venus me chercher .
juste une précision , Pedro , dit Clara .
on trouve des empanadas partout en Amérique du Sud , et aussi en Espagne , mais je crois bien que ceux du Pérou sont les meilleurs ...
avec mes parents , on parle souvent du Pérou , j' ai même commencé à apprendre l' espagnol à la MJC .
un jour , sur le marché à Vesoul , on a vu une dame qui vendait des empanadas .
sur une petite affiche , c' était marqué que ça venait du Pérou .
on en a acheté et ma mère a cherché la recette .
depuis , on en fait souvent à la maison .
mais c' est quoi , les empanadas ?
demande Noah .
tu connais les chaussons aux pommes ?
répond Pedro .
c' est un peu pareil , sauf qu' à l' intérieur , c' est de la viande , du poisson , ou des légumes .
et c' est pas la même pâte , non plus .
en fait , explique Clara , les empanadas font partie de la Finger Food .
les trucs qu' on mange avec les doigts , traduit Hector .
exact .
comme les sandwichs , les pizzas , les tacos ...
les samoussas , les rouleaux de printemps , dit Alice .
les hamburgers , dit Noah .
les briks et les crostini , dit Farid .
les croque monsieur , dit Rose
et caetera , dit Clara .
alors le programme de ce matin : d' abord atelier empanadas avec Pedro .
mais on les mange pas tous , on en garde pour Riana !
elle arrive ce soir à cinq heures ...
bien sûr , Pedro .
ensuite , vous rechercherez des idées pour le pique nique d' aujourd'hui .
la règle , c' est : que des plats qui se mangent avec les doigts .
les autres groupes du centre aéré seront affamés , alors vous devez être créatifs et productifs !
on y va ?
trois recettes différentes d' empanadas et dix variétés de finger food .
Clara est satisfaite ( et épuisée ) .
les cuisiniers sont très contents d' eux ( et rassasiés à force de gouter ceci ou cela et encore ci et encore ça ) .
Alice et Rose ont une idée : écrire la liste des dix variations réalisées et demander aux invités du pique nique de deviner la recette correspondante .
génial , dit Clara , mais inventez des dénominations originales .
nominations ?
fait Noah .
ça veut dire quoi ?
le nom de la recette , idiot , dit Alice .
idiote toi même , répond Noah du tac au tac .
c' est le seul incident de la matinée .
voici la liste des plats présentés ce matin là :
" mini sandwich de la mer
Tramezzino rigolo
Samoussa de la basse cour
hamburger du jardinier
canapé des jours de pluie
Crostini de bonne humeur
Wrap qui plait à la vache et à son veau
croque bébé d' été
rouleau d' été ( sucré )
petit chou très chou ( sucré ) " ( cher lecteur , à ton tour , tu peux envoyer tes propositions de recette correspondant à ces dénominations au site : )
à la fin du pique nique , Clara a rassemblé le groupe .
pour fêter la fin de l' atelier , Sylvie et moi avons pensé organiser une fête dans le parc de la mairie pour tous les participants du centre aéré et leurs amis .
l' idée est simple : tous ceux qui veulent venir cuisiner apportent une recette et les ingrédients nécessaires .
il y aura des feux de cuisson , des frigos et des ustensiles de cuisine .
je rappelle à Kev et Noah qu' ils n' ont pas présenté leur recette ...
ah , j' ai oublié , dit Noah en mettant la main sur sa bouche .
Kev ne réagit pas .
si vous avez des questions ou des idées pour l' organisation , vous pouvez m' appeler sur mon portable , dit encore Clara .
à demain !
interlude