il était une fois , un petit pingouin tout blanc qui vivait sur la banquise avec ses parents .
un jour , il partit jouer avec ses copains et copines , mais , lorsqu' il arriva , il n' y avait personne .
plus tôt dans la matinée , les parents de pingouin avaient en fait préparé une fête d' anniversaire et avaient envoyé des cartes postales via monsieur le facteur pour inviter tous ses copains .
sur la carte postale , il était écrit : " venez à dix neuf heures trente pour l' anniversaire de Pingouin ! il y aura du gâteau ! M . et Madame Pingouin "
quelqu' un appela Pingouin qui arriva .
tout le monde sortir en criant : " joyeux anniversaire Pingouin ! "