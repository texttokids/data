boucle D' OR était une toute petite fille aux cheveux bouclés et dorés , qui habitait avec sa maman une maisonnette près du bois .
" boucle d' or " , lui avait dit sa maman , " ne t' en va jamais seule au bois . on ne sait pas ce qu' il peut arriver , dans les bois à une toute petite fille . "
un jour , comme elle se promenait au bord du bois , au bord seulement !
boucle d' or vit briller sous les arbres une jacinthe bleue .
elle fit trois pas dans le bois et la cueillit .
un peu plus loin , elle vit une jacinthe blanche , plus belle encore que la bleue .
elle dit trois pas et la cueillit .
et , un peu plus loin , elle vit tout un tapis de jacinthes bleues et de jacinthes blanches .
elle y courut et se mit à faire un gros bouquet .
mais quand elle voulut sortir du bois , tous les chemins étaient pareils .
elle en prit un au hasard et elle se perdit .
elle marcha longtemps , longtemps ...
à la fin , bien fatiguée , bien triste , elle allait se mettre à pleurer quand , soudain , elle aperçut à travers les arbres une très jolie maison .
toute contente , Boucle d' or reprit courage et alla vers la maison .
la fenêtre était grande ouverte .
boucle d' or regarda par la fenêtre .
et voici ce qu' elle vit :
l' une à côté de l' autre , bien rangées , elle vit trois tables : une grande table , une moyenne table et une toute petite table .
et trois chaises : devant la grande table , une grande chaise , devant la moyenne table , une moyenne chaise , devant la petite table , une toute petite chaise .
et sur chaque table , un bol de soupe : un grand bol sur la grande table , un moyen bol sur la moyenne table , un petit bol sur la toute petite table .
boucle d' or trouva la maison très jolie et très confortable , sentit la soupe qui sentait bon , et entra .
elle s' approcha de la grande table , mais , oh !
comme la grande table était haute !
elle s' approcha de la moyenne table mais la moyenne table était encore trop haute .
alors elle alla à la toute petite table , qui était tout à fait juste .
boucle d' or voulut s' asseoir sur la grande chaise , mais voilà : la grande chaise était trop large .
elle essaya la moyenne chaise , mais crac ...
la moyenne chaise n' avait pas l' air solide , enfin elle s' assit sur la toute petite chaise , et la toute petite chaise était tout à fait juste .
elle gouta à la soupe , mais , aïe !
comme la soupe du grand bol était brulante !
elle gouta la soupe du moyen bol , mais , pouah !
elle était trop salée .
enfin , elle gouta la soupe dans le tout petit bol , et elle était tout à fait à point .
boucle d' or avala jusqu'à la dernière goutte .
boucle d' or se leva , ouvrit une porte , et voici ce qu' elle vit :
elle vit , bien rangés , l' un à côté de l' autre , trois lits :
un grand lit , un moyen lit , et un tout petit lit .
elle essaya d' atteindre le grand lit , mais il était bien trop haut .
le moyen lit ?
peuh !
il était trop dur .
enfin , elle grimpa dans le tout petit lit et il était tout à fait juste .
et Boucle d' or se coucha et s' endormit .
cependant , les habitants de la jolie maison revinrent de promenade , et ils avaient très faim .
ils entrèrent , et voilà : c' était trois ours !
un grand ours , un moyen ours , et un tout petit ours .
et tout de suite , le grand ours cria , d' une grande voix :
" quelqu' un a touché à ma grande chaise ! "
et le moyen ours , d' une moyenne voix : " quelqu' un a dérangé ma moyenne chaise ! "
et le tout petit ours , d' une toute petite voix , dit : " quelqu' un s' est assis dans ma toute petite chaise ! "
puis le grand ours regarda son bol et dit : " quelqu' un a regardé ma soupe ! "
et le moyen ours : " quelqu' un a gouté à ma soupe ! "
et le tout petit ours regarda son bol et se mit à pleurer : " hi , hi , hi ! quelqu' un a mangé ma soupe ! "
très en colère , les trois ours se mirent à chercher partout .
le grand ours regarda son grand lit et dit : " quelqu' un a touché à mon grand lit ! "
et le moyen ours : " quelqu' un est monté sur mon moyen lit ! "
et le tout petit ours cria , de sa toute petite voix : " oh !
voyez !
il y a une toute petite fille dans mon tout petit lit .
à ce cri , Boucle d' or se réveilla et elle vit les trois ours devant elle .
d' un bond , elle sauta à bas du lit , et , d' un autre bond , par la fenêtre .
les trois ours , qui n' étaient pas de méchants ours , n' essayèrent pas de la rattraper .
mais le grand ours lui cria de sa grande voix : " voilà ce qui arrive quand on n' écoute pas sa maman ! "
et le moyen ours de sa moyenne voix : " tu as oublié ton bouquet de jacinthes , Boucle d' or ! "
et le petit ours , de sa toute petite voix eut la bonne idée de crier aussi : " prends le tout petit chemin à droite , petite fille , pour sortir du bois . "
boucle d' or prit le tout petit chemin à droite , et il conduisait bien vite hors du bois , et juste à côté de sa maison .
elle pense : " ce tout petit ours a été bien gentil . et pourtant , je lui ai mangé sa soupe ! "