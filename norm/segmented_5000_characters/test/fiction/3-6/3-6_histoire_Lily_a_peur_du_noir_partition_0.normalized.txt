ce week end , le papa et la maman de Lily ont décidé de l' emmener camper .
la petite fille est toute excitée .
arrivés dans la forêt , chacun a de quoi s' occuper : papa monte la tente , Maman et Lily ramassent du bois pour le feu .
une fois le campement préparé , Lily part se balader avec Papa et Maman .
mais il ne faut pas trop tarder car le soleil va bientôt se coucher .
quand la nuit tombe , tout le monde se met autour du feu que Papa a allumé .
la maman sort un sachet de guimauve et la met à griller .
mais Lily n' a pas l' air très rassurée ...
" que t' arrive t il ma Lily ? " demande la maman .
" Y a plein de bruits bizarres autour de nous , et comme il fait tout noir , on ne sait pas d' où ça vient " explique la fillette inquiète .
" tu n' as pas à avoir peur ma chérie , ce que tu entends ce sont juste des oiseaux qui vivent la nuit , le vent qui souffle ou encore les branches des arbres qui frémissent . tu entends les mêmes choses quand il fait jour , mais tu y fais moins attention car tu peux les voir . " Lily comprend mais a du mal à oublier tous ces bruits .
il se fait tard , il est temps d' aller dormir .
tout le monde se glisse dans son duvet , mais la fillette a toujours l' air effrayé .
son papa a une idée .
il allume sa lampe torche et commence à faire des ombres avec ses mains .
un petit chien , puis un lapin et une poule .
son Papa dessine tout plein d' animaux sur la toile de la tente .
" tu vois Lily " , dit t il , " c' est rigolo d' être dans le noir " .
la petite fille sourit , elle a l' air d' avoir oublié ses peurs et peut s' endormir paisiblement entre son papa et sa maman .