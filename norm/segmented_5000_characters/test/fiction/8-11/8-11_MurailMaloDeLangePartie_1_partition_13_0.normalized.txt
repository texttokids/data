avec l' automne revinrent les premières brumes , les premiers froids .
l' état de Craquelin empira .
il avait mal aux os .
il était devenu rêveur et il jouait silencieusement avec le petit pantin de bois .
il parlait encore de tante Briqueboeuf , il me racontait ce qu' il ferait avec elle quand elle serait là , ce qu' il lui dirait , ce qu' elle lui répondrait .
mais il ne me demandait plus jamais quand elle viendrait .
grâce à l' argent de SaintJust , j' avais pu payer une visite du docteur et les quelques remèdes qu' il avait prescrits .
la Bouillie veillait sur nous , elle volait de la nourriture à la mère Gargoton et nous avions en quantité du bouillon et des arlequins .
janvier y ajoutait du vin et se faisait gronder par la servante du Lapin Volant .
pourtant , tous deux s' entendaient très bien , et Janvier entravait l' arguche comme vous à présent .
un matin d' octobre , La Bouillie m' apprit une nouvelle à laquelle j' aurais dû m' attendre : les Bonnechose étaient de retour dans leur hôtel parisien , au sept rue de la Tour desDames .
la saison des bals et des spectacles allait commencer .
Y a pas eu de chopin chez les Bonnechose , me dit aussi La Bouillie .
on t' a fait des batteries , frangin .
le cambriolage n' avait pas eu lieu , parce que la police était intervenue à temps comme chez les Desfontaine .
Roland et le cocher n' avaient pas été assassinés par SaintJust .
ils étaient en prison , là où se trouvaient aussi Lamproie et Bobino .
je souffrais de savoir que Léonie de Bonnechose était à Paris et qu' il ne m' était pas possible de la revoir .
et pourquoi tu ne la reverrais pas ?
me demanda cet idiot de Bourguignon .
tu as vu comment je suis habillé ?
m' écriai je , furieux .
tu crois qu' on vous reçoit dans les beaux quartiers avec un pantalon troué et une casquette d' ouvrier ?
et votre robe , monsieur Malo ?
je me tournai vers Craquelin , encore plus furieux .
il était sur le lit en train de tirer sur les ficelles du pantin , le désarticulant comme lui même se disloquait jadis .
je compris soudain pourquoi mon pantin était son jouet préféré et ma colère s' envola .
de quoi parles tu , de la robe de mademoiselle Augustine ?
elle est jolie et elle vous allait très bien .
bourguignon ricana et je fis tomber ma mauvaise humeur sur lui :
et alors ?
tout le monde n' est pas un gros garçon comme toi !
j' avais jeté la robe et le chapeau dans le bas de l' armoire et ils me parurent faire pauvre figure quand je les mis sous les yeux de La Bouillie .
Toinou va t' arranger ça , frangin .
c' était la jeune lingère .
non seulement Toinou lava , recousit , repassa la robe , mais elle ajouta des rubans et des fleurs au chapeau .
quand j' enfilai de nouveau la robe , je m' aperçus que j' avais grandi .
elle était juste à ma taille et je peux dire qu' elle m' allait à ravir .
mes boucles blondes et mes yeux bleus sous la capote étaient aussi du plus bel effet .
je pense que je vais m' appeler Hortense Saint Just .
ah ?
fit simplement Janvier .
tout le monde avait bien compris que mon déguisement n' admettait pas la plaisanterie .
la Bouillie connaissait toujours quelqu' un qui connaissait quelqu' un , ce qui lui donnait ses entrées partout .
la cousine de Toinou était la filleule de la femme de chambre des Bonnechose .
par elle , nous apprîmes qu' on allait fêter les treize ans de Léonie au sept rue de la Tour des Dames .
il y aurait quantité d' invités et notamment de petites demoiselles .
une de plus ne se remarquerait pas .
le jour dit , et c' était un onze décembre , un jeune ramoneur fut introduit chez les Bonnechose par la femme de chambre qui le cacha dans la buanderie .
là , je sortis de mon sac à suie la robe et le chapeau , et j' attendis que la soirée débutât en claquant un peu des dents .
quelques coups frappés à la porte de la buanderie m' avertirent enfin que le moment était venu de me joindre à la fête .
les salons des Bonnechose ressemblaient aux descriptions que Léonie m' en avait faites , les musiciens sur l' estrade , les valseurs glissant sur le parquet , les miroirs , les fleurs , les laquais , et je me sentis tout de suite à mon aise .