il en a pris pour dix ans .
à présent , Riflard était libre , et j' aurais pu certifier à monsieur Personne que le bagne ne l' avait pas amélioré .
si Riflard , me dit il en désignant sa loupe , est le complice de Pigrièche ...
il posa la loupe près de l' encrier puis prit un crayon :
et que Riflard est allé voir Grip' sou ( c' était le crayon ) , cela peut vouloir dire que Riflard veut écouler le trésor de Sainte Ursule grâce à Grip' sou .
la conclusion était qu' en surveillant nuit et jour le crayon on tomberait sur la loupe .
et sur le trésor .
ce mardi , comme tous les mardis , le chef de la Sûreté me remit mes trente francs .
je finissais par être encombré de tant d' argent .
en revenant vers mon logis , je croisai une petite marchande de fleurs , de timides fleurs printanières .
est ce que la petite marchande me fit pitié ?
je lui achetai un de ses bouquets mal ficelés , puis je me demandai à qui je l' offrirais .
ma tante en avait déjà reçu un la semaine passée , La Bouillie me rirait au nez , Bourguignon était plus habitué à mes coups de pied .
je ne voyais qu' une solution .
Léonie .
cela faisait bientôt un an que mademoiselle de Bonnechose et moi nous étions parlés pour la dernière fois .
depuis , j' avais appris qui j' étais .
en arrivant devant le sept rue de la Tour des Dames , je me posai la question de monsieur Personne : " as tu honte , Malo de Lange ? " à la face du monde , j' aurais crié : non !
mais en face de mon miroir ?
et en face de Léonie ?
je levai la tête vers la fenêtre de sa chambre au deuxième étage et je m' aperçus qu' elle était ouverte .
alors , je mis le bouquet entre mes dents et , profitant que la rue au crépuscule était déserte , j' escaladai la façade en m' aidant de la gouttière .
Léonie ne me vit pas entrer dans sa chambre .
elle me tournait le dos , assise à son secrétaire .
elle écrivait .
une pensée affreuse me traversa : elle écrivait au boutonneux !
la force me manqua .
je posai doucement le bouquet sur le lit et je m' apprêtai à repartir quand elle se retourna .
j' avais l' agilité d' un chat , mais elle en avait l' ouie .
Malo !
je joignis les mains sans prononcer un mot .
que viens tu faire ?
comment oses tu ...
je lui désignai mon bouquet sur le lit et les yeux de Léonie , étincelants de colère , se brouillèrent de larmes .
mais pourquoi , Malo , pourquoi ?
un jour , elle m' avait donné trois pièces d' or et je n' étais plus jamais revenu .
qu' avait elle pu penser de moi , sinon que j' avais profité de sa générosité ?
Léonie , j' ai questionné le cocher .
je sais qui je suis ...
je ne suis pas le fils d' un duc .
je suis le fils de Bébert la Banquette , un voleur , et de Madelonnette , une prostituée .
elle me sourit .
mais je le savais .
j' ai toujours su que tu es un bandit avec un couteau .
je n' ai pas tout dit , Léonie .
ma mère m' a abandonné .
mais avant ...
ma voix se nouait .
mes jambes tremblaient sous moi .
il y a quelqu' un , peut être mon père , quelqu' un qui m' a marqué au fer rouge .
je ne sais pas pourquoi .
marqué au fer rouge ?
répéta Léonie sans paraitre comprendre .
marqué de la fleur de lys .
comme un bagnard .
et tu as cette marque sur ton épaule ?
la droite .
je peux voir ?
voir ?
je rougis , était ce de honte ?
les mains tremblantes , j' ôtai ma veste et la jetai sur le lit près du bouquet .
puis je me tournai vers le mur et j' ouvris ma chemise en arrachant presque les boutons tant j' étais pressé d' en finir .
enfin , en étouffant un soupir , je l' abaissai jusqu'à mes reins .
Léonie s' approcha de moi dans un froissement de soie .
je me souvins alors des cris d' horreur de Mariette chaque fois que je prenais mon bain .
qu' est ce que Léonie pouvait bien penser ?
je tressaillis en sentant le bout de son doigt qui dessinait les contours de ma fleur de lys .
puis ses lèvres s' y posèrent , légères et fraiches comme un flocon de neige .
Léonie , Léonie ...
me retournant , je l' étreignis , et je demanderai à mes lectrices de bien vouloir regarder par la fenêtre restée ouverte , car je l' embrassai comme je n' avais encore jamais embrassé personne .
je revins au Lapin Volant à la nuit tombée , et quand je passai rue François Miron , je vis la fenêtre de Janvier faiblement éclairée .
nous n' étions pas fâchés , lui et moi , mais nous prétendions être trop occupés pour nous voir .
ce soir là , j' avais besoin de parler avec quelqu' un , de dire que j' étais amoureux et que Léonie était la plus jolie fille de Paris , et la plus libre d' esprit .
François parut heureux de m' apercevoir sur son palier .
il avait les cheveux en bataille et de l' encre sur la joue .
je vous dérange en pleine écriture ?
aucune importance !
mon cher Malo , j' ai précisément besoin de vous parler .
ah ?
fis je , ennuyé d' avance .
je veux faire un cadeau à une jeune fille ...
un chapeau ?
heu , non , se troubla un peu Janvier .
une bague .
il fouilla dans un tiroir , tout en me parlant de topaze et d' améthyste .
quand on aime , dis je , des fleurs suffisent .
justement , j' ai acheté ...
ah , la voilà !
s' exclama Janvier en brandissant une petite boite .