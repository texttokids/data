p' t être ...
mais moi , j' ai pensé à ce qu' elle était , la Madelonnette , avant de tomber sur Bébert .
c' était une fille honnête qui venait de Saint Malo ...
nom de Dieu , jura Picpoc .
c' était l' histoire que m' avait racontée Personne , mais plus tout à fait la même , et mon coeur était à la torture .
alors , moi , continua Riflard , je me suis dit que l' air de la mer me ferait du bien , et me v' là à Saint Malo avec la Riflarde , et on se renseigne , on cherche une Madelonnette qui serait revenue au pays ...
et tu l' as retrouvée ?
ben , pour faire court , oui .
je l' ai retrouvée .
elle était mariée avec un bonnetier , elle avait grossi , elle allait z' à la messe , mais c' était elle .
elle a pas été aussi contente de me voir que moi .
je l' ai un petit peu ...
comment on va dire ça ...
secouée ?
et elle m' a raconté la fin de l' histoire .
le bébé qu' était mort à Saint Lazare , c' était le sien .
l' autre , elle l' avait abandonné à l' hospice des Ursulines et elle savait pas ce qu' il était devenu .
mais y avait moyen de le reconnaitre .
c' était Bébert qu' avait eu l' idée parce qu' il confondait les deux petits .
il savait plus lequel c' était le sien , lequel c' était l' autre , surtout quand il avait bu un coup de trop .
et un jour qu' il avait vraiment bu un coup de trop , il a fait une marque sur le petit Julien .
une marque ?
le père à Bébert , il faisait un pas joli métier .
il était bourreau .
comme souvenir de famille , il avait laissé à son fils le fer avec la fleur de lys .
et Bébert a marqué le petit .
de la fleur de lys ?
le petit ?
riflard partit d' un rire sauvage en hoquetant :
hein , si c' est pas drôle , le fils de Personne , marqué de la fleur de lys !
mais c' est pas ça , la fin de l' histoire !
se récria Picpoc .
ce Malo que tu cherches ...
le Malo que je cherche ?
julien Donnadieu , le fils de Personne ?
mais je l' ai trouvé !
j' entendis alors des pas qui se rapprochaient et je dus me mordre les doigts pour ne pas hurler .
je pensais : " non , non , il ne peut pas savoir que je suis là . " mais le couvercle s' ouvrit .
et Riflard , penchant au dessus de moi son ignoble figure , me fit un sourire de travers :
tu m' as roulé , une fois , Malo .
mais t' aimes trop les coffres .
il m' attrapa brutalement par la veste pour me forcer à me relever .
alors , le trésor était dans le coffre , hein ?
tu l' as cru ?
mais t' es aussi sinve que ce Bourguignon qui croit nous espionner !
allez , debout , sors de là !
je ne tenais pas sur mes jambes , elles se dérobaient sous moi .
mais , à la force des bras , il me souleva hors du coffre et me jeta à terre .
il est armé !
prévint Picpoc .
ils m' ôtèrent le couteau que je gardais le long de mon mollet .
regarde moi ça , dit Riflard en me désignant à Picpoc .
il pisserait dans sa culotte tellement qu' il a peur .
il cracha sur moi .
ah , il fait pas honneur à son père !
je me demande si Personne se doute pas de quelque chose , remarqua Picpoc .
le Malo , c' est devenu son favori à la brigade .
trente francs par semaine et pour rien faire ...
bon , tu l' as retrouvé , mais ça te donne quoi ?
ça nous donne quoi , rectifia Riflard .
oh , moi , ça me regarde pas , tes histoires de rebif .
je l' aime pas , ce momacque .
mais c' est tout .
ah non , c' est pas tout .
toi et moi , on est comme qui dirait des complices .
oui , c' est ça , des complices .
sûr que non , gronda Picpoc .
c' est pas toi qui m' a z' ouvert la porte avec ton passe de la Sûreté ?
je regardais les deux hommes en train de s' affronter et je reprenais courage .
tu m' as dit : " viens au Lapin , j' ai des choses à te raconter sur le Malo de ton patron . " mais c' est tout , s' obstina Picpoc .
tu t' es servi de ton passe , et c' est pas la seule fois que tu t' en sers pour entrer dans les maisons la nuit .
ferme ça !
c' est toi qui ouvres , plaisanta Riflard .
et le Bobino qu' est en prison , il pourrait dire deux trois choses sur toi au curieux ...
si je lui demandais de le faire .
c' était ainsi que Riflard obligeait les autres à devenir ses complices : par le chantage .
il expliqua à Picpoc ce qu' il attendait de lui .
cette nuit ci , Personne planquait en face du taudis de Grip' sou .
or , le trésor de Sainte Ursule y était , caché au milieu des ordures .
les crucifix en or avaient déjà été fondus , les pierres précieuses avaient été descellées , mais Riflard ne pouvait rien récupérer , car Personne avait instauré un tour de garde avec ses agents pour surveiller les allées et venues .
alors , toi , camarade , tu vas aller raconter au patron que le trésor est au grenier du Lapin Volant , dans un coffre , que t' as entendu des grinches dire qu' on allait le déménager bientôt , et qu' il y a le petit Malo qui monte la garde dessus .
si le trésor , ça le fait pas bouger , le Malo , ça le fera courir , parce que t' as raison , il se doute de quelque chose .
et puis ?
quand il arrive ici , tu le chourines ?
moi tout seul contre le patron ?
tu rigoles !
pour tuer Personne , faut être à plusieurs .
tu vas revenir avec lui par la rue Miron et là , tu donnes deux petits coups de sifflet .
et tu verras , y aura deux zigs qu' auront l' air de sortir des murs .