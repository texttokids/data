la brigade de Sûreté de Paris devrait vous fournir une quantité de magnifiques bosses à étudier .
tenez , commencez donc par ce jeune garçon !
nous l' appelons le pégriot , le petit voleur en argot .
ou bien par moi même ...
vous ?
je ne sais pas si j' avais la bosse du volume Mais j' ai volé .
et quand on m' a enfermé , je me suis enfui .
j' avais sans doute aussi la bosse de la liberté .
le professeur Gavater s' était levé et il faisait des petits saluts comme quelqu' un souhaitant s' en aller sans trop savoir comment s' y prendre .
bonne journée , professeur Gavater , le congédia Personne .
mes amitiés à monsieur le Préfet .
quand la porte se fut refermée , le chef de la Sûreté me désigna à nouveau la chaise .
maintenant que tu sais qui je suis , dis moi qui tu es .
je ne suis rien , monsieur Personne .
il me regarda en plissant les yeux , puis fit semblant de m' étudier à travers sa loupe .
qui était ton père ?
Bébert la Banquette .
cela fait des années qu' il est mort au cours d' une rixe au couteau .
qui t' a élevé ?
j' étais à l' hospice , l' hospice des Ursulines à Tours .
mais je me suis enfui l' an dernier .
ce que je disais semblait toujours tourmenter Personne .
devinait il que je mentais ?
il passa la main sur ses yeux et murmura :
va t' en .
puis dans un cri , il répéta :
va t' en !
je ne me le fis pas dire trois fois et je me retrouvai vite fait dans la galerie Véro Dodat .
ce monsieur Personne , moitié cogne et moitié grinche , m' effrayait plus que jamais .
mais j' étais content d' appartenir à ce qu' il appelait la brigade de Sûreté , même si je n' avais rien demandé .
j' avançais si distraitement que je n' aperçus Janvier qu' une fois sous mon nez .
il était lui même absorbé dans la contemplation d' une vitrine de chapelier .
François ?
il eut un sursaut comme un voleur pris sur le fait .
oh !
vous m' avez fait peur .
je rêvais .
devant des chapeaux ?
il eut un rire gêné et m' en montra un , très mignon ( et je suis connaisseur ) avec un petit oiseau perché .
pour vous ?
pour La Bouillie .
croyez vous que cela lui ferait plaisir ?
je crois , oui , surtout qu' elle n' a plus de poux .
janvier me jeta un regard de côté .
il n' appréciait pas toujours mes remarques .
il faudrait peut être commencer par lui offrir des chaussures , reprit il .
les siennes sont dépareillées .
et ce sont deux pieds droits .
janvier soupira .
dites moi , Malo , savez vous si La Bouillie porte un autre nom ?
qu' est ce qu' elle en ferait ?
vous êtes désespérant .
je sentis qu' il allait renoncer au chapeau .
achetez le , François , mais avant de le lui offrir , laissez moi parler avec La Bouillie .
parler de quoi ?
s' inquiéta Janvier .
de vous .
nous nous regardâmes , puis François fit un lent signe d' acquiescement .
et nous entrâmes dans la boutique du chapelier .
ce soir là , j' attendis la fermeture du Lapin Volant pour aller rejoindre La Bouillie dans la cuisine .
c' était là qu' elle dormait , sous une table , dans les odeurs de graisse et de bouillon .
ça va ti comme tu veux , frangin ?
très bien .
tu canes la pégrenne ?
( tu as très faim ?

non , ça va ...
la Bouillie , je voulais te demander : qu' est ce que tu penses de Janvier ?
un sourire s' épanouit sur la bonne bouille de mon amie .
c' T un type !
tu as remarqué qu' il n' envoie plus de lettres à Lucie Desfontaine ?
tiens , c' est vrai , ça , fit elle semblant de s' étonner .
elle avait rougi .
peut être qu' il ne l' aime plus ?
ajoutai je .
Possib' , frangin .
peut être qu' il en aime une autre ?
j' suis fatiguée .
bonne sorgue !
( bonne nuit !

peut être que c' est toi qu' il aime ?
et peut être qu' il est fou ?
la Bouillie , si tu te lavais , si tu essayais d' être un peu présentable ...
j' serais toujours La Bouillie .
tu n' as pas d' autre nom ?
si .
lequel ?
virginie .
pourquoi tu ne l' as jamais dit ?
on me l' a jamais demandé , frangin .
deux grosses larmes roulèrent dans ses yeux , de jolis yeux noisette .
bonne sorgue , Virginie !
je la laissai pour me rendre rue François Miron .
janvier écrivait sa prochaine pièce de théâtre , Les grinches , à la lueur vacillante d' une bougie .
alors ?
fit il en reposant sa plume .
elle est mure pour un chapeau avec un petit oiseau .
et elle s' appelle Virginie .
janvier me tendit la main :
merci ...
frangin .