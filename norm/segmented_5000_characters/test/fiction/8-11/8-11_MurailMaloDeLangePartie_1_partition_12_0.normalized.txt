nous formions maintenant une vraie bande , Craquelin , La Bouillie , Bourguignon , Janvier et moi , pas une bande de grinches , une bande de bons zigs , comme disait La Bouillie .
grâce à l' argent de Saint Just , j' avais aussi acheté des béquilles à Craquelin et il s' y suspendait pour faire le chemin de sa chambre à la cour .
j' avais mis un banc contre le mur et le petit s' y adossait .
nous apportions alors nos chaises , et La Bouillie se faisait professeur d' arguche .
elle même savait à peine lire , et comme Janvier s' étonnait de ce qu' elle ait pu rédiger l' avis de recherche , elle lui avoua en rougissant qu' elle avait payé un écolier pour écrire sous sa dictée .
janvier , en rougissant à son tour , lui proposa d' échanger les leçons d' arguche contre des leçons de français .
et ainsi fut fait .
craquelin et Bourguignon , qui savaient tout juste ânonner , suivirent aussi les leçons .
pendant ce temps , je sortais mon chourin de ma poche et je taillais dans des bouts de bois des sifflets et des petits bateaux pour amuser Craquelin .
janvier finit par raconter ses peines de coeur à La Bouillie , que ce genre d' histoire intéressait plus que moi .
elle trouva même une ruse pour lui permettre d' échanger du courrier avec mademoiselle Lucie .
la Bouillie connaissait la jeune lingère qui lavait , raccommodait et repassait les chemises et les jupons des Desfontaine .
désormais , c' était La Bouillie qui les rapportait dans un grand panier au fond duquel la bonne de mademoiselle Lucie trouvait une lettre de Janvier .
la réponse de Lucie partait sous le linge sale , et ainsi de suite .
mais , s' il voulait un jour épouser mademoiselle Desfontaine , Janvier devrait se trouver une situation .
il avait d' abord tenté de faire publier ses poésies , mais les éditeurs lui avaient claqué la porte au nez .
il s' était donc tourné vers le journalisme et avait écrit quelques articles sur des comédies qu' il avait vues .
puis à force de regarder des pièces , il eut l' idée d' en écrire une , Papa ne veut pas , où il faisait rire les spectateurs de ses propres malheurs .
dès que le directeur du théâtre lui donna l' argent des premières représentations , il alla le boire puis rentra chez lui en chantant " poule en haut , poule en bas " .
un soir , sans crier gare , Jean Saint Just entra dans ma chambre avec son passe partout .
craquelin dormait déjà , un pantin de bois de ma fabrication serré contre son coeur .
sur un signe que me fit Saint Just , je le rejoignis au dehors .
j' ai parlé de toi à un grinche pour un vol à la venterne ...
tu sais ce que c' est ?
par la fenêtre ?
saint just acquiesça puis me donna quelques précisions .
on allait cambrioler un premier étage d' accès facile par la cour .
les propriétaires , un vieil homme et sa fille , avaient le sommeil lourd .
j' ai dit à Bobino ( c' est le nom du grinche ) que tu étais un ramoneur savoyard et que tu n' avais pas ton pareil pour l' escalade .
il t' attend au troisième étage du Lapin .
maintenant ?
demandai je , pris de panique .
troisième étage , répéta seulement Saint Just .
quelque chose de plus fort que moi décidait de ma vie : j' étais un grinche , c' était mon destin .
sur la pointe des pieds , je grimpai l' escalier en colimaçon du Lapin Volant .
au premier étage , un nuage de fumée flottait déjà au dessus des buveurs .
au deuxième étage , une partie de cartes était engagée entre quatre joueurs .
j' eus le temps de reconnaitre de profil un des joueurs avant qu' il ne m' aperçût .
je dégringolai les marches jusqu' en bas , terrorisé .
c' était Riflard !
il me semblait aussi avoir reconnu Picpoc qui jouait aux cartes contre lui .
je retournai dans la cour où Saint Just faisait les cent pas .
tu as déjà vu Bobino ?
s' étonna t il .
non ...
ne vous fâchez pas !
je n' ai pas pu parce qu' au deuxième il y a un joueur de cartes qui s' appelle Riflard , et j' ai eu peur .
c' est un ...
un mauvais homme .
le visage de Saint Just se crispa si fort que ses favoris roux commencèrent à se décoller .
riflard est au Lapin Volant !
s' exclama t il .
il a fini son temps au bagne , alors ?
il me regarda avec une lueur sauvage dans les yeux :
et pourquoi dis tu que c' est un " mauvais homme " ?
parce qu' il fait partie de la bande du cocher et de Roland .
comment le sais tu ?
tu as des preuves ?
non , non , non , fis je en reculant dans l' ombre .
alors , tu accuses au hasard !
s' emporta Saint Just .
je crus qu' il allait me secouer par le collet comme il l' avait déjà fait .
au lieu de cela , il prit une inspiration et fit apparaître une pièce d' or au bout de ses doigts :
tiens , voilà pour toi .
je te ferai rencontrer Bobino une autre fois .
l' autre fois ne se fit pas attendre .
dès le lendemain , Saint Just vint me chercher dans ma chambre .
il m' empoigna sans me laisser le temps de réveiller Craquelin et , dans la cour , il me tendit le plan de l' appartement que nous allions cambrioler :
tu passeras par la fenêtre de la cuisine .
par ces temps de chaleur , la cuisinière la laisse ouverte .
tu remonteras le couloir jusqu'à l' entrée et tu nous ouvriras .