Le petit sapin a été choisi par une jeune dame pour trôner dans son salon cet hiver .
il attend impatiemment sa rencontre avec le père Noël !
enfin , ce fut la nuit de Noël , après une agréable soirée entourée de plusieurs convives , la jeune femme alla se coucher , sans oublier de déposer sa paire de souliers sous le sapin .
un silence absolu régna alors dans la maison , les minutes , les heures passaient , sans que rien ne vienne troubler le calme de la nuit .
pendant ce temps , petit sapin commençait à s' impatienter .
" mais où est le Père Noël ? quand viendra T il ? " s' énervait il .
" et s' il ne venait pas ! si pour une raison que j' ignore il passe son chemin ? après tout , maintenant que j' y pense , il n' y a pas de cheminée dans cette pièce ! " perdu dans ses pensées , il ne s' aperçut pas comment le Père Noël rentra , cependant , il le vit tout à coup , assis prés de lui .
" alors mon petit , tu n' as pas confiance en mon pouvoir ? saches , que rien , ni personne ne m' empêchera de me rendre où je désire aller ! pas même l' absence d' une cheminée ! "
petit sapin fut très impressionné par ce vieil homme à la barbe blanche , il parlait d' une voix forte et claire , mais ce qui l' étonna beaucoup , c' est qu' il comprenait le langage des arbres .
ils discutèrent ainsi une bonne partie de la nuit .
avant de le quitter , Père Noël lui dit de ne pas s' inquiéter , car la jeune fille qui s' occupait de lui était très gentille , et que bientôt , lorsque les fêtes seront terminées , elle lui trouvera un bel endroit dans son jardin .
c' est la première fois que je vois un si beau et si petit Sapin de Noël !
lui dit il en lui caressant les branches .
" au moment où il le toucha , un frisson étrange traversa , petit sapin qui frémit de la cime jusqu'à la pointe de ses aiguilles . puis , le Père Noël ajouta . " tu es si jeune que tu mérites bien un cadeau !
il t' apparaîtra dans quelques jours seulement , et tu seras alors comblé de bonheur , car je sais ce que tu désires au plus profond de ton coeur !

petit Sapin fut troublé par les derniers mots du Père Noël .
les fêtes s' achevèrent et plus personne ne venait voir la jeune femme , qui d' ailleurs partait toute la journée au lycée .
il voulait à présent voir le ciel , observer les nuages , sentir la chaleur du soleil sur son feuillage , et entendre les chants des oiseaux .
un matin , alors que dehors le soleil brillait , la jeune femme se hissa sur la pointe des pieds et en le saisissant s' exclama .
" mon beau Sapin de Noël , il est tant maintenant de te rendre ta liberté et c' est le jour idéal pour te replanter ! "
elle entreprit aussitôt , de lui ôter ses décorations .
" mais que fait elle ? elle tire sur mes noeuds colorés , elle détache mes merveilleux rubans ! je ne veux pas que l' on m' enlève mes belles guirlandes , tout ceci est à moi ! " mais rien ne se détachait , tout restait à sa place comme si on les avait collés !
point petit sapin à travers ses larmes , comprit les difficultés qu' elle avait , elle était dans l' impossibilité de retirer la moindre guirlande .
" le voilà ! s' écria T il , le voilà mon cadeau ! "
la jeune femme finit par abandonner , en pensant qu' il était plutôt mignon ce petit sapin avec toutes ses couleurs , et qu' il embellirait parfaitement son jardin .
elle le planta donc , dans un grand trou , et fit très attention de ne pas lui casser ses aiguilles car il était si fragile !
" bien fit elle , une fois sa tâche terminée , je crois que tu te plairas bien ici ! "
de nombreuses années passèrent , et petit sapin poussa incroyablement .
ses branches s' allongèrent et se multiplièrent , il devint alors un arbre immense , impressionnant !
et sur ses nouvelles branches , de gros noeuds colorés apparurent , ses guirlandes scintillantes se rallongèrent au fur et à mesure qu' il grandissait , glissant sur ses aiguilles , elles serpentèrent entre ses branches , le couvrant d' étincelles multicolores .
il prit ainsi l' apparence d' un fantastique Sapin de Noël .
à présent la jeune femme a beaucoup vieilli , mais vient très souvent avec ses enfants et ses petits enfants admirer son arbre prodigieux .
et pour lui , la Nuit de Noël reste une nuit très spéciale !
en petits flocons légers , la neige commence doucement à tomber .
ses belles décorations aussitôt s' illuminent et prennent l' apparence d' un magnifique costume de gala .
son étoile devient si luisante que l' on pourrait croire , qu' un astre venu du fin fond de la voie lactée , se serait posé sur sa cime , telle une tête couronnée .
et comme tous les enfants du monde , il attend , impatient , l' heure solennelle où Papa Noël descend du ciel !
