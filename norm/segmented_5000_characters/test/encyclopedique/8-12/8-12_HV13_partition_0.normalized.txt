quand Vidocq entre dans la police , en dix huit cent dix , les rues de Paris sont de vrais coupe gorge .
mais il connait cet univers pour l' avoir fréquenté .
cela fera de lui un policier pas comme les autres !
la police se modernise Après Vidocq , le criminologue Alphonse Bertillon ( dix huit cent cinquante trois dix neuf cent quatorze ) a développé des techniques scientifiques qui ont fait progresser police .
dix huit cent quatre vingt deux : identification des criminels par leurs mensurations ( mesures de la tête , de l' oreille droite ...

on commence à recueillir les empreintes digitales .
dix huit cent quatre vingt huit : les photographies de face et de profil se développent .
autre avancée , au XXe siècle cette fois ci , l' analyse de l' empreinte génétique par l' ADN ( acide désoxyribonucléique ) , unique pour chaque individu .
au début du XIXe siècle , avec l' essor de l' industrie , les paysans partent travailler en ville .
la vie y est dure et la misère favorise les crimes et les délits .
on dénombre alors à Paris vingt cent zéro voleurs et autant d' anciens bagnards .
après un an de collaboration avec la police , Vidocq obtient carte blanche pour créer son service , la brigade de sûreté , et le gérer à sa manière .
il va donc recruter d' anciens forçats qui ont connu " de l' intérieur " le monde du crime .
mais , attention , Vidocq affiche un règlement dans ses bureaux : interdiction de boire de l' alcool à l' excès , de jouer aux jeux d' argent ...
ses succès lui permettront d' élargir son service : en un an , son équipe de douze agents procède
à huit cents arrestations , dont cinq meurtriers et cent huit cambrioleurs .
à son départ , en dix huit cent vingt sept , la brigade de sûreté compte presque trente agents !
Vidocq crée un solide réseau d' indicateurs , des personnes qui le renseignent contre de l' argent .
déguisements , filatures , infiltrations des milieux louches , les méthodes de la " rousse " ( le surnom de la bande de Vidocq ) font mouche .
pour les historiens , Vidocq a créé les bases de la police moderne !