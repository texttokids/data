quatre buts , il est désigné " meilleur jeune joueur " de la compétition par la FIFA .
à l' âge de dix neuf ans et deux cent sept jours , il devient le troisième plus jeune champion du monde de l' histoire , derrière le Brésilien Pelé en mille neuf cent cinquante huit ( dix sept ans , huit mois et six jours ) et l' Italien Giuseppe Bergomi en mille neuf cent quatre vingt deux ( dix huit ans , six mois et dix sept jours ) .
il réussit trente deux dribbles dans cette Coupe du monde , un record pour un joueur français .
il est par ailleurs adoubé par Pelé en personne , qui tweete le quinze juillet " si Kylian continue d' égaler mes records comme ça , je vais devoir dépoussiérer mes crampons " , à quoi Mbappé répond : " le Roi restera toujours le Roi " .
par ailleurs , Miroslav Klose , recordman du nombre de buts marqués en Coupe du monde ( seize , en quatre participations entre deux mille deux et deux mille quatorze ) est persuadé que Kylian Mbappé peut égaler ou améliorer son record : " pour y parvenir , il faudra que la France reste compétitive sur la durée . à l' heure actuelle , il y a de quoi être optimiste " , dit il .
le onze octobre deux mille dix huit, en inscrivant son dixième but en sélection face à l' Islande ( deux deux ) , Kylian Mbappé devient le premier joueur de l' équipe de France à passer la barre des dix buts en équipe de France avant l' âge de vingt ans .
lors des matchs de qualification à l' Euro deux mille vingt , il se distingue en marquant deux fois en deux matchs , rencontres au cours desquelles il est titulaire .
sa première réalisation intervient en fin de rencontre face à la modeste équipe de Moldavie , qui permet à l' équipe de France de l' emporter quatre un .
enfin , au stade de France , il participe également à la large victoire des Bleus avec un but face à l' Islande ( quatre zéro ) .
il s' agit de son douzième but en trente sélections , un total qu' il est le premier à atteindre en équipe
lors de la reprise de ces éliminatoires de l' Euro deux mille vingt , très critiqué après la défaite des Bleus en Turquie ( zéro deux ) , Kylian Mbappé se reprend quatre jours plus tard , le onze juin , et ouvre le score face à l' Andorre but de sa carrière professionnelle ( vingt sept avec l' AS Monaco , soixante avec le PSG , treize en équipe de France ) , mieux que Lionel Messi et
Kylian Mbappé peut jouer à tous les postes de l' attaque ( gauche , droite , pointe ou soutien ) alliant vitesse et technicité .
il sait particulièrement bien exploiter l' espace dans le dos des adversaires
défenseurs , mais aussi grâce à la qualité technique de ses dribbles , y compris en sprintant .
capable d' accélérations fulgurantes , avec une pointe de vitesse à trente huit kilomètres par heure , il est l' un des joueurs les plus rapides du monde et même le plus rapide balle aux pieds selon certaines sources .
il est régulièrement remarqué pour la qualité de ses gestes techniques .
c' est un attaquant qui participe très activement au travail défensif de récupération de la
Ronaldo ou encore , lors de la Coupe du monde deux mille dix huit , au " roi Pelé " , notamment pour les records de précocité qui le rapprochent de ce dernier .
en deux mille dix neuf , sa tendance à se replacer de plus en plus comme un attaquant axial au PSG comme en équipe de France , lui vaut notamment d' être comparé à l' avant centre emblématique du club
caption : statistiques de Kylian Mbappé au onze mars deux mille vingt
saison Club Championnat Coupe nationale Coupe de la Ligue Supercoupe
division M B Pd M B Pd M B Pd M B Pd C M B Pd M B Pd deux mille quinze deux mille seize Drapeau de la France AS Monaco Ligue un onze
deux mille seize deux mille dix sept drapeau de la France AS Monaco Ligue un vingt neuf
deux mille dix sept deux mille dix huit drapeau de la France AS Monaco Ligue un un
sous total quarante et un seize neuf quatre deux quatre quatre trois zéro un zéro zéro dix premier juin mille neuf cent soixante vingt sept quatorze deux mille dix sept deux mille dix huit Drapeau de la France Paris Saint Germain deux mille dix huit deux mille dix neuf Drapeau de la France Paris Saint Germain Ligue vingt neuf janvier mille neuf cent trente trois six quatre deux trois deux zéro zéro C un huit quatre avril mille neuf cent quarante trois trente neuf treize deux mille dix neuf deux mille vingt Drapeau de la France Paris Saint Germain Ligue vingt janvier mille neuf cent dix huit cinq deux quatre un trois deux trois un un zéro C un sept quatre mai mille neuf cent trente trois trente treize Sous total sept neuf deux cinq un un zéro Total sur la carrière cinq cinq deux un zéro trente trois dix neuf décembre
caption : statistiques de Kylian Mbappé au quatorze novembre deux mille dix neuf
saison Sélection Campagne Phases finales Éliminatoires Matchs amicaux
deux mille seize deux mille dix sept drapeau : France France Coupe du monde deux mille dix huit
deux mille dix huit deux mille dix neuf drapeau : France France Ligue des nations quatre un
caption : liste des buts en sélection de Kylian Mbappé
saint denis , France Éliminatoires Coupe du monde deux mille dix huit volts quatre zéro Drapeau : pays bas Pays Bas But inscrit après quatre vingt dix plus un
drapeau : Russie Russie But inscrit après quarante minutes quarante E du
trois E but inscrit après quatre vingt trois minutes quatre vingt trois E du pied droit un trois
décines charpieu , France N un un Drapeau : états unis
drapeau : pérou Pérou But inscrit après trente quatre minutes trente quatre E du
inscrit après soixante quatre minutes soixante quatre E du pied gauche trois deux dix neuf E sept E But inscrit après soixante huit minutes soixante huit E du pied droit quatre deux
Russie le cinquième quatre deux Drapeau : Croatie Croatie But inscrit
deux mille dix huit deux mille dix neuf V deux un Drapeau : pays bas Pays Bas But inscrit
Islande Islande But inscrit après quatre vingt dix minutes quatre vingt dix E S .
P du pied
Moldavie Moldavie But inscrit après quatre vingt sept minutes quatre vingt sept E du pied droit
Islande But inscrit après soixante dix huit minutes soixante dix huit E du pied droit trois zéro trente E