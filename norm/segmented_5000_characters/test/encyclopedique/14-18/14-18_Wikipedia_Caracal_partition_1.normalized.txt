cette sous espèce a été décrite sous le nom Felis caracal nubicus par Johann Baptist Fischer en mille huit cent vingt neuf à partir d' un animal
cette sous espèce a été décrite sous le nom Caracal caracal poecilotis par Oldfield Thomas et Martin A C Hinton en mille neuf cent vingt et un
cette sous espèce a été décrite sous le nom Felis ( Caracal ) caracal schmitzi par Paul Matschie en mille neuf cent douze sur la base d' un animal
le Caracal est un mammifère de la famille des félidés de taille moyenne
corps de l' animal .
il se distingue par la présence d' un bouquet de longs poils noirs à l' extrémité des oreilles , dépassant quasiment leur longueur , caractéristique qu' il partage avec le Lynx et avec le chat des marais ( Felis chaus ) .
son poil est court d' une couleur gris fauve à rougeâtre uniforme sur le dos , les côtés et la queue .
le menton , la gorge , la poitrine , le ventre et l' intérieur des jambes sont blanchâtres et peuvent être
pales .
le train arrière très musclé est plus haut que le train avant .
les pattes du Caracal sont assez larges avec cinq doigts sur les pattes antérieures ( dont le premier ne touche pas le sol ) et quatre sur les postérieures munis de griffes rétractables de trois centimètres de long .
le Caracal possède de nombreux poils raides qui émergent d' entre ses coussinets lui assurant un bon
le crane est haut et bombé , la mâchoire courte avec une denture assez particulière .
en effet , la majorité des individus ne possède pas de première petite prémolaire supérieure ce qui leur confère , tout comme le Lynx , un total de vingt huit dents mais dans huit pour cents des cas la présence de cette prémolaire leur confère une denture conforme à celle de la famille Felidae avec trente dents .
la tête du Caracal comporte des marques faciales caractéristiques qui comprennent une ligne très foncée allant du bas du bord interne de chaque oeil au museau , une autre verticale passant au centre de la face très marquée sur le front et plus fine vers le museau et enfin , une marque allongée verticale très foncée au dessus du bord interne des yeux .
deux marques blanchâtres encadrent les yeux et deux autres sont présentes de chaque côté du nez sous les narines .
des spots très foncés marquent la naissance des moustaches ( vibrisses ) .
l' arrière des oreilles , triangulaires et pointues , est noir .
la surface intérieure du pavillon est recouverte de petits poils blancs .
les yeux sont de couleur claire , généralement verdâtres pouvant varier du marron au bleu .
la pupille est plutôt ronde , potentiellement légèrement oblongue verticalement .
les mâles peuvent atteindre une longueur de cent six centimètres sans la queue pour un poids maximal de vingt kilogrammes .
les femelles sont plus petites avec une longueur maximale de cent trois centimètres sans la queue et un poids maximal de seize
le Caracal peut présenter des cas de mélanisme , où
caption : caractéristiques biométriques du Caracal
le caracal a été par le passé classé dans le genre Lynx sur la base d' éléments d' identification communs .
le caracal possède de longs pinceaux de poils à l' extrémité des oreilles semblables à ceux du Lynx ainsi qu' un pelage ventral plus clair mais ses poils sont plus courts et sa robe unie de couleur fauve .
le chat doré africain ( Caracal aurata ) , l' autre espèce du genre Caracal , lui est assez similaire mais sa robe tachetée et ses petits poils à l' extrémité des oreilles le différencient assez aisément .
c' est également le cas du serval ( Leptailurus serval ) .
le puma ( Puma concolor ) possède une robe fauve avec un pelage ventral assez similaire à celle du caracal mais ne possède pas les pinceaux sur les pointes des oreilles .
comme tous les félins , le caracal est un animal territorial et n' accepte pas d' autres félins sur son domaine qu' il marque en urinant sur les rochers et les arbustes afin d' y laisser des marques olfactives .
le caracal est solitaire mais on peut cependant le rencontrer en couple durant les périodes de reproduction et les femelles sont accompagnées de leurs petits jusqu'à l' âge de dix mois .
le territoire des mâles chevauche celui de plusieurs femelles .
le caracal est un animal plutôt nocturne .
il a une activité diurne mais évite les heures les plus chaudes de la journée où il se retire probablement dans des grottes , des anfractuosités ou des cavités creusées par d' autres animaux .
le corps de ce félin est plus délié que celui des lynx nordiques , ce qui en fait un excellent coureur .
c' est également un excellent grimpeur .
ses oreilles caractéristiques pourraient lui servir