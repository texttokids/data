Abdelaziz Bouteflika , est né le deux mars dix neuf cent trente septà Oujda ,
originaires de la région de Tlemcen , Nedroma , une ville de l' Ouest de l' Algérie près de laquelle son père , Ahmed Bouteflika , est né
d' Algérie pour des " raisons économiques " , selon Jeune Afrique , ou pour " fuir la répression coloniale " , selon
tailleur , Ahmed Bouteflika devient mandataire au marché de la ville pour le compte d' un autre algérien , également propriétaire d' un hammam dont sa mère , Mansouriah Ghezlaoui , est la
Abdelaziz Bouteflika a quatre frères , Abdelghani , Mustapha , Abderahim demi soeurs , Fatima , Yamina et Aïcha .
il est le premier enfant de sa mère et le second de son père , sa demi soeur Fatima étant son ainée .
Bouteflika est issu de l' arabe algérien , , qui signifie
pendant sa présidence , Abdelaziz Bouteflika ne donne pas d' indication sur sa situation maritale .
en août dix neuf cent quatre vingt dix, il épouse dans la plus grande discrétion Amal Triki , fille d' un diplomate , Yahia Triki .
ils n' ont pas d' enfants et elle demeure à Paris .
Abdelaziz Bouteflika fait ses premières classes au début des années dix neuf cent quarante , à Sidi Ziane , une école moderne fondée en dix neuf cent sept , au lendemain de la prise d' Oujda par les troupes de Lyautey .
en dix neuf cent cinquante , il rejoint l' école de scoutisme Hassania .
il poursuit sa scolarité au lycée Abdelmoumen d' Oujda , où il adhère à la cellule de
Bouteflika au sein du " clan d' Oujda " , en dix neuf cent cinquante huit .
( accroupi au
durant la guerre d' Algérie , l' Armée de libération nationale principal parti nationaliste , ordonne à tous les lycéens et étudiants algériens de rejoindre ses rangs .
âgé de dix neuf ans , Abdelaziz Bouteflika intègre l' " armée des frontières " et ses bases installées sur le territoire du Maroc , qui vient de recouvrer son indépendance , .
il suit l' instruction militaire de l' école des cadres de l' ALN à Dar El Kebdani , entre Oujda et Nador , puis devient " contrôleur " pour la direction de la wilaya V ( Ouest ) : sa tâche consiste à " plaider la cause de l' ALN " auprès des populations rurales algériennes , pour en obtenir le ralliement .
il se consacre à cette fonction durant dix mois , entre dix neuf cent cinquante sept
en dix neuf cent cinquante huit , il est nommé secrétaire administratif par Houari Boumédiène , dont il suit l' ascension du poste de commandement de la wilaya V à celui de l' état major de l' Ouest , puis de l' état major général , au sein du " clan d' Oujda " .
en dix neuf cent soixante , il est envoyé en mission sur la frontière sud , d' où il revient avec le surnom d' " Abdelkader El Mali " , .
en dix neuf cent soixante et un , au moment du conflit entre l' état major et le GPRA , Houari Boumédiène le charge de trouver un appui parmi les " chefs historiques " alors emprisonnés au château d' Aunoy : il joue un rôle décisif dans la
ses responsabilités à cette époque lui valent , en vingt cent deux et vingt cent cinq , d' être accusé par l' homme politique français Pierre Messmer d' avoir
l' historien Pierre Vidal Naquet , ce massacre s' inscrit dans une logique de vendetta non planifiée , résultat de la vindicte
articles connexes : crise de l' été dix neuf cent soixante deux et Coup d' État du dix neuf
houari Boumediène ( premier plan à droite ) et son ministre des Affaires étrangères Abdelaziz Bouteflika ( au premier plan à gauche ) .
ministre de la Jeunesse et du Tourisme dans le gouvernement du président Ahmed Ben Bella .
membre de l' Assemblée nationale constituante , il est élu député de Tlemcen aux première et deuxième Assemblées législatives .
après le congrès du FLN d' avril , il est nommé membre du Comité central et du bureau politique du FLN .
missions à l' étranger comme ministre des Affaires étrangères par intérim .
en juin mille neuf cent soixante trois, il est confirmé dans ses fonctions , qu' il conserve jusqu'à la mort du président Houari Boumédiène .
après son limogeage , le vingt huit mai mille neuf cent soixante cinq, par le président Ahmed Ben Bella , il est partie prenante du coup d' État du dix neuf juin mille neuf cent soixante cinq .
trois ans après , il conclut avec la France , au nom de la République algérienne , l' accord du vingt sept décembre mille neuf cent soixante huitrelatif à la circulation , à l' emploi et au séjour en France des ressortissants algériens et de leurs familles , pierre angulaire de la politique de
pendant son mandat , Bouteflika plus jeune ministre des Affaires du tiers monde et l' interlocuteur privilégié dans les rapports entre le Nord et le Sud .
au cours de la même période , il obtient sa plus grande consécration en présidant la vingt neuf E session de
le président Chadli Bendjedid le nomme ministre d' État en mille neuf cent soixante dix neuf .
Bouteflika est néanmoins peu à peu écarté de la scène politique , qu' il quitte en mille neuf cent quatre vingt un .
il est traduit devant le conseil de discipline du FLN , qui l' exclut du comité central , et est poursuivi par la Cour des comptes : il avait détourné , sur les trésoreries des différentes chancelleries algériennes à l' étranger , l' équivalent de soixante millions de francs qu' il avait placés dans deux comptes ouverts auprès de la
il choisit alors de s' exiler pendant six ans entre la suisse où il prépare un temps une thèse , qui restera inachevée , au canton de
international , il fait des affaires et continue de