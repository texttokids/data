l' étude des comètes a considérablement progressé avec l' avènement de l' ère spatiale .
dix sondes ont contribué à mieux connaître les noyaux cométaires , les quatre premières s' étant approchées de la comète
mille neuf cent quatre vingt quatre , après avoir détaché un module vers la planète Vénus , s' approche à huit huit cent quatre vingt dix kilomètres de Halley le six mars
mille neuf cent quatre vingt dix neuf , passe à moins de deux cent trente six kilomètres de la comète Wild deux le
traversant sa queue et la ramène sur Terre le quinze janvier deux mille six. en deux mille sept , une seconde mission lui est assignée , vers une nouvelle comète : le quinze février deux mille onzeelle passe à cent quatre vingt dix kilomètres de
janvier deux mille cinq, creuse un cratère artificiel sur le noyau
collision d' un impacteur .
puis , après avoir utilisé l' assistance gravitationnelle de la Terre fin deux mille sept , la sonde rebaptisée EPOXI passe à environ sept cents kilomètres de la comète cent trois P Hartley
deux mille quatre , après avoir survolé les astéroïdes teins ( cinq
deux mille dix ) , se met en orbite à cent kilomètres autour de la comète soixante sept P Tchourioumov Guérassimenko dix ans après son lancement , le
technologique .
les images de haute définition sont transmises , révélant de nombreux détails de l' astre .
Philaé , un petit
lancée le deux décembre mille neuf cent quatre vingt quinze, destinée à étudier le Soleil en continu et qui de ce fait a permis de découvrir des comètes qui finissaient leur vie en " tombant " dans le Soleil ,
terrestrial RElations Observatory ) , lancés le vingt cinq octobre deux mille sixet destinés eux aussi à étudier le Soleil , ont permis , comme SoHO , la découverte de nombreuses comètes rasantes .
sonde spatiale Comet Interceptor qui sera dirigée vers une comète ou un objet interstellaire n' ayant jamais survolé le Soleil .
en effet , jusque là , les comètes étudiées ont toutes effectué plusieurs passages près du Soleil , ce qui a transformé leur structure et leur composition .
cette interception est rendue possible par l' existence d' observatoires terrestres permettant de découvrir de nouvelles comètes longtemps à l' avance .
avant la publication en mille sept cent cinq d' Edmond Halley sur la comète portant son nom , ces petits corps du Système solaire étaient considérés comme des phénomènes isolés , uniques et non périodiques , aussi les
mise à part la comète de Halley , ou celle de Encke , le nom d' une comète est attribué officiellement par une commission de l' Union astronomique internationale ( UAI , IAU en anglais ) , dont le siège est à Washington , D C certaines comètes historiques , spectaculaires et aisément visibles à l' oeil nu , n' ont aucun nom officiel et sont simplement désignée comme grande comète .
par exemple
traditionnellement , on donne aux comètes le nom de son ( ou de ses ) découvreur deux noeuds , jusqu'à trois noms maximum .
dans le cas des comètes Halley , Encke ou Lexell , il s' agit du nom des personnes qui ont déterminé la périodicité de ces astres .
quelques comètes sont nommées d' après le lieu de leur découverte ( la comète Lulin ) et un nombre de plus en plus important reçoit le nom d' un programme de recherche automatique , comme LINEAR ou NEAT , ou bien celui
en plus du nom , les comètes reçoivent une référence officielle dont l' attribution obéit à un nouveau procédé ( préfixe selon la période suivie d' une désignation séquentielle suivant l' ordre des découvertes : l' année , puis une lettre majuscule identifiant le demi mois de la découverte , puis un nombre indiquant l' ordre de la découverte dans ce
avant le un er janvier mille neuf cent quatre vingt quinzeles comètes recevaient une désignation provisoire constituée par l' année de la découverte suivie d' une lettre en minuscule correspondant à l' ordre de la découverte .
par exemple , mille neuf cent soixante cinq F , sixième comète trouvée pendant l' année mille neuf cent soixante cinq .
plus tard , le nom définitif lui était attribué selon les critères suivants : l' année du passage au périhélie , suivie d' un numéro noté en chiffres romains indiquant l' ordre chronologique du passage au périhélie ( exemple : mille neuf cent quatre vingt quatorze IV , quatrième comète passée au périhélie en mille neuf cent quatre vingt quatorze ) .
ce procédé comportait de nombreux inconvénients : la multiplication des découvertes épuisait l' alphabet .
quand on découvrait une vingt sept E comète dans l' année , il fallait recommencer l' alphabet en faisant suivre la lettre du chiffre un ( comme mille neuf cent quatre vingt onze a un ) .
les découvertes de comètes après leur passage au périhélie rendaient difficile une désignation officielle cohérente .
les comètes à courte période multipliaient les désignations , une nouvelle étant attribuée à chacun de leurs retours .
depuis le un er janvier mille neuf cent quatre vingt quinze, une nouvelle nomenclature , inspirée par celle appliquée aux astéroïdes , est attribuée comme ceci : un .
une lettre servant à identifier le type de comète : C indique une comète à longue période ( supérieure à deux cents ans ) ou non périodique .
P indique une comète à courte période ( inférieure à deux cents ans ) .
la lettre D est utilisée pour les comètes perdues .
X pour une
trois .
une lettre majuscule correspondant à la quinzaine du mois de la
quatre .
un chiffre précisant l' ordre chronologique de découverte durant
lorsque plusieurs comètes portent le nom d' un même découvreur , un numéro est parfois ajouté pour les différencier ( comète Hartley deux