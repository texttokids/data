billard .
il peut également accomplir quelques missions annexes facultatives comme trouver et exterminer une centaine de " rats volants " ( pigeons ) , faire des cascades acrobatiques en véhicule ,
véhicule de police blanche sur une route pendant la nuit .
véhicule du New York City Police Department ( NYPD ) , semblable à
en vue objective .
il permet au joueur de tirer à couvert ou à l' aveugle et de cibler un ennemi spécifique ou certaines parties corporelles qui peuvent également et individuellement être visées .
la barre de santé du personnage joueur est représentée par un demi cercle vert , accompagné d' un demi cercle bleu représentant l' état de la protection pare balles , autour d' une mini carte positionnée sur le bas côté gauche de l' écran .
la barre de santé des autres personnages est représentée par un petit cercle blanc qui s' affiche lorsqu' ils sont ciblés par arme à feu ou par système de combat rapproché .
en combat rapproché , le joueur peut frapper par coups de poing ou coups de pied , désarmer , parer et contre attaquer un éventuel opposant .
il peut également grimper sur des hauteurs ou sur des échelles , s' accroupir et se mettre à couvert derrière des murs ou objets pour se protéger des tirs ennemis .
hors combats , le joueur peut , si nécessaire , utiliser plusieurs techniques pour rehausser sa barre de santé comme se revigorer en mangeant un hot dog ou en buvant une canette de soda , utiliser des kits de premier secours , faire appel aux services des prostituées , ou appeler une ambulance
en dehors des attaques par arme à feu ou affrontements au corps à corps , la barre de santé peut diminuer lors de chutes ou collisions en véhicules ou depuis une hauteur , lorsqu' une explosion est essuyée de
totalement vide , le joueur meurt et réapparait automatiquement devant l' entrée de l' hôpital le plus proche , moyennant contribution pour les soins hospitaliers , les armes réapparaissent sur lui lorsqu' il est devant l' hôpital , mais sont confisquées lorsqu' il est appréhendé par les forces de l' ordre et qu' il réapparait devant le commissariat de proximité .
lorsqu' il est en cavale et lorsque le niveau de recherche est élevé , le joueur peut rencontrer et percuter des véhicules appartenant aux forces
le niveau de recherche indique l' importance du délit de fuite par la police locale ( LCPD ) .
lorsqu' un délit est commis , le joueur est repéré et recherché dans la zone clignotante indiquée sur la mini carte .
le joueur peut choisir de se rendre sans résistance , de fuir , ou d' attaquer les autorités .
plus le joueur tente de résister à son arrestation , plus le niveau augmente et plus la zone de recherche s' élargit , faisant apparaître une force de police plus nombreuse , armée et agressive .
le niveau de recherche peut disparaitre lorsque le joueur amène son véhicule dans un garage Pay' N' Spray , ou en changeant de véhicule lors de la fuite sans être repéré .
cependant , le niveau peut augmenter de façon considérable lorsque le joueur est sur le point d' être arrêté et qu' il fuit au
côtés , ont la possibilité d' appeler les autorités lorsqu' ils assistent est envoyée uniquement lors d' un conflit entre citoyens ou lors d' une agression .
les crimes les plus graves sont traités par une organisation antiterroriste connue sous le nom de NOOSE ( National Office Of Security Enforcement ) , qui fait usage d' armes avancées ,
le joueur peut utiliser un total de quinze armes à feu et armes blanches , dont les catégories varient entre faibles et améliorées .
il dispose donc de couteaux et battes de base ball , de pistolets ( Glock dix huit et Désert Eagle ) , de
pompe ( Mossberg cinq cents et Winchester mille trois cent avec canon raccourci et poignée ) , de fusils d' assaut ( Colt le millième quatre À un et AK quarante sept ) , de fusils à lunette ( M quarante et PSG un ) , de grenades à main
le joueur peut faire usage du téléphone portable pour effectuer plusieurs actions .
il possède un rôle fondamental dans la progression scénaristique .
grâce au menu , le joueur peut accéder aux services de messagerie et d' annotations , organiser des activités entre amis et rejouer les missions échouées .
il peut prendre des photos et le connecter dans un véhicule de police pour repérer des criminels pendant certaines missions .
il peut en composant le neuf cent onze , les ambulanciers peuvent soigner le joueur moyennant rétribution financière .
le téléphone portable permet également d' accéder au mode multijoueur sur
le jeu dispose de son propre réseau Internet qui compte une centaine de sites web exclusifs .
le joueur peut s' y connecter en utilisant l' un des ordinateurs mis à disposition dans les cyber cafés nommés tw situés à plusieurs endroits de la ville , ou
recevoir des messages électroniques contenant notamment des