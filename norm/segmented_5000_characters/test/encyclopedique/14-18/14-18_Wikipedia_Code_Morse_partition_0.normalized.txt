message transmis en Scott ( Morse lumineux ) , par un timonier , au
le code peut être transporté via un signal radio permanent que l' on allume et éteint ( onde continue , généralement abrégé en CW , pour continuous wave en anglais ) , ou une impulsion électrique à travers un câble télégraphique ( de nos jours remplacé par d' autres moyens de communication numérique ) , ou encore un signal visuel ( flash lumineux ) .
l' idée qui préside à l' élaboration du code morse est de coder les caractères fréquents avec peu de signaux , et de coder en revanche sur des séquences plus longues les caractères qui reviennent plus rarement .
par exemple , le " E " , lettre très fréquente , est codée par un simple point , le plus bref de tous les signes .
les vingt cinq autres lettres sont toutes codées sur quatre signaux au maximum , les chiffres sur cinq signaux .
les séquences plus longues correspondent à des symboles les plus rares : signes de ponctuation , symboles et caractères
parallèlement au code morse , des abréviations commerciales plus en utilisant des mots tels que BYOXO ( Are you trying to crawl out of it ?
) , liouy ( Why do you not answer my question ?
) et AYYLU ( Not clearly coded , repeat more clearly .

l' intention de ces codes était d' optimiser le cout des transmissions sur les câbles .
les radioamateurs utilisent toujours certains codes appelés Code Q et Code Z Ils sont utilisés par les opérateurs afin de s' échanger des informations récurrentes , portant par exemple sur la qualité de la liaison , les changements de fréquences et les télégrammes .
antenne radiotélégraphique de canot de sauvetage en mille neuf cent quatorze .
articles détaillés : officier radiotélégraphiste de la marine marchande et Radiotélégraphiste de station côtière .
les premières liaisons radiotélégraphiques sans fil utilisant le code morse datent du début du XX E siècle .
en mille neuf cent trois , la conférence de Berlin attribue la longueur d' onde de six cents mètres officialise en mille neuf cent six le signal SOS comme appel de détresse .
jusqu' en mille neuf cent quatre vingt sept , plusieurs conférences mondiales des radiocommunications définissent les bandes à utiliser pour les communications en télégraphie morse .
depuis le un er février
maritimes côtiers et mobiles de France et de nombreux autres pays ont abandonné la veille radiotélégraphique obligatoire et cessé les émissions en morse , notamment sur la fréquence de cinq cents kilo hertz ( maritime et aéronautique ) et sur la fréquence de huit trois cent soixante quatre Hz , affectées au trafic de détresse ou d' appel en radiotélégraphie , depuis les années dix neuf cent soixante dix , un système de satellites de télécommunications ayant pris le relais .
à partir de ce moment , le trafic maritime radiotélégraphique et radiotéléphonique utilisant les ondes hertziennes commence à décliner lentement .
cependant , il existe encore à ce jour un des fréquences internationales affectées par l' UIT à la diffusion de l' heure , de la météo marine ou aux communications maritimes en radiotélégraphie ( parmi d' autres , quatre cent quatre vingt deux kilo hertz
la bande des six cents mètres notamment reste utilisée par une vingtaine de pays dans le monde , parmi lesquels : l' Arabie saoudite , l' Argentine , l' Azerbaïdjan , le Cameroun , la
l' Érythrée , les États Unis , l' Indonésie , l' Italie , l' Irlande , Oman , la Roumanie , la Fédération de Russie , les Samoa américaines et les Seychelles .
à quelques exceptions près , la plupart des stations maritimes encore en activité n' émettent plus en morse que leur indicatif d' appel et certaines fréquences destinées au trafic en CW de la marine marchande ont encore une affectation , même si elles ne sont plus utilisées que par quelques pays et très rarement .
depuis le début du XX E siècle et l' invention de la lampe Aldis , les bateaux peuvent également communiquer en morse lumineux .
alors que la capacité à émettre de tels signaux reste exigée pour devenir officier de la marine marchande dans de nombreux pays , dont la France , cette pratique a tendance à devenir rare et ne se retrouve plus que dans la marine de guerre et chez
les premières liaisons radiotélégraphiques aéronautiques remontent au début du XX E siècle et ont cessé avant les années dix neuf cent soixante dix , à une communiquaient en radiotélégraphie dans la bande aéronautique des neuf cents mètres ( trois cent trente trois , trente trois kilo hertz ) , en vol au dessus des mers et des océans dans la bande marine des six cents mètres ( cinq cents kilo hertz ) , sur la longueur d' onde de radiogoniométrie de quatre cent cinquante mètres transcontinental radiotélégraphique au dessus des océans dans la
en vol une antenne pendante longue de cent vingt mètres à quatre cent cinquante mètres avait pour but d' établir les communications radiotélégraphiques sur ces longueurs d' onde .
à l' extrémité de l' antenne pendait un plomb
une autre antenne tendue le long de la coque de l' aéronef établissait sol sur la longueur d' onde de neuf cents mètres ( trois cent trente trois , trente trois kilo hertz ) et dès