le lynx des cavernes est le nom que l' on donne parfois aux plus anciens lynx pardelles , ceux qui ont côtoyé les hommes préhistoriques , et dont on a retrouvé des fossiles dans les grottes .
le lynx des cavernes est extrêmement proche du lynx pardelle actuel .
certains scientifiques le considèrent comme une espèce à part , dont le nom scientifique est Lynx spelaea , mais beaucoup considèrent en fait qu' il ne s' agit que d' une sous espèce disparue , qui se nomme alors Lynx pardinus spelaeus .
le lynx des cavernes est principalement connu grâce à ses dents , et quelques os .
les dents du lynx des cavernes sont légèrement plus grandes que celles du lynx d' Espagne huit ( c' est à dire le lynx pardelle actuel ) , ce qui semble indiquer qu' il était un peu plus gros .
s' il existe bien des différences qui permettent de reconnaitre le lynx des cavernes du lynx d' Espagne , la plupart des scientifiques trois considèrent que ces différences ne sont pas suffisantes pour en faire une espèce à part : ils pensent plutôt qu' il s' agit de deux sortes différentes de lynx pardelle .
le lynx des cavernes ne vivait pas vraiment dans des cavernes ( en tous cas , pas plus que le lynx pardelle actuel ...
) : son nom ne vient pas de là .
les premiers sites archéologiques qui ont permis de connaître les hommes préhistoriques étaient des grottes : on a donc pensé que les premiers humains y vivaient , et on les a appelés les " hommes des cavernes " , et l' époque à laquelle ils vivaient , l' " époque des cavernes " .
en fait , on sait aujourd'hui que c' est essentiellement faux : si les hommes préhistoriques occupaient effectivement des cavernes , ce n' était pas vraiment leur mode de vie .
plusieurs sites , notamment celui de Terra Amata , à Nice , ont montré que les premiers êtres humains en Europe vivaient déjà dans des sortes de tentes , ou de huttes .
ils devaient utiliser les cavernes comme des refuges , ou bien comme des sites sacrés , ce qui expliqueraient que , bien plus tard , ils y aient réalisé des peintures rupestres .
les animaux qui ont côtoyé les premiers humains à cette époque ont été chassés par ces hommes préhistoriques , qui les ont ramenés dans les cavernes , où on a pu retrouver leurs fossiles .
c' est pour cela qu' ils ont reçu ce nom .
ainsi , on connait le lion des cavernes , l' ours des cavernes , la hyène des cavernes , et , bien sûr ...
le lynx des cavernes !
en réalité , le lynx des cavernes ne présentait pas de grandes différences avec le lynx pardelle actuel ...
il devait donc vivre de la même façon .
bien sûr , il est possible , et même probable , que certains lynx aient pu utiliser des cavernes comme tanière , s' ils en trouvaient une ...
mais ce n' était probablement pas leur mode de vie habituel .
aujourd'hui , le lynx pardelle ne survit que dans deux régions d' Espagne , il ne resterait que entre quatre vingts et cent vingt lynx deux vivant en Espagne ( et donc dans le monde !
) environ .
l' espèce est donc considérée comme en danger critique d' extinction .
le lynx pardelle était autrefois beaucoup plus répandu : on le trouvait notamment dans les Pyrénées jusqu' au XIXème siècle , mais aussi au Portugal .
les derniers lynx pyrénéens se sont éteints dans les années mille neuf cent quatre vingt dix deux mille mais il ne s' agissait probablement pas du lynx pardelle mais du ) Au Portugal , en deux mille deux , une étude pour tenter de recenser les lynx n' a pu en découvrir aucun , ce qui peut laisser penser qu' ils ont disparu ...
des indices indirects , comme la présence d' ADN laissent supposer qu' il pourrait en rester quelques uns .
au maximum , on pense qu' il y aurait moins de quarante lynx actuellement vivants au Portugal .
la chasse et les maladies , comme la myxomatose , ont fait fortement diminuer le nombre de lapins , or , comme le lynx pardelle ne mange quasiment que des lapins , il est menacé par la raréfaction de ses proies préférées .
aujourd'hui , l' espèce est strictement protégée .
le lynx fait l' objet d' un programme d' élevage ex situ , c' est à dire en dehors de leur habitat naturel .
ces lynx d' élevage sont destinés à être relâchés dans la nature , pour repeupler les zones d' où il a disparu .
en décembre deux mille dix, cinq lynx ont été relâchés en Andalousie .
neuf