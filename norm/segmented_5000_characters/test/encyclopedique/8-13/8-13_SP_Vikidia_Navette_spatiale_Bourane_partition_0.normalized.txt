la navette spatiale Bourane ( " tempête de neige " en russe et Buran en anglais ) , ou aussi OK un .
un , est la première navette spatiale russe .
c' est la navette la plus connue du programme Bourane car c' est la seule du programme qui ait connu l' espace et fait un vol orbital .
elle est la première d' une série de cinq navettes ( uniquement deux d' entre elles ont été achevées ) .
construite en dix neuf cent quatre vingt six , elle a effectué son unique vol en mode automatique sans équipage le quinze novembre dix neuf cent quatre vingt huit. elle a été accidentellement détruite le douze mai vingt cent deuxpar suite de l' effondrement du toit de son hangar dont l' architecture n' était pas adaptée à la pluie .
la construction de cette navette commence en dix neuf cent quatre vingt six et dure deux ans .
Bourane est construite dans sa totalité à Moscou .
au début , elle se nomme Baïkal avant d' être renommée en Bourane peu de temps avant son premier vol un .
c' est l' avion Antonov An deux cent vingt cinq Mriya qui permettra le transport de la navette du site de construction au site de lancement deux .
la préparation de ce premier vol est longue : commencée en même temps que la construction du projet Bourane ( qui voulait attendre que la navette spatiale américaine fasse plusieurs missions avec parmi elles , la mission au cours de laquelle aura lieu la catastrophe de Challenger ) , elle dure douze ans .
officiellement , le décollage a lieu le vingt neuf octobre dix neuf cent quatre vingt huitmais , cinquante et un secondes avant le décollage , une anomalie se déclenche au niveau du système de guidage .
ce problème retarde le lancement de la navette au quinze novembre dix neuf cent quatre vingt huit.
le jour venu , les préparatifs se déroulent sans aucun problème .
en revanche , les conditions météo sont très défavorables et le service météo envoie un signal avertisseur de tempête aux personnes concernées .
en principe , des réacteurs sont censés se trouver sur la navette pour que lors de la descente , Bourane puisse reprendre de l' altitude s' il y avait un problème , mais ils ne sont pas installés pour ce volume Bourane serait donc obligée de planer à l' atterrissage .
ceci dit , des ingénieurs ayant participé à ce projet de vol assurent que le vol sera un succès , car les conditions limites pour l' atterrissage de la navette ne sont même pas atteintes .
le lancement est finalement décidé .
le quinze novembre dix neuf cent quatre vingt huit, la navette spatiale Bourane effectue son premier vol qui sera le seul de tout le programme Bourane .
le décollage a lieu de nuit .
à six H du matin ( heure de Moscou ) , Bourane et le lanceur Energia s' arrachent de la plate forme de lancement .
dès le décollage , Bourane effectue une rotation comme la navette spatiale américaine ( qui utilise le pas de tir d' un ancien lanceur passé et qui a besoin de faire cette rotation pour se mettre sur la bonne orbite ) .
même si son pas de tir a été construit en même temps que le programme , Bourane fait la même manoeuvre que la navette spatiale américaine .
en effet , Bourane est censée la concurrencer .
un peu plus de deux minutes après le décollage ( cent quarante secondes ) , les quatre boosters ( premier étage du lanceur ) se séparent en s' éjectant par deux de la partie centrale d' Energia ( deuxième étage du lanceur ) , se séparent à nouveau pour retomber individuellement et des parachutes se déploient : les boosters sont réutilisables .
huit minutes après le décollage , le lanceur Energia se sépare de Bourane .
c' est à ce moment là que le lanceur pourrait larguer un satellite , mais ce ne sera pas le cas lors de ce vol ( et donc jamais ) .
contrairement à la navette spatiale américaine où le réservoir externe brule et se détruit en rentrant dans l' atmosphère , le lanceur Energia tombe dans l' océan Pacifique sans bruler , car il n' est pas prévu pour l' être et mettrait plusieurs jours à bruler .
les moteurs de cette partie du lanceur ne sont pas réutilisables .
la navette Bourane se met en orbite avec ses propres moteurs .
durant son vol orbital , Bourane est inclinée à cinquante et un , six degrés .
elle effectue deux tours du monde ( cent vingt kilomètre ) , sans aucun problème .
après un heure et demie minutes de vol , Bourane se prépare à la manoeuvre de désorbitage et à la rentrée dans l' atmosphère .
au moment de la rentrée dans l' atmosphère , la liaison avec la navette est coupée .
après vingt minutes , la liaison revient .
l' approche finale se fait automatiquement .
un avion , un MIG vingt cinq , s' envole et s' approche de Bourane lorsqu' elle est encore à sept kilomètres du sol pour la filmer durant l' atterrissage qui est marqué par les figures faites entre la navette et l' avion .
Bourane touche doucement la piste d' atterrissage , déploie ses parachutes , se fait survoler par l' avion d' accompagnement avant de s' immobiliser en même temps que lui .