Cénile Spilorcio est un armateur vénitien .
ce vieil avare est prêt à tout pour avoir de l' argent , quitte à être malhonnête : il n' hésite en effet pas à s' associer avec le capitan Mendoza ou à travailler pour le prince Jean , uniquement dans le but d' avoir le trésor ou de s' enrichir .
cela ne l' empêche pas de ressentir parfois des sentiments , comme vis à vis de sa fille adoptive Séléné pour qui il ressent un amour plus que paternel ...
mais généralement , sa soif d' argent et d' or est la plus forte .
il est largement inspiré du personnage d' Harpagon dans L' Avare de Molière seize .
personnages changeants de camp ou dont le camp n' est pas clair
Andreo Spilorcio est le fils de Cénile .
il est très différent de son avare de père : beau comme un dieu , dépensier , plutôt sympathique , il est particulièrement lâche et même un peu niais ( il se cache dans sa chambre pour faire croire à son enlèvement !

toutefois , il se ragaillardit au fil de l' aventure , risquant même sa vie pour sauver celle d' Hermine , dont il est éperdument amoureux .
il se découvre , sur la fin de l' histoire , une vocation pour le théâtre , avec son ami et domestique Plaisant ( sorte d' hommage à Scapin des Fourberies de Scapin dix sept ou Figaro du Mariage de Figaro ) .
Andreo lui même peut faire penser à Léandre de la pièce Les Fourberies de Scapin dix sept .
le Captain Boney Boone est le capitaine d' une bande de pirates cruels , stupides et peureux .
lui même est assez ridicule avec son poulet sur son épaule ( qu' il prend pour un perroquet ) et sa manie de pousser des jurons centrés sur le squelette humain ( " mille tibias ! " , etc ) .
son nom vient d' ailleurs de " bone " , qui signifie " os " en anglais .
caricature bête et méchante du capitaine Haddock des Aventures de Tintin dix huit ou de Barbe rouge d' Astérix dix neuf , il devient plus humain au fil des tomes : né dans la misère ( " dans le ruisseau " selon ses propres dires ) , il ne voulait pas être pirate et ne rêve que de gloire .
il devient gentil grâce au discours de son second , Monsieur de Cigognac .
monsieur de Cigognac est une cigogne .
c' est le second du Captain Boone .
il fait entrer les pirates dans le camp des gentils grâce à son discours dans le tome neuf .
mademoiselle est la soeur du roi de la Lune et du prince Jean .
cette beauté vénéneuse , comme Milady de Winter du roman Les Trois Mousquetaires vingt , n' hésite pas à se servir de ses charmes sur Lope , et est totalement dévouée à son renégat de frère , le Prince Jean .
physiquement , elle ressemble très fortement à Séléné Spilorcio .
en vingt cent deux , à l' occasion de la sortie du tome quatre , Ayroles et Masbou s' associent pour une " farce héroïque " en un acte , Le Médecin imaginaire , qui est distribuée avec le nouveau volume vingt et un .
elle raconte comment les deux héros se sont rencontrés .
la pièce contient des références aux pièces de Molière , Le Malade imaginaire et Le Médecin malgré lui , mais aussi à Cyrano de Bergerac d' Edmond Rostand .
elle a été jouée notamment en vingt cent deux au théâtre Montansier de Versailles et en vingt cent onze au château de Vaux le Vicomte par la compagnie des Mille chandelles .
la compagnie des Masques a créé deux pièces reprenant l' univers de la saga : le Trésor des îles Tangerines et De Cape et de Crocs : l' Expérience vingt deux .
bien que l' histoire originale soit terminée dans le tome dix , de nouveaux tomes vont venir compléter l' univers de la série : ainsi , les tomes onze et douze vont raconter l' histoire d' Eusèbe , le lapin blanc au passé trouble , avant qu' il ne prenne part à la quête de Lope et Armand .
le tome onze est sorti en novembre vingt mille millecent quarante cinq .
et le tome douze est sorti en vingt cent seize .