Les coulisses de la magie
Des premiers pas de Mickey à La Reine des neiges, les films Disney ont profité d'innovations techniques très importantes.
MICKEY
1928 Les aventures de Mickey Steamboat Willie est le premier dessin animé avec le son synchronisé (le cinéma est devenu parlant en 1927). Mais la grande innovation de Disney et d'Ub lwerks (ci-contre) est d'avoir créé un court métrage d'animation qui ne repose pas seulement
sur le gag mais sur une histoire construite. Ils s'inspirent du cinéma d'Hollywood, et ca marche ! Le public adore la personnalité amusante de Mickey.
1942 et 1995 Les animaux de Bambi et du Roi Lion Très investi dans le tournage des films de Bambi, il amène donc des faons dans Disney veut que ses artistes dessinent au les studios pour des séances de croquis. plus près du réel, en particulier pour Cinquante ans plus tard, la même les animaux. Lors de la production
technique sera utilisée... avec des lions!
STEAMBOAT WILLIE
1937 La forêt de Blanche-Neige Pour filmer les décors multiplane. Les animateurs féeriques de Blanche ont donc inventé une machine Neige et les sept nains et de 4 mètres de haut pour filmer plonger le public dans une différents niveaux de décors et forêt magique, qui semble donner la juste impression de vivante, il a fallu utiliser une profondeur. Un défi technique nouvelle caméra: la caméra et financier qui a payé !
1995 Le monde de Toy Story Rendre des jouets vivants afin qu'ils incarnent de vrais personnages... Ce drôle de défi a été relevé avec brio par les studios Pixar, en collaboration avec Disney. Toy Story est le premier film réalisé entièrement en 3D par ordinateur. Résultat : les héros semblent vrais ! C'est une nouvelle étape dans le monde du dessin animé.
1940 La folie de Fantasia Sans dialogues, Fantasia repose sur la parfaite synchronisation entre les images et la musique. Le son a d'ailleurs été enregistré avec un procédé unique, le "Fantasound" inventé pour l'occasion. Les animateurs ont aussi rivalisé d'inventivité pour reproduire le réel. Pour obtenir les séquences aquatiques, ils ont par exemple mis leurs dessins sur celluloïd 2 sous l'eau ! 1. La bande-son et l'image fonctionnent en même temps, de façon coordonnée. 2. Matière plastique transparente sur laquelle on peut peindre.

