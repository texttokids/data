
Une mosquée est un lieu de culte où se rassemblent les musulmans pour les prières communes, notamment la prière du vendredi.

Éléments constitutifs d'une mosquée
Le plan de la mosquée reprend le plan de la maison de Mahomet à Médine. À travers le monde musulman, les mosquées sont toutes bâties sur ce plan, même si la décoration varie.
Les mosquées ne sont pas décorées de statues ni de peintures représentant des personnages. Bien que la représentation figurée ne soit pas interdite dans le Coran, elle ne trouve pas sa place dans les mosquées1. Les décorateurs ont donc eu recours à des figures géométriques répétées à l'infini et de couleurs variées. 

Extérieur
Les musulmans sont appelés à la prière par le muezzin qui lance l'appel du haut de la tour appelée minaret. 
Avant de pénétrer dans la salle de prière, les musulmans font leurs ablutions rituelles de purification aux fontaines qui se tiennent dans la cour de la mosquée. 

Intérieur
La salle de prière est une vaste salle rectangulaire où les fidèles s'installent en rangs parallèles tournés du côté de la longueur (signe d'égalité). La direction de la Mecque s'appelle la qibla vers laquelle se tournent les musulmans pour prier. 
Le mihrab, petite niche maçonnée montre la qibla : l'imam se place dans cette structure. C'est un élément essentiel de la salle de prière d'une mosquée. Le mihrab est un sorte de niche bâtie à l'intérieur du mur de qibla, c'est-à-dire du mur qui est dans la direction de la Kaaba de La Mecque. Pour prier, les musulmans font face au mihrab, donc sont tournés vers La Mecque, ce qui depuis 629 est une obligation rituelle.
Dans la salle se trouve une estrade-escalier, le minbar, du haut de laquelle l'imam lit le sermon du vendredi, appelé la khutba2. 
-Salle de prière de la mosquée Al-Rifaï, Le Caire.
-Mihrab de la mosquée des Omeyyades, Damas, Syrie.
-Minbar de la mosquée d'Eyüp, Istanbul.

Architecture selon les régions
-Plan arabe
Le plan arabe, ou plan hypostyle (= sous des colonnes), est un plan de forme carrée ou rectangulaire, composé d’une cour à portique et d’une salle de prière à colonnes, généralement récupérées de monuments romains trouvés sur place. 
On le trouve dans tout le monde islamique, de la Syrie (Grande mosquée des Omeyyades à Damas) au Maghreb (Grande Mosquée de Kairouan en Tunisie), aussi bien qu'en Espagne ou en Irak. Les mosquées de plan arabe ont été construites notamment sous le règne des Abbassides et des Omeyyades.
-Plan classique d'une mosquée du Maghreb : la Grande mosquée de Kairouan (Tunisie).
-La Grande Mosquée de Kairouan, de plan arabe, en Tunisie.
-Plan iranien 
Apparaissant au Xe siècle avec la dynastie seldjoukide, il se caractérise par l’emploi d’une arcade d'entrée monumentale appelée iwan (wp) et d’un pishtak (wp), portail formant une avancée, souvent surmonté de deux minarets et ouvert par un grand arc. 
On le trouve en Iran, mais aussi en Irak en Afghanistan, au Pakistan et en Inde.
La Grande Mosquée d'Ispahan est l’un des meilleurs exemples de plan iranien.
-Iwan de la Grande mosquée d'Ispahan
-Pishtak du mausolée d'Ali ibn Abi Talib, à Nadjaf, Irak.
-Plan moghol
Ce plan, que l'on trouve en Inde à partir du XVIe siècle, est une variante du plan iranien. Il se caractérise par une immense cour à quatre iwans, dont un ouvre sur une salle de prière étroite et rectangulaire, couronnée par trois ou cinq coupoles bulbeuses. 
Les grandes mosquées de Delhi et de Bîdâr (wp) utilisent ce type de plan.
-Mosquée moghole Jama Masjid, à Delhi.
-Cour de la Mahmud Gawan Madrasa, Bîdâr.
-Plan ottoman
Le plan à coupole centrale reposant sur quatre piliers est né après la Chute de Constantinople (1453), lorsque les Turcs découvrirent l'architecture unique de Sainte-Sophie, qui avait déjà résisté à cinq siècles de séismes et d'histoire. 
L’architecte Sinan, au XVIe siècle, demeure le plus grand bâtisseur de mosquées turques : on lui attribue plus de 300 édifices, dont la mosquée Süleymaniye à Istanbul. C'est un de ses disciples qui réalisa, au début du XVIIe siècle, la fameuse Mosquée bleue, sorte d'aboutissement mathématique où toutes les forces s'équilibrent jusqu'au sol, grâce au jeu des demi-coupoles, corrigeant les défauts encore présents dans la structure de Sainte-Sophie.
-Style marocain
Les tribus berbères du Maroc ont été converties à l'islam par le roi Idris Ier, fondateur des villes de Fès et Moulay Idris. Le style de cette époque a continué jusqu'à nos jours avec des tuiles émaillées, des fontaines intérieures, des motifs géométriques d'arabesques et de fleurs stylisées. Les décors de stuc, plâtre, bois, sont partout, dans les palais aussi bien que dans les mosquées. Les minarets sont de type carré, à étages, décorés et émaillés.
-Style mauresque d'Espagne
Le style mauresque d'Andalousie est apparenté au précédent, avec des minarets carrés et des décors de stuc. 
La Grande mosquée de Cordoue et la Giralda de Séville en sont les meilleurs exemples. 
-Intérieur de la Grande Mosquée des Omeyyades, à Cordoue.
-La cathédrale et la Giralda, à Séville.
-Mosquées d'Afrique subsaharienne
L'architecture d’Afrique subsaharienne est marquée par l’architecture de terre. Les mosquées, aussi bien que les maisons, sont souvent en terre crue. Les grandes mosquées de Tombouctou et de Djenné (Mali) sont dotées de contreforts et de pinacles. La grande mosquée d’Agadès (Niger), du XVIe siècle, possède un minaret sahélien traditionnel construit avec de la terre et des étais de bois.
-Mosquée Sankoré, à Tombouctou
-Marché et Grande mosquée de Djenné
-Mosquée d'Agadès

Mosquées célèbres
- Grande mosquée de Kairouan (Tunisie) ;
- Grande mosquée de Cordoue (Andalousie, Espagne) ;
- Mosquée al-Aqsa (Jérusalem) ; 
- Mosquée bleue (Istanbul) ;
- Mosquée Hassan II (Casablanca, Maroc) ;
- Grande Mosquée des Omeyyades (Damas, Syrie).

Pour compléter sur les lieux de culte
- Église
- Temple protestant
- Synagogue
- Temple grec
- Temple égyptien