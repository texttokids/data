
Les singes (nom scientifique : Simiiformes, parfois encore appelés simiens) sont un groupe de mammifères appartenant à l'ordre des primates.
Comme tous les primates, ils ont une main avec un pouce opposable aux autres doigts, ce qui leur permet d'attraper des objets ou de saisir les branches pour grimper dans les arbres.
Il existe 285 espèces de singes vivant dans le monde. Notre propre espèce : l'humain, fait partie de ce groupe.
Dans le langage courant, le mot « singe » désigne souvent n'importe quel autre singe, à l'exception de l'être humain. La guenon est la femelle du singe, quelle que soit son espèce.

Description

Caractères généraux
Comme tous les primates, les singes ont des mains à cinq doigts (et souvent des pieds aussi) avec un pouce opposable, qui leur permet de s'accrocher aux branches. Ils ont aussi des ongles au lieu des griffes.
Contrairement aux autres primates, les singes ont un nez plutôt qu'une truffe, une mâchoire plus courte et des yeux placés sur le devant de la tête : ils ont donc un visage plutôt qu'un museau1.
Il existe un grand nombre d'espèces de singes très différents. Le plus petit est le ouistiti pygmée : il mesure 12 cm et pèse entre 120 et 130 g. Le gorille, quant à lui, est le plus grand singe du monde. Un singe préhistorique, le gigantopithèque, qui a disparu il y a 400 000 ans, était cependant encore plus grand.

L'œil et la vision
Les singes ont également une très bonne vue : ils partagent un certain nombre de points communs au niveau des yeux qui leur sont très utiles pour vivre dans les arbres et y trouver de la nourriture :
- Chez les primates et tout particulièrement les singes, les yeux se trouvent sur l'avant du crâne, plutôt que sur les côtés : du coup, une bonne partie de ce que voit un œil est vu par l'autre aussi ; cela a deux conséquences :
- les singes ont un champ de vision plus restreint que les autres animaux : ils voient moins de choses à la fois et sont obligés de tourner la tête…
- en revanche, dans la zone de leur champ de vision qui est perçue par les deux yeux en même temps, les singes voient en relief, ce qui est un avantage, surtout lorsque l'on se déplace dans les arbres…
- Chez les singes, les orbites sont complètement fermées, grâce à un os, l'os post-orbital. Les tarsiers, les cousins des singes, ont aussi un os post-orbital, mais leur orbite n'est pas complètement fermée. Chez les autres primates, comme les lémuriens, l'orbite est ouverte. Le fait que l'orbite soit fermée permet à l'œil de moins bouger, d'être plus stable : les singes ont donc une vision plus nette que les autres animaux.
- Les singes et les tarsiers ont également une zone à l'intérieur de l'œil appelée fovéa. La fovéa permet de mieux voir ce qui se trouve au centre de notre champ de vision, avec une netteté beaucoup plus grande.
- Les singes de l'Ancien Monde, dont l'homme fait partie, ont en plus une particularité supplémentaire : leur œil est capable de capter trois couleurs (le bleu, le rouge et le vert), au lieu de deux pour les autres singes (le bleu et le jaune). C'est grâce à ces couleurs que nous sommes capables de reconstituer les couleurs réelles de ce que nous voyons : plus l'œil capte de couleurs, plus la vision des couleurs sera précise. On pense qu'elle est très utile, notamment, pour chercher sa nourriture : en forêt, les jeunes pousses sont souvent rouges, de même que les fruits mûrs : un singe capable de distinguer le rouge du vert peut donc voir des bourgeons bien tendres parmi les feuilles, ou distinguer un fruit mûr d'un fruit vert, alors qu'un singe qui en est incapable, lui, ne verra que du jaune.

Mode de vie
L'homme vit sur tous les continents du monde, à l'exception de l'Antarctique. Les autres singes vivent plus généralement en Amérique du sud, en Afrique et en Asie. Il existe des modes de vie très différents, avec toutefois quelques points communs.

Milieu de vie
Un grand nombre de singes vivent dans les forêts : ils sont arboricoles, c’est-à-dire qu'ils savent grimper et y vivre, dans les arbres. On pense que les premiers singes, nos ancêtres à tous, étaient probablement arboricoles. Par la suite, le groupe s'est diversifié et de nombreux singes se sont adaptés à de nouveaux milieux très différents.
Les singes utilisent généralement leurs mains (ainsi que leurs pieds, qui, contrairement à celui de l'homme, ont souvent aussi un pouce opposable), pour attraper les branches et s'y accrocher. Cela permet aux singes de se déplacer dans les arbres sans tomber. Un grand nombre d'espèces de singes sont assez petits. En cas de danger, ils se réfugient dans les arbres, sur les plus hautes branches, où leurs prédateurs, plus gros et plus lourds, ne peuvent pas venir les chercher.
Certains singes sont tout particulièrement adaptés à la vie dans les arbres et n'en descendent que rarement, voire jamais. Les singes-araignée ont de longs bras, mais aussi une queue qui est capable de s'enrouler autour d'une branche et de s'y accrocher. C'est presque comme une « cinquième main », grâce à laquelle le singe ne tombe jamais ! Il peut ainsi rester suspendu par la queue, pendant qu'il utilise ses mains pour se nourrir, par exemple. Les gibbons, eux, n'ont pas de queue, mais des bras gigantesques, capables de grands mouvements. Ces singes ne marchent quasiment jamais : ils se déplacent en se balançant de branches en branches, suspendus par leurs bras et « volent » littéralement d'arbre en arbre.
D'autres singes plus grands, comme les babouins, vivent plutôt à terre, notamment dans la savane. C'est, notamment, le cas de l'homme, un des rares singes à avoir des jambes plus longues que les bras. Les singes sont capables de coloniser les milieux les plus divers : les géladas, par exemple, vivent sur les hauts plateaux, au sommet des montagnes d'Éthiopie, au bord de très hautes falaises, qui les protègent des prédateurs. Les macaques japonais vivent dans les montagnes et profitent l'hiver des sources chaudes pour se baigner et se réchauffer.
L'homme, enfin, est le seul singe qui se construise un environnement à lui : il bâtit des villes, où il va vivre. D'autres singes se sont d'ailleurs adaptés à la vie en ville, au côté des humains, comme certains macaques, notamment en Asie.

Société
La plupart des singes vivent en groupes, mais ces groupes ne sont pas toujours organisés de la même façon :
- de nombreux singes, comme les babouins ou les géladas, vivent en harem : un mâle vit seul avec un groupe de femelles sur un territoire. Il protège son territoire et ses femelles contre les prédateurs, mais aussi contre les autres mâles, qui voudraient les lui prendre. Ces singes ont souvent des canines transformées en crocs, pour se défendre.
- d'autres singes vivent en famille, des groupes plus petits, composés d'un couple et de ses enfants. C'est généralement le mâle dominant qui dirige la famille ; les enfants, devenus grands, quittent leurs parents pour former leur propre famille. Le bonobo est la seule espèce connue chez qui la société et la famille ne soient pas dirigées par les mâles, mais par les femelles. Les singes se reproduisent toute l'année comme l'homme. Les singes vivent plus de quarante ans et l'homme plus de soixante ans.

Régime alimentaire
La plupart des singes sont végétariens ou omnivores à dominante végétarienne : ils ont des dents très différentes, mais leurs canines servent généralement à se défendre et non pas à chasser leurs proies. Beaucoup de petits singes sont herbivores et se nourrissent exclusivement de tiges et de feuilles. C'est également le cas du gorille.
La plupart des singes mangent des fruits, des graines, des noix. Ceux qui sont omnivores complètent ce régime avec de petits animaux, des insectes… Très peu de singes consomment de la viande : l'homme est un vrai omnivore, qui élève et chasse d'autres animaux pour leur viande, leur lait et leurs œufs. D'autres espèces disparues, comme l'homme de Néandertal, étaient beaucoup plus carnivores et se nourrissaient presque uniquement de viande. En Afrique, certaines tribus de chimpanzés chassent des animaux pour se nourrir.

Intelligence
Les singes comptent parmi les animaux les plus intelligents sur Terre ; l'homme se caractérise parmi les animaux par sa capacité à utiliser un langage, à construire des choses et à utiliser des outils ; on retrouve certaines de ces caractéristiques chez d'autres singes :

Langage
Les singes font des grimaces pour se reconnaître, c'est comme un code. Ils expriment leurs émotions avec des gestes, et des expressions faciales. Les grimaces du singe ont différentes significations, il y en a qui exprime la tristesse, la peur et la colère. Ce langage gestuel est aujourd'hui étudié par les scientifiques : certains tentent mettre de même au point un dictionnaire2. 
Mais on sait aussi que certaines espèces de singes ont un véritable langage : le mone de Campbell, possède son propre langage : ces singes maîtrisent six cris différents, qu'ils utilisent dans des circonstances différentes, un peu comme nous utilisons des mots… En les associant, ils forment un véritable langage, avec des « phrases », destinées à communiquer les uns avec les autres. En étudiant les cercopithèques, des primatologues ont constaté qu'ils avaient des cris différents prédateurs, mais aussi qu'ils pouvaient préciser où se trouvait le prédateur grâce à leur cri3. 
Les scientifiques se sont demandés pourquoi les singes ne pouvaient pas prononcer un langage articulé, comme celui des humains. Le larynx est la partie de la gorge qui permet à l'homme de produire des sons : la voix. Chez les autres singes, le larynx est différent, ils ne peuvent donc pas parler. On a cru longtemps que cette différence dans l'anatomie était la raison pour laquelle les singes ne pouvaient pas parler, mais certains scientifiques pensent aujourd'hui qu'une différence de connexions entre les neurones pourrait être la vraie raison. Pourtant, des primatologues ont réussi à communiquer avec des singes en leur apprenant d'autres types de langages :
- Panzee, un chimpanzé, Panbanisha, un bonobo et surtout Kanzi, son célèbre frère adoptif, sont capables de communiquer avec la scientifique qui les étudie, Sue Savage, grâce à un clavier comportant différents symboles qu'elle leur a appris à utiliser ;
- Washoe, un chimpanzé et surtout Koko, une femelle gorille, ont été capables d'apprendre un langage humain, le langage des signes.

Outils
L'homme n'est pas le seul animal à utiliser des outils… Le chimpanzé, notamment, utilise fréquemment des brindilles pour attraper des fourmis, ou des pierres pour casser les noix. Les scientifiques ont découvert que le chimpanzé utilise ce genre d'outils depuis au moins 4300 ans ! Plus récemment, on a découvert que certains chimpanzés étaient capables d'utiliser une branche comme une sorte de lance, pour attraper de petits galagos.
Lors d'une expérience scientifique, les bonobos Kanzi et Panbanisha ont été capables de créer un outil tranchant pour obtenir de la nourriture, en frappant deux pierres l'une contre l'autre et en se servant de l'éclat pour couper.

Classification
Il existe deux grands groupes de singes : ces deux groupes ont évolué séparément à partir d'ancêtres communs quand l'Océan Atlantique sud s'est formé, entraînant la séparation de l'Amérique du Sud et de l'Afrique, il y a environ 30 millions d'années.
On appelle ces deux groupes de singes les singes du Nouveau Monde, qui vivent en Amérique du Sud et les singes de l'Ancien Monde, qui vivent en Afrique et en Asie, avec une exception, l'homme, qui a depuis peuplé l'ensemble de la planète.
On peut les distinguer d'après la forme de leur nez : les singes du Nouveau Monde ont les narines beaucoup plus écartées que les singes de l'Ancien Monde.
- Singes du Nouveau Monde, ou platyrhiniens : ouistitis, tamarins, sapajou, singes-araignées… ;
- Singes de l'Ancien Monde, ou catarhiniens ;
- cercopithécidés : babouins, macaques, cercopithèques, mangabeys, mandrill, drill, gélada, colobes et nasiques.
- hominoïdes : singes sans queue : gibbons, orang-outan, gorille, chimpanzé, bonobo et homme.
Il existe en tout 8 familles de singes :
- platyrhiniens ;
- les callitrichidés : la famille des ouistitis, des tamarins et des singes lions, notamment.
- les cébidés : la famille des sapajous, les capucins et les saïmiris, ou singes-écureuil
- les aotidés : la famille des douroucoulis, les seuls singes nocturnes au monde.
- les pitheciidés, qui comprennent les sakis et les ouakaris, notamment
- les atélidés, qui rassemblent les singes-araignée, ou atèles et les hurleurs
- catarhiniens ;
- les cercopithécidés : famille des macaques, des babouins, du mandrill, du drill, du gélada, des mangabeys, des entèles, et, bien sûr, des cercopithèques
- les hylobatidés : famille des gibbons et du siamang
- les hominidés : famille de l'homme, du chimpanzé, du bonobo, du gorille et de l'orang-outan
Parmi les primates, les plus proches parents des singes sont les tarsiers.

Les différentes espèces de singes

Liste des espèces4

Galerie

Culture
Les singes, au sens courant du terme (sans l'homme, donc) interviennent dans plusieurs aspects de la culture populaire :

Symbolique
En tant qu'animal le plus proche de l'homme, le singe apparaît souvent comme particulièrement intelligent, mais aussi habile (puisqu'il possède des mains et sait se servir d'outils). Dans de nombreuses cultures, il symbolise la sagesse et l'intelligence.
- Le singe est l'un des douze signes du zodiaque chinois. Il est réputé très malin ;
- Les singes de la sagesse sont un symbole représentant trois singes, dont l'un se cache la bouche, un autre les yeux et le troisième les oreilles. Généralement on associe ce symbole à la maxime suivante : « Ne rien voir, ne rien entendre, ne rien dire ». La légende veut que celui qui suit cette maxime il n'arrivera que du bien.

Mythologie et contes traditionnels
- Le dieu égyptien Thot, dieu de la sagesse, est parfois représenté sous la forme d'un babouin. Le singe est, d'ailleurs, avec l'ibis, le deuxième animal sacré du dieu.
- Dans la religion hindouiste, Hanuman est un dieu-singe, compagnon de Râma et l'un des héros de l'épopée du Rāmāyana. Il est décrit comme très fort (son nom veut dire puissantes mâchoires, en sanskrit) et capable de vaincre les démons. Il est souvent équipé d'une massue.
- Sun Wukong est le héros d'un des plus célèbres contes traditionnels chinois, Le Voyage en Occident. Il s'agit d'un singe aux pouvoirs extraordinaires qui accompagne le bonze Xuanzang dans son voyage. Sun Wukong a peut-être été inspiré de Hanuman, le dieu-singe indien. Il est appelé Son-Goku en japonais et est le héros de nombreuses adaptations de ce conte, notamment le célèbre manga, Dragon Ball.

Singes de fiction célèbres
- King Kong est un gorille géant, héros de plusieurs films américains ;
- Donkey Kong est un gorille de fiction, héros de plusieurs jeux vidéos produits par Nintendo. Inspiré de King Kong, il est d'abord l'ennemi de Mario, sur lequel il jette des tonneaux, avant de devenir le héros de ses propres aventures ;
- Dans le roman de Edgar Rice Burroughs, Tarzan est un homme qui a été élevé par une tribu de singes, les orangs (une espèce imaginaire, intermédiaire entre l'homme, le chimpanzé et le gorille) ;
- Dans Le Roi Lion, de Walt Disney, Rafiki est le sorcier qui met Simba au monde et l'aide à retrouver son royaume. Il se présente comme un babouin, mais ressemble également un peu à un mandrill. Son nom veut dire « ami » en swahili ;
- Dans Le Livre de la jungle, de Rudyard Kipling, on découvre la tribu des singes de Bandar Log. Dans la version de Walt Disney, apparaît le personnage de King Louie, le Roi des Singes, un orang-outan (bien qu'il n'y ait pas d'orang-outans en Inde).

Expressions
- Être malin comme un singe signifie « être très malin » ;
- Faire le singe veut dire : « faire l'imbécile » ;
- Ce n'est pas à un vieux singe que l'on apprend à faire des grimaces : signifie qu'on ne peut pas enseigner à une personne déjà très expérimentée comment faire les choses. Se dit quand une personne essaie de donner des leçons à quelqu'un de plus compétent que lui ;
- Payer en monnaie de singe signifie ne pas payer, trouver un moyen d'éviter de payer : cette expression remonterait à Saint Louis, qui avait autorisé que les saltimbanques puissent s'acquitter de leurs droits de péages pour entrer dans Paris en faisant jouer des tours à leurs singes dressés, plutôt qu'en payant réellement. Par extension, la monnaie de singe peut désigner la fausse monnaie.