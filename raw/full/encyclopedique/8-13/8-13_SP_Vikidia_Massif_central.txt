
45°32′N 2°49′E / 45.533, 2.817
Le Massif central est un massif montagneux au centre de la France. Il s'agit des restes érodés d'une très ancienne chaîne de montagnes, abritant principalement des roches granitiques, sur lesquelles on trouve les traces d'une grande chaîne de volcans, aujourd'hui éteints.
Au Sud, on trouve d'immenses plateaux calcaires : les Grands Causses. Le point culminant est le puy de Sancy (1 886 m), qui est un volcan. Par sa position centrale, les pluies et l'enneigement une partie de l'année, il donne naissance à de nombreuses rivières qui alimentent la Seine, la Loire, le Rhône, la Garonne et la Dordogne. On le surnomme le « château d'eau » de la France.
Le Massif central occupe une position centrale et un rôle de carrefour, en France et au sein de l'Europe : au cours de son histoire, il a servi de point de repère et de frontière, et a souvent été divisé entre plusieurs pays, ou provinces. Il en résulte l'existence de régions très différentes et diversifiées au sein du Massif central, tant du point de vue de l'histoire que de la culture.

Paysages du Massif central
Géographiquement, le Massif central est présent dans dix-huit départements : l'Allier, l'Ardèche, l'Aude, l'Aveyron, le Cantal, la Corrèze, la Creuse, le Gard, l'Hérault, la Loire, la Haute-Loire, le Lot, la Lozère, le Puy-de-Dôme, le Rhône, le Tarn, le Tarn-et-Garonne et enfin la Haute-Vienne. Dans chacun d'entre eux, l'altitude dépasse au moins une fois les 500 m. Le Massif central recouvre 80 000 km², c’est-à-dire environ 1/7e de la France.
Du fait de son étendue et de la durée de son histoire géologique, le Massif central présente de grands ensembles de relief.

Partie Ouest
À l'ouest, le Limousin est formé de plateaux étagés, dominés par le plateau de Millevaches qui culmine à 978 m. Les rivières très nombreuses sont encaissées dans des vallées étroites. C'est un ensemble de petits vallons très humides et verdoyants. Le sol, formé à partir du granite, est peu favorable aux cultures céréalières et convient mieux aux prairies pour l'élevage de bovins destinés à la boucherie. La forêt de châtaigniers et les landes de bruyères forment la végétation naturelle.

Partie Nord

Centre du massif
Au centre, les paysages sont plus variés, en raison de l'histoire géologique du Massif (voir ci-dessous : la formation du Massif central).
Le socle cristallin apparaît pour former des plateaux très élevés (jusqu'à 1 634 m) comme le Forez et la Margeride. Ce sont des régions très rudes en hiver.
Les reliefs volcaniques sont très divers. Le Velay présente des coulées de lave avec des cheminées volcaniques dégagées des matériaux du cône : ce sont les necks et les dykes. La ville du Puy-en-Velay possède ainsi un site très spectaculaire. L'Aubrac est un vaste épanchement de basalte. Les monts d'Auvergne sont des volcans surmontant le socle cristallin. Ces reliefs postiches forment la chaîne des Puys, avec des formes volcaniques assez bien conservées, surtout dans les monts Dôme qui sont de formation plus récente. C'est là que se trouve le puy de Sancy, point culminant du Massif central, avec 1 886 m d'altitude. L'immense volcan du Cantal a été fortement démantelé par l'érosion et ne présente plus que des vestiges de son ancienne importance comme le Plomb du Cantal qui culmine à 1 855 mètres. Les fossés d'effondrement sont de grandes dimensions et forment deux plaines ouvertes vers le nord. La Loire draine une plaine argilo- sableuse, tandis que l'Allier coule dans la plaine des Limagnes aux bons sols formés de débris volcaniques.

Partie Est
À l'est, c'est une succession de hautes terres qui sont des massifs trapus aux allures de plateaux bosselés. Ils sont limités par des failles. Du nord au sud, on distingue le Morvan (902 m), les monts du Charolais, du Mâconnais, du Beaujolais (1 014 m) et du Lyonnais, puis le Vivarais (1 434 m). Ces blocs sont formés de terrains cristallins (comme le granite et le gneiss) appartement au socle datant du Paléozoïque. Ils sont couverts de forêts et de pâturages. Les rebords exposés au sud-est portent des vignobles (Mâconnais et [[vignoble du Beaujolais|Beaujolais). Entre les massifs sont logées des dépressions d'orientation sud-ouest-nord-est. Elles sont dégagées dans des terrains schisteux datant du Paléozoïque. Elles permettent le passage du centre du massif vers les vallées de la Saône et du Rhône. Leur sous-sol est riche en houille et a donné naissance, à partir du XVIIIe siècle, à des villes industrielles comme Le Creusot, Firminy ou Saint-Étienne.

Partie Sud
Le sud du Massif central est partagé entre des régions de roches cristallines et de roches calcaires.
Les Cévennes forment une montagne granitique culminant au mont Lozère, à 1 702 m. Elles se terminent au sud-est par un brusque escarpement taillé dans les schistes, qui est très fortement raviné par les violentes averses d'automne du climat méditerranéen que connaît cette région.
Le Ségala qui s'étend au sud-ouest est aussi formé dans des terrains cristallins, mais son altitude est moins importante (de 1000 à 1200 mètres)
Les Grands Causses sont logés entre les Cévennes et le Ségala. Ce sont des plateaux s'élevant entre 800 et 1200 mètres. Dus à l'empilement de couches calcaires, les Grands Causses offrent un catalogue complet des formes du relief karstique avec ses gouffres (les igues et les avens ou sotchs), ses petites dépressions (les dolines ou sotchs). L'eau est très rare en surface et la végétation forme une maigre pelouse avec des genévriers. Les rivières alimentées par les pluies et les neiges des Cévennes y ont creusé des vallées très profondes et très étroites : les gorges (comme celles du Tarn). C'est une région difficile à vivre, aussi bien en été qu'en hiver, où il fait très froid.

Faune et flore
Les paysages du Massif central sont marqués par une faune et une flore très riches.

Les plantes
On trouve dans le Massif central un grand nombre de plantes, dont certaines sont rares :
- dans les tourbières, on peut trouver des plantes carnivores, comme le droséra, notamment au plateau de Millevaches ;
- les hauts sommets abritent des espèces qui vivent là depuis la dernière période glaciaire, comme le saule arctique ou la linaigrette.

Les animaux

Mammifères
- loutre

Oiseaux
- circaète Jean-le-Blanc

Reptiles et amphibiens
- lézard vivipare ;
- vipère péliade ;
- crapaud sonneur à ventre jaune.

Géologie
Le Massif central est une montagne très ancienne, qui ne s'est pas formée en une seule fois. La plus grande partie du Massif central, cependant, est ce qui reste d'une montagne encore plus grande, qui a aujourd'hui disparu.
Il y a environ 300 millions d'années, au Permien, une immense chaîne de montagnes, la Chaîne hercynienne, traversait ce qui est aujourd'hui l'Europe. C'était une très grande chaîne de montagnes, qui devait ressembler à l'Himalaya aujourd'hui. À cette époque, les Alpes et les Pyrénées n'existaient pas encore.
Au cours du temps, la pluie, la neige, le vent, ont fini par user petit à petit cette chaîne de montagne, qui a fini par disparaître : c'est l'érosion. Aujourd'hui, ce qui reste de cette vaste chaîne de montagnes est divisé en petits massifs, comme le Massif armoricain, ou les Vosges… Le plus vaste de tous ces massifs est bien sûr le Massif central, qui occupe aujourd'hui le centre de la France.
Le niveau de la mer a beaucoup varié au cours du temps : à plusieurs reprises, il a été beaucoup plus haut qu'aujourd'hui, si bien que ce qui constitue aujourd'hui l'Europe était recouvert par la mer. Les montagnes qui existent sont les seules à dépasser au-dessus du niveau de la mer et forment des îles. À plusieurs reprises, notamment au Crétacé, (il y a environ 80 millions d'années), le Massif central est devenu un archipel ! Durant ces périodes où le niveau de la mer est très haut, du calcaire se dépose dans le Massif central et forme les grands plateaux que l'on appelle les causses.

Volcanisme
Il y a environ 35 millions d'années, un grand épisode volcanique se déroule dans l'est et le nord du Massif central : plusieurs volcans voient le jour. Il en existe deux types :
- des volcans qui créent des cônes hauts et abrupts, appelés puys ;
- des volcans qui ne créent quasiment pas de cône, mais qui libèrent d'énormes coulées de lave très liquide.
Le plus gros de cet épisode volcanique a eu lieu il y a entre 6 et 13 millions d'années. Aujourd'hui, ces volcans ne sont plus actifs, mais l'activité volcanique existe encore : pour preuve, certaines sources d'eau du Massif central sont des sources réchauffées par l'activité volcanique !
Ces anciens volcans forment aujourd'hui la chaîne des Puys, en Auvergne.
Il est possible que les premiers humains qui ont peuplé le Massif central aient été témoins de ce volcanisme, notamment les hommes de Néandertal.

Histoire

Préhistoire

L'homme de Néanderthal
L'homme de Néanderthal a été la première espèce d'être humain à s'installer dans le Massif central. Il est apparu en Europe il y a environ 400 000 ans et y a vécu jusqu'à il y a environ 40 000 ans. Il a laissé des indices de sa présence, notamment dans le Massif central :
- à La Chapelle-aux-Saints, en Corrèze, on a découvert le fossile d'un homme de Néanderthal, qui y a vécu, il y a environ 45 000 ans ;
- le site du Moustier, en Dordogne, est également célèbre. On y a découvert plusieurs squelettes d'hommes de Néanderthal, mais, surtout, des outils de pierre taillée. Le site du Moustier a d'ailleurs donné son nom à ce type d'outils : on parle d'outils moustériens, ou d'industrie moustérienne ;
- toujours en Dordogne, à La Ferrassie, on a découvert une nécropole d'hommes de Néanderthal, c’est-à-dire un lieu où les morts étaient enterrés. Cela prouve que l'homme de Néanderthal enterrait ses morts. Huit squelettes d'hommes de Néanderthal ont été retrouvés, qui ont vécu il y a entre 75 000 et 60 000 ans, durant le Moustérien.

L'Homo sapiens
Il y a environ 40 000 ans, l'homme de Néanderthal (Homo neandertalensis) est peu à peu remplacé par l' Homo sapiens, ou Homme moderne, notre espèce. L’Homo sapiens peuple peu à peu le Massif central, il y a environ 30 000 à 35 000 ans et il y a laissé beaucoup de sites préhistoriques célèbres, qui ont permis de mieux le connaître.
- C'est aux Eyzies-de-Tayac, en Dordogne, qu'ont été découverts, en 1868, dans une petite caverne creusée dans la falaise, l'abri sous-roche de Cro-Magnon, les restes de cinq hommes préhistoriques. Le site a donné son nom à l'une des espèces d'hommes préhistoriques les plus célèbres, notre espèce, l'homme de Cro-Magnon. Les Homo sapiens ont peuplé l'abri sous-roche de Cro-Magnon il y a environ 35 000 ans.
- On connaît plusieurs grottes célèbres, qui ont été occupées et décorées de peintures par les hommes préhistoriques, comme la fameuse grotte de Lascaux, en Dordogne, ou la grotte Chauvet, en Ardèche, qui sont décorées de peintures rupestres. Les hommes préhistoriques n'habitaient pas vraiment les cavernes, mais plutôt à leur entrée : elles leur servaient plutôt de refuge, ou de site sacré, un peu comme les temples d'aujourd'hui…
Durant le Néolithique, de nombreux dolmens, menhirs et pierres dressées ont été érigés dans le Massif central, par exemple dans le Gévaudan. Les 154 menhirs de la Cham des Bondons en font le deuxième plus grand site mégalithique de France, après les alignements de Carnac, en Bretagne.

Antiquité
Durant l'Antiquité, le Massif central est occupé par plusieurs peuples, notamment :
- les Arvernes, au nord, dans ce qui est aujourd'hui l'Auvergne ;
- les Gabales, au centre, dans la province du Gévaudan (qui correspond aujourd'hui au département de la Lozère) ;
- les Vélaves, à l'est, dans le Velay (aujourd'hui la Haute-Loire) ;
- les Helviens, dans le Vivarais (aujourd'hui, l'Ardèche) ;
- les Rutènes, au sud, dans ce qui est aujourd'hui le Tarn et l'Aveyron ;
- les Lémovices, à l'ouest, dans ce qui est aujourd'hui la Haute-Vienne, la Creuse et la Corrèze (leur territoire débordait également du Massif Central) ;
- les Pétrocores, en Périgord (actuel département de la Dordogne) ;
- les Cadurques, dans le Quercy.
Les territoires occupés par ces différentes tribus formeront les futures régions du Massif central tel que nous le connaissons.
Au Ier siècle avant J.-C., Jules César, général romain, tente de conquérir un vaste territoire qu'il appelle la Gaule (Gallia, en latin) et qui recouvre les territoires de ces tribus et de plusieurs autres, qu'il appelle les « Gaulois ». Vercingétorix, le chef de la tribu des Arvernes, réussira à convaincre les autres chefs de tribus de s'allier à lui, pour combattre César.
C'est notamment sur le plateau de Gergovie, en Auvergne, qu'a lieu une grande bataille opposant Vercingétorix et ses alliés, à Jules César, en 52 av. J.-C.. Vercingétorix y remporte une grande victoire, mais sera finalement vaincu quelques mois plus tard, à Alésia.
Après la victoire de César, les Romains s'installent dans le territoire conquis et se mêlent à la population locale, pour former la civilisation gallo-romaine. La Gaule est divisée entre deux grandes Provinces romaines : la Gaule narbonnaise et la Gaule romaine, puis en trois, la Narbonnaise, la Lyonnaise et l'Aquitaine, après le partage de la Gaule romaine, sous le règne d'Auguste. Le Massif central, avec ses reliefs, sert de frontière et se retrouve lui aussi divisé en trois.

Moyen Âge
À la fin de l'Empire romain, au Ve siècle après J.-C., ce qui était la Gaule est divisé en plusieurs royaumes : le royaume des Francs, celui des Alamans, des Burgondes et des Wisigoths. Le Massif central est alors partagé entre les Burgondes - qui occupent la région de Lyon et la future Bourgogne - et les Wisigoths, en Aquitaine.
En 507, Clovis, roi des Francs, allié aux Burgondes, remporte la bataille de Vouillé, près de Poitiers, contre les Wisigoths. Après avoir tué Alaric II, roi des Wisigoths, il s'empare d'une grande partie de leur territoire, notamment l'Aquitaine, ainsi que tout l'ouest du Massif central (l'Est restant contrôlé par ses alliés, les Burgondes).
Selon la tradition franque, ce n'était pas l'aîné des enfants qui héritait du trône à la mort de son père : celui-ci était plutôt partagé entre les différents enfants. Durant de nombreuses années, les frontières des différents royaumes francs vont beaucoup changer, sous le règne des différents rois mérovingiens qui les gouvernent.
L'Auvergne, notamment, revient à Thierry, fils de Clovis, mais les Auvergnats refusent de lui obéir. Alliés aux Wisigoths, ou aux autres rois francs, ils se révoltent contre Thierry. Il s'ensuit une longue période de guerres, de sièges et de pillages en Auvergne.
Après la mort de Thierry, son fils, Théodebert, roi d'Austrasie, allié à son oncle Childebert, s'empare du royaume des Burgondes, qui regroupe l'actuelle Bourgogne, ainsi que les territoires situés à l'est du Massif central. Ils créent ainsi le royaume franc de Bourgogne.
En 613, Clotaire II, roi de Neustrie, s'empare de l'Austrasie, ainsi que de l'Aquitaine. Il devient ainsi le premier roi de tous les Francs, depuis Clovis. C'est la première fois que la totalité du Massif central se retrouve au sein d'un seul et même pays.
Au VIIIe siècle, les musulmans, qui ont conquis toute la péninsule ibérique, traversent les Pyrénées et envahissent le royaume des Francs. En 725, ils ont conquis tous les territoires autour du Massif central : l'Aquitaine et la vallée du Rhône, jusqu'en Bourgogne. Le maire du Palais, Charles Martel, arrête les musulmans à côté de Poitiers, en 732. Charles Martel deviendra ensuite roi des Francs, à la suite du dernier roi mérovingien, Thierry IV. Il crée ainsi la dynastie des rois carolingiens.

Temps modernes
Aux XVIIe et XVIIIe siècles, la Jacquerie des croquants est une série de révoltes du peuple, surnommé « les croquants », et qui a notamment touché plusieurs régions du Massif central :
- les révoltes de 1593–1595, initiées dans le Limousin, gagnent peu à peu le Périgord : les paysans se plaignent de payer trop d'impôts ;
- en 1624, une révolte similaire a lieu dans le Quercy : à cette époque, le Quercy ne payait pas la gabelle, un impôt sur le sel. À la suite à l'annulation de ce privilège, beaucoup de paysans se rebellèrent : armés de piques et de fourches, ils attaquèrent et incendièrent les propriétés des collecteurs d'impôts. Mais ils furent vaincus et leurs chefs exécutés, sur ordre de Richelieu ;
- en 1636, en Angoumois et Périgord, les paysans refusent de se soumettre à la taille, l'un des principaux impôts. Ils se révoltent et déclenchent une véritable guerre civile, qui durera presque deux ans ;
- en 1707, une nouvelle révolte des croquants eut lieu dans le Quercy, par suite de la mise en place d'un nouvel impôt. Elle fut surnommée "la révolte des Tard-avisés".

Époque contemporaine
Durant la Seconde Guerre mondiale, le Massif central va jouer un rôle majeur : en effet, lors de l'armistice du 22 juin 1940, est établie une ligne de démarcation qui sépare la France en deux, une zone libre administrée par le gouvernement de Vichy, au sud et une zone occupée par les Allemands, au nord. Cette ligne de démarcation suit sensiblement le tracé de la limite nord du Massif central.
C'est la ville de Vichy, en Auvergne, qui est choisie comme capitale de l'État français, dirigé par le maréchal Pétain. Le régime de Vichy durera jusqu'en 1944.

Économie

L'agriculture
Le secteur primaire est particulièrement développé dans le Massif central, qui regroupe certains des départements les moins peuplés de France. On y trouve notamment des régions particulièrement rurales, qui se sont spécialisées dans l'agriculture et notamment dans l'élevage.

L'élevage
Le Massif central compte beaucoup d'herbages qui conviennent parfaitement pour faire brouter les troupeaux, comme les plateaux des Causses, ou encore les Ballons. L'élevage est l'activité du secteur primaire qui est la plus développée dans le Massif central. On élève principalement des vaches, mais aussi des brebis et des chèvres, qui sont élevées pour leur viande et pour leur lait, utilisé pour faire des fromages.
Les brebis élevées dans le Massif central représentent 30 % de la production nationale en France.
Plusieurs races d'animaux sont spécifiques du Massif central, notamment :
- des races bovines (vaches) :
- la Salers, une race de vache, originaire du Cantal. Elle se reconnaît à sa couleur rouge et à ses grandes cornes. C'est une variété très rustique, qui produit aussi bien du lait que de la viande,
- la limousine, originaire, comme son nom l'indique, du Limousin,
- l'Aubrac, originaire des monts de l'Aubrac, dans le sud du Massif central : Aveyron, Cantal, Lozère et Haute-Loire ;
- des races ovines (moutons) :
- la brebis blanche du Massif central est une race de brebis, qui, comme son nom l'indique, est blanche. Ni la brebis, ni le bélier ne portent de cornes. C'est une race qui fournit aussi bien du lait que de la viande. Il y en aurait environ 350 000 dans le Massif central et ses environs,
- la grivette,
- la limousine, originaire du plateau des Millevaches,
- la brebis noire du Velay,
- la rava,
- la bizet ;
- des races caprines (chèvres) :
- la chèvre du Massif central, ou « Massif central », est une chèvre originaire, comme son nom l'indique, du Massif central,
- la blanche des Cévennes.

Les fromages
Le Massif central est réputé depuis l'Antiquité pour ses fromages ; le mot « fourme » vient du latin forma, qui a également donné le mot français « fromage ». Pline l'Ancien, dans le livre XI de son Histoire Naturelle, écrit que les fromages les plus appréciés à Rome sont ceux qui viennent du Massif central. On raconte que Jules César et même Charlemagne, appréciaient tout particulièrement les fromages de cette région :
- des fromages de vache :
- cantal,
- bleu des Causses,
- bleu d'Auvergne,
- Saint-nectaire,
- fourme d'Ambert,
- fourme de Cantal,
- fourme de Rochefort,
- le Laguiole, ou Fourme de Laguiole ;
- des fromages de brebis :
- Roquefort,
- Pérail ;
- des fromages de chèvre :
- Pélardon.

Les vins
Si l'élevage est dominant dans le Massif central, on pratique également les cultures et notamment celle de la vigne, afin de produire du vin. Plusieurs vins du Massif central sont réputés, notamment :
- le beaujolais, notamment le beaujolais nouveau qui c'est un vin jeune, qui se consomme l'année même, le troisième jeudi de novembre ; les autres crus de beaujolais peuvent se conserver plusieurs années.

Galerie

L'eau
Surnommé « le château d'eau de la France », le Massif central est une région très riche en sources, lacs et cours d'eau. Sa géologie très particulière confère certaines propriétés à cette eau : ainsi, les lacs installés dans les anciens cratères des volcans, en Auvergne, contiennent beaucoup de minéraux. Dans les Causses calcaires, beaucoup d'eau circule dans les réseaux karstiques, plus ou moins riches en dioxyde de carbone : certaines de ces sources contiennent même naturellement des bulles de gaz carbonique dissous !
On trouve ainsi plusieurs sortes de sources dans le Massif central :
- des sources d'eau plate ;
- des sources d'eau gazeuse naturelle ;
- des sources d'eau chaude naturelle, en raison de la présence de volcans.
L'eau a également permis l'implantation d'industries dans le Massif central.

Stations thermales
Les Romains connaissaient déjà les propriétés des sources chaudes et froides du Massif central, chargées de minéraux. Ils construisirent des thermes, des bains chauds, notamment à proximité de la ville de Vichy, en Auvergne.
Aujourd'hui encore, l'Auvergne compte plusieurs stations thermales, qui attirent les touristes.
La source de Rouzat est également connue depuis l'Antiquité. On dit qu'elle a été découverte par les Romains, lors de la conquête des Gaules. Aujourd'hui, on y capte l'eau Rozana. On peut encore y visiter la source des Romains, qui y ont construit des thermes, ainsi qu'un aqueduc.

Eaux de sources
Plusieurs sources potables, d'eau plate ou gazeuse, sont exploitées dans le Massif central. Certaines sont très réputées pour leurs sels minéraux :
- eaux plates :
- Volvic,
- certaines sources d'eau plate sont également exploitées par la marque Cristaline, comme la source Saint-Médard.
- eaux gazeuses naturelles :
- Quézac,
- Arvie,
- Arcens,
- Vichy Célestins,
- Saint-Yorre,
- Reine des Basaltes,
- Rozana,
- Sainte-Marguerite,
- Vals.

L'industrie
Quelques industries sont implantées de manière historique dans le Massif central :
- Michelin est une des plus grandes entreprises françaises. Implantée à Clermont-Ferrand, cette entreprise est le premier producteur mondial de pneumatiques ;
- la coutellerie : la ville de Thiers, dans le Puy-de-Dôme, est la capitale de la coutellerie en France : on y produit des couteaux très réputés. La ville de Laguiole, également très réputée pour ses couteaux, se situe dans l'Aveyron, également dans le Massif central ;
- le pôle européen de la céramique est basé à Limoges, capitale historique de la céramique. On y fabrique de la vaisselle et des couverts, mais aussi des prothèses dentaires et de nouveaux matériaux à base de céramique.

Le tourisme
Le tourisme tend à se développer de plus en plus dans le Massif central. Ce qui attire les touristes, ce sont les paysages, la gastronomie…
Activités que l'on peut pratiquer dans le Massif central :
- randonnée ;
- tourisme sportif : spéléologie, canyonisme, kayak et rafting dans les gorges de l'Ardèche et du Tarn ;
- pêche ;
- découverte gastronomique ;
- cure thermale.
Il existe quelques stations de sports d'hiver dans le Massif central : La Loge des Gardes, Chalmazel, Prabouré / Saint-Anthème, Le Mont-Dore, Besse, Chastreix, Le Lioran, Laguiole Brameloup, Les Estables / Menzec, Croix de Bauzon, Mont Lozère et Prat Peyrot.

Culture

Traditions
La bourrée est une danse traditionnelle, originaire du Massif central. Il en existe plusieurs variantes, selon les régions : bourrée auvergnate, ou la bourrée du Bourbonnais (qui correspond actuellement à l'Allier).

Mythes et légendes
La Bête du Gévaudan est un animal qui a attaqué plusieurs personnes au XVIIIe siècle et semé la terreur en Lozère. Il semble que la créature était un loup, mais beaucoup de légendes courent à son sujet.
Le site de Cotteughe, dans le Cantal, abrite les ruines d'une ville dont on ignore l'origine. La légende dit que la ville avait été construite par des fées, qui furent contraintes de l'abandonner, en y laissant tous leurs trésors. On raconte qu'un montagnard, s'étant égaré, y aperçut une petite vieille traînant derrière elle une lourde marmite remplie d'or et qui disparu dès qu'elle l'aperçut. Selon la légende, le trésor des fées seraient caché dans un souterrain fermé par une dalle portant un anneau de bronze, qu'il n'est possible de découvrir et d'ouvrir que le Jeudi saint ou le dimanche de Pâques. Toujours selon la légende, il y aurait, parmi le trésor de Cotteughe, une table en or. Si elle était découverte, de grands malheurs s'abattraient sur la région.
Durant le moyen-âge, il y eut plusieurs procès pour sorcellerie dans le Massif central. Une sorcière condamnée au bûcher raconta que le puy de Dôme servait de point de rendez-vous à tous les sorciers du Massif central pour leurs messes noires. Au cratère du Nid de la Poule, tout près du puy de Dôme, une poule noire à trois queues, symbole du diable, serait apparue et aurait pondu trois œufs noirs avant de disparaître dans les flammes.