
Le bouddhisme est une religion, une philosophie ou une pratique basée sur la méditation. Il a été fondé par Siddharta Gautama, plus connu sous le nom de Bouddha, et concerne aujourd'hui plusieurs centaines de millions de personnes (entre 230 millions et 500 millions, selon un sondage datant de 2005)1.
Le bouddhisme naît vers -556 en Inde, fondé par Bouddha2.

Histoire

Origines au VIe siècle avJC
Le bouddhisme est né des expériences et réflexions de Siddhartha Gautama, un prince indien du VIe siècle av. J.-C.. Après avoir mené la vie agréable de l'aristocratie indienne, à 29 ans il change d'attitude et fait une série d'expériences qui devraient le mener au Nirvana. Par la suite, il convainc de nombreuses personnes de se joindre à sa philosophie, si bien qu'à sa mort, elle a déjà beaucoup d'adeptes : le bouddhisme est né2. Bouddha meurt vers -4803.

Essor (-525 — 800)
Après la mort de Bouddha, la philosophie bouddhiste se répand en Inde du nord-est2.
La philosophie bouddhiste se transmet avant tout oralement, depuis -525, date à laquelle Bouddha prononce un discours dans lequel il transmet les Quatre Nobles Vérités à ses cinq premiers disciples3,4. Les textes sur le sujet du bouddhisme, appelés Tipitaka, sont rares et peu fiables ; c'est pourquoi on sait peu de choses du bouddhisme à cette époque-là4.
En -370 a lieu la deuxième réunion des adeptes, le concile de Vaisali3. Les moines n'étant alors pas d'accord entre eux3, bouddhisme se sépare en deux courants : les Mahasamghika et les Sthaviravadin4. En -249, une troisième assemblée se déroule sous le règne d'Asoka ; ce roi, lassé des guerres, veut établir une politique basée sur le bouddhisme4 et veut le faire connaître au-delà des frontières3. Des moines bouddhiques, comme le fils d'Asoka, commencent à convertir les peuples à l'enseignement du bouddhisme sur tout le continent, le faisant ainsi connaître4. Il s'étend alors, grâce à Asoka et sur une période de plusieurs siècles, sur tout le territoire asiatique, grâce s'enrichissant des éléments culturels de nombreuses régions comme l'Extrême-Orient (Chine, Japon, Thaïlande, Polynésie...) ou l'Himalaya2,4. En -242, le bouddhisme atteint Ceylan (aujourd'hui Sri Lanka)3.
Si le bouddhisme continue à se répandre au cours des siècles suivants, c'est parce qu'il offre la perspective d'une délivrance immédiate, c'est-à-dire que Bouddha devrait mettre fin aux souffrances d'ici peu de temps ; c'est une croyance, mais cela séduit les gens4. De plus, il n'implique pas de rite et est très tolérant envers ses fidèles4.
Le courant bouddhique atteint la Chine vers 65, mais il ne s'y implantera durablement qu'au Ve siècle (entre 400 et 500)4. Plus tard, il atteint le Viêt Nam et la Corée4. Cette dernière n'adopte réellement le bouddhisme qu'au VIe siècle (entre 500 et 600) et le transmet au Japon en 552 (ce dernier fera du bouddhisme la religion officielle en 594)4,3. C'est la branche du bouddhisme tantrique, grâce au moine Padmasambhava, qui atteint le Tibet en 7474,3.

Déclin et évolution (600 — aujourd'hui)
En Inde, où il a été fondé, le bouddhisme est victime, aux VIIe et VIIIe siècles (entre 600 et 800), des plaintes des brahmanes, les hommes cultivés du pays, qui n'acceptent pas cette religion ; cet évènement amorce le déclin du bouddhisme en Inde (aujourd'hui, l'Inde a 80,7 % de pratiquants hindouistes, et seulement 0,6 % de bouddhistes)4. Au IXe siècle (entre 800 et 900), la Chine connaît des troubles graves pendants lesquels des endroits sacrés sont détruits3 ; l'empereur Wuzong interdit la pratique de cultes étrangers, dont le bouddhisme, en 8454. Au XIIe siècle (entre 1100 et 1200), l'Islam atteint l'Asie ; c'est la disparition du bouddhisme dans la majeure partie du territoire4.
Mais cet apparent déclin permet au bouddhisme de se renouveler, d'évoluer et d'atteindre de nouveaux pays4. Par exemple, au XIIe siècle (entre 1100 et 1200), au Japon, il a connu des modifications qui ont fait de lui une religion répandue dans le pays4,3.
Entre 1357 et 1419, vit le moine Lobsang Dragpa3. Il devient une personnalité historique importante pour le bouddhisme car il défend la vie monastique mais conseille aussi une éducation des jeunes dans tous les domaines3.
En 1870, l'empereur du Japon fait d'une religion, issue du bouddhisme, sa religion officielle3.
Malgré les troubles qu'il a connus, le bouddhisme est resté ancré dans les territoires qu'il a conquis, car il propose un enseignement pratique, tolérant et pas contraignant, et constitue une philosophie qui répond à sa façon aux grandes questions, comme celle du rôle de l'homme sur la Terre4.
Au XXe siècle (entre 1900 et 2000), il est présent un peu partout en Asie, sous différentes formes4. Il réapparait en Chine, malgré les restrictions gouvernementales, et en Inde à partir des années 19504. Il est de plus en plus pratiqué au Moyen-Orient (Turquie, Arabie Saoudite, Afghanistan...)4. Au XXIe siècle (entre 2000 et 2100), il est surtout présent en Thaïlande, en Birmanie, au Sri Lanka, au Népal et au Japon4.

Dalaï-lama
Le dalaï-lama est un moine bouddhiste qui est considéré comme le chef spirituel du Tibet, et politique depuis le XVIIe siècle (entre 1600 et 1700)5. En tout, 14 dalaï-lama se sont succédé depuis 1391. C'est le 17 novembre 1950 que Thubten Gyatso, le 14e élu, accède au pouvoir par anticipation.

Les différents courants bouddhiques
Au cours de son existence, la philosophie et la religion bouddhiques (ou bouddhistes) ont évolué et se sont divisées en plusieurs courants, chacun différent des autres dans l'enseignement qu'il propose. Parmi les principaux courants, ou écoles, on peut citer4 :
- le Grand Véhicule, ou Mahayana, qui est répandu et ouvert à tous ;
- le Petit Véhicule, ou Hinayana, qui est réservé aux adeptes les plus fidèles ;
- le Véhicule de Diamant, ou Vajrayana, apparu au VIIe siècle (entre 600 et 700), qui est influencé par l'hindouisme et qui implique des traditions rituelles.

Art gréco-bouddhique
Quand sont apparues les premières représentations de Bouddha (statues notamment), un nouvel art, l'art bouddhique, est né6. Très vite, entre -160 et -135, cet art s'est mélangé aux grecs envoyés en Inde pour les conquêtes d'Alexandre le Grand pour devenir l'art gréco-bouddhique7, qui dura jusqu'au VIe siècle (entre 600 et 700) à certains endroits.