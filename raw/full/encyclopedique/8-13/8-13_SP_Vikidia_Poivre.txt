
Le poivre est une épice, au goût chaud, fort, et piquant. C'est une des épices les plus connues et les plus utilisées dans le monde. Il provient d'une plante appelée poivrier.
Il existe plusieurs sortes de poivre : les différentes espèces de poivrier produisent des variétés différentes de poivres. D'autres épices, provenant d'autres plantes, et dont le goût rappelle celui du poivre, sont également appelées « poivre », même si elles n'en sont pas réellement.
Afin de développer ses arômes, le poivre doit être moulu au dernier moment avant de s'en servir.

Les différentes sortes de poivre

Les variétés du poivrier noir
Le poivrier noir est une plante, dont le nom scientifique est Piper nigrum. C'est une liane qui est beaucoup cultivée de par le monde : on le fait pousser sur un support appelé treille. À partir de l'âge de sept ou huit ans, le poivrier produit des grappes de petits fruits verts, qui donnent différentes variétés de poivre, selon la façon dont ils sont préparés :
- Le poivre noir est obtenu à partir du fruit entier, cueilli un peu avant qu'il soit mûr. Il est ensuite séché au soleil, et son enveloppe, le péricarpe, durcit, devient tout ridé, et noir.
- Le poivre blanc est obtenu à partir du fruit mûr, débarrassé de son enveloppe, et séché. L'intérieur du fruit apparaît alors blanc. Il est plus fin au goût et moins piquant que le poivre noir.
- Le poivre gris est du poivre noir, moulu (il n'existe donc qu'en poudre). La couleur noire de l'enveloppe, et blanche de l'intérieur du fruit, se mélangent pour donner cette couleur grise.
- Le poivre vert est formé des fruits encore verts (pas mûrs), cueillis à environ 6 mois. On l'utilise frais, en grain, ou bien séché. En Asie, on utilise les grappes de poivre vert entières. Son goût est doux, fruité et épicé.
- Le poivre rouge, également appelé poivre de Pondichéry : il est obtenu à partir du fruit, bien mûr. Il est séché à l'abri de la lumière afin de lui conserver sa couleur rouge.Il a des parfums d'agrumes et d'ananas. Le fruit du poivrier met 9 mois à mûrir et à devenir rouge. C'est une variété rare et coûteuse de poivre.

Les autres poivres
Dans la famille du poivrier (les pipéracées), il existe aussi d'autres espèces de plantes, qui donnent elles aussi des variétés différentes de poivre. Ce sont :
- Le cubèbe, ou poivre à queue, est le fruit du poivrier Piper cubeba. Son goût est plus fort que le poivre noir. On l'utilise dans la cuisine asiatique, notamment dans le curry. Les fruits sont récoltés encore verts, puis séchés. On l'appelle aussi embèbe, poivre de Java, ou poivre du Kissi.
- Le poivre long provient lui aussi d'une autre espèce de poivrier, Piper longum. Contrairement aux autres poivres, ce ne sont pas ses fruits, que l'on utilise, mais les chatons, c'est-à-dire les grappes de fleurs mâles, qui sont récoltées et séchées. Cette variété de poivre est un peu moins forte que les autres, et son goût rappelle la cannelle.
- Le voatsiperifery, ou poivre sauvage est le fruit du poivrier Piper borbonense. C'est une liane qui pousse à l'état sauvage à Madagascar. Grimpant sur les arbres, il peut atteindre une hauteur de 20 m de haut. Ses fruits sont récoltés par les malgaches. Ils ressemblent à ceux du poivre à queue, mais plus petits. Son goût piquant est plus frais et citronné que celui du cubèbe.

Les « faux poivres »
Certaines épices au goût piquant, proche de celui du poivre, ou utilisé comme tel, ne viennent pas des poivriers, mais d'autres plantes : ce ne sont donc pas de "vrais" poivres. Parmi ces espèces, on peut citer notamment :
- Le poivre rose, ou baie rose : Il s'agit des baies d'un arbre, le faux-poivrier, dont il existe deux espèces (nom scientifique : Schinus molle et Schinus terebinthifolius), qui font partie de la même famille que le pistachier. Originaires d'Amérique du Sud, ces arbres sont cultivés au Pérou, mais aussi à Madagascar, et surtout à la Réunion, d'où son autre nom de poivre de la Réunion, ou poivre Bourbon ;
- Le poivre de Guinée, ou poivre de paradis, ou maniguette ;
- Le poivre des moines ;
- Le poivre du Sichuan, est le fruit d'un petit arbuste de la famille des rutacées (qui produit des agrumes)
- Le poivre de Cayenne, qui est un petit piment (de la famille des solanacées) qui est séché et réduit en poudre. Il est utilisé dans la fabrication du tabasco et de la harissa.

Histoire
Durant l'Antiquité et le Moyen Âge, les épices, et tout particulièrement le poivre, étaient des marchandises très rares et chères, importées d'Asie. À cette époque, on ne disposait pas des moyens modernes pour conserver les aliments, surtout la viande. Les épices servaient non seulement à donner du goût à un plat, mais aussi à conserver les aliments, ou à masquer le goût d'une viande pas très fraîche.
Au Moyen Âge, le poivre était si cher que l'on avait l'habitude de dire "cher comme poivre", pour parler de quelque chose de très cher. C'est également de cette époque que date l'expression payer en épices, qui s'est quelque peu modifiée pour devenir "payer en espèces".
Le poivre est, avec la cannelle, une des épices qui sont connues depuis le plus longtemps : le philosophe grec Théophraste en parle, au IVe siècle avant J.-C., sous le nom de peperi.
Selon une légende, Cléopâtre aurait séduit Jules César au moyen de plats riches en poivre, réputé pour ses vertus aphrodisiaques.1
Le poivre long a été une des premières épices importées en Europe. Son nom indien, pippali, est d'ailleurs à l'origine du mot peperi. Il sera peu à peu remplacé par les autres variétés de poivres, notamment le poivre noir, et oublié pendant longtemps : en effet, contrairement au poivre en grains, le poivre long n'est pas très facile à moudre....
Ce sont les marchands arabes qui vont, durant longtemps, contrôler la route des épices, par laquelle l'Europe et le Moyen-Orient sont approvisionnés en poivre et autres épices, jusqu'au XIIème siècle, au cours des croisades. Au XVe siècle, les Portugais tentent d'ouvrir une nouvelle route pour l'approvisionnement des épices, par voie maritime, cette fois. C'est l'époque des grandes découvertes maritimes, et des grands explorateurs, comme Vasco de Gama, qui atteindra l'Inde en 1498, en passant par le cap de Bonne Espérance (au sud de l'Afrique), et ouvrira une autre voie d'approvisionnement des épices.
En 1492, Christophe Colomb cherche également à rejoindre l'Inde et ses épices, en empruntant une autre route, faisant le tour du monde. Il découvrira ainsi accidentellement les Antilles, et de nouvelles épices. Il ramène notamment une nouvelle épice au goût piquant, qu'il appelle poivre de Jamaïque (bien qu'il ne s'agisse pas réellement de poivre), parce que le poivre est une épice très chère, à cette époque.
Au XVIIe siècle, Colbert, ministre de Louis XIV, crée la Compagnie des Indes Orientales, dans le but de permettre le commerce avec l'Asie, et notamment l'approvisionnement en épices. 
Au XVIIIe siècle, le botaniste Pierre Poivre, au nom prédestiné, parviendra à ramener et à cultiver des plants de poivrier, de giroflier et de muscadier sur l'île Maurice.

Propriétés
Le goût du poivre est dû principalement à une substance, la pipérine, qui est contenue dans les vrais poivres, et quelques autres épices. Elle procure une sensation de brûlure, de chaleur, et de piquant sur la langue.
L'échelle de Scoville, qui est utilisée pour mesurer la force des piments, peut également être utilisée pour le poivre.
Le poivre est réputé pour ses vertus cicatrisantes, c'est pourquoi, autrefois, on en versait sur les plaies. Mais cette propriété n'a jamais été vraiment correctement expliquée. En revanche, on sait que le poivre, en quantité raisonnable, est bon pour la santé : il permet notamment d'éviter certains cancers.
Depuis l'Antiquité, le poivre est également réputé pour ses vertus aphrodisiaques, c'est-à-dire qu'il est sensé provoquer le désir amoureux. On dit d'ailleurs que la Reine Cléopâtre utilisait beaucoup le poivre dans ses plats pour séduire ses amants, et notamment Jules César. Le poivre était aussi réputé pour lutter contre les maladies vénériennes.

Production
Le poivre est cultivé dans les pays tropicaux. Les principaux producteurs sont les pays dAsie du Sud-Est, d'Afrique et d'Amérique du Sud2.
En 2008, les trois plus gros producteurs de poivre étaient le Vietnam, l'Indonésie et le Brésil, qui en ont produit plus de 60 milliards de tonnes. Ils sont suivis par l'Inde, la Chine et la Malaisie.
Le poivrier étant une liane, on le fait pousser sur de grands tuteurs, dans des plantations.

Dans la culture populaire
- Poivre Blanc est une marque de vêtements, créée en 1984 à Marseille.

Utilisation du mot « poivre »
- la salamandre géante du Japon produit, lorsqu'elle est menacée, une substance qui a la même odeur que le poivre, ce qui lui a valu son nom japonais de Ōsanshōuo, c'est à dire « poisson poivré géant ».
- la menthe poivrée est une variété de menthe, très riche en menthol, ce qui lui procure un goût très fort. Son nom commun et son nom latin (Mentha x piperita) sont inspirés de celui du poivre.

Expressions
- avoir les cheveux poivre et sel désigne une personne qui a déjà beaucoup de cheveux blancs, mais encore beaucoup de cheveux noirs (ou gris) : sa chevelure apparaît alors de deux couleurs.
- Ne jugez pas le grain de poivre d'après sa petite taille, goûtez-le et vous sentirez comme il pique. : c'est un proverbe arabe, qui signifie qu'il faut se méfier des apparences.
- Un grain de poivre l'emporte sur une corbeille de citrouilles. Citation extraite du Talmud.