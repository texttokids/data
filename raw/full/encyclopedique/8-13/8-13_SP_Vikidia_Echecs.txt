
Le jeu d'échecs est un jeu de stratégie qui se joue à deux. Il est composé d'un plateau nommé échiquier, qui porte 32 cases blanches et 32 cases noires, et de 32 pièces (16 blanches et 16 noires).
Sur toute la surface de la Terre, le jeu d'échecs est joué avec les mêmes règles de déplacement des pièces.
Il y a trois périodes dans une partie d'échecs :
- l'ouverture ou le début de partie. Elle se différencie d'après les joueurs (exemples d'ouvertures : la Française, la Sicilienne, l'Alekhine, la Pirk, l'Italienne...);
- le milieu de jeu, où les joueurs élaborent leur stratégie ;
- la finale, ou fin de jeu, où chaque joueur essaye de promouvoir ses pions (voir plus bas la définition).

Origine et évolution
Le jeu d'échecs est originaire d'Inde, certainement avant le Ve siècle av. J.-C. où il s'appelait chatiranga ; il se jouait à deux sur un échiquier divisé en cases unicolores. Il a été répandu en Asie par l'intermédiaire des bouddhistes qui ont quitté l'Inde pour échapper aux violences des hindouistes. Les bouddhistes avaient innové en créant le jeu à quatre1.
Le jeu d'échecs a été importé en Occident au Moyen Âge vers le IXe siècle. Dès cette époque, le déplacement des pièces était celui qu'elles ont aujourd'hui (sauf la reine qui ne pouvait faire qu'un mouvement d'une case en diagonale et le fou qui sautait à la troisième case en oblique, même par-dessus une autre pièce). Le déplacement actuel de ces deux pièces a été fixé dans le dernier quart du XVe siècle, probablement en France.
Au XVIe siècle, le saut du roi est transformé en rue et le pion qui arrive à la dernière rangée de cases de la moitié opposée peut se transformer en n'importe quelle pièce et non plus seulement en reine.
Le club d'échecs de Zurich est le plus ancien au monde, il a été fondé en 1809.
Le premier tournoi d'échecs moderne a été organisé en 1851 à Londres.
Depuis le XXe siècle, des tournois internationaux rassemblent les meilleurs joueurs. Le plus connu est celui de Noël à Hastings (sud-est de Londres). 

Règles du jeu

Généralités
Les pièces du jeu d'échecs sont appelées :
- roi (king) ;
- dame ou reine (queen) ;
- fou (bishop) ; 
- cavalier (knight) ;
- tour (rook) ;
- pion (pawn).
Chaque joueur commence avec une case blanche à sa droite.Soit les blancs sur les ligne 1 et 2 les noirs 8 et 7 .
Chaque joueur dispose au départ d'un roi, d'une dame, de deux fous, de deux cavaliers, de deux tours et de huit pions.
Les pièces sont disposées sur deux lignes de la manière suivante :
- 1e ligne (la plus proche du joueur) : Tour - Cavalier - Fou - Dame / Roi - Fou - Cavalier - Tour (le roi est placé sur la case de couleur opposée à la sienne, la dame sur la case de même couleur que la sienne).
- 2e ligne : les 8 pions.
Le joueur qui a les blancs joue en premier ; chaque joueur déplace une pièce par tour (mais pas de la même façon pour toutes les pièces).
Il faut que la case en bas à droite soit blanche pour le bon positionnement des pièces.
La partie se termine quand il y a « échec et mat » (le roi est menacé et ne peut plus bouger), « pat » (le joueur ne peut plus bouger aucune pièce), quand une même configuration se répète pour la troisième fois, à cause de la « règle des cinquante coups » (plus de cinquante coups ont été joués depuis la dernière prise ou le dernier mouvement de pion) ou quand un joueur abandonne.

Les joueurs en compétition
Chaque joueur a une fiche où il est écrit son nom, son prénom, son élo (point de force), la cadence de jeu et le nom de son club.
Lors des compétitions, les deux adversaires doivent respecter le temps qui leur est donné pour réfléchir (cadence de jeu). Ce temps est déterminé sur une pendule mécanique qui comporte deux horloges. Après chaque coup, le joueur qui vient de jouer appuie sur un bouton qui a pour effet de stopper son décompte et d'activer le décompte de l'adversaire.
Lorsqu'un joueur dépasse sa limite de temps, il perd à partir du moment où son adversaire le remarque. Cela signifie que tant que l'adversaire ne voit pas que l'on a dépassé le temps imparti, la partie continue !
Le temps alloué à la partie dépend d'une compétition à l'autre.
Il existe également des types de parties extrêmement rapides comme le Blitz où chaque joueur ne dispose que de 5 minutes, ou le Bullet où chaque joueur ne dispose que d'une minute.

Déplacement des pièces
Une pièce peut se déplacer soit vers une case libre, soit vers une case occupée par une pièce adverse. S'il y a une pièce adverse, la pièce déplacée la prend, et la pièce prise est retirée du jeu.
Chaque pièce a sa règle de déplacement :
- Le roi peut se déplacer dans toutes les directions (ligne droite ou diagonale), mais d'une seule case à la fois.
- La dame peut se déplacer dans toutes les directions et d'autant de cases que l'on veut.
- Le fou peut se déplacer d'autant de cases que l'on veut, mais uniquement en diagonale.
- La tour peut se déplacer d'autant de cases que l'on veut, mais uniquement en ligne droite.
- Le cavalier a un déplacement spécial : il se déplace de deux cases sur une ligne droite, puis d'une case à gauche ou à droite de la case où il arrive. Contrairement aux autres pièces, il peut sauter par-dessus une autre pièce qui est sur sa trajectoire.
- Le pion se déplace d'une case à la fois devant lui (il peut se déplacer de deux cases pour son premier déplacement), sauf pour prendre une pièce : dans ce cas, il se déplace d'une case en diagonale.
Cas particuliers :
- Le roque : le roi et la tour peuvent bouger en même temps, si le roi et la tour n'ont pas bougé depuis le début de la partie, s'il n'y a pas échec au roi et qu'aucune case entre le roi et la tour n'est ni bloquée, ni attaquée par une pièce adverse. Cela s'appelle le roque. Il s'agit d'un mouvement spécial où l'on met le roi à l'abri, protégé par une tour qui devient une pièce centrale (sur l'échiquier).
Il existe deux types de roques : le grand roque et le petit roque. Dans le grand roque, le roi bouge, côté dame, de deux pas en direction de la tour, laquelle se positionne de l'autre côté du roi. Dans le petit roque, le roi bouge de deux cases et la tour se place immédiatement de l'autre côté. Pour réaliser un roque, il faut qu'il n'y ait aucune pièce entre la tour et le roi, qu'aucune case entre le roi et la tour ne soit attaquée par une pièce adverse et que le roi ne soit pas en échec.
- La prise en passant : lorsqu'un pion se trouve sur la cinquième rangée et que l'autre joueur avance son pion de deux cases en ouverture, les deux pions se retrouvent côte à côte. Sauf que, en ouvrant de la sorte, le pion est passé (comme s'il avait "sauté") au-dessus d'une case attaquée par le pion de la cinquième rangée. Dans ce cas de figure, le pion de la cinquième rangée peut prendre l'autre pion, en diagonale, et se positionner sur la sixième rangée. 
- Les deux rois ne peuvent jamais être côte à côte. S'ils se font face, une ligne de cases doit les séparer. En effet, avancer son roi vers l'autre roi impliquerait de mettre son propre roi en échec.
- La promotion: un pion qui traverse l'échiquier et parvient à atteindre la dernière rangée peut être "promu". Ce qui signifie qu'une fois le parcours réalisé, il peut être transformé en n'importe quelle pièce (mis à part le roi). Il peut donc (rarement) arriver de posséder deux dames !

Fin de la partie
Si une pièce est en position pour prendre le roi adverse, on dit qu'il y a « échec » ou « échec au roi ». Le joueur adverse doit alors éviter l'échec, soit en déplaçant son roi, soit en empêchant la pièce menaçante de le prendre. 
Quand il n'y a plus moyen d'éviter l'échec, on dit qu'il y a « échec et mat », et c'est la fin de la partie. La partie peut également se terminer par l'abandon d'un des joueurs. Mais aussi quand la pendule indique qu'il n'y a plus de temps pour l'un des deux joueurs. 
On dit que « échec et mat » vient de l'arabe el-cheikh mat (الشيخ ماتَ), expression elle-même empruntée au persan, qui veut dire en français « le roi est mort ».

Petite histoire
Il paraît qu'il y a très longtemps, un grand prince de Perse était friand de la guerre et y perdait beaucoup d'argent. Alors un grand savant vint lui proposer un jeu qui lui permettrait d'avoir l'ardeur et la stratégie du combat dans un simple jeu.
Le prince, ravi de cette invention, proposa n'importe quoi au savant en échange de cette invention. Le savant lui demanda alors de mettre 1 grain de blé sur la première case, 2 sur la seconde, 4 sur la troisième, 8 sur la quatrième et de doubler ainsi le nombre de grains sur chaque case jusqu'à la dernière. C'était ce qu'il voulait de grains de blé. Le prince demanda des esclaves pour l'aider et il commença. Puis, au bout de 10 cases, il s'arrêta car le nombre de grains était trop grand !
En réalité, la quantité réelle de blé nécessaire à ce problème fait une couche de 10 cm sur toute la surface de la Terre !

Quelques joueurs célèbres
Jouer aux échecs fait appel au calcul et à deux mémoires, à court terme et à long terme. L'expertise vient de l'expérience. Le pédagogue Alfred Binet, spécialiste de l'hypnose et des tests psychométriques, a publié en 1894 Psychologie des grands calculateurs et joueurs d'échecs2.
- Magnus Carlsen (champion du monde depuis 2013) ; 
- Garry Kasparov ; 
- Bobby Fischer ; 
- Viswanathan Anand ; 
- Anatoli Karpov ; 
- Emmanuel Lasker ; 
- José Raul Capablanca ; 
- Mikhaïl Tal ; 
- Alexandre Alekhine ; 
- Levon Aronian ; 

Le jeu d'échecs dans l'art et la culture
- Le tableau de Lucas van Leyden La partie d'échecs, datant du début du XVIe siècle, est en fait une métaphore pour la rencontre amoureuse entre un fiancé et sa promise, conseillée par son père : elle est sur le point de gagner.
- Le thème des joueurs et des parties d'échecs a inspiré de nombreux cinéastes3, en particuler Le Prodige, sorti en 2015 et qui raconte la vie de Bobby Fischer. En 1957 Ingmar Bergman avait tourné Le Septième Sceau qui met en scène une partie d'échecs entre un croisé et la Mort.
En littérature4, ce divertissement a servi de support à toutes sortes d'interprétations allégoriques5.
- Stefan Zweig a rédigé quelques temps avant son suicide en 1942 une nouvelle, Le Joueur d'échecs qui se passe sur un paquebot qui traverse l'Atlantique en direction de l'Argentine.
- Le romancier russe Fiodor Dostoïevski a publié Le joueur en 1866 qui condamne l'addiction au jeu.