
Les plantes fournissent de l'oxygène, indispensable à la vie sur la Terre.
Au départ, il y a une t'explique graine, comme un pépin de pomme, de poire ou d'orange. Tu peux acheter dans une jardinerie des sachets de graines de fleurs, de légumes ou de plantes vertes. Place tes graines dans un pot de terre, laisse le pot près d'une source de lumière et arrose régulièrement. Pour grandir, tes graines ont besoin d'eau, de chaleur et de lumière, c'est tout ! Observe une plante se développer : tu ne le vois pas, mais elle bouge, elle vit, et sa tige comme
ses racines poussent chaque jour.
Les salades t'apportent des fibres, des vitamines et des minéraux !

Les plantes, c'est pas uniquement pour les lapins ! Toi aussi, tu manges des plantes ! Les racines, comme le radis, les feuilles des salades ou des aromates comme le persil, dont on peut même consommer les tiges, tous les fruits et légumes... Depuis quelques années, certains cuisiniers proposent dans leur restaurant des plats avec des plantes sauvages ou des fleurs ! Connais-tu le beignet de fleurs de courgette que l'on mange sur la Côte d'Azur ? Tu peux aussi goûter la fleur d'acacia, très sucrée. Miam ! À partir d'aujourd'hui, les lapins ne seront plus les seuls à manger des pissenlits !


