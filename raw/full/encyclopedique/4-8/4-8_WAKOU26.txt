Dans la serre tropicale, ce lori cardinal se régale de vers. Je fais attention à ce qu'il ne me pince pas avec son joli bec orange !
Une tortue géante des Seychelles.
Je termine la matinée en rendant visite aux plus grandes tortues du monde ! J'aime leur calme et je peux les caresser. À bientôt !
Le caméléon sait diriger longue langue gluante avec ision. Schlack, colles ! Les étés n'ont aucune chance...
Toi aussi, inscris-toi et deviens soigneur d'un jour.
Les koalas vivent dans les arbres et ne mangent que des feuilles fraîches d'eucalyptus. Le reste du temps ? Ils dorment !
Jeannette donne un bout de carotte à ce kangourou arboricole. C'est un bon grimpeur qui vit et se nourrit surtout dans les arbres.
