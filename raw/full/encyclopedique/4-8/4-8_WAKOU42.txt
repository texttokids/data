Pendant la journée, les vaches mangent des kilos d'herbe fraîche ! Grâce à leurs 4 estomacs, elles vont ensuite les ruminer, c'est-à-dire les digérer en 2 fois, pendant de longues heures.
Au printemps, après l'avoir porté 9 mois dans son ventre, cette vache a mis bas son premier veau. Elle le lèche aussitôt pour faire sa toilette.
Cette jolie vache limousine appartient à la famille des bovidés. Ses cornes sont creuses et les larges naseaux de son mufle rose lui permettent d'avoir un très bon odorat. Avec son troupeau, elle va rester au pré toute l'année. Quelle chance !
