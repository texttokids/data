Un son peut être plus ou moins fort. Pour le mesurer, on parle de décibels. Si tu chuchotes, le bruit est de 20 décibels, si tu discutes, c'est plutôt 60.
Le bruit d'un marteau-piqueur est d'environ 120 décibels. C'est très fort, et les ouvriers portent des casques pour éviter d'avoir des douleurs dans l'oreille.
Ees sortes de sons différents, E fonctionnent tes oreilles ?
Pour produire un son, il faut faire vibrer l'air : cela crée des ondes sonores.
Le pavillon de tes oreilles sert à capter ces ondes sonores, qui voyagent ensuite jusqu'à ton oreille interne.
Attention, même si tu n'as pas mal, écouter de la musique trop fort et trop longtemps peut abîmer tes oreilles sans que tu t'en aperçoives. Alors, baisse le son !
Les personnes qui n'entendent pas sont sourdes. D'autres, comme certaines personnes âgées, entendent moins bien et sont malentendantes.

