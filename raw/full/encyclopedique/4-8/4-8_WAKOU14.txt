Mon petit compagnon
TON CHIEN À LA PLAGE ?
Conseils :
Les vacances à la plage, c'est génial ! Si tu veux emmener ton chien avec toi,voici quelques conseils de Wakou !
Respecter les règles
À l'entrée de chaque plage, il y a un panneau qui dit si les chiens sont autorisés, à quel endroit, et à quels horaires. Tes parents et toi, vous êtes responsables des bêtises que votre chien pourrait commettre. Pas question de le laisser aboyer, s'ébrouer sur les gens, piétiner les serviettes ou faire ses besoins dans le sable ! La plage, c'est seulement pour les chiens calmes et bien élevés !
Prévoir avant le départ :
1 grande bouteille d'eau pour le faire boire et le rincer
1 collier et une laisse .. pour être sûr qu'il ne s'éloigne pas trop
Ton chien est vieux, il a des problèmes de santé ? Laisse-le plutôt au calme à la maison.
1 balle ou un jouet qui flotte pour s'amuser dans l'eau
Protéger son chien Avec son manteau de fourrure, ton chien a chaud : il faut le faire boire et le mouiller souvent. Pour le sortir, choisis de préférence les moments frais de la journée : tôt le matin ou le soir. Quand vous vous baignez, faites attention aux oursins et aux méduses, qui piquent aussi les chiens ! Mais surtout, empêche-le de boire de l'eau de mer, sinon il aura très mal au ventre.
N'oublie pas de le rincer après la baignade pour lui enlever le sel de mer.
Emmène-le marcher sur les sentiers de bord de mer ou le long de la plage, nage avec lui, entraîne-le à ramener un jouet flottant ou à en chercher un caché dans le sable... Tous les chiens savent nager, mais les grosses vagues ou les courants sont dangereux pour lui comme pour toi, alors soyez prudents.
Ton chien n'aime pas l'eau ? Ne le force pas, il y a plein d'autres activités sympas à faire avec lui !
