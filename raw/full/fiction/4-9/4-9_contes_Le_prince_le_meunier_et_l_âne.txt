Parmi les membres de l’honorable tribu arabe de Qurayë, l’on cite Mu‘âwia, le frère de ‘Abdel-Malek ben Marwân.
Un jour Mu‘âwia attendait chez un meunier, à l’une des portes de Damas, son frère le Calife ‘Abdel-Malek.
Il observa l’âne du meunier qui tournait la meule, un grelot attaché au cou ; il interrogea le meunier :
-« Pourquoi as-tu accroché au cou de l’âne un grelot ? »
-« Il m’arrive, répondit le meunier, de m’endormir ou d’avoir un petit somme ; si je n’entends plus le tintement du grelot, je sais que l’âne ne tourne plus la meule, alors je le gronde. »
-« Mais dis-moi, interrogea Mu‘âwia, comment sauras-tu si l’âne s’arrête et hoche la tête ainsi ? » et il joignit le geste à la parole.
-« Sire, lui dit le meunier, où trouverai-je un cerveau aussi brillant et intelligent pour mon âne, que le cerveau de Votre Altesse ? »
