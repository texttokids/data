Cet après-midi-là, un renard épuisé s’arrêta au portait d’une ferme.
- Auriez-vous un peu de nourriture à offrir à un voyageur affamé ? lança-t-il
- Non, dit la vache.
- Je n’ai rien à t’offrir, déclara l’âne
- Moi non plus, renchérit la chèvre.
- Pas une miette ! ajoutèrent les quatre poules.
- Passe ton chemin ! gronda la vieux chien.
- Bon, répondit le renard. Auriez-vous au moins un peu d’eau pour que je prépare une soupe ?
Le mouton, plus serviable que les autres, apporta un seau d’eau. Le renard allume un feu. Il sortit une marmite de son sac à dos, y versa l’eau et la déposa sur le feu. Peu après, l’eau se mit à frémir.
Avec précaution, le renard choisit une pierre sur le sol, la renifla et la jeta dans l’eau en disant :
- Voilà qui devrait faire une bonne soupe au caillou !
Les animaux de la ferme l’entourèrent avec curiosité. Le renard sortit alors une cuillère de son sac, la plongea dans l’eau et annonça tout sourire :
- Hum, délicieux… Mais ce n’est pas encore prêt ! Ce serait mieux avec un peu de sel et de poivre. En aurais-tu, mouton ?
Le mouton apporta un peu de sel et de poivre. Le renard assaisonna aussitôt la soupe, et les animaux, intrigués, s’approchèrent encore quand il y goûta. Mais la soupe ne semblait toujours pas prête.
- Il faudrait sans doute ajouter un navet pour lui donner un peu plus de goût, murmura le renard avec malice.
La vache trotta jusqu’à son étable et en rapporta un navet. Le renard l’éplucha et le mit dans la marmite. Puis, il prit une longue inspiration… et goûta. Mais la soupe ne semblait toujours pas prête.
- Je me demande s’il ne faudrait pas rajouter une carotte, dit le renard à haute voix.
Sans hésiter, l’âne galopa jusqu’au potager.
Il revient avec une grosse carotte que le renard s’empressa d’éplucher et de plonger dans la marmite. Mais la soupe ne semblait pas prête.
- Mais bien sûr, c’est un chou qu’il manque ! s’exclama le renard.
La chèvre partir à toute allure et en rapporta un. A présent, les animaux, l’eau à la bouche, se penchaient au-dessus de la marmite fumante. Mais la soupe ne semblait toujours pas prête.
- Quelques grains de maïs apporteraient la touche finale, j’en suis certain, dit le renard.
Les poules disparurent aussitôt avec des bols remplis de maïs que l’on renversa dans la marmite.
La soupe mijotait à gros bouillons. Les animaux se léchaient les babines.
Enfin la soupe au caillou fut prête. Tous les animaux la goûtèrent, se régalèrent… et n’en laissèrent pas une goutte. Ils affirmèrent qu’ils n’avaient jamais mangé de soupe aussi bonne. Le vieux chien s’exclama même :
- Et tout ça avec une simple pierre !
- C’est incroyable, renchérirent les autres.
- Bien, je dois partir, dit le renard en soulevant son sac à dos.
Tous les animaux lui souhaitèrent bonne chance. Ils l’invitèrent à s’arrêter à la ferme pour leur préparer une autre soupe au caillou, si jamais il repassait par là.
- Merci, répondit le renard, avec un grand sourire, je reviendrai sûrement.
Et il s’éloigna sur le chemin, un sourire en coin.

