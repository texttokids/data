﻿Par ce beau dimanche ensoleillé, Lily, son papa et sa maman vont déjeuner chez ses grands-parents.
Mais la mamie et le papi de la fillette n’habitent pas en ville, ils vivent dans une ferme, à la campagne. Et la petite fille adore aller à la ferme.
En arrivant chez ses grands-parents, Lily court à l’étable.
Mais les vaches sont dans le champ aujourd’hui.
Par contre, les petites veaux sont restés dans l’étable, ils sont encore trop petits pour sortir. Et leur maman est là pour veiller sur eux.
Lily leur donne du foin à manger, mais les petits veaux n’ont pas l’air d’apprécier. C’est le lait de leur mère qu’ils aiment savourer.
Après un petit tour à l’étable, Lily décide d’aller à la porcherie. Sa Mamie et son Papi possèdent trois cochons, deux roses et un noir. Et le petit cochon noir amuse beaucoup la fillette. Même si Ronron le petit cochon rose lui plaît beaucoup.
Alors que les deux cochons roses font la sieste au soleil, Lily s’approche du petit cochon noir. Elle lui tend une pomme que le cochon attrape et mange. La fillette aime nourrir le petit cochon, ce qui n’est pas pour lui déplaire. Il attend avec impatience une autre pomme.
Même si Lily adore la ferme, il y a un animal qu’elle n’apprécie pas : c’est le coq de ses grands-parents. Ce volatile chante tout le temps et adore embêter les autres poules du poulailler.
De temps en temps, Lily se met à courir vers le coq pour l’effrayer. Cela fait beaucoup rire la fillette.
Une fois son petit tour terminé, la fillette rejoint tout le monde dans la maison familiale.
“Tiens donc, je pensais qu’on ne te verrait pas de la journée” dit sa mamie en rigolant. “Pardon Mamie...” s’excuse Lily, “mais j’adore aller voir tous les animaux de la ferme, je m’amuse bien avec eux”. Lily raconte alors sa balade à tout le monde.
Mais il se fait tard, il est temps de rentrer à la maison. Lily est ravie de sa journée, elle a profité de ses grands-parents mais aussi de tous les animaux de la ferme.
