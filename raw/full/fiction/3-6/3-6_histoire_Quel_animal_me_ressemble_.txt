Léon se pose beaucoup de questions. "A quel animal je ressemble le plus ?"
Léon va voir Alban l'éléphant. "- Tu as 4 pattes, comme moi ! C'est toi mon cousin ? - Non, ce n'est pas moi, j'ai une trompe et toi un nez"
Léon va voir Félicien le chien. "- Tu as un nez, comme le mien ! C'est toi mon cousin ? - Non, ce n'est pas moi, j'ai de grandes oreilles et toi non."
Léon va voir Martine, l'hippopotame. "- Tu as des petites oreilles, comme moi ! C'est toi ma cousine ? - Non, ce n'est pas moi, je marche à quatre pattes et toi tu es debout."
Léon va voir Gaston l'ourson. "Tu as des poils comme moi ! C'est toi mon cousin ? - Non, ce n'est pas moi, j'ai de grandes griffes et toi de petits ongles. »
Léon va voir Maude la taupe. "Tu as 5 doigts à chaque main, comme moi ! C'est toi ma cousine ? - Non, ce n'est pas moi, je vis sous terre et toi tu vis à la lumière. »
Léon est perdu. Il ne trouve pas l’animal qui lui ressemble…
Léon entend un rire et reçoit une peau de banane sur la tête. Il suit la piste des peaux de banane.
"C'est moi ton cousin !" Léon découvre Romain le singe. "J'ai 4 pattes, 10 doigts, des poils, des dents, un nez, des petites oreilles, je peux me tenir debout, et mon visage ressemble au tien. »
« Les singes sont aussi rigolos et aiment faire des grimaces, comme moi ! »
