Ce matin, Papa est dans le garage. Il fait du bricolage.
Lily vient le voir. “Qu’est ce que tu fais, Papa ?” questionne la fillette. “J’essaie de réparer la tondeuse à gazon”, explique Papa, “tu veux me donner un coup de main ?” “Oh oui !” dit Lily toute enjouée.
Papa montre la tondeuse à la fillette. Lily dispose alors les vis dans les trous pour que Papa les visse correctement.
“Regarde Papa, j’ai les mains toutes noires comme toi !” rigole Lily.
Papa vient dessiner deux traits sur les joues de Lily avec ses doigts noirs. “Et voilà”, dit Papa, “maintenant tu es une indienne bricoleuse !”.
Lily rit et imite le cri des indiens. “Aller, ma petite indienne, maintenant on nettoie la carrosserie.”
Papa donne un chiffon à Lily pour nettoyer la tondeuse.
La petite fille plonge le chiffon dans le seau d’eau, et Paff ! Elle le lance sur Papa qui se retrouve tout mouillé. “Petite coquine, attention si je t’attrape” s’écrie Papa. Lily et Papa s’amusent bien.
Mais il est temps de remonter la tondeuse à gazon. “Tire sur la corde Lily” dit Papa. La fillette tire et fait démarrer la tondeuse. “On a réussi !” s’écrie-t-elle. Papa et Lily sont très fiers de leur travail d’équipe.
Il est temps de rentrer se laver, et de raconter à Maman tout le travail réalisé !
