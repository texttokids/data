Nitro est le premier robot intelligent, qui a pour mission de protéger la planète Terre et ses habitants. Comme tous les jours, il survole le monde pour voir si tout va bien.
En survolant le Sahara, il aperçoit un petit extraterrestre à coté de sa soucoupe volante.
Nitro va immédiatement voir le petit extraterrestre pour savoir ce qu'il se passe. " Salut petit extraterrestre ! je m'appelle Nitro ! Qu'est-ce qui t'est arrivé ?" demande Nitro.
" Salut Nitro ! Moi c'est Kal, je viens d'une galaxie voisine, une météorite s'est coincée dans ma soucoupe volante... Du coup, j'ai perdu le contrôle et j'ai atterri sur votre planète." répond Kal.
" Ah, c'est dangereux les météorites, il faut faire attention. Est-ce-que tu t'es fait mal ?" s'inquiète Nitro. "Non, je vais bien, mais j'ai besoin d'aide. Je n'arrive pas à sortir la météorite qui s'est coincée dans le moteur."
Kal tire de toutes ses forces sur la météorite. "Si je ne la sors pas, je ne pourrais pas rentrer chez moi. Tu veux bien m'aider ?" demande Kal.
" Bien sûr mon ami, allons la sortir ensemble !" répond Nitro en se plaçant à côté de Kal. Ils se mettent à tirer tous les deux sur la météorite de toutes leurs forces.
En unissant leurs forces, la météorite ne résiste pas longtemps et elle finit par tomber tout de suite par terre.
" On a réussi !" crie Kal en sautant de joie. Il s'assoit immédiatement dans sa soucoupe volante. " Grâce à toi, je pourrais rentrer chez moi, merci Nitro." dit-il en démarrant sa soucoupe.
" Je t'en prie mon ami ! Dépêches-toi de rentrer, ta famille doit s'inquiéter." répond Nitro.
" J'espère que l'on se reverra un jour, au revoir Nitro !" crie Kal en s'envolant dans le ciel.
Nitro est heureux, il a une fois de plus accompli sa mission. Il repart immédiatement parcourir la Terre pour de nouvelles aventures.
