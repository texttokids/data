Jules est passionné par le football. Il regarde tous les matchs de Messi, son joueur préféré, à la télévision. Et ce samedi a lieu un match très important pour lui et son équipe. Il vont rencontrer l'école voisine qui est leur principale rivale.
Mais aujourd'hui, à l'entraînement, Jules est triste. Ses baskets sont abîmées et usées. Il y a des trous et elles lui font mal aux pieds. Alors Jules reste assis sur le banc et ne veut plus jouer au football.
"Que t'arrive-t-il ?", lui demande son entraîneur. "Mes baskets sont usées et j'ai peur de rater le match de samedi.", répond Jules, tête baissée.
Le jour du grand match est arrivé, mais au réveil, Jules n'a toujours pas retrouvé le sourire. Il se lève de son lit et trouve une boîte posée à ses pieds. Qu'est-ce que cela peut bien être ?
Jules ouvre la boîte et découvre une magnifique paire de baskets rouges ! "Quelles sont belles !", s'écrie Jules. Il les enfile immédiatement et court voir sa maman pour la remercier de ce joli cadeau.
C'est maintenant l'heure du grand match. Jules s'échauffe et se prépare avec son équipe pour la rencontre. Ils sont très concentrés. Toutes leurs familles sont là pour les encourager.
Le match commence. Jules s'élance après le ballon et court, court très vite pour le rattraper.
Mais à ce moment-là, un joueur de l'équipe adverse se jette vers lui pour lui faire un tacle. Jules trébuche et tombe ! Aïe aïe aïe, il a mal au genou !
Jules est blessé, au sol. Mais il décide de ne pas se décourager. Il regarde ses belles baskets rouges qu'il aime tant et se dit qu'il ne doit rien lâcher pour sa maman. Il se relève, frotte ses genoux et repart !
Il court encore plus vite après le ballon. Tellement vite que les autres joueurs n'arrivent plus à le rattraper. Il tire et marque un but ! Tout le monde l'applaudit, surtout sa maman, dans les gradins, qui est fière de son petit garçon courageux. Jules est heureux, son équipe a gagné le match grâce à ses baskets magiques et sa maman.
