Un matin, au coeur de la savane, Arthur le lion, Roi des animaux, est réveillé par le cri des singes. "Sire, Sire, tu dois venir" lui disent-il. "Vite, vite !"
"Que se passe-t-il pour que vous veniez troubler mon royal sommeil ?" demande le Roi. "La tempête a bloqué la rivière et les animaux n’ont plus d’eau !" expliquent les singes.
"La rivière devrait être pleine, je ne comprends pas !" dit le roi Arthur. "Je vais rendre visite aux hippopotames sur-le-champ !"
En arrivant devant la rivière des hippos, plus une seule goutte d’eau. Arthur demande à l’aigle d’aller voir ce qu’il se passe en amont de la rivière.
Quelques minutes plus tard, l’aigle revient. "La rivière est bloquée. Un éboulement de pierres et de troncs d’arbres a causé un immense barrage."
"Il faut agir !" dit Arthur. "Les hippos, les éléphants, les singes, venez nous aider ! Nous devons débloquer l’eau de la rivière."
Tous les animaux entament une longue marche pour aller jusqu’au barrage naturel. Les éléphants parlent aux hippopotames, les singes avec les lions. Toute la savane est réunie.
Arthur, en bon chef, propose aux animaux un plan. "Les singes, vous allez récupérer des morceaux de lianes pour en faire des cordes. Les éléphants, vous allez tirer les morceaux de bois et les pierres. Les hippos, vous pousserez les pierres et aiderez les éléphants."
"Un, deux, trois, allez-y !" Tous les animaux sont solidaires et font d’énormes efforts. Peu à peu, l’eau commence à couler à nouveau jusqu’à ce que les dernières pierres cèdent.
"Bravo, Bravo !" Les cris de joie des animaux résonnent partout dans la savane.
La rivière coule à nouveau. Tous les animaux se désaltèrent tranquillement. Et Arthur, le Roi de la savane s'endort paisiblement à l’ombre d’un Baobab.
