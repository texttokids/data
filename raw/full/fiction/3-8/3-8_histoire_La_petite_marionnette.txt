Il y a quelques temps, Alfy était le meilleur compagnon qui soit. Aujourd’hui Alfy a vieilli, il lui manque une jambe et il n’a plus de vêtements.
Alfy est tout seul, il fait froid et très très noir.
Quand soudain, un homme vient et emporte Alfy.
Alfy, blottit dans un sac, se demande où il va. L’homme lui dit « Je vais prendre soin de toi ».
Alfy est au chaud, il va retrouver une vie,
De nouveaux habits, et un nouvel ami.
« Oh mais qu’est-ce donc cette étrange cape que je porte ? » se dit-il. Alfy s’est transformé en marionnette !
Et Alfy parle maintenant, il joue avec d’autres amis comme lui tous vêtus d’une cape.
Mais ce qui fait le plus plaisir à Alfy : « Faire rire les enfants qui assistent à ses spectacles !" Il joue avec eux comme avant.
Des années ont passé, Alfy est toujours là et les enfants aussi.
