Le grand jour est enfin arrivé ! Pierre va enfin réaliser son rêve: participer au Tour de France ! Pierre est prêt: il a son maillot, ses gants, son casque, ses lunettes de soleil, une bouteille d’eau sans oublier son vélo de course.
Une immense foule acclame les coureurs. Plusieurs équipes de télévision sont venues filmer l’événement. Pierre se sent confiant et remercie la foule.
La course commence ! Pierre pédale le plus vite possible mais le trajet reste long à faire. Il passe par plusieurs communes. À chaque étape, une foule de supporters les encourage.
Le soleil tape fort, Pierre n’oublie pas de boire de l’eau pour s’hydrater.
Avant la fin du tour, Pierre doit monter une impressionnante pente. Après un long effort, il réussit à arriver en haut de la pente !
Soudain Pierre voit la ligne d’arrivée ! Prenant son courage à deux mains, il réussit à dépasser les autres coureurs et arrive premier ! Il a du mal y croire, il est arrivé vainqueur du Tour de France !
Les journalistes se précipitent pour l’interviewer tandis que les supporters lui demandent des autographes. Pierre se place sur le podium et enfile le maillot jaune du meilleur coureur. Il est si heureux ! Toute sa famille et ses amis sont fiers de lui.
