Claire est partie en vacances chez ses grands-parents.
C’est le printemps, les arbres fleurissent à nouveau, les oiseaux chantent. Mais de temps en temps, la pluie montre le bout de son nez.
Rester à la maison n’est pas amusant, on serait si bien dehors à courir sous la pluie. Alors Claire décide de sortir dans le jardin de ses grands-parents.
Claire enfile ses bottes jaunes et part jouer sous la pluie.
Quand soudain, elle rencontre un escargot. "Petit escargot" lui dit-elle, "tu vas te mouiller." L’escargot avance doucement. Claire lui fait une caresse.
L’escargot se replie sur lui-même pour rentrer dans sa coquille. Claire se dit que son nouvel ami doit avoir peur car elle est très grande par rapport à lui.
Alors Claire a une idée. Elle prend le plus grand pot de fleurs vide de sa grand-mère. "Cela fera un abri pour toi, petit escargot" se dit-elle.
La grand-mère de Claire lui explique que les escargots aiment la pluie. Ils portent une maison sur leur dos et n’ont pas forcément besoin d’un abri pour les protéger. Il pleut de plus en plus et Claire doit rentrer pour ne pas attraper froid.  « Au revoir petit escargot, à demain ! ».
Le lendemain, Claire court dans le jardin pour voir si l’escargot est toujours là. A quelques mètres du pot, Claire retrouve le petit escargot. "Chouette, tu es encore là, nous allons passer toutes les vacances ensemble" dit-elle.
Et tous les jours, l’escargot alla dormir dans son abri pour le plus grand plaisir de Claire.
