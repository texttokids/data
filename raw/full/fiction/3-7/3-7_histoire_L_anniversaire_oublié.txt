Aujourd’hui, c’est l’anniversaire de la petite Awa! Elle va avoir 10 ans, c’est un âge important pour une petite fille.
Mais ce matin-là, ses parents sont partis au travail et il n’y a personne pour lui souhaiter un joyeux anniversaire. Même ses frères et soeurs ont l’air d’être partis plus tôt à l’école, et ça c’est vraiment bizarre!
Soucieuse, Awa part à l’école en ce demandant s’ils n’auraient pas tous oublié son anniversaire.
Dans la cour de l’école, ses copines ont des tas de choses à se raconter comme d’habitude. Mais aucune d’entre elles n’a pensé à son anniversaire!
Mais pas d’inquiétude, se dit-elle! Car la maîtresse pense toujours à tout : elle a un calendrier avec tous les anniversaires de ses élèves. Il est juste à côté de son bureau, elle ne risque pas d’oublier elle au moins !
Mais lorsque Awa regarde cette fois, son nom n’est pas écrit sur la case du jour. La petite fille est si triste qu’elle n’ose rien dire et passe la journée dans son coin.
«  Pourtant je suis gentille et généreuse avec tout le monde » Se dit la petite fille sur le chemin du retour. Awa est tellement dans ses pensées qu’elle n’a pas remarqué toutes les voitures garées devant sa maison.
Lorsqu’elle entre dans la maison, la lumière est éteinte et elle entend de petits chuchotements. Quand tout à coup …
« Joyeux anniversaires Awa ! » La lumière s’allume et tout le monde est là. Il y a ses parents, ses deux frères et toutes ses copines de l’école.
Awa se rend compte qu’il y a même une carte de sa maîtresse! Elle aussi était dans le coup. Quelle belle surprise !
