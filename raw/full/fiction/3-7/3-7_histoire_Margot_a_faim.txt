Il est midi, l’heure du déjeuner. Et Margot a très faim.
“Maman, on mange quoi à midi ?” demande la petite fille. “Je prépare des cordons bleus avec des petits pois Margot” répond sa maman.
“Oh non !”, dit Margot en grimaçant, “j’aime pas les petits pois”. “Tu en mangeras un peu, c’est important de manger des légumes. En attendant que le repas soit prêt, va mettre la table s’il te plaît”.
Margot fait la tête, elle n’a pas envie de manger des légumes à midi.
La petite fille met la table, tandis que sa maman appelle tout le monde pour le déjeuner.
Elle sert l’enfant qui fait toujours la tête. “Mange au moins une cuillère Margot” dit le papa. “Mais je n’aime pas ça ! Pourquoi faut-il manger des légumes ?”
“Tu sais Margot, quand j’étais petit, moi non plus je n’aimais pas les légumes”, raconte son Papa. "Et puis, dans une cuillère de petits pois, tu as pleins de bonnes choses qui t’aident à avoir de l’énergie."
"Ton corps a besoin de légumes pour grandir et bien se porter " dit la maman. "Tu ne veux pas être grande et forte ?” Margot écoute, baisse la tête et répond tout doucement : “Si…”.
Papa prend l’assiette de Margot et place les petits pois de manière à dessiner un bonhomme qui sourit.
Margot rigole en voyant son assiette et dit “Je vais lui manger le nez !” Et Margot prend une cuillère de petits pois. “Maintenant je vais lui manger les yeux !" Petit à petit, les petits pois disparaissent de l’assiette.
Margot a fini tous ces légumes verts, sans grimacer, et en s’étant bien amusée !
