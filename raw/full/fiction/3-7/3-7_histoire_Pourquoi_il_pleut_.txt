Pourquoi il pleut ?
Parce que le soleil pleure sur nous ?
Parce qu’un géant nous fait pipi dessus ?
Ou bien c’est la lune qui arrose les arbres de la terre avec un arrosoir ?
La pluie, ce sont des milliers de petites gouttes d’eau qui font la taille d’un grain de riz qui tombent.
Elles tombent des nuages gris que tu vois dans le ciel.
L’eau de la mer monte dans les nuages blancs, et ils l’absorbent comme une éponge.
Quand ils sont trop lourds, les gouttent se jettent dans le vide et atterrissent sur nos têtes.
Puis les gouttes retournent dans le ciel, retombent et ainsi de suite.
Ça a l’air bien d’être une goutte d’eau !
