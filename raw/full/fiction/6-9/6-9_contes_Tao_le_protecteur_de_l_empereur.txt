Il était une fois le protecteur de l’empereur: Tao, expert en arts martiaux.
Un soir, alors que l’empereur se trouve dans la Cité Interdite, un mystérieux voleur dérobe le sabre en argent de l’empereur.
Tao ordonne aux gardes de fermer toutes les portes du palais et part à la recherche du mystérieux voleur.
Soudain, Tao aperçoit une ombre derrière un paravent: c’est le voleur ! Tao s’élance à sa poursuite jusque dans la cour intérieure. Les archers de l’Empereur se positionnent sur les toits de la cour. Le voleur est piégé et n’a aucun moyen de s’échapper.
Le voleur sort alors un poignard de sa poche et menace Tao. Un violent combat s’ensuit. Tao enchaîne les mouvements d’arts martiaux avec une telle rapidité qu’il parvient à désarmer son adversaire.
Tao dévoile le visage caché du voleur et découvre qu’il s’agit de Fu, l’ennemi juré de l’empereur !
L’empereur félicite Tao pour son courage et fait emprisonner le voleur. - Pour ton courage et ta maîtrise du combat, je t’offre mon sabre en argent. Que ce cadeau puisse te protéger Tao. - Merci Votre Majesté Impériale. C’est un honneur de recevoir un tel présent. S’exclame Tao.
