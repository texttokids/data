	
Bébé	va	au	marché	avec	Maman.	
	
Le	marché	est	bondé.		
Bébé	est	très	curieux.	
Bébé	est	si	curieux	que	madame	Ade,	la	marchande	de	bananes,	donne	à	Bébé	six	
bananes.	
	
Bébé	est	très	étonné.		
Bébé	mange	une	banane…	
	
…	et	met	cinq	bananes	dans	le	panier.	
	
Maman	ne	remarque	rien.	
Elle	est	occupée	à	acheter	du	riz.	
	
Le	marché	est	bondé.		
Bébé	a	très	chaud.	
Bébé	 a	 si	 chaud	 que	 monsieur	 Femi,	 le	 marchand	 d’oranges,	 donne	 à	 Bébé	 cinq	
oranges	juteuses.	
	
Bébé	sourit.	
Bébé	suce	une	orange…	
	
…et	met	quatre	oranges	dans	le	panier.	
	
Maman	ne	remarque	rien.		
Elle	est	occupée	à	acheter	de	l’huile	de	palme	faite	maison.	
	
Le	marché	est	bondé.		
Bébé	est	très	joyeux.		
Bébé	 est	 si	 joyeux	 que	 monsieur	 Momo,	 le	 marchand	 de	 biscuits,	 donne	 à	 Bébé	
quatre	chin-chin	sucrés.		
	
Bébé	applaudit.		
Bébé	mange	un	chin-chin…		
	
…et	met	trois	chin-chin	dans	le	panier.	
	

Maman	ne	remarque	rien.		
Elle	est	occupée	à	acheter	des	piments.	
	
Le	marché	est	bondé.		
Bébé	est	très	drôle.	
	
Bébé	est	si	drôle	que	madame	Kunle,	la	marchande	de	maïs	sucré,	donne	à	Bébé	
trois	épis	de	maïs	grillés.	
	
Bébé	rayonne.	
Bébé	mange	un	épi	de	maïs…	
	
…et	met	deux	épis	de	maïs	dans	le	panier.		
Maman	ne	remarque	rien.		
Elle	est	occupée	à	acheter	des	chaussures.	
	
Le	marché	est	bondé.	
Bébé	est	très	coquin.		
Bébé	est	si	coquin	qu’il	fait	tomber	TOUS	les	vêtements.	
	
Mais	Bébé	est	si	embêté	que	madame	Dele	donne	à	Bébé	deux	morceaux	de	noix	
de	coco.		
	
Bébé	salive.	
Bébé	mange	un	morceau	de	noix	de	coco…	
	
…et	met	l’autre	morceau	dans	le	panier.	
	
Maman	ne	remarque	rien.	Son	panier	est	très	lourd.	
Très,	très	lourd.		
Et	Maman	pense	soudain	que	son	joli	Bébé	doit	avoir	bien	faim	maintenant.	
«	Taxi	!	crie	Maman.	Nous	devons	vite	rentrer	à	la	maison	!	»	
	
Maman	dépose	son	panier.		
«	Mais	qu’est-ce	que	c’est	que	ça	?	s’écrie	Maman.	Cinq	bananes	!	Quatre	oranges	!	
Trois	biscuits	chin-chin	!	Deux	épis	de	maïs	grillés	!	Un	morceau	de	noix	de	coco	!		
Je	n’ai	jamais	acheté	tout	ça	!	»	
	
«	En	effet	!	»	
rit	madame	Ade,	la	marchande	de	bananes,	
et	monsieur	Femi,	le	marchand	d’oranges,	
et	monsieur	Momo,	le	marchand	de	chin-chin,	
et	madame	Kunle,	la	marchande	de	maïs,	
et	madame	Dele,	la	marchande	de	noix	de	coco.	
«	C’est	nous	qui	les	avons	offerts	à	Bébé	!	»	
	
Maman	regarde	son	Bébé.	
Bébé	rit.	
Maman	rit	aussi.	

«	Quel	gentil	Bébé	!	dit-elle.	Tu	as	tout	rangé	directement	dans	le	panier	!	»	
	
Maman	grimpe	sur	le	moto-taxi.		
Bébé	s’endort.	
«	Pauvre	Bébé	!	dit	Maman.	Et	en	plus	il	n’a	rien	eu	à	manger	!	»	


