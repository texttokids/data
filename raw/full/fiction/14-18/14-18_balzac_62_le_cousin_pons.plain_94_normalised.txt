
Quelques moments après, le docteur Poulain suivait au chevet du lit les progrès de l'agonie de Pons, que Schmucke suppliait vainement de se laisser opérer. Le vieux musicien ne répondait aux prières du pauvre Allemand désespéré que par des signes de tête négatifs, entremêlés de mouvements d'impatience. Enfin, le moribond rassembla ses forces, lança sur Schmucke un regard affreux et lui dit : - Laisse-moi donc mourir tranquillement !...

Schmucke faillit mourir de douleur ; mais il prit la main de Pons, la baisa doucement, et la tint dans ses deux mains, en essayant de lui communiquer encore une fois ainsi sa propre vie. Ce fut alors que le docteur Poulain entendit sonner et alla ouvrir la porte à l'abbé Duplanty.
- Notre pauvre malade, dit Poulain, commence à se débattre sous l'étreinte de la mort. Il aura expiré dans quelques heures ; vous enverrez sans doute un prêtre pour le veiller cette nuit.  Mais il est temps de donner madame Cantinet et une femme de peine à monsieur Schmucke, il est incapable de penser à quoi que ce soit, je crains pour sa raison, et il se trouve ici des valeurs qui doivent être gardées par des personnes pleines de probité.

L'abbé Duplanty, bon et digne prêtre, sans méfiance ni malice, fut frappé de la vérité des observations du docteur Poulain ; il croyait d'ailleurs aux qualités du médecin du quartier ; il fit donc signe à Schmucke de venir lui parler, en se tenant au seuil de la chambre mortuaire.  Schmucke ne put se décider à quitter la main de Pons qui se crispait et s'attachait à la sienne comme s'il tombait dans un précipice et qu'il voulût s'accrocher à quelque chose pour n'y pas rouler. Mais, comme on sait, les mourants sont en proie à une hallucination qui les pousse à s'emparer de tout, comme des gens empressés d'emporter dans un incendie leurs objets les plus précieux, et Pons lâcha Schmucke pour saisir ses couvertures et les rassembler autour de son corps par un horrible et significatif mouvement d'avarice et de hâte.
- Qu'allez-vous devenir, seul avec votre ami mort ? dit le bon prêtre à l'Allemand qui vint alors l'écouter, vous êtes sans madame Cibot...
- C'esde eine monsdre qui a dué Bons ! dit-il.
- Mais il vous faut quelqu'un auprès de vous ? reprit le docteur Poulain, car il faudra garder le corps cette nuit.
- Che le carterai, che brierai Tieu ! répondit l'innocent Allemand.
- Mais il faut manger !... Qui maintenant, vous fera votre cuisine ? dit le docteur.
- La touleur m'ôde l'abbédit ! répondit naïvement Schmucke.
- Mais, dit Poulain, il faut aller déclarer le décès avec des témoins, il faut dépouiller le corps, l'ensevelir en le cousant dans un linceul, il faut aller commander le convoi aux pompes funèbres, il faut nourrir la garde qui doit garder le corps et le prêtre qui veillera, ferez-vous cela tout seul ?...  On ne meurt pas comme des chiens dans la capitale du monde civilisé !

Schmucke ouvrit des yeux effrayés, et fut saisi d'un court accès de folie.
- Mais Bons ne murera bas... che le sauferai !
- Vous ne resterez pas long-temps sans prendre un peu de sommeil, et alors qui vous remplacera ? car il faut s'occuper de monsieur Pons, lui donner à boire, faire des remèdes...
- Ah ! c'esde frai ! ... dit l'Allemand.
- Eh bien ! reprit l'abbé Duplanty, je pense à vous donner madame Cantinet, une brave et honnête femme...

Le détail de ses devoirs sociaux envers son ami mort, hébéta tellement Schmucke, qu'il aurait voulu mourir avec Pons.
- C'est un enfant ! dit le docteur Poulain à l'abbé Duplanty.
- Eine anvant ! ... répéta machinalement Schmucke.
- Allons ! dit le vicaire, je vais parler à madame Cantinet et vous l'envoyer.
- Ne vous donnez pas cette peine, dit le docteur, elle est ma voisine, et je retourne chez moi.

La Mort est comme un assassin invisible contre lequel lutte le mourant ; dans l'agonie il reçoit les derniers coups, il essaie de les rendre et se débat. Pons en était à cette scène suprême, il fit entendre des gémissements, entremêlés de cris. Aussitôt, Schmucke, l'abbé Duplanty, Poulain accoururent au lit du moribond. Tout à coup, Pons, atteint dans sa vitalité par cette dernière blessure, qui tranche les liens du corps et de l'âme, recouvra pour quelques instants la parfaite quiétude qui suit l'agonie, il revint à lui, la sérénité de la mort sur le visage et regarda ceux qui l'entouraient d'un air presque riant.
- Ah ! docteur, j'ai bien souffert, mais vous aviez raison, je vais mieux.,. Merci, mon bon abbé, je me demandais où était Schmucke !...
- Schmucke n'a pas mangé depuis hier au soir, et il est quatre heures : vous n'avez plus personne auprès de vous, et il serait dangereux de rappeler madame Cibot...
- Elle est capable de tout ! dit Pons en manifestant toute son horreur au nom de la Cibot. C'est vrai, Schmucke a besoin de quelqu'un de bien honnête.
- L'abbé Duplanty et moi, dit alors Poulain, nous avons pensé à vous deux...
- Ah ! merci, dit Pons, je n'y songeais pas.
- Et il vous propose madame Cantinet...
- Ah ! la loueuse de chaises !  s'écria Pons. Oui, c'est une excellente créature.
- Elle n'aime pas madame Cibot, reprit le docteur, et elle aura bien soin de monsieur Schmucke...
- Envoyez-la-moi, mon bon monsieur Duplanty... elle et son mari, je serai tranquille. On ne volera rien ici...
