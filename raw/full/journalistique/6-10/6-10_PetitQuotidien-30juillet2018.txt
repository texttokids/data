Le Petit Quotidien – N°5659-5660 – Lundi 30 juillet 2018

Reportage : visite d’une clinique pour chevaux, près de Paris
La clinique vétérinaire de Grosbois, dans le Val-de-Marne, accueille des chevaux malades ou blessés. Le Petit Quotidien t’y fait entrer.



Des chevaux examinés et soignés

La clinique de Grosbois se trouve à Boissy-Saint-Léger, au sud-est de Paris. Des chevaux de course et de loisir y sont soignés presque comme les humains. Une consultation rapide coûte 30 euros. Une opération chirurgicale s’élève parfois à plus de 15000 euros. La clique peut accueillir jusqu’à 25 chevaux en même temps.

Première observation
2 vétérinaires, Mathilde (à gauche) et Aude, observent comment une jument marche et trotte, avant de l’examiner de plus près à l’intérieur de la clinique.

Une échographie
Mathilde a appliqué du gel sur les parties du corpus de la jument qu’elle veut examiner. À l’aide d’une sonde connectée à un ordinateur, elle voit sur un écran l’intérieur du corps de l’animal (photo). C’est une échographie. Chez les humains, on observe ainsi un bébé dans le ventre d’une femme enceinte.
Pendant cet examen de la jument, il n’y a aucun bruit. « En général, les chevaux n’aiment pas quand c’est silencieux comme ça. Cela les inquiète », remarque la maîtresse de la jument, restée à côté de l’animal.

Une radio
Ce cheval passe une radio du pie. À la clinique, les chevaux sont soignés pour des boiteries, des douleurs au ventre, des difficultés respiratoires, mais aussi pour des problèmes aux yeux, aux dents…

Un test d’effort
« Ce trotteur n’arrive pas à terminer ses courses. On essaie de savoir pourquoi », explique Valérie. Sur la photo, on voit le cheval faire un test d’effort. « On le fait courir à différentes vitesses sur un tapis roulant, pour étudier son rythme cardiaque. On lui fait aussi des prises de sang pour analyser son oxygénation. Enfin, on lui installe un endoscope, pour regarder s’il a un problème dans la gorge », ajoute la vétérinaire. Pendant le test, le bruit des sabots sur le tapis résonne de plus en plus for au fur et à mesure que la vitesse augmente.

Une opération
Ce cheval a été endormi avant d’être opéré. Il y a 2 salles de chirurgie : l’une pour les problèmes aux os et aux articulations, l’autre pour tout ce qui concerne la digestion et la peau.

Calmer la douleur
Aude pratique la mésothérapie sur une jument après une matinée d’examens. « C’est une technique pour soulager la douleur de l’animal : on lui injecte des médicaments sous la peau », explique-t-elle. La juments est restée tranquille. Elle va maintenant rentrer chez elle avec sa maîtresse.
