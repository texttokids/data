N°91 - 1 au 7 février 2019
le mal-logement
Avoir un espace à soi pour faire ses devoirs ou pour jouer est très important. C’est la même chose pour les plus grands. Pourtant, près de 4 millions de personnes sont aujourd’hui mal logées en France. Ça veut dire qu’elles n’ont pas de logement du tout ou vivent dans une habitation en très mauvais état ou beaucoup trop petite par exemple. Je t’explique tout dans ce nouveau numéro.


L’immeuble d’Inès et d'Adam menaçait de s’effondrer

Dans l’appartement d’Inès, 4 ans, et Adam, 2 ans, il y a des dizaines de photos de famille sur les murs. Des coussins moelleux sont disposés sur le canapé, et les enfants regardent des dessins animés à la télé. Rien d’anormal ! Enfin presque… Cet appartement situé à Coulommiers, une ville à l’est de Paris, n’est pas vraiment le leur. Ils y ont emménagé en catastrophe avec leurs parents et leurs deux chats, la veille de Noël. L’immeuble où ils vivaient juste avant menaçait de s’effondrer… La famille était dans une situation de mal-logement.
«Il y a deux ans et demi, des fissures ont commencé à apparaître sur les murs de notre immeuble. Petit à petit, elles ont grossi. A la fin, on pouvait passer nos bras à l’intérieur !» raconte Jennifer, la maman d’Inès et Adam. A cause de ces trous, il faisait toujours très froid et humide chez eux. Adam tombait souvent malade : «On l’emmenait trois fois par mois chez le pédiatre», affirme Djiad, son papa.
Inès aimait beaucoup sa chambre «violette et blanche avec des rideaux roses». Mais elle se souvient des murs qui n’arrêtaient pas de faire «crac». Ça lui faisait très peur. «La maison allait tomber sur nous…» murmure-t-elle en se blottissant sur le canapé. Pour protéger leurs enfants, Jennifer et Djiad ont fini par les faire dormir chez leur grand-mère.
Les habitants de l’immeuble ont demandé des dizaines, voire peut-être des centaines de fois aux propriétaires de faire des travaux, sans succès. Cet automne, Jennifer a appelé l’association Droit au logement, qui aide les personnes dans ce genre de situation. Une bénévole a alors tout mis en œuvre pour les sortir de là.
La maire de Coulommiers leur a trouvé un appartement de secours, dans lequel ils peuvent vivre gratuitement en attendant de trouver une solution. Même si leur combat pour trouver un nouveau logement est loin d’être terminé, toute la famille est déjà un peu soulagée. Inès la première : «Maintenant la nuit, on a bien chaud», se réjouit-elle.
Coulommiers, la ville où habite la famille


Qu’est-ce que le mal-logement ?

Ces derniers mois, on a beaucoup parlé d’un problème de logements en très mauvais état à Marseille, dans le sud-est de la France. En novembre, il s’est en effet passé un grave accident : deux immeubles se sont effondrés en plein centre-ville. Huit personnes sont mortes. Depuis, 230 autres immeubles ont été évacués à Marseille parce qu’ils sont aussi considérés comme dangereux et qu’il ne faut pas que ce drame se reproduise.
Le problème, c’est que de plus en plus de personnes sont très mal logées ou pas logées du tout partout en France. Selon la Fondation Abbé-Pierre, qui lutte contre ce problème, près de 4 millions de Français sont aujourd’hui mal logés. Ce chiffre a beaucoup augmenté en 10 ans. Le mal-logement, ça concerne différentes situations :
Environ 2 millions de personnes vivent dans des maisons ou des appartements en très mauvais état. Elles n’y sont pas en sécurité. 2 millions de personnes, c’est beaucoup : c’est presque autant que la population de Paris.
Dans leur logement, il fait par exemple très froid ou très humide à cause d’une mauvaise isolation. Il peut aussi manquer un accès à l’eau et aux toilettes ou y avoir un système électrique très vieux et dangereux.
Certains vivent aussi dans des bâtiments tellement abîmés qu’ils menacent de s’effondrer, comme ça s’est passé à Marseille.
Pour éviter de dormir dans la rue, beaucoup demandent à leurs proches, souvent leurs parents, de dormir chez eux. Plus de 640 000 personnes dorment ainsi chez d’autres gens.
143 000 personnes sont sans domicile, ça veut dire qu’elles n’ont de logement du tout. Certaines vivent dans la rue. En plus de ça, il existe des personnes qui vivent dans des habitations de fortune.
Des familles qui habitent dans des appartements en bon état peuvent aussi être victimes du mal-logement. Il arrive que les enfants et les parents soient obligés de dormir tous dans une seule pièce, qui est à la fois la chambre, la cuisine et le salon. «Quand quatre personnes s’entassent dans un petit studio, elles ne peuvent pas vivre correctement», explique Manuel Domergue, de la Fondation Abbé-Pierre.


Quelles sont les conséquences du mal-logement ?

«Certains logements rendent les gens malades au lieu de les protéger», regrette Manuel Domergue, le directeur des études de la Fondation Abbé-Pierre. Les personnes qui vivent dans des habitations insalubres ont plus de risque d’être en mauvaise santé.
Le plus souvent, elles ont des maladies respiratoires, qui peuvent parfois se transformer en maladies très graves comme des pneumonies. «La trop grande humidité abîme les poumons des gens», assure Jean-Baptiste Eyraud, le porte-parole de l’association Droit au logement. Certains vivent dans des habitations sans fenêtre, où il y a très peu de lumière et d’aération. Leur corps se fragilise et ils tombent plus souvent malades.
Les victimes du mal-logement souffrent aussi souvent de graves problèmes psychologiques. «Quand il y a des grandes fissures dans les murs d’une maison, ses habitants ont peur en permanence qu’elle s’écroule. Etre tout le temps angoissé, ce n’est pas une vie !» explique Jean-Baptiste Eyraud, la voix pleine de colère. Dans les familles, cette situation crée des tensions. «A force d’être les uns sur les autres dans un logement trop petit ou d’être confrontés au froid, on s’énerve et on se dispute sans cesse», insiste-t-il.
Quand des gens habitent dans un logement en très mauvais état, c’est aussi plus compliqué d’avoir une vie sociale. «Ils n’osent plus inviter des amis chez eux car ils ont honte. Du coup ils n’osent pas aller chez les autres non plus et se recroquevillent sur eux-mêmes», affirme Manuel Domergue.
C’est aussi très difficile pour les enfants. Comment grandir dans un logement trop petit pour y faire ses devoirs ou pour jouer et pas assez chauffé pour bien y dormir ? Manuel Domergue explique que «les plus jeunes vont à l’école fatigués, ils ont plus de mal à se concentrer et à apprendre leur leçon».


Pourquoi y a-t-il autant de mal-logés en France ?

En France, la loi dit que tout le monde doit pouvoir habiter dans un logement en bon état. Dans la réalité, c’est loin d’être le cas. Il y a plusieurs causes à ça.
En France, se loger coûte cher. Les logements sont plus confortables que ceux de nos grands-parents lorsqu’ils étaient petits. Du coup, ça coûte plus cher de les construire… Nous sommes aussi toujours plus nombreux. Près de 67 millions de personnes vivent aujourd’hui en France. C’est 7 millions de plus qu’au début des années 2000, il y a 19 ans.
Il y a aussi de plus en plus de familles à loger à cause des divorces et des parents qui élèvent seuls leurs enfants. Or, même si on construit beaucoup de logements, on n’en construit pas encore assez pour satisfaire tout le monde. Dans certaines villes où il y a beaucoup de demandes, comme Paris, les logements deviennent de plus en plus rares et donc de plus en plus chers.
Les personnes qui gagnent peu d’argent peuvent normalement avoir accès à un logement social. C’est un logement construit et payé en partie par l’Etat. On appelle ça un HLM, une habitation à loyer modéré, c’est-à-dire à un prix bas. C’est réservé aux familles qui ne gagnent pas beaucoup d’argent. En France, on compte près de 5 millions de logements sociaux.
Ce n’est pas suffisant pour abriter toutes les familles qui y ont droit. Environ 1 million de personnes attendent de pouvoir emménager dans un logement social. Elles doivent souvent patienter pendant plusieurs années. En attendant, elles sont parfois obligées d’attendre dans des logements trop petits ou en mauvais état.
Quand les gens n’arrivent pas à trouver de logement parce qu’ils ne gagnent pas bien leur vie par exemple, ils finissent par accepter de louer des logements en très mauvais état à des propriétaires qu’on appelle des «marchands de sommeil». Ces personnes profitent de la misère de ceux qui cherchent juste un toit pour vivre et dormir. Elles leur louent des logements sales, dangereux et tout petits pour un prix souvent très cher. C’est une pratique interdite et punie par la loi.


Comment lutter contre le mal-logement ?

La Fondation Abbé-Pierre, qui s’occupe des personnes mal logées, estime qu’il y a environ 600 000 logements insalubres en France. Environ 1 million de personnes vivent dans ce type d’habitat.
L’Etat paye déjà des travaux pour améliorer des logements. Mais pour les associations, ce n’est pas suffisant. Christophe Robert, le délégué général de la Fondation Abbé-Pierre, souhaite que le gouvernement en fasse plus. Ça demande un important travail de recherche parce que les taudis ne sont pas visibles de l’extérieur, ils sont souvent bien cachés.
Une loi vient aussi d’être votée pour lutter contre les «marchands de sommeil». Depuis le début de l’année, les maires peuvent mettre en place des permis de louer : si un propriétaire veut louer son appartement, il doit obtenir l’autorisation auprès de la mairie, qui vérifie s’il peut être habité. S’ils ne possèdent pas d’autorisation, les propriétaires risquent d’être condamnés à d’importantes amendes.
Enfin, les associations souhaitent aussi que les logements vides ne le restent pas. Dans le centre de Paris, un logement sur quatre est inhabité ! Au début du mois de janvier, des militants de l’association Droit au logement et des sans-abri ont occupé pendant quelques heures les locaux vides d’une grande entreprise française. Ils demandaient de pouvoir utiliser ces bureaux pour abriter des personnes sans domicile. Ils ont finalement été chassés. En France, on ne peut pas occuper un logement vide, même si son propriétaire ne l’utilise pas depuis très longtemps.


