N°6 - février 2016 Les secrets du 29 février 
Le portrait de Malo, né un 29 février

Environ 4 millions de personnes dans le monde sont nées un 29 février. C’est le cas de Malo, 12 ans, qui habite dans la région parisienne. Cette année, il va fêter son anniversaire le jour de sa naissance, ce qui ne lui était arrivé que deux fois dans sa vie : à 4 ans et à 8 ans. Ses copains disent en riant qu’il va donc avoir trois ans cette année ! Et ça l’amuse aussi.
Quand sa maman montre sa carte d’identité aux gens, on lui dit souvent que son fils n’a pas de chance d’être né un 29 février. Malo, au contraire, est assez fier de sa particularité.
D’habitude, il souffle ses bougies le 28 février au soir, avec ses parents, et ça ne le dérange pas. Comme personne d’autre dans son entourage ne fête son anniversaire le 29 février, tout le monde pense à le lui souhaiter le jour d’avant ou celui d’après. Ça prolonge donc le plaisir !
Malo dit qu’il n’attend pas particulièrement cette grande journée cette année, mais il va quand même partir à l’étranger pour l’occasion, avec ses parents : ce sera à Rome, en Italie, ou dans le sud du Portugal, il ne sait pas encore. Et sa mère lui prépare aussi une petite surprise au retour des vacances...
