N°11 - juillet 2016 L'attaque de Nice 
Que s'est-il passé à Nice ?

Le jour de la fête nationale, le 14 juillet, une attaque a eu lieu à Nice, dans le sud-est de la France.
Puisque c’était le 14 juillet, beaucoup de gens s’étaient réunis pour regarder le feu d’artifice. A la fin du spectacle, ils sont repartis, en marchant sur la promenade des Anglais, la rue la plus connue de la ville, au bord de la mer. A ce moment-là, un homme qui conduisait un gros camion a foncé sur la foule. Il a tué au moins 84 personnes, dont 10 enfants et adolescents.
En voyant l’homme renverser les gens avec son camion, des policiers lui ont tiré dessus pour l’arrêter. Le conducteur a lui aussi tiré avec son pistolet, puis la police l’a tué.
Les gens qui se trouvaient sur la promenade des Anglais et dans d’autres rues de Nice ont eu très peur quand ils ont vu le camion et se sont mis à courir. Ils se sont cachés dans des restaurants, des hôtels ou chez eux. Beaucoup d’adultes et d’enfants ont été blessés et sont allés à l’hôpital pour être soignés. Les personnes ayant plus de 18 ans ont donné un peu de leur sang pour aider les blessés qui en ont perdu.
