N°124 - 18 au 24 octobre 2019 le génie Léonard de Vinci
Louis, 8 ans, adore Léonard de Vinci

Pour Louis, 8 ans, c’est évident, «Léonard de Vinci, c’est un génie. C’est même un peu plus qu’un génie. Ça me saute aux yeux». Il admire ce peintre et inventeur depuis plusieurs années déjà «parce qu’il a construit des machines incroyables, je ne sais même pas comment il a fait».
Louis habite à Blois (Loir-et-Cher), tout près d’Amboise (Indre-et-Loire) où l’artiste italien a passé les trois dernières années de sa vie avant de mourir il y a 500 ans. Louis a d’ailleurs visité récemment le château du Clos Lucé où Léonard de Vinci a habité. C’est là qu’il a pu admirer les maquettes grandeur nature des machines imaginées et dessinées par l’artiste qu’on peut découvrir plus loin dans ce numéro. «J’ai même fait tourner l’hélice de l’hélicoptère», raconte-t-il avec beaucoup de fierté.
Louis sait aussi que Léonard de Vinci a peint des «tableaux magnifiques», dont la célèbre Joconde qui est exposée au Louvre, un très grand musée à Paris. Il sait que c’est le portrait d’une dame mais c’est son père qui lui souffle qu’elle s’appelle Monna Lisa. Louis aimerait beaucoup la voir au Louvre. Il espère que ses parents vont bientôt l’y emmener.
Louis adore inventer des choses et construire des chars ou des hélicoptères avec ses Legos et son «imagination». Il dessine aussi «de tout», d’ailleurs. Plus tard, il rêve d’être archéologue ou alors «inventeur de machines, comme Léonard de Vinci».
Blois, la ville où habite Louis, et Amboise, où Léonard de Vinci a vécu à la fin de sa vie
