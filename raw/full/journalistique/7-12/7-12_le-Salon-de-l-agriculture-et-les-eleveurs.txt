N°46 - 23 février au 1er mars 2018
le Salon de l'agriculture et les éleveurs
Des vaches, des cochons ou des poules en plein Paris : comme chaque année, des agriculteurs de toute la France se retrouvent dans la capitale pendant plusieurs jours pour présenter leurs animaux et leurs produits au Salon de l’agriculture. Ce grand événement se déroule cette fois du 24 février au 4 mars. Parmi les agriculteurs présents, il y a des éleveurs. Comment travaillent ceux qui s’occupent des animaux ? Que devient le lait de la vache et d’où vient la viande que l’on mange ? Dans ce nouveau numéro, je t’emmène découvrir les fermes.


Corinne élève des poulets et des dindes

«J’aime passer toute la journée dehors, dans la nature», explique Corinne. Cette agricultrice vit dans la Drôme, dans le sud-est de la France. Dans sa ferme, elle élève des volailles (des poulets et des dindes). Elle cultive aussi des arbres fruitiers (abricotiers, poiriers) et des champs de céréales.
Corinne travaille en famille, avec son mari, leur fils et sa femme et le frère de Corinne. Ils possèdent 40 000 volailles et ça demande beaucoup de travail : «Je me lève à 6 heures et je vais voir les volailles dans leur bâtiment, pour m’assurer qu’il est à la bonne température et qu’elles ont assez d’eau, de nourriture et qu’elles sont en bonne santé.» Ensuite, elle remet de la paille dans les bâtiments où vivent les volailles. «C’est une tâche assez longue car ça se fait à la main», explique-t-elle.
L’après-midi, elle doit souvent remplir des papiers ou payer des factures par exemple. Parfois, elle aide aussi son mari à s’occuper des arbres fruitiers : soigner les arbres, ramasser les fruits lorsque c’est la saison…
Le soir, elle fait un dernier tour des volailles vers 19 heures avant de rentrer dans sa maison, située tout près. «Lorsqu’on a des animaux, il faut habiter dans la ferme. C’est plus pratique, et ça nous rassure. Mais du coup, on ne peut pas prendre beaucoup de vacances.»
Quand les volailles ont un peu plus d’un mois, elles sont emmenées à l’abattoir. La viande est ensuite mise en barquettes par d’autres personnes, pour être vendue dans les supermarchés de la région.
Corinne et son mari travaillent dans leur ferme depuis 27 ans. Mais ils ont commencé à élever des volailles il y a 5 ans. «Le métier d’agriculteur, c’est savoir s’adapter. Il faut s’adapter aux nouvelles lois, à ce que les gens ont envie de manger.»
Saint-Barthélemy-de-Vals, le village où vit et travaille Corinne


Qu’est-ce que le Salon de l’agriculture ?

Le Salon de l’agriculture existe depuis 54 ans et se tient tous les ans à Paris. Cette année, il a lieu du 24 février au 4 mars. On dit que c’est un salon parce qu’il réunit des agriculteurs de toute la France qui viennent avec leurs bêtes et les produits qu’ils fabriquent. Ils vendent ainsi sur place aux visiteurs du fromage, du miel, du vin ou de la viande… Des entreprises qui travaillent à partir des produits de l’agriculture sont aussi présentes.
«Le salon permet aux agriculteurs de montrer le résultat de leur travail. Pour les visiteurs, c’est l’occasion de leur demander comment ils travaillent. Certains viennent voir les animaux, demander aux éleveurs combien de fois par jour ils traient les vaches. D’autres encore veulent goûter les produits des régions qu’ils ne connaissent pas», explique Valérie Le Roy, la directrice du Salon de l’agriculture.
«En France, l’agriculture est très importante : beaucoup de gens ont eu des grands-parents ou un membre de leur famille qui était agriculteur», indique Valérie Le Roy. Cet événement intéresse donc de nombreuses personnes.
La visite du Salon est aussi une tradition pour les femmes et les hommes politiques. Ils y vont pour se montrer et pour voir les agriculteurs, qui leur parlent chaque année des problèmes qu’ils rencontrent. Comme tous les présidents avant lui, Emmanuel Macron s’y rendra aussi.


Qu’est-ce qu’un éleveur ?

En France, 710 000 personnes travaillent dans l’agriculture, c’est-à-dire environ une personne sur 100. Il y a 450 000 fermes, dont les tailles sont très différentes.
Certains agriculteurs cultivent la terre pour faire pousser des fruits, des légumes et des céréales. D’autres élèvent des animaux : vaches, cochons, poules, chevaux… Ce sont des éleveurs.
Certains pratiquent l’élevage «intensif», c’est-à-dire qu’ils élèvent un maximum d’animaux pour produire le plus possible de viande, de lait ou d’œufs. On parle par exemple d’élevage intensif quand une ferme a plus de 40 000 volailles (comme des poules). Les animaux grandissent souvent serrés les uns contre les autres dans de grands hangars avec de la lumière artificielle, sans lumière du jour donc. Les volailles sont tuées lorsqu’elles ont entre un mois et trois mois.
D’autres éleveurs ont de plus petites exploitations. C’est par exemple le cas d’Anthony Clouté, 42 ans, qui possède 1 000 poulets à Pontiacq-Viellepinte, dans les Pyrénées-Atlantiques. Cet agriculteur achète des poussins lorsqu’ils ont un jour. Il les élève pendant un mois dans des espaces chauffés, puis pendant trois mois dans des enclos à l’air libre. «Les poulets ont des petites cabanes pour la nuit ou pour se protéger du froid. Il y a aussi des arbustes pour leur faire de l’ombre l’été», explique-t-il.
Anthony élève donc ces poulets pendant quatre mois, soit un mois de plus que dans l’élevage intensif. Il les nourrit avec les céréales qu’il cultive sans pesticide et avec le moins d’engrais possible. «J’ai choisi d’élever peu de volailles pour leur bien-être mais aussi parce qu’on ne peut pas faire à la fois de la quantité et de la qualité», remarque-t-il.
L’éleveur vend ensuite ses poulets sur les marchés ou avec l’aide d’associations qui mettent en lien les petits producteurs et les consommateurs. «Ça me permet d’être payé en fonction du travail que je fais mais aussi de voir les personnes qui mangent ma viande. Je fais du poulet pour nourrir les gens. C’est important d’avoir leurs retours», ajoute cet agriculteur.


Que devient le lait de la vache ?

En France, 70 000 agriculteurs élèvent des vaches laitières. Ces fermiers produisent 23,5 milliards de litres de lait par an. Mais tout ce lait n’est pas seulement bu en France. D’ailleurs, une grande partie du lait n’est pas bu du tout mais transformé en d’autres produits.
Dès qu’il sort du pis de la vache, le lait est gardé au froid. L’agriculteur le garde dans sa ferme entre deux et trois jours. Ensuite, il est transporté dans une laiterie. Là, le lait est transformé en produits laitiers comme du fromage, du beurre ou des yaourts.
Les produits qui sortent des laiteries sont principalement vendus dans les supermarchés. Beaucoup sont aussi vendus dans les pays étrangers.
Quand on achète une brique de lait, une partie de l’argent revient à l’agriculteur. Une partie va à la laiterie et une autre au magasin qui a vendu la brique. Chacun veut gagner une «marge», c’est-à-dire vendre le lait ou le produit un peu plus cher que ce que ça lui a coûté à fabriquer. Si on achète un produit 1 euro et qu’on le vend 1 euro, on n’a rien gagné. En revanche, si on vend ce produit 1,20 euro, on a gagné 20 centimes : c’est ça, la marge.
Parfois, des agriculteurs manifestent à cause des marges. Ils ne sont pas d’accord avec ceux qui dirigent les laiteries ou les supermarchés. Ils n’arrivent pas à s’entendre sur le prix du lait. Les agriculteurs disent que les laiteries n’achètent pas leur lait assez cher, par rapport à ce que leur coûte la fabrication de ce lait (machines pour traire les vaches, temps passé à les soigner…).
«Souvent, les laiteries et les grandes surfaces font partie de gros groupes, d’entreprises puissantes. Ce sont elles qui fixent les prix et les agriculteurs n’ont pas assez de pouvoir pour s’y opposer», explique Vincent Chatellier, scientifique spécialiste de l’agriculture. Cette «guerre des prix» existe aussi pour d’autres produits issus de l’agriculture, comme la viande.


Comment savoir d’où viennent la viande ou le lait ?

Quand on achète de la viande au supermarché, par exemple du steak haché, on peut trouver des indices sur l’étiquette pour savoir d’où elle vient. Il y a :
Toutes ces informations sont utiles pour savoir d’où vient cette viande. Elles permettent de retracer le parcours de la viande : d’où venait la vache, où elle a été abattue, dans quelle usine elle a été hachée et mise en barquette.
Ces informations peuvent être utiles en cas de problème. Si plusieurs personnes tombent malades après avoir mangé de la viande, le supermarché regarde le numéro du lot pour voir quelles autres barquettes de viande ont été fabriquées au même moment que celle qui a rendu les gens malades.
Pour éviter que d’autres personnes tombent malades, le magasin les retire alors de ses rayons et prévient les clients qui en ont acheté de ne pas manger la viande. Ça peut arriver avec de la viande mais aussi avec du lait, comme il y a quelques semaines avec le lait en poudre de l’entreprise Lactalis. Des bébés ont été malades après en avoir bu. Le lait en poudre avait en effet été contaminé par une bactérie.


