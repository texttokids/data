N°90 - 25 au 31 janvier 2019 les restaurants étoilés
Description

Ils sont capables de préparer un cordon bleu meilleur que tu ne l'as jamais rêvé… Les grands chefs cuisiniers réalisent des plats incroyables et attendent chaque année l’événement le plus important pour leur profession : la remise des étoiles du Guide Michelin. Ça a eu lieu le 21 janvier. C’est le top des prix que peuvent recevoir les meilleurs cuistots. Enfile ton plus beau tablier, nous allons entrer ensemble dans les coulisses du Guide Michelin et dans la cuisine d’un restaurant étoilé !