N°66 - 13 au 19 juillet 2018 le festival d'Avignon
Comment crée-t-on une pièce de théâtre ?

Pour monter une pièce de théâtre il faut bien sûr des comédiens. Mais ce n’est pas suffisant ! Il faut beaucoup d’autres personnes pour créer un spectacle. Il y a différentes étapes à suivre pour qu’il existe. Camille Hazard, metteuse en scène de la pièce Une bouteille à la mer, qui sera à Avignon cet été au théâtre du Petit-Louvre, a aidé P’tit Libé à y voir plus clair.
1) Choisir l’histoire
L’histoire de la pièce de théâtre peut venir d’une pièce ou d’un livre qui existent déjà ou elle peut être complètement nouvelle, c’est-à-dire venir d’une idée originale de l’auteur.
2) Ecrire la pièce
Si la pièce ne vient pas d’un livre, il faut l’écrire. Il y a beaucoup plus de dialogues que dans un roman. Les émotions et les déplacements des personnages sont par exemple indiqués. La pièce est divisée en actes, qui sont comme des chapitres d’un livre.
3) Faire le casting et trouver de l’argent
Une fois la pièce écrite, il faut des comédiens pour jouer les rôles des personnages. Un casting est organisé pour les trouver : des comédiens viennent jouer un morceau de la pièce pour être sélectionnés.
Avant ou après le casting, il faut trouver de l’argent. C’est souvent le producteur qui s’en occupe. Il peut par exemple demander de l’argent auprès d’une région.
1) Les répétitions
Une fois que tous les comédiens sont trouvés, les répétitions commencent. Ils se rassemblent avec le metteur en scène et lisent le texte, chacun leur tour, en fonction de leur personnage. On appelle ça «faire des lectures». Ensuite, les comédiens apprennent leur texte et jouent sur scène pour répéter. Parfois, ils jouent sans utiliser le texte de la pièce, en inventant une histoire, ce sont des improvisations.
2) La mise en scène
Au cours des répétitions, le metteur en scène décide de la forme de la pièce. Par exemple : où vont être placés les comédiens et quand ils vont parler. Le metteur en scène est accompagné par des techniciens, qui s’occupent de la lumière, du son, du décor et des costumes.
3) La tournée
La pièce est prête. Le directeur d’un théâtre a choisi de la programmer dans son théâtre. La pièce va être jouée pour la première fois, ça s’appelle une «première». Après les premières représentations, le spectacle peut partir en tournée. Dans ce cas, ça veut dire que la pièce sera jouée parfois pendant plusieurs mois dans un grand nombre de villes différentes, en France ou même à l’étranger.
