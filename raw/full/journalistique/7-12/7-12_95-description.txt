N°95 - 1er au 7 mars 2019 l’antisémitisme
Description

Ces dernières semaines, plusieurs actes antisémites, c’est-à-dire dirigés contre des personnes juives ou des symboles juifs, ont été dénoncés. Des manifestations ont été organisées en France pour dire «non à l’antisémitisme» et au rejet de l’autre. Pourquoi des gens s’attaquent-ils aux juifs ? Comment lutter contre cette violence ? Qu’est-ce que le judaïsme ? Je réponds à ces questions dans ce nouveau numéro, qui est important pour lutter contre l’intolérance.