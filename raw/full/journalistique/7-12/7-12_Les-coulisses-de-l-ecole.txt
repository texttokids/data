N°13 - septembre 2016
Les coulisses de l'école
Jeudi 1er septembre, plus de 12 millions d’élèves font leur rentrée des classes. Et ils ne sont pas les seuls à aller à l’école. Leurs professeurs, plus de 860 000 cette année, ont rejoint la classe un jour avant eux.


Florent fait sa première rentrée comme prof

Florent, 26 ans, fait sa première rentrée en tant que professeur à l’école primaire cette année. Il a en effet réussi ses examens à l’école des enseignants, où il a étudié pendant deux ans. L’année dernière, il a donné des cours à une classe de CE2 : il enseignait deux jours et demi par semaine pour apprendre le métier. Cette fois-ci, il est vraiment professeur.
Comme beaucoup de maîtres et de maîtresses qui commencent, il est remplaçant, ça veut dire qu’il prend la place des enseignants qui sont absents, par exemple parce qu’ils sont malades. La plupart du temps, les remplaçants ont plusieurs classes dans l’année.
Florent a appris deux jours avant la rentrée qu’il travaillerait avec une classe de CE2 dans le XXe (20e) arrondissement de Paris jusqu’au mois de mars. Ça tombe bien puisqu’il avait déjà travaillé avec des CE2 l’an dernier. Pour bien se préparer, il a discuté avec le professeur qu’il remplace de la façon dont il enseigne.
Florent sait qu’il travaillera beaucoup les premiers mois pour préparer ses cours. Quand il enseignait l’an dernier, il travaillait déjà jusqu’à minuit après l’école pour que tout soit prêt pour ses élèves. Ça ne le dérange pas parce qu’il aime son métier : il a le sentiment de servir à quelque chose quand il voit les élèves apprendre des choses grâce à lui.
Paris, la ville où enseigne Florent


D’où viennent les profs ?

Avant d’arriver devant une salle de classe, les enseignants, eux aussi, sont sur les bancs de l’école. Pour devenir professeur, en primaire ou au collège, ils doivent d’abord obtenir une licence, le diplôme qu’on passe après trois années d’études à l’université. En général, les professeurs de collège ont choisi une licence qui correspond à la matière qu’ils enseignent. Beaucoup de professeurs de français, par exemple, ont étudié la littérature à l’université.
Après ces trois ans d’études, ils passent deux ans dans une école de professeurs, à l’université. Ils y apprennent à proposer des activités accessibles à des écoliers ou à des collégiens dans différentes matières, ou à gérer une classe. Par exemple, ils font des exercices de respiration : ils s’entraînent à parler fort sans pour autant crier. C’est une habitude à prendre, d’ailleurs les jeunes professeurs n’ont parfois plus de voix à la fin de la journée !
A l’école, les futurs profs découvrent aussi comment la mémoire fonctionne. On leur déconseille par exemple de donner trop d’informations en même temps, sinon les élèves n’arrivent pas à les retenir. Dans leur emploi du temps, il y a aussi des cours sur les valeurs de l’école, comme la laïcité. Parce que c’est un principe important en France : l’Etat n’interdit aucune religion et n’oblige personne à en pratiquer une. L’école publique, qui est gérée par l’Etat, est donc laïque.
Durant leurs études, les futurs enseignants vont aussi dans des classes pour voir comment se passent les cours. Au début, ils s’installent au fond de la salle pour observer un professeur face à ses élèves. Puis c’est à leur tour de faire cours aux élèves, qui ne savent pas qu’ils ne sont pas vraiment encore des professeurs. Cette fois-ci, un tuteur les observe : c’est un enseignant qui les évalue et les conseille. Sans rien imposer, il peut leur donner des astuces, comme passer dans les rangs, regarder les élèves dans les yeux ou parler fort pour se faire respecter.
Pour être diplômé, et donc devenir professeur, il ne suffit pas d’entrer dans cette école. Il faut réussir un concours à la fin de la première année, puis un examen à la fin de la deuxième.


Qui fait quoi à l’école ?

Les parents peuvent choisir d’inscrire leur enfant dans une école publique ou privée. L’école publique dépend de l’Etat et est donc gratuite. Dans une école privée, les parents participent aux frais de scolarité. La plupart des écoles privées reçoivent de l’argent de l’Etat. Quelques-unes n’ont pas d’aide, elles sont plus chères et ont plus de libertés dans les programmes des cours. Il existe notamment des écoles privées catholiques, musulmanes ou juives.
Des élèves aux professeurs, en passant par le directeur et les parents d’élèves : tout le monde a un rôle à jouer à l’école.
Les élèves
A partir de 2 ans, un enfant peut aller à l’école maternelle mais ce n’est pas une obligation. La scolarité est en revanche obligatoire à partir de 6 ans et jusqu’à 16 ans. Les élèves vont à l’école élémentaire (du CP au CM2) de 6 à 11 ans. Ils ont 24 heures de cours par semaine, du lundi au vendredi, parfois le samedi matin.
Le directeur de l'école
Son rôle est très important : le directeur est un professeur qui est là pour que tout marche bien dans l’établissement et qui est responsable de la sécurité. C’est lui qui répartit les élèves dans les classes en début d’année. Dans certaines écoles il enseigne, dans d’autres non.
Le directeur préside le conseil d’école qui a lieu au moins une fois par trimestre, c’est-à-dire tous les trois mois. Autour de lui, il y a le maire de la ville ou quelqu’un qui le représente, les professeurs de chaque classe, des représentants des parents d’élèves et des membres de l’Education nationale (ils travaillent pour l’Etat).
Tous ensemble, ils mettent en place un projet d’école (ce sont des objectifs pour améliorer les résultats des élèves) et votent un règlement intérieur. Dans le règlement intérieur, on trouve les heures d’entrée et de sortie, les règles d’hygiène (de propreté) et de sécurité, les mesures de prévention contre le harcèlement ou encore les punitions.
Le directeur préside aussi le conseil des maîtres qui a lieu une fois par trimestre et permet de discuter de l’organisation et de tous les problèmes qu’il peut y avoir à l’école.
Le professeur des écoles (maitresse ou maitre)
Il organise les cours comme il veut, mais il doit suivre le programme scolaire décidé par le ministère de l’Education nationale. A l’école primaire, il apprend notamment aux élèves à lire, à écrire et à compter. Français, mathématiques, histoire et géographie, sciences, langue étrangère, musique, arts plastiques… Le professeur des écoles doit tout savoir enseigner et il ne fait pas que ça ! Il doit aussi surveiller les élèves quand ils ne sont pas en classe, préparer les cours, corriger les copies, organiser les sorties scolaires ou se réunir avec les autres enseignants et la direction.
Les parents d'élèves
Les parents sont informés toute l’année des résultats et du comportement de leur enfant, notamment avec le livret scolaire. A partir de cette année, ils pourront aussi suivre les informations du livret scolaire sur Internet. Les parents peuvent également rencontrer le professeur ou le directeur quand ils le souhaitent.
Chaque année en octobre, tous les parents votent pour élire leurs représentants, qu’on appelle aussi des délégués des parents d’élèves. Ils représentent les parents d’élèves à l’école et doivent donc s’intéresser à tous les élèves et pas seulement à leur enfant. Les parents délégués participent au conseil d’école.
L'animateur
L’animateur s’occupe des écoliers en dehors du temps passé en classe, pendant la pause déjeuner ou l’après-midi. Il organise des activités sportives, comme de la gym ou du basket, ou des activités culturelles, comme du théâtre, de la peinture ou du chant. Il propose aussi de l’aide aux devoirs. Il y a en général plusieurs animateurs par école.
Les autres métiers
Le personnel de la cantine
En primaire, les repas sont parfois cuisinés en dehors de l’école avant d’être servis. Sinon, ils sont préparés sur place par un chef cuisinier. Il peut être aidé par d’autres cuisiniers. Dans une cantine, il y a aussi le personnel de service : ces personnes sont chargées de mettre la table, d’apporter les repas, d’aider les plus petits à couper leur viande, par exemple, et doivent surveiller les enfants. Il peut aussi y avoir des animateurs pour les aider. Ensuite, il y a les personnes chargées de tout nettoyer, de la vaisselle au sol. Et parfois ce sont les mêmes personnes qui s’occupent de servir et de nettoyer.
Le médecin et l’infirmier
Le médecin scolaire doit faire un bilan de santé. C’est obligatoire quand l’enfant a 6 ans. S’il ne l’a pas fait en grande section de maternelle, il peut le faire en CP. A ce moment-là, le médecin vérifie, en présence des parents, que les enfants entendent et voient bien et n’ont pas de difficultés pour parler, par exemple. Le médecin ou un infirmier peut aussi venir à l’école si les élèves en ont besoin, mais il n’y a pas assez de médecins scolaires en France donc ce n’est pas toujours possible.
Le psychologue scolaire
Le psychologue scolaire travaille dans plusieurs écoles, où il aide les élèves qui ont des difficultés pour apprendre ou des problèmes de comportement. Il propose des solutions pour régler ça en discutant avec les parents et l’équipe enseignante.
L’AVS
L’accompagnant d’élève en situation de handicap (avant, on l’appelait l’auxiliaire de vie scolaire, ou AVS) aide les élèves handicapés à prendre des notes en cours par exemple ou à se déplacer dans l’école. Il peut accompagner un ou plusieurs enfants.
Le personnel d’entretien
Il est là pour que tout soit bien propre à l’école : dans la cour, sous le préau, dans les classes.
Le gardien
Il accueille les personnes extérieures à l’école et s’occupe de l’ouverture et de la fermeture de l’établissement.
Les élèves
Ils entrent au collège autour de 11 ans et y restent de la sixième à la troisième. Quand ils arrivent au collège, les élèves voient leur quotidien et leurs habitudes changer : ils ont plusieurs professeurs au lieu d’un seul, changent de salle entre les cours, ont de nouvelles matières, surtout en cinquième, comme la physique-chimie.
Chaque classe a deux délégués. Ce sont les porte-parole des élèves auprès des enseignants et des autres adultes du collège. Ils sont élus par leur classe (ça veut dire qu’elle vote) et participent notamment au conseil de classe et au conseil d’administration.
L'équipe de direction
Le principal est le directeur du collège. C’est lui qui répartit les élèves dans les classes en début d’année. Il préside le conseil de classe, qui se réunit trois fois par an pour discuter des élèves et de l’organisation du collège avec les professeurs, les délégués des élèves, les délégués des parents d’élèves… Il préside aussi le conseil d’administration, qui vote le budget du collège, décide des achats pour les salles de cours, autorise les voyages scolaires et décide de la répartition de certaines heures de cours, comme plus de soutien en français en sixième, par exemple.
Le principal adjoint aide le principal dans son travail et le remplace quand il est absent. Il s’occupe surtout de l’organisation : emplois du temps des élèves, des enseignants, occupation des salles…
Les professeurs
Le professeur est spécialisé dans une matière (maths, histoire-géographie ou français, par exemple) et enseigne dans plusieurs classes. Il doit préparer les cours, les contrôles, corriger les copies, être en contact avec les parents d’élèves… Les collégiens ont environ dix professeurs différents.
Le professeur principal accueille les élèves dans leur classe le jour de la rentrée. Chaque classe a un professeur principal qui a été choisi en début d’année parmi tous les professeurs par le principal du collège. Son travail est de faire le lien entre toutes les personnes qui s’occupent des collégiens : professeurs, parents ou surveillants. C’est le professeur qui connaît le mieux les élèves parce qu’il suit leurs résultats et se renseigne sur leur famille et leur parcours scolaire.
Le professeur documentaliste
Il est responsable des livres et de tous les documents qu’il y a au CDI (le centre de documentation et d’information), la bibliothèque du collège. Sa mission est d’apprendre aux élèves à trouver les documents dont ils ont besoin pour travailler et les former à bien chercher et bien utiliser l’information dans les livres, dans les médias ou sur Internet.
Les parents d'élèves
Ils suivent la scolarité de leurs enfants grâce au carnet de correspondance, ou carnet de liaison, où les professeurs écrivent leurs remarques sur les élèves et donnent des informations aux parents.
Chaque année en octobre, tous les parents votent pour élire leurs représentants, qu’on appelle aussi des délégués des parents d’élèves. Ils représentent les parents d’élèves au collège et doivent donc s’intéresser à tous les élèves et pas seulement à leur enfant. Ils participent au conseil de classe.
Le conseiller principal d'éducation
Le conseiller principal d’éducation, le CPE, est responsable de ce qu’on appelle la vie scolaire. C’est lui qui prend le relais des professeurs en dehors des cours. Il est là pour faire respecter le règlement intérieur, informe les élèves, vérifie leurs absences et leurs retards, règle les conflits… Il est aidé par des surveillants qui observent si tout se passe bien en dehors des cours (dans les couloirs, la cour de récréation, au portail…). Il peut aussi aider les élèves dans leur travail pendant les heures de permanence.
Les autres métiers
Le personnel de cantine
Au collège, les repas sont le plus souvent cuisinés sur place. Une personne s’occupe de la livraison des aliments, d’autres sont parfois chargées des entrées froides par exemple et le chef de cuisine est aidé par au moins un ou deux cuisiniers. Il y a aussi le personnel de service qui guide les élèves pour choisir leur repas. Les plats sont souvent servis en self-service : les élèves prennent un plateau et choisissent ce qu’ils veulent manger. Il y a également des personnes chargées de les surveiller et celles qui doivent tout nettoyer, de la vaisselle au sol.
L'infirmier et le médecin
Au collège, les élèves de sixième doivent faire un deuxième bilan de santé, après celui qu’ils ont fait en grande section de maternelle ou en CP. Cette fois-ci, c’est un infirmier qui vérifie que tout va bien et les parents ne sont pas présents. L’infirmier peut faire appel au médecin scolaire si un élève a un problème de santé important.
L'assistant de service social
Il est là pour écouter, aider et conseiller les élèves qui ont des problèmes à l’école ou dans leur famille.
Le conseiller d’orientation
Le COP (conseiller d’orientation-psychologue) aide les élèves à trouver ce qu’ils feront plus tard. A la fin du collège, en troisième, ils doivent choisir d’aller dans un lycée technologique (qui permet de se spécialiser dans un métier) ou un lycée d’enseignement général (il n’y a pas de cours pour apprendre un métier, seulement des matières comme le français ou l’histoire). Le conseiller d’orientation intervient en classe ou rencontre un à un les élèves pour les informer sur les formations et les métiers qui existent.
Le personnel d'entretien
Ils sont là pour que tout soit bien propre à l’école : dans la cour, sous le préau, dans les classes.
Le gardien
Il accueille les personnes extérieures à l’école et s’occupe de l’ouverture et de la fermeture de l’établissement.


Qui paye pour l’école et combien ça coûte ?

Pour bien apprendre, il faut des professeurs, du matériel, des salles de classe… Tout ça coûte de l’argent, notamment pour donner leur salaire aux enseignants ou acheter du matériel informatique. C’est surtout l’Etat qui paye, mais les familles participent.
A l’école, les élèves ont besoin de stylos, de cahiers, d’un compas ou encore de crayons de couleur. Ce sont leurs familles qui les achètent. Celles qui n’ont pas assez d’argent sont aidées par l’Etat. La somme qu’il leur donne s’appelle l’allocation de rentrée scolaire.
On estime que pour cette rentrée, une famille devra dépenser 190 euros de fournitures si elle a un enfant qui entre en sixième. Cette somme n’est qu’une moyenne : ça coûte forcément plus cher s’il faut racheter un cartable que si on réutilise celui de l’année précédente.
Tout le matériel utilisé par les élèves n’est cependant pas financé par les familles. Les tables ou les chaises, par exemple, sont achetées par les mairies pour l’école primaire, par les départements pour le collège. Le matériel informatique, à partir de la sixième, est financé à moitié par les départements, à moitié par le ministère de l’Education nationale.
La construction des écoles, les travaux dans les bâtiments ou encore le ménage sont gérés par la mairie de la commune (une ville ou un village). Pour les collèges, c’est le département qui décide et qui paye.
En général, le tarif d’un repas est fixé en fonction des revenus des parents et du nombre de leurs enfants : plus la famille gagne d’argent, et moins elle a d’enfants, plus elle va payer cher. A Bordeaux (Gironde) par exemple, un repas à l’école primaire coûte entre 45 centimes et 5,35 euros par enfant. Et si une famille a plus de trois enfants, ça peut descendre à 23 centimes par repas. A partir du quatrième, la cantine peut même être gratuite.
La partie du repas qui n’est pas payée par la famille est prise en charge par la commune pour les écoles primaires et par le département pour les collèges. C’est aussi la commune et le département qui décident des menus de la cantine.
Que ce soit en primaire ou au collège, si on habite loin de son établissement, il faut parfois emprunter les transports en commun. Il y a deux cas de figure : soit l’élève prend les moyens de transport en commun habituels (le bus, le métro, le tramway…) avec un abonnement payé par ses parents, soit il prend un car de ramassage scolaire, qui peut être payé par la mairie ou par le département.
Chaque année, l’Education nationale dépense 6 120 euros pour un élève de primaire et 8 410 euros pour un collégien. Ce coût comprend le salaire des enseignants, du directeur d’école, du conseiller principal d’éducation au collège, etc. En primaire comme au collège, c’est l’Education nationale (et donc l’Etat) qui les paye. C’est notamment à ça que servent les impôts (l’argent qu’une partie des gens qui travaillent donne à l’Etat).
Chaque mois, un professeur (de primaire ou de collège) touche entre 1 200 et 2 800 euros environ selon son niveau d’expérience et ses diplômes, un directeur d’école environ 2 200 euros et un principal de collège entre 4 000 et 7 000 euros.


Comment fonctionnent les emplois du temps ?

En primaire, on a souvent un seul professeur, alors qu’au collège, on en a un par matière. Forcément, il faut organiser tout ça dans un emploi du temps.
A l’école primaire, ce sont les professeurs qui décident des emplois du temps. Au collège, c’est le principal ou le principal adjoint. A partir du collège, les emplois du temps changent d’une semaine sur l’autre, entre la semaine A et la semaine B, ce qui permet de mieux répartir les élèves et les matières.
En primaire, depuis 2014, les élèves ont école quatre jours et demi par semaine au lieu de quatre jours. Ça fait 24 heures de cours par semaine maximum. On appelle ça la réforme des rythmes scolaires. La plupart des élèves vont à l’école du lundi au vendredi, et il n’y a pas classe le mercredi après-midi. Dans certains établissements, ils ont classe le samedi matin, mais pas du tout le mercredi.
L’idée, c’est qu’en ayant plus de jours d’école, et donc moins d’heures chaque jour, on puisse mieux apprendre. A la fin, ça ne fait pas plus d’heures de cours, mais elles sont davantage réparties dans le temps.
Dans les écoles privées, cette réforme n’est pas obligatoire. C’est le directeur qui décide du nombre de jours travaillés.
La France est découpée en trois zones : A, B et C. La Corse et les territoires français qui ne sont pas en Europe (l’outre-mer) dépendent d’un autre calendrier. Les dates des vacances changent selon la zone dans laquelle on va à l’école. Ainsi, lors des vacances de février ou de celles de printemps, tous les élèves de France ne sont pas en congés en même temps. Si des membres d’une même famille n’habitent pas dans la même zone, ils ne pourront donc pas forcément partir ensemble.
Heureusement, ils pourront se retrouver à la Toussaint (du 19 octobre au 3 novembre), aux vacances de Noël (du 17 décembre au 3 janvier), ou pour les grandes vacances (à partir du 8 juillet 2017), qui ont lieu aux mêmes dates dans toutes les zones.
Si tout le monde était en vacances en même temps, les restaurants, les piscines, les plages ou encore les stations de ski seraient remplis de monde. Cette organisation permet donc de ne pas trop encombrer les lieux à visiter, comme les châteaux ou les musées, ou les autoroutes. Ça permet aussi aux hôtels ou aux campings, par exemple, d’avoir des touristes pendant plus longtemps. Les dates sont fixées par le ministère de l’Education nationale.


Qui décide de ce qu’on apprend à l’école ?

Les programmes, c’est-à-dire ce qui doit être enseigné à l’école, sont choisis par un groupe de personnes désignées par le ministère de l’Education nationale. Il est composé de députés, de sénateurs, de professeurs, d’inspecteurs des écoles et de chercheurs. Ils ont par exemple décidé qu’à la fin du collège un élève devait maîtriser la langue française, pratiquer des langues étrangères, comprendre les maths, avoir appris à utiliser des outils numériques ou encore connaître les bases de la citoyenneté. C’est pour ça que les élèves ont maintenant un livret personnel de compétences qui permet de suivre leur progression.
Même s’il a des consignes, le professeur a ensuite la liberté de choisir la façon dont il va faire cours. Par exemple, le professeur de français sélectionne les livres qu’il veut faire étudier. En sixième, il doit choisir des textes de l’Antiquité, la période qui commence avec l’apparition de l’écriture (il y a 6 000 ans) et finit autour du Ve (5e) siècle. En cinquième, c’est la littérature du Moyen-Age, du Ve au XVe (15e) siècle, et de la Renaissance, du XIVe (14e) au XVIe (16e) siècle.
Quand il y a de nouveaux programmes, comme c’est le cas cette rentrée, il y a aussi de nouveaux manuels. Ce sont des maisons d’édition, des entreprises qui fabriquent des livres, qui se chargent de recruter des professeurs pour rédiger ces livres. En général, les professeurs reçoivent les manuels de chaque maison d’édition et choisissent celui qu’ils préfèrent pour enseigner.
Au collège, les élèves ont leur mot à dire sur ce qu’ils apprennent puisqu’ils peuvent souvent choisir les langues étrangères qui leur sont enseignées, par exemple.


