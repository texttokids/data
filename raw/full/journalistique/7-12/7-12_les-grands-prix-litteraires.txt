N°30 - 3 au 9 novembre 2017
les grands prix littéraires
Chaque année, environ 70 000 nouveaux livres sont vendus dans les librairies. Parmi eux, certains obtiennent des récompenses, qu’on appelle des prix littéraires. Ça veut dire qu’on trouve ces livres meilleurs que les autres. Les prix les plus connus sont distribués début novembre. Comment les gagnants sont-ils choisis ? Qu’est-ce que ça change d’avoir un prix ? Et comment fait-on un livre ? Suis-moi pour en savoir plus !


Alessandro, 17 ans, juré du Goncourt des lycéens

Chaque année, des écrivains choisissent le roman qu’ils préfèrent et lui donnent le prix littéraire le plus connu de France : le Goncourt. Au même moment, près de 2 000 adolescents choisissent leur propre gagnant : celui du prix Goncourt des lycéens.
C’était le cas d’Alessandro l’an dernier, quand il était en classe de première. Ses camarades et lui ont reçu la liste des 14 romans sélectionnés pour le prix Goncourt. Chaque élève était libre de lire ceux qu’il voulait, s’il le voulait. Alessandro en a lu quatre ou cinq. Régulièrement, ses professeurs organisaient avec sa classe des moments de discussion au sujet des livres.
Alessandro entendait toujours ses copains dire beaucoup de bien du roman Petit pays, de Gaël Faye. Alors ça lui a donné envie de le lire. «Je suis un des seuls à ne pas vraiment avoir aimé, reconnaît le lycéen. On m’a tellement vendu du rêve qu’au final j’ai été un peu déçu.» Malgré tout, c’est ce livre qui a obtenu le prix Goncourt des lycéens l’an dernier.
Après avoir lu les livres, Alessandro et ses camarades de classe ont rencontré les auteurs. «On leur a posé des questions, on a échangé avec eux. On a pu voir un peu leur personnalité, comment leur était venue l’idée de leur livre», se souvient Alessandro. Puis ils ont voté pour leurs trois romans préférés. Tous les lycéens de France qui étaient jurés ont fait pareil. Le gagnant a été sélectionné après des discussions entre des représentants des élèves.
Pour ce prix, Alessandro lisait à peu près une heure tous les soirs, après avoir fait ses devoirs. «J’ai appris à aimer à lire, raconte l’adolescent. Les livres ne nous étaient pas imposés, on pouvait choisir le thème. Si on n’aimait pas, on pouvait le reposer.» C’était différent de l’école et ça, ça lui a bien plu !
Neschers, la commune du Puy-de-Dôme où habite Alessandro


À quoi ressemble une remise de prix littéraire ?

En France, il existe environ 2 000 prix littéraires. Mais on parle surtout d’une petite dizaine d’entre eux, les «grands prix d’automne». Quand un livre obtient l’un de ces prix, il se vend ensuite souvent très bien en librairie. Pour les lecteurs, c’est une preuve de sa qualité.
La plupart de ces grands prix sont remis cette semaine. Il s’agit du prix Renaudot, Femina, Médicis, du prix Interallié… et surtout, du plus prestigieux de tous : le prix Goncourt.
Les lauréats des prix Goncourt et Renaudot sont annoncés dans un restaurant parisien appelé Drouant. Des dizaines et des dizaines de journalistes et photographes sont présents, et chacun veut être au premier rang pour bien entendre le nom du gagnant, pouvoir le prendre correctement en photo ou le filmer. C’est la folie ! «Je me demande comment le plancher de la salle à manger tient le coup», s’amuse l’écrivaine Paule Constant, membre de l’Académie Goncourt, qui choisit le vainqueur du prix Goncourt.
Il y a tellement de monde qu’il fait très chaud et les gens se poussent et se cognent. Une fois, un membre de l’Académie Goncourt «s’est mis un casque de moto sur la tête pour se protéger des coups», raconte Paule Constant.
Après avoir eu son prix, posé pour les photographes et fait un petit discours, le lauréat du prix Goncourt va manger. Accompagné des jurés et de son éditeur, il a droit à un repas de fête : coquilles Saint-Jacques, caviar, cuisses de grenouille, homard… «Ce sont des menus incroyables. Après ça, on ne peut plus manger pendant plusieurs jours !», s’esclaffe Paule Constant.


Comment choisit-on les gagnants ?

Les lauréats des grands prix littéraires d’automne sont le plus souvent choisis par des écrivains et des journalistes. Prenons l’exemple du Goncourt, dont le jury est composé d’auteurs. Ils reçoivent des dizaines et des dizaines de romans. Ils ne les achètent pas, ce sont les maisons d’édition qui les leur envoient.
«J’ai reçu à peu près 300 livres cette année. J’ai dû en lire 86-90, explique Paule Constant. Je prends des notes sur les choses qui m’intéressent. Je suis très sensible au style.» Les jurés ne sont pas payés pour faire ça.
Les jurés font en sorte que le livre choisi soit «grand public», c’est-à-dire que beaucoup de lecteurs arrivent à le lire et le trouvent bien. Comme ça, ils seront nombreux à l’acheter.
Chaque membre du jury lit les livres qu’il veut. «J’attire l’attention d’autres membres sur ceux qui me plaisent, qui me semblent intéressants, qui m’ont ennuyée ou choquée. On échange des mails», précise l’écrivaine. Puis ils se réunissent pour choisir leurs préférés et continuent à beaucoup en parler entre eux. Ils votent plusieurs fois, jusqu’à ce qu’il n’y ait qu’un grand gagnant, annoncé début novembre.
Beaucoup de gens disent que les jurés des grands prix littéraires ne sont pas toujours libres de choisir qui ils préfèrent vraiment. Qu’ils votent pour leurs copains ou pour un roman fait par la maison d’édition qui publie leurs livres, par exemple. «Globalement, c’est toujours les grands éditeurs qui remportent les prix : Gallimard, Grasset, Albin Michel», même si les choses changent un peu, remarque Sylvie Ducas, qui enseigne et fait de la recherche en littérature française à l’université Paris-Nanterre.
Et puis les jurés des différents prix discutent entre eux afin de ne pas récompenser la même personne. «Ils se disent : "Il vaut mieux qu’on se mette d’accord." Si deux prix sont donnés au même auteur, ça en annule un», explique Sylvie Ducas. En effet, si un auteur obtient deux récompenses, on parlera surtout de la plus connue et on oubliera un peu l’autre.


Qu’est-ce que ça change d’avoir un grand prix ?

Obtenir un des grands prix littéraires d’automne permet de vendre plus de livres qu’en temps normal. D’habitude, on considère qu’un roman a du succès s’il s’en vend autour de 6 000 exemplaires. Un livre qui obtient le prix Femina, par exemple, se vend en moyenne à 83 000 exemplaires. Et un prix Goncourt tourne autour de 400 000 exemplaires. C’est énorme !
«Un prix, ça facilite une carrière», résume Sylvie Ducas, qui enseigne et fait de la recherche en littérature française à l’université Paris-Nanterre. Tout à coup, tout le monde vous connaît ! C’est une bonne chose si on veut écrire d’autres livres et qu’ils aient du succès.
Quand on gagne le prix Goncourt, on remporte un chèque de… 10 euros seulement. Mais ce n’est pas très grave parce que, grâce aux ventes, on gagne bien plus d’argent après. Et en plus des ventes en France, les livres qui gagnent sont traduits dans au moins 25 langues pour être vendus à l’étranger. «Le Goncourt peut permettre à un écrivain de ne pas travailler pendant plusieurs années», affirme Sylvie Ducas.
Ventes de livres : qui gagne quoi ?Beaucoup de personnes travaillent pour nous permettre d’avoir un livre dans les mains. L’auteur écrit un texte, l’éditeur décide que ce texte peut devenir un livre, puis le fabricant l’imprime. Ensuite, le diffuseur fait connaître le livre aux libraires pour qu’ils aient envie de le vendre. Puis le distributeur l’envoie dans les magasins et, enfin, le libraire le vend.Quand un livre est vendu, chacune de ces personnes gagne un peu d’argent. Et ce n’est pas l’auteur qui en reçoit le plus, loin de là ! Pour un livre vendu 10 euros, voici ce que gagne chacun :
Mais les gens qui gagnent beaucoup d’argent avec des livres ne sont qu’«une poignée, signale Sylvie Ducas. Il y a des écrivains qui ne vendent même pas 300 exemplaires, ils sont au RSA ou ne gagnent rien.»


Comment fait-on un livre ?

Il existe des montagnes de livres, et ils ne sont pas tous faits de la même façon. Alors prenons un exemple, celui d’une série d’ouvrages qui a du succès auprès des enfants : Chien Pourri. C’est l’histoire d’un chien errant qui n’a pas toujours la vie facile, racontée avec beaucoup d’humour et de jeux de mots.
Colas Gutman est l’auteur des textes. L’idée de Chien Pourri lui est notamment venue des livres qu’il avait à la maison et de choses qu’il a vues dans la rue. «J’écris les livres que j’aurais aimé lire enfant. Pour les 7-10 ans, on ne proposait pas vraiment ce genre d’humour en roman», précise Colas Gutman.
Il met entre trois et six mois pour écrire une histoire, qu’il tape à l’ordinateur.
Les histoires de Chien Pourri sont illustrées. Mais contrairement à ce qu’on pourrait croire, l’auteur et le dessinateur ne travaillent pas ensemble. C’est leur éditrice, Hélène Millot, qui fait le lien entre eux et organise toute la création du livre. Une fois qu’elle a reçu le texte, qu’elle corrige si besoin, elle crée la maquette, c’est-à-dire qu’elle choisit les endroits où il y aura des dessins et la façon de placer le texte.
Ensuite, elle envoie tout ça à l’illustrateur, qui met environ trois mois à faire tous les dessins.
Marc Boutavant, l’illustrateur, a cherché à «créer des personnages attachants». «J’aurais pu en rajouter une couche dans le malheur, mais ce n’est pas ce qu’il faut faire. J’ai voulu adoucir ça», note le dessinateur.
Il ne dessine rien sur papier. Il utilise une tablette graphique, une sorte d’écran sur lequel on dessine et qui est relié à un ordinateur. Quand il a fini, il envoie son travail à l’éditrice, qui le montre à l’auteur.
Quand tout est fini, l’éditrice envoie les pages par ordinateur aux imprimeries, situées dans plusieurs villes de France. Une fois les livres imprimés, ils sont mis dans des cartons, puis des camions les emmènent dans un lieu où ils sont gardés. Les libraires disent combien d’exemplaires de Chien Pourri ils veulent et la maison d’édition, qui s’appelle L’Ecole des loisirs, les leur envoie. Et voilà, on peut acheter Chien Pourri !


