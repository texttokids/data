N°33 - 24 au 30 novembre 2017 Jean-Luc Mélenchon
Nour, 13 ans, a assisté à un discours de Jean-Luc Mélenchon

Nour habite à Marseille. Pendant la campagne pour les élections législatives, elle voyait de nombreuses affiches dans les rues de sa ville avec le visage de Jean-Luc Mélenchon. C’est normal : c’est là que cet homme politique se présentait pour être élu député, donc il y faisait souvent des discours.
Il n’habite pas à Marseille mais il a choisi de se présenter là parce qu’il avait de bonnes chances de gagner, vu qu’il a obtenu un bon score à l’élection présidentielle dans cette ville. D’ailleurs, il a gagné.
Une fois, l’adolescente est allée l’écouter avec son père. «Ça fait bizarre de le voir en vrai, on n’y croit pas. On a l’impression que c’est la télé mais en fait non, se souvient-elle. J’ai bien aimé comment il a parlé, mais je n’ai pas tout compris.»
Nour sait que Jean-Luc Mélenchon est de gauche. Pour résumer, la gauche défend avant tout l’égalité entre les gens et la droite trouve que le plus important, c’est la liberté.
Nour avait jeté un œil au programme de Jean-Luc Mélenchon parce que sa mère le lisait dans le train. Des propositions l’ont marquée, même si elle ne sait pas bien ce que ça veut dire. Par exemple, elle se souvient qu’il voudrait que le cannabis, qui est une drogue, soit autorisé en France. Ou encore que le salaire minimum donné aux gens qui travaillent augmente.
«Je n’ai pas tout compris quand je l’ai vu, mais il emploie des bons mots, pas forcément super compliqués, salue la Marseillaise. C’est un très bon orateur, il arrive à captiver l’attention des gens, en tout cas la mienne. Par rapport aux autres candidats, il parle bien. Sa voix est rassurante, c’est comme s’il ne faisait pas peur, mais en étant quand même dur et strict.»
En revanche, quand elle le voit à la télévision, Nour «le trouve tout le temps énervé. Quand il parle des journalistes, je trouve qu’il abuse un peu». Jean-Luc Mélenchon critique en effet souvent les médias, dit qu’ils font mal leur travail, qu’ils sont tout le temps contre lui. «Il devrait un peu sourire», conclut l’adolescente.
Marseille, la ville où habite Nour
