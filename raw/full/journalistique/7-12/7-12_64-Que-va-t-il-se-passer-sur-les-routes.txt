N°64 - 29 juin au 5 juillet 2018 la sécurité routière
Que va-t-il se passer sur les routes ?

A partir du dimanche 1er juillet, les voitures devront rouler moins vite sur certaines routes de France. La vitesse maximum sera de 80 kilomètres par heure (km/h) au lieu 90 km/h sur ces routes situées, pour la plupart, en dehors des villes, à la campagne.
Cette nouvelle règle s’applique plus précisément sur les routes à double sens, sur lesquelles deux voitures peuvent se croiser. Celles dont les voies ne sont pas séparées par un muret ou une barrière mais seulement par une ligne blanche.
C’est le gouvernement qui a pris cette décision. Grâce à ça, il espère réduire le nombre de morts. «Ça va permettre de sauver des vies car la vitesse provoque des accidents et peut rendre ces accidents plus graves», se réjouit Emmanuel Renard, de l’association Prévention routière.
D’autres pensent au contraire que ça ne changera rien. C’est par exemple le cas des associations d’automobilistes ou de motards, qui manifestent depuis plusieurs mois. Certains ruraux trouvent cette décision injuste parce qu’ils doivent utiliser ces routes tous les jours, pour aller au travail ou à l’école. Et ils n’ont pas envie de rouler moins vite, sinon leurs trajets dureront plus longtemps.
«Les personnes qui vivent à la campagne n’ont que leur voiture pour se déplacer car il y a très peu de transports en commun sur ces territoires», explique Christophe Jerretie, député de la Corrèze, un département rural.
Pour lui, les accidents ont aussi lieu parce que certaines routes sont trop abîmées. Pour réduire le nombre d’accidents, il pense qu’il faudrait surtout que les routes soient mieux entretenues.
