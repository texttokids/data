N°19 - mars 2017 L'élection présidentielle 
Qui se présente à l'élection ?

Deux candidats à l’élection présidentielle font beaucoup parler d’eux en ce moment : François Fillon, du parti Les Républicains, et Marine Le Pen, du Front national. La police mène des enquêtes parce qu’ils sont soupçonnés d’être impliqués dans des affaires d’emplois fictifs. Un emploi fictif est un travail pour lequel on gagne de l’argent alors qu’en fait on ne travaille pas. C’est un faux travail.
Les deux candidats assurent que ces accusations sont fausses et qu’on veut juste les empêcher de devenir président ou présidente de la République. La justice devra dire si c’est vrai ou pas.
Si ces deux affaires font beaucoup de bruit, c’est justement parce que l’élection présidentielle arrive bientôt et que c’est un moment très important en France. Le 7 mai, on connaîtra le nom du nouveau président. Aujourd’hui, c’est François Hollande.
Plusieurs personnes veulent lui succéder : il devrait y avoir une dizaine de candidats à l’élection. On ne connaîtra le nombre officiel qu’aux alentours du 20 mars. En 2002, 16 personnes s’étaient présentées à l’élection présidentielle, c’est un record.
Cette année, on parle surtout de cinq candidats. Par ordre alphabétique, il s’agit de François Fillon, Benoît Hamon, Marine Le Pen, Emmanuel Macron et Jean-Luc Mélenchon.
Francois Fillon
François Fillon est un candidat de droite. Il appartient au parti Les Républicains.
Sa vie personnelle
Il est né dans les Pays-de-la-Loire, il a 62 ans et cinq enfants.
Son parcours
François Fillon a été plusieurs fois ministre. De 2007 à 2012, il a été le Premier ministre de l’ancien président Nicolas Sarkozy. Il a commencé sa carrière politique à 22 ans.
Ses idées
On considère qu’il est plus à droite que d’autres personnes de son parti. Il aimerait notamment qu’il y ait moins de fonctionnaires, c’est-à-dire de travailleurs payés avec de l’argent public (l’argent de l’Etat ou de la mairie, par exemple) et que les gens travaillent plus et plus longtemps.
Benoît Hamon
Benoît Hamon est un candidat de gauche. Il appartient au Parti socialiste.
Sa vie personnelle
Il est né en Bretagne, il a 49 ans et deux enfants.
Son parcours
Il a notamment été ministre de l’Education nationale, en 2014. Benoît Hamon fait de la politique depuis qu’il a 19 ans.
Ses idées
On considère qu’il est plus à gauche que d’autres personnes de son parti. Il aimerait notamment créer un revenu universel : tout le monde recevrait 750 euros chaque mois de la part de l’Etat, que l’on travaille ou pas, que l’on gagne déjà beaucoup d’argent ou pas.
Marine Le Pen
Marine Le Pen est une candidate d’extrême droite. Elle appartient au Front national.
Sa vie personnelle
Elle est née en région parisienne, elle a 48 ans et trois enfants.
Son parcours
Avant de devenir responsable politique, Marine Le Pen a été avocate. Elle est devenue présidente du Front national il y a six ans, en 2011. Le parti avait été créé par son père, Jean-Marie Le Pen. Elle est députée européenne, ça veut dire qu’elle est élue au Parlement européen.
Ses idées
Marine Le Pen n’aime pas qu’on dise que le Front national est d’extrême droite parce qu’elle veut se différencier de son père, qui a une mauvaise image auprès d’une partie de la population parce qu’il a fait des déclarations choquantes et qu’elle l’a exclu du parti. Elle souhaite notamment que les Français aient plus de droits que les étrangers qui vivent dans le pays.
Emmanuel Macron
Emmanuel Macron dit qu’il n’est ni de gauche ni de droite. Beaucoup de gens disent qu’il est entre les deux, au centre. C’est le candidat d’«En marche».
Sa vie personnelle
Il est né en Picardie et il a 39 ans.
Son parcours
Avant de faire de la politique, Emmanuel Macron a été banquier. Il a été nommé ministre de l’Economie en 2014 et a démissionné l’an dernier afin de pouvoir être candidat à la présidence de la République.
Ses idées
Il propose notamment d’adapter la durée de travail à l’âge des gens : les plus jeunes travailleraient chaque semaine plus d’heures que les plus âgés.
Jean-Luc Mélenchon
Jean-Luc Mélenchon est un candidat de gauche. Il se présente au nom de «la France insoumise».
Sa vie personnelle
Il est né au Maroc, il a 65 ans et un enfant.
Son parcours
Jean-Luc Mélenchon a notamment été professeur de français et journaliste. Plus tard, il est devenu ministre de l’Enseignement professionnel. Membre du Parti socialiste pendant plus de trente ans, il l’a quitté en 2008 pour créer le Parti de gauche.
Ses idées
On considère qu’il est davantage de gauche que le candidat socialiste, Benoît Hamon. Il voudrait notamment supprimer la fonction de président de la République. Il voudrait que les lois ne soient pas votées que par les députés élus à l’Assemblée nationale, mais aussi par des citoyens.
