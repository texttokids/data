N°13 - septembre 2016 Les coulisses de l'école
Florent fait sa première rentrée comme prof

Florent, 26 ans, fait sa première rentrée en tant que professeur à l’école primaire cette année. Il a en effet réussi ses examens à l’école des enseignants, où il a étudié pendant deux ans. L’année dernière, il a donné des cours à une classe de CE2 : il enseignait deux jours et demi par semaine pour apprendre le métier. Cette fois-ci, il est vraiment professeur.
Comme beaucoup de maîtres et de maîtresses qui commencent, il est remplaçant, ça veut dire qu’il prend la place des enseignants qui sont absents, par exemple parce qu’ils sont malades. La plupart du temps, les remplaçants ont plusieurs classes dans l’année.
Florent a appris deux jours avant la rentrée qu’il travaillerait avec une classe de CE2 dans le XXe (20e) arrondissement de Paris jusqu’au mois de mars. Ça tombe bien puisqu’il avait déjà travaillé avec des CE2 l’an dernier. Pour bien se préparer, il a discuté avec le professeur qu’il remplace de la façon dont il enseigne.
Florent sait qu’il travaillera beaucoup les premiers mois pour préparer ses cours. Quand il enseignait l’an dernier, il travaillait déjà jusqu’à minuit après l’école pour que tout soit prêt pour ses élèves. Ça ne le dérange pas parce qu’il aime son métier : il a le sentiment de servir à quelque chose quand il voit les élèves apprendre des choses grâce à lui.
Paris, la ville où enseigne Florent
