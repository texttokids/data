N°36 - 15 au 21 décembre 2017 le conflit Israël-Palestine
Pourquoi y a-t-il des Israéliens et des Palestiniens sur ce territoire ?

Deux peuples, les Israéliens et les Palestiniens, vivent sur ce petit territoire constitué d’Israël et des Territoires palestiniens. Ces deux peuples se disputent la même terre depuis 69 ans.
Les Israéliens ont un pays, l’Etat d’Israël, mais pas les Palestiniens. Ces derniers se répartissent pour la plupart dans trois Territoires palestiniens :
D’autres Palestiniens sont devenus Israéliens et vivent en Israël, notamment dans des villes comme Nazareth.
Pour tout comprendre, il faut remonter dans l’histoire. Israël a été créé en 1948 pour donner un pays aux juifs. Durant la Seconde Guerre mondiale (1939-1945), près de six millions de juifs ont été tués, soit environ un tiers de ceux qui habitaient en Europe. Leur extermination a été décidée par les nazis, dont le chef s’appelait Adolf Hitler.
Des survivants ont voulu se réfugier hors d’Europe. Ils rêvaient de se retrouver sur cette terre sur laquelle leurs ancêtres habitaient il y a 3 000 ans.
Après la Seconde Guerre mondiale, les pays puissants ont décidé de donner aux juifs une partie de cette terre. A l’époque, elle s’appelait la Palestine. Des juifs y vivaient déjà, tout comme un autre peuple présent en plus grand nombre : les Palestiniens.
En 1947, l’ONU a donc décidé de partager la Palestine en deux Etats : un pour les Juifs et un pour les Palestiniens. Ces derniers n’étaient pas du tout d’accord : alors qu’ils occupaient 90% du territoire, l’ONU leur en a donné moins de la moitié. L’Etat d’Israël a été créé le 14 mai 1948, mais pas d’Etat palestinien.
Frontières proposées par l’ONU en 1947
