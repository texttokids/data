N°22 - juin 2017 L'Assemblée nationale
Qui sont les députés ?

Les députés sont des hommes et des femmes élus pour cinq ans par les citoyens français pour les représenter. Ils appartiennent en grande majorité à un mouvement politique.
Pour devenir député, il faut avoir la nationalité française, être âgé d’au moins 18 ans et être candidat aux élections législatives là où on le souhaite, même à un endroit où on n’habite pas. Chaque candidat se présente ainsi sur une circonscription.
Pour bien comprendre, la France est découpée en 101 départements. Chaque département est divisé, en fonction de son nombre d’habitants, en plusieurs circonscriptions. Il y a 577 circonscriptions, avec un député élu dans chacune d’elle. Il y a donc 577 députés.
Les circonscriptions de la Charente
Les députés sont élus sur un territoire, mais ils représentent tout le pays. Ils font partie d’une assemblée, l’Assemblée nationale, qui est située au Palais-Bourbon, à Paris.
Les députés ont deux grandes missions :
Mais un député ne travaille pas qu’à l’Assemblée nationale. Il va aussi dans sa circonscription pour rencontrer les habitants et les différents élus, comme les maires, qui lui font part de leurs préoccupations.
On dit souvent que les députés ne représentent pas bien les Français. Ce sont surtout des hommes blancs qui ont déjà fait de la politique avant et ont en moyenne 60 ans. Il y a donc très peu de femmes, de Noirs, d’ouvriers et de jeunes, par exemple. Pour ces législatives, certains mouvements politiques veulent que ça change. Ils proposent donc des personnes qui ont un parcours un peu différent et qui n’ont pas fait de politique avant.
