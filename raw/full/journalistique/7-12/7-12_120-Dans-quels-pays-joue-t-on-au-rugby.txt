N°120 - 20 au 26 septembre 2019 Mondial de rugby : c'est parti !
Dans quels pays joue-t-on au rugby ?

Le rugby est plus ou moins présent dans une centaine de pays. Mais de manière générale, c’est un sport beaucoup moins répandu que le football, qui compte des millions de joueurs et de joueuses en plus dans le monde.
Les principales équipes de rugby dans le monde se trouvent en Europe, comme en Angleterre ou en France, et dans l’hémisphère sud, comme en Nouvelle-Zélande ou en Afrique du Sud.
C’est en Angleterre que le rugby a été inventé il y a un peu moins de 200 ans, dans une ville dans le nord du pays qui s’appelle… Rugby. D’après la légende, c’est là qu’un étudiant, William Webb Ellis, l’a créé en prenant un jour le ballon dans ses mains pour marquer pendant un match de football. Mais cette histoire n’a jamais été prouvée.
En fait, à l'époque, chaque école privée anglaise avait ses propres règles pour le football. Les étudiants de Rugby jouaient en utilisant aussi leurs mains. Cette pratique s’est répandue, et finalement, en 1871, les règles du rugby ont été clairement définies.
Les Anglais ont fait connaître ce nouveau sport en voyageant dans le monde, et surtout dans leurs colonies, c’est-à-dire des territoires qu’ils ont longtemps occupés et exploités. C’est le cas de l’Australie ou de la Nouvelle-Zélande. Dans ce dernier, le rugby est devenu le sport star ! Son équipe a gagné trois fois la Coupe du monde.
En France, les premiers clubs de rugby ont été créés dans les années 1870 et 1880. Ensuite, ce sport s’est beaucoup développé dans le Sud-Ouest.
Les joueuses de rugbyLes femmes aussi jouent au rugby, et elles sont même de plus en plus nombreuses. Il existe une Coupe du monde féminine depuis 28 ans. La prochaine aura lieu en 2021, en Nouvelle-Zélande.
