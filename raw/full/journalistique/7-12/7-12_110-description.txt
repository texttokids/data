N°110 - 14 au 20 juin 2019 les avions
Description

Du 17 au 23 juin, une grande exposition d’avions a lieu au nord de Paris : ça s’appelle le Salon du Bourget. A cette occasion, on t’explique tout ce qu’il faut savoir sur cet engin qui nous fait rêver, mais qui est aussi très critiqué parce qu’il pollue beaucoup. Comment un avion vole-t-il ? Est-ce que c’est sûr ? A quoi ressemblera l’avion du futur ? Attache bien ta ceinture, décollage immédiat !