N°46 - 23 février au 1er mars 2018 le Salon de l'agriculture et les éleveurs
Corinne élève des poulets et des dindes

«J’aime passer toute la journée dehors, dans la nature», explique Corinne. Cette agricultrice vit dans la Drôme, dans le sud-est de la France. Dans sa ferme, elle élève des volailles (des poulets et des dindes). Elle cultive aussi des arbres fruitiers (abricotiers, poiriers) et des champs de céréales.
Corinne travaille en famille, avec son mari, leur fils et sa femme et le frère de Corinne. Ils possèdent 40 000 volailles et ça demande beaucoup de travail : «Je me lève à 6 heures et je vais voir les volailles dans leur bâtiment, pour m’assurer qu’il est à la bonne température et qu’elles ont assez d’eau, de nourriture et qu’elles sont en bonne santé.» Ensuite, elle remet de la paille dans les bâtiments où vivent les volailles. «C’est une tâche assez longue car ça se fait à la main», explique-t-elle.
L’après-midi, elle doit souvent remplir des papiers ou payer des factures par exemple. Parfois, elle aide aussi son mari à s’occuper des arbres fruitiers : soigner les arbres, ramasser les fruits lorsque c’est la saison…
Le soir, elle fait un dernier tour des volailles vers 19 heures avant de rentrer dans sa maison, située tout près. «Lorsqu’on a des animaux, il faut habiter dans la ferme. C’est plus pratique, et ça nous rassure. Mais du coup, on ne peut pas prendre beaucoup de vacances.»
Quand les volailles ont un peu plus d’un mois, elles sont emmenées à l’abattoir. La viande est ensuite mise en barquettes par d’autres personnes, pour être vendue dans les supermarchés de la région.
Corinne et son mari travaillent dans leur ferme depuis 27 ans. Mais ils ont commencé à élever des volailles il y a 5 ans. «Le métier d’agriculteur, c’est savoir s’adapter. Il faut s’adapter aux nouvelles lois, à ce que les gens ont envie de manger.»
Saint-Barthélemy-de-Vals, le village où vit et travaille Corinne
