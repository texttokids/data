N°108 - 31 mai au 6 juin 2019 la prison
Description

Près de 72 000 personnes sont en prison en France. Pourtant, il n’y a pas assez de place pour tout le monde. Alors dans certains établissements, il y a deux fois plus de prisonniers que de lits. A quoi sert la prison exactement ? A quoi ressemble la vie à l’intérieur ? Et que se passe-t-il quand on en sort ? Cette semaine, je t’explique tout sur ce monde très fermé. 