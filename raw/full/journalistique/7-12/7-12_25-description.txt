N°25 - 29 septembre au 5 octobre 2017 Ouragans : une année record
Description

Harvey, Irma, José, Maria… Depuis la fin du mois d’août, des ouragans très puissants se sont formés dans le nord de l’océan Atlantique, faisant des dizaines de victimes et des dégâts très importants. Pourquoi y a-t-il des ouragans si intenses cette année ? Comment se forment-ils ? Peuvent-ils toucher la France métropolitaine un jour ? Je t’emmène à la découverte de ce phénomène climatique impressionnant.