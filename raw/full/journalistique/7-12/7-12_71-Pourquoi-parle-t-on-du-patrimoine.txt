N°71 - 14 au 20 septembre 2018 la protection du patrimoine
Pourquoi parle-t-on du patrimoine ?

Chaque année, mi-septembre, ont lieu les Journées européennes du patrimoine. Leur but ? Permettre aux citoyens de visiter des lieux qui font partie du patrimoine, c’est-à-dire qui ont une histoire et qui doivent être protégés. Ainsi, ils peuvent se rendre compte de tout ce qu'on peut admirer dans leur pays.
Pour l’occasion, on peut visiter certains lieux gratuitement alors que d’habitude ils sont payants, et des visites guidées sont organisées afin d’en apprendre plus sur l’histoire de ces endroits. Certains lieux n’ouvrent au public que lors de ces journées. C’est par exemple le cas de l’Elysée, l’endroit où le président de la République travaille.
Cette année, on parle plus de ce sujet que d’habitude parce que des jeux ont été créés spécialement pour aider à sauver le patrimoine. Pendant quelques semaines, on peut acheter des jeux à gratter et des tickets de Loto spéciaux, pour essayer de gagner de l’argent (un homme a d'ailleurs déjà gagné 1,5 million d'euros en grattant un ticket !). Une partie du prix qu’on paye est mise de côté pour ensuite faire des travaux dans des lieux «en péril», c’est-à-dire qui risquent d’être détruits ou de disparaître si on ne les sauve pas.
En tout, 269 lieux ont été choisis pour recevoir l’argent. Il s’agit de forts, de ponts, de châteaux, d’églises ou encore de maisons. Dix-huit ont été jugés prioritaires, ce qui veut dire qu’ils ont besoin d’être sauvés avant les autres ; il y en a un par région.
A toi de jouer !
Voici les 18 sites qui devront être sauvés en priorité. Sais-tu situer les cinq dont les noms sont écrits dans l'encadré ? Réponse à la fin du coin lecture.
Ces jeux vendus pour l’occasion devraient permettre de rapporter entre 15 et 20 millions d’euros pour le patrimoine. Il faudrait beaucoup plus d’argent pour tout rénover, ça sert plutôt à donner un coup de pouce et à montrer aux gens qu’il faut prendre soin des bâtiments.
