N°35 - 8 au 14 décembre 2017 les jeux de société
Pourquoi aime-t-on tant les jeux de société ?

Les jeux de société, «ça permet d’être ensemble, d’être en même temps au même endroit avec les mêmes règles du jeu», constate Brigitte Jobbé-Duval, une ludologue, autrement dit une spécialiste des jeux. Comme respirer ou bouger, «c’est naturel pour tout le monde de jouer», précise-t-elle.
Sans s’en rendre compte, on apprend beaucoup avec les jeux de société. D’abord parce qu’on comprend qu’il faut respecter des règles et on améliore ses relations avec les autres. Et puis on développe son imagination et sa capacité à réfléchir à une stratégie.
«On est beaucoup plus libres que quand on est dans la réalité. On peut avoir d’autres comportements», remarque Brigitte Jobbé-Duval. Par exemple, quand on s’amuse à prendre l’argent des autres au Monopoly, on sait que ce n’est pas vrai. On joue un rôle et ce personnage disparaît une fois que la partie est terminée.
Les jeux vidéo aussi permettent de se réunir avec des amis et de s’amuser, mais c’est un peu différent. «Avec les jeux vidéo, vous vous plongez complètement dans un univers graphique très prenant. Dans les jeux de société, c’est le fait d’être avec les autres qui rend le jeu sympa», affirme la ludologue.
Bien sûr, tout le monde n’aime pas les jeux de société. Certains ont des difficultés à accepter un règlement, n’aiment pas perdre ou ont peur de paraître ridicules aux yeux des autres. «Ils n’arrivent pas à décoller de la réalité», constate Brigitte Jobbé-Duval.
Quoi qu’il en soit, pour faire travailler son imagination et sa créativité, il n’y a rien de mieux que d’inventer ses propres jeux.
