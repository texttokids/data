N°4 - décembre 2015 Les régionales 
Description

On en parle peu mais ce sont des élections très importantes. Les dimanches 6 et 13 décembre, les Français sont appelés à voter aux élections régionales. Et cette année, il y a de grands changements puisque ce ne sont plus les mêmes régions qu'avant. Alors à quoi ressemble ta région aujourd'hui ? Et au fait, à quoi ça sert exactement ?