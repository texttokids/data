N°33 - 24 au 30 novembre 2017 Jean-Luc Mélenchon
Pourquoi est-il apprécié ?

Pendant la campagne présidentielle, Jean-Luc Mélenchon a plu à beaucoup de gens, et notamment aux jeunes. Un électeur sur trois ayant entre 18 et 25 ans a voté pour lui, c’est beaucoup. De nombreux jeunes en avaient assez de la façon dont le pays était dirigé, ils voulaient un grand changement et pensaient que Jean-Luc Mélenchon pouvait le leur apporter.
«Il a un talent particulier : de l’avis de tous c’est un très bon orateur, il fait des discours intéressants, construits, avec de belles phrases, de belles formules», constate Thierry Vedel, spécialiste de la communication politique au Cevipof. «Les gens qui le connaissent disent que c’est quelqu’un de très cultivé, un grand connaisseur de l’histoire de France», poursuit le politologue Bruno Cautrès.
Ses idées séduisent beaucoup de monde. Jean-Luc Mélenchon pense que le président de la République a trop de pouvoir et que le peuple doit prendre plus de décisions pour le pays. Il critique beaucoup les élites, comme les responsables politiques et les journalistes.
Jean-Luc Mélenchon voudrait qu’il y ait davantage d’égalité entre les gens. Il défend ceux qui ont le moins d’argent et critique ceux qui s’enrichissent en rendant les autres plus pauvres.
Durant la campagne pour l’élection présidentielle, il a beaucoup utilisé YouTube pour communiquer. C’est notamment grâce à ça qu’il a séduit des jeunes. Il enregistrait des messages vidéo pour parler directement aux électeurs, sans être interrompu ou interrogé par des journalistes.
Aujourd’hui, il a 370 000 abonnés. Ça peut paraître ridicule par rapport aux 10 millions de personnes qui suivent l’humoriste Norman, mais pour un responsable politique français, c’est énorme.
