N°56 - 4 au 10 mai 2018
le premier anniversaire du président à l'Elysée
Il y a un an, le 7 mai 2017, Emmanuel Macron devenait le nouveau président de la France, ou chef de l'Etat. Il avait 39 ans et devenait le plus jeune président de la République française. Que s’est-il passé en un an ? Qui l'entoure dans son travail ? Que pensent les Français de lui ? Dans ce nouveau numéro, je t’emmène à l’Elysée, où travaille le Président.


Louise, 20 ans, a voté pour la première fois l’an dernier

Louise a 20 ans et a voté pour la première fois l’an dernier lors de l’élection présidentielle. C’était un grand moment pour cette étudiante à la faculté de Sciences politiques de Lille : «J’étudiais la politique depuis deux ans, sans pouvoir voter. Ça représentait donc quelque chose pour moi qui suis ça depuis que je suis toute petite.» D’autant plus que c’était pour élire le Président, «qui a un rôle super important en France», explique-t-elle.
Lorsqu’elle a mis son bulletin dans l’urne, elle était sûre de son choix. Mais elle a beaucoup hésité avant et a tout fait pour mieux comprendre les programmes des candidats à la présidentielle. Elle a par exemple lu leurs livres et est allée à quatre meetings de candidats très différents : «J’avais besoin d’écouter tout le monde pour tout comprendre et être sûre de mon choix.»
Louise a beaucoup aimé débattre avec ses amis pendant l’élection et en garde un très bon souvenir : «C’était super intéressant, on avait tous des avis différents. On regardait les débats entre les candidats à la télé et c’était très animé.» Louise aime les débats d’idées parce qu’elle pense que c’est «la base de la démocratie». Alors elle échange avec tout le monde, même ceux qui n’ont pas les mêmes opinions politiques qu’elle : «Je ne veux pas rester dans ma bulle avec mes opinions. Ecouter les autres permet de comprendre pourquoi les gens votent pour telle ou telle personne.»
Louise ne s’est jamais engagée en politique mais elle le fera peut-être un jour «si je trouve un candidat qui me va.»
Lille, la ville où étudie Louise


Que s’est-il passé il y a un an ?

Lundi prochain, le 7 mai, cela fera tout juste un an qu’Emmanuel Macron a été élu président de la République en France. Cet anniversaire est l’occasion de faire un premier bilan des décisions prises par le nouveau chef de l’Etat au cours de l’année.
Emmanuel Macron a remporté l’élection présidentielle le dimanche 7 mai 2017. Lors du second tour, cet homme politique qui dit n’être ni de gauche, ni de droite, a battu Marine Le Pen, la candidate du Front national, un parti d’extrême droite.
Mais beaucoup d’électeurs se sont abstenus : ça veut dire qu’ils ne sont pas allés voter du tout, par exemple parce que ça ne les intéressait pas, ou alors parce qu’ils ne souhaitaient vraiment pas apporter leur voix à l’un ou à l’autre.
C’était la première fois qu’Emmanuel Macron se présentait à une élection. Avant ça, il a travaillé dans une grande banque. Puis il a été ministre de l’Economie de 2014 à 2016, quand François Hollande était Président. Il a finalement démissionné pour créer son propre mouvement politique qui s’appelle La République en marche (LREM).
Son élection a surpris. D’abord, il n’avait que 39 ans quand il a été élu : c'est très jeune pour un Président. Ensuite, il ne représentait aucun des deux partis d’où venaient jusque-là les Présidents : soit Les Républicains (le principal parti de droite), soit le Parti socialiste (celui de gauche). Leurs candidats avaient été éliminés au premier tour.
Après ça, en juin, La République en marche a remporté les élections législatives. Ça veut dire que la majorité des nouveaux députés élus à l’Assemblée nationale étaient du même parti que le Président. Cette victoire a été très importante pour Emmanuel Macron puisque ce sont les députés qui votent les lois. S’ils soutiennent le président, celui-ci peut plus facilement faire appliquer ses décisions.


Que faut-il retenir de sa première année à l’Elysée ?

Voici cinq décisions ou moments importants de la première année d’Emmanuel Macron à l’Elysée.
Le soir de sa victoire, le 7 mai 2017, Emmanuel Macron a fait un discours devant la pyramide du musée du Louvre, à Paris.
Une cérémonie officielle a été mise en scène, un peu comme au théâtre, au Louvre, le plus grand musée au monde, situé à Paris. Emmanuel Macron n’a pas choisi cet endroit par hasard : ça montre notamment qu’il est différent des autres présidents, puisque c’est le premier à avoir choisi de faire un discours à cet endroit-là. Ce soir-là, il a aussi montré qu’il défendait l’Europe en choisissant par exemple de faire jouer l’Ode à la joie, l’hymne européen.
Emmanuel Macron a lancé beaucoup de réformes à l’école et à l’université. Concernant le primaire par exemple, les classes sont, depuis la rentrée, composées de 12 élèves maximum en CP dans les écoles des quartiers les plus défavorisés (où il y a moins d’argent et où les élèves ont moins de chances de réussir à l’école). Problème : les écoles qui n’ont pas assez de place dans leurs bâtiments ont dû diviser une salle de classe en deux, avec deux instituteurs.
Emmanuel Macron a assuré qu’il mettrait en place les changements qu’il a annoncés, même si des gens ne sont pas contents et le font savoir. C’est par exemple le cas des cheminots, des salariés de la SNCF, qui sont en grève depuis le mois d’avril ou pour des étudiants qui bloquent l’entrée de leur université pour protester contre des décisions prises par le gouvernement.
Dimanche 15 avril, Emmanuel Macron a fait une longue interview à la télévision. Il a notamment parlé de la première attaque menée par la France, les Etats-Unis et le Royaume-Uni, pour détruire des armes très dangereuses utilisées par le régime du dictateur Bachar al-Assad en Syrie, un pays où une guerre a lieu depuis sept ans.
Fin avril, Emmanuel Macron a été le premier dirigeant étranger à se rendre aux Etats-Unis pour faire une visite d’Etat (la plus importante des visites officielles) depuis que le président américain Donald Trump a été élu en 2016.
Donald Trump est très critiqué un peu partout dans le monde. Emmanuel Macron ne partage pas toujours ses opinions. Mais il est l’un des seuls dirigeants européens à avoir de bonnes relations avec lui.


Avec qui Emmanuel Macron gouverne-t-il ?

Pour gouverner, Emmanuel Macron est aidé par un Premier ministre, qui est le chef du gouvernement. Ça veut dire qu’il est le chef des ministres, qu’il a choisis avec le Président.
Certains sont d’anciens membres du parti de droite Les Républicains, d’autres viennent du Parti socialiste (à gauche), mais il y a aussi des personnes qui n’ont jamais travaillé dans la politique. Voici les portraits de quatre membres du gouvernement dont on a beaucoup parlé cette année.
C'est le Premier ministre. Son rôle est notamment d’aider les ministres à défendre les nouvelles lois qui sont critiquées, en parlant aux journalistes ou en rencontrant les députés. Il l’a fait plusieurs fois cette année, par exemple pour une loi importante qui a modifié les règles définissant ce qu’on peut faire ou non dans une entreprise. (Photo Laurent Troude pour Libération)
C'est le ministre de l’Education nationale. Il est chargé d’appliquer les décisions du Président concernant les écoles, les collèges ou les lycées.
Cette année, il a annoncé de nombreux changements qui toucheront les élèves, de l’école jusqu’à l’université. Certains sont déjà mis en place, comme dans les classes de CP. D’autres le seront dans les années à venir, comme la nouvelle formule du baccalauréat. (Photo Martin Colombet. Hans Lucas pour Libération)
Gérard Collomb est le ministre de l’Intérieur. Il est notamment chargé de veiller à la sécurité des Français.
Il a préparé une loi sur l’asile et l’immigration qui vient d’être votée et qui définit comment les migrants sont accueillis dans le pays. Cette loi est très critiquée par des députés de gauche mais aussi par quelques députés de La République en marche, qui trouvent cette loi trop dure envers les migrants. (Photo Thomas Samson. AFP)
Marlène Schiappa est secrétaire d’Etat chargée de l’Egalité entre les femmes et les hommes. Avant d’occuper ce poste, elle avait peu d’expérience en politique. Elle n’avait jamais été élue par des citoyens. (Photo Joël Saget. AFP)
Elle veut faire voter une loi pour punir plus sévèrement les violences sexuelles et sexistes. On a beaucoup entendu parler de cette loi car depuis plusieurs mois de plus en plus de femmes en France et dans le monde racontent, notamment sur les réseaux sociaux, les violences qu’elles ont subies de la part d’hommes.


Que pensent les Français du président ?

Un an après son élection, les Français ne sont pas tous du même avis au sujet d'Emmanuel Macron.
«Ceux qui sont contents trouvent que le président tient ses promesses : il fait ce qu’il a dit qu’il ferait pendant sa campagne. Pour eux, il transforme le pays en faisant beaucoup de choses, explique le politologue Brice Teinturier. Ils pensent que ces changements auront des résultats positifs dans les mois ou les années à venir. C’est important pour eux parce qu’ils avaient l’impression qu’avant, avec les autres présidents, le pays était bloqué, que rien ne changeait.»
Ces personnes sont aussi fières de leur président. «Elles sont contentes de voir qu’Emmanuel Macron est apprécié à l’étranger. Pour elles, ils donnent une bonne image de la France et des Français parce qu’il est jeune, qu’il s’exprime bien et que c’est un bon chef. Elles ne ressentaient pas ça pour les deux présidents précédents, François Hollande et Nicolas Sarkozy», ajoute Brice Teinturier.
A l’inverse, d’autres Français ne sont pas du tout satisfaits. D’abord, ils pensent que les mesures prises par le président sont injustes car elles n’aident pas les plus pauvres. «Il est vu comme le président des riches, celui qui ne s’occupe que des personnes qui ont le plus d’argent dans le pays», commente le politologue Bruno Cautrès.
Une partie de la population se sent oubliée par le président, par exemple ceux qui gagnent peu d’argent ou qui vivent à la campagne. «Entre son élection et aujourd’hui, les Français ont moins le sentiment qu’Emmanuel Macron est proche d’eux et qu’il comprend leurs problèmes», assure Bruno Cautrès.
Pour Bruno Cautrès, Emmanuel Macron n’est finalement pas différent des anciens présidents : «Il est jeune et entouré de gens qu’on ne connaissait pas avant, donc il donne l’impression d’être différent mais sa façon de gouverner est assez identique. Comme les anciens présidents, c’est lui qui est le chef. Même s’il tient compte de l’avis des ministres, c’est toujours lui qui prend les grandes décisions à la fin.»


