N°6 - février 2016 Les secrets du 29 février 
Que va-t-on faire de cette journée ?

On l’aura compris, 2016 compte un jour supplémentaire par rapport à 2015. Et il s’en passe des choses en une journée !
Puisque ça tombera un lundi, certains passeront le 29 février à l’école. Tous ne seront peut-être pas ravis, mais pas de quoi s’arracher les cheveux pour autant : chaque personne perd déjà 60 cheveux par jour, en moyenne. Heureusement, ils repoussent !
Et puis pour les gourmands, ça fera un goûter en plus. Rien qu’en France, ce jour-là, on avalera 32 millions de baguettes de pain et plus de 200 000 kilos de Nutella.
Pas étonnant que les toilettes fonctionnent autant : les Français utiliseront, comme tous les jours, près de 274 millions de litres d’eau en tirant la chasse.
Les enfants qui habitent dans la zone C (à Paris, Montpellier ou Toulouse, par exemple) seront, eux, en vacances le 29 février. L’occasion de faire du tourisme, par exemple en allant visiter la tour Eiffel à Paris, comme 19 178 personnes le font en moyenne chaque jour...
… et d’envoyer une photo souvenir par texto à leur famille. Elle voyagera avec les 567 millions de messages échangés par téléphone chaque jour en France.
Plusieurs de ces messages annonceront un heureux événement, car 2 243 bébés naissent quotidiennement dans notre pays.
En grandissant, ils deviendront peut-être serveurs, comptables ou musiciens, mais peut-être y aura-t-il dans le lot un astronaute ? Aujourd’hui, ils sont six dans la Station spatiale internationale, et ils font 16 fois le tour de la Terre chaque jour.
Entre deux expériences scientifiques, les astronautes peuvent surfer sur Internet et envoyer des mails. Ils sont les seuls à le faire depuis l’espace, mais leurs messages se mélangeront aux plus de 200 milliards de mails envoyés en un jour dans le monde.
