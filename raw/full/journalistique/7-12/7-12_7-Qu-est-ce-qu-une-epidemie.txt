N°7 - mars 2016 Zika et les épidémies
Qu'est-ce qu'une épidémie ?

En Amérique latine, il y a en ce moment une épidémie de Zika. On parle d’épidémie quand plusieurs personnes tombent malades en même temps au même endroit. Ça peut durer une journée ou des années. Si plusieurs enfants ont une intoxication alimentaire à la cantine, par exemple, on peut parler d’épidémie. Mais ça ne dure pas longtemps.
D’autres épidémies sont plus inquiétantes parce qu’elles s’étendent sur plusieurs pays, comme c’est le cas pour Zika. Récemment, on a aussi beaucoup parlé d’Ebola. Ce virus (qui provoque notamment une très forte fièvre) a beaucoup touché l’Afrique de l’Ouest et a fait plus de 11 000 morts en deux ans.
Ce sont les microbes, des petits organismes qu’on ne peut pas voir à l’œil nu, qui sont responsables des maladies, donc des épidémies. Il y en a des milliards dans la nature. Ils sont présents dans l’eau, l’air, la terre, dans notre corps et sur notre peau.
Les bactéries
Ce sont des organismes minuscules qui se multiplient tout seuls et très vite. La plupart ne font pas de mal et sont même très utiles. Le corps humain en contient des dizaines de milliards qui permettent par exemple de digérer. Certaines bactéries nettoient aussi la mer, d’autres servent à fabriquer des yaourts ou des fromages.
Mais il existe aussi des bactéries mauvaises pour notre santé. Elles profitent d’une faiblesse dans notre corps pour nous faire tomber malade. Ça peut provoquer des maladies graves comme la tuberculose ou le choléra.
Les virus
Ils sont encore plus petits que les bactéries, mais ils sont très forts ! Pour survivre, les virus rentrent dans certaines de nos cellules (on en a des milliards dans notre corps), ils se multiplient et en contaminent d’autres. Ça peut provoquer des maladies graves comme le sida.
Les virus et les bactéries provoquent aussi de nombreuses maladies beaucoup moins inquiétantes comme le rhume, la gastro-entérite ou l’otite.
De l’animal à l’homme
C’est le mode de contamination le plus courant (par des morsures ou des piqûres, par exemple). Les puces, les poux, les tiques et les moustiques sont ceux qui donnent le plus de maladies.
D’homme à homme
Par la salive (quand on tousse ou quand on s’embrasse, par exemple), par le sang ou encore par les rapports sexuels.
Dans l’eau et dans l’air
S’il y a des microbes dans l’eau qu’on boit ou dans laquelle on se baigne. Ça arrive par exemple quand des rats font pipi dans l’eau, parce que leur urine peut donner des maladies.
Quand la climatisation est mal entretenue, des microbes peuvent s’y développer et se répandre dans l’air quand on allume l’appareil.
Quand une épidémie touche plus de deux continents, on parle de pandémie. En ce moment, il existe trois grandes pandémies dans le monde.
Le sida
Qu’est-ce que c’est ?
Le sida est une maladie causée par un virus qui s’appelle le VIH et qui fragilise le système immunitaire du corps. Ça veut dire que, petit à petit, le corps devient incapable de se défendre contre des maladies dont il se débarrasse normalement sans problème, et ça peut être mortel. Des personnes ont le virus (le VIH) dans leur corps sans que la maladie (le sida) se développe.
Comment on l’attrape ?Par les relations sexuelles, par le sang (si le sang d’un malade touche la blessure d’une autre personne, par exemple) et de la mère à l’enfant au moment de l’accouchement (parce que le sang de la maman peut contaminer le bébé).Combien de personnes sont touchées ?Dans le monde, environ 35 millions de personnes vivent avec le virus VIH et plus de 1,5 million de personnes meurent du sida chaque année.Ça se soigne ?Il n’existe pas de vaccin contre le sida et on ne guérit pas de cette maladie. Mais il existe des traitements qu’il faut prendre toute la vie et qui permettent aux malades de vivre presque normalement et beaucoup plus longtemps.
Le paludisme
Qu’est-ce que c’est ?Le paludisme, qu’on appelle aussi la malaria ou le palu, est une maladie qui provoque une très forte fièvre et plusieurs symptômes notamment une grande fatigue.
Comment on l’attrape ?Elle est transmise par les moustiques femelles dans certains pays où il fait très chaud et humide.
Combien de personnes sont touchées ?En 2013, près de 200 millions de personnes ont eu cette maladie. Elle tue chaque année plus de 500 000 personnes, surtout en Afrique.
Ça se soigne ?Oui. Il n’existe pas de vaccin mais il existe des traitements pour éviter de l’attraper quand on voyage dans les zones où le paludisme est présent. Ensuite, si on l’attrape, ça se guérit si le malade est soigné à temps. Mais beaucoup de personnes en meurent, surtout en Afrique, parce que ce continent manque d’argent pour lutter contre la maladie.
La tuberculose
Qu’est-ce que c’est ?La tuberculose est une maladie qui touche le plus souvent les poumons. Elle entraîne notamment une très forte toux et de la fièvre.
Comment on l’attrape ?La tuberculose se transmet d’une personne à une autre. Quand un malade tousse, éternue ou crache, il peut infecter une autre personne.
Combien de personnes sont touchées ?En 2015, 9,5 millions de personnes ont eu la tuberculose et 1,5 million en meurent chaque année. C’est la deuxième maladie la plus mortelle après le sida.
Ça se soigne ?Oui. Il existe un vaccin qui est efficace chez les jeunes enfants, et aussi des médicaments. Mais une nouvelle forme de tuberculose est aussi apparue et elle résiste à ces médicaments.
