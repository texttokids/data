N°107 - 24 au 30 mai 2019 les marches pour le climat
Description

On entend souvent que l’environnement est l’affaire de tous, c’est-à-dire qu’il concerne tout le monde, sans exception. Alors pas question de laisser ça aux adultes ! Depuis plusieurs semaines, des jeunes de différents pays du monde, dont la France, organisent des grèves pour le climat : le vendredi, ils ne vont pas à l’école et, à la place, ils marchent dans la rue pour défendre la planète et lutter contre le changement climatique. Suis-moi pour tout comprendre.