N°59 - 25 au 31 mai 2018 Roland-Garros
A quoi ressemble ce tournoi ?

Dans l’univers du tennis, les tournois les plus importants sont réunis dans ce qu’on appelle «le Grand Chelem». Roland-Garros en fait partie, avec trois autres compétitions :
Les tournois du Grand Chelem
A Roland-Garros, il y a deux tournois : un pour les femmes et un pour les hommes. La gagnante remporte la coupe Suzanne-Lenglen (c’est le nom d’une ancienne joueuse très connue dans les années 1920). Le gagnant soulève la coupe des Mousquetaires (en hommage à quatre joueurs de tennis français qui étaient très forts dans les années 1920 et 1930).
La particularité du tournoi de Roland-Garros ? Les terrains de tennis sont en terre battue. C’est un mélange de brique, de calcaire et de cailloux. L’intérêt, c’est que la balle rebondit plus haut. Mais le jeu est aussi plus lent.
Le plus grand terrain s’appelle le court Philippe-Chatrier (cet homme a été le président de la Fédération française de tennis, organisation qui dirige le tennis en France). On l’appelle aussi le court central. C’est là que les deux finales (hommes et femmes) ont lieu. Les gradins peuvent accueillir jusqu’à 15 000 personnes.
Le tournoi de Roland-Garros se déroule en différentes étapes. Il démarre avec 128 joueurs. La moitié est éliminée à chaque tour. A la quatrième étape, qu’on appelle les huitièmes de finale, ils ne sont plus que 16.
Chaque année, il y a beaucoup de monde à Roland-Garros. Au printemps 2017, plus de 470 000 spectateurs sont venus voir des matchs. C’était un record.
