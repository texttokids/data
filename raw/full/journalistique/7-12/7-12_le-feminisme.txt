N°74 - 5 au 11 octobre 2018
le féminisme
Il y a un an, un scandale a éclaté dans le monde du cinéma, qui a secoué la planète. Un producteur de films très puissant, Harvey Weinstein, a été accusé par de très nombreuses femmes de leur avoir fait du mal. Depuis, beaucoup de femmes dénoncent les mauvais agissements des hommes à leur égard. Cette affaire a redonné de l’importance au féminisme, un combat ancien pour que les femmes soient les égales des hommes, car partout dans le monde, il existe encore des inégalités entre les sexes.


Marjane et Louison défendent l’égalité entre les filles et les garçons

Chez Marjane, 9 ans, et son frère Louison, 5 ans et demi, pas question de croire qu’il existe des choses réservées aux garçons et d’autres réservées aux filles. «Par exemple, il y a des familles où c’est les femmes qui font la cuisine, le ménage, et qui pensent que c’est normal. Moi je pense que c’est tout le monde qui doit faire les mêmes choses», assure Marjane. A la maison, c’est plutôt papa qui fait à manger et le ménage. Pour «les courses, ça dépend».
Leurs parents essayent de les éduquer de façon féministe. Qu’est-ce que ça veut dire ? «C’est le fait de se battre pour les droits des femmes», répond Louison.
Récemment, il a lu un livre dans lequel deux garçons déguisés en chevaliers veulent sauver une princesse. «Ce serait bien que ce soit la princesse qui sauve les chevaliers, parce que sinon c’est toujours les chevaliers qui sauvent les princesses, regrette Louison. Ce qui serait bien, c’est qu’il y ait un chevalier et une chevalière, qui sauveraient un roi et une reine !»
Marjane est d’accord : «Pourquoi ce serait toujours les filles qui seraient enfermées dans des tours et les chevaliers qui viennent les sauver ?» Parmi les films Disney qu’elle a vus, elle a gardé un bon souvenir de Rebelle, avec cette héroïne qui refuse de se laisser faire. «J’aime bien que parfois ce soit pas des princesses comme d’habitude, qu’elles puissent tirer à l’arc et tout ça», explique Marjane.
Le frère et la sœur ont entendu dire que les femmes étaient parfois moins payées que les hommes au travail. Comment régler ça ? «Il faut faire une manifestation, propose Marjane. Il faut convaincre tout le monde de payer autant les femmes que les hommes, parce que c’est de l’injustice.» Pour y arriver, son frère a une solution : «On va appeler la police municipale pour leur montrer qu’il faut arrêter de faire ça. Sinon ils iront en prison. Comme ça, ils auront peur d’aller en prison et ils arrêteront.»
Les Lilas, la ville où habitent Marjane et Louison


Que s’est-il passé depuis l’affaire Weinstein ?

Il y a un an, en octobre 2017, un scandale a éclaté dans le monde du cinéma. Des articles de journaux ont révélé que Harvey Weinstein, un célèbre producteur de films américain, avait fait des choses très graves, punies par la loi, à des femmes. En tout, une centaine de personnes, parmi lesquelles des actrices et des mannequins célèbres, l’ont accusé de les avoir agressées sexuellement ou de leur avoir très mal parlé.
On a découvert que ça faisait des dizaines d’années qu’il faisait ça. Beaucoup de gens étaient au courant, mais ils ont gardé le silence car Harvey Weinstein était très puissant dans le milieu du cinéma.
Quand cette affaire a été révélée, des gens du monde entier ont été choqués et en colère. Ça a donné à de très nombreuses femmes, connues ou inconnues, le courage d’oser dire que des hommes leur avaient fait du mal à elles aussi. Beaucoup ont témoigné sur le réseau social Twitter. Elles ont accompagné leurs messages des hashtags #balancetonporc et #metoo («moi aussi» en anglais).
Depuis, des femmes ont osé porter plainte, alors qu’elles se taisaient parfois depuis des années, par honte ou par peur qu’on ne les croie pas. Aux Etats-Unis, des hommes accusés d’avoir fait du mal à des femmes ont perdu leur travail (des animateurs de télévision, des hommes politiques, des acteurs…).
Depuis le début de cette affaire, on parle plus de féminisme que les années précédentes. Le féminisme, «c’est deux choses, explique la militante Caroline De Haas. C’est d’abord se rendre compte que les femmes et les hommes ne sont pas égaux. La deuxième chose, c’est avoir envie de changer ça». Les hommes comme les femmes peuvent être féministes.
Certaines personnes trouvent que les féministes exagèrent, qu’il n’y a plus d’inégalités entre les femmes et les hommes, que les hommes devraient continuer à pouvoir séduire les femmes comme ils le veulent, que les femmes n’ont pas besoin de travailler autant que les hommes, etc. Le féminisme crée parfois du rejet et donc des débats.
Il y a un an, le P’tit Libé avait consacré un numéro à l’affaire Weinstein et au harcèlement sexuel. Profites-en pour le lire ou le relire.


à quoi sert le féminisme ?

Caroline De Haas est une militante féministe. Elle a répondu aux questions du P’tit Libé.
Caroline De Haas : Les inégalités persistent. Le féminisme, c’est une sorte de réveil, qui sonne tous les jours pour dire «attention il y a encore des problèmes, il ne faut pas arrêter de changer le monde». Si le féminisme n’était pas là, on oublierait de faire avancer l’égalité femmes-hommes. Et l’égalité femmes-hommes quand elle n’avance pas, elle recule.
[En France,] la loi dit qu’on doit être égaux. Mais ce n’est pas appliqué. Tant que les femmes seront moins payées que les hommes pour leur travail, tant qu’il y aura des remarques pas sympas contre les femmes, on aura besoin du féminisme.
Par plein de moyens différents : apprendre aux petits garçons et aux petites filles qu’ils ont les mêmes droits, qu’on doit se respecter, obliger les patrons et les patronnes à payer de manière égale les femmes et les hommes. Je pense que si on veut vraiment l’égalité, ça peut aller très vite.
On se rend compte que, parfois sans faire exprès, les parents vont plus demander aux petites filles de mettre le couvert ou de ranger leur chambre qu’aux petits garçons. Ce n’est pas marrant de ranger sa chambre ou de mettre le couvert, on devrait tous le faire un peu. Si à la maison c’est toujours maman qui s’occupe de faire les courses, vous pouvez dire «ce n’est pas normal, c’est bien qu’on partage».
Chacun, chacune devrait être libre de tout faire. Si je veux être cosmonaute, qu’on ne me dise pas «bah non les filles ça peut pas être cosmonaute» ou si mon copain veut être danseur qu’on ne dise pas «les garçons ne peuvent pas devenir danseurs».
Il y a des gens qui pensent que les femmes et les hommes ne doivent pas avoir le même rôle dans la société. Ou peut-être que parmi vos copains-copines il y en a qui disent «le rose c’est pas pour les garçons». Moi je ne suis pas d’accord avec ça. C’est ce qu’on appelle un débat et moi j’aime bien les débats. On peut essayer de discuter, se convaincre, c’est ça qui nous fait grandir.


Quels événements ont marqué le féminisme ?

On parle beaucoup de féminisme depuis un an, mais ça fait longtemps que des personnes luttent pour que les femmes soient les égales des hommes. Chaque combat s’est développé en réaction à des inégalités précises, comme «ne pas avoir le droit d’aller à l’école, pas le droit de voter, pas le droit de choisir son mari», illustre l’historienne Florence Rochefort. Dans l’histoire, les femmes ont souvent été considérées comme inférieures aux hommes et ont ainsi eu moins de droits. Retour sur trois épisodes importants du féminisme.
«On peut dire que le féminisme prend son essor durant la Révolution française», en 1789, indique l’historienne Michèle Riot-Sarcey. Lors de cet événement, une partie du peuple s’est révoltée contre le roi et a demandé l’égalité entre les individus. C’est à ce moment-là qu’a été rédigée la Déclaration des droits de l’homme et du citoyen. Trois ans plus tard, en 1791, une femme, Olympe de Gouges, a décidé d’écrire… la Déclaration des droits de la femme et de la citoyenne. Son texte a été refusé, mais c’est l’une des premières fois qu’une femme a réclamé aussi clairement l’égalité entre les sexes.
A partir de la fin du XIXe (19e) siècle, de plus en plus de femmes, dans de nombreux pays, ont réclamé le droit de voter. C’était alors en effet réservé aux hommes. Au Royaume-Uni, à partir de 1903, des femmes se sont réunies et ont organisé des actions spectaculaires pour obtenir le droit de vote. On les a surnommées les suffragettes. Certaines ont été emprisonnées, ont fait la grève de la faim. Finalement, elles ont obtenu le droit de vote en 1918 (mais pour les citoyennes de plus de 30 ans seulement, dans un premier temps).
Le premier pays où les femmes ont eu le droit de voter est la Nouvelle-Zélande. C’était en 1893, il y a 125 ans. En France, ça date de 1944, il y a moins de 75 ans. C’est très récent ! En Arabie Saoudite, les femmes n’ont obtenu le droit de vote qu’en… 2015.
«Dans les années 1960 se développent des mouvements beaucoup plus axés sur l’intimité, la sexualité, le corps», indique l’historienne Florence Rochefort. Les femmes demandaient le droit de faire l’amour avec qui elles voulaient sans être jugées et de choisir si elles voulaient un enfant et à quel moment.
En France, les femmes n’avaient alors pas le droit d’avorter, c’est-à-dire d’arrêter leur grossesse si elles le souhaitaient. Après des années de lutte, l’avortement a finalement été autorisé en 1975, grâce à la ministre de la Santé, Simone Veil. C’est aujourd’hui encore interdit ou difficile d’accès dans de nombreux pays.


Des féminismes différents selon les endroits ?

La lutte pour l’égalité entre les hommes et les femmes, ça existe sur tous les continents. Mais «il n’y a pas une solution et une seule qui va valoir pour tous les pays», affirme l’historienne Florence Rochefort. C’est normal : les problèmes ne sont pas les mêmes d’un endroit à un autre.
En France, par exemple, la loi dit que les hommes et les femmes sont égaux, qu’ils ont droit aux mêmes choses. Les inégalités ne sont donc pas dans la loi mais dans la vie réelle : les femmes gagnent moins d’argent que les hommes, font plus de tâches ménagères, sont la cible de remarques irrespectueuses…
Dans d’autres pays, il est écrit dans la loi que les femmes ont moins de droits. Dans les pays arabes, par exemple, la loi se base sur le droit musulman et dit que les femmes doivent recevoir moins que les hommes quand elles héritent, c’est-à-dire quand elles reçoivent l’argent de quelqu’un qui vient de mourir. En Tunisie, le président a proposé de changer cette règle.
En Argentine, l’avortement, c’est-à-dire le fait de choisir d’arrêter une grossesse, est interdit. Des femmes et des hommes ont manifesté pour demander qu’il soit autorisé. Mais au mois d’août, les sénateurs, qui votent les lois, ont refusé. Dans ce pays, l’Eglise catholique refuse l’avortement et son avis est important pour les politiques quand ils prennent des décisions. Pour montrer leur désaccord, des milliers d’Argentins ont décidé de renoncer à leur baptême, et donc à leur lien avec l’Eglise et la religion catholique.
L’intersectionnalité, qu’est-ce que c’est ?Des femmes reprochent aux mouvements féministes de ne défendre que les droits des femmes blanches, et d’oublier les autres. Elles veulent que l’on prenne en compte la difficulté d’être une femme ET la difficulté d’être noire, musulmane, handicapée, lesbienne… C’est ce qu’on appelle l’intersectionnalité, autrement dit le fait de se battre contre plusieurs discriminations dont une même personne peut être victime. Par exemple, une femme musulmane voilée peut être rejetée dans des situations où une femme non voilée sera acceptée.


Y a-t-il des jeux pour les garçons et des jeux pour les filles ?

A l'occasion de la journée internationale des droits de l'enfant, le 20 novembre, Bloom, la radio des enfants, a produit un programme audio pour la ville des Lilas (Seine-Saint-Denis), en partenariat avec le P'tit Libé.
Existe-t-il des jouets de fille et des jouets de garçons ? On laisse la parole aux enfants...


