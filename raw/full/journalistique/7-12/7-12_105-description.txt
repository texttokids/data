N°105 - 10 au 16 mai 2019 le phénomène «Fortnite»
Description

En moins de deux ans d’existence, le jeu vidéo «Fortnite» est devenu l'un des grands sujets de conversation dans la cour de récré. «Fortnite» compte près de 250 millions de joueurs dans le monde entier : des enfants, comme des grands. C’est un record absolu. Les passionnés s'affrontent en ce moment pour pouvoir participer à la première coupe du monde de leur jeu favori, qui aura lieu cet été. Rejoins-moi dans ce numéro sur les jeux vidéo en ligne.