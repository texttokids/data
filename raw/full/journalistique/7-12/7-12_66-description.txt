N°66 - 13 au 19 juillet 2018 le festival d'Avignon
Description

Comme chaque année à cette période, une grande fête du théâtre est organisée à Avignon, une ville située dans le sud-est de la France. Depuis une semaine et jusqu’au 24 juillet, il y a de très nombreux spectacles partout dans la ville. Que peut-on voir pendant ce festival ? Comment fait-on pour créer une pièce de théâtre ? Monte sur scène pour découvrir tout ça !