
Tout le sport
Jeudi ler février 2018
Du 9 au 25 février, la ville de PyeongChang, en Corée du Sud (Asie), va accueillir les 23e Jeux Olympiques d'hiver. Que sait-on déjà de ces Jeux ?
Corée du Sud : derniers jours avantles JO
Le 6 juillet 2011, la ville de PyeongChang, en Corée du -Sud, est en fête. Elle vient d'être choisie par leCIO, le Comité international olympique, pour organiser les 23e Jeux Olympiques d'hiver, du 9 au 25 février 2018. Depuis, la ville se prépare pour cet événement géant, qui va durer 17 jours.
Comme plusieurs régions de France, Paris a les pieds dans l'eau. Mais le niveau de la Seine n'a pas entraîné de perturbations trop graves dans le pays. Après juin 2016, c'est la deuxième fois en peu de temps que la Seine est en crue
12 stades.
Depuis 2011, six nouveaux stades ont vu le jour à PyeongChang et dans les environs. Le plus grand est le stade olympique avec ses sept étages et ses cinq faces, comme les cinq anneaux du drapeau olympique. C'est dans ce stade que se dérouleront les cérémonies d'ouverture et de fermeture des Jeux. Six autres stades ont été rénovés.
Record.
Il y aura en tout 102 épreuves sportives différentes, dont 6 nouvelles, comme le big air, un tremplin pour faire des figures en snowboard. C'est un record dans l'histoire des Jeux Olympiques d'hiver.
Les équipes de Corée du Nord et de Corée du Sud vont défiler ensemble lors des cérémonies d'ouverture et de fermeture de Jeux Olympiques d'hiver en
Corée du Sud. Une équipe de hockey sur glace, com posée de joueuses des deux pays, va aussi participer à la compétition. Une premiere dans l'histoire des Jeux Olympiques. C'est un signe de paix très important. A l'origine, ces deux pays n'en formaient qu'un, la Corée, qui a été séparée en 1945. Depuis, les deux pays s'entendent très mal et la frontière qui les sépare est fermée. Les Jeux Olympiques de PyeongChang pourraient être un premier pas vers un véritable dialogue entre les deux pays. Ce n'est pas officiel, mais les deux pays marcheront peut-être en portant le même drapeau, représentant la Coréé La Corée du Sud a annoncé que l'organisation des Jeux lui coûtera plus de 10 milliards d'euros. Pour rembourser cette somme, le pays compte en partie sur la vente des billets pour voir les compétitions, mais ils n'ont pas encore tous été vendus.
Soohorang est la mascotte des Jeux : de PyeongChang. C'est un tigre blanc, l'animal protecteur de la Corée du Sud. Son nom est un mélange de deux mots coréens : Sooho, la protection, et Rang, comme ho-rang, le tigre. Sur son ventre, il porte: l'emblème des Jeux fait de deux signes de l'alphabet coréen. Cet emblème représente la neige, la glace, et les étoiles (pour les athlètes).



