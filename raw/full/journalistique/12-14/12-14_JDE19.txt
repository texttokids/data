Jeudi 25 janvier 2018
Tout le sport
Une nouvelle technologie a fait son apparition dans le championnat de France de Ligue 1 de football
pour aider les arbitres à ne pas se tromper. Mais ce système ne marche pas encore très bien.
Est-ce qu'il y a but, m'sieur l'ordinateur ?
Zoom Ne pas confondre...
La scène se passe le 15 juin 2014, au Brésil. On joue la 48e minute du match entre la france et le Honduras au 1er tour la Coupe du monde. L'attaquant français Karim Benzema envoie une frappe qui touche le poteau et revient sur le gardien qui tente désespérément de l'empêcher de franchir la ligne du but. Impossible de dire si le ballon est rentré. Pourtant l'arbitre valide le but sans hésitation grâce  à un nouveau système informatique : la goal line technology (la technologie sur ligne de but, en français). C'est la première fois qu'elle est utilisée.
Il ne faut pas confondre la goal line technology (GLT), qui permet de savoir si le ballon est entré dans le but, et l'assistance vidéo à l'arbitrage (VAR). La VAR, c'est la possibilité pour un arbitre de bénéficier de l'aide de la vidéo, autrement dit de revoir les images d'une action, avant de prendre une décision. Elle a été testée pour la première fois en France le 9 janvier lors du match entre Nice et Monaco en quart de finale de la Coupe de la Ligue. Une équipe d'arbitres assistants [qui aident l'arbitre principal] est installée devant des écrans de télévision sur lesquels ils peuvent voir le match et les images au ralenti. S'ils se rendent compte que l'arbitre s'est trompé, ils le préviennent grace a une oreillette. S'il a un doute, l'arbitre peut lui-même demander vérification. La vidéo ne peut être utilisée que dans quatre situations : après un but marqué, sur une situation de penalty, pour un carton rouge direct ou pour corriger une erreur d'identité d'un joueur sanctionné. Elle pourrait faire son apparition lors de la Coupe du monde en Russie.
Cette technologie permet de savoir si le ballon est bien rentré dans le but ou non.
Elles sont réels à un ordinateur qui analyse les images.

Ce jeune garçon transporte une tortue de rivière âgée de 60 ans. Elle vit dans une ferme en Colombie (Amérique du Sud) où l'on prend soin de cette espèce en danger de disparition.
Faute du maillot fluo > À plusieurs reprises, le système n'a pas actionné. Comme le 10 janvier dernier, lorsqu'un joueur du Paris Saint-Germain a inscrit un but de tête contre Amiens. 
Le football n'est pas le premier sport à utiliser la technologie pour aider les arbitres. Le tennis, le rugby, le cricket [sport qui ressemble au baseball] ou encore le football américain s'en servent aussi. Au Tennis par exemple, le système appelé «Hawk-Eye» (oeil : de faucon en français) ressemble beaucoup à celui utilisé dans le foot.
Des caméras reliées à un ordinateur permettent de savoir si la balle est tombée dans le terrain ou en dehors.

