Qui s'occupe de la campagne ?

Quand un candidat se présente à l’élection présidentielle, on dit qu’il fait campagne : ça veut dire qu’il fait tout pour être élu en se déplaçant dans toute la France, en étant le plus possible dans les médias… Mais il n’est pas tout seul pour le faire. Chaque candidat est entouré d’une équipe de campagne pour l’aider à réaliser son objectif.
Un directeur de campagne
Il y a beaucoup de choses à faire pendant une campagne et ça demande une bonne organisation. C’est le rôle du directeur de campagne. Il s’agit généralement d’une personne très proche du candidat, en qui il a une grande confiance. C’est une sorte de chef d’orchestre : il dirige toutes les personnes qui travaillent pour le candidat.
Des porte-parole
Le candidat ne peut pas répondre à toutes les questions de tous les journalistes dans les médias, ni faire des discours toute la journée, il n’aurait pas le temps. Il a donc des porte-parole, des personnes qui sont officiellement chargées de diffuser ses idées.
Des conseillers
A l’école, on peut être bon élève et avoir à la fois de très bonnes notes en français, en sciences et en sport. Mais quand on fait de la politique, c’est vraiment difficile de connaître sur le bout des doigts tout ce qui concerne les Français. Alors les candidats ont des conseillers : ce sont des spécialistes d’un sujet, qui partagent leurs connaissances avec eux.
Une équipe de communication
Si un candidat a de très bonnes idées mais que personne ne les connaît, ça ne sert pas à grand-chose : personne n’aura envie de voter pour lui. Des personnes sont donc spécialement payées pour faire connaître le candidat : elles créent des affiches qui donnent une image sympathique, elles contactent les journalistes pour qu’ils parlent de lui dans leur média…
Des militants
Les candidats sont aidés par des citoyens qui partagent leurs idées et les aident à les défendre. Il s’agit de militants. Ils distribuent des tracts (des feuilles sur lesquelles sont écrites certaines de leurs propositions) dans la rue, ils collent des affiches, ils sonnent chez les gens ou les appellent pour leur donner envie de voter pour leur candidat.
Des journalistes
Les journalistes accompagnent les candidats lors de leurs déplacements, racontent ce qui s’y passe, leur posent des questions au sujet de leurs propositions ou de l’actualité, interrogent les électeurs sur ce qu’ils pensent…
Des instituts de sondage
Tout au long de l’année, des entreprises, qu’on appelle des instituts de sondages, demandent aux Français pour qui ils voudraient voter ou quels sujets leur semblent les plus importants à défendre pendant la campagne présidentielle. Parfois, des candidats changent d’avis en voyant des sondages. Par exemple, si l’un d’eux veut absolument changer les menus de la cantine mais se rend compte que ça n’intéresse pas les Français, il peut laisser tomber cette idée et en trouver une autre.