A quoi sert le président ?

La France est une République. Ça veut dire que les citoyens élisent un président et leurs représentants pour une durée limitée. Le président de la République est élu pour cinq ans et n’a pas le droit d’occuper cette fonction plus de deux fois de suite.
C’est le chef du pays. C’est d’ailleurs pour ça qu’on l’appelle aussi le chef de l’Etat. En France, il travaille à Paris, dans un palais qui s’appelle l’Elysée. C’est très grand : il y a plus de 360 pièces !
Une fois élu, le président doit mettre en place les idées qu’il a proposées. Pour ça, il s’entoure d’une équipe, qu’on appelle le gouvernement, composée du Premier ministre (choisi par le président) et de plusieurs ministres (sélectionnés par le Premier ministre). Le Premier ministre travaille dans un lieu qui s’appelle Matignon.
Chaque ministre travaille dans un domaine différent (ça va de la sécurité à la culture) avec sa propre équipe. Par exemple, le ministre de l’Education nationale peut changer ce qu’on apprend à l’école. Si le président n’est pas content d’un ministre, il peut le renvoyer.
Où travaillent les personnes qui dirigent la France ?
1. L’Elysée : construit il y a près de 300 ans, c’est un palais où vit et travaille le président de la République.
2. L’Assemblée nationale : construite il y a près de 300 ans, c’est le lieu où les députés votent les lois. On l’appelle aussi «le Palais Bourbon».
3. Matignon : c’est le lieu de travail et de vie du Premier ministre depuis 1934.
4. Le ministère de l’Education nationale : il a été créé comme on le connaît aujourd’hui en 1829. C’est là où travaille la ministre qui s’occupe de l’école.
Le rôle du président est aussi de faire en sorte que la Constitution soit respectée. La Constitution, c’est le texte qui définit les droits et les libertés des citoyens. C’est aussi ce texte qui impose une séparation entre les différents pouvoirs : les personnes qui votent les lois (le pouvoir législatif), celles qui les font appliquer (le pouvoir exécutif) et celles qui disent si elles sont respectées ou non (le pouvoir judiciaire) doivent être différentes.
Le président est aussi le chef des armées. Il est responsable de la sécurité de la France et peut décider de faire la guerre à un autre pays.
Le chef de l’Etat doit évidemment respecter les lois, comme tout le monde. Mais on n’a pas le droit de porter plainte contre lui tant qu’il est président. Si jamais il se comporte vraiment très mal, par exemple s’il trahit le pays, il peut être destitué, c’est-à-dire qu’on lui retire sa fonction de président.
Quelques histoires de présidents
La France a eu 24 présidents de la République différents. Mais il n’y a jamais eu de femme. Le premier président était Louis-Napoléon Bonaparte, élu en 1848.
Valéry Giscard d’Estaing est l’un des quatre présidents encore en vie, il a 91 ans. Il a dirigé la France il y a quarante ans.
François Mitterrand détient le record de la présidence la plus longue ! Il a dirigé la France pendant 13 ans, 11 mois et 26 jours, entre 1981 et 1995. A l’époque, le président était élu pour sept ans et non cinq comme aujourd’hui.
Adolphe Thiers est le président élu le plus vieux : il avait 74 ans. C’était en 1871. Le plus jeune est Louis-Napoléon Bonaparte : il n’avait «que» 40 ans.
Quatre présidents sont morts pendant qu’ils dirigeaient la France. Parmi eux se trouve Sadi Carnot : il a été tué en 1894 par un homme qui n’était pas d’accord avec lui.
En France, il existe beaucoup de rues Victor-Hugo (un écrivain) ou de places Jean-Jaurès (un homme politique). Mais c’est l’ancien président Charles de Gaulle qui a le plus d’endroits à son nom : on en compte 3 900 !