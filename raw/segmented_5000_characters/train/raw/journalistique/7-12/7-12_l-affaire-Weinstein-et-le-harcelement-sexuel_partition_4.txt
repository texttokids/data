Que faire en cas d’agression ?

Malheureusement, on peut être victime de harcèlement ou d’agression sexuelle quand on est un enfant. Un camarade de classe peut soulever ta jupe ou essayer de t’embrasser alors que tu ne veux pas, quelqu’un peut te toucher le sexe ou te demander de toucher le sien.
Ça peut concerner des filles comme des garçons. Ça peut venir d’un adulte comme d’un enfant. Dans tous les cas, personne n’a le droit de te faire ça.
On s’en est pris à moi et je n’ai pas réagi, c’est de ma faute ?
«C’est normal d’être bloqué, de ne pas comprendre ce qui arrive, explique la psychiatre Muriel Salmona. Surtout, il ne faut pas se sentir coupable de ne pas avoir réagi.»
Il faut imaginer qu’on a un interrupteur sur le cerveau. Quand on subit une agression, ça s’éteint. On ne se rend plus vraiment compte de ce qui se passe, on n’est pas capable de se défendre, parce que la situation fait très peur et c’est le moyen que le cerveau a trouvé pour nous protéger. Ça ne veut pas dire qu’on est d’accord.
A qui puis-je parler ?
«Il ne faut surtout pas rester seul, mais en parler autour de soi, insiste Muriel Salmona. Ce n’est pas un secret, c’est quelque chose de très grave, il faut le dire». Tu peux te confier à tes parents, tes cousins-cousines, tes oncles ou tantes, un professeur, ou encore te rendre à l’infirmerie scolaire, chez l’assistante sociale ou chez le médecin.
«Si tu parles à un adulte et qu’il ne te prend pas au sérieux, il faut en parler à quelqu’un d’autre. Les adultes ne savent pas toujours comment réagir», conseille Muriel Salmona.
Tu peux aussi appeler le 119 : c’est le numéro de téléphone pour aider les enfants en danger. C’est gratuit. Il existe aussi le 17, pour appeler la police.
Si tu as peur de parler à un adulte, confie-toi à un copain, ça peut être plus facile.
Si un copain a subi une agression, qu'est-ce que je fais ?
Tu dois en parler à un adulte. Si quelqu’un a fait du mal à ton copain ou ta copine, il faut que ça s’arrête, que quelqu’un l’aide et que son agresseur soit puni. Tu n’aides pas ton copain ou ta copine si tu gardes son «secret».