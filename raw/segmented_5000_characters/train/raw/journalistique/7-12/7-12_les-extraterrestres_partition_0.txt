N°87 - 4 au 10 janvier 2019
les extraterrestres
On les imagine verts avec de longs bras, débarquant sur Terre pour conquérir notre planète… Mais au-delà de notre imagination, que sait-on vraiment des extraterrestres ? Existent-ils ? Pourquoi cherche-t-on des traces de vie sur Mars ? Embarque dans la fusée P'tit Libé pour le savoir !

Paul Martin imagine des histoires d’extraterrestres

Paul Martin a toujours été passionné par les extraterrestres. Enfant, il collectionnait les articles de journaux sur les objets volants non identifiés. Adolescent, il a passé une nuit à chercher des soucoupes volantes dans le ciel. Désormais âgé de 50 ans, il est le scénariste de Kiki et Aliène, une bande dessinée sur… les extraterrestres !
Quand Astrapi, le magazine pour enfants pour lequel il travaille, lui a demandé de créer une histoire rigolote, Paul a tout de suite su qu’il voulait des héros extraterrestres. «Ce sont des êtres complètement étrangers, qui ne connaissent rien à notre monde. J’avais envie d’imaginer leur réaction face à des objets de notre quotidien», explique l’auteur lillois. Il commence alors à inventer les aventures de deux extraterrestres, venus sur Terre non pas pour l’envahir, mais pour faire du tourisme.
Paul et Nicolas Hubesch, l’illustrateur de la bande dessinée, se sont inspirés de deux films très connus sur les extraterrestres pour créer Kiki et Aliène. «Comme E.T., Kiki est rond et sympathique, on a envie de le protéger. Aliène, lui, ressemble aux personnages de la série de films Alien. Il a un côté plus méchant», explique-t-il.
Pour créer des situations marrantes, Paul aime aussi s’inspirer des stéréotypes qui entourent les extraterrestres. Par exemple, dans les romans de science-fiction, les extraterrestres kidnappent souvent les humains pour les étudier. «Mais quand Kiki et Aliène enlèvent une personne, c’est juste pour lui demander la recette de la bolognaise !», raconte le scénariste.
Même s’il les aime beaucoup, Paul ne croit pas aux extraterrestres. «S’ils existaient, on le saurait», dit-il. Peu importe, son plaisir, c’est de les imaginer.
Lille, la ville où habite Paul Martin