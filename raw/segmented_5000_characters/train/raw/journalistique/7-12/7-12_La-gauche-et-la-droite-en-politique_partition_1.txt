Les primaires, qu'est-ce que c'est ?

En ce moment, on entend beaucoup parler d’hommes et de femmes politiques qui se présentent à des primaires en France. Ça ne veut pas dire qu’ils vont retourner à l’école ! Les primaires, ce sont des élections organisées par des partis politiques pour choisir leur candidat qui se présentera à l’élection présidentielle. Ce n’est pas obligatoire. Certains partis ont déjà désigné leur candidat sans passer par cette étape.
Dans chaque camp, plusieurs candidats s’affrontent. Pour se présenter, ils doivent pour la plupart recueillir des parrainages : ils doivent être soutenus par un certain nombre d’élus (comme des maires) et de militants (des personnes qui défendent leur parti en collant des affiches dans la rue, par exemple).
Les candidats qui ont obtenu assez de parrainages essayent de convaincre les gens de voter pour eux. Pour ça, ils organisent des meetings (des grandes réunions où ils parlent devant ceux qui les soutiennent), écrivent des livres pour exposer leurs idées et affrontent les autres concurrents dans des débats retransmis à la télé.
Ensuite, les électeurs votent pour la personne qu’ils préfèrent pour défendre leurs idées.
Celui qui gagne la primaire doit ensuite récolter 500 nouveaux parrainages d’élus pour avoir le droit de se présenter à l’élection présidentielle.
Cette année, il y a trois primaires : à droite, à gauche et chez les écologistes.
Date de l'élection : 20 et 27 novembre
C’est la première fois que la droite organise une primaire en France.
Conditions pour voter : toutes les personnes qui souhaitent voter doivent signer un papier pour dire qu’elles sont d’accord avec les valeurs de la droite et du centre et payer deux euros.
Candidats : Jean-François Copé, François Fillon, Alain Juppé, Nathalie Kosciusko-Morizet, Bruno Le Maire, Jean-Frédéric Poisson, Nicolas Sarkozy.
Date de l'élection : 22 et 29 janvier
Le principal parti de gauche, le Parti socialiste (PS), a été le premier à organiser une primaire en France, en 2006. Ségolène Royal avait gagné, mais elle avait ensuite perdu l’élection présidentielle, remportée par Nicolas Sarkozy. Cette fois-ci, des candidats de plusieurs partis de gauche se présentent à la primaire.
On ne connaît pas encore tous les candidats parce qu’ils ont jusqu’au 15 décembre pour se déclarer. Beaucoup de gens pensent que le Président, François Hollande, se présentera, mais il annoncera sa décision début décembre.
Conditions pour voter : toutes les personnes qui souhaitent voter doivent signer un papier pour dire qu’elles sont d’accord avec les valeurs de la gauche et payer un euro.
Candidats : Jean-Luc Bennahmias, Gérard Filoche, Benoît Hamon, Marie-Noëlle Lienemann, Arnaud Montebourg, François de Rugy.
Date de l'élection : 19 octobre et 6 novembre
C’est la deuxième fois que les écologistes organisent une primaire. La première fois, c’était en 2011.
Ils étaient quatre candidats au départ. Après la première étape de l’élection (le premier tour), deux ont été éliminés.
Conditions pour voter : contrairement aux autres primaires, les électeurs n’ont pas besoin se déplacer pour voter. Ils s’inscrivent sur Internet, puis doivent envoyer un courrier avec le nom du candidat qu’ils préfèrent et certains doivent payer 5 euros.
Candidats : Karima Delli (éliminée), Cécile Duflot (éliminée), Yannick Jadot, Michèle Rivasi.