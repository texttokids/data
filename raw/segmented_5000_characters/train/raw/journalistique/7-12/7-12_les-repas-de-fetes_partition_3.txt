Comment réussir son goûter de Noël ?

Eden et Ambre sautillent partout. Ce samedi de décembre, les deux sœurs jumelles de 7 ans participent à un cours de pâtisserie, à l’Atelier des chefs, à Paris, et ont hâte de s’y mettre. Le thème du jour, c’est le goûter de Noël. Au menu : des bûches à la crème de marrons et aux poires, des mendiants (du chocolat sur lequel on met des fruits secs ou des bonbons) et un chocolat chaud à l’orange.
Julien, le chef, a 1h30 seulement pour apprendre à quatre enfants et trois parents à faire ces recettes.
14 heures : les pâtissiers se lavent les mains et enfilent leurs tabliers. Première étape : éplucher les poires. Pas facile d’utiliser un économe, l’ustensile qui permet d’enlever la peau…
Puis ils taillent les poires en julienne, c’est-à-dire en bâtonnets. Julien montre à Eden comment utiliser le couteau sans se couper, en ne mettant pas ses doigts sur le chemin de la lame.
Pour préparer le biscuit de la bûche, c’est physique : il faut battre les blancs en neige à la main, avec un fouet, pour faire entrer de l’air.
Une fois tous les ingrédients mélangés, il faut étaler la pâte sur une plaque, puis la mettre au four.
Pendant que les biscuits cuisent, les petits cuistots s’attaquent aux mendiants. Ils forment des ronds de chocolat fondu sur une plaque tout juste sortie du frigo.
«Vous faites la déco que vous voulez», leur indique Julien. Eden, Victor et les autres plongent leurs mains dans les paquets de mini-marshmallows et de fruits confits et les disposent sur leurs ronds de chocolat.
On ne baisse pas le rythme : maintenant que les mendiants sont prêts, on passe au chocolat chaud. Les enfants pèlent des oranges et mettent les écorces dans le lait chaud, pour lui donner du goût, avant de le mélanger avec le chocolat.
Revenons-en maintenant au plus important : la bûche. Les pâtissiers étalent de la mousse de marron sur le biscuit qui a un peu refroidi, mettent des bouts de poire et roulent le tout. Ambre s’en sort sans l’aide de personne.
Julien, le chef, verse du chocolat fondu sur les bûches. «Après on pourra manger le chocolat qui a coulé ?», demande Eden. Tous plongent leurs doigts dedans à plusieurs reprises. Esteban a plein de chocolat autour de la bouche.
15h30 : c’est l’heure de la dégustation. Les enfants et leurs parents boivent le chocolat chaud qu’ils ont préparé et mangent quelques mendiants. Ils emporteront les bûches chez eux pour les manger plus tard.
Bilan de ce cours de pâtisserie ? Tout le monde est ravi, les enfants comme les parents. «J’ai aimé faire la décoration, et enrouler la bûche, c’était drôle», apprécie Ambre. Sa sœur et elle ont même prévu de refaire la bûche poire-marron le soir de Noël.
Tu veux refaire les recettes de l'Atelier des chefs ? Les voici !