M. K.

société

Rue de la Banque à Paris, on évacue encore et encore...

Hier, les femmes africaines mal logées qui occupent depuis plus d'un mois la rue de la Banque, au centre de Paris, ont eu à subir les foudres des forces de l'ordre lors d'une énième évacuation. Trois d'entre elles ont été blessées. Depuis deux jours, de nombreuses personnalités sont venues les soutenir : Josiane Balasko, Emmanuelle Béart, Gérard Depardieu, Guy Bedos et Richard Bohringer. De son côté, le Comité actions logement a ouvert un nouveau front en installant une centaine de mal-logés place de Stalingrad, dans le nord-est de Paris.

social - économie

Alcatel-Lucent licencie 4 000 personnes

Télécommunications . L'équipementier annonce un « plan de la dernière chance » qui équivaut à une réduction de 26 % des effectifs.

« On ne change pas une stratégie perdante. » Dans un communiqué commun, CGT, CFDT et CFTC fustigent la dernière décision de la direction d'Alcatel-Lucent, qui prévoit 4 000 suppressions de postes supplémentaires d'ici à 2009. Au début de l'année, le groupe né de la fusion du français Alcatel et de l'américain Lucent avait déjà revu à la hausse le nombre de suppressions d'emplois, de 9 000 à 12 500, dont 1 468 pour la France. Devant les menaces de conflit social, cette fois-ci Alcatel-Lucent a refusé de préciser la répartition géographique de ces nouvelles coupes. L'intersyndicale a donné le ton, mercredi, mais c'est de la CFE-CGC qu'est venu l'avertissement le plus clair : « Les salariés européens, après une année traumatisante et un conflit social sans précédent, n'accepteront pas une seule suppression d'emploi supplémentaire en France et dans l'Union européenne. »

C'est peu dire que le « plan d'action agressif », terme employé par la direction elle-même, suscite des inquiétudes. Dans son communiqué, l'intersyndicale souligne la contre-productivité de la « stratégie » de suppression d'emplois : « non-qualité des produits, délais non tenus, absence d'innovation »... Selon Alain Hurstel, secrétaire (CFDT) du comité d'entreprise européen du groupe, « on est dans une logique désespérante ». Les deux dirigeants d'Alcatel-Lucent, Serge Tchuruk et Patricia Russo, « sont maintenant discrédités ». Quant aux salariés, « déjà durement marqués par les 12 500 suppressions d'emplois initiales », CGT, CFDT et CFTC estiment que « cette nouvelle annonce les maintient dans le cercle infernal des plans sociaux à répétition ». « Depuis que nos directions licencient, si c'était la solution, Alcatel-Lucent serait leader mondial », ironisent les syndicats, qui appellent les salariés à « se mobiliser ». Plusieurs centaines de salariés de quatre sites dans l'ouest de la France (Orvault, Lannion, Cesson-Sévigné et Saint-Grégoire) ont entendu cet appel : mercredi, ils ont débrayé pendant une heure pour marquer leur inquiétude. « Les salariés sont écoeurés, lâche Jean-Pierre Clavaud, délégué CGT à Orvault. On a peur pour tout le monde à partir du moment où on ne sait pas où sont localisées les suppressions d'emplois. »

Grégory Marin

Politique

Comment imposer un référendum sur le mini-traité ?

Europe. L'association Gauche Avenir invite tous les parlementaires de gauche à voter contre la révision constitutionnelle.

La position vers laquelle chemine la direction du PS vis-à-vis du traité de Lisbonne jette le trouble chez les militants socialistes qui avaient voté « non » à la constitution Giscard, comme 55 % des Français. Au cours de la campagne de l'élection présidentielle, Ségolène Royal, exprimant en la matière l'opinion du parti, avait déclaré qu'il fallait respecter le vote du 29 mai 2005 et que tout autre projet susceptible de remplacer le traité constitutionnel ne pourrait être soumis à ratification que par un nouveau référendum. Le revirement de François Hollande abandonnant la revendication du référendum puis souhaitant « laisser passer » le traité dans la forme voulue par Nicolas Sarkozy - la voie parlementaire - a été perçu comme un reniement et un déni de démocratie par les militants socialistes qui participaient mardi soir à Paris à une réunion de l'association Gauche avenir, cet espace de dialogue et de réflexion entre militants de gauche, socialistes et communistes principalement. « La parole politique est ridiculisée », s'emportait un militant PS.

Plusieurs députés européens - les socialistes Marie-Noëlle Lienemann, Françoise Castex, Anne Ferreira (membres du groupe PSE) et le communiste Francis Wurtz, président du groupe de la gauche unitaire européenne (GUE-GVN) - ont démonté la manipulation en cours visant à faire passer, en contournant le peuple, un texte qui est « un copié-collé de la constitution », selon Marie-Noëlle Lienemann. L'astuce consiste à ne plus l'appeler constitution, à supprimer les symboles de l'Union (le drapeau et l'Hymne à la joie) et à disperser toutes les dispositions de la constitution sous forme d'amendements et de protocoles additionnels aux traités antérieurs.