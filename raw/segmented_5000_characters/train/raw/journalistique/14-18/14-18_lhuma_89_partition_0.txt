Il y a des poètes et ils sont vivants. Je te propose de t'en faire découvrir quelques-uns. Je n'ai que cinq jours.

C'est court, mais si je t'ouvre cette porte et que tu la franchis, ce sera pour longtemps.

Aujourd'hui, je t'ouvre une porte vers Édith Azam, une jeune femme, poète du sud de la France, avec sa voix venue du ventre et l'énergie vitale qui irrigue son écriture. Elle lit le monde avec amour et détermination.

Pensé à toi ma Soeur Cartouche, Pensé je ne sais plus même quoi,Pensé dans cette obscurité qui tombe et qui n'a toujours fait que ça.

J'ai été recouverte de lune, J'ai été happée par le vent.

Trop vite - Trop fort - Trop haut. Étirée plus loin que moi-même, Et le sternum :

BANG !

Soeur Cartouche

À demain, ami.

http://michel.thion.free.fr

événement

Agenda

Les grèves de novembre

Le 13 : personnels techniques de l'Opéra de Paris.

Le 14 : cheminots, salariés d'EDF, de GDF.

Le 20 : fonctionnaires, enseignants, étudiants et lycéens, salariés des postes et télécommunications.

Le 22 : salariés de LCL (ex-Crédit lyonnais).

Le 29 : magistrats et salariés de la justice.

événement

« L'arrogance ou la division ne peuvent tenir lieu de politique »

Député PCF du Havre, Daniel Paul témoigne des difficultés des habitants de sa circonscription et de la colère face à la baisse du pouvoir d'achat.

Depuis la grève du 18 octobre, Sarkozy ne cesse de répéter : « J'ai été élu pour faire ces réformes, ce n'est pas la rue qui m'arrêtera. » Qu'en pensez-vous ?

Daniel Paul. Je ne pense pas que les mesures prises depuis six mois aient été détaillées dans le programme de Nicolas Sarkozy. Et il ne s'agit pas d'opposer la rue et les urnes. Parmi les manifestants du 18 octobre comme parmi ceux qui se préparent pour le 14 novembre, pour le 20 ou, comme les magistrats, pour la fin du mois, il y a des électeurs qui ont cru au

discours sarkozyste. J'en connais personnellement. L'arrogance ou la division ne peuvent tenir lieu de politique. Et la démocratie, je dirai même la République, ce n'est pas le diktat permanent, c'est la discussion, la négociation.

Le gouvernement parle, pour les luttes en cours, de corporatisme, de privilégiés...

Daniel Paul. Pour les retraites, l'objectif du gouvernement est de passer à quarante et un ans de cotisations. Il veut donc commencer par aligner tout le monde sur la durée la plus élevée possible. Parler de privilèges et montrer du doigt les cheminots ou les salariés d'EDF et de GDF est une tromperie. Les privilégiés, ce sont les actionnaires de la Générale de santé qui vont se partager un dividende exceptionnel de 420 millions d'euros, soit la moitié du montant des franchises médicales. Ce sont les bénéficiaires de stock-options qui n'ont pas à s'inquiéter de la taxation à 2,5 % que l'Assemblée vient de voter, même si c'est un premier pas. Ce sont les actionnaires de Suez qui vont recevoir en 2008 une pépite nommée GDF. Le pouvoir cherche à diviser le monde du travail pour mieux faire passer ses mauvais coups en direction de tous les salariés. C'est pourquoi, comme tous les députés communistes, je serai dans la rue, avec les salariés, le 14 et les jours suivants éventuellement.

Quel est l'état d'esprit de ceux que, comme député, vous rencontrez ?

Daniel Paul. Ma circonscription est composée essentiellement de cités d'habitat social où l'on vit de plus en plus mal. Aujourd'hui, nous menons une grande bataille sur les charges locatives. Leur augmentation pose des problèmes énormes compte tenu de la stagnation du pouvoir d'achat des retraites, des pensions et des salaires. De plus en plus de personnes me parlent de leur impossibilité à faire face aux rappels de charges qui tombent actuellement. Et avec l'augmentation des prix du pétrole, cela va continuer. Il y a donc de l'inquiétude. De la colère aussi devant l'accumulation des mauvais coups, la précarité qui se développe, les inégalités qui se creusent et qui sont ressenties comme telles. Mais ce qui est le plus fort aujourd'hui dans ces cités, c'est la colère contre la baisse du pouvoir d'achat.

Après six mois de présidence Sarkozy, quel bilan ?