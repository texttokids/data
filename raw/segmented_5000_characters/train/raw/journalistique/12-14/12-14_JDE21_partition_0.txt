Jeudi 26 janvier 2018
La Norvège a annoncé son intention de mettre fin aux élevages d'animaux pour leur fourrure.
Les associations de défense des animaux le demandent depuis longtemps.
Des oiseaux heureux en ville.
Le plus petit chat sauvage au monde.
Vers la fin des élevages de fourrure ?
Tout a débuté en 2006. À Copenhague (Danemark), le projet « Happy City Birds » avait pour but de recréer des habitats pour les oiseaux menacés par la disparition des arbres. Aujourd'hui, l'artiste de rue Thomas Dambo a construit plus de 3500 cabanes à oiseaux dans les rues de la capitale danoise. Ces habitats sont fabriqués à partir de bois recyclé, puis installés par l'artiste dans les rues, les parcs, ou sur les façades.
Les Zoulous l'abandonnent

La loi contre la fourrure n'a pas encore été votée, mais la majorité [plus grande partie] du gouvernement de Norvège a annoncé qu'elle était d'accord.
Le skate se transforme en lunettes.
En Allemagne, la marque de sport Adidas a imaginé des chaussures qui fonctionnent comme un titre (ticket de transport. L'idée a été imaginée pour fêter les 90 ans du métro de Berlin.
Pour se procurer ces peaux, certains chassent illégalement et menacent la population de léopards. Ces peaux coûtent cher et les membres les plus pauvres de la tribu ne pouvaient pas en acheter, A la place, ils prenaient des peaux de vaches et : peignaient les taches du léopard
La fourrure a d'abord été utilisée pour se réchauffer. Puis elle est venue orner les tenues de cérémonie des puissants, avant, au début du 20' siècle, de devenir un symbole de luxe. Dans les années 1970/1980, la mode est aux manteaux entièrement en fourrure. Mais des voix s'élèvent pour dénoncer la façon dont sont chassés ou élevés les animaux qui fournissent ces fourrures. Conséquence, de nombreux créateurs de mode ont renoncé à employer de la fourrure naturelle et utilisent de la fourrure synthétique (créée par l'homme).

A cette occasion, Adidas a fabriqué 500 exemplaires qui ont été vendus uniquement dans deux boutiques de la capitale allemande. Le prix : 180 euros pour les baskets et l'abonnement annuel au métro. A quand les mêmes pour les transports en commun en France ?
Une couturière a réparer l'aile d'un papillon.
Au Japon, une alerte au poisson tueur.

Mais malgré toutes les précautions qui sont prises, chaque année, le fugu fait des morts au Japon. Alors quand on a découvert que cing fugus avaient été vendus sur un marché de la ville de Gamagori, au centre du Japon, sans que leurs organes aient été enlevés avant, une alerte a été lancée. Des messages ont été diffusés par hauts-parleurs dans toute la ville pour retrouver les acheteurs. Mais seuls trois poissons ont été localisés.
Comment recycler d'anciennes planches de : skateboard ? En paire de lunettes, bien sûr ! C'est l'idée originale qu'a eue Florent Baraban, opticien-lunetier, créateur de la marque "7Plis". Florent et ses associés adorent le skateboard. En côtoyant les professionnels, ils se sont rendu compte que ces derniers n'utilisaient leurs planches que deux semaines avant : de les jeter. Quel gâchis !
La question s'est donc posée : comment réutiliser ces planches souples, toutes composées de 7 plis [couches] de bois d'érable ? : C'est ainsi que l'idée est venue de façonner [travailler] ce matériau pour en faire des lunette.
La vie d'un papillon est aussi fragile que courte. Mais une couturière du Texas (Etats-Unis) en a décidé autrement pour un papillon en détresse. Habituellement, les mains de Romy McCloskey lui servent à confectionner de très belles broderies pour des costumes de filmes. Récemment, elle découvre un magnifique papillon Monarque dont l'aile droite est abimée et trouée. Cette sorte de papillon peut vivre de deux semaines jusqu'à cinq mois, en fonction de la période à laquelle il est sorti de l'euf Pour aider ce beau papillon, Romy a alors installé une véritable petite table d'opération : éponge, cintre métallique, colle contact, cure-dent, coton-tige, ciseaux, pince à épiler et talc, Pas besoin d'endormir le papillon avant de l'opérer : les ailes sont comparables aux ongles ou aux cheveux. Ils ne sentent donc pas la douleur. Après le travail minusfieux de la couturière, le papillon a pu reprendre sont vol. Bluffant !