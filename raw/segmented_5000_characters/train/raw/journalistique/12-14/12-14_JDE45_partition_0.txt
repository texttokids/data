Jeudi 5 octobre 2017
Le monde compte aujourd'hui 1,1 milliard de filles. Elles sont pleines de talent et d'ambition, mais trop souvent confrontées à des situation
de violence ou à des interdictions auxquelles ne sont pas soumis les garçons.

Pourquoi les filles sont-elles plus vulnérables

Ne pas aller à l'école enferme les filles dans la pauvreté.
Apprendre à lire et à écrire les aide à en sortir. 
Mais pour aller à l'école, les filles ont besoin d'être en sécurité et respectées.
Vous êtes peut-être assis tranquillement dans votre chambre, ou sur votre chaise à l'école ou à la bibliothèque.
Vous venez de lire le titre de cet article et vous vous demandez ce que vulnérable peut vouloir dire ici.
A l'occasion de la 5e journée internationale des filles, célébrée chaque année le 11 octobre, le JDE a voulu faire le point sur la situation des filles sur la planète.
Un très grand nombre d'entre elles est vulnérable, c'est-à-dire qu'elles peuvent être facilement atteintes par quelque chose de mauvais.
Elles peuvent être maltraitées, forcées à faire des choses dont elles n'ont pas envie.

Construire son avenir

Dans les pays en développement,1 fille sur 3 est mariée de force avant ses 18 ans.
116 millions de filles dans le monde sont exploitées et obligées de travailler.
Si une fille va à l'école, elle peut grandir normalement.
Elle n'est pas obligée de faire les travaux de la maison, ou pire, de se marier et d'avoir des enfants très jeune.
En apprenant à lire et écrire, elle comprend qu'elle a des droits.
En apprenant un métier, elle gagne de l'argent.
Et plus longtemps elle reste à l'école, plus élevé sera son salaire.
Gofran, 17 ans, vit dans un camp.
Elle aura moins d'enfants, et il sera plus facile d'élever et nourrir sa famille.
Elle produira des richesses p son pays, et pourra le faire part de ses talents.
Elle change l'avenir de son pays, et le sien.
Sala, une jeune réfugiée âgée de 6 ans, fait un exercice de maths dans la salle de classe à Bosso, Niger (Afrique).
Pourquoi sont-elles si vulnérables ? Parce que dans de nombreux pays, notamment les plus pauvres, les femmes, et encore moins les filles n'ont pas la même importance que les garçons.
Elles ne peuvent pas rapporter assez d'argent et sont une bouche à nourrir dont il faut se débarrasser, le plus souvent en les mariant ou en les vendant.
Envoyer les filles à l'école n'est souvent pas une priorité.
Résultat, 2 personnes sur 3 qui ne savent ni lire ni écrire dans le monde sont des femmes.