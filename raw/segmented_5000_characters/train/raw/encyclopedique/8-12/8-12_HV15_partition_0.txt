WHITE
Documents
COLORED
leurs écoles et n'ont pas le droit de fréquenter les boutiques, les cinémas, les restaurants réservés aux Blancs. Violences et injustices Presque 100 ans après l'abolition de l'esclavage, non seulement les populations ne se mélangent pas, mais elles n'ont pas les mêmes droits. Les Noirs ne votent pas car les tests de langue anglaise imposés par les États pour qu'ils puissent s'inscrire sur les listes électorales sont trop durs : il faut savoir lire et écrire. Beaucoup n'ont pas été scolarisés et ont du mal à faire des études, à trouver un travail bien payé. La vie d'un Noir n'a pas la même valeur que celle d'un Blanc. A cause de la couleur de leur peau, les Noirs sont sans cesse humiliés, persécutés et trop souvent victimes de terribles violences. De la fin du XIXe siècle aux années 1960, plus de 4000 Afro-Américains ont été lynchés.
Fontaines publiques, Caroline du Nord, en 1950. White pour Blancs et Colored pour personnes de couleur, chacun devait respecter les écriteaux !
De l'esclavage à la ségrégation
L'esclavage aboli, les Noirs travaillent comme ouvriers agricoles. Ici, des cueilleurs de coton vers 1900.
Le visage recouvert d'un capuchon pointu, les membres du Ku Klux Klan se rassemblent la nuit (ici, en 1948).
Martin Luther King naît dans une Amérique raciste où les Noirs, surtout dans le Sud, sont traités comme des êtres inférieurs.
Le Ku Klux Klan
Cette organisation ultra raciste, créée en 1865 dans le Tennessee, existe encore. Les membres du Ku Klux Klan considèrent que les Blancs sont des êtres supérieurs. A l'époque de Martin Luther King, cette société secrète voit d'un très mauvais ceil les victoires s obtenues par les défenseurs des Noirs et multiplie les assassinats.
La dure conquête de l'égalité En 1865, à la fin de la guerre de Sécession, l'esclavage est aboli sur tout le territoire américain. Le président, Abraham Lincoln, proclame l'égalité des droits pour tous. Pourtant, les Etats du Sud ne respectent pas la Constitution américaine et ils continuent à pratiquer la ségrégation. Les Noirs y vivent séparés des Blancs ils ont leurs quartiers, leurs églises,
BIEN ESCORTÉ ! En 1962, James Meredith est le premier Noir à s'inscrire dans une université du Mississippi, l'un des États les plus racistes des États-Unis. Non sans mal ! Après une nuit d'émeutes, 127 policiers l'accompagnent jusqu'en salle de cours !