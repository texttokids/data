Documents
Au musée Art Ludique, à Paris, tu peux admirer des centaines de dessins qui ont mené aux films Disney. Une expo féerique!
L'art de Disney
« Je sens que nous ne pouvons assurément pas faire de choses merveilleuses,
basées sur le réel, à moins de connaître d'abord le réel. »
Voici la citation de Disney qui nous accueille, à l'entrée de l'exposition L'art des studios d'animation Walt Disney le mouvement par nature au musée Art Ludique. À tous ceux qui ont grandi
bercés par la magie Disney, le musée propose de découvrir les coulisses incroyables du travail des animateurs. Avis aux artistes en herbe : vous allez en prendre plein les yeux!
Des centaines d'oeuvres De Blanche-Neige à Zootopie, de La Belle au bois dormant à Tarzan, la diversité des oeuvres exposées révèle la créativité des artistes qui ont travaillé pour les studios au fil des années. Autre point fort : des vidéos qui montrent l'évolution du travail, des premiers croquis aux films. Devant ces images, on se dit qu il y a vraiment quelque chose de magique dans les films des studios Disney. Une magnifique expo à voir en famille !
L'histoire du dessin animé Dans la salle réservée aux premiers pas de l'art de l'animation, tu verras à quoi ressemblaient les cartoons avant Disney. Ensuite, place aux héros du génie Walt ! Mickey bien sûr, mais aussi Donald ou Pluto, avec des premiers dessins qui ont mené à leur visage d'aujourd'hui. C'est tout l'intérêt de l'expo: découvrir les croquis, les peintures ou les sculptures qui ont servi à créer les héros et décors des célèbres dessins animés.
A voir, à lire Dans l'ombre de Mary, la promesse de Walt Disney. Mary Poppins est un roman anglais que tu peux lire en poche). Ce joli film raconte comment Disney l'adapta au cinéma en 1964.