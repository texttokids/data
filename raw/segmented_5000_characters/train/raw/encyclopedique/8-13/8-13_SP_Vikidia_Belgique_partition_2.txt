La bande dessinée belge est principalement représentée par Le Journal de Spirou, fondé à Marcinelle (ancienne commune, faisant aujourd'hui partie de celle de Charleroi) par Jean Dupuis en 1938, et Le Journal de Tintin, reprenant l’œuvre d'Hergé, lancé à Bruxelles en 1946 par les éditions du Lombard. Ces deux maisons d'édition concurrentes ont suscité une créativité littéraire exceptionnelle en Europe de l'Ouest depuis les années 1950, dans un domaine considéré jusque-là comme marginal.
Une dizaine d'auteurs belges se sont particulièrement fait connaître : Hergé (Tintin), Franquin (Marsupilami), Peyo (Les Schtroumpfs), Charlier (Barbe-Rouge, Blueberry, Tanguy et Laverdure), Ptiluc, Willy Vandersteen (Bob et Bobette), Midam (Kid Paddle), Salvé (Les Tuniques bleues), Cauvin (Cédric), Philippe Geluck (Le Chat).
Le projet de parcours BD de Bruxelles a démarré en 1991 : les grands auteurs de la BD belge, française, italienne et suisse ont vu certaines de leurs œuvres transformées en peintures murales gigantesques. On compte actuellement une quarantaine de fresques disposées de façon fantaisiste et attractive : un circuit-vélo permet d'en faire le tour.










L'architecture romane est représentée par des monuments défensifs comme le beffroi de Namur et le château de Bouillon ainsi que par des églises. La cathédrale de Tournai, très impressionnante, est principalement romane.
Le style gothique dans le domaine civil se retrouve dans plusieurs hôtels de ville, notamment ceux de Bruxelles, Bruges et Louvain, et dans les beffrois, tels ceux de Bruges, d'Ypres et de Gand. La Cathédrale Saints-Michel-et-Gudule de Bruxelles a été construite en plusieurs étapes et sa façade date de la fin du XVe siècle. La cathédrale Notre-Dame d'Anvers, de même que la cathédrale Saint-Bavon de Gand, sont des exemples de gothique brabançon.
La Renaissance a été influencée par la créativité italienne (Palais des Princes-Évêques de Liège) et a conservé les traditions flamandes (Grand place de Bruxelles). 
Le centre de Bruges est une destination touristique très prisée et mérite son nom, La Venise du Nord. Ville dynamique et étudiante, Gand conserve un centre ancien marqué par les « Trois Tours » (ou Drie Torens) du beffroi, de l'église Saint-Nicolas et de la cathédrale, ainsi que le château des Comtes de Flandre (Gravensteen), un ancien château-fort entièrement restauré. De même, la ville de Namur a gardé un certain cachet, mariant le style classique des maisons et le style baroque des monuments, par exemple l'université et la cathédrale.
Comme dans le reste de l'Europe, les constructions du XIXe siècle ont adopté le style néoclassique à la mode, sauf pour quelques exemples d'Art nouveau de qualité à Bruxelles ; les plus connus sont la Maison Art Nouveau sur l'Avenue Louise et Old England.




Le monument le plus connu de Belgique est sans doute le Manneken Pis, de son nom bruxellois Maenneke Pis. C'est une statue en bronze d'une cinquantaine de centimètres de hauteur située au cœur de Bruxelles. Il s'agit d'une fontaine qui représente un petit garçon en train d'uriner. Les mots maenneke pis signifient en bruxellois (dialecte flamand proche du néerlandais) « le môme qui pisse ». Moins connus, il y a également sa version féminine (Jeanneke Pis) et même canine (Zinneke Pis). On voit ici l'humour des Bruxellois ! Mais il y a aussi l'Atomium, relique de l'Expo 58, tout comme le théâtre américain qui s'est transformé en studio pour les télévisions et radios néerlandophones de Bruxelles.

-Atomium (image censurée : on n'a pas le droit de montrer l'Atomium !)


De nombreux chanteurs belges francophones sont devenus célèbres : Jacques Brel, Axel Red (d'origine flamande, mais qui chante en français), ou encore Adamo et Arno, lui aussi d'origine flamande. La scène musicale flamande est prolifique et de nombreux groupes, qui chantent souvent en anglais, sont connus hors des frontières belges : The Van Jets, Admiral Freebee, Ghinzu ou encore Hooverphonic comptent parmi les principaux.
C'est également le pays où le saxophone a été inventé, en 1846, par le Belge Adolphe Sax (wp).
Les peintres belges sont également très connus, depuis les Flamands de la Renaissance comme Rubens, jusqu'au peintre surréaliste René Magritte.









La peinture flamande se développe au début du XVe siècle, en effet les principaux peintres d'Europe du Nord sont flamands. Ces derniers travaillent dans les cours d'autres pays et ont influencé toute l'Europe. Les principaux peintres flamands sont Rubens, Van Dyck, Brueghel l'Ancien qui ne se contente pas de sujets religieux ou princiers, mais représente les gens du peuple, Van Eyck et Bosch, le spécialiste du fantastique.