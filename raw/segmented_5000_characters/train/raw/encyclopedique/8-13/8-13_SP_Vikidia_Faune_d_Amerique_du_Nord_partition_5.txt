-Une ourse polaire et son ourson, dans le Manitoba. L'ours polaire vit l'hiver sur la banquise arctique ; en été, lorsqu'elle fond, cherche refuge dans la toundra et dans le nord de la forêt boréale.


-Un ours noir, capturant un saumon rose, en Alaska.
-Un ours brun, capturant un saumon rose, en Alaska.


-Un groupe de saumons rouges remonte une rivière, dans l'Idaho.

-Une salamandre à points bleus, dans le Wisconsin.









La faune des forêts tempérées est sans doute la plus familière des Américains, puisque les animaux qui vivent dans les bois, les champs et les prairies s'aventurent aussi dans les parcs, et les jardins, comme le cerf de Virginie ou l'écureuil gris, et on peut les apercevoir même au cœur des grandes villes, comme le raton laveur.
Les animaux qui vivent dans la forêt sont souvent très connus, même ceux qui sont difficiles à observer, parce qu'ils sont étonnants, comme les écureuils volants, ou le porc-épic. Ils ont été popularisés par de nombreuses œuvres, comme le dessin animé Bambi, de Walt Disney, dont le héros est un faon de Virginie, et qui met en scène des animaux de la forêt, comme Pan-Pan, le lapin à queue blanche, ou encore Fleur, la mouffette.
Il existe en fait plusieurs types de forêts, en Amérique du Nord, comme la forêt tempérée humide, avec un climat plus humide et plus de pluie, sur la côte Pacifique, la forêt boréale, avec un climat plus froid, au Nord, et les forêts de montagne. Elles n'ont pas de frontières clairement établies entre elles, c'est pourquoi beaucoup d'animaux des forêts tempérées peuvent aussi se retrouver dans d'autres milieux.
-Une biche de Virginie, dans l'état de Washington.

-Le raton laveur cherche parfois sa nourriture au bord de l'eau, ce qui a fait croire qu'il la « lavait » avant de la manger, et lui a valu son nom de raton-laveur.



-Un écureuil roux américain, dans la Baie du Saint-Laurent, au Québec.
-Un écureuil de Douglas, dans le parc national du mont Rainier.




-Un lapin à queue blanche, en Colombie-Britannique.

Les animaux arboricoles ont besoin des arbres pour vivre, on les trouve donc dans les forêts. Il existe plusieurs espèces d'écureuils en Amérique du Nord, le plus connu est l'écureuil gris, qui a été introduit dans d'autres régions du monde, comme l'Europe, où il est aujourd'hui devenu très courant, mais il en existe d'autres, comme l'écureuil roux américain, ou l'écureuil du douglas. Le petit polatouche et le grand polatouche sont des écureuils volants, qui se déplacent en planant d'arbre en arbre. Le porc-épic d'Amérique est également arboricole, il se réfugie dans les arbres, où il trouve nourriture et protection. Ce gros rongeur n'est pas un proche cousin du porc-épic d'Afrique, mais comme, comme lui, il a des piquants sur le dos pour se protéger des prédateurs, il lui ressemble. Même les oursons sont capables de grimper aux arbres quand ils sont petits !
On trouve bien sûr beaucoup d'oiseaux dans les forêts américaines. La plupart des états des États-Unis, et des provinces du Canada, choisissent un oiseau comme emblème. Plusieurs espèces de pics vivent en Amérique du Nord, dans des habitats très variés, mais la plupart, comme le pic à ventre roux et le grand pic, se nourrissent d'insectes vivant dans le bois, qu'ils attrapent en creusant de petits trous à l'aide de leur bec. Ils font également leur nid dans un trou dans un arbre : rien d'étonnant donc à ce qu'ils vivent en forêt ! Le pic glandivore est une exception : il ne se nourrit pas d'insectes, mais principalement de glands de chêne. Le geai buissonnier mange également des glands, il en fait des provisions pour l'hiver. Parfois, il vole ceux du pic...
-Une mésange à tête noire, en Colombie-Britannique.