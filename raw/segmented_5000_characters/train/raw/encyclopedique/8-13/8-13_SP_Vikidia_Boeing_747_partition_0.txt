Le Boeing 747, aussi surnommé Jumbo Jet, est un avion de ligne développé par Boeing depuis 1965.
Il possède, comme l'Airbus A380, 2 étages ; cependant, ces étages ne s'étendent que sur une petite partie de l'appareil, ce qui lui confère une configuration de 366 à 524 places. Il s'agit d'un avion de mythe, car il possède une longue histoire.
Il a longtemps été le plus gros long-courrier de l’histoire de l’aviation, maintenant remplacé par l’Airbus 380.


Dans les années 60, la compagnie Pan American World Airways, l'une des plus grandes compagnies aériennes, demande à Boeing de construire un avion plus gros que le Boeing 707, qui était déjà très grand pour l'époque. De plus, le trafic aérien étant en augmentation, il fallait construire des avions plus grands que les célèbres Boeing 707 ou encore le Douglas DC-8, qui connaissaient un grand succès commercial.
Il fallait pour cela une usine plus grande pour construire un tel appareil. Boeing se lance donc dans la construction d'une nouvelle usine, c'est la plus grande du monde, de plus, afin de livrer les 747 déjà commandés à temps, Boeing n'avait que 4 ans pour construire un tel appareil, la construction débute avant même que la construction de l'usine soit terminée, et la construction ne sera terminée qu'au bout de seulement 16 mois grâce aux 50 000 employés de Boeing.
Son premier vol a eu lieu en 1969, mais les compagnies aériennes ne semblent pas être attirées. Boeing pensait que l'avion, qui connaîtrait le succès, sera son futur avion supersonique qu'il avait commencé à étudier. Mais le manque d'argent et sa très forte consommation en carburant l'avaient mis dans l'impasse, et le projet se termina par une simple maquette grandeur nature. Mais finalement, le jumbo connaîtra le succès.




Le Boeing 747-100 fut la première version de l'avion, il a fait son premier vol le 9 février 1969. Il était à l'époque, le plus grand avion jamais construit. Son premier vol commercial (c'est-à-dire avec des passagers à bord) a été assuré par la compagnie Pan Am en janvier 1970. Cette version était reconnaissable avec ses trois hublots au 2e étage, contenant souvent un bar.
Une version un peu améliorée a par la suite été construite, le Boeing 747-100B, elle possède plus de hublots sur le 2e étage, ce qui la rend plus adaptée pour y mettre des passagers.


En 1972, Boeing met en service le Boeing 747-200, un appareil encore plus grand, avec des réacteurs plus puissants, les réservoirs de carburant encore plus grands, ce qui lui permettait de parcourir de plus longues distances. Une version cargo de cet avion a également été conçue. On peut reconnaître le Boeing 747-200 avec les huit hublots du deuxième étage.


Le Boeing 747SP (pour Special Performance : « performance spéciale ») est une version spéciale du Boeing 747 conçue en 1976. En effet, le 747SP pouvait parcourir de très longues distances (environ 12 000 km) et était très rapide (vitesse maximale : 1 130 km/h). Il était également très raccourci, cela lui permettait d'aller plus vite, mais ne pouvait transporter qu'environ 220 passagers. Le but de cette version était de concurrencer le McDonnell Douglas DC-10 avec un avion ayant des performances similaires.
Cette version fut une réussite technique, mais pas commerciale, seulement 44 exemplaires ont été vendus.


En 1980, Boeing construit le Boeing 747-300, le deuxième étage était plus grand, et pouvait être accessible par un escalier droit, et non pas en spirale comme sur les versions plus anciennes, Swissair fut la première compagnie à utiliser le Boeing 747-300.


Le Boeing 747-400 a fait son premier vol en 1988, celui-ci était la plus grande avancée pour le Boeing 747, de nombreux appareils de ce type ont été construits, y compris deux versions cargos, 694 exemplaires furent construits depuis 1993. Celui-ci a un cockpit plus moderne, avec de nombreuses écrans, contrairement aux anciennes versions, qui n'avait pas d'écran, il existe aussi deux versions cargo de cette avion, le Boeing 747-400F, ainsi que le Boeing 747-400BCF (dont la longueur du deuxième étage est la même que la version passager). Il possédait aussi au bout des ailes ce que l'on appelle des winglets, cela lui permet de mieux voler et de moins consommer en kérosène.
Un Boeing 747-400 décolle en moyenne à 290 km/h, peut parcourir 12 900 km.


En 2005, lorsque le Boeing 747 est surpassé par l'Airbus A380, plus grand et plus économique que le Boeing 747-400, Boeing décida revoir la conception de son avion à partir de zéro, ce nouveau 747 n'est pas aussi grand, mais est très économique, en effet, il consomme moins que l'A380.
Son premier vol a eu lieu en 2011, et son premier vol commercial a été effectué par la compagnie Lufthansa en 2012. Malheureusement, très peu de compagnies ont l'intention d'acheter un tel appareil, et il semblerait que ce soit la fin du Boeing 747. Cependant, sa version cargo a un bien meilleur succès.