Le premier titre de Kylian Mbappé est la Coupe Gambardella 2016, remportée avec l'AS Monaco U1939. Il termine ensuite premier de la Ligue 1 2016-2017 avec les Monégasques, auteurs d'une saison record avec 95 points engrangés et 107 buts étalés sur 37 des 38 matchs40, et réalise également une belle aventure en Ligue des champions stoppée en demi-finales par la Juventus de Turin41.
Arrivé au Paris Saint-Germain, Mbappé étoffe son palmarès d'une deuxième Ligue 1, consécutive à celle remporte avec Monaco, mais aussi d'une Coupe de la Ligue et d'une Coupe de France.


En équipe de France des moins de 19 ans, Kylian Mbappé est sacré champion d'Europe en 2016. Il fait partie de l'équipe de France A qui remporte la Coupe du monde 2018 et totalise 22 sélections pour 8 buts avec cette équipe14.


Kylian Mbappé a déjà reçu de nombreuses distinctions individuelles :
- Meilleur espoir de la Ligue 1 2016-2017 et 2017-2018
- Joueur du mois de Ligue 1 en avril 2017, mars 201842 et août 2018.


- Quatrième Meilleur footballeur de l'année FIFA.


-Vainqueur du 1er trophée Kopa en 2018 du meilleur U21 
-Meilleur joueur français de l'année 2018 d'après France Football


- Plus jeune Français à disputer un match de Coupe du monde (19 ans et 5 mois)43
- Plus jeune Français à marquer en Coupe du monde (19 ans et 6 mois)
- Premier joueur né après la victoire de la France dans la Coupe du monde 1998 à être sélectionné par cette équipe
- Premier joueur né après la victoire de la France en 1998 à marquer en Coupe du monde
- Deuxième plus jeune joueur à marquer quatre buts en Coupe du monde (19 ans et 6 mois), derrière Pelé (17 ans et 8 mois)
- Deuxième plus jeune joueur à marquer un doublé en Coupe du monde (19 ans et 6 mois), derrière Pelé (17 ans et 8 mois)
- Joueur Français ayant tenté le plus de dribbles en un match de Coupe du monde depuis 1966 (15, sept réussis)
- Deuxième plus jeune buteur d'une finale de Coupe du monde (19 ans et 6 mois), derrière Pelé (17 ans et 8 mois)
- Deuxième plus jeune buteur Français en Ligue des champions44 (18 ans et 5 mois)
- Deuxième joueur de football le plus cher de l'histoire (180 millions d'euros)
- Plus jeune joueur à marquer dix buts en Ligue des champions (18 ans et 11 mois)
- Plus jeune buteur de l'histoire des demi-finales de Ligue des champions45 (18 ans et 4 mois)
- Plus jeune joueur à marquer un but de chaque pied en quart de finale de Ligue des champions (18 ans et 3 mois)
- Plus jeune buteur du PSG en Ligue des champions (18 ans et 8 mois)
- Deuxième plus jeune buteur du PSG en Ligue 1 (18 ans et 8 mois)
- Plus jeune buteur pour deux clubs différents en Ligue des champions (18 ans et 8 mois)


Kylian Mbappé est un personnage jouable dans le jeu vidéo FIFA 18, dans le mode de jeu Ultimate Team46. En 2012, alors qu'il n'est âgé que de 14 ans, Mbappé signe son premier contrat et touche une prime de 400 000 euros47. Lors de sa saison 2016-2017 avec l'AS Monaco, le salaire annuel de Kylian Mbappé était d'environ 1 million d'euros48. Avec le PSG, ces revenus explosent pour atteindre une somme vertigineuse de 17,5 millions d'euros gagnés sur la saison 2017-201849. Malgré de nombreuses propositions, Kylian Mbappé n'a choisi qu'un unique sponsor : son équipementier Nike.