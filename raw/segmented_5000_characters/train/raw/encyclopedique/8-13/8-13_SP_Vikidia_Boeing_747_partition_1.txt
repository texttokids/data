Aux États-Unis, un Boeing 747-200 est utilisé pour transporter le président, l'intérieur possède des aménagements de luxe et pour des raisons de sécurité, des avions de chasse escortent l'appareil présidentiel lors du survol de certaines zones. En 2018, le Boeing 747-200, assez ancien, sera remplacé par un Boeing 747-8. D'autres pays possèdent également des 747 pour transporter leur chef d'état, c'est le cas du Japon et du Qatar.


Le Dreamlifter est un dérivé du Boeing 747 servant à transporter les pièces d'avions à Everett pour les assembler, car le transport par bateau est trop long. Boeing décida alors de prendre 3 Boeing 747-400 ordinaires pour les agrandir, afin qu'ils fussent assez grands pour transporter les pièces. Celui-ci est comparable au Beluga d'Airbus.





Cette version très spéciale, dérivée du Boeing 747-100 permet de transporter la navette spatiale, celui-ci était utilisé par la NASA afin de la tester en vol, l'avion transporte la navette jusqu'à une certaine altitude, puis, elle se détache, et peu ainsi permettre de voir si la navette peut voler une fois rentrée dans l'atmosphère, puis se poser. Au départ, il s'agissait d'un 747 normal, sur lequel des modifications ont été apportées en 1977. Un deuxième avion de ce type a été créé en 1990.
Le Shuttle Carrier Aircraft avait comme concurrent le Lockheed C-5 Galaxy, mais la NASA préférait utiliser un Boeing 747 car elle avait l'habitude de prendre les avions de Boeing. De plus, elle disposait déjà d'un 747.


Le poste de pilotage du Jumbo a bien évolué depuis la sorti de l'avion. Les premiers n'avaient pas d'instruments électroniques, et donc, possédaient beaucoup plus d'instruments, ce qui rend la préparation de l'avion plus difficile. L'équipage contenait 3 personnes, le pilote, le copilote et le mécanicien.
Dans le Boeing 747-400, les instruments de pointe commencent à apparaitre, dans l'équipage, il n'y a plus que 2 personnes, et une partie des instruments est remplacée par des écrans, dans le Boeing 747-8, les instruments ont encore été un peu améliorés.


L'avion a connu plusieurs catastrophes aériennes, mais la plupart était dues à des erreurs de pilotage, et non à des problèmes de conception. Les accidents connus par le 747 ont généralement eu lieu avant l'an 2000.


- Le 27 mars 1977 : Deux Boeing 747 ont eu de nombreuses confusions avec la tour, un Boeing 747-200 de KLM Royal Dutch Airlines met plein gaz pour décoller, au même moment, un Boeing 747-100 de la Pan Am traverse la piste, les deux avions se percutent, causant 583 victimes, 61 personnes ont survécu au crash.
- Le 4 octobre 1992 : Un Boeing 747-200 (version cargo) de la compagnie israëlienne El Al s'écrase à Amsterdam, suite à la perte de deux des moteurs. Il y aura 43 victimes, dont 4 à bord, et 39 au sol.
- Le 17 juillet 1996 : Un Boeing 747-100 de la TWA explose en vol, quelques minutes après le décollage de New York. L'accident aura fait 230 victimes.


Plusieurs Boeing 747 ont été exposés dans des musées, en voici la liste :
- Le premier Boeing 747 : Exposé à Seattle, aux États-Unis
- Boeing 747-200 de la KLM : Exposé à Lelystad, aux Pays-Bas
- Boeing 747-200 de la Qantas : Exposé à Longreach, en Australie
- Boeing 747-200 de la South African Airways : Exposé à Johannesburg, en Afrique du Sud
- Boeing 747-200 de la Lufthansa : Exposé à Speyer, en Allemagne
- Boeing 747-100 d'Air France : Exposé à Paris, en France
- Boeing 747-200 (cargo) d'Iran Air : Exposé à Téhéran, en Iran
- Boeing 747-200 de la Korean Air : Exposé à Jeju, en Corée du Sud
- Boeing 747-100 de la Northwest Airlines : Exposé à Washington, aux États-Unis