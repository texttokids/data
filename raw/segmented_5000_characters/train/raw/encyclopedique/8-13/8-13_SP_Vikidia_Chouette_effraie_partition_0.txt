La chouette effraie (nom scientifique : Tyto alba) est une espèce de chouette commune (c'est l’espèce de strigiforme la plus répandue au monde). Comme les hiboux et les autres chouettes, c'est un animal nocturne. Il est donc plutôt rare de voir cette chouette pendant la journée, car la peur de se faire agresser par des corvidés est assez puissante pour la dissuader de quitter son nid. On la surnomme aussi "dame blanche" ou "chouette des clochers", en France. C'est un animal plutôt sédentaire, bien que quelques cas de migrations aient été observés chez de jeunes individus.


Elle vit dans d'anciens bâtiments comme des granges, des ruines, des clochers ou encore des églises et plus rarement dans des troncs d'arbres creux. Le sol du nid est recouvert de pelotes de réjection. La disparition de ces bâtiments ou leur rénovation provoque la réduction des lieux de nidification. Certains clochers deviennent inaccessibles lorsqu'ils sont fermés par un grillage. 
On retrouve ces chouettes partout en Europe et dans le monde, sauf près du Cercle polaire arctique et en Antarctique, ce qui explique la diversité des 28 sous-espèces. Il existe trois sous-espèces en France, dont une qui n'est présente qu'en Corse. La chouette effraie est l'espèce la plus répandue dans le monde parmi les strigiformes.


Son disque facial en forme de cœur est entièrement blanc (cela peut varier selon les sous-espèces : certaines ont un disque facial brun roux et blanc, voire entièrement brun) et entouré de plumes brun chamois à blanches, lui permettant d'augmenter son audition. Ses yeux sont noirs (cas plutôt rare chez les rapaces), le manteau est brun clair et chamois avec des parties gris-bleu. Son poitrail est blanc duveteux avec des reflets roux. Ses rémiges, ainsi que ses rectrices, sont blanches. Comme toutes les chouettes (et contrairement aux hiboux), elle ne possède pas de touffes auriculaires. 
Sa longueur est d'environ 32 à 35 cm. On compare parfois sa taille à celle d'un pigeon. Le poids (et la longueur) varient selon les sous-espèces (il en existe 28) allant de 250 g à 600 g. La femelle est plus grande et plus lourde que le mâle, ce qui est un phénomène courant chez les rapaces.


Grâce à son ouïe exceptionnelle, la chouette effraie peut repérer une proie à plusieurs dizaines de mètres et la capturer sans difficulté. Sa vue est bonne, mais insuffisante pour chasser. Une femelle borgne a été observée en train de chasser pour sa couvée, ce qui montre que la vue n'est pas chez elle le sens le plus important. Ses oreilles, cachées sous les plumes, ne sont pas placées à la même hauteur.


La chouette effraie pousse des cris ressemblant à « krûh » ; elle émet aussi des chuintements : elle chuinte par conséquent, et elle claque de son bec lorsqu'elle a peur ou qu'elle est énervée. On compare parfois son cri aux ronflements d'un dormeur. Attention, contrairement à une opinion répandue, la chouette effraie ne hulule pas !


Comme tous les rapaces (c'en est un), elle a un vol silencieux, grâce à un « peigne » qui termine certaines de ses plumes. Son plumage duveteux et dense absorbe les bruits et son vol ne peut être entendu par un humain à plus de deux mètres. Son vol silencieux lui évite de devenir une potentielle proie. Les dentelures situées au bout de ses ailes ont inspiré des modèles de ventilateurs et d'engins rotatifs particulièrement silencieux. Son vol lent et souple lui donne une allure fantomatique.  
Des observations ont montré que la chouette effraie a les pattes pendantes lorsqu'elle ralentit ou fait de grands battements d'ailes en restant sur place.


La chouette effraie se nourrit principalement de petits rongeurs (campagnols, musaraignes, mulots et souris), ou plus rarement de lapins, de belettes, de petits oiseaux, de rats et d'amphibiens, ainsi que de chauves-souris et d'insectes qu'elle avale entiers, avant de recracher les morceaux non digestibles (os, plumes, poils) sous forme de pelotes de réjection noires, de forme ovale. La base de son alimentation est essentiellement composée de muridés et de campagnols.