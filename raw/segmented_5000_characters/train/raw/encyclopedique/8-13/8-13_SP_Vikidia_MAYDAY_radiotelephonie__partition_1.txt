- le maximum de renseignements pouvant être utiles aux services de secours ;
- le trafic de détresse ne doit jamais être gêné par un autre bateau.



Le capitaine d'un navire en mer qui reçoit, de quelque source que ce soit (y compris d'une radiobalise pour la localisation des sinistres en mer), un message indiquant qu'un navire ou un aéronef ou leurs embarcations et radeaux de sauvetage se trouvent en détresse est tenu de se porter à vitesse maximale au secours des personnes en détresse et de les en informer, si possible. En cas d'impossibilité ou si, dans les circonstances spéciales où il se trouve, il n'estime ni raisonnable ni nécessaire de se porter à leur secours, il doit inscrire au journal de bord la raison pour laquelle il ne se porte pas au secours des personnes en détresse.
Ce qui donne sur le même canal radioélectrique des appels de détresse reçus par exemple :
-MAYDAY de Bélougas Bélougas Bélougas DE Carpathia Carpathia CarpathiaMAYDAY reçu Carpathia part à vitesse maximale sera à la position Longitude 3° Ouest Latitude 46° Nord dans 2 heures.Le relais de votre appel est reçu sur la fréquence de 2 182 kHz par le CROSS CORSEN NB 2 A vous


Le capitaine d'un navire peut faire usage des trois répétitions du mot MAYDAY RELAIS en radiotéléphonie seulement dans l'un des trois cas suivants : 
- pour signaler qu'un autre navire ou un aéronef est en détresse si celui-ci n'est pas en mesure de le signaler lui-même ;
- pour demander des secours supplémentaires lorsque, s'étant porté à l'aide d'un navire ou d'un aéronef en détresse, il juge ces secours nécessaires ;
- pour répéter un appel de détresse dont aucun autre navire ou station côtière n'a accusé réception immédiatement, lorsqu'il est dans l'impossibilité de se porter lui-même au secours du navire ou de l'aéronef en détresse.




Quand le capitaine d'un navire qui a émis un signal de détresse estime ultérieurement que l'assistance n'est plus nécessaire, ou qu'il n'y a plus lieu de donner suite au message, il doit immédiatement le faire savoir à toutes les stations intéressées sur le canal déjà utilisé pour trafic de détresse.






Depuis 1947, dans les secteurs de couleur vert ou bleu des montres radio-marine, les stations radiotéléphoniques effectuent un silence radio obligatoire de trois minutes deux fois par heure, de H + 00 à H + 03 et de H + 30 à H + 33 en temps universel coordonné, cette disposition ne s'applique pas aux stations en détresse. Toutes les stations des paquebots effectuant des trajets internationaux, les navires de pêche en haute mer et les navires de charge supérieurs à 300 tonneaux effectuant des voyages internationaux et toutes les stations côtières marines doivent cesser la radiotéléphonie, puis aller en écoute obligatoire sur la fréquence de 2 182 kHz 10 ’ 11, afin de repérer un éventuel signal de détresse arrivant même très faible, non perçu durant le reste du temps utilisé pour les appels divers avec un dégagement sur une autre fréquence: appel général de routine (CQ), appel individuel de routine, appel d'urgence (PANPAN) et appel de sécurité (sécurité).


Chaque année, les centres de coordination des opérations de sauvetage (RCC), en France les centres régionaux opérationnels de surveillance et de sauvetage (CROSS) enregistrent jusqu'à 10 % de faux appels de détresse. Il y a parfois des personnes de bonne foi suite à des erreurs diverses (donc l'exemple fréquent: l'interrupteur de détresse en position mode automatique ou en mode balise ou suite à un choc violent subit par une balise aéronautique, mais il y a aussi des actes délibérés de malveillance punis en France d'une peine pouvant aller jusqu'à 2 ans d'emprisonnement et 30 000 euros d'amende pour une fausse alerte de nature à provoquer l'intervention inutile des secours 12.



Le signal d'urgence : PANPAN PANPAN PANPAN prononcé comme le mot français panne panne est une urgence d'un navire ou d'un aéronef ou d'une personne quelconque se trouvant à bord ou en vue du bord. Le signal d'urgence PANPAN MAY-DEE-CAL PANPAN MAY-DEE-CAL PANPAN MAY-DEE-CAL, prononcé comme le mot français «médical», en radiotéléphonie est une urgence médical. (Dans un navire, dans un aéronef, une personne qui va bientôt mourir, c'est de l'urgence médical). Ou l'expression «transports sanitaires» recouvre tout moyen de transport, par terre, par eau ou par air, militaire ou civil, permanent ou temporaire, affecté exclusivement au transport sanitaire placé sous la direction d'une autorité compétente d'une partie à un conflit ou d'États neutres et d'autres États non parties à un conflit armé, lorsque ces navires, ces embarcations et ces aéronefs portent secours aux blessés, aux malades et aux naufragés. Le signal d'urgence PANPAN NEUTRAL est un transport neutre dans une zone de combats, le navire ou l'aéronef est placé sous la direction d'un État neutre à un conflit.