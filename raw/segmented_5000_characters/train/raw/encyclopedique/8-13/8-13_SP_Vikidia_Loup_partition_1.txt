Le loup n'est pas solitaire. Il vit et chasse en groupe (dit clan ou meute). La meute est composée de 8 à 20 loups.
La meute est composée de deux parents, et de leurs petits. On a longtemps cru que la meute formait un groupe hiérarchisé, car les deux parents sont les seuls à se reproduire, et ce sont les jeunes qui mangent en priorité. En réalité, ces règles sont là pour assurer la survie de l'espèce et empêcher l'inceste. Les études qui avaient aboutis à la conclusion d'une hiérarchie dans la meute était en réalité réalisées par des nazis, qui calquaient leur idéologie sur ce qu'ils pensaient être le mode de vie des loups. Le terme de "loup alpha" avait été introduit par le Dr. David L Mech, mais celui-ci est revenu dessus, avouant que le terme était inexact. Une vidéo Youtube est disponible à ce sujet : "Alpha" Wolf ?
Les signaux dits de "soumission" (lorsque les loups en lèchent d'autres sur les joues, ou se mettent sur le dos) sont des signaux utilisés pour éviter les conflits. La dominance, chez les loups (comme chez les chiens), ne résulte pas d'une hiérarchie stable, mais plutôt d'une situation (un loup A peut dominer B, et C peut dominer A dans une certaine situation, mais dans une autre cette boucle ne sera pas forcément la même). C'est pourquoi le terme de hiérarchie est erroné chez les loups. 
Le père loup et sa femelle restent en couple toute leur vie ; eux seuls ont des petits. Un louveteau devenu adulte peut choisir de quitter la meute, pour en fonder une nouvelle. 
Le clan a aussi un territoire : les loups le délimitent avec leur odeur et leurs excréments4 et aucun loup extérieur au clan n'a le droit d'y pénétrer. La nécessité de ce territoire très vaste explique ses conflits avec l'homme.


Les loups sont en âge de se reproduire vers 22 mois.
Après avoir préparé l'arrivée de ses louveteaux au début du printemps (aménagement d'une tanière avec des poils et des herbes sèches), la louve en met au monde de quatre à sept. Les louves plus jeunes ne font naître que de un à trois louveteaux.
Le petit du loup, communément appelé « louveteau », porte plus précisément différents noms en fonction du stade de sa croissance : 



Tout fragile, il naît sourd et aveugle. C'est sa mère qui chasse et lui rapporte de la viande. 
Adulte, il a la taille d'un gros chien et pèse environ 40 kg. Il vit entre 8 et 16 ans ; le record de longévité en captivité est de 20 ans.


Il y a des loups essentiellement dans l'hémisphère Nord. Selon la région, les loups sont quelque peu différents.
Par exemple, au nord, le pelage des loups est blanc. Il est adapté à son environnement : le loup peut mieux se camoufler dans un paysage de neige, surtout quand il souhaite chasser.
Si l'on compte aussi tous les loups domestiques, qui sont plus connues sous le nom de... chien, l'espèce est présente dans presque tous les pays du monde, partout où vit l'homme.
Le dingo est une sous-espèce de loup qui vit en Australie. C'est la seule sous-espèce sauvage de loup de l'hémisphère sud. Les dingos sont les descendants d'anciens chiens domestiques qui accompagnaient des hommes ayant abordé l'Australie il y a environ 4 000 ans, et qui depuis sont redevenus sauvages.


Longtemps, les loups ont été très craints des hommes. Ils étaient très nombreux à vivre au sein de grandes forêts, et il est arrivé qu'ils s'attaquent également à l'homme. Les jeunes enfants, qui à la campagne, étaient souvent chargés de garder les troupeaux, en étaient les principales victimes. Du coup, les hommes inventèrent de redoutables pièges, les chassèrent au fusil et en tuèrent beaucoup.
De nos jours, les loups ne sont pas très dangereux pour l'homme. Ils font encore parfois peur parce qu'avec leurs grandes gueules, ils peuvent dévorer du petit au gros ; aussi à cause de l'existence de contes anciens (comme le célèbre Petit Chaperon rouge), etc.. 
En revanche, ils ont bien failli disparaître totalement d'Europe et on a décidé de les protéger en 1990. Les loups sont revenus en France dans certains massifs de montagne comme les Alpes ou les Vosges et le massif central. Ils n'ont pas été réintroduits mais ont recolonisé ces régions à partir de populations de loups d'Italie. De nombreux éleveurs de brebis se plaignent maintenant d'attaques sur leurs troupeaux et souhaitent voir leur population maîtrisée.


C'est probablement une race de loup qui fut l'un des premiers animaux domestiqués par l'homme à la Préhistoire. Les descendants de ces premiers loups apprivoisés sont devenus les différentes sous-espèces d'animaux de compagnie communément appelées « races de chiens ».