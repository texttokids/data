Il existe 319 espèces de colibris en Amérique. Ces oiseaux, qui doivent battre des ailes très vite pour se maintenir en vol, ont besoin d'énormément d'énergie, qu'ils tirent du nectar des fleurs, un aliment très sucré. C'est la raison pour laquelle ils ont du mal à supporter le froid, et on les trouve principalement en climat tropical, en Amérique centrale et du Sud ; cependant, 21 espèces peuvent tout de même se rencontrer en Amérique du Nord, et, pour quatre d'entre elles, même au Canada ! Le colibri roux est celui qui vit le plus au nord : on peut l'observer jusqu'en Alaska.


-Une jeune femelle colibri à gorge noire, en train de butiner les fleurs, en Californie







On trouve en Amérique du Nord de nombreux serpents, notamment beaucoup d'espèces de petites couleuvres, comme le serpent des blés, le serpent-jarretière, ou les serpents-rois. Très colorés et parfaitement inoffensifs, ils sont souvent élevés en terrarium comme animaux de compagnie.
La couleuvre d'eau est un serpent aquatique. Excellente nageuse, elle chasse dans l'eau des poissons et des grenouilles. Sa couleur foncée lui sert de camouflage dans les mares où elle vit. Malheureusement, elle ressemble beaucoup à un autre serpent aquatique américain, le mocassin d'eau, qui, lui, est très dangereux : c'est pour cette raison qu'elle est souvent tuée par erreur.
Le mocassin d'eau est l'un des serpents les plus venimeux d'Amérique du Nord, et même du monde. En anglais, il est également appelé cottonmouth, c'est-à-dire « bouche de coton », car, lorsqu'il est dérangé, ce serpent ouvre grand la gueule, pour montrer l'intérieur de sa gorge, qui est blanche, comme un avertissement. 
Les crotales sont de proches cousins du mocassin d'eau. Comme lui, ils sont très venimeux, et peuvent être dangereux pour l'homme. On en rencontre plusieurs espèces en Amérique du Nord, comme le crotale diamantin, et, surtout, le crotale du Texas, l'un des plus courants, et qui est responsable d'un grand nombre d'accidents.




-Une autre, qui vient de capturer un poisson-chat, en Caroline du Nord.
-Une couleuvre à collier américaine, en Californie.
-Une couleuvre rayée (également appelée serpent-jarretière), dans l'Ohio




Contrairement à ce que son nom indique, le crapaud cornu n'est pas un amphibien, mais plutôt un lézard. Il en existe plusieurs espèces en Amérique du Nord. On rencontre aux États-Unis et au Mexique plusieurs espèces d'iguanes, comme l'iguane du désert ou le « chuckwalla ». Le monstre de Gila est l'un des rares lézards venimeux au monde. Même s'il n'est pas vraiment très dangereux pour l'homme, cette particularité l'a rendu célèbre, et il a même fait l'objet de films d'horreur !
L'alligator du Mississippi est l'un des plus grands crocodiliens au monde, avec le crocodile du Nil, le crocodile marin, et le caïman noir. Il vit dans les zones humides du sud-est de l'Amérique du Nord, notamment dans le Bayou et les Everglades. Mais il n'est pas le seul en Amérique du Nord, et on peut également apercevoir certains de ses cousins, plus petits, comme le crocodile américain, ou le crocodile de Morelet.
L'Amérique du Nord compte également plusieurs espèces de tortues d'eau douce, parfois très colorées, comme la tortue de Floride, la plus connue, qui a été importée un peu partout dans le monde, comme animal de compagnie, et qui est finalement devenue une espèce invasive... Vivant dans les forêts, la tortue boîte n'est pas une « vraie » tortue de terre : c'est plutôt une cousine des tortues d'eau douce. La tortue du désert est une tortue terrestre rare, et menacée, qui vit dans les zones arides du sud du continent.
-Un monstre de Gila, la nuit, dans son milieu naturel, en Arizona.

-Un jeune alligator du Mississippi, dans les marais d'Okefenokee, en Géorgie.


-Une tortue de Floride, ou tortue à oreilles rouges.






-Une tortue à ventre rouge (la plus grosse, à gauche), et une tortue peinte (à droite).
-Une tortue de Nelson (à gauche), et une tortue à carapace molle (à droite, dans l'eau), en Floride.
-Une tortue musquée, dans l'Ontario. Lorsqu'elle est dérangée, cette tortue émet une odeur très forte et repoussante !