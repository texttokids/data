Le sud du Massif central est partagé entre des régions de roches cristallines et de roches calcaires.
Les Cévennes forment une montagne granitique culminant au mont Lozère, à 1 702 m. Elles se terminent au sud-est par un brusque escarpement taillé dans les schistes, qui est très fortement raviné par les violentes averses d'automne du climat méditerranéen que connaît cette région.
Le Ségala qui s'étend au sud-ouest est aussi formé dans des terrains cristallins, mais son altitude est moins importante (de 1000 à 1200 mètres)
Les Grands Causses sont logés entre les Cévennes et le Ségala. Ce sont des plateaux s'élevant entre 800 et 1200 mètres. Dus à l'empilement de couches calcaires, les Grands Causses offrent un catalogue complet des formes du relief karstique avec ses gouffres (les igues et les avens ou sotchs), ses petites dépressions (les dolines ou sotchs). L'eau est très rare en surface et la végétation forme une maigre pelouse avec des genévriers. Les rivières alimentées par les pluies et les neiges des Cévennes y ont creusé des vallées très profondes et très étroites : les gorges (comme celles du Tarn). C'est une région difficile à vivre, aussi bien en été qu'en hiver, où il fait très froid.


Les paysages du Massif central sont marqués par une faune et une flore très riches.


On trouve dans le Massif central un grand nombre de plantes, dont certaines sont rares :
- dans les tourbières, on peut trouver des plantes carnivores, comme le droséra, notamment au plateau de Millevaches ;
- les hauts sommets abritent des espèces qui vivent là depuis la dernière période glaciaire, comme le saule arctique ou la linaigrette.















Le Massif central est une montagne très ancienne, qui ne s'est pas formée en une seule fois. La plus grande partie du Massif central, cependant, est ce qui reste d'une montagne encore plus grande, qui a aujourd'hui disparu.
Il y a environ 300 millions d'années, au Permien, une immense chaîne de montagnes, la Chaîne hercynienne, traversait ce qui est aujourd'hui l'Europe. C'était une très grande chaîne de montagnes, qui devait ressembler à l'Himalaya aujourd'hui. À cette époque, les Alpes et les Pyrénées n'existaient pas encore.
Au cours du temps, la pluie, la neige, le vent, ont fini par user petit à petit cette chaîne de montagne, qui a fini par disparaître : c'est l'érosion. Aujourd'hui, ce qui reste de cette vaste chaîne de montagnes est divisé en petits massifs, comme le Massif armoricain, ou les Vosges… Le plus vaste de tous ces massifs est bien sûr le Massif central, qui occupe aujourd'hui le centre de la France.
Le niveau de la mer a beaucoup varié au cours du temps : à plusieurs reprises, il a été beaucoup plus haut qu'aujourd'hui, si bien que ce qui constitue aujourd'hui l'Europe était recouvert par la mer. Les montagnes qui existent sont les seules à dépasser au-dessus du niveau de la mer et forment des îles. À plusieurs reprises, notamment au Crétacé, (il y a environ 80 millions d'années), le Massif central est devenu un archipel ! Durant ces périodes où le niveau de la mer est très haut, du calcaire se dépose dans le Massif central et forme les grands plateaux que l'on appelle les causses.


Il y a environ 35 millions d'années, un grand épisode volcanique se déroule dans l'est et le nord du Massif central : plusieurs volcans voient le jour. Il en existe deux types :
- des volcans qui créent des cônes hauts et abrupts, appelés puys ;
- des volcans qui ne créent quasiment pas de cône, mais qui libèrent d'énormes coulées de lave très liquide.
Le plus gros de cet épisode volcanique a eu lieu il y a entre 6 et 13 millions d'années. Aujourd'hui, ces volcans ne sont plus actifs, mais l'activité volcanique existe encore : pour preuve, certaines sources d'eau du Massif central sont des sources réchauffées par l'activité volcanique !
Ces anciens volcans forment aujourd'hui la chaîne des Puys, en Auvergne.
Il est possible que les premiers humains qui ont peuplé le Massif central aient été témoins de ce volcanisme, notamment les hommes de Néandertal.