Pour les scientifiques, l'œuf est une cellule qui permet la reproduction : quand un spermatozoïde rencontre un ovule, ils fusionnent (c'est la fécondation), et forment une nouvelle cellule appelée œuf, cellule-œuf, ou zygote. C'est la première cellule du nouvel être vivant. Elle va se diviser, grandir, pour redonner un nouvel animal.
Du coup, l'œuf « non fécondé », qui est pondu par les poules, sans qu'elles se soient accouplées avec un coq, n'est pas vraiment un œuf : pour avoir un œuf, il faut un ovule, et un spermatozoïde, qui est produit par le coq. Comme il n'y a pas de coq, ce n'est pas un œuf, mais plutôt un ovule, même s'il est entouré par une coquille, comme un œuf...
D'ailleurs, pour les scientifiques, seule la cellule qui va donner le poussin, c'est-à-dire le « jaune », est un œuf. Autour, le blanc, la coquille, et les autres éléments ne font pas partie du vrai « œuf », au sens biologique du terme.
Presque tous les animaux se reproduisent en faisant des spermatozoïdes et des ovules, donc presque tous les animaux ont des oeufs ! Mais l'œuf ne va pas se développer de la même façon chez tous les animaux.
Chez beaucoup d'espèces aquatiques, comme les poissons ou les amphibiens, la femelle pond des « œufs », que le mâle féconde dans l'eau : ce ne sont donc pas vraiment des œufs que pond la femelle, mais plutôt des ovules ! Chez d'autres espèces, comme les oiseaux, il faut que le mâle et la femelle s'accouplent, avant de pouvoir pondre des œufs. Les ovules sont fécondés dans le corps de la femelle, et deviennent des œufs, avant d'être pondus.
Les œufs à coquilles des oiseaux, des reptiles et des mammifères sont appelés œuf amniotique par les scientifiques. Leur coquille, qui leur évite de se dessécher, leur permet de vivre sur terre plutôt que dans l'eau. L'existence de cet œuf a permis aux scientifiques, (c'est la classification), classer les oiseaux, reptiles et mammifères dans un même groupe : celui des amniotes.
L'œuf contient aussi généralement beaucoup de réserves, qui servent à nourrir le bébé animal : le poussin, par exemple, va devoir passer plusieurs semaines dans l'œuf en ne se nourrissant que de son jaune. Chez d'autres espèces, comme les mammifères, l'œuf contient beaucoup moins de réserves, voire pas du tout : l'œuf humain est si petit qu'il faut un microscope pour le voir. A la place, il reste dans le ventre de la maman, où elle peut le nourrir grâce au cordon ombilical : nous faisons donc bien des œufs nous aussi, même si nous ne les pondons pas : ils se développent dans le ventre de la mère, qui donne naissance directement à un enfant déjà bien formé.


Parce qu'ils contiennent beaucoup de réserves, destinées à nourrir l'embryon, l'œuf est également un aliment très riche. Nous consommons régulièrement des œufs de poules, et parfois de quelques autres animaux.
L'œuf contient des vitamines A, D, E, K et des vitamines du groupe B, B2, B5, B8 et B12. Il recèle également de nombreux oligo-éléments comme le fer et le phosphore.


Selon leur mode de production, donc les conditions faites aux poules, on distingue plusieurs catégories.
Le repérage se fait à partir du premier chiffre imprimé sur la coquille:
- Le chiffre 0 correspond à des œufs produits dans un élevage relevant de l'agriculture biologique. 
- Le chiffre 1 à un élevage où les poules ont un accès à un parcours herbeux. 
- Le chiffre 2 est donné à des œufs produits par des poules élevées au sol, c'est à dire dans un bâtiment, sans accès à l'extérieur.
- quant au chiffre 3 il qualifie des œufs de poules élevées en batterie, dans des cages. 
Les lettres qui suivent ce chiffre correspondent aux deux premières lettres du pays d'origine.
Les œufs sont calibrés en quatre catégories : (S) petits (moins de 53 g), (M) moyens (53 à 63 g), (L) gros (63à 73 g) et (XL) très gros (plus de 73 g). Dans les recettes de cuisine, sauf mention contraire, il s'agit d'œufs moyens.


Les œufs sont consommables pendant quatre semaines après leur ponte. Ils bénéficient de la mention extra-frais pendant 9 jours : une étiquette détachable est dans ce cas mise sur la boîte, elle doit être retirée après neuf jours.
Pour vérifier facilement si un œuf est frais il existe un moyen simple. On plonge l'œuf dans l'eau. S'il coule, l'œuf est extra-frais. Si l'œuf surnage entre deux eaux il est frais. S'il reste en surface, il est certainement périmé ou du moins douteux. Cela est dû à l'augmentation de volume de la poche d'air contenue dans l'œuf au cours de son vieillissement.
Les œufs extra-frais sont recommandés pour une préparation sans cuisson (gobage, mayonnaise) ou à cuisson partielle (œufs coque et œuf mollet).