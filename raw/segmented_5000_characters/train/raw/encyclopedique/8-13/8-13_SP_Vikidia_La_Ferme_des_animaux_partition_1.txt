Ce cochon, inventif et loyal, devient l'un des deux dirigeants de la Ferme des animaux après la fuite de Jones. Il est très populaire. Son idée de moulin à vent pour alléger la tâche des animaux causera sa perte : Napoléon, s'y opposant, le fait chasser de la ferme avec les chiens qu'il a dressés pour répandre la terreur. Napoléon se justifie en déclarant que Boule de Neige n'était autre qu'un espion à la solde de Jones. Boule de Neige est ensuite accusé de tous les maux de la ferme, bien qu'il n'y soit jamais revenu.
Il symbolise Léon Trotski. Cet acteur majeur de la Révolution russe est le principal rival de Staline, qui réussit à le chasser d'URSS après la mort de Lénine. Le dictateur soviétique est par la suite véritablement obsédé par Trotski, le faisant traquer et assassiner au Mexique en 1940, où il s'était exilé3.




Le cheval Malabar travaille sans cesse. Ses deux devises, Je vais travailler plus dur et Napoléon ne se trompe jamais, en disent long sur sa loyauté et son obéissance. Si Napoléon le cite pour exemple, il n'hésite pas à l'envoyer à l'équarrisseur dès qu'il est sur le point de mourir, afin de s'acheter de l'alcool.
Malabar symbolise les russes qui ont cru sincèrement et loyalement aux valeurs du régime soviétique. Il peut également être associé au mineur Aleskeï Stakhanov, qui avait lors d'un concours extrait une quantité très importante de charbon et qui avait été utilisé par la propagande soviétique comme exemple à tous les ouvriers de l'URSS.


Lubie est une jument coquette qui préfère les flatteries des humains à la loi dure et austère instaurée par Napoléon. Elle finit par s'enfuir et se mettre au service des hommes plutôt que rester dans la ferme.
Lubie représente les Russes qui ont préféré s'enfuir d'URSS lorsque la loi est devenue trop dure, comme les intellectuels qui ont émigré lors des années 1920.


Manipulés par les cochons, ils sont totalement soumis à ces derniers et n'ont absolument pas conscience de la gravité de la situation, trop bêtes pour se poser la moindre question. Ils acclament systématiquement les cochons dans les débats, empêchant les rares contestataires de continuer, en chantant Quatre pattes, oui ! Deux pattes, non !. Ce refrain devient plus tard Quatre pattes, bon ! Deuxpattes, mieux !.
Les moutons représentent les Russes les plus endoctrinés et soumis à leurs maîtres sans tenter de résister.


Ce sont les animaux les plus exploités de la ferme par les animalistes, devant pondre

Elles représentent les femmes en URSS, qui étaient surexploitées.


Ce goret est un animal très persuasif, qui sert de porte-parole des cochons en allant voir les autres animaux. Il réussit à chaque fois à les convaincre que Napoléon ne commet des actions contestables que par sécurité ou par souci du bien-être de ses sujets. C'est lui qui modifie subtilement les sept commandements afin de justifier les actions injustes des cochons.
Il représente le très large réseau de propagande de Staline qui visait à endoctriner la population4. 
On peut classer dans la même catégorie Minimus, un autre cochon, qui interdit le chant Bêtes d'Angleterre, et les remplace par des poèmes à la gloire de Napoléon.


Ce vieil âne cynique prédit dès le début les dérives de l'Animalisme. Il dit à la fin du récit une phrase devenue célèbre « Tous les animaux sont égaux, mais certains le sont plus que d'autres. ».
Il symblolise George Orwell, qui était lui-même un cynique et détestait le stalinisme5. Sa présence dans le roman permet à Orwell de critiquer le communisme à sa guise.


Ce corbeau, d'abord fidèle à Jones, change de camp et soutient finalement Napoléon. Il répète sans cesse qu'après leur mort, les animaux iront au Paradis, la « montagne de Sucrecandi ».
Il représente la religion en URSS, notamment l'Église orthodoxe.


Napoléon a enlevé ces chiens à leur mère, il les dresse pour terroriser les animaux. Ce sont eux qui chassent Boule de Neige de la ferme.
Ils symbolisent les services secrets et polices politiques de Staline, qui faisaient régner la terreur en URSS6.




Ce fermier est le propriétaire de la Ferme du Manoir. Négligeant ses animaux, il oublie un soir de les nourrir, ce qui provoque la révolte de ses bêtes. Il se fait chasser de sa propriété avec tout son entourage et tente ensuite de la reprendre.
Il représente le Tsar de Russie Nicolas II. Ce dirigeant autocrate voit son pays déstabilisé par la Première Guerre Mondiale et ne peut résister face à la vague révolutionnaire qui s'empare de son pays. Il abdique en mars 1917 et est assassiné ainsi que sa famille en 1918, sur ordre des communistes7.