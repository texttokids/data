En raison de ces traits particuliers, une distinction spécifique s'est lentement dessinée entre heavy metal et hard rock. Si les deux s'appuient sur la mise en avant des guitares et sur une structure à base de riffs, le heavy metal diffère tout particulièrement du hard rock dans le fait que les structures harmoniques et mélodiques du blues sont remplacées par des progressions modales et des relations tonales instables (chromatisme, intervalles dissonants, progressions d'accords noyant l'orthodoxie


Une autre particularité sont les célèbres power ballad. Typiquement, une power ballad s'ouvre sur un air doux et mélodique, à la guitare ou au synthétiseur. Plus tard dans la chanson, souvent après le refrain, intervient la batterie. Le volume sonore et l'effet dramatique qui l'accompagne augmente encore avec l'ajout d'une guitare électrique en distorsion. La fin de la chanson peut


Le hard rock aborde de nombreux thèmes tels que la science-fiction, la fantasy, la libre pensée mais aussi des thèmes humanitaires, sociaux ou environnementaux (cf. par exemple Rush et Jimi Hendrix). Les chansons de Queen et de Led Zeppelin réemploient beaucoup d'airs qui traitaient de romance, d'amour non partagé ou de conquête sexuelle, des thèmes courants du rock, du pop et du blues. Certains textes, en particulier ceux de Led Zeppelin, ont été considérés comme misogynes. Beaucoup de chansons évoquent également la religion (notamment celles d'Alice Cooper), la mort et la violence (If You Want Blood (You've Got It) d'AC/DC), la guerre (Civil War des Guns N' Roses) et les drogues, explicitement et implicitement comme


Durant les années 1970, à côté du hard rock, se développe un nouveau genre de rock dit « heavy metal », représenté par Black Sabbath puis notamment Judas Priest. En général, le hard rock s'inspire directement du blues et utilise des gammes pentatoniques mineures alors que le heavy metal s'oriente davantage vers des rythmiques lourdes et puissantes qui l'éloignent du shuffle. Certains groupes comme Judas Priest se sont radicalisés avec le temps, alors que leur musique au début de leur carrière pouvait parfois une confusion entre le hard rock et le heavy metal. La distinction est parfois subtile. Beaucoup de groupes composent dans les deux genres comme Def Leppard et Mötley Crüe, tandis que d'autres comme AC/DC et Aerosmith restent fidèles au hard rock. La distinction entre hard rock et heavy metal est débattue depuis le milieu des années 1980 : l'expression « heavy metal » est


Creem^. Toutefois, cette distinction est sujette à controverses ; Lester Bangs est décédé peu après avoir livré sa définition et n'a eu pas le temps de la défendre et de l'affiner. Selon lui, le hard rock se définit avant tout comme une musique de la période 1968-1976 ; le heavy metal, lui, se définit par un certain nombre d'attributs stylistiques (look cuir, clous et badges) et par les thèmes des chansons tels que l'horreur et la science-fiction.


Cependant, il existe des exceptions à la règle chronologique : Guns N' Roses, groupe connu à la fin des années 1980, est généralement classé dans la catégorie hard rock. Le premier album de Black Sabbath, souvent considéré comme le premier album de heavy metal, sort dès 1969. L. Bangs lui-même classe le groupe Scorpions dans le heavy metal alors que ses membres ne possèdent aucun des attributs qu'il évoque (les membres de Scorpions préféraient clairement les foulards aux clous). De ce fait, comme le précisent très justement Jean-Marie Leduc et Jean-Noël Ogouz, « la distinction entre hard


L’Encyclopedia of Rock & Roll de Rolling Stone ne fait pas de distinction entre hard rock et heavy metal. Seule l'expression « heavy metal » est définie et englobe les composantes hard rock et heavy metal en faisant référence à la chanson de Steppenwolf, Born to Be Wild (1968) : « heavy metal thunder ». L'encyclopédie de Rolling Stone journaliste chez Rolling Stone), sans en tenir compte. L'expression


En France, le grand public ne connaissait jusque dans les années 1990 que l'expression « hard rock », « heavy metal » n'étant utilisé que par les connaisseurs. Depuis, le terme « metal »