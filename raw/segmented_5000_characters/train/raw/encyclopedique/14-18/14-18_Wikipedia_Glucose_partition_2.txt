La réaction de Maillard est une réaction (liaison) des sucres réducteurs — dont le glucose — sur les acides aminés des


Cette fonction est portée par l'atome de carbone C-6, son oxydation donne de l'acide glucuronique si la fonction aldéhyde a été


les deux fonctions terminales du glucose des atomes de carbone C-1 et C-6 et forme l'acide saccharique, un acide aldarique, c'est-à-dire un acide dicarboxylique dérivé d'un ose.


Il existe plusieurs méthodes de dosage du glucose :


solution est proportionnel à la concentration en glucose. Cette méthode est utilisée en médecine : les diabétiques


réfraction d'une solution aqueuse sucrée à l'aide d'un réfractomètre, sachant que l'indice de la solution dépend de la concentration en sucre. Cette méthode est utilisée dans la fruiticulture et viticulture pour mesurer le taux de


utilise la glucose oxydase d'Aspergillus niger. Le


est dosé par colorimétrie. En présence d'une peroxydase, le peroxyde oxyde un chromogène réduit dont l'absorbance est mesurée avec un spectrophotomètre. Le chromogène pourrait quinone-imine. Dans d'autres techniques, le peroxyde d'hydrogène est oxydé au niveau d'une électrode, le courant produit étant proportionnel à la concentration de glucose. Cette méthode est utilisée en biochimie médicale pour mesurer la


Le glucose produit dans la biosphère provient essentiellement de la photosynthèse réalisée par les plantes, les algues et les bactéries photosynthétiques. Ce processus convertit l'eau


lumineuse. Le glucose n'est généralement pas libre dans les cellules sous forme de monomères mais plutôt sous forme d'oligomères et de polymères, tels que le lactose betterave sucrière, le raffinose (galactose–glucose–fructose) des légumes, ou encore l'amidon et la cellulose des


Lorsqu'ils sont absorbés par des animaux, des champignons et des bactéries, ces polymères sont hydrolysés pour en libérer le glucose par des enzymes. Chez l'homme, ce processus commence dès la mastication sous l'action de l'amylase


Le glucose est métabolisé dans l'organisme en d'autres composés chimiques à l'origine de voies métaboliques variées. Il peut être phosphorylé en glucose-6-phosphate pour être


tréhalose par des bactéries, ou encore pour produire du


Le glucose est transporté à l'intérieur de la cellule entre compartiments cellulaires à travers des membranes à l'aide de plusieurs dizaines de protéines différentes appartenant essentiellement à la major facilitator superfamily de


Articles détaillés : glycosylation et glycation.


Le glucose est l'aldohexose le plus couramment utilisé par le métabolisme des êtres vivants. Ceci pourrait s'expliquer en partie par le fait qu'il a moins tendance que les autres aldohexoses à réagir


altère ou supprime les fonctions biologiques de nombreuses protéines. Le fait que le D-glucose soit moins sujet aux réactions de glycation spontanées que d'autres aldohexoses pourrait s'expliquer par des formes cycliques plus stables, ce qui réduirait la probabilité qu'il soit sous forme à chaîne ouverte, la plus réactive. Cette stabilité particulière lui vient du fait que tous ses groupes hydroxyle sont en position équatoriale, hormis celui de l'atome de carbone C-1 de l'anomère α. En revanche, l'addition de glucides aux protéines contrôlée par des enzymes, appelée glycosylation, est un processus vital indispensable aux fonctions biologiques de


La plupart des glucides alimentaires contiennent du glucose, que ce soit comme constituant unique, comme l'amidon ou le glycogène, ou avec d'autres oses, comme le saccharose ou


Dans la lumière du duodénum et de l'intestin grêle, les oligosaccharides et polysaccharides de glucose sont


pancréatiques. C'est notamment le cas du saccharose et du lactose. Les autres oligosaccharides et polysaccharides ne peuvent pas être complètement hydrolysés de cette façon et ne peuvent être assimilés sans l'assistance du microbiote intestinal. Le D-glucose ainsi libéré est transporté à travers la membrane apicale des entérocytes par le cotransporteur sodium-glucose 1 (SGLT1), puis à travers leur membrane basale par le transporteur de glucose GLUT2^, il s'agit d'une assimilation essentiellement active^. Une partie du glucose est converti par les astrocytes en acide lactique, qui est ensuite utilisé comme source d'énergie par les cellules du cerveau. Une autre partie est utilisée par les cellules de l'intestin et les érythrocytes. Le


muscles, où il est absorbé et stocké sous forme de glycogène sous l'influence de l'insuline. Le glycogène des hépatocytes peut être à nouveau converti en glucose et repasser dans le sang lorsque le taux d'insuline est bas. Le glycogène des myocytes ne retourne pas dans le sang faute d'enzymes pour ce faire. Dans les adipocytes, le glucose est utilisé notamment pour produire des graisses. Le glycogène est un moyen de stockage du glucose efficace dans la mesure où cela le rend plus compact et moins réactif.