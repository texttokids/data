Les mailles à l'endroit et à l'envers peuvent être tordues : d'habitude une seule fois (c'est-à-dire un demi-tour) au plus, mais quelquefois deux ou (très rarement) trois fois. Quand on la regarde du dessus, la torsion peut être dans le sens des aiguilles d'une montre ou opposé. Dans le premier cas le fil de droite passe devant celui de gauche, sinon c'est l'inverse. On appelle ces mailles torses à droite, respectivement à gauche. Les tricoteurs à la main produisent généralement les mailles torses à droite en tricotant à travers les mailles par derrière, c'est-à-dire en passant l'aiguille dans la maille d'une façon inhabituelle, mais en tournant le fil sur l'aiguille comme faites par les tricoteurs à la main en tournant le fil à l'envers sur l'aiguille et en enfonçant cette dernière comme d'habitude dans la maille. Bien qu'elles soient symétriques dans leur forme, les mailles torses sont équivalentes fonctionnellement. Les deux types de mailles torses donnent une texture visuelle subtile mais intéressante. Elles tendent à resserrer le tricot, le rendant plus raide. Les mailles torses sont une méthode habituelle pour tricoter de fins fils


Certaines techniques de tricot plus avancées créent une variété surprenante de textures complexes. L'aspect du vêtement est aussi influencé par le poids de la laine, qui décrit l'épaisseur de fibres filées. Plus la laine est épaisse, plus les mailles seront visibles. Les points ornementaux doivent être intégrés avec soin au projet final,


Illustration de torsades tricotées. La torsade centrale est faite de côtes 2-2 : le fond est à l'envers et les brins de la torsade sont chacun 2 colonnes à l'endroit. En changeant l'ordre des mailles, on


Peinture du VII^e siècle sur parchemin illustrant un tapis. Le fond est formé de nœuds celtiques très fins (cliquer 3 fois par étapes pour voir


Chandail d'Aran, couvert des fameux motifs en torsades, dont certains


D'habitude, les mailles sont tricotées dans le même ordre sur chaque rang, et les colonnes sont parallèles, verticales le long du tricot.


En changeant l'ordre des mailles d'un rang à l'autre, d'habitude avec une aiguille à torsades, ou réserve de mailles, on peut tricoter une variété infinie de tresses, de torsades, de nids d'abeilles, de motifs des chandails d'Aran. Les torsades ont tendance à resserrer le tricot, le rendant plus dense et moins élastique^. Les chandails d'Aran utilisent une grande variété de tricots en torsade^. On peut ainsi réaliser des tresses arbitrairement complexes, sous la restriction que les colonnes doivent toujours se diriger vers le haut ; il est en général impossible d'avoir une colonne qui monte puis descend dans le tricot. Des tricoteurs ont mis au point des méthodes pour donner l'illusion d'une colonne circulaire, comme celles qui apparaissent dans les nœuds celtiques, mais ce ne sont que des approximations inexactes : le sens de tricot fait que les mailles ont la forme d'un V, et il faudrait les raccorder au sommet avec des mailles en < aux intercolonnes en forme de Λ, ce qui est impossible . Cependant de telles colonnes circulaires sont possibles en utilisant la reprise suisse, une forme de broderie, ou en tricotant séparément un tube et en l'appliquant sur le tricot.


Dans le tricot en dentelle, le motif est fait de petits trous stables,


En combinant les augmentations faisant de petits trous en œillet, avec des diminutions assorties, on peut créer un tissu très léger ressemblant à la dentelle : c'est le tricot en dentelle, qui consiste en motifs et dessins utilisant de tels trous, plutôt qu'avec les mailles elles-mêmes^^,^. Les grands et nombreux trous de la dentelle la rendent extrêmement élastique : par exemple certains châles en Shetland sont nommés alliances, parce qu'ils sont si fins qu'ils peuvent passer à travers une alliance.


Illustration de l'entrelacement. Les colonnes bleues et blanches sont parallèles entre elles, et perpendiculaire aux noires et brunes,


Deux tricots peuvent être cousus par des méthodes de couture invisible empruntées à la broderie, le plus souvent le point Kitchener.


Mais on peut surtout commencer de nouvelles colonnes à partir d'un bord quelconque d'un tricot ; ceci s'appelle relever des mailles, et c'est la base de l’entrelacement. L'entrelacement forme un riche motif en échiquier, en tricotant de petits carrés, en relevant leurs côtés et en tricotant d'autres carrés pour continuer perpendiculairement le


En combinant les augmentations et les diminutions, il est possible d'orienter la direction d'une colonne en oblique par rapport à la verticale. Ceci est la base du tricot en biais, et peut être utilisé pour des effets visuels comparables à la direction du trait de pinceau