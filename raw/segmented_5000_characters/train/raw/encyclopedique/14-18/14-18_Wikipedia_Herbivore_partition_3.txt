N’étant pas en mesure de bouger, les végétaux ont donc développé des moyens de défense physiques et chimiques, soit de résistance, pour réduire les dommages. Parmi les moyens physiques, notons la présence d’épines ^, poils ou d’un feuillage épais et rugueux. Ces adaptations morphologiques ont pour but de diminuer l’herbivorie en rendant les plantes moins attrayantes et moins faciles d’accès vis-à-vis des herbivores. Pour ce qui est des moyens chimiques, les plantes peuvent produire un grand nombre de composés toxiques, nocifs^ ou tout simplement désagréables au goût afin de réduire l’herbivorie. Par exemple, certaines plantes produisent un acide aminé rare, la canavanine. Étant donné qu’il ressemble à l’arginine, les plantes l’incorporent dans leurs protéines et cela a pour effet de changer la conformation des protéines et leurs fonctions, menant donc l’insecte à la mort. D’autres composés tels que


les tanins^ ont un effet toxique pour les herbivores. Ces défenses sont en fait divisées en inhibiteurs quantitatifs et en inhibiteurs qualitatifs. Les inhibiteurs quantitatifs sont efficaces à grandes doses et se retrouvent surtout dans les vieilles feuilles et les tiges ligneuses. Notons parmi ceux-ci la lignine, la cellulose et les phénols. Pour leur part, les inhibiteurs qualitatifs sont efficaces à petites doses et sont retrouvés principalement dans les tissus vulnérables tels que les nouvelles


Par ajustement de la phénologie, on entend par exemple une plante qui aura une période de croissance tôt en saison afin d'éviter de faire ce travail pendant la période où les herbivores sont les plus


La sur-compensation quant à elle décrit le fait que certaines plantes endommagées par l'herbivorie ont une aptitude phénotypique supérieure


Certains herbivores lato sensu ou phytophages peuvent être


représentent cependant souvent la forme dominante de la consommation de la productivité primaire^. Ce sont des mammifères (campagnols par exemple), mais surtout des invertébrés d'herbivorie du sous-sol puissent favoriser le flux des nutriments du sol vers la plante, via une croissance accrue des racines des plantes hôtes et des plantes compagnes. Par exemple, de faibles taux d'infection des racines de trèfle blanc (Trifolium repens L.) par le nématodes du trèfle (Heterodera trifolii Goffart) augmente la croissance racinaire, respectivement de 141 % et 219 % chez la plante hôte et chez l'herbe voisine non infectée


Par contre, l'infection des racines du trèfle a augmenté la biomasse microbienne du sol dans la zone racinaire, avec dans le même temps un retour vers le sol d'une partie de l'azote capté par le trèfle (mesuré par le transfert de l'isotope Azote 15 de la plante hôte vers sol et l'herbe voisine)^. Dans ce dernier cas, cet azote pourrait être utilisé par des bactéries ou d'autres plantes résistantes au nématode. Ces données suggèrent que de manière générale, de faibles taux d'herbivorie du sous-sol peuvent accroître le transfert de carbone et d'azote du sol vers les plantes, avec augmentation de la croissance des racines et recyclage plus rapide des éléments nutritifs du sol dans les prairies^. Ces interactions influencent probablement fortement la compétition ou les associations entre espèce et entre espèces végétales, en modifiant la structure des communautés végétales dans les prairies. Le contrôle de la croissance des plantes par les herbivores, se fait donc aussi de manière invisible


Le régime des herbivores peut fortement varier d'une saison à l'autre, particulièrement dans les zones tempérées, en fonction de la végétation disponible selon les périodes de l'année. Les herbivores doivent passer une grande partie de leur temps à brouter (et à digérer) car le rapport C:N des plantes est de 40:1 et celui des animaux de 9:1, les animaux sont composés d'environ quatre fois plus d'azote que les plantes. C'est pourquoi les herbivores doivent se nourrir abondamment afin de répondre à leurs besoins nutritionnels. Le carbone excédentaire est rejeté dans les excréments ou recyclé à la mort de l'animal via sa nécromasse exploitée par les nécrophages (des insectes nécrophages aux vautours et condors en passant par de nombreuses autres espèces). Les prédateurs (consommateurs secondaires) ; ceux qui se nourrissent d'autres animaux par exemple, n'ont pas besoin de manger autant car leur nourriture se compose du même rapport qu'eux.


Au sein de leur territoire et de leur aire vitale, les herbivores doivent se déplacer pour manger tout en échappant à leurs prédateurs^. avec d'autant plus de difficultés que les patchs riches en aliments sont rares et/ou que les prédateurs sont nombreux et