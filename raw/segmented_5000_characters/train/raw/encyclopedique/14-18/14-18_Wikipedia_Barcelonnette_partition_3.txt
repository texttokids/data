La commune est durement touchée par la Première Guerre mondiale, avec 81 morts. Une souscription publique est lancée afin de financer la construction du monument aux morts. Une autre souscription, de fin 1919 à 1921, a lieu dans toute la vallée de l'Ubaye (avec Allos) et permet de financer un monument aux 509 morts de la vallée, érigé à Barcelonnette par Paul Landowski^. Outre les morts tués par les Allemands, un Barcelonnette natif de Revel est fusillé pour l'exemple ; son nom figure sur le monument aux morts de Barcelonnette^. Enfin, un certain nombre d'habitants de Barcelonnette ayant émigré au Mexique, une plaque commémorative évoque la mort de 10 citoyens mexicains, venus s'engager durant la Première Guerre mondiale.


Au début de la Seconde Guerre mondiale, Barcelonnette est protégée de l’invasion italienne par la ligne Maginot des Alpes. L’effondrement des armées françaises lors de la campagne de France devant la Wehrmacht permet cependant à l’Italie de satisfaire ses exigences : la ligne violette passe à l’ouest de Barcelonnette, qui se trouve ainsi en zone démilitarisée : les chasseurs alpins doivent changer de garnison. Elle subit également les visites de la Délégation de contrôle du dispositif militaire des Alpes, qui cherche à contrôler les armements français, afin de neutraliser les fortifications en cas de


La 89^e compagnie de travailleurs étrangers, internant et soumettant au travail forcé des étrangers jugés indésirables par la Troisième République et par Vichy, a été établie à Barcelonnette^. Le 22 juillet, la gendarmerie reçoit l'ordre de surveiller la résidence de Paul Reynaud, qui réside au Plan, à l'écart de la ville. Reynaud était président du Conseil jusqu'au 16 juin et partisan de la poursuite de la guerre, et Pétain craignait qu'il quitte la France, voire rejoigne De Gaulle à Londres. Mais la zone étant démilitarisée, et Barcelonnette à 15 km seulement de la ligne de démarcation de la zone d'occupation italienne, un Reynaud en fuite serait difficile à rattraper. Le dispositif est renforcé par degrés successifs, puis Reynaud est arrêté le 7 septembre pour être emprisonné au château de


La vallée de l’Ubaye a été l’un des centres de l’importante mobilisation résistante qui a suivi le débarquement en Normandie, le 6 juin 1944, dans la région provençale. Elle fut reprise par les Allemands à partir du 13 juin. À Barcelonnette, Paul Geay*, Ernest Gilly*, Louis Lèbre*, Léon Signoret, Émile Donnadieu (résistants) furent interceptés en mission dans la nuit du 14 au 15 juin 1944 et exécutés sans jugement à 20 heures, dans la cour du lycée de Barcelonnette, le 16 juin 1944. À la fin de la guerre, 26 Juifs sont arrêtés à Barcelonnette avant d’être déportés^, puis les FFI libèrent la ville à l’été 1944, avant l’arrivée des troupes


La commune a été décorée, le 11 novembre 1948, de la Croix de


Le 11^e bataillon de chasseurs alpins est en garnison à


Blason de Barcelonnette Blason Parti : au premier palé d'or et de gueules, au second d'argent à la clef renversée de gueules, le panneton


Barcelonnette a été fondée en 1231 par le comte de Barcelone, qui était aussi comte de Provence, sous le nom de Raimond Bérenger V. La partie dextre du blason rappelle la maison de Barcelone. La partie senestre Le statut officiel du blason reste à déterminer.


Article connexe : Élections municipales de 2014 dans les


Cette section est vide, insuffisamment détaillée ou incomplète.


Article détaillé : Liste des maires de Barcelonnette.


Une école normale est créée à Barcelonnette en 1833 : elle y fonctionne jusqu’en 1888, lors de son transfert à Digne^.


Le lycée André-Honnorat, succède au collège Saint-Maurice, rebaptisé d’après le nom du ministre de l’instruction André Honnorat en 1919. Désiré Arnaud (préfet), Pierre-Gilles de


Jusqu’à la Troisième République, les classes primaires sont installées dans des bâtiments qu’elles partagent avec d’autres activités : les garçons au collège Saint-Maurice, avec l’école normale de garçons, et les filles dans les locaux de l’hospice tenu par les


Actuellement, trois écoles fonctionnent à Barcelonnette : une école maternelle et une école élémentaire publiques, et une école privée nationale)^. L’école primaire de l’avenue des Trois-Frères-Arnaud est construite en 1882-1883 pour accueillir les classes primaires de filles. Dès 1884, les classes de garçons sont installées au rez-de-chaussée, les filles occupant l’étage ; un cours complémentaire est ouvert pour les filles la même année, pour les garçons peu de temps après. En 1957, l’école compte 9 classes, dont deux de maternelle, et 10 en 1993. De 1963 à 1973 (création des