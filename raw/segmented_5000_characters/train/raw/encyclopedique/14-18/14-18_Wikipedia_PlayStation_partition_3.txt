L'installation d'un modchip permettait aux PlayStation d'avoir des fonctionnalités avancées, et beaucoup d'options étaient disponibles. Vers la période de fin de vie de la console, presque n'importe qui pouvait, avec une simple soudure, réaliser une modification de la console. Ces modifications permettaient de jouer à des jeux NTSC sur une console PAL^ (donc d'outrepasser la restriction du code régional), ou alors de pouvoir jouer à des copies de jeux illégales sans problème. Les modchips permettaient de jouer à des jeux gravés sur des CD-ROM vierges. Cela a généré une vague de jeux créés avec des compilateurs GNU, ainsi que la reproduction illégale de jeux originaux. Avec l'introduction de tels dispositifs, la console devint très attrayante à la fois aux yeux des


Les individus qui ont insisté à copier des jeux auxquels ils voulaient jouer comme les versions originales ont rencontré plusieurs problèmes à la fois, vu que les disques fabriqués par Sony n'étaient pas facilement copiables. Non seulement les disques originaux Sony ont une couleur noire spécifique, mais ils contenaient également des secteurs vides de données qui interdisaient leur copie. La console était supposée ne pas


sans ces espaces de mémoire volontairement vierges. Parfois, les disques à face noire contenant des secteurs vides étaient impossibles à recopier avec les logiciels habituels, et les copies étaient inutiles sans modification de la console. Les logiciels de copies de CD ont bénéficié de nouvelles fonctions comme "Ignorer les mauvais secteurs", et combinés avec les modifications de consoles ont résolu les problèmes, et ont permis au grand public d'utiliser tous les disques voulus, à partir du moment où la puce de modification était installée dans leur console. La création et la production en masse de ces puces à très bas prix (12C508), couplées avec leur facilité d'installation (de 4 à 8 fils à souder), marquèrent véritablement le


Le premier successeur de la PlayStation est la PlayStation 2, qui est rétrocompatible avec son prédécesseur, dans le sens où elle peut lire presque tous les jeux PlayStation. Cela a été possible en intégrant la majorité des composants de la première PlayStation dans la seconde. Contrairement aux émulateurs sur PC, la PlayStation 2 contient le processeur original de la PlayStation, permettant aux jeux de fonctionner exactement comme sur la première PlayStation. Pour les jeux PlayStation 2, ce processeur, appelé IOP, est utilisé pour les entrées et les sorties (les cartes mémoires, le lecteur de DVD, l'adaptateur réseau, et le disque dur). Comme son prédécesseur, la PlayStation 2 est basée sur des composants fabriqués par Sony. La PlayStation Portable (PSP) est une console portable sortie en décembre 2004 au Japon,


2005 en Europe. Elle utilise le format UMD. Une partie du catalogue de jeux PlayStation est réédité sur la console en téléchargement sur le PlayStation Network. Depuis fin décembre 2006, un firmware spécial permet aussi aux utilisateurs de convertir leurs jeux PlayStation au format PSP EBOOT.


La PlayStation 3, est lancée en novembre 2006 aux Europe. La plupart des jeux PlayStation fonctionnent sur les modèles 40 et 60 Go de la PlayStation 3 (la rétrocompatibilité avec les jeux PlayStation 2 est dépendante du modèle). Les jeux PlayStation et PlayStation 2 restent zonés. À l'instar de la PSP, une partie du catalogue de jeux PlayStation est réédité sur la console en


En 2011, la PlayStation Vita est annoncée, elle succède à la PlayStation Portable. Dotée d'un écran tactile et d'un pavé tactile à l'arrière, de deux sticks analogiques et de composants plus puissants, elle sort en décembre 2011 au Japon, et en


En 2013, la PlayStation 4 est annoncée, elle succède à la


Le bloc laser du lecteur de CD intégré aux premières unités produites (notamment la série des 1000 avec le KSM-440AAM) était construit autour d'une structure en plastique qui glissait sur des rails eux aussi en plastique. Avec le temps, la friction provoquée par les déplacements répétés durant les accès au disque rongeait le plastique sur la partie la plus lourde du bloc optique. S'ensuit au bout de quelques mois, voire années, une inclinaison horizontale souvent inégale du bloc optique. Visuellement, on peut s'apercevoir qu'il penche d'un côté. Le laser ne pointe plus ainsi perpendiculairement sur le CD mais légèrement de biais. Par conséquent les disques ne pouvaient plus être lus correctement. Sony a résolu le problème en changeant le matériau des patins de la partie mobile en contact avec les rails de translation/déplacement (sled) par du silicone/nylon plus robuste. Le corps du bloc optique n'est plus réalisé en plastique lui non plus mais en métal sur modèles suivants, à