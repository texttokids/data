Pour concurrencer le nouvel Airbus A380, Boeing a annoncé en novembre 2005 le coup d'envoi officiel d'une nouvelle version de 747. Dénommé 747-8 et plus connu sous le nom 747 advanced, ce nouvel appareil est une mise à jour du 747 censée relancer les ventes du gros-porteur de l'avionneur américain. Son autonomie sera de 14 815 km^. Les principales évolutions à noter sont un nouveau poste de pilotage modernisé, de nouveaux réacteurs fournis par GE Aviation, un fuselage allongé, une aile allongée et fortement incurvée


Le 21 juillet 2005, la société Cargolux a annoncé le commencement de négociations avec Boeing sur l'achat d'au minimum dix Boeing 747 Advanced (version fret), suivie de Emirates et Nippon Cargo qui ont commandé respectivement dix et huit unités. La première commande a commande ferme de treize avions et une option sur dix autres^.


En 2011, ce 747 existe en deux variantes. Le 747-8I (Intercontinental) qui permet d'embarquer 467 passagers sur une distance de 15 000 km et le 747-8F adapté au transport de marchandises. La compagnie allemande Lufthansa est la première à exploiter la version passagers, avec vingt exemplaires commandés et une option pour vingt autres. Le 747-8F a effectué son premier vol le 8 février 2010. Il a décollé de Paine Field à 12 h 39 heure locale (21 h 39 à l’heure de Paris), avec Mark Feuerstein et Tom Imrich aux commandes.


Articles détaillés : Air Force One, Shuttle Carrier Aircraft


En 1972, le prototype du 747 est transformé en avion ravitailleur


Depuis 1990, l'avion du président des États-Unis est un VC-25A, basé sur le 747-200B ; il est utilisé comme Air Force One, indicatif d'appel donné à tout appareil de l’United States Air Force transportant le président américain. Il existe deux Boeing VC-25A Air Force One qui assurent, à tour de rôle, le transport du président américain. Ce sont des Boeing 747-200 spécialement aménagés. D'autres 747 spéciaux existent, tels les quatre Boeing E-4 de l’US Air Force qui peuvent servir de centre de commandement en cas d'urgence Shuttle Carrier Aircraft, des 747 de transport pour les navettes spatiales américaines^, et un exemplaire d'avion ravitailleur qui fut utilisé exclusivement par l'armée de l’air iranienne^. Le dernier modèle militaire du 747 est le modèle expérimental YAL-1 Airborne Laser de « laser aéroporté » destiné au programme de défense anti-missiles


Evergreen International Aviation a modifié et transformé un Boeing 747-100 et un -200 en avions de lutte anti-incendie, le Evergreen Supertanker. Mis en service en 2009, ce type d'avion d’une capacité d’environ 74 000 litres a déjà été utilisé en Espagne et société, Global SuperTanker Services, a repris le projet avec un


D'autres gouvernements emploient également des 747 pour le transport de


La plupart des compagnies aériennes internationales emploient les 747 sur leurs itinéraires les plus fréquentés. Cependant, étant donné que le trafic sans escale entre villes d'importance moyenne est devenu prédominant, certaines grandes compagnies ont remplacé leurs 747 par des biréacteurs plus petits et plus efficaces. Beaucoup de compagnies, grandes ou moyennes, américaines ou non, ont donc supprimé le jumbo jet de leur flotte. C'est le cas de Aer Lingus, Air Canada, Alitalia, American Airlines, America West Airlines,


En 2011, la plupart des 747 appartiennent à des compagnies européennes ou asiatiques. Dans les années 1970, 17 compagnies nord américaines utilisaient des 747 pour leurs liaisons commerciales. Elles n'étaient plus que deux en 2017 : Delta Air Lines et United Airlines. United Airlines a opéré son dernier vol commercial en 747 le 7 novembre 2017 ^ et Delta Air Lines a effectué son dernier vol commercial le 19


Les 7 plus gros opérateurs de Boeing 747 (sauf cargo)


5 Drapeau de la République populaire de Chine Air China 10


Année 1966 1967 1968 1969 1970 1971 1972 1973 1974 1975 1976 1977 1978


Commandes 83 43 22 30 20 7 18 29 29 20 14 42 76 72 49 23 14 24 Livraisons - - - 4 92 69 30 30 22 21 27 20 32 67 73 53 26 22 Année 1984 1985 1986 1987 1988 1989 1990 1991 1992 1993 1994 1995 1996


Commandes 23 42 84 66 49 56 122 31 23 2 16 32 56 36 15 35 26 16 Livraisons 16 24 35 23 24 45 70 64 61 56 40 25 26 39 53 47 25 31 Année 2002 2003 2004 2005 2006 2007 2008 2009 2010 2011 2012 2013 2014


Commandes 17 4 10 46 53 16 2 5 1 3 7 13 2 6 18 6 18 0 1 572 Livraisons 27 19 15 13 14 16 14 8 - 9 31 24 19 18 9 14 6 5 1 553


Le nombre de livraisons est en diminution ces dernières années, bien que le chiffre historique de 1500 ventes ait été atteint en 2014 avec la vente d'un 14^e 747-8I à Lufthansa^. En 2010, aucun 747 n'a été livré, et la seule commande enregistrée porte sur une version de prestige (VIP).


Reconstitution d'une partie du fuselage du 747-131 opérant le vol


Depuis sa mise en service, le 747 a connu 146 accidents et


pour un total de 3 722 morts^ ; le dernier accident est celui