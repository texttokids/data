Article détaillé : Liste des primates décrits par Carl von Linné.


Parallèlement aux travaux de Buffon, le naturaliste suédois Carl von Linné jette les bases de la nomenclature binominale. Dans la dixième édition de son Systema Naturae^, il introduit le genre Simia qui regroupe tous les singes ainsi qu'une espèce de tarsier. Il en fait l'un des quatre taxons de l'ordre des Primates, aux côtés des genres Homo (l'espèce


Contrairement à Buffon, Linné ne fait pas de différences entre les espèces de l'Ancien et du Nouveau Monde. La division interne du genre Simia est en effet basée sur l'antique distinction entre les animaux sans queue (les « singes des Anciens », Simiae veterum), ceux à queue courte (les « babouins », Papiones) et ceux à longue queue (les


En 1777, le naturaliste allemand Johann Christian Erxleben reprend cette première classification mais y inclut les travaux de Buffon en séparant les singes du Nouveau Monde. Il réduit le genre Simia aux seuls singes sans queue et crée quatre nouveaux genres pour le reste : Papio pour les babouins, Cercopithecus pour les guenons, Cebus pour les sapajous et Callithrix pour les sagouins^. Il place en revanche l'unique espèce de tarsiers décrite par Buffon dans le genre Lemur (Lemur tarsier). On notera qu'afin de rester en accord avec les récits antiques, Erxleben construit les noms des deux genres américains à partir de racines greco-latines : Cebus évoque les « cèbes » d'Aristote alors que Callithrix fait écho au « callitriche » de Pline.


La liste des genres s'allonge durant les décennies qui suivent, grâce notamment aux travaux du comte de Lacépède à Paris


En 1812, Étienne Geoffroy Saint-Hilaire entérine la division entre Ancien et Nouveau Monde en proposant la séparation entre les Catarrhini (« singes de l'Ancien continent ») et les Platyrrhini (« singes d'Amérique »). Il crée également plusieurs


Cercocebus (les mangabeys), Nasalis (les nasiques)


Parallèlement, le concept de famille s'impose peu à peu en zoologie et c'est John Edward Gray, du British Museum de Londres, qui propose le premier une division des mammifères selon ce principe. Il sépare ainsi l'ordre des Primates en Hominidae, Sariguidae, Lemuridae (prosimiens), Galeopithecidae classées dans un groupe nommé « anthropomorphes » et correspondent


macaques) et Cynocephalina (babouins). Les « Sariguidae » se divisent quant à eux en Mycetina (hurleurs), Atelina


C'est ainsi que dès les années 1830, la classification scientifique des singes atteint, dans ses grandes lignes, l'ordre qui prévaut encore au XXI^e siècle et recense les principaux groupes d'espèces connus aujourd'hui. La principale exception à ce constat est la place réservée à l'espèce humaine, qui y est systématiquement rangée dans un groupe bien à part, les savants de l'époque rechignant à faire tomber l'ancestrale barrière entre


Caricature de Faustin Betbeder représentant Charles Darwin et


Le premier scientifique à avoir soutenu que les autres primates pouvaient être apparentés aux hommes est Giulio Cesare Vanini^, avant Charles Darwin, dans les années 1600. L'affirmation du fait que l'homme est un singe est aujourd'hui banale, certains titres comme « L'homme est un singe comme les autres »


Les singes forment un infra-ordre (Simiiformes) qui appartient, avec le groupe frère des Tarsiiformes, au sous-ordre des Haplorrhini. Ces primates se distinguent en effet des Lorisiformes et des Lemuriformes (sous-ordre


Cette section ne cite pas suffisamment ses sources (juillet


Pour l'améliorer, ajoutez des références vérifiables [comment faire ?] ou le modèle {{Référence nécessaire}} sur les passages


En Afrique sub-saharienne (ici au Burkina Faso), les artistes s'inspirent également du singe pour sculpter leurs masques Statue égyptienne de babouin de l'époque ptolémaïque.


Dans les mythologies et les cosmogonies, le singe occupe une place toute particulière et nombre de ses aspects symboliques se retrouvent


Dans la Roue de l'existence tibétaine, il symbolise la Conscience versatile, celle qui, liée au monde sensible, se disperse d'un objet à l'autre. Réputé être l'ancêtre des Tibétains, qui le considèrent comme un Bodhisattva, il est, selon Si Yeou Ki, le fils du Ciel et de la Terre. Il accompagne donc Xuanzang (Hiun-Tsang) dans son voyage à la recherche des Livres saints du Bouddhisme. Il y apparaît comme le compagnon facétieux, magicien taoïste de grande envergure. Le Roi-Singe, dans l'art extrême oriental, évoque la sagesse, le détachement. C'est pourquoi les célèbres Singes du Jingoro, au temple de Nikko, sont représentés l'un se bouchant les oreilles, le second se cachant les yeux, le troisième se fermant la bouche. Une interprétation occulte plus ancienne tend à voir dans les trois sages de Jingoro la représentation d'un Singe créateur de toutes choses ici bas, conscient de l'illusion et de l'impermanence de la réalité.