L'activité des geysers, comme celle de toutes les sources chaudes, est liée à une infiltration d'eau en profondeur. L'eau est chauffée par sa rencontre avec une roche, elle-même chauffée par le magma ou par l'action du gradient géothermique (le fait que la température et la pression augmentent avec la profondeur), c'est pourquoi il est possible de trouver des sources d'eau chaude et des geysers dans les régions non volcaniques. Cette eau, chauffée et mise sous pression, jaillit alors vers la surface par effet de convection. Les geysers diffèrent des simples sources chaudes par la structure géologique souterraine. L'orifice de surface est généralement étroit, relié à des conduits fins qui mènent à d'imposants réservoirs d'eau souterrains.


L'intensité des forces en jeu explique la rareté du phénomène. Autour de nombreuses zones volcaniques, on peut trouver des sources chaudes accompagnées de fumerolles (île Sainte-Lucie, Java, Dallol, etc.). Mais souvent, les roches sont trop friables, ce qui engendre une


L'activité d'un geyser est assez fragile et capricieuse ; certains se sont éteints parce qu'on y avait simplement jeté des déchets, d'autres parce qu'on y avait exploité l'énergie géothermique^.


Il faut distinguer un geyser d'autres phénomènes paravolcaniques :


ou un lac géothermique très chaud (comme le Prismatic Spring


des sédiments à sa surface (boue, argile, matériaux volcaniques…


De l'eau s'introduit dans le réservoir du geyser (qui est proche d'une poche magmatique) par infiltration puis, en s'accumulant dans le réservoir, elle monte dans le conduit étroit, résistant et haut^. Il faut rappeler que la pression ne dépend pas du volume mais de la hauteur, et que plus la pression est grande plus la température d'ébullition est élevée. Ainsi l'eau du conduit va faire pression sur l'eau du réservoir et augmentera la température d'ébullition. Au bout d'un certain temps, la poche magmatique aura suffisamment chauffé pour vaporiser une partie de son eau, créant ainsi une bulle de vapeur qui va remonter vers la surface. Or, le seul chemin de sortie est le conduit, où la bulle va donc s'engouffrer. Elle va pousser vers le haut l'eau du conduit, qui n'exercera donc plus la pression sur l'eau du réservoir. Cette dernière va entrer en


L'éruption se termine par épuisement du réservoir. Un nouveau cycle accumulé suffisamment d'eau dans le réservoir pour que l'eau monte et commence à se vaporiser. La durée de chaque éruption et le temps séparant deux éruptions varient d'un geyser à l'autre, et leur


Séquence éruptive du geyser de Strokkur, en Islande : geyser inactif, arrivée des bulles de gaz, naissance du geyser puis vidange de


Il existe deux types de geysers. Le geyser dit « en cône » est terminé par un cône étroit, avec un conduit très fin. Lorsqu'une éruption se produit et qu'une colonne d'eau jaillit, elle est en fait expulsée par la pression due à l'étroitesse du conduit. C'est le cas par exemple d'Old Faithful, dans le Parc national de Yellowstone, aux


L'autre type de geyser est le geyser dit « fontaine ». Il s'agit généralement d'une source chaude qui, lorsque du gaz est expulsé, fait remonter les bulles d'eau qui explosent au contact de la surface et qui créent une large colonne d'eau, souvent de courte durée. C'est le cas


Les geysers sont relativement rares car dépendants de conditions climatiques et géologiques que l'on ne retrouve qu'en peu d'endroits sur terre. Il existe de par le monde cinq zones principales de geysers


geysers détruite partiellement à la suite d'un glissement de terrain survenu le 4 mai 2007, mais toujours bel et


Il existe des geysers dans le Nevada, aux États-Unis. Ils sont aujourd'hui éteints, à cause de l'industrie thermale, mais il en subsiste toujours un dans l'État voisin de l'Utah, le Fly


Yellowstone est de loin la zone la plus active au monde avec près de 400 geysers recensés^. Le parc possède en outre les deux spécimens les plus imposants dont le célèbre Old Faithful. Fichier:Old Faithful (California).ogv Lire le média


Le site Dallol (Éthiopie), est célèbre pour ses concrétions de soufre et de sel fondu, et ses petits geysers gazeux.


Aux Açores, à São Miguel ou à Terceira, il s'agit surtout d'eaux très chaudes mélangées à de l'oxyde de fer fondu.


Au Kenya, c'est au lac Bogoria^, situé dans le même axe volcanique que le fameux lac Turkana, que l'on peut trouver des dizaines de sources chaudes bouillonnantes et des geysers, gazeux et en


mares de boues. De petits geysers sont aussi présents mais ne


Puits géothermique jaillissant du Old Faithful of California à


Le geyser d'Andernach (vallée du Rhin) est le plus haut geyser d'eau