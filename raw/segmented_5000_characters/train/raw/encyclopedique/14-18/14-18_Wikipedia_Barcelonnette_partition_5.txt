L'évolution du nombre d'habitants est connue à travers les recensements de la population effectués dans la commune depuis 1793. À partir de 2006, les populations légales des communes sont publiées annuellement par l'Insee. Le recensement repose désormais sur une collecte d'information annuelle, concernant successivement tous les territoires communaux au cours d'une période de cinq ans. Pour les communes de moins de 10 000 habitants, une enquête de recensement portant sur toute la population est réalisée tous les cinq ans, les populations légales des années intermédiaires étant quant à elles estimées par interpolation ou extrapolation^. Pour la commune, le premier recensement exhaustif entrant dans le cadre du


diminution de 1,37 % par rapport à 2012 (Alpes-de-Haute-Provence :


1793 1800 1806 1821 1831 1836 1841 1846 1851 2 050 2 182 2 080 2 130 2 144 2 154 2 267 2 270 2 242


1856 1861 1866 1872 1876 1881 1886 1891 1896 2 153 2 026 2 000 1 919 2 082 2 303 2 234 2 009 2 286


1901 1906 1911 1921 1926 1931 1936 1946 1954 2 363 2 405 2 532 2 216 2 705 2 723 2 987 3 007 3 000


1962 1968 1975 1982 1990 1999 2006 2007 2012 2 432 2 476 2 626 2 735 2 976 2 819 2 818 2 766 2 634


De 1962 à 1999 : population sans doubles comptes ; pour les dates


En 1471, la communauté de Barcelonnette (qui comprenait plusieurs paroisses des alentours) comprenait 421 feux. En 1765, elle comptait 6 674 habitants^. Les migrations jusqu'à la Grande Guerre, notamment au Mexique, ont un impact sur la ville.


Selon le recensement de la population 2007, Barcelonnette compte près de 2 766 (population municipale) ou 2 939 (population totale)^ habitants répartis sur 16,42 km^2. La ville se caractérise par une faible densité de population.


Entre 1999 et 2007, le taux de croissance annuel moyen était de 1999^. En effet, en 2009, on y a compté 20 naissances


La station de Pra-Loup est à 10 kilomètres de Barcelonnette, celle du Sauze à 4 km. Saint-Anne - La Condamine est une petite station agréable et moins touristique que les deux autres.


Tous les étés depuis 1994 a lieu à Barcelonnette le Festival du jazz


Les remparts n’ont laissé leur trace que dans le tracé des rues du


La mairie est construite dans les années 1930, après destruction de la chapelle Saint-Maurice (en juillet 1934^). Son fronton provient lui de l’ancien couvent des dominicains, classé en


Bien que l’architecture des maisons anciennes soit archaïque, elles ne datent pour les plus anciennes que du XVIII^e siècle, la ville ayant été reconstruite après l’incendie de 1628.


L’ancienne gendarmerie, place Manuel, construite pour abriter la sous-préfecture en 1825, et actuellement transformée en logements, est de style néo-classique. Sa façade, qui occupe tout un côté de la place, est percée de portes en plein cintre. Les pierres à bossages animent la façade^. La place Manuel est nommée en honneur de l’homme politique de la Restauration, Jacques-Antoine Manuel ; la fontaine qui en occupe le centre porte son portrait sculpté par


La sous-préfecture est installée depuis 1978 dans une des villas des Mexicains, la villa l’Ubayette, construite en 1901-1903.


De nombreuses maisons construites par les Barcelonnettes revenus du Mexique sont classées monument historique. Ayant émigré en masse, entre 1850 et 1950 au Mexique, ils ont détenu le monopole du commerce et de l'industrie textile tout en y découvrant « l'importance de l'architecture et son pouvoir de représentation dans ce siècle de l’Industrie » (François Loyer), en particulier sous le gouvernement de Porfirio Diaz. Leur position sociale leur a permis de devenir les promoteurs d'une architecture monumentale liée à la création de leurs grands magasins. De retour du Mexique, ils reprirent pour l'édification un style directement issu de l'art industriel qu'ils


De leurs grands magasins mexicains à leurs villas de la vallée de l'Ubaye, pour les Barcelonnettes les références culturelles sont restées identiques. Les villas de Barcelonnette et de Jausiers ont les mêmes architectes, décorateurs et fournisseurs spécialisés. L'objectif de ceux-ci et de leurs commanditaires fut « d’exprimer avant tout l'image du progrès et de la réussite sociale^ ». Article détaillé : Villas mexicaines de la vallée de l'Ubaye.


L’église paroissiale Saint-Pierre-ès-Liens est construite au Moyen reconstruite, trop vite, en 1634-1638, puis rebâtie en 1643-1644. Celle-ci est à nouveau démolie, en 1926-1927, pour laisser la place à l’église actuelle, commencée en 1923. Son clocher date de la reconstruction du XVII^e siècle. En 1653, il est augmenté d’un pyramidions et de gargouilles, et surmonté en 1860 d’un campanile en fer forgé portant une statue de Vierge en métal doré^.


Plusieurs tombes du cimetière sont signalées par Raymond Collier pour


L’église Saint-Pons comporte deux porches (sud et ouest), tous les deux abondamment illustrés, compte tenu de la pauvreté des décors en style