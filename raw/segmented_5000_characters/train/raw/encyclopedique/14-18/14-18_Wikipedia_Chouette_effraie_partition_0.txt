Le nom vernaculaire de cette chouette vient d'orfraie (du latin ossifraga « briseur d'os ») qui désignait certaines espèces de rapaces pêcheurs diurnes. Depuis le XVI^e siècle et en particulier chez Ronsard, ce terme orfraie existe déjà, mais pour désigner un rapace nocturne. Sous l'influence du verbe effrayer, il s'est déformé en effraie, donnant le nom à cette chouette^.


La femelle est légèrement plus grande que le mâle, elle mesure 34 à 40 cm de long pour un poids de 570 g en moyenne. Le mâle mesure entre 32 et 38 cm pour un poids de 470 g. L'envergure est de 1,07 à 1,10 m pour les deux sexes^. L'effraie des clochers possède un masque facial blanc en forme de cœur. Le dessus du corps est gris cendré à brun jaune, richement pointillé et perlé de fines taches blanchâtres ourlées de noir. Le poitrail est blanchâtre à blanc roussâtre plus ou moins piqueté de brun foncé. Ses pattes sont longues couvertes de plumes blanches et munies de doigts puissants aux serres bien développées. Ses ailes sont longues et plutôt étroites. L'iris de l'œil est noir. Les sexes sont identiques. Son envergure va de 0,90 à 0,95 m et son poids est d'environ 415 g^. Il est possible d'en voir une


Chouette effraie en vol dans les Pyrénées (France)


Son espérance de vie dépasse rarement 10 ans (en moyenne 8 ans^)


Son cri est un « khrûh » ou « khraikh » rauque, strident et répétitif qu'on compare souvent au ronflement d'un dormeur, ponctué de sonorités aiguës. Il y a aussi un deuxième cri, le chant territorial du mâle, durant environ 2 secondes, qui fait un « chhhhhh »^. Elle chuinte. Elle peut également pousser des sonorités plus discrètes


Elle claque également très fort du bec lorsqu'elle se sent menacée.


Bordées d'une frange souple, couvertes d'un moelleux duvet, les plumes de l'effraie absorbent très bien les frottements de l'air et réduisent les turbulences. Son vol silencieux permet à la chasseresse de surprendre ses proies avant qu'elles ne s'enfuient ou se cachent.


Dormant le jour, elle est protégée par son plumage « camouflé ».


Son vol est le plus silencieux de tous les oiseaux de la planète, grâce aux petites dentelures de ses plumes. Cette forme a inspiré des


Son régime alimentaire se compose essentiellement de petits rongeurs rarement elle capture des belettes ou des lapins, ainsi que des petits oiseaux, des amphibiens, de gros insectes, voire de chauves-souris capturées de manière opportuniste^. Un ornithologue, Uttendoerfer, a étudié le régime alimentaire de l'effraie, par l'analyse des pelotes de réjection. Il a ainsi constaté que, sur 77 602 vertébrés recensés, on trouvait 74 250 mammifères (113 chauves-souris, 195 taupes, 20 466 musaraignes, 9 belettes), dont 54 438 petits rongeurs 8 lapins, 2 414 oiseaux (1 273 moineaux domestiques, 149 moineaux friquets, 95 hirondelles de fenêtre, 77 hirondelles de cheminée, 71 martinets noirs…), 936 batraciens, 1 poisson,


Les pelotes de réjection mesurent environ 45 mm sur 26 mm. Elles sont caractérisées par leur aspect noir, brillant, arrondies aux deux


La chouette effraie chasse la nuit dans des étendues cultivées ou des prairies, elle est d'ailleurs connue en anglais sous le nom de "Barn Owl" ou "Chouette des granges". La forme de ses yeux permet de concentrer un maximum de lumière sur la rétine. Ainsi, la chouette effraie a besoin de cinquante fois moins d'éclairage que l'homme pour voir distinctement. Pendant longtemps, on a débattu de savoir si, dans le noir complet, les chouettes effraies repéraient leurs proies, par l'odeur, la captation de rayons infrarouges, ou par les sons. Des recherches menées par le zoologiste Roger S. Payne^ dans les années 1950 ont clos ce débat grâce à une expérience astucieuse. Dans une grange complètement noire, observée avec des lunettes infrarouges, ils ont fait courir une souris silencieusement sur un tapis en mousse avec un morceau de papier émettant un bruissement attaché derrière sa queue. Une chouette, descendue de son perchoir, a alors capturé le morceau de papier et non la souris, démontrant par là que la chouette effraie réagissait aux sons et non aux odeurs ou aux