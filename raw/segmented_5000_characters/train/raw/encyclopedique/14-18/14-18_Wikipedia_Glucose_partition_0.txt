Le glucose a été isolé en 1747 à partir du raisin par le chimiste allemand Andreas Marggraf. En 1812, Constantin Kirchhoff parvient à transformer l'amidon en sucre (en sirop de maïs), par chauffage avec de l'acide sulfurique^. En 1838, un comité de l'Académie des sciences composé des chimistes et physiciens français Thénard, Gay-Lussac, Biot et Dumas, décide d'appeler le sucre se trouvant dans le raisin, dans l'amidon, et dans le miel du nom de glucose, en fournissant comme Littré ayant donné une autre étymologie, l'adjectif γλυκύς / glukus


Le rôle important du glucose dans la biochimie de la plupart des formation dans les cellules s'est accompagnée de grandes avancées en chimie organique. On doit l'essentiel de ces résultats au chimiste allemand Emil Fischer, qui obtint le prix Nobel de chimie en 1902 « en reconnaissance des services extraordinaires qu'il a rendus par son travail sur la synthèse des sucres et des purines »^. La synthèse du glucose permit de comprendre la structure des substances organiques et constitua la première validation définitive des théories du chimiste néerlandais Jacobus Henricus van 't Hoff relatives à la cinétique chimique et à l'arrangement des liaisons chimiques dans les composés organiques. Fischer années 1891 et 1894, et prédit correctement tous les isomères possibles en appliquant les théories de van 't Hoff des atomes de


Le glucose est un aldose dont les groupes hydroxyle –OH sont disposés selon une configuration déterminée le long d'une chaîne formée de six atomes de carbone. La molécule de cet hexose peut adopter une configuration à chaîne ouverte ou cyclique.


Dans sa configuration à chaîne ouverte, la molécule de glucose possède une chaîne carbonée linéaire constituée de six atomes de carbone, numérotés de C-1 à C-6 : l'atome de carbone C-1 est impliqué dans le groupe aldéhyde –CHO, et les atomes de carbone C-2 à C-6 portent chacun un groupe hydroxyle –OH. La présence d'un groupe aldéhyde fait du glucose un aldose, tandis que la présence de six atomes de carbone en fait un hexose : il s'agit donc d'un aldohexose. Comme tous les aldoses, le glucose est un ose


Chaque atome de carbone de C-2 à C-5 est un centre stéréogène, c'est-à-dire que ces atomes de carbone sont liés chacun à quatre substituants différents. Par exemple, un aldohexose qui, dans la projection de Fischer, a ses hydroxyles en C-2, C-4 et C-5 à droite et son hydroxyle en C-3 à gauche est du D-glucose ; si la position relative de ses hydroxyles est, toujours dans la projection de Fischer, inversée par rapport à celle du D-glucose, alors il s'agit de L-glucose. Ces deux stéréoisomères sont par conséquent des


Projection de Fischer des deux énantiomères du glucose.


Le glucose est l'un des huit aldohexoses, les sept autres étant


l'idose, le mannose et le talose, chacun avec deux


Il existe deux formes chaise selon que l'hydroxyméthyle est en position


En solution aqueuse, les molécules de glucose à chaîne ouverte sont en équilibre avec plusieurs formes cycliques de glucose, chacune comprenant un hétérocycle contenant un atome d'oxygène. En fait, moins de 0,25 % des molécules de glucose en solution aqueuse sont pyranose ; la forme furanose du glucose est présente en quantité négligeable. Les termes glucose et D-glucose s'emploient de l'addition nucléophile entre le groupe aldéhyde –CHO du carbone C-1 et le groupe hydroxyle –OH du carbone C-4 ou C-5, ce qui donne un groupe hémiacétal –CHOH–O– : l'addition sur l'hydroxyle du carbone C-4 donne la forme furanose, avec un hétérocycle à cinq atomes, tandis que l'addition sur l'hydroxyle du carbone C-5 donne la forme pyranose, avec un hétérocycle à six atomes. En solution aqueuse, la presque totalité des molécules de glucose en configuration cyclique sont sous


La fermeture du cycle fait de l'atome de carbone C-1 un centre stéréogène supplémentaire donnant deux anomères pour le D-glucopyranose et deux autres anomères pour le D-glucofuranose. Ces anomères sont distingués par les préfixes α- et β- selon que


position trans (de part et d'autre) ou en position cis (du même côté) par rapport au cycle pyranose dans la projection de Haworth. Ainsi, le D-glucose linéaire peut donner quatre molécules cycliques différentes : l'α-D-glucopyranose, le β-D-glucopyranose, l'α-D-glucofuranose et le β-D-glucofuranose, qui sont toutes les quatre


Le L-glucose donne de la même façon quatre molécules cycliques différentes, chacune étant énantiomère d'une forme cyclique de


Les cycles pyranose et furanose, plans en projection de Haworth, sont en réalité tordus pour former une structure tridimensionnelle non plane. Le glucopyranose adopte ainsi tantôt une configuration dite chaise, tantôt une configuration dite bateau, tandis que le glucofuranose adopte une structure dite enveloppe à l'instar du cyclopentane. Il existe toute une série de configurations


droite et de haut en bas : chaise ^4C, bateau, gauche, chaise