Il arrive parfois que des volcans naissent loin de toute limite de plaque lithosphérique (il pourrait y avoir plus de 100 000 montagnes sous-marines de plus de 1 000 mètres^). Ils sont en général interprétés comme des volcans de point chaud. Les points chauds sont des panaches de magma venant des profondeurs du manteau et perçant les plaques lithosphériques. Les points chauds manteau, des volcans se créent successivement et s'alignent alors, le plus récent étant le plus actif car à l'aplomb du point chaud. Lorsque le point chaud débouche sous un océan, il va donner naissance à un chapelet d'îles alignées comme c'est le cas pour l'archipel d'Hawaï ou des Mascareignes. Si le point chaud débouche sous un continent, il va alors donner naissance à une série de volcans alignés. C'est le cas du mont Cameroun et de ses voisins. Cas exceptionnel, il arrive qu'un point chaud débouche sous une limite de plaque lithosphérique. Dans le cas de l'Islande, l'effet d'un point chaud se combine à celui de la dorsale médio-atlantique, donnant ainsi naissance à un immense empilement de lave permettant l'émersion de la dorsale. Les Açores ou les Galápagos sont d'autres exemples de points chauds débouchant sous une limite de plaque


Néanmoins de nombreux volcans intra-plaque ne se présentent pas sur des alignements permettant d'identifier des points chauds profonds et


Une éruption volcanique survient lorsque la chambre magmatique sous le volcan est mise sous pression avec l'arrivée de magma venant du manteau. Elle peut alors éjecter plus ou moins de gaz volcaniques qu'elle contenait selon son remplissage en magma. La mise sous pression est accompagnée d'un gonflement du volcan et de séismes très superficiels localisés sous le volcan, signes que la chambre magmatique se déforme. Le magma remonte généralement par la cheminée principale et subit en même temps un dégazage ce qui provoque un trémor, c'est-à-dire une vibration constante et très légère du sol. Ceci est dû à des petits séismes dont les foyers sont concentrés le long de la cheminée.


Au moment où la lave atteint l'air libre, selon le type de magma, elle s'écoule sur les flancs du volcan ou s'accumule au lieu d'émission, formant un bouchon de lave qui peut donner des nuées ardentes et/ou des panaches volcaniques lorsque celui-ci explose. Selon la puissance de l'éruption, la morphologie du terrain, la proximité de la mer, etc il peut survenir d'autres phénomènes accompagnant l'éruption : séismes importants, glissements de terrain,


La présence éventuelle d'eau sous forme solide comme une calotte glaciaire, un glacier, de la neige ou liquide comme un lac de cratère, une nappe phréatique, un cours d'eau, une mer ou un océan va provoquer au contact des matériaux ignés tels que le magma, la lave ou les tephras leur explosion ou augmenter leur pouvoir explosif. En fragmentant les matériaux et en augmentant brutalement de volume en se transformant en vapeur, l'eau agit comme un multiplicateur du pouvoir explosif d'une éruption volcanique


phréato-magmatique. La fonte de glace ou de neige par la chaleur du magma peut également provoquer des lahars lorsque l'eau entraîne des tephras^ ou des jökulhlaups comme ce


L'éruption se termine lorsque la lave n'est plus émise. Les coulées de lave, cessant d'être alimentées, s'immobilisent et commencent à se refroidir et les cendres, refroidies dans l'atmosphère, retombent à la surface du sol. Mais les changements dans la nature des terrains par le recouvrement des sols par la lave et les tephras parfois sur des dizaines de mètres d'épaisseur peuvent créer des phénomènes destructeurs et meurtriers. Ainsi les cendres tombées sur des cultures les détruisent et stérilisent la terre pour quelques mois lac qui noiera des régions habitées ou cultivées, des pluies tombant sur les cendres peuvent les emporter dans les rivières et créer des


Une éruption volcanique peut durer de quelques heures à plusieurs années et éjecter des volumes de magma de plusieurs centaines de kilomètres cubes. La durée moyenne d'une éruption est d'un mois et demi mais de nombreuses ne durent qu'une journée. Le record absolu est celui du Stromboli qui est quasiment en éruption depuis environ


Article détaillé : Éruption volcanique#Types d'éruptions


Lors des débuts de la volcanologie, l'observation de quelques volcans a été à l'origine de la création de catégories basées sur l'aspect des éruptions et le type de lave émise. Chaque type est nommé selon le volcan référent. Le grand défaut de cette classification est d'être assez subjectif et de mal tenir compte des