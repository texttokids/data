Cette marque américaine est certainement l'une des plus appréciées des astronomes amateurs, sinon la plus appréciée. Bien qu'orientés haut de gamme, ses produits demeurent relativement populaires : assez souvent innovants et performants, ils offrent la plupart du temps un champ apparent assez vaste et bien corrigé des aberrations et notamment de l'astigmatisme, ce qui leur permet de très bien fonctionner, même pour des rapports F/D inférieurs à 5. La gamme proposée est plutôt vaste puisqu'on peut trouver des formules Plössl assez abordables, mais aussi l'exclusivité de Tele Vue. Le produit phare du fabricant est sans conteste la série des Nagler (du nom de son concepteur, le gérant de Tele Vue) à 82° de champ. Les Nagler, nés en 1982, ont connu plusieurs modifications de formule optique : nous en sommes aujourd'hui aux types 4, 5 et 6 (les premiers Nagler modifiés étaient de type 2 et il n'y eut pas de type 3). Curieusement, malgré des tarifs en conséquence, le champ apparent est proportionnel à la popularité : s'il est vrai que les Panoptic (68°) ont également les faveurs des amateurs, les Radian focale équivalente n'est pas tellement plus onéreux. Tele Vue est correction de l'astigmatisme ophtalmique, ce qui évite à l'observateur astigmate de garder ses lunettes sur le nez pour observer dans de bonnes conditions avec de grandes pupilles de sortie — malheureusement ces lentilles ne peuvent pas s'adapter sur tous les


Cette petite manufacture américaine, spécialisée dans la conception de lunettes astronomiques, a, de façon plus prolixe que son concurrent Astro-Physics, sorti plusieurs lignes d'oculaires et en particulier remis au goût du jour une formule simple optimisée pour l'observation des planètes : cette série d'oculaires monocentriques (appelée Super Monocentric) à un seul élément (composé de trois lentilles) offre un champ apparent réduit à 32° mais garantit un taux de transmission lumineuse proche des 99 % et un contraste préservé.


Thomas M. Back est également connu pour une petite série d'oculaires de 30 à 40mm grand champ à 68° : les Paragon. Il a laissé pour héritage les séries Planetary à focale courte de 2,5 à 9mm de 58°/60° ayant un bon confort d'observation, ils sont aujourd'hui plus ou moins bien


On connaît plutôt ce petit fabricant américain par le biais de deux séries d'oculaires appréciées des amateurs, toutes les deux d'origine nippone : une série d'orthoscopiques de type Abbe (du nom de l'opticien allemand), en tout point identique à celle commercialisée sous le label Kasai, tente de compenser la fin de la production des modèles Zeiss ; en outre, University Optics propose aussi une série d'oculaires à grand champ 68° König (formules


Encore un autre fabricant américain, à qui l'on doit une (seule) série d'oculaires plutôt réputés : les Brandon, qui n'utilisent volontairement pas de traitement multicouches pour limiter la diffusion fournis avec les télescopes de la fameuse marque Questar.


Ce grand fabricant japonais de matériel d'astronomie est notamment le créateur de plusieurs séries d'oculaires assez connues. Outre les traditionnels orthoscopiques au coulant 24,5 mm, Vixen commercialise deux séries à grand dégagement oculaire comportant des verres au lanthane : ce sont les gammes LV, dotée d'un champ apparent standard de l'ordre de 50°, et LVW, à grand champ (65°), particulièrement réputée pour la qualité des optiques, proches de


Ce fabricant originaire de Taïwan propose notamment, outre ses traditionnels réfracteurs, deux séries notables : les Swan, très abordables en regard de leur champ apparent de 72°, et les Uwan qui, avec 82° de champ, chassent plutôt sur les terres des Meade UWA et


Si la célèbre manufacture d'Iéna ne fabrique plus de matériel pour les amateurs, il n'en demeure pas moins qu'elle a laissé au cours de son histoire quelques nobles restes que les amateurs-collectionneurs s'arrachent parfois à prix d'or. Citons notamment de fameuses séries d'oculaires monocentriques et d'orthoscopiques de type Abbé. Certains amateurs, suivis par les revendeurs, redoublent d'astuce pour tenter d'adapter aux porte-oculaires des télescopes des oculaires de conception récente destinés à l'observation terrestre...


1 - Oculaire 2 - Compensation slide 3 - Prism 4 - Beam splitter 5 -


Elles se composent de deux oculaires identiques dont on peut régler l’écartement (à l’intérieur du corps de la tête binoculaire un prisme sépare en deux le faisceau lumineux qui arrive de l’instrument). Avantages : l’impression de relief en imagerie lunaire et planétaire, et surtout une moindre fatigue oculaire puisqu’on sollicite les deux yeux. Les têtes binoculaires conviennent pour tous les instruments


Cette section est vide, insuffisamment détaillée ou incomplète.