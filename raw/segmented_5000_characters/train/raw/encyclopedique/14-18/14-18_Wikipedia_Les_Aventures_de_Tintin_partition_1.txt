Pendant et après l'occupation allemande, Hergé est accusé par les autorités d'épuration d'être un collaborateur, car le journal Le Soir était contrôlé par les nazis, et il est brièvement incarcéré qu'il avait tout simplement fait son métier pendant l'Occupation, comme l'auraient fait un plombier ou un charpentier. Les histoires nées durant cette période, contrairement à sa production d'avant et d'après-guerre, sont dans l'ensemble politiquement neutres, sans référence avec la situation de l'Europe en guerre. Néanmoins, Le Sceptre d'Ottokar dénonce bien le déroulement d'un Anschluss, commandité par un dictateur nommé Musstler (contraction de


Par ailleurs, l'apocalyptique album L'Étoile mystérieuse traduit la peur de l'avenir éprouvée par l’auteur durant cette époque de guerre et distingue, dans sa version originale, un groupe ami de scientifiques issus de pays neutres ou occupés par l'Allemagne, d'un groupe concurrent conduit par un banquier juif américain. Mais les histoires sont souvent tournées vers les voyages, l'aventure et la chasse aux trésors, comme dans Le Secret de La Licorne, Le Trésor de Rackham le Rouge ou Les Sept Boules de cristal.


La pénurie de papier née de la guerre entraîne un changement de format des Aventures. Hergé avait pour habitude de donner à ses albums le nombre de pages nécessaire au développement de ses scénarios en produisant 2 planches contenant chacune 6 cases réparties en 3 lignes de 2 colonnes. Petit à petit, la place réservée aux aventures se réduit jusqu'à se limiter à un simple strip de 4 à 5 cases. Le 5 février 1942^, la maison d'édition Casterman persuade Hergé de passer à la couleur grâce aux machines offset que possède l'imprimeur. Mais cela suppose de dessiner des planches plus petites et d'adopter une longueur de 62 pages par album. En effet, un album est constitué de 4 cahiers de 16 pages, soit 64 (62 + page de titre + verso). Hergé agrandit donc son équipe (les dix premiers albums ont été conçus par lui-même et sa femme), qu'il finit par regrouper en studio.


L'adoption de la couleur permet à Hergé de donner une plus grande envergure à son œuvre. Sa manière de l'utiliser est plus subtile que


rendues à l'impression, permettant l'emploi de la quadrichromie et, de ce fait, une approche cinématographique de la lumière et des


pour remplir des demi-pages ou tout simplement pour détailler et mettre en avant une scène. L'emploi de la couleur fait ressortir les détails importants. Hergé insiste sur ce point en affirmant : « Je considère mes histoires comme des films. Donc, pas de narration, pas de description. Toute l'importance, je la donne à l'image. »^


La vie personnelle d'Hergé a également influencé la série. Par exemple, Tintin au Tibet est fortement marqué par sa dépression ; ses cauchemars, qu'il aurait décrits comme étant « tout blancs », trouvent un écho dans les paysages enneigés de l'album. L'intrigue est basée sur les recherches menées par Tintin pour retrouver Tchang, qu’il avait rencontré dans Le Lotus bleu. Cet épisode ne met en scène aucun bandit, et Hergé, qui s'abstient de tout jugement de valeur, se refuse à qualifier l'homme des neiges (le yéti) « d'abominable ».


Hergé aura aussi dénoncé dans ses ouvrages l'exploitation des minorités le trafic d'armes et les dictatures en Amérique du Sud (L'Oreille cassée, Tintin et les Picaros) et en Europe (L'Affaire


Les Aventures de Tintin se sont terminées avec la mort d'Hergé le 3 mars 1983. La vingt-quatrième aventure, Tintin et l'Alph-Art, est restée inachevée. Dans cet album, Tintin évolue dans le monde de l'art moderne, et l'histoire se termine sur une scène où il risque la mort, enfermé dans du plexiglas et exposé comme une œuvre d'art.


Les neuf premiers albums ont d'abord été publiés en noir et blanc. Hergé, à l'aide de son équipe, a fait par la suite une version en couleurs de tous ces premiers albums (qu'il a redessinés et dont il a plus ou moins modifié le scénario), à l'exception de Tintin au pays des Soviets ; L'Île Noire a même fait l'objet d'une troisième version. Bien qu'initialement publiés en couleur, les albums L'Étoile mystérieuse et Tintin au pays de l'or noir ont politiquement : publiée pendant la Seconde Guerre mondiale, la première version de L'Étoile mystérieuse a pu être interprétée comme une œuvre de propagande en faveur de l'Axe Rome-Berlin, tandis que la première version de Tintin au pays de l'or noir fait explicitement référence au conflit israélo-palestinien.


Les dates mentionnées ci-dessous sont celles de la première édition en album. Les trois premiers albums ont été publiés aux éditions du Petit Vingtième, à Bruxelles, et les autres, chez Casterman, les 13^e et 14^e albums, la couleur est d’Edgar P. Jacobs.


Parmi ces vingt-quatre albums, seul Tintin et l'Alph-Art ne fut


Les 22 albums canoniques (de Tintin au Congo à Tintin et les Picaros) représentent au total 15 000 cases et 1 364 planches^.