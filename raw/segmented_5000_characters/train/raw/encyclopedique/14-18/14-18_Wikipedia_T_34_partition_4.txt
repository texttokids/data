Cependant, l'armement semblant toujours insuffisant, surtout avec l'arrivée du T-34-85 armé d'un canon équivalent, on étudia la possibilité de monter un canon de calibre 100 mm. Le canon envisagé, le S-34 de la marine, se révéla évidemment trop lourd et trop encombrant pour le châssis dans sa forme initiale. Les efforts du TsAKB (bureau central d'étude de l'artillerie) pour essayer de l'adapter donnèrent naissance au SU-100-2 qui fut écarté au profit du projet de l'usine Uralmarsh, qui, plus pragmatique, avait demandé à l'équipe de F.F. Petrov de dessiner un nouveau canon plus léger et petit, le D-10, nécessitant donc moins de modifications sur le véhicule. Les essais menés face au SU-100-2 en mars, puis en juin, se révélant satisfaisants, la production en grande série fut décidée (environ 2 300 unités produites pendant la guerre). Le blindage avant de la superstructure avait été porté de 45 à 75 mm, le canon de 100 mm, avec une vitesse initiale de 895 m/s, pouvait percer un Panther ou un Tigre à 1 500 m et avait une dotation de 33 obus.


Le canon D-10, trop jeune, souffrait de quelques défauts. Pire, sa munition perforante, la BR-412B, se révélait difficile à produire. En conséquence, la production d'un modèle transitoire armé avec le D-5S de 85 mm fut lancée jusqu'en décembre, moment où le SU-100 put enfin lui


Le T-34 fut utilisé pendant toute la Seconde Guerre mondiale en nombre sans cesse croissant. La variante T-34-85 semble encore utilisée dans certains pays (Cuba, Corée du Nord, Angola, etc.).


Au moment de l'opération Barbarossa, un millier de ces chars est disponible. Les Allemands ne connaissent que vaguement les matériels blindés des soviétiques : KV-1, KV-2 et les T-34 n'étaient connus que des plus hauts gradés de la Wehrmacht. Bien que supérieurs à tout ce que les Allemands pouvaient leur opposer, ils souffraient principalement du manque d'entraînement de leurs équipages et de la désorganisation de l'Armée rouge à cette époque. En effet, par suite des décisions contradictoires et irréalistes du


L'arme blindée soviétique, pourtant longtemps pionnière, était complètement incapable de mener une guerre générale. Les unités, de formation trop récente, manquaient de cohésion. Le matériel et les hommes, en nombre insuffisant, étaient dispersés.


Le corps des officiers a été sérieusement affaibli lors des Grandes Purges. Staline avait fait massacrer les meilleurs cadres de l'Armée rouge, les plus talentueux, expérimentés l'arme blindée comme Mikhaïl Toukhatchevski. Lors de la guerre, Joukov a brillamment appliqué la stratégie des « opérations en profondeur » ou « art opératif » développés par Toukhatchevski dans l'entre-deux-guerres, les T-34 étant chargés de l'exploitation en


Les officiers ayant survécu aux purges étaient souvent inexpérimentés et incapables de mener une guerre moderne. Pour corser le tout, par mesure d'économie, le personnel ne s'entraînait pas sur le matériel récent, mais sur des chars démodés comme les chars T-26 ou BT-2 qui avaient peu de rapport avec leurs futures machines de temps de guerre. De plus, les corps mécanisés étaient déployés à des centaines de kilomètres de la frontière, ce qui, combiné aux défauts de jeunesse des nouveaux modèles, à l'absence de matériel de dépannage adapté (on utilisait surtout des tracteurs agricoles réquisitionnés) et l'omniprésence de la Luftwaffe provoqua de nombreuses pertes avant même que le combat s'engageât. L'un des principaux défauts du T-34 est l'absence de radio à bord, ce qui rend plus difficile la coordination des manœuvres. L'autre faiblesse majeure du T-34 (corrigée par le T34-85 modèle 1944) était l'absence de chef de char, alors que les


Par exemple, le plus grand engagement de chars qui eut lieu pour contrer la percée du Panzergruppe 1, avec ses 799 panzers, est une contre-offensive lancée par le front du Sud-Ouest entre les 5^e et 6^e armées soviétiques, rassemblant les 2 156 chars


19^e (ru) corps mécanisés, dont plus de la moitié


T-34 combattaient. La Bataille de Moscou fut surtout gagnée avec des chars légers, comme le T-60, seuls 45 T-34 y participèrent. Au cours de l'hiver 1942-1943, le T-34 est engagé en masse lors des offensives géantes dites des « quatre planètes » (Uranus,


Stalingrad. Ce ne fut qu'en 1943 que le char devint majoritaire dans l'Armée rouge, époque à laquelle sa puissance de feu devenait insuffisante. Son nombre et son endurance permirent de tenir jusqu'à l'arrivée fin 1943 de la variante dotée d'un 85 mm qui pouvait combattre efficacement les meilleurs blindés allemands.