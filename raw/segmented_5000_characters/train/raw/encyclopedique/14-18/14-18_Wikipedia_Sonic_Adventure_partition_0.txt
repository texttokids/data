Sonic Adventure est le premier jeu de la série Sonic à prendre place


Cet univers se divise en trois mondes (Adventure Field) entre lesquels le joueur évolue, où se déroule l'histoire et où sont lancés les


casino, ses hôtels luxueux et son parc d'attractions. d'anciennes ruines, plusieurs cavernes et l'Angel Island. Mystic


plusieurs formes. Elle est accessible à divers instants de l'aventure, puis en bateau depuis Station Square et Mystic Ruins


Article détaillé : Liste des personnages de Sonic the Hedgehog.


Le joueur peut incarner sept personnages différents qu'il débloque au fur et à mesure de la progression dans le jeu. Le système de jeu dépend


supersonique, capable de courir plus vite que n'importe qui. Corey Bringas^), meilleur ami de Sonic, renard jaune à deux queues qui lui permettent de voler. Passionné de mécanique, il passe son temps à construire des avions dans son


anglais : Michael McGaharn^), puissant échidné rouge, gardien d'Angel Island et de l'Émeraude Mère (Master Emerald).


construit par le D^r Eggman, faisant partie de la série des E-100. Chacun de ces robots est construit à partir d'un oiseau enfermé en


anglais : Jennifer Douillard^), jeune fille hérisson rose, qui se bat avec son grand marteau, qui s'est auto-proclamée comme


temps à pêcher avec son ami la grenouille Froggy. laquelle il se transforme lorsqu'il a récupéré les sept


chargée de protéger l'Émeraude Mère et les émeraudes du chaos. autel suivi de son enfermement dans l'Émeraude Mère, l'ont rendu fou de rage. Il grandit et gagne en puissance en absorbant les gigantesque, Perfect Chaos, avec les sept émeraudes. Otsuka ; doublage anglais : Deem Bristow^) est le principal ennemi de la série Sonic. Il veut rassembler les sept émeraudes du chaos et utiliser leur pouvoir afin de détruire le monde^[M 2]. Dans ce jeu, il libère Chaos de l'Émeraude Mère, et lui fait absorber les émeraudes du chaos pour le rendre plus


Site archéologique maya de Tikal au Guatemala, qui a inspiré le décor des Mystic Ruins et le nom du personnage de Tikal.


Des siècles avant les événements narrés dans le jeu, les sept pouvoir, ainsi que l'émeraude mère (Master Emerald), ayant le pouvoir de les contrôler et d'amplifier ou d'annihiler leur pouvoir, furent confiées à la Terre et déposées dans un temple. Des guerres


Un des Chao (petites créatures innocentes vivant dans le temple) entra accidentellement en contact avec l'eau sacrée baignant les extrêmement puissante, qui devint le gardien des Chao^. Plus tard, une tribu d'échidnés, menés par Pachamac, énervèrent Chaos de par leur volonté de voler les émeraudes du chaos, de ruiner le temple et de massacrer les Chao. Une nuit, ces derniers furent tous tués, ce qui emplit Chaos de rage ; il utilisa alors le pouvoir négatif des émeraudes pour se transformer en Perfect Chaos et tua les échidnés. Tikal, la fille de Pachamac, opposée aux idées de son père, parvint à se sceller, elle et Chaos, dans l'Émeraude Mère afin que ce


Sept émeraudes : une bleue, une jaune, une violette, une noire,


Les sept émeraudes du chaos convoitées par le D^r Eggman.


Plusieurs siècles après cette histoire, Eggman se rend sur Angel Island, île volante gardée par Knuckles, et brise l'Émeraude Mère, libérant Chaos. L'île s'écrase dans la mer, dispersant les sept Knuckles décide de partir à la recherche de ces fragments.


Pendant ce temps, dans la ville de Station Square, Sonic combat Chaos chaos) après l'avoir vu attaquer la police locale. Le lendemain, après avoir aperçu Tails atterrir difficilement avec l'un de ses derniers avions, le Tornado, Sonic lui vient en aide. En revenant à Station Square, Tails lui explique qu'il se servait d'une des sept Devant l'atelier de Tails, au cœur des « Mystic Ruins », Eggman surgit,


Afin d'empêcher Eggman de mener à bien son plan, Sonic et Tails partent leur échappent et tombent entre les mains d'Eggman, qui s'enfuit à bord de son vaisseau, l'Egg Carrier. Sonic et Tails poursuivent l'Egg Carrier avec le Tornado mais se font attaquer et leur avion s'écrase. Grâce à une autre émeraude qu'il a trouvé, Tails travaille sur une nouvelle version de son avion, le Tornado II, et se lance à la poursuite de l'Egg Carrier. Cette fois-ci, Sonic et Tails parviennent à


Dans le même temps, Eggman sort son nouveau robot de la série des E-100, E-102 « Gamma », et le lance à la poursuite de Froggy, la grenouille de Big, qui a mangé une émeraude du chaos et la queue de Chaos. De son côté, Amy trouve un petit oiseau (qui serait auparavant contenu dans un robot de la série de E-100) et se fait poursuivre par E-100 « Alpha », nommé « Zéro », envoyé par Eggman pour récupérer l'émeraude que posséderait l'oiseau. Capturée par Zéro et enfermée dans une cellule sur l'Egg Carrier, elle parvient à se libérer grâce à Gamma, qu'elle convainc et qui finit par céder sous l'émotion.