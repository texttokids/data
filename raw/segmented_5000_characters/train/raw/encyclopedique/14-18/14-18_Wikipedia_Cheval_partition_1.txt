Le cheval est un animal quadrupède. Une terminologie spécifique s'applique aux différentes parties de son corps, dont des termes habituellement réservés à l'être humain, comme « bouche », animal domestique. Sa hauteur se mesure au garrot, sorte de renflement situé à la jonction de l'encolure et du dos^. Par convention, le cheval a trois parties externes principales : l'avant-main, qui comprend la tête, l'encolure et les membres antérieurs ; l'arrière-main composée de la croupe, des hanches, des membres postérieurs et de la queue ; et le corps, la partie centrale^. Il porte une crinière et une queue dont les


morphologie permet de décrire et d'apprécier la beauté, les


L'anatomie du cheval comprend l'étude du squelette, des muscles, des tendons, du système digestif, respiratoire, reproducteur, cardiaque et nerveux. Il possède 469 muscles qui représentent environ la moitié de son poids^. Toutes ses particularités anatomiques (incapacité à vomir, possibilité de bloquer ses jambes pour dormir debout en phase de sommeil léger, etc.) résultent de sa niche écologique, celle des grands herbivores dont la fuite rapide est la seule défense^. Le pied du cheval est particulièrement important et doit faire l'objet de soins attentifs, justifiant l'expression populaire « pas de pied, pas de


Articles détaillés : races chez le cheval, cheval de selle,


Le cheval arabe est l'une des races de chevaux de selle les


Le cheval de trait est de haute taille et de forte constitution, souvent avec des fanons abondants. Ici, un Gypsy cob, une race parfois classée comme cob, d'autres fois comme trait.


Les races issues de l'espèce chevaline sont nombreuses et variées. Cette grande diversité a pour origine leur adaptation à l'environnement (aptitude à jeûner, résistance aux hautes températures ou encore sûreté de pied en terrain montagneux), et surtout l'élevage sélectif puis les croisements opérés par l'homme sur le cheval domestique. Certains traits tels la rapidité, la capacité de portage ou encore celle à tracter de lourdes charges, ont été privilégiés^. Les races sont généralement divisées en trois grandes catégories : les chevaux de trait destinés à la traction, les chevaux de selle destinés à être montés (y compris chevaux de sport pour le haut niveau) et les poneys. Les cobs, chevaux à deux fins pouvant être montés aussi bien qu'attelés, sont parfois classés à part^. Pour le cheval comme pour bon nombre d'animaux domestiques, des listes d'ancêtres ont été établies et de nombreuses races possèdent un registre d'élevage qui peut être fermé (seuls les animaux descendants d'animaux déjà enregistrés peuvent faire partie de la race) ou ouvert (le registre accepte des croisements avec d'autres races). L'inscription d'un cheval à un tel registre est soumise à des règles de signalement et de conformité au standard de race^. Ces informations sont reprises par de vastes bases de


poney Shetland^. La liste des races de chevaux est


Les poneys, comme ce Shetland, se différencient souvent des chevaux par des caractères morphologiques spécifiques.


Le poney est un cheval de petite taille, souvent avec une conformation et un tempérament particuliers. Par rapport aux chevaux, ils présentent une crinière plus épaisse, une queue et un pelage plus fournis, ainsi que des jambes proportionnellement plus courtes, un corps plus large et une ossature plus lourde^, bien que certains poneys puissent ressembler à des chevaux en modèle réduit. La Fédération équestre internationale (FEI) ne prend en compte que la taille pour définir un poney. Selon ses normes, tout cheval de moins d'1,50 m au garrot (ou 1,51 m ferré) est classé « poney », afin de faciliter les compétitions


Il y a toutefois des exceptions à cette classification, comme le


cheval miniature, malgré sa taille de 70 cm en moyenne, possède


De nombreux chevaux sont capables de retourner à l'état sauvage et de former des troupeaux. C'est le cas des mustangs aux considérés comme invasifs et provoquent des dégâts importants sur la flore et les sols^. Seul le cheval de Przewalski est


Articles détaillés : génétique équine, mulet, Bardot


Le cheval peut s'hybrider avec d'autres équidés, mais l'animal hybride est généralement stérile. Le produit d'un entier et d'une ânesse est un « bardot », celui d'un âne et d'une jument est un


Articles détaillés : Robe du cheval et Marques du cheval. Chevaux islandais aux robes variées, dont un rouan, un


La couleur des poils et des crins du cheval constituent sa robe. Très variées, elles sont un moyen d'identification de chaque animal, aussi font-elles l'objet d'une classification règlementée et d'un vocabulaire précis. Le nom des robes est basé sur la couleur des