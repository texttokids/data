Une des premières méthodes de détection d’un trou noir est la détermination de la masse des deux composantes d’une étoile binaire, à partir des paramètres orbitaux. On a ainsi observé des invisible. Le compagnon massif invisible peut généralement être interprété comme une étoile à neutrons ou un trou noir, puisqu’une masse du compagnon (ou la fonction de masses, si l’angle d’inclinaison est inconnu) est alors comparée à la masse limite maximale des étoiles à neutrons (environ 3,3 masses solaires). Si elle dépasse cette limite, on considère que l’objet est un trou noir.


On considère également que certains trous noirs stellaires apparaissent lors des sursauts de rayons gamma (ou GRB, pour gamma-ray burst en anglais). En effet, ces derniers se formeraient via l’explosion d’une dans certains cas (décrits par le modèle collapsar), un flash de rayons gamma est produit au moment où le trou noir se forme. Ainsi, un GRB pourrait représenter le signal de la naissance d’un trou noir. Des trous noirs de plus faible masse peuvent aussi être formés par des supernovæ classiques. Le rémanent de supernova SN 1987A est soupçonné d’être un trou noir, par exemple.


Un deuxième phénomène directement relié à la présence d’un trou noir, cette fois pas seulement de type stellaire, mais aussi super-massif, est la présence de jets observés principalement dans le domaine des ondes radio. Ces jets résultent des changements de champ magnétique à grande échelle se produisant dans le disque


L'objet trou noir en tant que tel est par définition inobservable ; toutefois, il est possible d'observer l'environnement immédiat d'un trou noir (disque d'accrétion, jets de matière..) à proximité de son horizon, permettant ainsi de tester et vérifier la physique des trous noirs^. La petite taille d’un trou noir stellaire difficile. En guise d’exemple, et même si la taille angulaire d’un trou noir est plus grande que celle d’un objet classique de même rayon, un trou noir d’une masse solaire et situé à un parsec (environ 3,26 années-lumière) aurait un diamètre angulaire de


Cependant, la situation est plus favorable pour un trou noir super-massif. En effet, la taille d’un trou noir est proportionnelle à sa masse. Ainsi, le trou noir du centre de notre galaxie a une masse, bien estimée, d’environ 3,6 millions de masses solaires. Son rayon de Schwarzschild est donc d’environ 11 millions de kilomètres. La taille angulaire de ce trou noir, situé à environ 8,5 kiloparsecs de la terre est de l’ordre de 40 microsecondes d’arc. Cette résolution est inaccessible dans le domaine visible, mais est assez proche des limites actuellement atteignables en interférométrie radio. La technique de l’interférométrie radio, avec une sensibilité suffisante, est limitée en fréquence au domaine millimétrique. Un gain d’un ordre de grandeur en fréquence permettrait une résolution meilleure que la taille angulaire du trou noir.


Le 10 avril 2019, le projet Event Horizon Telescope publie les premières images de M87*, le trou noir supermassif se trouvant au cœur de la galaxie M87^. Ces restitutions sont obtenues grâce à un algorithme de reconstitution d'image, baptisé « CHIRP » (Continuous High-resolution Image Reconstruction using Patch priors), mis au point par la scientifique


Ces images permettent de distinguer la silhouette du trou noir dans un


Cygnus X-1, détecté en 1965, est le premier objet astrophysique identifié comme pouvant être la manifestation d’un trou noir. C’est un système binaire qui serait constitué d’un trou noir


Les systèmes binaires stellaires qui contiennent un trou noir avec un disque d’accrétion formant des jets sont appelés micro-quasars, en référence à leurs parents extragalactiques : les quasars. Les deux classes d’objets partagent en fait les mêmes processus physiques. Parmi les micro-quasars les plus étudiés, on notera GRS 1915+105, découvert en 1994 pour avoir des jets supraluminiques. Un autre cas de tels jets fut détecté dans le système GRO J1655-40. Mais, sa distance est sujette à controverse et ses jets pourraient ne pas être supraluminiques. Notons aussi le micro-quasar très spécial SS 433, qui a des jets persistants en précession et où la matière se déplace par paquets à des vitesses de quelques fractions de


Les candidats comme trous noirs supermassifs ont premièrement été les noyaux actifs de galaxie et les quasars découverts par les radioastronomes dans les années 1960. Cependant, les observations les plus convaincantes de l’existence de trous noirs supermassifs sont celles des orbites des étoiles autour du centre galactique appelé Sagittarius A*. Les orbites de ces étoiles et les vitesses atteintes ont permis aujourd’hui d’exclure tout autre type d’objet qu’un trou noir supermassif, de l'ordre de 4 millions de masses solaires à cet endroit de la galaxie. Par la suite, des trous noirs supermassifs ont été détectés dans de nombreuses autres galaxies.