Depuis le jubilé de la reine Élisabeth II (2012), la tour de l'Horloge se nomme la tour Élisabeth (Elizabeth Tower)^.


C'est à tort que l'Elizabeth Tower est appelée parfois St Stephen's Tower. Durant le règne de la reine Victoria, les membres du Parlement tenaient leurs séances au St Stephen's Hall. Les journalistes citaient ou commentaient les comptes-rendus du Parlement comme provenant de St Stephen. La dénomination s'était étendue au Parlement tout entier. Cet usage, qui trouvait sa justification au XIX^e siècle n'a plus de raison d'être aujourd'hui. Quant aux termes de St Stephen's Tower, ils s'appliquent à St Stephen's Entrance, un bâtiment néo-gothique orné d'une tourelle à chaque angle et servant de hall d'accès (St Stephen's Porch) à la fois au Westminster Hall et au St


Manifestation sur la pelouse du jardin devant les houses of


Quelques petits jardins entourent le palais de Westminster. Les jardins de la tour Victoria (Victoria Tower Gardens), qui bordent la Tamise au sud du palais, font office de parc public. Le Black Rod's Garden, appelé ainsi en référence à l’huissier de la Chambre des lords (le Gentleman Usher of the Black Rod), est fermé au public et sert d’entrée privée. La cour du Vieux Palais (Old Palace Yard), face au palais, est pavée et recouverte par sécurité de blocs de béton. D’autres espaces comme le Jardin Cromwell (Cromwell Green) sur le devant, la Cour du Nouveau Palais (New Palace Yard) et le jardin du Speaker (Speaker's Green) au nord sont tous enclos et fermés au


Le palais de Westminster compte environ un millier de pièces, une centaine d’escaliers et 4,8 km de couloirs^. L’ensemble du bâtiment est organisé sur trois étages : le rez-de-chaussée abrite des bureaux, des salles à manger et des bars. C’est le premier étage qui réunit la plupart des salles principales du palais, dont les deux chambres parlementaires, les vestibules et les bibliothèques. On y trouve une perspective architecturale monumentale du fait de l’enfilade en droite ligne d’une longue série de pièces, à savoir la salle de Robe députés (Members’ Lobby) et enfin la Chambre des communes. Le deuxième et le troisième étage sont occupés par les bureaux des


Autrefois, en raison de son ancienne fonction de résidence royale, le palais était officiellement dirigé par le Grand Chambellan. Il a ensuite été décidé en 1965 que chaque chambre parlementaire devrait pouvoir assurer le contrôle de ses propres pièces. Le speaker et le lord chancelier exercent ainsi leur autorité au nom de leurs assemblées respectives. Le Grand Chambellan, toutefois, conserve son emprise sur certaines salles de cérémonie.


La salle accueillant la Chambre des lords est située dans la partie sud du palais. Cette pièce, somptueusement aménagée, mesure 24,4


Chambre, tout comme les autres meubles de la section du palais réservée aux lords, sont de couleur rouge. La partie supérieure de la salle est agrémentée de vitraux et de six fresques allégoriques représentant la religion, la chevalerie et la loi.


d’or. Bien que le souverain puisse en théorie assister à n’importer quelle audience, il ou elle ne se déplace que pour les cérémonies d’ouverture du Parlement. Les autres membres de la famille royale présents à cette cérémonie utilisent des chaises d’État (Chairs of State) placées à proximité. Devant le trône se situe le Woolsack, simple coussin rouge rempli de laine sur lequel s’assied le Lord Speaker, président de la chambre. Le Woolsack des Juges (Judges’ Woolsack), un coussin rouge plus large occupé par les Law Lords lors des séances d’ouverture, et la table de la Chambre


Les membres de l’assemblée siègent sur des bancs rouges répartis dans trois côtés de la salle. Les bancs situés à la droite du Woolsack forment le Côté spirituel (Spiritual Side), et ceux de gauche le Côté temporel (Temporal Side). Les Lords spirituels (archevêques et évêques de l’Église d'Angleterre) sont tous placés du Côté spirituel. Les Lords temporels (membres de la noblesse), en revanche, s’organisent en fonction de leurs allégeances politiques : les membres du parti politique au pouvoir se rangent du Côté spirituel, tandis que les représentants de l’opposition s’assoient du Côté temporel. Certains pairs sans affiliation politique siègent sur les bancs du milieu de la salle, face au Woolsack. Ils sont de ce fait surnommés les cross-benchers (littéralement les « parlementaires de