Aristote, né en Macédoine, est le plus célèbre des métèques


Si le terme est attesté dans plusieurs cités, les sources


Les étrangers jouissant de l'isotélie, les isotèles (ἰσοτελής) sont dispensés du paiement de toutes les taxes pesant sur les métèques, au premier rang de laquelle le metoikion (μετοίκιον, taxe annuelle attestée à partir du IV^e siècle av. J.-C., du droit de marché pour pouvoir commercer sur l'agora (ξενικὰ τέλη) et de la skaphēphoria, une liturgie qui leur est réservée et qui vise à rappeler leur statut inférieur : D'après Sur le Gouvernement de Démétrios de Phalère, les filles de métèques doivent porter des hydries et surtout des parasols pour les filles de citoyens lors de la procession des Panathénées^. Inversement, les isotèles sont redevables de l’eisphora (contribution de guerre) et d'epidoseis (dons volontaires) ainsi que des liturgies, tout comme les citoyens. Les isotèles restent placés sous la responsabilité de l'archonte polémarque mais n'ont plus de patron. Un métèque devient isotèle


Théophraste, dans son onzième livre des Lois, ajoute que parfois les Athéniens conféraient l'exemption à des cités entières, par exemple aux Olynthiens et aux Thébains. Il dit aussi que les isotèles sont exempts des autres taxes des métèques (taxe des commerçants en détail, et les liturgies). On peut apprendre par le discours Contre Elpagoras d'Isée à quelles charges l'isotèle restait soumis. L'isotélie est également un titre honorifique, mentionné comme tel dans les documents officiels. Dans la Grèce antique, le terme de « métèque » désigne l'étranger domicilié dans une cité autre que celle dont il est originaire. Il ne comporte alors aucune connotation péjorative, au contraire de son usage


Le mot, attesté à partir du VI^e siècle av. J.-C. vient du grec ancien L'interprétation de meta dans un sens de changement est plus vraisemblable : le métèque est « celui qui a changé de résidence ».


Les Grecs différencient le métèque, étranger résident, de l'étranger de passage. Ce dernier ne bénéficie d'aucun droit. En cas de problème, il doit s'adresser au proxène, citoyen protecteur des citoyens d'une autre cité. Ainsi, Cimon est le proxène de Sparte à Athènes. En outre, un traité bilatéral d'hospitalité (ξενία / xenía) peut être conclu entre deux cités. L'étranger de passage peut devenir métèque au bout d'un mois de résidence. Si le métèque athénien peut rester à Athènes toute sa vie, les métèques des autres cités sont moins bien lotis : les expulsions ne sont pas rares, Sparte pratiquant régulièrement la xénélasie (ξενηλασία), expulsion


terre ou de maison dans la cité) Mais ils ont le devoir de la défendre en cas de guerre - généralement dans les troupes légères ou dans la marine - et doivent payer un impôt spécial ; ils sont presque tous artisans ou commerçants. Théophraste dit que les isotèles sont exempts des autres taxes des métèques (taxe des commerçants en détail, et les liturgies). Selon les cités, le métèque peut être astreint aux liturgies, comme la chorégie. En revanche, il est exempté de la triérarchie, qui implique le commandement d'une trière. Les métèques suffisamment aisés doivent est très rare que les métèques partent effectivement en campagne. Il est toujours soumis à des contrôles, mais, plus ou moins importants selon le caractère cosmopolite ou non de la cité, et selon les conventions judiciaires bilatérales éventuelles entre cités.


On trouve souvent les métèques dans les métiers du commerce ou de la finance, et ils forment une bonne partie des employés administratifs. Nombreux sont également les métèques riches, au point que la figure du métèque parvenu et arrogant devient un classique des comédies grecques. L'étranger homme libre et grec est exclu de la sphère politique, mais il appartient à la même communauté culturelle que le