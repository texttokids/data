Le gros de la « génération des Hitlerjugend » était né entre les années 1920 et 1930. Ils formèrent la génération adulte de l'après-guerre et des années 1970 et 1980. Il n'était donc pas rare pour les anciens dirigeants de la République démocratique allemande et de l'Allemagne de l'Ouest d'avoir eu un passé chez les Jeunesses hitlériennes. Du fait que l'organisation était devenue obligatoire dès 1936, il n'y eut pas de volonté de bannir les politiques qui avaient servi dans les Jeunesses hitlériennes, à partir du moment où l'on considérait qu'ils n'avaient pas eu le choix.


L'exemple le plus patent fut celui de Manfred Rommel, fils d'Erwin Rommel, qui devint maire de Stuttgart en dépit du fait qu'il a fait partie des Jeunesses hitlériennes. Mais aussi, le ministre allemand des Affaires étrangères Hans-Dietrich Genscher, le philosophe Jürgen Habermas, et le Prince consort des Pays-Bas Claus von Amsberg. En outre, le 19 avril 2008, les médias annoncèrent que le pape de l'Église catholique romaine Benoît XVI (de son nom civil à la naissance Joseph Ratzinger) avait servi contre son gré dans les Jeunesses hitlériennes à l'âge de 14 ans. Cette information suscita une polémique selon laquelle une personne qui avait été liée d'une manière ou d'une autre au nazisme ne devrait pas devenir pape. Cependant, les faits révélèrent que Joseph Ratzinger ne partageait pas l'idéologie des nazis


Cependant, rapidement, le caractère subversif des Jeunesses hitlériennes disparaît, et cette organisation devient impopulaire au sein même des groupes qu'elle est censée encadrer. En effet, comme pour le KdF, les membres des Jeunesses hitlériennes utilisent les infrastructures pour la satisfaction de leurs besoins et désirs ; les activités d'embrigadement, les veillées, le camping, pratiqué de manière militaire, et la collecte de dons sont particulièrement


L'endoctrinement de la jeunesse, s'il se voulait totalitaire, rencontre des réserves au sein de la société allemande. Tout d'abord auprès du public que cette organisation est censée encadrer, puis au sein de la


Obligatoire à partir du décret Gesetz über die Hitlerjugend du


transforme en structure bureaucratique, ce qui détourne beaucoup de jeunes de ses rangs^. De plus, le caractère militaire de l'encadrement et des activités proposées jouent un rôle non négligeable dans la désaffection des jeunes à l'égard de l'organisation : dans le meilleur des cas, ils s'ennuient dans les veillées, ne participent pas


En outre, l'application du Führerprinzip finit par éloigner de l'organisation un nombre de plus en plus croissant de jeunes : obéissance inconditionnelle aux ordres, même lorsqu'ils semblent absurdes, et châtiments sans appel semblent la règle et incitent de


Auprès de la population, les jeunesses hitlériennes jettent le trouble au sein des familles : séparés de leur famille, les enfants sont souvent utilisés comme informateurs par le NSDAP^. Au sein de la société, lorsqu'ils sont en groupes, les membres sont souvent grossiers et sans gêne à l'encontre des gens qu'ils peuvent croiser^. De plus, indisciplinés et jouissant d'une quasi-impunité de fait, les jeunes militants de la Hitlerjugend mènent suscite de fortes réserves dans le corps enseignant^.


En 1940, Artur Axmann prend la tête des Jeunesses hitlériennes pour transformer l'organisation en une force auxiliaire utile dans un contexte de guerre. Les Jeunesses hitlériennes assistent les pompiers et l'effort de reconstruction des villes lors des bombardements alliés. Elles accomplissent des missions dans le


radiodiffusion et servent dans les équipes de défense anti-aérienne.


Vers 1943, les chefs nazis transforment les Jeunesses hitlériennes en une réserve militaire où ils puisent des troupes à la suite des pertes importantes et croissantes dues à la guerre. Ainsi la 12^e Panzerdivision SS Hitlerjugend sous le commandement de Fritz Witt est entièrement composée de jeunes garçons entre seize et dix-huit ans. Cette division est déployée pendant la bataille de Normandie contre les forces canadiennes et britanniques au nord de Caen. Pendant les mois qui suivent, la division obtient une réputation de férocité et de fanatisme. Quand Fritz Witt est tué par l'artillerie alliée, le SS-Brigadeführer Kurt Meyer en prend le commandement et devient le plus jeune commandant de division à l'âge de


Lors de l'invasion de l'Allemagne par les Alliés, la Wehrmacht recrute des membres des Jeunesses hitlériennes de plus en plus jeunes. En 1945, la Volkssturm engage dans des combats meurtriers et sans espoir des membres des Jeunesses