Aire de répartition de l'espèce Conolophus marthae selon l'UICN


Cette espèce est endémique des îles Galápagos^. L'unique population connue de cette espèce vit sur les flancs du volcan Wolf


Sa population est estimée à une centaine d'individus. L'UICN du fait du faible nombre d'individus subsistant en fait une espèce en


Son nom espagnol iguana rosada dérive de la couleur rosée de l'animal.


Cette espèce confondue avec Conolophus subcristatus jusqu'en 2008, a été décrite comme nouvelle en 2009 à la suite d'analyses génétiques. Celles-ci ont montré que cette espèce est génétiquement différente et distincte de l'autre espèce d'iguane de cette île. Les analyses suggèrent que cette espèce a divergé de ses ancêtres il y a environ 5,7


Cette découverte prouve la divergence très ancienne des iguanes de cette île, qui est également la plus ancienne trace de divergence d'espèces enregistrée pour des espèces des Galápagos^.


Cette espèce est nommée en mémoire de Martha Rebecca Gentile, la fille


Iguanidae), a new species of land iguana from the Galápagos archipelago. Zootaxa, n^o 2201, p. 1-10 (texte intégral).