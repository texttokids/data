Il n'existe aucune preuve du lieu et de la date de naissance de Billy


Authentic Life of Billy The Kid, rédigé par Ash Upson, qui écrit que Billy The Kid est né sous le nom de William Henry Bonney à New York le 23 novembre 1859^. Il semblerait que cette date ait novembre^^,^. Le nom de Bonney était le nom sous lequel


On ne sait que peu de choses de son enfance. Sa mère s'appelle Catherine McCarty et serait une immigrée irlandaise probablement née en 1829, venue aux États-Unis pour fuir la famine^. On en


McCarty ne serait pas le nom de jeune fille de sa mère^. Elle aurait été la veuve d'un nommé McCarty, mais il est possible que cet homme n'ait pas été le père de Billy the Kid^. Son père aurait pu être un certain Bonney^, ce qui pourrait expliquer l'utilisation de ce nom par Billy the Kid plus tard dans sa vie. Il a un jeune frère nommé Joseph McCarty, mais il semblerait qu'il soit son demi-frère^. Néanmoins, selon Brushy Bill (qui prétendra quelques années être le vrai Billy The Kid), Catherine McCarty n'était pas sa mère mais sa tante^. Toujours est-il qu'il porte dans son


En 1870, Catherine, sa mère qui est veuve, s'installe avec ses enfants dans le Kansas et ouvre une laverie. Comme on lui diagnostique une tuberculose, elle doit migrer vers une région plus chaude et plus sèche. La famille se rend alors dans le Nouveau-Mexique, à Santa Fé, Catherine épouse un prospecteur d'argent, William Antrim, en 1873. Parce que son beau-père s'appelle aussi William, Billy The Kid est appelé par son deuxième prénom, Henry. Sa mère meurt l'année suivante à Silver City. William Henry prend le nom de son beau-père, Antrim. Ce dernier les place lui et son frère dans des familles d'accueil et quitte Silver City pour l'Arizona. À


Henry occupe de petits emplois pour survivre, il se tourne alors vers la petite criminalité de Silver City^. Arrêté pour avoir caché le butin d'un vol commis dans une laverie chinoise, il s'évade de prison et s'enfuit chez son beau-père en Arizona. Rejeté par Antrim, il doit se débrouiller seul et gagne sa vie en travaillant dans les ranchs et en jouant aux cartes occasionnellement^.


En 1877, il tue son premier homme lors d'une bagarre, Frank « Windy » Cahill à Camp Grant. Cahill l'avait plaqué au sol et le frappait au visage. Libérant sa main, Henry lui tira une balle dans le ventre^. Billy n'a que 18 ans, mais est loin de se douter qu'il va devenir un des plus grands criminels de l'Ouest et qu'il sera abattu 3 ans plus tard environ. Recherché pour meurtre, il s'enfuit et retourne au Nouveau-Mexique dans la région de Lincoln. Henry rejoint alors un gang surnommé « The Boys »^ qui commet des vols de chevaux dans la région. Quelque temps plus tard, il est arrêté pour avoir volé des biens appartenant à un jeune entrepreneur anglais John Tunstall, qui souhaite s'établir dans la région.


John Tunstall est opposé au clan de John Dolan et Lawrence Murphy, et est appuyé par John Chisum, propriétaire d'un grand ranch de plus 100 000 têtes de bétail^ dans une guerre de territoire pour le bétail. Tunstall, qui a besoin d'hommes de main, propose à Henry de travailler pour lui en échange de l'abandon de sa plainte^. Le jeune homme accepte. C'est à cette époque qu'il


Le 16 février 1878, alors qu'il convoie neuf chevaux vers Lincoln, John Tunstall est abattu de sang-froid par la bande du shérif William J. Brady, qui travaille aux ordres de Dolan et de Murphy. Cet événement marque le début de ce que l'on va appeler la « guerre du comté de Lincoln ». Les hommes de Tunstall décident de le venger et créent une bande, les « Regulators », dirigée par Dick Brewer et Frank McNab. La bande est composée d'une douzaine d'hommes, Américains et Mexicains, dont Charlie Bowdre, Tom O'Folliard, Doc Scurlock, George Coe, Frank Coe, John Middleton, Jim French Henry Newton Brown, Fred Waite, Jesus Chavez y Chavez et le « Kid » William


Le 9 mars 1878, les « Regulators » capturent deux membres de la bande qui a tué Tunstall, Frank Baker et William Morton, et les exécutent près de Blackwater Creek. Le 1^er avril 1878, le shérif Brady et son adjoint Hindman sont abattus dans la rue principale de Lincoln au cours d'une embuscade tendue par les « Regulators »^. Lors de la fusillade, le « Kid » est atteint d'une balle à la hanche par l'un des assistants du shérif, Billy Matthews^. Mais la blessure est


Trois jours plus tard à Blazer's Mill, les « Regulators » abattent un autre homme du camp de Dolan, Andrew « Buckshot » Roberts. Lors de la fusillade, Dick Brewer est tué. McNab, promu chef de la bande à la suite de la mort de Brewer, meurt quelque temps plus tard au cours d'une fusillade au ranch Fritz qui opposaient les « Regulators » à un