Les autres volcans situés le long des fosses de subduction et ceux formés par un point chaud donnent naissance à une montagne sous-marine à sommet plat et à pente très raide : un guyot. Lorsqu'un volcan sous-marin parvient à atteindre la surface, il émerge dans une éruption de type surtseyenne. Deux volcans sous-marins sont célèbres et surveillés : le Lōʻihi qui sera le prochain volcan d'Hawaï à émerger de l'océan Pacifique et le Kick-'em-Jenny au nord de l'île de la Grenade dans les Antilles et qui est très proche de la surface et a une activité


Le massif Tamu est un volcan bouclier sous-marin considéré comme le plus vaste volcan de la Terre et l'un des plus grands du


Image satellite de l'Olympus Mons sur Mars prise par la sonde


La Terre n'est pas la seule planète du Système solaire à


Vénus connaît un intense volcanisme avec 500 000 édifices volcaniques, Mars comporte l'Olympus Mons, un volcan considéré comme éteint et haut de 22,5 kilomètres faisant de lui le plus haut sommet du Système solaire, la Lune est couverte par les


Des volcans existent aussi sur des satellites de Jupiter et


Voyager 1 a permis de photographier en mars 1979 une Triton en août 1989 des traces de cryovolcanisme et des geysers. Encelade, satellite de Saturne, est le siège de


Cryovolcanisme). La composition chimique variant considérablement entre les planètes et les satellites, le type d'éjecta est très différent de ceux émis sur Terre tel du soufre, de la glace


L'éruption d'un volcan à proximité d'une zone peuplée est très souvent vécue comme un événement majeur dans la vie d'un pays car, outre le caractère spectaculaire et inattendu d'une éruption, celle-ci nécessite une surveillance et, parfois, l'évacuation et la prise en


Les volcans sont parfois les acteurs principaux de certains films catastrophes comme Le Pic de Dante et Volcano ou le docu-fiction Supervolcan de la BBC et de Discovery Channel qui met en scène le réveil du supervolcan de Yellowstone dans une éruption d'indice d'explosivité volcanique de 8. Le film Stromboli raconte l'histoire d'une femme étrangère qui ne parvient pas à s'intégrer sur l'île volcanique Stromboli, en raison de différences de mentalité avec ses habitants, y compris son mari qu'elle a épousé dans la précipitation dans un camp de prisonnier.


Plus couramment, les volcans font l'objet de nombreux documentaires télévisés scientifiques, informatifs ou de


Fournaise se disputent le record avec une éruption tous les un an à


terrestre : Toba formé il y a 73 000 ans avec cent kilomètres


Sumbawa en Indonésie en 1816 avec 88 000 morts liés directement à l'éruption et 200 000 morts supplémentaires par


entendue jusqu'à l'île Rodrigues à 500 kilomètres à l'est de l'île Maurice, soit à 4 811 kilomètres de distance de


Nouvelle-Zélande avec une hauteur estimée à cinquante