les notions de respiration et de production d’oxygène par les plantes et l'importance de la lumière dans ce dernier phénomène. Ce sont d’abord deux chimistes anglais : Stephen Hales en 1727, qui pense que l'air et la lumière contribuent à la croissance des plantes, et Joseph Priestley entre 1771 et 1777 qui met en évidence le rejet d’oxygène. À leur suite, Jan Ingen-Housz, médecin et botaniste hollandais, établit en 1779 le rôle de la lumière dans la production d’oxygène par les plantes. Puis Jean Senebier, un pasteur suisse, à partir des travaux d’Antoine Lavoisier sur la composition de l'air, comprend que les plantes consomment du dioxyde de carbone et


démontre la consommation d’eau lors de la photosynthèse. La chlorophylle est isolée par des chimistes français en 1817, Pierre Joseph Pelletier et Joseph Bienaimé Caventou. comprises, transformation de l'énergie lumineuse, consommation d’eau et de dioxyde de carbone, production d’amidon et rejet de dioxygène. L'expérience cruciale d’Engelmann (1882), où des bactéries servent d’indicateur de la production d’oxygène, montre


du processus s’établit. Le début du siècle voit la description de la structure chimique de la chlorophylle puis la découverte de l'existence des types a et b. Robert Emerson établit en 1932^ que 2 500 molécules de chlorophylle sont nécessaires


les années 1930, les travaux de Robert Hill permettent d'y voir plus clair. À l'issue de ses expériences, la photosynthèse se présente comme une réaction d'oxydo-réduction au cours de laquelle le carbone passe d'une forme oxydée à une forme réduite :


où il n'y a pratiquement plus de lumière. Certaines algues


chroomonas sp.), à température ambiante et en condition d'illumination défavorable, sont capables, grâce à des « antennes » protéiques, et semble-t-il grâce à des protéines (bilines) utilisées en plus de la chlorophylle, de mieux capter la lumière et d'utiliser la cohérence quantique pour optimiser leur utilisation du rayonnement solaire incident. Cela leur permet de conduire plus de 95 % des photons jusqu'à leur « but »^. Ce phénomène pourrait exister chez d'autres végétaux, voire être


l'Université de Göteborg, en collaboration avec des équipes de l'École polytechnique Chalmers et d’autres universités européennes, ont observé grâce aux puissants rayons X de l'European Synchrotron Radiation Facility, les mouvements des atomes au sein de protéines engagées dans un processus de photosynthèse. L'expérience qui a fourni des informations en trois dimensions sur des mouvements de l’ordre de 1,3 angström de molécules, pourrait servir à créer des dispositifs faisant de la photosynthèse artificielle afin de produire l’énergie du futur à


Quand l'intensité lumineuse le permet, des chapelets de bulles d'oxygène natif (issue de la photosynthèse) peuvent être observées (ici


phytoplancton marin qui produisent le plus d’oxygène, suivi des forêts. On a longtemps cru que les mers froides et tempérées une étude de 2009^ montre que les océans subtropicaux oligotrophes sont également producteurs d’oxygène, bien qu'ayant une production saisonnière irrégulière. Ces océans jouent donc un rôle en termes de puits de carbone. Pour le sud de l'hémisphère nord, la production d’oxygène est basse en début d’hiver, augmente jusqu’en


même on a longtemps cru que l'oxygène n'était produit que dans les couches très superficielles de l’océan, alors qu'il existe


Dans les zones de dystrophisation ou dans les zones mortes de


Le flux d’énergie capté par la photosynthèse (à l’échelle planétaire) est immense, approximativement 100 térawatts^ : qui est environ de 10 fois plus élevé que la consommation énergétique mondiale (intégrée sur un an). Ce qui signifie qu'environ un peu moins du millième de l’insolation reçue par la Terre est captée par la photosynthèse et fournit pratiquement toute l’énergie de la


Le processus de production d'oxygène peut être inhibé par des causes externes, avec par exemple une pollution particulaire importante rayons solaires (les bloquer éventuellement) ou nuire aux échanges feuille/atmosphère ; de même pour la turbidité de l'eau pour les


Certains pesticides détruisent des mécanismes biochimiques, enzymatiques ou métaboliques élémentaires nécessaires à la photosynthèse, de même que certains phytopathogènes.