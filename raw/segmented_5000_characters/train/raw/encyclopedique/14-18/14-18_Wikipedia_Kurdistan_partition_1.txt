Juste après la Seconde Guerre mondiale, les Kurdes d’Iran proclament une république kurde indépendante à Mahabad


autonomie relative au Kurdistan, avec la « Loi pour l’autonomie dans l’aire du Kurdistan » qui dispose notamment que « la langue kurde doit permet aussi l’élection d'un conseil législatif autonome qui contrôle son propre budget. Cependant 72 des 80 membres élus de ce conseil de la première session d'octobre 1974 ont été sélectionnés par Bagdad. En octobre 1977, la totalité du conseil est choisie par le régime.


Les relations avec les Kurdes d’Irak se dégradent considérablement par la suite. Le 16 avril 1987, Saddam Hussein lance un raid à l’arme chimique sur la vallée du Balisan. Au cours de l’opération Anfal, 182 000 personnes périssent dans des bombardements chimiques^. En décembre 2005, une cour de La Haye a qualifié cette campagne de « génocide ». Le 24 juin 2007, le tribunal pénal irakien a condamné Ali Hassan al Madjid, surnommé « Ali le chimique », et deux autres anciens hauts dignitaires du régime de Saddam Hussein, à la peine de mort par pendaison pour le génocide commis contre les Kurdes au cours de cette opération


Au printemps 1991, à l’issue de la première guerre du Golfe, Saddam Hussein réprime sévèrement les populations kurdes (ainsi


Le Kurdistan est devenu une zone de conflit intense impliquant les différents pays limitrophes, mais aussi les États-Unis depuis le début du conflit avec l’Irak en 1991. Cette situation a entraîné un accroissement de l’émigration kurde vers les pays de la région ou vers l’Europe. Entre avril et juillet 1991, la France met en place l’opération « Libage », une mission humanitaire de l'armée française destinée à porter secours aux populations kurdes irakiennes qui se dirigeaient vers la Turquie. Pour sa part, la Centrale sanitaire suisse apporte une aide médicale à ces populations en


Débutée en 2011, la guerre civile syrienne bouleverse les régions kurdes tant syriennes qu'irakiennes. Les Kurdes de Syrie ont profité des désordres de la guerre civile pour prendre le contrôle d'un vaste territoire, le « Kurdistan syrien »^. Depuis le 12 novembre 2013, ce dernier dispose d'une administration autonome, qui gère les questions « politiques, militaires, économiques et de sécurité


Le Kurdistan syrien, appelé le Rojava, passe aux mains du Parti de l'union démocratique (PYD), la branche syrienne du Parti des travailleurs du Kurdistan (PKK). Il dispose d'une branche armée, les Unités de protection du peuple (YPG). Les relations des Kurdes avec les autres parties prenantes au conflit sont fluctuantes : plutôt proches de l'ASL, les YPG entrent en conflit avec des brigades islamistes en juillet 2013^. Jouant leur propre carte, les rebelles kurdes concluent parfois des alliances ponctuelles et opportunistes, tantôt avec les forces loyalistes, tantôt avec les rebelles^. Hostiles au régime de Bachar el-Assad, dont ils souhaitent la chute, les Kurdes du PYD affrontent cependant rarement les forces loyalistes avec lesquelles ils cohabitent dans certaines villes. Les YPG livrent l'essentiel de leurs combats contre


L’offensive de Daech dans la région de Mossoul, en juin 2014, crée un front entre les Kurdes et les djihadistes de l’Armée islamique. Une coalition internationale (qui regroupe notamment les aide aérienne et logistique dans les combats contre Daech^^,^. Depuis l'offensive de Daech, la plupart des chrétiens irakiens (200 000 environ) se sont réfugiés dans des


En 2015, les effectifs des YPG sont estimés entre 35 000 et


Le Kurdistan est une région montagneuse et de hauts plateaux d'Asie centrale. Les estimations de la superficie du Kurdistan sont variées : l’Encyclopædia Britannica cite 191 660 km^2^, Jacques Leclerc parle de 350 000 km^2^ et l'observatoire franco-kurde de 500 000 km^2^. Le territoire du Kurdistan s'étend de la Turquie à l'ouest jusqu'en Iran (golfe Persique) en passant par l'Irak et la Syrie, avec quelques îlots de peuplements kurdes en Arménie, Géorgie, Azerbaïdjan, Turkménistan, Kirghizistan et


Les régions principales du Kurdistan se situent dans le nord de l'Irak (Kurdistan irakien), dans les monts Zagros à l'ouest de l'Iran (Kurdistan iranien), au sud-est de la Turquie (Kurdistan turc) ainsi qu'au nord-ouest et au nord-est de la Syrie. Les superficies des différentes parties du


Zones géographiques Superficie en km^2 Pourcentage du Kurdistan


Kurdistan oriental (iranien) 195 000 38,77 % 11,83 %


Kurdistan occidental (syrien) 15 000 2,98 % 8,10 %


Le dialecte Mukriani est parlé dans les villes de Piranshahr et de Mahabad. Piranshahr et Mahabad sont deux principales villes de Mukrian.