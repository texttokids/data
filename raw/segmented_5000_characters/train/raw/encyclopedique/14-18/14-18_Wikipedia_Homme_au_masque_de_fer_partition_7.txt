Dauger avait été arrêté près de Dunkerque en juillet 1669 sur la base d'une lettre de cachet, dont Jean-Christian Petitfils a montré qu'elle arrestation a été minutieusement organisée par Louvois, alors secrétaire d'État de son père, Michel Le Tellier.


On ne sait rien de ce Dauger. Dans la lettre qu'il envoie à Saint-Mars pour faire préparer son cachot à Pignerol, Louvois indique : « ce n'est qu'un valet ». L'intéressé savait pourtant lire, puisqu'il fut autorisé Masque de fer et Dauger est désormais la plus généralement admise, les spéculations se sont portées sur l'identité véritable de Dauger et sur


incarcéré pour avoir été le chirurgien Auger, l'un des acteurs de


Louis XIII serait le père de Louis et Eustache Oger de Cavoye. Rupert Furneaux a retrouvé un tableau représentant Louis Oger de Cavoye. La ressemblance entre Louis XIV et Louis Oger de Cavoye serait la preuve d'un lien de sang entre Louis XIV


révolutionnaire, 1974), François de Cavoye, mari d'une dame d'honneur de la reine (Marie de Lort de Sérignan), et capitaine des mousquetaires de Richelieu, était l'amant occasionnel d'Anne d'Autriche et serait le vrai père de Louis XIV. Ainsi Eustache Dauger de Cavoye (né le 30 août 1637) serait le demi-frère de Louis XIV (les deux ayant le même père, qui n'était pas le roi, mais non pas la même mère), et lui ressemblait beaucoup, ce qui expliquerait


du Masque, 1998), Anne d'Autriche aurait été enceinte deux fois des face à un risque de répudiation, mettant enfin au monde Louis XIV et Philippe d'Orléans, des fils « Cavoye » et non des Bourbon. Or, François de Cavoye avait déjà une progéniture dont deux fils, Louis et Eustache, qui ressemblaient étrangement au roi. Une confidence de leur père aurait tout déclenché. La question, selon Jean d'Aillon, est donc de savoir si Eustache Dauger était Eustache Dauger de Cavoye, le frère de « l'Ami du Roy », Louis de Cavoye, qui avait disparu justement en juillet 1669. Pour Jean d'Aillon, Eustache fut probablement emprisonné pour avoir essayé de menacer le roi Louis XIV en révélant qu'il était son demi-frère, et non le fils de Louis XIII. Le masque de fer était alors nécessaire pour que personne ne découvre la ressemblance, car Eustache ressemblait encore plus au roi que son frère. Louis XIV ne l'aurait pas fait tuer car ils étaient frères. Pour la même raison, il couvrit de


d'aujourd'hui publié en 2006, Henri Lamendin reprend la thèse de Marie Madeleine Mast. Parlant de la grossesse d'Anne d'Autriche, il entre autres, dans l'entourage de la reine, une de ses dames d'honneur et son mari François Dauger de Cavoye, lesquels avaient déjà huit enfants ». Et certains auteurs ont avancé que ce dernier aurait pu être le géniteur opportun de l'enfant que l'on n'attendait plus. Pour étayer cette thèse, parmi beaucoup d'autres Louis XIV (qui en avait pris conscience) de deux des enfants Dauger de Cavoye (Eustache, né en 1637 et Louis, né en 1639), compagnons de jeux du jeune roi dans son enfance. Des portraits de Louis XIV et de Louis Dauger de Cavoye attestent de la frappante ressemblance de l'ensemble de leurs visages, dont « le même dessin de leur bouche et un petit creux identique sous la lèvre inférieure ». A contrario, « on ne peut imaginer visages plus dissemblables que ceux de Louis XIII et de Louis XIV. (…) Par ailleurs, il n'a jamais disparu et dont personne ne sait ce qu'il en advint ».


Les théories de Maurice Duvivier, Rupert Furneaux, Jean d'Aillon et Marie Madeleine Mast ont en commun le fait de considérer qu'Eustache Dauger (ou d'Oger ou Oger) de Cavoye et Eustache Dauger de Pignerol sont la même personne. C'est l'historien Maurice Duvivier qui a découvert l'acte de baptême d'Eustache Dauger de Cavoye.


né le 30 août 1637, fils de Francois Dauger, escuier, sieur de Cavouet, capitaine des mousquetons de Monseigneur le Cardinal de Richelieu, et de dame Marie de Sérignan, demeurant rue des Bons-Enfants. »


En 1659, il participe à l'Orgie de Roissy. En 1665, il tue un page et il est alors renié et déshérité par sa famille soit 4 ans avant l'arrivée d'Eustache Dauger à Pignerol. Officiellement Eustache Dauger de Cavoye est mort à la prison de Saint-Lazare en 1680 et Jean-Christian Petitfils affirme que des preuves de sa présence dans cette prison bien après l'apparition du Masque de Fer attestent de