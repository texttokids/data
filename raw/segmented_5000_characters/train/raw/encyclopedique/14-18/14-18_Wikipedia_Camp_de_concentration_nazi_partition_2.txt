Article connexe : Système de marquage nazi des prisonniers. Signes distinctifs des catégories de détenus dans les camps de


Immédiatement après l'incendie du Reichstag, le 28 février 1933, près de 4 000 membres du Parti communiste d'Allemagne sont internés dans les camps^. Son président, Ernst Thälmann est interné, passe de prison en prison pour aboutir à Buchenwald où il est assassiné en août 1944^. La création du Deutsche Arbeitsfront le 10 mai 1933, qui supprime les syndicats, puis l'instauration du NSDAP comme parti unique, le 14 juillet 1933 permettent d'arrêter à leur tour les membres des partis interdits, au premier rang desquels ceux du Parti socialiste et les militants


Ces militants de gauche sont rejoints par tous les opposants politiques au nazisme, membres d'un parti politique interdit ou sans appartenance précise^. La catégorie des politiques reprend également des membres du parti nazi ayant commis des délits au sein de celui-ci, des soldats de la Wehrmacht condamnés pour vol, désobéissance grave ou désertion, mais aussi des allemands qui ont enfreint la législation sur les devises ou écouté des radios étrangères, ou des personnes dénoncées rapport avec une quelconque opposition politique^.


Presque tous les étrangers internés après le déclenchement de la seconde guerre mondiale sont également versés dans la catégorie des détenus politiques, de même que 4 000 à 5 000 pasteurs protestants et


comprennent d'une part les détenus internés par mesure de sécurité purgent leur peine dans un camp de concentration alors que leur place est en prison. D'autre part, on y retrouve les « détenus préventivement initiales en allemand signifient également « criminels professionnels » droit commun. Cette deuxième catégorie est le vivier des Kapos les


Le triangle bleu correspond au marquage pour les apatrides (notamment, les républicains espagnols déchus de leur nationalité par Franco).


Article détaillé : Témoins de Jéhovah sous le IIIe Reich.


Après l'incarcération des dirigeants des Témoins de Jéhovah à Magdebourg en 1934, une vague d'arrestations frappe leurs membres en 1936, amplifiée en 1937 à la suite d'une ordonnance du ministre de


Poursuivis pour leur refus absolu de porter les armes ou de prêter serment au national-socialisme, environ 3 000 Bibelforscher sont


Le triangle rose (en allemand : Rosa Winkel) était dans l'univers concentrationnaire nazi le symbole utilisé pour marquer les homosexuels. De taille supérieure aux autres triangles, ce symbole de persécution, de discrimination, a été repris par la communauté


camps nazis, les déportés homosexuels doivent porter un triangle rose, pointe tournée vers le bas, qui les identifie comme tels. La hiérarchie concentrationnaire les place au plus bas de l'échelle sociale des


Les « asociaux » constituent sans doute la catégorie la plus hétérogène au sein des déportés. On y trouve notamment des vagabonds, braconniers, voleurs à la tire, ivrognes, souteneurs mais aussi des personnes dénoncées à la Gestapo par de « bons nazis » pour être arrivées en retard à plusieurs reprises à leur travail, avoir pris congé sans avertissement préalable ou avoir déplu à leur patron^.


Cette section est vide, insuffisamment détaillée ou incomplète.


Dans la typologie raciale nazie, les Tziganes étaient classés comme une catégorie « hybride » dont la non-sédentarité caractérisait la séparément, dans le Zigeunerfamilienlager. D'autres camps d'internement provisoires étaient construits en particulier en Europe centrale où les Tziganes étaient parqués avant de partir pour les camps de


La politique d'annihilation des populations tziganes en Europe durant la Seconde Guerre mondiale est appelée Porajmos, et a été qualifiée de génocide à plusieurs reprises en Allemagne, notamment en


Red triangle repeater.svg Green triangle repeater.svg Blue triangle repeater.svg Purple triangle repeater.svg Pink triangle repeater.svg Black triangle repeater.svg


S'ajoute au-dessus du triangle une barre rectangulaire de même couleur


Red triangle penal.svg Green triangle penal.svg Blue triangle penal.svg Purple triangle penal.svg Pink triangle


triangle jew.svg Purple triangle jew.svg Pink triangle jew.svg Black triangle jew.svg Brown triangle jew.jpg


Un triangle jaune inversé sous le triangle d'une autre couleur pour les Juifs entrant dans une autre catégorie. Ces deux triangles forment l'étoile de David. Les déportations de Juifs débutèrent en 1941 et faisaient suite aux persécutions dans les ghettos et à des exécutions