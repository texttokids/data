La crise financière majeure qui frappe les marchés mondiaux à la suite de la crise des subprimes (février 2007) a par ailleurs contribué à un regain important de critiques envers le capitalisme et l'« ultralibéralisme ». Alan Greenspan, président pendant 18 ans de la Réserve fédérale et libertarien proclamé qui défendait la supériorité de l'autorégulation des marchés sur la régulation étatique, a estimé le 23 octobre 2008 face à la commission de contrôle d'action gouvernementale qu'il avait eu « partiellement tort » de faire plus confiance au marché qu'au gouvernement pour réguler le système financier. Il a par ailleurs fait part de son désarroi : « J'ai trouvé une faille [dans mon idéologie]. Je ne sais pas à quel point elle est significative ou durable, mais ce fait m'a plongé dans un grand désarroi » Greenspan utilise ici le mot idéologie non comme un ensemble de croyances irréfutables mais comme le cadre conceptuel à travers lequel il explique le monde. Greenspan est fortement critiqué, en particulier par les libéraux américains[réf. souhaitée], pour être un des responsables de la crise en ayant conduit une politique monétaire laxiste, par absence de véritable indépendance par rapport au pouvoir politique, à l'origine de l'explosion du crédit (baisse systématique des taux pour entretenir la croissance). Il est à noter que la politique monétaire expansionniste est contraire aux principes libertariens de laissez-faire en matière de création monétaire. Greenspan, plutôt que de trouver une faille dans son idéologie, ne l'a tout simplement pas appliquée. Sa politique monétaire relevant plutôt de l'interventionnisme selon les économistes de l'école autrichienne.


Selon certains analystes et critiques (par ex. Alain Touraine), le système économique a subi une dérive financière qui l'a éloigné du capitalisme. Le développement de la financiarisation a conduit à une économie d'endettement généralisé, s'éloignant d'une éthique capitaliste où les risques sont principalement assumés par ceux qui fournissent un capital stable.


Pour d'autres[Qui ?], la spéculation financière est inhérente au capitalisme, et la grande place accordée à la finance dérégulée a été un des leviers du capitalisme pour maintenir un taux de profit croissant malgré le ralentissement de la croissance à la fin des années 1970.


Le philosophe André Comte-Sponville avait posé la question de savoir si le capitalisme était moral dès avant la crise économique de 2008. Selon lui, il est d'autant plus question de morale dans les discours et les préoccupations que la morale fait défaut dans les comportements humains.


Il voit trois raisons complémentaires à ce retour de la morale, associées à trois temporalités différentes :


Dans le court terme (depuis vingt ou trente ans), le passage de la génération des soixante-huitards aux nouvelles générations moins politisées marque le retour d'une certaine morale ; Dans le moyen terme (depuis le XXe siècle), le triomphe du capitalisme l'a rendu moralement moins justifiable que lorsque l'adversaire communiste lui tenait lieu de faire-valoir ; Dans le long terme (depuis la Renaissance et le XVIIe siècle), la mort sociale de Dieu correspond à un processus de laïcisation, de sécularisation, et donc, s'agissant de la France, de déchristianisation, qui pose toutes sortes de problèmes tournant autour de la communauté, qui n'est plus fondée sur une communion religieuse.


Il remet en perspective le problème des limites opposées au comportement humain en distinguant une hiérarchie de quatre ordres au sens pascalien du terme :


l'ordre techno-scientifique (auquel appartient l'économie en tant que science) ; l'ordre juridico-politique ; l'ordre de la morale (ce qui relève du devoir) ; l'ordre de l'éthique (ce qui relève de l'amour).


Même si les ordres de la morale et de l'éthique ont des préoccupations plus élevées, l'ordre no 1 (économico-techno-scientifique) n'est pas soumis à l'ordre no 3 (l'ordre de la morale) du fait de structurations internes différentes : le possible et l'impossible n'ont que faire du bien et du mal.


Un système addictif