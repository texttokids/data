Bien qu'Einstein ait rencontré un grand nombre de personnalités majeures de son époque, dans les domaines scientifique, politique et artistique, laissant une correspondance très riche, il se décrivait lui-même comme un véritable solitaire : « Je me sens lié réellement à l'État, à la patrie, à mes amis, à ma famille au sens complet du terme ; mais mon cœur ressent face à ces liens un curieux sentiment d'étrangeté, d'éloignement, et l'âge accentue encore cette distance. »


Parmi ses relations célèbres, on compte une amitié avec la reine Élisabeth de Belgique, avec qui il joua du violon, Arnold Berliner dont il témoigne de l'affection lors de son 70e anniversaire George Bernard Shaw au sujet duquel il écrit « on trouve rarement des hommes assez indépendants pour s'apercevoir des faiblesses et des sottises de leurs contemporains, sans en être affectés eux-mêmes » ou Bertrand Russell.


Modeste et pensant quant à lui que « Chacun doit être respecté dans sa personne et nul ne doit être idolâtré », il ironisait au sujet de sa célébrité et de ses effets : « Cela pourrait bien provenir du désir irréalisable pour beaucoup, de comprendre quelques idées que j’ai trouvées, dans une lutte sans relâche, avec mes faibles forces ».


Sa première épouse, Mileva Maric est atteinte de coccygodynie, ce qui la rend boiteuse. C’est aussi une jeune femme brillante, élève du Polytechnicum. Elle tombe enceinte alors qu’ils ne sont pas encore mariés, et elle accouche en janvier 1902 chez ses parents, en Serbie, d’une fille prénommée Lieserl (Élisabeth) et dont on perd la trace. Einstein se montra très dur avec elle, ainsi qu'avec sa compagne suivante, Elsa (doublement sa cousine).


Il voit peu son fils Hans-Albert (né en 1904) qui, à l’âge adulte, travaille en Californie. La santé mentale de son autre fils, Eduard, né en 1910, se détériore brutalement alors qu’il est âgé de vingt ans, et il doit être interné une première fois en 1930 à la clinique psychiatrique universitaire de Zurich, où les médecins lui diagnostiquent une schizophrénie. Son père lui rend une dernière visite en 1933. Eduard meurt dans cette clinique en 1965. D’abord critique envers la psychanalyse (« Il n'est peut-être pas toujours bon de fouiller dans l'inconscient. Croyez-vous que connaître le mouvement de tous les muscles qui composent nos jambes nous aiderait à marcher ? »), il refuse que son fils Eduard suive un nouveau traitement psychanalytique[réf. nécessaire]. En 1933, il choisit cependant Sigmund Freud pour publier un échange de lettres intitulé Pourquoi la guerre ?. Einstein et la religion


Einstein écrit plusieurs textes traitant des relations entre science et religion. Dans son article paru en 1930 Einstein distingue trois formes de religion :


 la première est due à la crainte et à une incompréhension de la causalité des phénomènes naturels, d’où l'invention d’êtres surnaturels ; la deuxième est sociale et morale ; la troisième, qu’Einstein appelle « religiosité cosmique », est une contemplation de la structure de l'Univers. Elle est compatible avec la science et n'est associée à aucun dogme ni croyance. Einstein déclare être religieux, mais seulement dans ce troisième sens qu’il voit dans le mot religion.


Lorsque, en 1929, le rabbin (en) Herbert S. Goldstein lui demande « Croyez-vous en Dieu ? », Einstein répond : « Je crois au Dieu de Spinoza qui se révèle lui-même dans l’ordre harmonieux de ce qui existe, et non en un Dieu qui se soucie du destin et des actions des êtres humains. ».


Einstein se réclame également du panthéisme de Spinoza dans son ouvrage Comment je vois le monde. Il définit le sentiment religieux du scientifique comme la croyance en l'intelligibilité du monde, et en une « raison supérieure » qui se dévoile dans « le monde de l'expérience ». Selon lui, les religions traditionnelles relèvent de l'histoire et de la psychologie.


Einstein a souvent utilisé le mot Dieu, comme dans ses célèbres formules « Dieu est subtil, mais pas malicieux » ou « Dieu ne joue pas aux dés », cependant le sens qu’il donnait à ce mot fait l’objet de diverses interprétations. Une partie du clergé a considéré que les vues d’Einstein étaient compatibles avec la foi. À l’inverse, le Vatican dénonce alors « un authentique athéisme même s'il est dissimulé derrière un panthéisme cosmique ». Si Einstein rejette les croyances traditionnelles, il se distingue personnellement des athées et répète qu’il est « un non-croyant profondément religieux ».


Une lettre manuscrite écrite en allemand un an avant sa mort, et adressée au philosophe Eric Gutkind, a été vendue sur eBay en octobre 2012 pour la somme de 3 000 100 $US. Einstein y écrivait :


 « Le mot Dieu n’est pour moi rien de plus que l’expression et le produit des faiblesses humaines, la Bible un recueil de légendes, certes honorables mais primitives qui sont néanmoins assez puériles. Aucune interprétation, aussi subtile soit-elle, ne peut selon moi changer cela. »