La croissance géométrique de la population est donc une réalité théorique qui ne s'est pas encore traduite dans les faits dans les pays industriels. Contrairement à ce que pensait Malthus, avant d'être limitée par la productivité agricole, elle l'a plutôt été par des phénomènes socio-culturels complexes, liés à la culture, à l'enrichissement de la société, aux progrès de la contraception et à des choix d'organisation sociale qui amènent les familles à avoir moins d'enfants ou à ne pas en avoir. Des spécialistes de la reproduction notent aussi depuis quelques décennies, pour des raisons apparemment environnementales, et au moins dans les pays industriels, une diminution de la fécondité naturelle des individus (délétion de la spermatogenèse chez l'homme et moindre fertilité chez la femme).


Si l'analyse de Malthus correspond bien à l'évolution de la population et des ressources par le passé (il s'était entre autres fondé sur de copieuses données concernant les États-Unis, accumulées par Benjamin Franklin), elle devient caduque au moment même où elle est publiée car c'est alors que s'amorce la transition démographique, qui aboutit à une réduction plus ou moins « volontaire »


Bien que le modèle de Malthus soit exact (à fécondité maximale, tous les descendants d'une génération ne peuvent survivre), ses prévisions ne se sont pas réalisées. Les éléments nouveaux ont été : moitié de l'humanité est déjà au-dessous du seuil de remplacement » des générations (Gilles Pison (INED, Six milliards d'hommes).


période où deux hommes sur trois souffraient de malnutrition officiellement adopté des politiques malthusiennes l'équation sur laquelle Malthus avait basé son raisonnement en :


mais avec des conséquences environnementales, climatiques et sociales qui sont aujourd'hui mesurables notamment dans les théories de l'effondrement ne relevant pas de la preuve scientifique directe, mais s'appuyant sur des indices mesurables et des études documentées.


Fort du scandale provoqué par son Essai, Malthus passe le reste de sa vie à lui donner une apparence moins littéraire et plus scientifique, et à acquérir dans un domaine voisin, mais différent, l'économie, par ses traités et sa correspondance avec David Ricardo, une grande


Il en vient ainsi à considérer que la loi des débouchés est


l’extension qu’on lui a donnée me semble tout à fait fausse, et en contradiction manifeste avec les grands principes qui règlent


Au contraire, selon Malthus, l'offre ne crée pas forcément la demande, le niveau de la production et celui de la demande ne sont pas


produits, mais beaucoup s’échangent contre du travail ne se traduisant pas par un bien matériel (comme celui des domestiques),


Un recul de la demande (demande effective) est donc possible avant une baisse de production (voire, paradoxalement, à la suite d'un accroissement de production), ce qui provoquera un recul de l'activité Sismondi développe aussi cette idée à ce moment) à tenter de théoriser les crises dites de surproduction, notion que récuse Jean-Baptiste Say. Cette idée sera reprise et développée par John Maynard Keynes pour analyser la crise de 1929, ce qui fait de Malthus un annonciateur du keynésianisme. Keynes écrira d'ailleurs un essai en 1933 intitulé "Robert Malthus, the first of the Cambridge economists" (Robert Malthus, le premier des économistes de


Principle of Population, as it Affects the Future Improvement of Society with Remarks on the Speculations of Mr. Godwin, M. Condorcet, and Other Writers London, printed for J. Johnson, in St.


complétée et publiée avec le nom de l'Auteur : An essay on the Principle of Population; or, a view of its past and present effects on human happiness ; with an enquiry into our prospects respecting the future removal or mitigation of the evils which it occasions. Nouvelles éditions anglaises avec de légères variations par rapport Traduit en français en 1805 sous le titre : Essai sur le


principles by which it is regulated (énonce la théorie


Investigation of the Couse of the Present High Price of Provisions)


or Fall in the Price of Corn on the Agriculture and General Wealth


Importation of Foreign Corn: intended as an Appendix to" (1815) application aux altérations de la valeur de la monnaie anglaise


l'Encyclopedia Britannica. Un long extrait de ce texte fut publié en 1830 sous le titre de "A summary view of the Principle of