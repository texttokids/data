En liberté, le mâle manifeste son activité sexuelle dès l'âge d'un an à dix-huit mois^, et la jument est apte à pouliner dès deux ans^. En captivité, entiers et juments sont rarement autorisés s'effectue de plus en plus souvent par insémination artificielle en sperme congelé. Une éjaculation d'un étalon est en moyenne de 70 ml. Cette technique permet aux éleveurs de disposer plus facilement d'un large choix de géniteurs mâles pour leurs poulinières. Pour des raisons économiques, certains éleveurs recherchent une naissance précoce au début de l'année, et parviennent à déclencher des chaleurs chez la jument en jouant par exemple sur l'intensité de


La durée de gestation est en moyenne de onze mois, soit 330 jours^. La jument donne naissance à un poulain à la fois, sauf exception, généralement au printemps. Il peut marcher moins d'une heure après la naissance, et doit téter le colostrum de sa mère


Un cheval de quatre ans est généralement considéré comme adulte, bien que son squelette continue de se développer jusqu'à huit ans. Sa croissance dépend étroitement de sa taille, sa race, son sexe, et la qualité des soins qui lui sont prodigués. En fonction de la maturité, de la race, et du travail attendu, les chevaux sont d'ordinaire débourrés et montés entre deux et quatre ans. Bien que les Pur-sangs, réputés pour leur précocité, puissent être montés dès deux ans dans certains pays, les chevaux de sport équestre ne le sont pas avant trois ou quatre ans car leurs os et leurs muscles ne sont pas totalement développés. En compétition d'endurance, les chevaux ne sont pas autorisés à concourir avant cinq ans révolus. L'âge où vient la vieillesse n'est qu'une notion subjective, mais elle se situe le plus souvent entre 16 et 20 ans^. Il n'existe ni race précoce ni race tardive, tous les chevaux ont la même croissance. Les os de la colonne vertébrale sont les derniers à se solidifier (vers 7-8 ans) c'est pourquoi débourrer trop tôt est dangereux pour le cheval.


Les chevaux qui jouent peuvent se cabrer lors de simulacres de combat.


Les différentes façons dont le cheval se meut sont nommées allures. Tous les chevaux en possèdent naturellement trois. Le pas, la plus lente, est en quatre temps et correspond à une vitesse de 8 ou 9 kilomètres par heure. Le trot, allure intermédiaire et sautée à deux temps, permet habituellement d'atteindre une vitesse de 15 à 18 km/h^. Le galop, la plus rapide, est une allure en trois temps, basculée et sautée, permettant d'atteindre une vitesse moyenne de 20 à 25 km/h, jusqu'à 60 km/h chez le Pur-sang^. Certains chevaux sont capables d'aller l'amble, allure où les deux membres d'un même côté se déplacent


Le cheval saute naturellement les obstacles qui se présentent à lui, et effectue parfois des sauts sur place^. Il connaît le cabrer et la ruade, mouvements qui témoignent généralement d'une volonté d'attaque ou de défense de sa part^.


Articles détaillés : Airs relevés et Rassembler.


Le dressage permet d'apprendre de nouveaux mouvements au cheval. L'apprentissage du rassembler est souvent nécessaire afin de les obtenir. Le pas espagnol est un pas lent caractérisé par une forte extension des membres antérieurs, le passage, un trot majestueux,


classique inclut aussi des airs relevés travaillés à partir du cabrer et de la ruade, comme la levade, la croupade, la


Les allures peuvent présenter des irrégularités, telles l'aubin


Articles détaillés : Sens du cheval et œil du cheval.


Le cheval possède cinq sens, mais l'existence d'un sixième sens lui permettant de prévoir le climat ou un danger est souvent


angle de vue de 340 degrés, mais son acuité visuelle est moyenne à médiocre^ bien qu'il voie très clair durant la nuit^. Son ouïe, très fine^, lui permet de prévoir les tremblements de terre, de percevoir les ultrasons et de détecter les prédateurs^. Il possède un sens développé de l'odorat lui permettant entre autres de trouver de l'eau et de détecter une femelle en chaleur à 800 m^, et un organe de Jacobson pour analyser les odeurs pendant le flehmen^.


Le cheval est en principe peu attiré par le goût sucré, mais la fréquentation de l'être humain l'y a habitué^. Il possède un sens du toucher très développé sur la tête et le dos, et peut faire frémir une partie de son corps afin de chasser les mouches qui s'y


pression. Ses lèvres sont entourées de poils sensibles appelés


Les chevaux ont besoin d’une importante quantité d'eau propre à