Marx et Engels emploient de manière très restreinte le mot capitalisme : seulement deux fois dans volume I du Capital et dans les Théories de la plus-value quatre et trois fois dans les volumes II et III du Capital. Le mot « capitaliste » est beaucoup plus utilisé et figure plus de 2 600 fois dans les trois volumes (1867, 1885, 1894) combinés du Capital. Karl Marx et Friedrich Engels utilisent le terme de « capitaliste » (Kapitalist en allemand) dans leur Manifeste du Parti communiste en 1848 pour parler de la propriété privée détenue par une personne et de «forme capitaliste de production» (en allemand : kapitalistische Produktionsweise).


Marx et Engels parlent volontiers de « système capitaliste » (kapitalistisches System) et du « mode de production capitaliste » (kapitalistische Produktionsform) dans Le Capital en 1867. Marx n'utilise pas beaucoup le terme de « capitalisme », mais plutôt ceux de « capitalistes » et de « mode de production capitaliste », mentionnés plus de 2 600 fois dans les trois livres du Capital[note 2].


Le dictionnaire Larousse donne plusieurs définitions du capitalisme :


« statut juridique d'une société humaine caractérisée par la propriété privée des moyens de production et leur mise en œuvre par des travailleurs qui n'en sont pas propriétaires » ; « système de production dont les fondements sont l'entreprise privée et la liberté du marché » ; « système économique dont les traits essentiels sont l'importance des capitaux techniques et la domination du capital financier » ; « Dans la terminologie marxiste, régime politique, économique et social dont la loi fondamentale est la recherche systématique de la plus-value, grâce à l'exploitation des travailleurs, par les détenteurs des moyens de production, en vue de la transformation d'une fraction importante de cette plus-value en capital additionnel, source de nouvelle plus-value ».


L'encyclopédie économique de référence en langue anglaise (NewPalgrave) définit le capitalisme comme un système dans lequel les moyens de production sont détenus par des particuliers.


Dans son sens moderne, l'étymologie du terme « capitalisme » renvoie à plusieurs définitions :


le capital, réalité tangible, masse de moyens aisément identifiables, sans fin à l'œuvre ; le capitaliste comme agent opérationnel ou comme vecteur social ; le capitalisme qui est la façon dont est conduit, pour des fins peu altruistes d'ordinaire, ce jeu constant d'insertion.


Cette section est vide, insuffisamment détaillée ou incomplète. Votre aide est la bienvenue ! Comment faire ?


Ainsi que le démontre l'historien Fernand Braudel dans La dynamique du capitalisme celui-ci s'est développé à partir du XVIe siècle en Europe. Avant la révolution industrielle, l'aspect du capitalisme était principalement lié au commerce d'où le vocable « Capitalisme marchand » utilisé dans une vision marxiste, et ce, jusqu'à la fin du Moyen Âge. La croissance économique (assimilée au « capitalisme » dans un usage fréquent, notamment marxiste) a pu se développer dans le monde occidental après la disparition progressive du féodalisme. Cette disparition a permis l'extension de cette croissance économique dans toute l'Europe au cours du XIXe et du XXe siècle. En effet, l'amélioration des sciences et techniques et de la pratique économique après la Renaissance a permis l'industrialisation dans la majeure partie du monde.


Selon le sociologue Max Weber, le développement du capitalisme au XVIIIe siècle s'explique par l'apparition du protestantisme et notamment par la propagation des idées calvinistes. D'une part celles-ci conduisent les croyants, même riches, à un mode de vie à la fois consacré au travail (notion de vocation) et ascétique (et non hédoniste), ce qui est favorable à l'épargne et donc à la création de capitaux. D'autre part la notion de prédestination, selon laquelle Dieu aurait par avance choisi de façon irrévocable ceux qui seront damnés et ceux qui seront sauvés, créerait une véritable angoisse existentielle chez les réformés qui, dans cette incertitude, se tourneraient vers ce qui peut constituer pour eux des signes d'élection visibles : la bénédiction pendant la vie terrestre et la réussite économique - ce qui bien entendu encourage puissamment l'accumulation des richesses et donc le capitalisme.


Les révolutions de la fin du XVIIIe siècle poseront un cadre juridique nouveau permettant la croissance du capitalisme avec notamment l'apparition la propriété foncière, du marché du travail et de la propriété intellectuelle.


Le capitalisme prendra son véritable essor avec la révolution industrielle. Les modifications du travail et de son organisation engendrées par l'arrivée de machines entraînent pour la population paysanne une source d'emploi mais aussi pour les anciens travailleurs des domicile-ateliers, une source de chômage et de déqualification.