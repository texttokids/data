La politique d'un clan en matière de don est essentielle. Il existe plusieurs approches. Certains estiment que tout le monde doit donner et il existe même des clans qui nomment des chefs chargés de la vérification des ratios de dons (nombre de troupes données en regard du nombre reçu). D'autres au contraire interdisent aux joueurs de petit niveau (qui ont des troupes faibles) de faire des dons pour ne pas bloquer la place. Certains joueurs ne jouent que pour donner ou possèdent des comptes dont c'est la fonction principale. De façon générale, il reste cependant assez mal vu de ne pas ou peu donner et cela provoque régulièrement des conflits voire des exclusions. Certains clans regardent le profil des joueurs pour voir s'ils sont généreux ou non, bien qu'il existe de grandes disparités selon le type de clans que le joueur a fréquenté au cours de son expérience dans le jeu.


Les dons peuvent également être fait via des gemmes (plus ou moins cher en fonction des troupes). Supercell organise d'ailleurs des événements de temps en temps où tous les dons sont à 1 gemme (ou alors en achetant


La guerre des clans (abrégé G.D.C) est un événement lancé par les clans et qui consiste à affronter un clan adverse pour remporter la victoire. Pour ce faire, le chef du clan ou un des chefs adjoints doit sélectionner au minimum 5 membres et déclarer la guerre. Le nombre de participants se fait par tranche de 5 autrement dit 5, 10, 15, 20 etc. Lors des premières GDC, les membres retenus étaient ceux qui avaient le plus grand nombre de trophées. Les personnes qui ne voulaient ou ne pouvaient pas y participer devaient soit quitter le clan avant le lancement des guerres, soit perdre volontairement des combats pour descendre dans le classement soit, le plus souvent, être


Les chefs peuvent désormais sélectionner les joueurs. Les joueurs peuvent par ailleurs indiquer par un système de statuts s'ils veulent ou non participer mais la décision finale revient au chef ou au chef adjoint qui décide de lancer une recherche de clan adverse.


Dans une guerre de clans, il y a différentes phases : Il peut aussi donner des troupes de défense à ses coéquipiers en remplissant le château. Les plans des bases adverses ne sont


joueurs de son choix sur le clan adverse et vice-versa. Il ne peut pas ré attaquer le même joueurs mais peut attaquer un adversaire déjà attaqué par un membre du clan afin d'améliorer le score ou


la guerre de clans. En cas d'égalité, le taux de destruction est


Il y a aussi les guerres amicales où l’on ne gagne rien mais qui permet de défier un clan en particulier. La durée de préparation et la durée de la guerre peuvent être décidés, contrairement aux guerres contre un


Apparus en décembre 2017, ces jeux permettent aux membres du clan de gagner des points qui s'additionnent à ceux des autres membres pour obtenir ensuite des récompenses. Ce système de jeu est très inspiré des coffres de clan de l'autre jeu phare de Supercell Clash Royale. Il permet notamment d’acquérir des potions et des livres qui ont fait leur


Apparues récemment, les ligues de clans (L.D.C) sont produites tous les mois. Tous les joueurs qui font partie d’un clan peuvent y participer. 8 clans se font mutuellement la guerre chacun leur tour durant 7 jours chaque Guerre dure 24h). Chaque clan y inscrit 15 ou 30 personnes. Le nombre d'étoiles gagnées permet de monter ou de descendre dans le classement des 8 clans. A la fin, en fonction du classement, les joueurs gagnent des médailles de clan qu'ils peuvent utiliser dans le magasin la ligue. Le chef peut désigner certains participants pour en


Le marchand ou vendeur est apparu en mars 2018 ; ce personnage situé juste en dessous de l'emplacement pour la charrette des Jeux de Clans vous invite à lui échanger, séparément, 3 objets différents contre des gemmes^. Chaque jour, les objets échangeables diffèrent. Il arrive parfois qu'un objet soit donné gratuitement par le marchand


La Base des Ouvriers est un second village ajouté dans Clash of Clans lors de la mise à jour de mai 2017. Elle est accessible en réparant le bateau sur la rive gauche du village. Cette réparation est gratuite et possible pour tout joueur ayant un d'hôtel de ville de niveau 4 au


Ce second village inclut des nouveautés par rapport au système de jeu du village principal, comme des duels en temps réel (deux joueurs tentent de détruire le village adverse en simultané ; celui ayant réalisé le plus grand pourcentage de destruction sort victorieux), de nouveaux bâtiments (mine des gemmes, tour de l'horloge, double canon, etc.), un nouveau héros appelé la « machine de combat » ainsi


Cette section est vide, insuffisamment détaillée ou incomplète.


3 mois après son lancement, Clash of Clans était le numéro 1 des applications aux États-Unis. Le jeu est longtemps resté dans le top des jeux les plus rentables sur iOS et Android^. Sur la plate-forme Google Play, il totalise plus de 500 millions de téléchargements