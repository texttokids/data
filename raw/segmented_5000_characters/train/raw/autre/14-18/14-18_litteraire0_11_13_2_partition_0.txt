Recevez une injection de corticostéroïdes. L'injection de stéroïdes près de la gaine du tendon ou dans celle-ci peut réduire rapidement l'inflammation et permettre à nouveau un mouvement normal et non restreint de votre doigt. L'injection de corticostéroïdes est le premier choix de traitement pour le doigt à ressaut 
. Deux injections sont généralement nécessaires (à 3 ou 4 semaines d'intervalles) et elles sont efficaces chez 90 % des patients 
. Les préparations les plus courantes contiennent de la prednisolone, de la dexaméthasone et de la triamcinolone.
Il y a des complications potentielles à l'injection de corticostéroïdes : infection, saignement, affaiblissement des tendons, atrophie musculaire locale et irritation des nerfs.
Si l'injection de corticostéroïdes n'apporte pas les résultats escomptés, le recours à la chirurgie pourrait être envisagé.



Faites-vous opérer du doigt. Le premier élément permettant d'indiquer qu'un doigt à ressaut nécessite une opération chirurgicale est déjà de savoir s'il ne réagit pas favorablement à vos remèdes maison, à l'attelle et/ou aux injections de stéroïdes et si le doigt est gravement plié ou bloqué 
. Il existe deux principaux types de chirurgie : la chirurgie ouverte traditionnelle ou la chirurgie percutanée 
. La chirurgie ouverte nécessite de faire une petite incision à la base du doigt affecté et de découper la section resserrée de la gaine du tendon. La chirurgie percutanée nécessite l'insertion d'une aiguille dans le tissu entourant le tendon affecté, puis son déplacement en vue de briser la restriction.
La chirurgie d'un doigt s'effectue généralement en clinique ambulatoire sous anesthésie locale.
Il y a des complications éventuelles suite à cette opération : infection locale, réaction allergique à l'anesthésie, atteinte des nerfs et gonflement ou douleur chronique.
Le taux de rechute n'est que de 3 %, mais l'opération a moins de chances de succès chez les personnes diabétiques.




Soignez l'infection sous-jacente ou la réaction allergique. Parfois, une infection localisée imite le doigt à ressaut ou entraine une contraction des tendons. Si les articulations ou les muscles de votre doigt rougissent, se réchauffent ou deviennent fortement enflammés en quelques heures ou en quelques jours, vous devriez consulter immédiatement, car il peut s'agir d'une infection ou d'une réaction allergique à une piqure d'insecte. Pour soigner, on peut réaliser une incision et un drainage, faire un bain d'eau salée ou prendre des antibiotiques oraux 
Les bactéries sont le plus souvent à l'origine des infections de la main. Elles résultent souvent de coupures non soignées, de plaies par perforations ou d'ongles incarnés.
Les réactions allergiques aux piqures d'insectes sont relativement courantes, particulièrement lorsqu'il s'agit d'abeilles, de guêpes ou d'araignées.



Soignez la dislocation de l'articulation. Une articulation du doigt disloquée ressemble au doigt à ressaut, car elle est douloureuse et entraine la pliure ou la torsion du doigt. Les dislocations sont souvent causées par un traumatisme contondant, à l'opposé d'une contrainte répétitive. Elles nécessitent une assistance médicale immédiate afin de réinitialiser ou de réaligner l'articulation. Ensuite, le doigt disloqué se soigne à peu près pareil que le doigt à ressaut en termes de repos, d'anti-inflammatoires, de glace et d'attelle.
Un examen de la main aux rayons X pourra permettra d'identifier un doigt disloqué ou fracturé.
Outre votre médecin traitant, les ostéopathes, les chiropraticiens et les physiothérapeutes seront capables de soigner votre doigt disloqué.



Luttez contre l'arthrite. Parfois, un tendon du doigt enflammé ou contracté est dû à une poussée d'arthrite ou de goutte 
. L'arthrite rhumatoïde est une maladie auto-immune qui attaque les articulations du corps de façon agressive et qui nécessite l'utilisation d'anti-inflammatoires puissants et de suppresseurs du système immunitaire. La goutte est un trouble inflammatoire causé par des dépôts de cristaux d'acide urique dans les articulations (typiquement dans les pieds, mais aussi dans les mains), ce qui peut affecter les tendons concernés et entrainer des contractures.
L'arthrite rhumatoïde affecte souvent les mains et les poignets et est susceptible de défigurer les articulations avec le temps.
Votre médecin pourrait vous demander de subir des analyses de sang afin de vérifier les indicateurs de l'arthrite rhumatoïde.
Pour réduire le risque de goutte, limitez les aliments riches en purine comme les abats, les fruits de mer et la bière.