On raconte qu’un homme avait une vache dont il vendait le lait mélangé avec de l’eau.
Un jour, une inondation soudaine surprit la vache qui paissait près d’un ru et la noya.
Son propriétaire était là, à se lamenter sur sa mort ; un de ses fils lui dit alors :
« Ô Père ! ne te lamente pas ! L’eau que nous avons mélangée à son lait a débordé et l’a emportée et noyée ».