L'égoïsme et la crainte ont moins d'abnégation; mais encore une fois, il faut bien se dire que dans les jours de bouleversement, refuser son action, c'est donner à ceux qui veulent détruire, une facilité de plus. On accepte, non pour servir des hommes ou des choses qui déplaisent, mais pour les faire servir au profit de l'avenir. «En toute chose il faut considérer la fin,» a dit le bon La Fontaine, et cela n'est pas une simple maxime d'apologue. Je dois ajouter que l'amiral Bruix dont j'aimais et estimais le caractère, l'esprit et le talent, devait être nommé ministre de la marine, ce qui faisait que j'arrivais aux affaires avec quelqu'un d'aussi étranger que moi aux façons du directoire, et avec qui je pouvais m'entendre et sur le bien que l'on pouvait faire et sur le mal que l'on pouvait empêcher.

Pour donner une idée claire de ce que j'appelle ici les façons du directoire, je crois qu'il suffira de raconter ce qui se passa à la première séance à laquelle j'assistai. Une querelle s'engagea entre Carnot et Barras; ce dernier accusait son collègue d'avoir supprimé une lettre qui aurait dû être mise sous les yeux du directoire. Ils étaient debout l'un et l'autre. Carnot, en levant la main dit: «Je jure sur ma parole d'honneur que cela n'est pas vrai!-Ne lève pas la main, lui répond Barras, il en dégoutterait du sang.» Voilà les hommes qui gouvernaient, et c'est avec eux qu'il fallait essayer de faire rentrer la France dans la société européenne.

Je me jetai dans cette grande entreprise.

Membre du Comité de salut public, il fut chargé de la préparation et de la direction de la guerre. Élu au conseil des Anciens par quatorze départements, il fut nommé directeur. Proscrit au 18 fructidor, il se réfugia à Genève.

Sous le consulat, il fut ministre de la guerre, puis membre du tribunat. En 1814, il fut nommé général de division et gouverneur d'Anvers. Ministre de l'intérieur sous les Cent jours, il fut exilé à la Restauration et mourut à Magdebourg en 1823.

Presque tous les ennemis qu'avait eus la France depuis le début de la Révolution, avaient dû chercher leur salut dans une paix que la plupart avaient achetée par des cessions de territoire, ou par des contributions pécuniaires. L'Autriche, battue en Italie, battue en Allemagne, voyant son territoire envahi de deux côtés, et sa capitale menacée par le général Bonaparte, avait déjà signé avec lui des préliminaires de paix à Leoben, et négociait le traité définitif qui fut celui de Campo-Formio. C'est entre les préliminaires et la signature du traité que je devins ministre des relations extérieures. Le général Bonaparte, en apprenant ma nomination, écrivit au directoire pour lui en faire compliment, et m'adressa à cette occasion une lettre fort obligeante. A dater de cette époque, une correspondance suivie s'établit entre lui et moi. Je trouvais dans ce jeune vainqueur, dans ce qu'il faisait, disait ou écrivait, quelque chose d'assez nouveau, d'assez fort, d'assez habile et d'assez entreprenant pour attacher à son génie de grandes espérances. Au bout de quelques semaines, il signa le traité de Campo-Formio (17 octobre 1797).

Paris, 24 Juillet 1797.

J'ai l'honneur de vous annoncer, général, que le directoire exécutif m'a nommé ministre des relations extérieures.

Justement effrayé des fonctions dont je sens la périlleuse importance, j'ai besoin de me rassurer par le sentiment de ce que votre gloire doit apporter de moyens et de facilité dans les négociations. Le nom seul de Bonaparte est un auxiliaire qui doit tout aplanir. Je m'empresserai de vous faire parvenir toutes les vues que le directoire me chargera de vous transmettre, et la renommée, qui est votre organe ordinaire, me ravira souvent le bonheur de lui apprendre la manière dont vous les aurez remplies. (Correspondance inédite et officielle de Napoléon Bonaparte avec le directoire, les ministres, etc. Paris, 1819, 7 vol. in-8.) 

De son côté, l'Angleterre avait envoyé en France un plénipotentiaire (lord Malmesbury), pour y parler de paix; mais cette démarche n'était pas sincère. Le ministère anglais avait alors besoin de simuler une négociation pour se tirer de ses embarras intérieurs.

James Harris, comte de Malmesbury, était né en 1746.

Secrétaire d'ambassade en 1768. Ministre à Berlin, 1771; à Pétersbourg, 1777; à La Haye, 1783; membre de la Chambre des lords, 1788. Sa vie publique se termina avec ses missions en France. Il mourut en 1820.

Telle était, au dehors, la situation de la France quand j'entrai dans le ministère.

Au dedans, un parti travaillait à changer l'ordre de choses existant, pour y substituer quoi? c'est ce qu'on n'a jamais su et ce qu'on ne pourra jamais savoir; car ce parti, peu nombreux, était composé de républicains, de constituants et de conventionnels, qui pouvaient être réunis par des haines, mais qui, certainement, ne pouvaient l'être par aucun projet.