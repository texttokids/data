Ceux de France et d'Angleterre sont charmés de la chose. Superbe occasion de faire contribuer le clergé, de sanctifier la guerre, d'accuser Charles-Quint.

Ainsi cette chose inouïe et terrible qui devait effrayer la terre et faire crouler le ciel, elle fait à peine sensation. Qu'est-ce donc? Ce sanctuaire est-il comme les redoutés vases d'Éleusis qu'on n'osait regarder, mais, si l'on regardait, l'on ne découvrait que le vide?

Le vieil oracle virgilien: «À Rome, un Dieu réside,» s'est trouvé démenti. Le monde a eu la curiosité d'y aller voir; il demande: Où donc est ce Dieu?

Et la peinture récente de Raphaël, la flamboyante épée de saint Pierre et saint Paul qui fait reculer Attila, elle n'a pas fait peur aux soldats de Frondsberg. Des salles de conclave, de concile, ils font écurie. S'ils ont peur, c'est tout au contraire d'habiter ces voûtes païennes, de loger, eux chrétiens, pêle-mêle avec des idoles, dangereuse oeuvre du Démon.

N'est-ce pas ce que tant de martyrs du Libre Esprit avaient dit au bûcher contre la Babel du pape?

N'est-ce pas ce que les vrais patriotes italiens (d'Arnoldo de Brescia jusqu'à Machiavel) ont annoncé à l'Italie: qu'elle mettait sa vie dans la mort, et que la mort l'entraînerait?

«Rome a mangé le monde,» disait le vieil adage. Cette fois, le monde a mangé Rome.

Le génie italien, si longtemps captif et malade dans cette fatale fiction d'un faux empire du monde qui annula sa vitalité propre et fit avorter la patrie, le génie italien pourrait remercier cette grande calamité qui le délivre, repousser et nier cette communauté de la mort. Rome est morte; vive l'Italie!

Il n'en est pas ainsi. Ce n'est pas impunément que, toute une longue vie, l'esprit a endossé le corps, traîné cette chair de tentations, de péchés, de souillures. Quand il faut la jeter, et libre, déployer ses ailes, nous hésitons toujours. Telle l'Italie, qui si longtemps vivait dans cette forme, dans cette condition d'existence, fut accablée du coup, et il lui fallut des siècles pour s'en relever.

Voyons comment les deux grands Italiens ont pris la chose. Regardons un moment Michel-Ange et Machiavel.

Tous deux avaient erré. Tous deux, dans les illusions qui entourent des moments si sombres, avaient cherché l'espérance dans le désespoir, cru que l'on pourrait sauver le pays par les Médicis, faire la force avec la bassesse; mais non, il n'en est pas ainsi. Et Dieu punit de telles pensées.

D'abord le pape, qui était Médicis, accepta sa sentence, se mit plus bas encore que ne l'avait mis son malheur, montra que, pour être sorti de captivité, il n'était pas plus libre. Traité outrageusement comme un petit prince italien, il prouva qu'il n'était rien autre chose.

Florence lui tenait au coeur bien plus que Rome. Et, pour avoir Florence, il s'humilia devant l'Empereur. Il y fut ramené par le prince d'Orange, le chef des brigands italiens qui, derrière les Barbares, traîtreusement, avaient pillé Rome.

Dans le moment si court de la lutte suprême de Florence, d'une ville contre le monde, ni Machiavel, ni Michel-Ange ne manquèrent à la patrie.

Machiavel y trouva appliqué son Arte di guerra, toute la jeunesse levée en légions, dans la forme qu'il avait tracée. On prenait le système, mais on repoussait l'homme. Négligé, oublié, pas même persécuté.

L'indomptable vigueur de son esprit paraît encore dans l'étrange description qu'il a faite de la peste de Florence, un mois avant sa mort, un mois après le sac de Rome.

Cet homme, d'un malheur accompli, seul, vieux, pauvre, haï, méprisé, savez-vous ce qu'il fait? Parmi les litanies funèbres, sur le bord de sa fosse, il écrit une espèce de Pervigiliun Veneris du mois de mai.

C'est l'idylle de la peste.

Dans la ville, il est fort à l'aise: il va en long, en large, au milieu des fossoyeurs qui crient: «Vive la mort!» comme c'était l'usage de chanter Mai et le printemps. À travers les ténèbres, il croit voir passer la peste dans une litière. C'est une jeune morte traînée par des chevaux blancs.

Il s'en va sur la place où l'on élit les magistrats. Il n'y a plus de peuple. Des citoyens encore, mais allongés sur des civières qu'on porte. Au défaut de vivants, au vote on appelle les morts.

Étonnant aspect des églises! Le clergé est mort, les moines sont morts. Tel reste pour confesser les femmes malades qui se traînent et viennent mourir là. Il est assis au milieu de la nef, les fers aux pieds, aux mains, pour empêcher qu'il ne les touche. Songez-y, dans ce temps de morts, c'est tout d'être vivant. Trois dévots en béquilles, qui circulent dans l'église, lancent un regard d'amour à trois vieilles édentées. Machiavel, avec ses soixante ans, est sûr de plaire et de trouver fortune.