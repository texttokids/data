mais il ne sort pas des bornes de la vérité la plus scrupuleuse, quand il fait de main de maître le portrait d'une courtisane, qui avait été fameuse et qui allait revenir, en vieillissant, à son point de départ obscur et misérable. C'est à cette courtisane qu'il adresse sa satire XXV: 

Les chalands degoutez tournent ailleurs leurs pas.

Tu vois diminuer tous les jours ta prattique: Comme ce procureur, ferme donc ta boutique.

C'est bien force, à present que tu n'es plus des belles, Que tu sois à present vendeuse de chandelles.

La femme est laide, après qu'elle a trente ans vecu: Les roses à la fin deviennent gratte-cu.

Ce dernier vers est encore dans la mémoire de tout le monde, sans qu'on sache à quel sens il se rattache ni à quel auteur on puisse l'attribuer. Courval-Sonnet conseille à cette ancienne fille d'amour, de profiter de son reste; de tirer, d'escroquer, d'attraper de l'argent, par tous les moyens possibles; de chercher à émouvoir ses dupes, en leur disant qu'elle craint le sergent, qu'elle a mis en gage sa jupe et sa hongreline; de ramasser enfin un petit pécule qui lui permette de vivre du travail de ses mains dans sa vieillesse. Mais elle n'entend point de cette oreille et elle ne prévoit pas qu'un jour viendra où les ressources de la Prostitution lui manqueront tout à fait; elle ne se doute pas qu'elle ait vieilli; elle se fâche contre l'importun donneur d'avis: «Enné! s'écrie-t-elle, 

Qu'on ne s'attende pas que je couse ou tapisse: Le plus aisé travail pour moy n'est qu'un supplice; Puisque j'ay de quoy vivre et de quoy m'habiller, Qu'on me parle de rire, et non de travailler.

Tout mon contentement est d'estre bien ornée: Une femme d'amour vit au jour la journée.» 

Le sieur de Courval n'essaye plus de lui parler le langage de la raison, car chez elle l'habitude du vice est devenue incurable; il l'invite donc avec ironie à persévérer dans la voie où elle s'est perdue; pas de remords, pas de regrets; chacun ici-bas a sa destinée: celle d'une courtisane est de mourir courtisane.

Pratique habilement, en te moquant de moy, Tous les tours du bordel que tu scais sur le doy...

Tu possedes un peigne, un charlit, un miroir, Une table à trois pieds qu'il fait assez bon voir, Un busc, un esventail, un vieux verre sans patte, De l'eau d'ange, du blanc, de la poudre, une chatte, Une paire de gands qui furent jadis neufs, Une boîte d'onguent, une houppe, des nœuds, Un poilon, un chaudron, une écuelle, une assiette; Pour te servir de nappe, un engin de serviette.

Cette description du ménage d'une fille de joie, au commencement du dix-septième siècle, serait encore exacte aujourd'hui, si on l'appliquait à la plupart des femmes publiques de bas étage. Ces créatures n'ont pas plus changé de physionomie et de manière d'être, que de train de vie et de métier. Courval-Sonnet continue à les peindre toutes d'après nature, sous les traits d'une seule, qui arrivait à l'âge de la décadence: 

Tu n'apaises ta faim d'aucun friand morceau: Ta viande est du pain, ton breuvage est au seau; En esté, tu remplis ton ventre de salades; Extresmement habile à bailler des cascades, A faire niche à l'un et l'autre caresser, A tirer un present; cela fait, le chasser; Insensible aux bienfaits, conteuse de sornettes, Impudente menteuse et qui scait ses deffaites; Ton mestier est infame et doux infiniment: C'est pourquoy l'on n'en sort que difficilement.

Le sieur de Courval-Sonnet quitta Paris, quand il eut passé sa thèse de docteur à la Faculté de médecine; il n'était déjà plus jeune, et il avait échappé à tous les orages de la jeunesse: il vint se fixer à Rouen, pour y pratiquer son art, mais, tout en soignant ses malades, il composait encore des satires, et ces satires avaient toujours pour objet de corriger les mœurs, qui ne paraissent pas avoir été meilleures en province que dans la capitale. Ce fut à Rouen qu'il publia sous le voile de l'anonyme les Exercices de ce temps, qui eurent les honneurs de plusieurs éditions successives (chez de la Haye, 1627, in-8º; chez Laurens Maurry, 1631, in-4º; chez Delamarre, 1645, in-8º), sans que le poëte songeât à faire disparaître les incorrections et les grossièretés de son style. Ces Exercices sont des esquisses de mœurs, très-curieuses, dans lesquelles une foule de traits appartiennent à l'histoire de la Prostitution. «Courval n'a imité de Regnier, que ce que celui-ci a de blâmable, dit M. Viollet-Leduc (Catalogue des livres composant sa Bibliothèque poétique, avec des notes bibliographiques, biographiques et littéraires, Paris, Hachette, 1843, in-8º); il n'a pas même pris la peine de dissimuler ses larcins: son Débauché, son Ignorant, sont évidemment calqués sur les satires X et XI de Regnier; en sa qualité de médecin, il a abusé des termes et des descriptions sales, jusqu'au dégoût.» Nous ne nous occuperons que de trois satires, la première, la cinquième et la onzième, intitulées le Bal, la Promenade, et le Débauché.