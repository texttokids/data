G.; t. III, p. 72, édit. M.-Ibid. (20 mars 1673), t. III, p.

147, édit. G.; t. III, p. 76, édit. M.

III, p. 141, édit. G.; t. III, p. 71, édit. M.-LA FARE, Mémoires, p. 125, dans la notice par M. Monmerqué.

Cependant un autre motif que son amour pour madame du Ludres retenait Sévigné dans la capitale plus longtemps peut-être qu'il ne l'aurait voulu: c'était le besoin d'argent. Sans avoir aucun vice ou aucun goût ruineux, il avait peu d'ordre; et sa mère lui ayant déjà avancé de fortes sommes pour l'acquisition de sa charge de guidon et pour ses équipages, il n'osait plus rien réclamer. Aussi, malgré l'intimité qui régnait entre elle et lui, il crut devoir lui faire cette demande par l'intermédiaire de madame de la Fayette et de d'Hacqueville. La manière un peu sévère dont madame de la Fayette rappelle à son amie qu'elle est beaucoup plus prodigue pour sa fille que pour son fils prouve que l'on aimait moins la sœur que le frère, et que, comme tous les amis de madame de Sévigné, madame de la Fayette désapprouvait l'excessive faiblesse et la continuelle admiration de son amie pour madame de Grignan: 

«Je ne vous puis dire que deux mots de votre fils: il sort d'ici; il m'est venu dire adieu, et me prier de vous écrire ses raisons sur l'argent: elles sont si bonnes que je n'ai pas besoin de les expliquer fort au long, car vous voyez, d'où vous êtes, la dépense d'une campagne qui ne finit point. Tout le monde est au désespoir et se ruine: il est impossible que votre fils ne fasse pas un peu comme les autres; et, de plus, la grande amitié que vous avez pour madame de Grignan fait qu'il en faut témoigner à son frère. Je laisse au grand d'Hacqueville à vous en dire davantage.» 

G.; t. III, p, 74, édit. M. (Lettres de madame de la Fayette.) 

Madame de la Fayette gourmande aussi sur ses exigences madame de Sévigné, qui mettait en doute son amitié, parce qu'en raison de sa paresse naturelle elle négligeait de lui répondre. «Je suis très-aise, lui écrit madame de la Fayette, d'aimer madame de Coulanges à cause de vous.

Résolvez-vous, ma belle, de me voir soutenir toute ma vie, de toute la pointe de mon éloquence, que je vous aime plus encore que vous ne m'aimez. J'en ferais convenir Corbinelli en un quart d'heure; et vos défiances seules composent votre unique défaut et la seule chose qui peut me déplaire en vous. M. de la Rochefoucauld vous écrira.» 

La Rochefoucauld n'écrivit pas aussitôt qu'il l'avait promis; car il se sert encore de la plume de madame de la Fayette pour consulter madame de Sévigné et aussi Corbinelli, sur une pensée qu'il avait probablement le projet d'insérer dans son livre de Réflexions ou Sentences et Maximes morales. Il en avait déjà publié trois éditions: la seconde, avec des corrections et des retranchements; la troisième, corrigée et augmentée; il en fut de même des trois éditions qui suivirent. La Rochefoucauld avait fait de la composition de ce petit livre l'amusement des loisirs de sa vieillesse; il y faisait participer sa société intime, et surtout madame de la Fayette. N'ayant reçu durant les guerres civiles qu'une éducation imparfaite, il était beaucoup moins lettré que son amie; mais, par la tournure de son esprit et par son expérience du monde, il était plus capable de peindre l'homme corrompu des cours et de rédiger avec concision et finesse le code honteux de leur morale que tous les gens de lettres et toutes les femmes spirituelles dont il était entouré.

Il craignait toujours de trop grossir son recueil; et il essayait en quelque sorte l'effet de ses réflexions sur le public de son choix. Il acceptait différentes rédactions des mêmes pensées avant de les admettre à la publication. Segrais, auquel il soumettait la copie de chacune des éditions, dit qu'il y a des maximes qui ont été changées plus de trente fois.

II, p. 12.

Madame de la Fayette termine ainsi une de ses lettres à madame de Sévigné: «M. de la Rochefoucauld se porte très-bien; il vous fait mille et mille compliments, et à Corbinelli. Voici une question entre deux maximes: 

On pardonne les infidélités, mais on ne les oublie pas.

On oublie les infidélités, mais on ne les pardonne pas.

«Aimez-vous mieux avoir fait une infidélité à votre amant, que vous aimez pourtant toujours, ou qu'il vous en ait fait une, et qu'il vous aime toujours?» Et pour expliquer le sens de cette question entre deux maximes, question pourtant assez claire, madame de la Fayette dit qu'il s'agit ici d'infidélités passagères. Quant aux deux maximes, le choix de madame de Sévigné ne pouvait être douteux; mais, pour répondre à la subtile question que lui pose madame de la Fayette, une chose lui manquait, l'expérience; et elle pouvait, je pense, en renvoyer la décision à son amie. La Rochefoucauld avait déjà inséré dans la troisième édition cette maxime: «On pardonne tant que l'on aime.» Il ne parlait nulle part, dans cette édition, de l'infidélité entre amants.