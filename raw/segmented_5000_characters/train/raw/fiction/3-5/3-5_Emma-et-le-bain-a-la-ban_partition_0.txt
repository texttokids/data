Emma et le bain à la banane


Quelque fois le dimanche matin, Emma fait une matinée de beauté. 
Elle déteste se laver la tête, mais elle adore se mettre des crèmes en pinçant des tubes ou en versant des pommades parfumées sur tout son corps.  Elle collectionne des petites bouteilles de shampoing et des savonnettes que mamie lui apporte des hôtels quand elle voyage.
Emma accepte donc le shampoing sans crier, sans pleurer. Elle chante même dans son bain :
« Bulles de savon, li la la
Le bain est bon, lu la la
Dans le fleuve, lé la la
Pour une peau neuve, lo la la. »

Elle n’est pas seule dans le bain car il y a la famille de canards, des gobelets gigognes, une grenouille et une baleine, mais pas de requin. Et dans sa tête il y a Antonin, Guillaume, Anna et Mamie.
Mais quand le téléphone sonne, toute la bonne humeur d’Emma s’évapore. Sa mère parle et parle et parle et du coup, même avec les canards et la baleine, Emma se sent seule.
« Maman ! » crie-t-elle.
« Attends Emma …» 
« MAAAAMAAAN ! » hurle-t-elle.
« Je suis au téléphone ! »
« MAAAAAAAAAAAAMAAAAAAAAAAN !!!!!!! »
« Deux secondes, Emma ! »
« Maman, j’ai faim. »
Maman raccroche et vient dans la salle de bains.
« Maman, je veux une banane. »
Tu mangeras quand tu seras sortie du bain et habillée. »
« Maman ! Une banane ! Dans le bain ! »
« Toute à l’heure, Emma. »
« Tout de suite ! » pleurniche-t-elle.
« Emma ! » Sa mère essaie de la raisonner. « Tu ne peux pas manger une banane dans le bain. Elle peut tomber dans l’eau sale pleine de savon. »
Emma n’est pas d’accord. Pourquoi la banane tomberait-elle ? Elle mange bien des bananes dans la rue et elles ne tombent pas par terre.
La seule réponse d’Emma est de faire une crise. Elle remplit le bain de ses larmes.
Sa mère en a marre.
« Calme-toi ! »
« Une banane ! » pense Emma. « Ce n’est pas la mer à boire. »
Tout d’un coup sa mère revient avec … une banane.
« On fera l’expérience ! » Elle se met à peler la banane.
« Non, c’est moi qui l’épluche ! »
« Bon Emma ! Aujourd’hui c’est toi qui commandes. Que veux-tu d’autre dans le bain ... Un ananas, une noix de coco, une mangue, une pastèque ? »
« Non, juste une banane ! »
Emma l’épluche, elle tient la peau de banane dans une main et la banane dans l’autre. Elle croque et sa bouche se remplit du goût de la banane. C’est bien la chose la plus délicieuse qu’elle ait jamais mangée.
Et dans le bain !
Elle mord encore. Mais cette fois, la banane lui glisse de la main et tombe à l’eau. Elle flotte un moment et puis se noie.
Maman ne dit rien, mais elle ne dit rien si fort qu’Emma entend. Sa mère est contrariée.
Emma sort du bain sans se faire prier.
« Regarde ma peau Maman comme elle est belle. Un bain à la banane, ça rend belle.
Une banane, ça bouche aussi la baignoire, pense maman.
« C’était la dernière banane dans la maison. »
« Mais il y en a d’autres au marché Maman ! »