Il y a trois mois environ, on a sonné à ma porte alors que je préparais le dîner. Je suis allé ouvrir et j’ai découvert sur le palier une étrange créature, vêtue d’une combinaison de plongée et coiffée d’un casque de chantier.
- Bonjour, a dit l’apparition.
Sans me laisser le temps de réagir, elle est entrée au salon et s’est écroulée dans un fauteuil. Elle a ôté son masque de plongée, dévoilant un visage plein de douceur.
- Vous ne me reconnaissez pas ? a-t-elle demandé.
- Euh… non… excusez-moi, ai-je balbutié.
- Je suis l’institutrice que vous avez plongée dans un bocal à poissons rouges dans une de vos histoires. (Voir « Silence », dans Histoires pressées.)
J’ai rougi violemment.
- Vraiment ? C’était vous ?
- Oui, oui. C’est moi aussi qui ai reçu un pot de fleurs sur la tête dans Rédaction, vous vous souvenez ? (Voir « Rédaction », dans Encore des histoires pressées.) 
Voilà donc pourquoi qu’elle avait un casque sur la tête et une combinaison de plongée ! 
- Je ne vous en veux pas, vous savez. Vos histoires plaisent beaucoup à mes élèves…
- C’est gentil à vous, ai-je répondu, mais…
- J’ai besoin de vous ! m’a-t-elle interrompu en me fixant de ses grands yeux sombres. 
Comment rester insensible à un regard si éloquent ?
- Expliquez-moi, ai-je soupiré.
- Eh bien, j’écris une histoire avec mes élèves, et nous avons un problème. C’est un conte : une inspectrice arrive dans notre école, mais en réalité, c’est une sorcière déguisée, et elle transforme le directeur en crapaud. 
- Euh… c’est original, ai-je dit poliment. Et où est le problème ? 
Les yeux de la jolie institutrice se sont voilés de larmes :
- Nous n’arrivons pas à sauver le prince crapaud ! Je veux dire : le directeur ! Nous avons cherché partout une bergère pour qu’elle l’embrasse, mais impossible d’en trouver une ! Le pauvre est toujours dans sa mare et…
Elle ne put retenir un sanglot. Je lui tendis un mouchoir en papier.
- C’est très imprudent de votre part, ai-je commencé. Ecrire un conte est une chose sérieuse qu’on ne doit pas confier à des amateurs.
Elle a poussé un cri effrayé.
- N’ayez crainte, l’ai-je rassurée, je vais arranger ça. Vous tenez vraiment à récupérer votre crapaud ? On pourrait le laisser patauger dans sa mare…
- Oh non, s’il vous plaît ! a-t-elle imploré.
J’ai souri. Elle avait l’air de tenir à son directeur.
- Bon, bon, nous allons le sortir de là. Laissez tomber les bergères, on ne peut plus compter sur elles. N’importe quelle jeune femme peut faire l’affaire. Même une institutrice… Il suffit qu’elle embrasse le crapaud directeur sur la bouche et le brave garçon retrouvera forme humaine. A une condition, toutefois…
- Laquelle ? 
- Il faut que le baiser dure longtemps, un vrai baiser de cinéma, passionné…
- Oh, pas de souci, je m’en charge ! s’est-elle écriée.
- Si je comprends bien, vous êtes prête à vous dévouer ? ai-je demandé avec un petit sourire.
En guise de réponse, elle a baissé les paupières et rosi gracieusement. Je me suis levé.
- Alors, parfait. Le problème est réglé. Vous voyez, il suffisait d’un peu d’imagination…
- Mais l’histoire finira bien ? a-t-elle demandé.
- Oh ça, c’est à vous de décider, ai-je répondu. Un dernier conseil : changez de tenue ; ce casque et cette combinaison ne sont pas très… romantiques. 
Elle m’a remercié chaleureusement et a promis de m’envoyer l’histoire dès qu’elle serait terminée.
Eh bien, ce matin même, j’ai reçu un courrier : un faire-part de mariage. 
Oui, je sais, c’est une fin assez banale pour une histoire pas très originale. 
Mais, moi, elle me plaît. 



Bon débarras

- Et si je me débarrassais de mes parents ? se dit Sofia.
L’idée lui était venue comme ça, ce matin-là. Il était sept heures et demie et elle entendait son père et sa mère se disputer à la cuisine (une fois de plus). 
Alors elle prit une feuille de papier, écrivit au crayon PAPA et MAMAN. Puis elle chercha une gomme dans sa trousse et, soigneusement, effaça les deux mots.
Quand elle entra à la cuisine, il n’y avait personne. Elle trouva seulement une savate de son père et un mouchoir taché de rouge à lèvres.
Elle se prépara un bon petit déjeuner et alla à l’école sans se peigner.

C’était vendredi, jour de la dictée. Sofia n’était pas bonne en orthographe. Heureusement la maîtresse avait un nom facile à écrire : elle s’appelait Lise Martin.
Alors, Sofia écrivit sur son cahier : LISE MARTIN. Puis elle prit sa gomme et l’effaça. Pfuit, la maîtresse disparut. Bon débarras, pas de dictée aujourd’hui.

Au premier rang était assis un garçon, cheveux bouclés et yeux de velours : Juan. Sofia en était amoureuse. Comme toutes les filles de la classe.
- Trop nulles, ces filles, pensa Sofia. Juan est à moi, je ne le partagerai pas.
Alors, elle écrivit tous les prénoms de ses copines, puis un à un les effaça. Et une à une, de Miriam à Louise, toutes les filles s’évanouirent. Tous les garçons aussi : quelques coups de gomme sur le cahier, ça suffit.
Sauf lui, bien sûr : Juan, cheveux bouclés et yeux de velours.