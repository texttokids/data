Un jour d’hiver, les enfants appellent Roger (le chien) tout l’après-midi. Roger (le vieil homme), emmitouflé dans un long pull noir, lit le journal. 
D’abord, il ne prête pas attention aux cris des enfants. Mais ils appellent longtemps, et leur voix est désespérée. Alors, finalement, Roger (le vieil homme) se lève et ouvre la fenêtre. En bas, dans la cour, le garçon et sa sœur sont assis sur un banc. La fillette pleure silencieusement. D’une toute petite voix, une dernière fois, le garçon appelle :
- Roger !
Quelque chose dans le cœur de Roger (le vieil homme) s’émeut, se déclenche. Un frisson parcourt son corps fatigué et le transforme entièrement. Il se précipite vers la porte d’entrée, dévale les marches quatre à quatre et file, file vers le banc, les enfants. Il se dresse sur ses pattes arrière et jappe, jappe joyeusement.
La petite fille saute sur ses pieds, le garçon ouvre les bras.
- Oh, Roger ! disent-ils en chœur. Roger, te voilà !



Demandez-le gentiment

7h15
Mon frère aîné bloque l’entrée de la cuisine. 
- Pousse tes fesses, je lui dis.
Aussitôt, j’ai droit à un sermon de ma mère :
- Tu ne pourrais pas lui demander gentiment... politesse... respect... blablabla...
Comme je suis un bon petit, je prends ma voix la plus douce et demande :
- Mon frère adoré, pourriez-vous déplacer votre gros cul, s’il vous plaît ?
Et sans attendre, je lui écrase les orteils pour accéder à mon petit-déjeuner.

7h45
Je dévale les quatre étages et atterris devant la porte d’entrée de l’immeuble. Je lis la plaque clouée sur la face intérieure : POUSSER.
Est-ce qu’elle est polie, elle ? Ça la défriserait de me dire « s’il vous plaît » ?
Je l’ouvre d’un violent coup de pied.

9h07
Cours de français. Interrogation de grammaire. Consigne : « Souligner dans chaque phrase les verbes conjugués et leur sujet ». 
Non, mais quelle façon de parler aux gens ! Si elle était polie, la prof, elle aurait écrit : « Pourriez-vous avoir l’obligeance de souligner… », ou un truc du même genre.
Je croise les bras, furieux, et je jette mon stylo dans ma trousse.
De toute façon, je n’ai pas appris ma leçon.

12h25
J’ouvre un livre de recettes. Si, si, je fais très bien la cuisine. Voilà : far breton aux pommes ; ça m’a l’air appétissant. Que faut-il faire ?
Eplucher 4 pommes golden et les couper en quartiers. 
Chauffer 25 g de beurre dans une poêle, ajouter les quartiers de pommes… 
Je ferme le livre, l’envoie valser. Vous avez vu ? Jamais un « s’il vous plait » ni « un merci » ! Que des ordres ! Zut, à la fin, je ne suis pas leur esclave !
Puisque c’est comme ça, j’ouvre un paquet de chips et je prends un yaourt en dessert. 

17h10
Je joue sur l’ordi de mon père. Je n’ai pas le droit d’y toucher, mais chut ! Et, m… (Mince !), brutalement tout se bloque. Une fenêtre apparaît. C’est écrit en anglais :
Click Continue when you’re asked if you want to continue downloading. Then be sure to accept the terms in the license agreement.
Hou là là, moi en anglais, je ne connais que « I love you » et « Please ». Tiens, est-ce qu’ils me parlent gentiment ? Est-ce qu’ils disent « please », justement ? Rien du tout ! Ils sont aussi malpolis que les Français, les Américains. Eh bien, tant pis pour eux, je débranche l’ordi et je file chez mon copain Yamine. Vaut mieux ne pas être là quand mon père constatera les dégâts.

Et maintenant, je parie, vous aimeriez connaître la fin de cette histoire ? 
Demandez-le gentiment !

Et si vous n’êtes pas contents, tournez la page !



Lasagne