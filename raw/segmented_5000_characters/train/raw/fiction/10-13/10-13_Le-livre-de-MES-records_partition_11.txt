Mon père, ce matin, en me croisant dans l’escalier :
- Ben, tu as vu tes chaussettes ! Des chaussettes vert pomme avec un jean et des baskets rouges. Tu as l’air d’un clown !
Je l’ai pris comme un compliment. De toute façon, j’ai enfilé la première paire de chaussettes en bon état que j’ai trouvée. Je devrais dire : la seule paire. Car, après la réflexion de mon père, j’ai entrepris de faire le tri dans le tiroir à chaussettes. Résultat :
- sept chaussettes célibataires ;
- huit chaussettes trouées ;
- deux chaussettes autrefois blanches devenues rose sale et impossibles à mettre ;
- un paquet de chewing-gum (hélas vide) ;
- une carte postale de grand-mère Annie quand elle était en Irlande ;
- le devoir d’allemand que j’ai cherché partout parce que je devais le faire signer à mes parents (j’avais une très bonne note en plus) ;
- une photo de ma sœur déguisée en grenouille (ça lui va bien) ;
- un pièce de un centime d’euro ;
- une robe de poupée (qu’est-ce qu’elle vient faire là ?).
C’est tout. J’ai décidé de garder mes chaussettes. Et demain je mettrai deux chaussettes dépareillées. Tant mieux si on me prend pour un clown.

« Diplôme accordé à Ben Letourneux
le 17 avril 20..
Le jeune va-nu-pieds susnommé peut s’honorer du titre de plus grand « déparailleur de chaussettes » dans un rayon de 21 km autour de son domicile.

Le jury »



21 avril

Record de crottes, chewing-gums et autres saletés collées sous les semelles

Si j’avais oublié ce que j’ai fait aujourd’hui, il me suffirait de regarder les semelles de mes baskets : on peut y lire le résumé de ma journée, sous forme... collante et odorante !

Vers 10 heures, je suis allé au marché. Sans ma mère. Je voulais voir Lukas, car je savais qu’il présenterait de nouveaux sketchs.
J’ai d’abord fait les courses demandées par ma mère : carottes, poireaux, pommes de terre (une tonne) et un kilo de comté. Quand je suis arrivé devant le musée, Lukas avait commencé son numéro.
Il était vêtu d’une salopette blanche et d’une chemise à carreaux. Il avait aussi un escabeau, un seau, une éponge et des chiffons. Lukas n’est pas un clown traditionnel, ni un clown blanc, ni un auguste. Il a créé son propre personnage, un clown distrait, fragile, gentil, toujours étonné. 
Dans son nouveau numéro, il joue un laveur de vitres. Il pose le pied sur l’escabeau et, à la façon dont il grimpe, on le voit monter les étages d’une tour immense ; quand il arrive tout en haut, on a le vertige avec lui. Il s’aperçoit qu’il a oublié son seau, et il doit redescendre. L’histoire est très simple, mais il arrive sans arrêt à nous surprendre et à nous faire rire.
Tout à coup, il s’est tourné vers moi et m’a fait signe d’approcher. J’ai laissé mon panier et je l’ai rejoint. Il m’a tendu le seau, m’a fait comprendre que je devais aller chercher de l’eau. Pour m’amuser, j’ai d’abord refusé. Il a pris l’air étonné, triste, fâché. Tout le monde riait. Alors, je suis parti avec le seau. Mais au lieu de le remplir d’eau, j’y ai mis un poireau et une pomme de terre. Sans s’étonner, Lukas a remonté tous ses étages, a essayé de laver les vitres avec les légumes, est redescendu, m’a pris sur ses épaules, et s’est servi de moi comme éponge. Le public était écroulé. A la fin, Lukas m’a fait saluer avec lui et c’est moi qui ai passé le chapeau dans le public. Sans rien dire, Lukas m’a serré longuement la main et m’a donné cinq pièces de deux euros.
Après on est allé boire un café (lui) et un jus de pomme (moi) à la terrasse d’un bistro. J’ai raconté à Lukas l’histoire de la femme que j’ai prise pour ma mère, le jour où je l’ai rencontré pour la première fois. Il n’a pas ri. Et n’a rien dit. D’ailleurs, il n’a pas prononcé un mot. Je crois qu’il s’entraîne. Je suis sûr qu’il peut rester une semaine entière sans parler. De temps en temps, il faisait des gestes avec ses mains et ses bras, très vite et j’arrivais toujours à comprendre (enfin, je crois).
Quand je suis rentré à la maison, j’étais tellement content que, malgré les tonnes de pommes de terre et carottes, le panier me semblait léger.
Ah, et les crottes, les chewing-gums et autres trucs collants ? Eh bien, je n’en ai pas raté un à l’aller et au retour. A croire que mes pieds étaient aimantés. Mais bon, je suppose que personne ne s’intéresse aux détails, n’est-ce pas ?

« Diplôme accordé à Ben Letourneux
le 21 avril 20..
Le jury (revêtu d’une combinaison de survie) a contrôlé l’état des semelles de BL et atteste qu’il a marché sur un nombre record de déjections animales, gommes à mâcher (nom français du chewing-gum) et autres substances répugnantes, nauséabondes et collantes. 

Le jury »



24 avril

Record de la fiche de lecture la plus débile et la mieux notée

En réalité, ce n’est pas moi qui ai battu le record, mais ma prof de français. Mais c’est moi qui ai rédigé la fiche de lecture, alors je mérite la moitié du diplôme.