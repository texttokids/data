Encore un record battu malgré moi. Je n’ai pas fait une seule grimace, en réalité, j’ai seulement travaillé mon « expressivité faciale ». Mais allez expliquer ça au commun des mortels.
Avec Lukas, en cours, on s’entraîne à exprimer tous les sentiments par des mimiques et des mouvements du visage. Pas simple ! Lukas trouve toujours qu’on exagère ou, au contraire, qu’on ne peut pas « lire » les émotions sur notre binette. Le plus difficile est de passer très rapidement d’une expression à l’autre.
Alors, je m’entraîne. Tous les matins, je fais des exercices d’assouplissement. Je décontracte les muscles du visage, j’étire la bouche dans tous les sens, je creuse les joues, je bouge mon nez, mes oreilles, mes sourcils, je roule les yeux... Je m’enferme dans la salle de bain pour contrôler dans le miroir au-dessus du lavabo. Line est folle parce qu’elle ne peut plus accaparer la salle de bain pour elle toute seule. 
- Qu’est-ce que tu fiches là-dedans ! hurle-t-elle en tapant sur la porte. Tu te maquilles maintenant ?
Pas encore, mais ça viendra. Pour le spectacle qu’on présentera bientôt, on utilisera le maquillage, nous a annoncé Lukas. Mais très peu. Un signe pour chacun. On a fait des essais. Moi, j’ai trouvé : je me dessine un rond rouge sur la joue gauche. Ça suffit pour changer complètement mon visage.
Je m’entraîne toute la journée. Dans le bus, dans les couloirs du collège, pendant les cours. J’essaie chaque fois une expression nouvelle. Parfois, ça marche. En cours de math, j’ai pris un air souffrant, j’ai expérimenté des « grimaces » de douleur, et la prof m’a envoyé à l’infirmerie. Elle était persuadée que j’avais une péritonite. Parfois, ça marche un peu moins. Le plus souvent, en fait. Aujourd’hui, j’ai récolté un avertissement en sport (« Ben fait le pitre en cours ») et une punition à la cantine (cent fois à copier « Je ne dois faire le pitre »). Pas grave, ce sont les risques du métier. 
A la maison, je profite des repas pour m’entraîner. Ce soir, par exemple, je m’exerçais à la mimique « Hm, quelle délice, ce plat a l’air tellement succulent que je m’en faire péter le ventre ». Maman a attrapé un tel fou rire qu’elle a failli s’étrangler avec un morceau de viande. Papa n’a pas le sens de l’humour : il m’a exilé au garage jusqu’à la fin du repas. Quant à Line, elle est restée totalement indifférente à mes essais artistiques. Elle était trop occupée dans la contemplation béate de ses ongles peinturlurés de frais. Elle n’a même pas réagi quand je l’ai imitée. 
Si je fais le calcul, j’ai réussi trois « expressions faciales » et raté une bonne vingtaine de « grimaces », vingt et une si j’ai bien compté. J’ai amplement mérité un nouveau diplôme.

« Diplôme accordé à Ben Letourneux
 le 7 juin
pour avoir fait 21 grimaces qui lui ont mérité le titre de « pitre de l’année ».
(dessin des grimaces)
Libre à chacun de choisir sa grimace (ou expression faciale) préférée.
Le jury »

Bien. Maintenant, je vais répéter mon numéro pour le spectacle. C’est un numéro de clown, évidemment, mais je jongle aussi avec des cuillères en bois et je joue du saxophone. Je ferme à clé la porte de ma chambre, parce que c’est top secret et j’ai repéré une espionne planquée dans la chambre à côté.



14 juin

Record de « merci ! »