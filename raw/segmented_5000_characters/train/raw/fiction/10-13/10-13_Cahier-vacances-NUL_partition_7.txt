Pourquoi je parle de ça ? Aucun intérêt ! Je rature…
(gribouillis) 
… oh, ça ressemble au gâteau que j’ai mangé au goûter. Parce qu’il y avait un goûter aussi. Ils ne font que manger, les petits vieux.
A propos de vieux ! Les miens n’étaient pas là quand je suis rentré. J’ai trouvé un mot de mon père sur la porte : Je suis chez les Berkassi. Appelle-moi dès que tu rentres. Et sur le frigo était collé un message de ma mère : Je vais chez Zorah. Réchauffe-toi une pizza.
Tiens, tiens… Etant donné que Zorah, c’est Mme Berkassi, mes parents passent la soirée ensemble chez des amis. Sont-ils réconciliés, ou maman a-t-elle peur que papa fasse les yeux doux à Zorah Berkassi ? 
Hou, là, là, trop compliqué pour moi.

Il est neuf heures et je suis seul à la maison. 

Je n’ai pas vraiment faim. Le goûter me pèse encore sur l’estomac. Ou bien, c’est autre chose.

En plus, la télé est en panne. Quand j’allume, voilà ce que je vois sur l’écran :
(écran parasité)

Encore des gribouillis !



18 juillet

Ma mère a le sens de l’humour. Si, si…
Petit déjeuner ce matin. Elle consulte son agenda électronique, soupire :
- Trois réunions aujourd’hui, quatre rendez-vous, un rapport à terminer… Vivement les vacances !
Un petit temps d’arrêt, plus un regard noir à mon père (qui depuis cinq minutes tourne une petite cuillère dans son bol de café sans sucre). Puis :
- Ce qui est bien, quand on est vacances, c’est qu’on n’a pas d’obligations, pas d’emploi du temps, pas d’agenda, quoi !
Et tout à trac elle enchaîne :
- Cédric, tu n’oublieras pas de graisser le portillon du jardin. Et de porter les rideaux au pressing. Et d’acheter des bocaux à confiture. Et de cueillir les cerises chez M. Demirel. Et… (J’abrège, la liste était interminable). (Eric, c’est mon papa. Tiens, pourquoi je précise ça, puisque je suis le seul à pouvoir lire ce cahier ? C’est au cas où des Martiens le liraient, trois siècles après ma mort. Grâce à moi, ils découvriraient ce qu’était la vie sur Terre au XXIème siècle. Une drôle de responsabilité : à partir de maintenant, je vais m’appliquer…)
Papa-Cédric n’a pas réagi. Il a regardé maman avec des yeux de chien battu, pas encore remis du coup de massue reçu sur le crâne. Mais moi, j’ai été pris d’un fou rire irrésistible. Comme j’avais la bouche pleine de Chocoloulou, j’ai mitraillé la cuisine de céréales au chocolat. Jusque dans l’évier. Et sur la tête de papa, paf, en plein dans la cible (je veux dire : le morceau de crâne dégarni, premier signe d’une calvitie précoce). En plus, j’ai manqué m’étrangler.
Ça n’a pas fait rire maman. Je retire ce que j’ai dit : elle n’a pas le sens de l’humour. D’une voix pincée et haut perchée, elle a menacé :
- Toi… toi… (Là, quelques secondes de silence, le temps qu’elle cueille les trois Chocoloulou atterris sur son chemisier.) Toi… occupe-toi intelligemment !
Ouf ! J’ai cru que j’allais avoir ma liste, moi aussi ! Mais non. Après s’être débarrassé du dernier Chocoloulou accroché à sa frange, elle est partie en faisant sonner bien fort ses hauts talons.

Ma mère a raison sur un point : ce qui est bien, quand on est vacances, c’est qu’on n’a pas d’obligations. C’est vrai : qu’est-ce que j’ai fait aujourd’hui ? Pas grand-chose :
- un peu de Mathématique, du calcul pour être précis, addition et soustraction. J’ai vidé mon portefeuille et ma tirelire pour établir l’état de mes finances. Total : 27,24 euros, un ticket de réduction de 5 euros pour l’achat d’une bétonneuse, deux jetons en plastique (usage inconnu) et un mouchoir en papier absolument neuf. J’ai dû soustraire 18 euros que je devais à papa depuis deux mois et 2,49 euros pour l’achat d’une boussole en plastique. Total, calculé de tête : 6,75 euros. 

- de la Géographie : la boussole, c’est pour découvrir le trésor dont M. Demirel a parlé dans sa lettre à son neveu. Ça m’intrigue beaucoup cette histoire. Je n’ai pas osé poser de questions à M. Demirel parce qu’il n’a pas l’air d’apprécier son neveu. C’est peut-être une blague, le trésor, mais on ne sait jamais. J’ai acheté la boussole pour dessiner la carte. C’est logique : qui dit trésor, dit carte. J’ai donc mesuré le jardin de M. Demirel et déterminé l’orientation avec la boussole. Sauf qu’elle est en plastique et je ne suis pas sûr qu’elle soit très précise. Le plus gros problème est que je n’ai découvert aucun cactus dans le jardin. Je me suis quand même piqué en tombant dans un massif d’orties.