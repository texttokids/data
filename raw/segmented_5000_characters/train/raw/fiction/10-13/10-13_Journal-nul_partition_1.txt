REGLE n° 1 : nous devons raconter notre vie, étaler nos sentiments, détailler nos « faits et gestes » (dixit la prof) ;
REGLE n° 2 : il faut remplir au moins quatre pages par semaine ;
REGLE n° 3 : attention à l’orthographe, l’emploi des temps, la qualité du vocabulaire (par exemple, si on a laissé échapper un « merde » en se tapant sur les doigts, il faut écrire « zut » pour éviter l’emploi de « vocabulaire trop familier ») ;
REGLE n° 4 : le « projet autobiographie » durera trois mois et… SERA NOTé !

Et ainsi de suite jusqu’à la règle 16 : 
Interdit de recopier les programmes de télévision (pas assez intime, paraît-il).

Non, mais qu’est-ce qu’elle s’imagine, la prof ? Que je vais raconter ma VRAIE vie ? 
Par exemple : 
que je garde mes chaussettes pour dormir (si ma mère le savait, elle me passerait direct à la machine à laver) ? 
que je mange des céréales Chocoloulou à la banane au petit-déjeuner ? 
que je m’entraîne à remuer mon oreille droite sans faire bouger mon nez (c’est pour mon prochain numéro de clown) ? 
que je fais souvent des recherches interdites intimes secrètes privées personnelles scientifiques sur internet ?

Pas question. C’est contraire aux droits de l’enfant, j’en suis sûr.
De toute façon, ma vie n’a rien d’intéressant ; pas à ma connaissance, en tout cas. 
J’ai trouvé la solution à ce (petit) problème : je vais m’inventer une vie 100% autobiographique et remplir mes quatre pages hebdomadaires pour que la prof me fiche la paix. En écrivant en grosses lettres une ligne sur trois, ça ne devrait pas me prendre trop de temps. Allons-y…


(Faux) JOURNAL DE BEN LETOURNEUX
« 17 septembre
Mon cher Journal,
à toi seul, je vais confier mes secrets les plus secrets. Je n’ai pas besoin de me présenter car tu me connais très bien. Je précise juste que je mesure 1,67 mètre depuis hier soir et que je pèse 49 kilos depuis ce matin. 
Mon plus gros secret, c’est ma passion pour la lecture. Je ne peux pas en parler au collège, car tout le monde se moquerait de moi. Chez moi aussi, je dois me cacher pour lire. Mes parents trouvent que c’est du temps perdu et que je ferais mieux de réviser mes cours de math, de ranger ma chambre ou de bêcher le jardin. Quant à ma sœur, elle se paye ma tête quand elle me voit avec un livre entre les mains : elle m’accuse de regarder seulement les images et d’être incapable de déchiffrer trois lignes. Pour lire, je dois m’enfermer dans le grenier ou attendre la nuit que tout le monde dorme. Comme je suis astucieux, je lis avec une lampe frontale. J’ai dû économiser pendant six mois pour en acheter une. J’en ai trouvé en promotion à Décolama à 3,99 euros, mais comme les piles coûtent 8,51 euros l’unité, ça revient cher à l’usage. 
Le dernier livre que j’ai lu, ou plutôt que j’ai dévoré, s’intitule « Le cirque de tous les dangers ». C’est l’histoire d’un clown tchèque, Lukas Vasel, qui recueille Benjamin, un adolescent en fugue… »


Là, je recopie une fiche de lecture que j’ai écrite l’année dernière pour ma prof de français de 6ème. Je n’ai jamais lu « Le cirque de tous les dangers » : j’ai inventé une histoire en rapport avec le titre et ça a très bien fonctionné. Voilà, je vais remplir deux pages sans trop me fatiguer et j’en aurai terminé avec mon journal pour aujourd’hui.
J’ai chronométré : il m’a fallu 17 mn et 34 secondes pour écrire ces c……, euh pardon : niaiseries (plus 51 secondes pour consulter le dictionnaire de synonymes). 
Au cas (absolument impossible) où des yeux non autorisés tomberaient sur ces lignes :
1) prière de les ramasser vite fait et de les jeter à la poubelle (beurk, des yeux qui tombent sur MON journal INTIME, quelle image dégoûtante) ;
2) je précise : -je n’aime pas lire-. Je raye parce que ce n’est pas tout à fait vrai. Je lis de temps en temps : mangas, livres sur les clowns, journal local et plein de trucs sur internet. Mais je lis rarement de « vrais » livres. Sauf ceux que ma sœur lit en cachette, des histoires d’amour débiles, mais il y a des passages… intéressants (et comme par hasard, les livres de ma frangine s’ouvrent direct à ces passages !) ;
3) mes parents ne m’empêchent pas de lire, au contraire ils me répètent sans cesse : « tu devrais lire davantage, tu ferais des progrès en français » ; je ne vois pas le rapport.
4) je m’arrête pour aujourd’hui, car j’ai horriblement faim ; voilà presque une heure que je n’ai rien avalé et il reste une part de hachis Parmentier caché derrière le pot de fromage blanc. Je vais étaler par dessus une bonne couche de moutarde, des oignons et des cornichons et réchauffer le tout au micro-onde. Hmmmmmmmmm ! Ça me permettra tout juste de tenir jusqu’au dîner.



19 septembre