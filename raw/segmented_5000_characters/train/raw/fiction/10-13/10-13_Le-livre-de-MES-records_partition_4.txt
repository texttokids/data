Tout a commencé par mon réveil qui n’a pas sonné. Je l’avais bien réglé, mais la pile a rendu l’âme en pleine nuit. Bien sûr, ma mère est venue me secouer à 7 heures précises, donc pas de danger que j’arrive en retard à l’école. Mais moi, je mets le réveil à 7 heures moins cinq pour me réveiller en douceur. Pour avoir le temps de dire bonjour au jour, à ma chambre, à mon nombril et à mes doigts de pied. Si je suis réveillé brusquement, je me sens bizarre, comme si j’avais mal au cœur. Et la journée est ratée.
Pour être ratée, ça, elle a été ratée.
Encore groggy de sommeil, je suis allé prendre ma douche. Le chauffe-eau était en panne. Heureusement que je m’en suis aperçu à temps. Pas d’eau chaude, pas de douche. Bon, pas grave. Je me suis mouillé les cheveux pour faire semblant et je suis descendu à la cuisine.
Troisième panne : il ne restait plus de Chocoloulou, mes céréales préférées. Qu’est-ce que j’allais manger ? Je me suis goinfré de cookies au chocolat, le seul truc mangeable que j’ai trouvé dans les placards. Et là, j’ai vraiment attrapé mal au cœur. En plus, impossible de me chauffer un bol de chocolat, il n’y avait plus de gaz. Panne n° 4.
Je suis allé au WC pour... faire ce que j’avais à y faire. Sans réfléchir, j’ai fermé à clef. J’avais oublié que la serrure se bloque deux fois sur trois. Impossible de rouvrir. Maman a dû démonter la serrure. Panne n° 5. Et il n’était que 7 heures 22, d’après ma montre. 7 heures 58 d’après l’horloge de la cuisine. Ah bon ? Panne n° 6 (ma montre).
J’ai dû courir pour attraper le bus. Il était à l’heure, lui, exceptionnellement. Mais au bout de trois cent mètres, le moteur s’est mis à tousser, à crachoter, à râler et le bus a stoppé au beau milieu de la route. J’ai dû continuer à pied. Panne n° 6.
J’ai pris mon temps. Avec Martin, qui était dans le bus avec moi, on a fait un concours à celui qui sauterait sur un pied le plus longtemps. J’ai perdu parce que mon lacet s’est cassé et je suis tombé sur le nez (panne n°7).
Quand on est arrivé en classe, les autres terminaient un problème de math. La prof m’a envoyé au tableau pour la correction. Chaque fois que je voulais écrire, la craie se brisait en petits morceaux (panne n° 8). « N’appuie pas aussi fort ! » m’a dit la prof. Alors, j’ai essayé de tenir la craie le plus délicatement possible, mais elle m’a glissé des doigts et s’est écrasée au sol (panne n° 9). La prof m’a renvoyé à ma place. Quand j’ai voulu m’asseoir, la chaise s’est effondré sous moi (pourtant je ne pèse que 44 kilos pour 1m 65). C’était un pied qui était cassé. Panne n° 10.
Et ça a continué toute la journée. 23 pannes en tout. Je ne vais pas tout raconter en détails, parce que ce serait trop fatigant (pour moi). J’ai symbolisé les pannes par un dessin. Avec un peu imagination, vous pourrez deviner ce qui m’est arrivé. (De toute façon, si vous avez bien vu l’avertissement, vous ne lisez PAS ce livre.) Je raconte juste la panne 21. Après manger, parce que c’était mon tour, j’ai rangé la vaisselle dans le lave-vaisselle. Avec un grand sourire, Line m’a annoncé qu’il était en panne et que je devais faire la vaisselle à la main.
- Ça va pas la tête ? j’ai dit. 
- Désolé, mais c’est ton tour, a-t-elle répliqué.
- Mon tour de lave-vaisselle, pas de vaisselle !
- Mais comme le lave-vaisselle est en panne, tu dois le remplacer...
- Je ne m’appelle pas Melomatic 346 E (C’est la marque de notre lave-vaisselle.) !
- Non, c’est vrai, mon cher Ben à ordures ! (Une des plaisanteries préférées de ma sœur. Je ne prends même plus la peine d’y répondre.)
Etc., etc. A la fin, les parents nous ont obligés à faire la vaisselle tous les deux, elle à la plonge, moi au torchon pour essuyer.
Ce serait bien si les parents tombaient en panne de temps en temps. On les enverrait chez le réparateur et ils reviendraient tout neufs, bien huilés et toujours contents. 
Mais là, faut pas rêver...



10 mars

Record du fils indigne