Je rereprends. 
J’ai passé l’après-midi chez Lukas et Justine. Ils sont allés chercher M. Demirel à la maison de retraite. Au fait, je l’appelle Roger maintenant, c’est lui qui me l’a proposé.
On a goûté dans le jardin, sous le tilleul. Ensuite, Lukas et Justine ont montré la chambre qu’ils préparent pour leur futur bébé. Il ne naîtra que dans cinq mois, mais ils ont commencé à rénover et repeindre.
Roger a regardé avec attention, un long moment. Il frappait en rythme sa canne sur le parquet ciré. Et puis il a dit :
- Vous êtes montés au grenier ?
Lukas (il est muet, mais pas sourd, je rappelle) a expliqué en gestes que non, bien sûr, ils n’étaient pas montés au grenier, qu’il savait que M. Demirel y avait laissé des objets personnels, et ça ne leur viendrait pas à l’idée de fouiller là-dedans (à moi si !!!).
Justine a dit la même chose, mais avec des mots (Roger ne déchiffre pas le lukassien aussi bien que moi).
- Allons-y ! a décidé Roger, en levant sa canne pour donner l’ordre de marche.
On a dû le soutenir pour grimper l’escalier raide qui mène au grenier. Là, il nous a montré trois grands paquets plats, soigneusement enveloppés dans des couvertures militaires.
- Descendez-les, a-t-il dit sans plus d’explication.
On s’est exécutés. J’ai descendu un paquet (le plus lourd, je précise) et Lukas les deux autres.
On les a déballés dans la chambre. C’était un lit de bébé en bois, peint en blanc et décoré d’une ribambelle de clowns.
Roger a raconté :
- Suzanne et moi, on voulait avoir des enfants, au moins quatre. Mais on n’en a pas eu. Après une fausse-couche, Suzanne a dû être opérée et…
Il a fait des ronds en l’air avec sa canne, très lentement. Puis il a continué :
- On avait commandé ce lit à un menuisier, un de mes cousins. C’est Suzanne qui a peint les clowns. On s’est rencontrés dans un cirque, vous savez…
Il y a eu un grand silence. Justine avait les larmes aux yeux. M. Demirel fixait la guirlande de clowns avec un air étrange, douloureux. Moi, j’osais à peine respirer, j’avais comme un poids sur l’estomac.
Soudain, Lukas m’a touché l’épaule. Je l’ai regardé. D’un doigt, il a dessiné un sourire sur ses lèvres et a frappé son cœur de la main gauche. Puis il m’a fait un geste de la tête pour dire « à toi ».
J’ai compris. J’ai sauté en l’air, suis retombé accroupi, et me suis mis à chialer exagérément, de grands sanglots tragiques. C’est un numéro qu’on a répété avec Lukas. Ensuite, il me prend dans ses bras et me berce comme un bébé. Normalement, tout le monde s’écroule de rire à ce moment-là. 
J’ai vu le visage de Justine s’éclairer et Roger a souri, franchement.
Et moi, j’agitais les pieds et les poings, et Lukas me secouait pour me calmer. Roger s’est mis à rire ; Justine aussi, plus timidement. 
Mais tout à coup, je ne sais pas ce qui s’est passé, des tas de trucs ont remonté en moi. Les bébés jamais nés de Roger et Suzanne, Arianna qui me traite de « scemo » et de « fesso », et… des trucs… des trucs bizarres et tristes.
Alors j’ai pleuré pour de vrai, de plus en plus fort. Impossible de m’arrêter. Lukas m’a posé au sol, m’a serré contre lui. J’ai trempé son t-shirt de larmes et de morve. 
Enfin, il m’a soulevé la tête avec ses deux mains, m’a regardé dans les yeux, et ça s’est passé. 
Je n’avais même pas honte. On est redescendus en silence à la cuisine. Lukas a préparé une tisane. 
- Si jamais il y a une grande sécheresse, je compte sur toi, Ben, a dit Roger. Avec toute l’eau que tu produis, on pourrait arroser le parc municipal.
- Et remplir la piscine, ai-je ajouté. Au moins le petit bassin.
Justine s’est assise à côté de moi. Elle a versé la tisane dans les tasses. Et elle a parlé de son groupe « acrobatie » de l’école du cirque. Je ne savais pas que Soraya s’était inscrite. Elle est très douée, paraît-il. 
- Au début, je pensais qu’elle ne venait que pour suivre sa copine Arianna, a dit Justine. Mais non, elle est vraiment passionnée. Elle a un sens de l’équilibre incroyable. 
- Et Arianna, elle est comment ? a demandé -M. Demirel- Roger (j’ai un peu de mal à m’y faire).
Je l’ai regardé, étonné, mais il était occupé à ouvrir un paquet de madeleines (bio).
Justine a hésité un instant.
- Arianna ? Elle a une énergie formidable, mais elle est trop émotive. Pas étonnant, avec ce qu’elle vit. 
J’ai retenu mon souffle. Ce que je déconseille absolument de faire quand on a un morceau de madeleine (bio) dans la bouche. Je me suis étranglé, Lukas m’a tapé dans le dos, j’ai recraché le morceau de madeleine (bio) et l’ai re-avalé aussitôt (c’est bon, c’est bio).
- Oui, a repris Justine, son frère jumeau est gravement malade, une leucémie, je crois. Et son père est rarement là. Elle vit avec sa mère et sa grand-mère italienne…
J’ai senti les larmes remonter droit vers la sortie, et j’ai pensé : ça y est, je vais me remettre à pleurer comme une madeleine. Et top, mon cerveau a percuté : madeleine = bio = s’étrangler = … je ne sais pas quoi. En tout cas, ça a stoppé net mon circuit d’eau interne. J’ai respiré un bon coup et j’ai écouté à nouveau Justine. Mais elle parlait d’autre chose avec Roger, de rosiers à couper et d’oignons de tulipe.
Lukas m’a pris la main gauche. De son index, il a tracé deux ronds dans ma paume et un trait en diagonale. J’ai refermé ma main, l’ai posée sur mon front, mes lèvres, ma poitrine. Ça voulait dire… 
Non, je ne l’écris pas.