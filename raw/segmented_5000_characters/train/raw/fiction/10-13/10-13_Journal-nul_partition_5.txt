Cet après-midi, après le collège, je suis passé dire bonjour à M. Demirel à la maison de retraite. Il était dans le jardin en train de lire une revue. Avant même que je lui dise bonjour, il m’a tendu un paquet de gaufrettes. 
- Ton goûter, a-t-il dit.
Des gaufrettes fourrées à la noisette… mon ventre en a gargouillé de plaisir. Je tire une gaufrette du paquet, je la porte à ma bouche, je remarque juste à temps qu’il y a écrit quelque chose sur la gaufrette, deux mots : AMOUR TOUJOURS ?
C’est le point d’interrogation qui m’a déprimé, je crois. Ça a dû se voir sur ma tête, car M. Demirel m’a demandé :
- Tu as quelque chose sur le cœur, Ben ? 
- Euh non, ai-je soupiré.
Il a regardé ma gaufrette et a soupiré à son tour :
- Ah, l’amour...
Je sentais bien qu’il se moquait (gentiment) de moi. 
Sans transition, il m’a raconté comment il a rencontré sa femme à un spectacle de cirque. Elle était assise deux rangs devant lui, et il a raté tous les numéros : il ne regardait qu’elle.
MOI : Même pendant les numéros de clowns ? 
M. DEMIREL : Ah, les clowns ! Ça, c’était le bouquet !
MOI (légèrement vexé) : Comment ça ? 
M. DEMIREL : Mais parce qu’elle riait aux éclats et montrait toutes ses fossettes ! C’était le coup de grâce !
MOI : Et vous avez fait comment pour l’aborder ?
M. Demirel a fait craquer les jointures de ses mains (un bruit affreux, ça donne la chair de -poule- poulet) :
- J’avais une tablette de chocolat dans ma poche. Alors, à la sortie, je me suis précipité vers elle et je lui ai tendu le chocolat en disant : « Mademoiselle, vous avez perdu quelque chose. » 
MOI : Ça a marché ?
M. DEMIREL : Non. Elle m’a à peine regardé. Et je l’ai entendue dire à la fille qui était avec elle : « Non, mais quel nigaud, celui-là ».
MOI : C’est quoi un nigaud ?
M. DEMIREL : Un type pas malin, un idiot.
MOI : Et après ?
M. DEMIREL : La semaine suivante, je suis retourné au cirque. Et qui vient s’asseoir à côté de moi ? Suzanne ! Sauf que je ne savais pas encore qu’elle s’appelait Suzanne… Tu sais ce qu’elle a dit en me voyant ?
MOI : Bonsoir ?
M. DEMIREL : Presque ! Elle a dit : « Bonsoir, monsieur nigaud ». 
MOI : Oh, c’est pas sympa.
M. DEMIREL (rêveur) : Elle l’a dit avec le sourire… Et tout a commencé ce soir-là. Le plus drôle, c’est qu’elle m’a toujours appelé comme ça par la suite : « mon petit nigaud », ou même « mon nigaud adoré ».
Silence.
MOI (sans réfléchir) : Dans ma classe, il y a une fille qui me dit « chémo » chaque fois qu’elle me croise. C’est de l’italien, mais je ne sais pas ce que ça signifie.
M. DEMIREL : De l’italien ? Viens, on va demander à Maria, elle est née en Italie.
Il m’a traîné jusqu’au deuxième étage, chambre 207, et m’a présenté à une petite dame mince, aux beaux cheveux blancs bouclés. J’ai tendu la main à Maria (Mme Locatelli, en fait) et elle l’a serrée dans les siennes en s’écriant :
- Oh qu’il est « carino », ce petit (alors que je mesure au moins dix centimètres de plus qu’elle) ! Fais-moi un bisou, mon chou !
Oups ! J’ai dû embrasser les joues de la vieille dame qui ne lâchait pas ma main. 
M. DEMIREL : A propos d’italien, Maria, ce jeune homme a besoin de ton aide.
J’ai dû raconter la moitié de ma vie à Mme Locatelli qui continuait à me tripoter la main, tout en hochant la tête d’un air très concentré. Quand j’ai eu fini, elle a pris un air grave et j’ai tout de suite compris qu’elle allait m’annoncer une mauvaise nouvelle :
- « Scemo », mon poussin (cui-cui), ça veut dire « idiot ». On disait nigaud autrefois, n’est-ce pas, Roger ?
M. DEMIREL : Exact.
Mme LOCATELLI : Et ça s’écrit S.C.E.M.O. 
MOI : Et « seille scemo » ?
Mme LOCATELLI : Sei scemo ? « Tu es idiot », tout simplement. C’est mal parti pour toi, mon chaton (miaou). Ton Arianna m’a tout l’air d’une pimbêche. Tu ferais mieux de rester avec moi. Allez, fais-moi un  autre bisou.
J’ai regardé désespérément M. Demirel, mais il m’a laissé dans les mains de l’ogresse. Comme je ne réagissais pas, c’est elle qui m’a embrassé sur les joues, smatch, smatch, smatch, smatch (4 fois !!!!).
J’étais au bord de l’évanouissement. Quand Mme Locatelli m’a (enfin) libéré, je me suis effondré sur une chaise. Elle, tranquillement, a continué sa leçon d’italien :
- Si elle te traite de « stupido », « cretino », « deficiente », « tonto » ou « fesso », ce n’est pas bon signe non plus, chéri ! Dans ce cas, viens me voir, je te consolerai !
Merci bien ! J’ai reculé le plus loin possible avec la chaise avant de m’apercevoir que M. Demirel et Mme Locatelli riaient comme des perdus. 
- Sei un po’ fesso, è vero, a dit la vieille dame, ma molto carino. « Carino », ça c’est un compliment, ça veut dire « mignon », à peu près.
J’ai prétexté n’importe quoi pour m’éclipser et suis rentré tout troublé à la maison. 
Et maintenant, je dois trouver une idée pour mon « journal intime ». Sans dessin, ni collage, ni… Ah, j’ai une idée :
1) j’écris « en vrai » (c’est-à-dire ce que j’ai vraiment sur le cœur).
2) j’utilise un traducteur automatique pour traduire tout ça en russe, puis du russe vers le chinois, du chinois vers le zoulou, du zoulou vers le bulgare, et ainsi de suite à travers six ou sept autres langues retour vers le français (je ne donne pas les détails, je me méfie des espions).
Voilà ce que ça donne (juste un extrait : je réserve l’intégralité à la prof de français ; en tant qu’ancienne espionne, elle appréciera).