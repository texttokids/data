Premier avantage : papa a été bien content de se débarrasser de moi et a déblayé tout seul le grenier.

Pendant le déjeuner (commencé à treize heures pile, si j’en crois le coucou suisse de la cuisine), ma mère avait un comportement ultra bizarre. Je l’observais en douce, me demandant toujours ce qu’elle avait trafiqué ce matin. Elle prenait des poses,  coude sur la table et menton sur la main, n’arrêtait pas de se passer la main dans les cheveux, arrangeait son décolleté (elle avait mis une robe d’été). Au début, mon père ne s’est aperçu de rien. Puis je l’ai vu froncer les sourcils, arrêter la fourchette (chargée de rondelles de pommes de terre grillées) entre l’assiette et sa bouche, scruter ma mère un long moment, avaler finalement les pommes de terre, puis ouvrir grand les yeux et s’exclamer (la bouche encore pleine) :
- Tu es allée chez le coiffeur !
Ma mère a soupiré : « C’est maintenant que tu t’en aperçois », mais elle souriait, toute contente de son effet.  En regardant bien, j’ai constaté que, en effet, elle était chez le coiffeur. Elle avait même changé de couleur. C’était donc cela son rendez-vous  mystérieux ! Décevant. Est-ce qu’elle s’était mis en tête de séduire mon père à nouveau, comme il y a vingt ans ? Elle a battu les cils et murmuré :
- Je te plais ?
Moi : Bof !
Mon père : Oui, oui !
Ma mère : C’est la coiffure que j’avais quand on s’est rencontrés.
Moi : Ah bon ?
Mon père : Oui, oui !
Ma mère : Tu te souviens ?
Moi : Non.
Mon père : Oui, oui !
J’avais compris que ma mère ne s’adressait qu’à mon père, mais c’était amusant de mettre mon grain au sel. Au début, du moins, parce que c’est devenu très vite lassant. A deux heures moins le quart (coucou suisse faisant foi), j’ai demandé :
- Qu’est-ce qu’il y a au dessert ?
Pas de réponse. Je me suis donc servi deux boules de glace à la fraise et je suis remonté dans ma chambre. J’ai médité quelques minutes sur l’attitude étrange de mes parents pour conclure que, bon, il n’y avait rien à comprendre. L’amour rend les gens idiots, c’est bien connu.
Le téléphone a sonné alors que j’en arrivais à cette conclusion puissante. C’était ma sœur, une fois encore. 
- Allo, c’est moi ! a-t-elle chuchoté.
Moi : Ouais, ben, c’est moi aussi. Ça va, moi ? Parce que moi, ça va couci-couça.
Ma sœur : Hein, quoi ?
Moi : Moi !
Dix secondes de silence.
Ma sœur : Ben, c’est toi ?
Moi : Oui, et toi, c’est toi aussi ?
Ma sœur : Arrête, je ne comprends rien à ce que tu racontes. Et… j’ai besoin de toi.
Moi : Tiens, tiens. C’est encore à cause de ton Marco, je suppose.
Ma sœur : Euh, oui.
Moi : Tu veux en savoir plus sur la physique quantique ?
Ma sœur : Non… je… euh… c’est-à-dire…
Moi : Sur les maths ? La chimie ?
Ma sœur : Non, la poésie contemporaine.
Moi : Quoi ? Qu’est-ce que c’est que ça ?
Ma sœur : La poésie, c’est… la poésie… et contemporaine, c’est… euh… de notre époque.
Moi : Parce qu’il y a encore des gens qui écrivent de la poésie à notre époque ?
Ma sœur : Faut croire. En tout cas, Marco il est passionné de poésie, et j’ai pensé… Ce serait bien si j’en connaissais quelques-unes… pour parler avec lui…
Moi : Tu es sûr qu’il est normal, ce type ? 
Ma sœur (soupir qui fait trembler le téléphone) : Il est trop mignon… et super intelligent… S’il te plaît, Ben, essaie-moi de trouver quelques poèmes contemporains. Je ne connais que La Fontaine et Victor Hugo.
Alors, bing, dix lampes s’allument dans mon cerveau ! Bien sûr que je vais lui trouver des poésies contemporaines ! Suffit de tendre la main sous mon lit, tirer le carton avec les affaires de mon père, farfouiller dans le tas de papier, extraire le carnet de poésies qu’il a écrites pour draguer ma mère. 
Moi : Tu as ce qu’il faut pour noter ? Je dicte !
Ma sœur : Tu as déjà trouvé ?
Moi : Oui. Attention, j’y vais.
« Ta voix, tes yeux, tes larmes
et mes mains qui tremblent de toi
Ma nuit, mes rêves, mes chemins
engloutis sous ton ombre… »
J’avoue que je sautais des lignes de temps en temps, mais ma sœur a noté sans commentaire. A la fin, elle a demandé :
- C’est comment le nom de l’auteur ?
Moi (improvisant) : Euh… Cédric Letour… Oui, c’est ça : Cédric Letour. 
Elle n’a même pas tiqué. Elle m’a remercié et a raccroché. Machinalement, j’ai regardé ma montre : il était quatorze heures trente précises.
Je me fais un peu de souci pour elle. Son Marco a l’air légèrement toqué : physique quantique et poésie contemporaine, c’est louche, non ?