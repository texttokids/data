Justine et Lukas ont invité M. Demirel à déjeuner. Avec Tante Rosie. Et moi, bien sûr. 
- Tu l’as invité pour faire baisser la moyenne d’âge ? a demandé M. Demirel à Justine.
- Juste pour faire la vaisselle, a répondu Justine en déposant sur la table un gratin aux courgettes bien cramé sur les bords. 
- Et pour finir les plats, ajouté tante Rosie ; c’est pratique avec lui, il n’y a jamais de restes !
Après le café, j’ai accompagné M. Demirel dans le jardin. Il s’est promené sans rien dire, retournant des cailloux ou des feuilles du bout de sa canne.
- C’est ici, le trésor ? ai-je demandé quand on est passé près des restes de l’ancien puits.
- Le trésor ? a fait M. Demirel. Ah, oui… Viens.
Je l’ai suivi jusqu’au fond du jardin, vers un mur de pierre à moitié écroulé. Essoufflé, il s’est assis sur une souche d’arbre. Avec sa canne, il m’a montré une pierre couverte de mousse.
- Soulève-la.
J’ai obéi. Elle était lourde et glissante. J’ai dû en déplacer quelques autres aussi. Est apparue une dalle de béton, usée, brunâtre. 
- Cherche bien, il doit y avoir une fente quelque part.
Effectivement. En repoussant quelques cailloux, j’ai découvert une fente ou plutôt un trou grossièrement taillé dans le béton. M. Demirel m’a tendu un bâton ramassé sous un buisson.
- Plonge-le dans la fente et dis-moi ce que tu en sors.
Je me suis exécuté. Le bâton est ressorti enduit d’une substance noirâtre, liquide.
- Eh bien ? a demandé M. Demirel.
Je l’ai regardé. Il me fixait d’un air grave, mystérieux.
- Du pétrole ! j’ai dit. Vous avez trouvé du pétrole dans votre jardin ! C’est ça, le trésor…
Le sourcil droit du vieux monsieur s’est soulevé.
- Sens !
J’ai senti. Je ne sais pas vraiment ce que sent le pétrole brut, mais certainement pas l’odeur que j’ai respirée en portant le bâton à mes narines. Pouah !
Moi : Ça sent… ça sent pas bon ! 
M. Demirel : Hé, non. Tu devines ce que c’est ?
Moi : Ça me fait penser à de la bouse de vache.
M. Demirel : Tu brûles.
Moi : Je ne sais pas. Du jus de quelque chose fermenté ? En tout cas, c’est pas un gisement d’eau de toilette.
Là, il a éclaté de rire. Son corps en était secoué, j’avais l’impression d’entendre ses os s’entrechoquer. J’ai eu peur qu’il se fracasse en mille morceaux. Je l’ai saisi par les épaules, j’ai balbutié :
- M. Demirel, ça va ? Ça va, vous êtes sûr ?
Et lui riait, riait, et ses yeux en pleuraient.
- De l’eau de toilette… Tu n’es pas si loin ; c’est de l’eau de toilette dans un certain sens.
Là, je l’ai regardé avec mon plus bel air bête. Pas besoin me forcer ; chez moi, c’est naturel.
Il a respiré un grand coup pour chasser les derniers restes de fou rire. 
- C’est du purin, a-t-il finalement. 
- Purée ! ai-je lâché (désolé, ça m’a échappé).
M. Demirel : Tu sais au moins ce que c’est, du purin ? C’est le liquide qui s’écoule du fumier, essentiellement du pipi de vache, quoi !
Moi (réellement indigné) : Et c’est ça, votre trésor !
Il m’a pris la main, l’a serrée dans la sienne.
- D’abord, c’est un vrai trésor, d’une certaine manière. Parce que c’est un très bon engrais. Et je crois qu’on peut en faire du carburant maintenant. Mais ce n’est pas important. 
Il a lâché la main, m’a regardé d’une certaine façon… d’une façon qui m’a réchauffé le cœur, parce que j’ai compris à ce moment-là qu’il m’aimait bien, M. Demirel. Et que moi aussi, je l’aimais bien. Vraiment.
- Je ne sais pas depuis combien de temps il est là, ce purin. Depuis longtemps, en tout cas, c’est certain. Je sais seulement qu’il y  avait une ferme autrefois sur le terrain, avant qu’on construise ma maison dans les années 40. J’étais bien plus âgé que toi quand j’ai découvert la dalle de béton ; elle était déjà cachée par les herbes et les pierres. Eh bien, moi aussi, j’ai cru qu’il y avait un trésor enfoui là-dessous… Alors, pourquoi ce ne serait pas un trésor, après tout ?
Je n’ai pas répondu. Je l’ai aidé à se relever. On est allé retrouver les autres qui se reposaient à l’ombre du tilleul. Justine était assise sur les genoux de Lukas. Tante Rosie somnolait. 
J’ai dit :
- Je vais faire la vaisselle.

PS : pas d’exercice aujourd’hui, j’ai assez travaillé, je pense. Ah si, ça :


« Lecture suivie
Lire « L’île au trésor » de R.L. Stevenson. »



21 août

Il est minuit et quart (ou, autrement dit, 0H 15). Je viens de me relever parce que :
j’avais un petit creux (de clafoutis aux cerises ; trop peur que papa ne le finisse -demain matin- tout à l’heure au petit déjeuner) ;
un moustique m’a piqué à l’intérieur de l’oreille droite (celle que j’arrive à faire bouger, je ne sais pas pourquoi ça ne marche pas avec l’oreille gauche) ;
il y avait un petit truc qui me trottait dans la tête, comme un petit grillon qui n’arrêtait pas de chantonner : « Mon petit Ben, tu as écrit des choses compromettantes dans ton cahier de vacances ; bien sûr, ça ne peut pas arriver, mais imagine que quelqu’un le lise ? »