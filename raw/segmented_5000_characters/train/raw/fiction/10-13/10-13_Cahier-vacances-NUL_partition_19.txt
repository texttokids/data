Alors :
je suis descendu à la cuisine et j’ai bâfré allégrement le reste de clafoutis (hm, encore meilleur qu’au dîner) ;
j’ai frotté un glaçon à l’intérieur de l’oreille droite (remède de mamie Annie quand j’étais petit ; je déconseille absolument) ;
j’ai relu ce que j’avais écrit aujourd’hui dans mon cahier ; et c’est vrai, je vais passer pour un idiot si quelqu’un tombe sur ces lignes (Supposition totalement irréaliste puisque (je le rappelle à un improbable lecteur) ce document est protégé par des technologies ultra secrètes et ultra dangereuses.) ; mais ça m’est égal : c’était la plus belle journée de mes vacances ; je serais bien incapable d’expliquer pourquoi.



24 août

Tout va bien. 
Ma sœur est rentrée de vacances. Elle ne me parle plus (on se demande pourquoi) et passe sa journée sur Internet ou au téléphone. Elle poursuit ses recherches sur la physique quantique, la poésie contemporaine, les danses sud-américaines et le rock allemand, je suppose. Pour être honnête, je dois ajouter qu’elle n’a (presque) plus de boutons sur la figure. C’est sans doute son bronzage qui les cache.
Ma mère me fiche la paix. Elle est trop occupée. « Les vacances, c’est fait pour se reposer » affirme-t-elle dix fois par jour. Alors elle enchaîne cours de yoga, massages, séances de relaxation et stage anti-stress. Un programme d’enfer.
Mon père bricole. Dès qu’il rentre du travail, il s’enferme dans le garage. On entend de temps à autre des coups de marteau ou des grincements de scie. Je lui demandé ce qu’il fabriquait. Réponse : « Oh, un truc. » 
Moi, je répète avec Lukas tous les après-midi. On donne notre numéro samedi au centre d’entraide pour les réfugiés. On est allé repérer la salle ce matin. Le centre est tout près de la maison de retraite ; je suis passé devant des dizaines de fois sans le remarquer. En rentrant, près de la boulangerie, on a croisé Arianna. Elle a salué Lukas, mais m’a ignoré. Enfin, pas complètement. Quand je suis passé près d’elle, elle m’a pincé le bras. Wouah, ça a fait mal. Je pense que c’est bon signe.
Tout va bien.

Et si tout va bien, restons zen. Donc…

« Education physique et sportive
Initiation au yoga. Posture du Baddha Konasana (l’angle lié) :
En position assise sur le plancher, sans s’affaler, joignez les plantes de pied. Les mains entourent les pieds ou les chevilles. 
Ramenez les talons le plus près possible du pubis.
 Inspirez en allongeant le dos.
 Respirez en vous penchant vers l’avant, le dos droit. Respirez dans cette position et sentez vos muscles se détendre. »



28 août

-Lukas et moi, on a présenté notre numéro au Centre d’accueil de réfugiés et demandeurs d’asile (c’est le nom complet).
J’avais le trac en entrant dans la salle. Mains moites, jambes en coton, cœur boum boum. Heureusement Lukas était là. Le public riait. Ça m’a dégelé totalement. J’ai eu l’impression que ça a duré deux minutes. En fait, on a joué pendant presque une heure. Il y a une note qui coince dans mon saxo. Le public, c’était surtout des (jeunes) adultes de plein de pays différents. Et quelques enfants. Il y avait aussi ma mère, ma sœur, Tante Rosie, M. Demirel. Lukas m’a dit qu’Arianna était là. Je ne l’ai pas vue. On est restés longtemps après le spectacle, il y avait un goûter. Maman avait fait une tarte aux mûres. J’ai parlé à des tas de gens. Lise s’est renversé un verre de jus de pomme sur le t-shirt. M. et Mme Scholler m’ont offert un canif suisse made in China. Un jeune Malien m’a appris un tour de magie. Ma mère n’a pas arrêté de tapoter des messages sur son portable. Une petite fille d’Azerbaïdjan m’a donné le ruban rose qu’elle portait dans les cheveux. Je l’ai noué autour de mon poignet.-
 
Je raye, parce que je n’arrive pas à raconter. C’est tout embrouillé dans ma tête.
Vaut mieux que je résume : c’était bien. Lukas était content. Moi aussi.

Je me demande si notre numéro a plu à Arianna.

PS : un texte comme ça, ma prof de français adorerait. Je vois très bien ce qu’elle en ferait.

« Explication de texte
Selon vous, qu’a voulu dire l’auteur ? Justifiez votre réponse. »



30 août