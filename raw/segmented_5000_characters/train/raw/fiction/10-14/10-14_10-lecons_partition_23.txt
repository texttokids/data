Les autres sont déjà là quand ils arrivent. Simon a commencé :
- Oui, écrire, c’est se déplacer sur l’espace de la feuille. Ou du cahier. Ou de la plage, si on écrit sur le sable. A vous de choisir l’itinéraire. Rien ne vous oblige de commencer en haut à gauche, par exemple. Tenez, je vous ai préparé des bandes de papier…
Il distribue des bandes d’environ 30 x 10 cm.
- … vous les prenez dans le sens de la hauteur, et vous allez écrire de bas en haut, à l’inverse du sens habituel. 
- Mais on écrit quoi ? demande Alice.
- Ce qui vient, dit Simon. Laisse-toi surprendre.
Marion s’installe à l’écart, sous une fenêtre. Elle prend la bande de papier, enlève avec ses dents le bouchon du stylo et se met directement à écrire : « J’ai très envie de dormir. » Elle est arrivée au bord de la bande. Elle soulève le stylo, le repose juste au-dessus de ce qu’elle vient d’écrire. C’est étrange, ce mouvement. Un mouvement contraire. Un instant le stylo a plané, comme un aigle, avant de s’abattre à nouveau sur la feuille. Elle écrit : « Planer au-dessus des nuages. Vivre mes rêves. M’en aller. » Elle écrit vite. Elle a l’impression qu’elle fuit, qu’elle remonte le puits dans lequel elle est tombée. 
Arrivée en haut de la feuille, elle lâche son stylo, pose la feuille à côté d’elle et ferme les yeux.
Quand tout le monde a terminé, Simon propose :
- Ceux qui veulent, allez faire un tour, détendez-vous, respirez, et oubliez ce que vous venez d’écrire. Vous reprendrez vos textes demain.  
Marion ne sort pas. Elle vient s’asseoir près de Kèv, lui tend son texte.
Kèv lit. 
- Regarde pas les fautes, dit Marion.
Après un temps, Kèv lui rend la bande de papier.
- C’est bizarre de lire à l’envers, c’est comme si on grimpait un escalier.
Marion est tout contre lui, épaule contre épaule. Il sent ses cheveux effleurer son cou.
- Et toi ? dit-elle. Tu me montres.
- Je sais pas. C’est pas terrible.
Elle lui donne un coup de coude.
- Tu te souviens pas de ce qu’a dit Simon, l’autre jour ? « Ne vous critiquez pas, les autres s’en chargent ». Donne, et je te dirai que c’est nul, si ça te fait plaisir.
Sans attendre, Marion s’empare du texte de Kèv, le lit à mi-voix.
« Demain, je mourrai pour toi.
Aujourd’hui, j’ai besoin de toi.
Hier, j’ai pleuré avec toi
Il y a six jours, j’ai souffert à cause de toi.
Il y a cinq jours, j’ai dansé avec toi.
Il y a quatre jours, j’ai pensé à toi.
Il y a trois jours, j’ai ri avec toi.
Il y a deux jours, j’ai rêvé de toi.
Hier, j’ai pleuré avec toi.
Aujourd’hui, j’ai besoin de toi.
Demain, je mourrai pour toi. »

- Ça se lit dans quel sens ? demande Marion.
- Comme tu l’as lu. Ou en commençant par le haut. C’est pareil. J’ai écrit depuis le bas, mais c’est revenu au point de départ.
- J’aime bien. Je peux le garder ? Je le mettrai dans la boîte.
- Si tu veux.
Kèv attend que Marion ait plié la feuille et l’ait glissée dans la poche de son jean. Et dit, à voix très basse :
- C’est pour toi que je l’ai écrit.

Plus tard. Simon a distribué une feuille à chacun. Il explique :
- Posez une main sur la feuille et, avec l’autre, dessinez le contour. Ensuite, écrivez sur le dessin de votre main. Imaginez que vous la tatouez…
- On commence où ? demande Noah. Par les doigts ?
- Par où tu veux, répond Lucie.
- Prends ce qui vient, dit Hector.
- N’aie pas peur, dit Marion. Fonce.
Hector a écrit très vite, joyeusement. Il a d’abord retracé les contours en répétant le mot « main », tout simplement. 
- Mais elle est énorme, ta main, a dit Alice.
Hector a tendu la main devant lui, l’a examinée d’un œil critique, a répondu :
- Oui, c’est vrai. Je ne m’en étais pas rendu compte.
Lila a écrit dans tous les sens. Elle a du mal à se relire. Simon lui conseille de « démouler » son texte. Autrement dit : de le réécrire telle qu’elle voudrait qu’on le lise. Voici le résultat :

« Ma main a cinq doigts :
Asma, Chloé, Samuel, Maïa et Lucie.
Asma est la plus gentille,
Chloé est la plus petite,
Samuel est le plus rapide,
Maïa est la plus intelligente,
Lucie est la plus grande.

Ma main a cinq doigts :
Asma, Chloé, Samuel, Maïa et Lucie.
Avec Asma, je pleure,
Avec Chloé, je ris,
Avec Samuel, je cours,
Avec Maïa, je joue,
Avec Lucie, je vis. »

Pédro l’écoute yeux grand ouverts quand elle lit. Et puis, à son tour, il regarde sa main, compte sur ses doigts, sourit. 
Ils bougent beaucoup ce matin-là, se déplacent, écrivent dans le couloir, sous le préau, dans la cour. Avec des mots, ils conquièrent l’espace.



Interlude