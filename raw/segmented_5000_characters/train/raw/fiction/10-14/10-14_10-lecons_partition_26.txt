« Derrière ce masque je me cache.
Je garde au plus profond de moi mon secret, 
mon secret.
Tous les jours j'efface,
j’efface ce que je suis.
J'efface les mots trop sensibles
mots qui paraissent doux et sont durs en réalité.
J'efface la douleur et j'efface la joie.
Ligne couleur ou ligne grise,
je possède deux faces et tu ne le sais pas.
Le temps n’a pas de couleur, pas de rythme, pourtant 
chaque jour j'efface une partie de ma vie.
Je n'ai jamais osé dévoiler qui je suis.
Je suis peut-être une gomme
qui efface efface efface.
Suis-je celle à qui tu as fait tant de mal ?
Oui c’est moi. Moi.
Et je te le dis
à toi
toi. »



Interlude

- Pourquoi ta mère t’a confisqué ton portable ?
Ils sont au fond du jardin de Mamie, sous le grand prunier, Kèv dans un transat, Marion assise sur le billot qui sert à fendre le bois. 
- On s’est disputées. 
- Mais il y avait bien une raison…
- Oui. Elle s’est aperçue qu’il manquait vingt euros dans son porte-monnaie et elle est persuadée que c’est moi qui les ai piqués. 
Marion ramasse une prune dans l’herbe, l’ouvre, s’aperçoit qu’elle est vermoulue, la jette par-dessus son épaule.
- Elle a peur que je tourne mal, que je devienne une délinquante, comme elle dit. Elle pourrait me faire confiance, quand même. 
Elle ramasse une autre prune. Elle a plus de chance cette fois. Elle enlève le noyau, avale une moitié de prune.
- J’ai juste envie de faire le contraire de ce qu’elle me dit. Un jour, j’ai laissé traîner des mégots dans ma chambre, pour lui faire croire que je fumais. Et elle l’a cru, vraiment ! Elle m’a fait une scène terrible. 
- C’est comme ma sœur, dit Kèv. Elle se disputait toujours avec ma mère, avant. Je me souviens, une fois, ça a vraiment chauffé, quand ma sœur s’est fait tatouer un serpent dans le cou, ma mère était folle…
Ils se taisent. On entend un bruit de moteur de l’autre côté de la haie. Un motoculteur, sans doute.
- Elle est morte de quoi, ta mère ? demande Marion.
- Une tumeur au cerveau, répond Kèv. Tu me donnes une prune ?
Marion ramasse trois prunes, les ouvre, en tend deux à Kèv, jette la troisième.
- En fait, l’argent, les vingt euros, c’est moi qui les ai pris dans son porte-monnaie. Mais je les ai pas volés. Je les ai cachés dans le tiroir de sa table de nuit. Je voulais voir si elle me faisait confiance, tu comprends ?
Kèv rit.
- Non, je comprends pas bien.
- Je voulais voir si elle me croirait ! C’est comme si elle les avait perdus, les vingt euros. Mais non, tout de suite, c’est moi la coupable !
- Tu es un peu compliquée, non ?
Marion rit à son tour.
- Ouais, peut-être. Mais elle aussi…
Kèv reste silencieux. Il la regarde. Tête baissée, les cheveux cachant son visage, elle gratte une croûte sur son genou.   
- Tu as de beaux cheveux, dit-il.
Elle relève la tête, lui sourit.
- J’ai envie de les couper.
- Oh non, fais pas ça, s’il te plaît, ils sont vraiment beaux.
Elle prend ses cheveux à deux mains, les relève, dégage son cou.
- Et si je faisais un chignon ?
- Oh ouais, tu ressemblerais à ma prof de maths ! Elle est canon.
Elle ne répond pas. Elle se lève. Une main retenant ses cheveux, l’autre posée sur la hanche, elle remonte l’allée, un pied devant l’autre, parodie de mannequin en défilé. Kèv, rapide, prend son portable, la filme. Quand elle se retourne et s’en aperçoit, elle exagère la pose, lui fait une grimace. Et puis étend les bras et tourne sur elle-même, lentement, yeux fermés et tête renversée vers le soleil couchant.
- Bouge pas ! dit Kèv.
Il prend ses béquilles, la rejoint. Elle s’est figée, bras écartés, visage offert à la lumière. Alors, il lâche ses béquilles et l’enlace, la serre contre lui, la berce. Elle passe les bras autour de son cou, pose sa tête contre son épaule, et tout se dénoue en elle. En lui aussi, quelque chose se détend, lâche. Il la serre plus fort contre lui. Elle caresse sa nuque, son dos. Il embrasse son cou. Elle frotte doucement sa joue contre la sienne.
Quand ils se détachent, ils se regardent étonnés, apaisés. 
Mamie appelle depuis la cuisine. 
- Marion, ta mère passe te prendre dans deux minutes. Hé, Marion, Kèv, vous entendez ?
Kèv ramasse ses béquilles, boitille aux côtés de Marion jusqu’au portail du jardin.
- Tu redonneras les vingt euros à ta mère, ok ? dit Kèv. 
- Peut-être, oui.
- Envoie-moi un SMS quand elle t’aura rendu ton portable…

Ce soir-là, Kèv reçoit un message à 22 h 25. Il répond aussitôt.



Leçon 10
« Je ne fais pas de différence entre un poème et une poignée de main. » (Paul Celan)