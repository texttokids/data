Ils sont dans le salon de Mamie, la grand-mère de Kèv. Ici, on ne dit pas « le salon », en fait, mais « la chambre». Il y a un canapé en cuir, une table basse, une télé grand écran, deux fauteuils et la table à repasser. 
En venant chercher Kèv, Mamie a dit à Marion :
- Tu viens avec nous. Ta maman a appelé. Elle a dû aller à Vesoul, à l’hôpital, Mariette va pas bien fort. Tu mangeras avec nous, et tu pourras rester jusqu’à ce que ta maman revienne.
Mariette, c’est Manou, la grand-mère de Marion.
Kèv est affalé dans le canapé, sa jambe plâtrée sur la table basse (avec un coussin pour protéger). Marion est assise dans un fauteuil tendu de velours vert. Tous les deux équeutent des haricots verts que Mamie a cueillis très tôt ce matin, avant de partir au travail.
Kèv a râlé. Il voulait regarder un match à la télé. Mais Marion a dit que ça ne la dérangeait pas. Elle a donné à Kèv un couteau et un plateau pour mettre les haricots équeutés ; elle, elle a pris une passoire. Marion va beaucoup plus vite que Kèv ; elle a déjà vidé une fois la passoire dans une cuvette en plastique. 
- Tu savais que ta grand-mère et Manou, elles étaient dans la même classe au collège ? dit Marion.
- Non, dit Kèv.
- Il paraît que ta grand-mère faisait enrager les profs qu’elle aimait pas.
- Ah ouais ?
- Si, une fois, elle a mis de la bouse dans le cartable d’un prof d’anglais parce qu’il avait traité une fille de vache tout juste bonne à regarder passer les trains.
- Ah bon, marmonne Kèv, suçant l’index qu’il vient d’entailler légèrement.
- Si, je t’assure. On peut demander à ta grand-mère.
- Non, non, je te crois.
Marion travaille vite. Elle équeute les haricots en utilisant ses ongles, longs et soignés.
- Toi, ça va le collège ? demande-t-elle. 
- Pas mal. Sauf en français. J’aime pas. Histoire-géographie, pas trop non plus. 
- Moi, c’est l’anglais. J’arrive pas à prononcer les « th », ça m’énerve. Et en sport, je supporte pas le prof. Il engueule ceux qui ont du mal, il les insulte, alors moi je fais exprès d’arriver la dernière, ou de mettre la balle à côté en basket, juste pour le faire enrager. Il m’a collé des tas de punitions, des mots dans le carnet, même des heures de colle.
Kèv sourit. 
- J’aime pas les profs qui se croient les plus forts, dit Marion. 
Elle équeute trois haricots, s’arrête, regarde dans le vide. 
- Je crois que ma grand-mère va mourir.
Elle dit ça sobrement, surprise, incrédule. Kèv relève la tête. Leurs yeux se croisent. Pas longtemps.



Leçon 4
« Lorsque naît la poésie, on ne sait pas toujours ce qu’on dit. » (Raymond Queneau)