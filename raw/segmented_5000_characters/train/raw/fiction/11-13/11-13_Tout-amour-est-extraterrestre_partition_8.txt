L’édifice en question ressemble à un vieux palace de la Mitteleuropa, entouré d’un parc bien entretenu qu’encadre une épaisse forêt peuplée d’ombres menaçantes. L’intérieur tient les promesses de l’extérieur, salons somptueux, couloirs luxueux, parquets jonchés de tapis d’orient, meubles de style, grands vases garnis de bouquets de fleurs fraîches. À la réception, on me tend la clef de la chambre 37, troisième étage, et on me confisque mon téléphone mobile, le tout avec une courtoisie confondante. 
— Si vous voulez un petit déjeuner, dépêchez-vous, le service va finir.
Mes « amis » ne m’accompagnent pas. J’ai une étrange impression de liberté. Bizarre colonie de vacances !
La première chose que me frappe c’est l’ambiance résolument masculine qui caractérise ma chambre. C’est plutôt une suite, du reste, avec petit salon et chambre à coucher. Une baignoire jacuzzi en plein milieu de la salle de bains n’exclut pas la présence latérale d’une douche ultramoderne, tout ce qu’il y a de plus “high tech”. Il y a même un vélo d’appartement dans un coin à l’intention des forcenés de la mise en forme athlétique. Je regrette de n’être pas mieux habillé, de ne pas être né lord anglais ou aristocrate hongrois, plutôt que simple extraterrestre. Un panier de fruits trône sur le bureau. Une télé à écran plat couvre tout un pan de mur, mais, par contre, ni téléphone, ni ordinateur. Le tout dans un décor à la fois sobre et riche. Reposant, après le rose de ma chambre depuis toujours !
J’essaie le lit : un concentré de barbe-à-papa équipé d’au moins huit oreillers. J’aurais bien fait une petite station dans les draps, mais pour l’instant c’est mon estomac qui commande : je sors, ferme soigneusement la porte et descends à la salle à manger. 
Je n’ai vu qu’une fois dans ma jeune vie un pareil buffet, lors d’un week-end à Porto Rico payé par l’entreprise de ma mère. Ici – à je ne sais où – la variété, l’abondance et la qualité de ce qui est proposé à mes appétits gourmands me font regretter de ne pas avoir quatre ou cinq estomacs au lieu d’un. Un des chefs présents n’est là que pour confectionner l’omelette de vos rêves. J’élève une montagne de saumon fumé sur mon assiette. J’adore ça : chez nous, c’est une fois par an maximum. Je goûte le caviar. Je goûte un peu de tout. Heureusement, je n’aime que le salé, parce que la table des desserts achèverait un escadron de diabétiques.
Je suis tout seul à me faire servir, ce qui s’explique peut-être par mon arrivée tardive. Autour de moi et pendant que je m’empiffre, on dresse déjà les tables du déjeuner.
Tout ce que je suis en train de vivre, punition ou récompense ? Suis-je entre les mains d’humains ou d’extraterrestres ? Il n’y a personne pour me l’expliquer. Je me régale et puis je fais un tour. La piscine, le sauna, la salle de cinéma, la boîte de nuit. Suis-je mort et au paradis ? Mon premier regret est de ne pas avoir de maillot de bain. Mon deuxième regret est de ne pas avoir pu amener ici mes triples mères, ainsi qu’Oliver et Karen.
Je vois un chariot avec des maillots et des serviettes, comme si on m’avait deviné. Je pioche pour trouver ma taille et passe dans les vestiaires. Il y a plusieurs garçons comme moi. Je dis bonjour. Avant même toute entrée en conversation, je sens un lien très fort qui m’unit à ces jeunes. On échange timidement des prénoms.
— Tu viens d’arriver ? me demande un certain Don.
— Oui, et toi ?
— Hier.
Nous sommes tous arrivés plus ou moins en même temps.
— Où sommes-nous ?
— Aucune idée. Quelque part en Pennsylvanie. Ou dans l’Ohio.
— Et qu’est-ce qu’on y fait ?
— J’imagine qu’on va nous torturer, avant de nous assassiner.
— Ah bon.
— Après nous avoir gavés. C’est le point fort du programme.
— Tu as une idée de pourquoi tout ça ?
— Essaie de réfléchir : nous ne sommes pas n’importe qui.
— Parce que… vous tous aussi… comme moi ?
— Eh, oui.
— Tu t’appelais comment ?
— Donna. Et toi ?
— Pauline.
Dans la piscine, on se raconte nos histoires. Pour la première fois de ma vie (mis à part avec Oliver), je me sens en état de camaraderie très forte avec des garçons. 
— Il n’y a que des mecs, ici ?
— De récents mecs ! À peine nés, presque morts !
— Ça veut dire que le changement de sexe n’est jamais dans l’autre sens : garçon qui mute en fille.
— Pas que je sache.
— Et il n’y a que des serveurs, ici, pas une seule serveuse ?
— J’en ai vu une, pas mal. Mais c’est la seule.
— Et combien somme-nous, nous-autres ?
— On était une cinquantaine ce matin.
— Et personne ne nous donne d’explications ?
— Écoute, tu as vu les chambres ? Les repas ? Profitons du moment. On ne sait pas ce qui va nous tomber dessus.
Nous avons discuté encore quelques minutes, puis je suis remonté dans ma chambre.
Le lit m’appelait, me commandait de m’étaler sur toute sa surface. C’est bon, trop bon, je n’ai jamais rien senti de pareil. Même les draps, mi-soie, mi-lin, sont une sensation nouvelle. Ma famille n’est pas pauvre : nous habitons une maison confortable, nous mangeons à notre faim, mes mères sont fières de leur indépendance et d’avoir toujours gagné leur vie. Mais nous n’avons pas de draps comme ça, des draps qui ont dû inspirer l’expression « dormir dans de beaux draps ».
Sauf que je n’arrive pas à dormir, ce qui prouve que le meilleur lit ne garantit pas le sommeil : une Porsche n’est pas à l’abri des embouteillages. Je tourne et me retourne en essayant de réfléchir à ma situation. Finalement je suis si mal dans ce lit exquis que je me propulse au-dehors et fais un tour dans le jardin. Je marche jusqu'à la lisière de la forêt avec l’idée d’aller me perdre parmi ses arbres centenaires. Quel n’est pas mon étonnement de tomber sur une clôture électrique, qui décourage toute tentative de la franchir. 
C’est donc ça, nous sommes dans une prison… dorée.
Je retourne à ma chambre. J’allume la télé. Ça ne m’intéresse pas. Je n’ai jamais aimé. Mes deux grands-mères sont téléphages. Elles sont systématiquement scotchées à toutes les séries débiles. Elles ont bien essayé de m’entraîner dans leur passion pour les histoires d’amour mélo-mélasses, mais ont raté leur coup.
Par contre, j’aime la lecture. J’attrape un livre et y lis dix fois la même phrase sans savoir ce qui y est écrit. Je ferme les yeux mais le sommeil me fuit toujours. Je les rouvre lorsque j’entends qu’on glisse une lettre sous ma porte. 
J’ouvre l’enveloppe :
« Monsieur Simmons, 
Le docteur, Mme Rosewater, vous convoque à 17 heures pour vous examiner dans son cabinet au rez-de-chaussée, à côté de la salle à manger. Soyez ponctuel. »
Il est exactement 16 h 45. Je me coiffe, je m’asperge le visage d’un peu d’eau et je descends l’escalier.
Je frappe à la porte de la doctoresse en imaginant tout ce qu’elle va pouvoir me faire subir – piqûre d’arsenic, injection létales, vaccin mortel, absorption de pilules nocives, électrochocs…
J’aurai eu au moins une enfance heureuse.