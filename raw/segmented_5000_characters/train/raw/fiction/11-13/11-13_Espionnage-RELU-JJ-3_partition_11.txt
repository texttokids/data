Eh bien, tout nouveau jour est neuf et inhabituel. 
Qu’on pense à Marcel Proust. Il est resté au lit une grande partie de sa vie et il a écrit un chef d’œuvre. Il dit : « Le véritable voyage de découverte ne consiste pas à chercher de nouveaux paysages, mais à avoir de nouveaux yeux. » Faux, pense Angélique. Le vrai voyage de découverte c’est ARTHUR. Mais sa journée est obstinément sans Arthur. Elle rentre chez elle la tête vide et le cœur vide, dégoûtée. Personne à la maison, alors elle se console dans le garde-manger : il contient toujours des gâteaux pour Pablo, et des céréales, et des chips, et des sticks au sel. Elle mange des quantités de tout ce dont elle a besoin pour se tenir compagnie, mais voici Pablo qui arrive pour manger avec elle. Ou plutôt il va en avoir moins pour elle car Pablo mange beaucoup. Elle pousse les boîtes de gâteaux vers lui parce qu’elle aime penser qu’elle partage mais elle sait qu’il y a des choses qu’elle fait à contre cœur. Ils s’assoient en silence comme un vieux couple qui s’ennuie au bistrot devant deux verres d’absinthe. Angélique a du mal avec le silence alors elle essaie de le briser.
- Comment s’est passé ta journée ? 
- Bien, très bien. 
C’est sa réponse standard. Il faut lui arracher un à un les détails.
- Avec qui ?
- Jonathan, Arianna, Kévin, Madame Lambert, Jennifer, Emma, Mohammed, Orr.
- Il y a quelqu’un qui s’appelle Orr ?
- Oui, une fille, et une autre qui s’appelle Océane. 
- Bizarre.
- On s’habitue. 
Angélique ne peut s’empêcher de trouver cette petite peste de frère étrange. Il est incongru, un OVNI. Il peut être de ces petits frères odieux qui piquent les jouets et fichent la pagaille. Il veut toujours que l’on amène où que les grands vont. « Je peux venir avec vous ? » est son refrain préféré. Mais il est là depuis cinq ans et il fallait apprendre à faire avec.
Charles fait une entrée tourbillonnante dans la cuisine. Un Fred Astaire sans la tête chevaline. On n’avait pas senti une telle ébullition en lui depuis son fiasco au permis de conduire. Aurait-il eu une bonne note ? Il s’assoit avec eux et s’octroie à son tour quelques milliers de calories.
- Qu’est-ce qui t’arrive ? demande Angélique.
Après une hésitation, il se lance : 
- L’amour, ma chère. 
Rien n’est plus surprenant que cette réponse, venant d’un garçon de tempérament aussi sobre. « Pourquoi lui ? pense Angélique. Pourquoi pas moi ? »
- Love is all you need, chante Pablo.
- Qu’est-ce que tu en sais ? Tu es amoureux, toi aussi ?
- Oui, affirme Pablo. J’ai toujours été amoureux.
- De qui ? 
- De toi.
À nouveau, il la surprend. L’amour est sensé être réciproque !
Le klaxon de la voiture maternelle perce le mur de son. Ce signal veut dire que Marie a fait des courses et qu’elle veut du renfort pour décharger les achats. Charles exécute une sorte de cha-cha-cha jusqu’à la voiture, Pablo à sa suite, mais Angélique ne bouge pas, tout à ses suppositions quant à l’amoureuse de Charles. Comment pourrait-elle atteindre un tel stade avec Arthur ? 
- Tu es infirme, ou quoi ? 
Telle est l’aimable interpellation de sa mère. « BTF » est la formule diplomatique habituelle de Mme Blanc pour signifier « bouge tes fesses », expression trop rude pour l’employer carrément. Angélique n’est pas particulièrement bien lunée envers sa mère, alors elle se lève au ralenti et sort chercher quelques provisions qu’elle range du bout des doigts, sans un mot. Au moins, il y a un plat chaud qui sent bon.
- C’est quoi ? demande Pablo.
- Une daube à la provençale.
- Beurk !
Leur mère n’achète jamais de plats cuisinés. Et elle n’arrive jamais à cuisiner un plat.
- Excellent achat, commente Charles.
- Et puis ça ne doit pas coûter bien cher, maugrée Angélique.
- J’avais envie d’essayer quelque chose de nouveau. Il me faudrait des heures pour préparer un truc comme ça.
- Il y a vingt-quatre heures dans une journée, remarque Pablo
- Et on les passerait à faire la cuisine ? 
« On les passerait à rêver d’Arthur », pense Angélique.
- Vingt-quatre heures pour aimer, chantonne Charles.
Angélique est prête à consacrer au moins une des vingt-quatre heures de la journée à déverser son cœur dans son journal intime. Voici le résultat :