Il pleut quand ils sortent du brunch en allant vers le cinéma. C’est Marguerite qui donne l’impulsion à la suite des opérations. Elle y va à brûle pourpoint : « Si on prenait une petite chambre d’hôtel … »
Elle n’avait jamais dit un truc pareil de toute sa vie et elle n’en croit pas ses oreilles.
« Sans bagage ? » demande Charles, époustouflé.
« Nous avons bien assez de bagage ! Une vie de bagage ! »
Sans dire oui ou non, Charles entre au premier hôtel trois étoiles venu et demande une chambre. Lui non plus ne croit pas à ce que cette journée a apporté.
« Nous avons maintenant la chambre. Qu’est-ce que nous pouvons bien y faire ? »
Marguerite fait le tour, soupèse les produits dans la salle de bain, essaie le lit.  « La sieste ? »
« Et qu’est-ce que je vais dire à Samuel quand il me demande ce que nous avons vu et comment était ce film ? »
« Que nous avons vu un film coréen dont tu ne te souviens pas du titre qui parle d’un homme et d’une femme, plus très jeune, la soixantaine bien entamée, qui n’ont pas le temps de faire la cour pendant trois ans, qui ont tous les deux besoin des bras, des lèvres et tout ce qui advient de l’affection de base. Alors ils se plongent au lit comme la seule aventure qui reste encore. Un film presque porno que tu ne lui recommandes pas, mais beau. Un film sur la générosité de deux êtres qui sont prêts encore à donner ce qu’ils ont au fond d’eux-mêmes pour tenter un dernier amour. »
« Et le reste est classé X, » dit Charles en enlevant sa cravate.
Quand ils se réveillent de leur « sieste », Charles propose qu’ils gardent la chambre après un dîner dans un bon restaurant.
Marguerite téléphone à sa fille pour dire qu’elle ne rentre pas cette nuit.
« Maman, reviens immédiatement à la maison ! »  
« Ma chérie, il arrive un jour où il faut faire confiance à ses parents. A demain. »


La corvée d’Anatole / Anatole’s task

Lire est toujours la pire des corvées pour Anatole, il supporte cette corvée à condition de lire à voix haute. La tâche est transformée en un double plaisir car il voit et entend les mots en même temps. Et lire à deux double le jeu.  Lire pour quelqu’un d’autre et soi-même, éventuellement avoir un échange sur telle phrase ou telle page, en fait, Anatole n’aime rien faire seul.
Il va faire ses devoirs chez Bilgi, une maison où un bruit constant les accompagne à travers les maths, le français ou aujourd’hui un projet pour l’histoire qu’ils vont présenter ensemble. C’est Lulu qui a cherché l’information sur l’art anti-nazi. C’est Léonard qui fera le powerpoint. Ce que les deux garçons font, les cahiers et les livres étalés sur la table de cuisine, est de parler du dernier match avec quelques interrogations sur le stage qu’ils sont censés faire en troisième.
Anatole n’est cependant pas trop occupé pour regarder Dounya, la sœur de Bilgi, en quatrième, pour attendre son départ quand elle fera deux bises, une sur chaque joue, et qu’il essayera de nouveau de faire basculer son visage pour positionner ses lèvres sur les siennes.
Et pourquoi a-t-il encore faim ? Peut-être parce qu’une assiette de gâteaux turcs faits maison attendent les deux intellectuels pour les encourager à bien travailler.


Des potins / Gossip