L'immense avantage du plastique sur le cuir, c'est que l'on peut écrire dessus. L'inestimable supériorité des couleurs sur le noir, c'est aussi qu'on peut écrire dessus. À cheval sur le carré jaune et le carré rose de la robe multicolore d'Elmer s'étalaient autrefois, en toutes lettres, les trois identités remarquables nécessaires à la résolution et au développement de la plupart des équations. J'avais trouvé inutile d'encombrer mon esprit avec ça, alors que feue ma trousse offrait une surface idéale pour ces hiéroglyphes. Pas une seconde, en mettant au point cette anti-sèche permanente, je ne m'étais douté qu'elle pourrait un jour me faire défaut. Entre ma trousse Elmer et moi, c'était à la vie à la mort, jamais je n'aurais cru devoir en changer.
J'ai caressé tristement la surface granuleuse de ma nouvelle trousse muette en espérant, tel Alladin et sa lampe merveilleuse, que le génie en jaillirait. Je me suis concentrée pour essayer de reconstituer, de mémoire, les formules que j'avais tant de fois lues et relues. Même en cours de français je les avais sous les yeux et il m'arrivait d'admirer leur quadrille, je me racontais des histoires à leur sujet : a danse avec b, mais voilà qu'entre eux se glisse ab ; c'est l'image de leur couple, qui les éloigne l'un de l'autre. Ce qui donnait : a+b=a+ab+b, sauf qu'il y avait des « puissances 2 » par ci et des « fois 2 » par-là, qu'il ne fallait surtout pas confondre. Peut-être était-ce a au carré + b au carré ? Je me rappelais aussi qu'il y avait un truc bizarre avec le –, un – qui devenait +, et inversement. J'ai visualisé le carré jaune et j'ai essayé mentalement de reconstituer les signes qu'il contenait. Une des parenthèses avait bavé, et dessinait comme une chevelure au b. Ça, je m'en rappelais très bien, mais je n'arrivais plus à retrouver comment placer les exposants.
« Késki spasse? Pourquoi tu fé rien? »
Ces deux questions étaient rédigées sur un morceau de papier que Fleur venait de faire glisser sous mes yeux ?
« J'ai perdu mon anti-sèche des identités remarquables. Je suis bloquée ».
Ai-je répondu par retour de courrier.
Presque aussitôt, la bandelette de papier est revenue sur ma table :
(a+b) etc...
Les trois formules magiques venaient de jaillir du stylo de Fleur et je les ai aussitôt reconnues, comme de vieilles copines de maternelle.
« Comment tu fais pour t'en souvenir ? » ai-je demandé à Fleur.
« Je sais pas, c'est symétrique. Tu trouves pas ? »
Justement non, ce n'était pas symétrique, mais j'ai décidé de ne pas perdre trop de temps en discussions. Il ne nous restait plus que quarante minutes. 
Pendant que je travaillais, j'entendais la respiration tranquille de Fleur qui recopiait au fur et à mesure, confiante, paisible. Ça m'avait tellement manqué cette présence confortable, facile, familière. Ma mère avait peut-être raison, quelque chose clochait chez moi.
À la fin du contrôle, Fleur a poussé un long soupir et m'a dit :
« Heureusement que tu es là. Tu peux pas savoir comme j'ai prié pour que personne ne vienne s'asseoir à côté de moi avant que tu arrives. »
« Pourtant, je ne suis pas la meilleure en maths, » lui ai-je fait remarquer. « Tu risques d'avoir entre 12 et 14. »
« Ça, je m'en fiche. Tant que je suis au-dessus de la moyenne, c'est bon. Je préfère copier sur toi parce que tu écris hyper lisiblement. Tu n'as pas une écriture d'égoïste. Tu vois ce que je veux dire ? Des petites pattes de chenille toutes biscornues ? »
Je voyais très bien ce qu'elle voulait dire et j'avais instantanément reconnu l'écriture d'Allison. Une carte postale d'Allison, c'était une journée de vacances gâchée à essayer de déchiffrer son charabia visuel. 
« Comment tu fais pour te souvenir des identités remarquables ? » lui ai-je demandé alors que nous nous dirigions vers la salle d'Espagnol.
« Je les ai apprises par cœur. »
« Moi aussi, je les ai apprises par cœur, mais elles ne tiennent pas. C'est comme du vieux scotch avec de la poussière dessus, au bout d'un moment, elles finissent par tomber. »
Fleur m'a regardée par dessous sa frange impeccable.
« Tout est tellement compliqué avec toi, » a-t-elle dit. « Y a pas besoin de toutes ces histoires de scotch. Les identités remarquables, elles tiennent parce que c'est comme ça, c'est obligé, faut les savoir par cœur, c'est tout. »
« Mais comment ça se fait que tu es moins bonne en maths que moi alors ? »
« Je connais les formules, mais je ne sais pas les appliquer. Pour moi a, c'est a et b, c'est b, si on les remplace par un chiffre ou par un x, je perds complètement les pédales. Ma psy dit que c'est une forme d'inhibition. C'est comme si quelque chose en moi refusait d'y arriver. »
« Tu vas chez une psy ? »
« Ben oui. »
« Mais pourquoi ? »
« Je ne me souviens plus exactement, mais je crois que c'est à cause de ça, justement. Les notes. Ah oui, ça y est, ça me revient, j'ai un blocage scolaire. »
« Et comment ça se fait que tu ne me l'as jamais dit. »
Fleur a haussé les épaules.