Cher cahier rose, je me suis ennuyée pendant mes cours, ce matin. Je réussis d’habitude à éviter l’ennui en écrivant des lettres à des gens imaginaires ou réels, tandis que le prof me croit en train de prendre des notes, ou simplement en rêvassant à Arthur, qui ce matin se trouvait à pas plus de deux mètres de moi. Mais mes fantasmes étaient en panne et par ailleurs je ne me sentais pas motivée pour écrire une lettre. Enfin, la cloche a sonné. J’avais une faim de loup, une faim que même la bouffe de la cafète n’a pas découragée.  Je ne regardais ni à gauche, ni à droite, juste droit devant moi en direction de la queue, quand quelqu’un m’a barré la route et m’a attrapé par les épaules en disant : « Demi-tour, j’ai apporté le pique-nique. » C’était Brenda… Comme si rien ne s’était jamais passé entre nous. Pas d’excuses, pas d’explications, pas d’allusions aux blessures saignantes ni à la brèche profonde.
Je l’ai suivie, nouillasse débile que je suis. Je l’aurais suivie jusque dans la lune, pour une demi-heure de fous rires et de potins comme au bon vieux temps. Ah, comme c’était bon jadis de dézinguer chacun de la classe, pièce par pièce, cheveu par cheveu, tache de rousseur par tache de rousseur, en se moquant des pantalons larges d’un tel ou de la stupidité de telle autre, célébrant ainsi notre supériorité physique et morale sur tout autre être humain et nous cachant astucieusement nos propres tares, tellement moins graves que celles des imbéciles qui nous entourent. 
Brenda ! Pendant des semaines, j’ai réfléchi aux mille façons de la tuer. Elle était un champ d’orties dans mon âme. Sa pensée me faisait bouillir le sang au point de m’exploser les veines. Elle m’avait brisé le cœur. Et voici qu’elle me revient comme un boomerang, sans un mot pour ma souffrance, sans une excuse pour mon martyre, et moi je ne dis rien, je mâchonne ses carottes et je bouffe ses sandwichs au thon. Elle m’a eue, profitant de ma faim et de ma stupidité.
- Qu’est-ce que tu deviens ? me demande-t-elle.
Je meurs d’envie de lui offrir quelque chose de juteux. Je suis tellement désireuse de réintégrer ses bonnes grâces. Toute ma vieille affection avait resurgi, et j’avais tant envie de passer l’éponge. Sauf que je n’avais rien à lui raconter. 
- À quel point de vue ?
- Pablo, ta tante Mia, Charles… Il paraît que tu es accro à un dénommé Arthur…
- Bof ! c’est juste qu’il est… l’idéal.
- N’importe quoi. Un crétin.
- Tu n’avais pas l’air de le trouver si crétin, l’autre jour, collée à lui…
- C’est toi qui étais crétine, sans ton soutien-gorge.
- J’essaie de nouvelles sensations.
- Alors essaie celle-là. 
Elle a sorti un paquet de Gauloises et en a allumé une. Je n’ai pas pu m’empêcher d’admirer ses airs de star quand elle soufflait sa fumée. 
- Quand as-tu commencé ?
- Juste après mon anniversaire.
- Tu es en route pour les rides précoces et la mort prématurée. 
Je lui ai servi quelques-unes des mises en garde de tante Mia. Elles ne l’ont pas convaincue :
- Quoi qu’on fasse, on va mourir. Alors autant profiter de la vie. Tiens, vas-y.
- Jamais !
- Je t’assure que c’est super. Et ce n’est pas une malheureuse cigarette qui va te donner des rides et te tuer.
- Ça ne me tente pas du tout. Je ne te comprends pas.
- Ça me détend.
- La mort aussi, ça détend.
Elle a fouillé dans son sac et en a sorti un petit paquet. « Tiens, je t’ai acheté un truc. »
Sans doute pour se faire pardonner, une offrande de paix, une manière de remplacer les mots qu’elle ne savait pas dire. J’ai pris la boîte et j’ai défait l’emballage. C’était un cadre en forme de cœur, avec dedans une photo de nous deux, nos deux têtes souriantes. Au dos, une inscription : « Je t’aime Angie ! » J’étais émue.
- Merci.
Brenda a allumé une autre cigarette à la sienne. « Tiens, a-t-elle dit en me l’enfonçant entre les lèvres. C’est pas drôle de fumer seule. Et puis, tu dois t’ouvrir à la vie et aux expériences. Ça va te dégourdir. Tu avais l’air d’une morte, en classe, ce matin… »
Je ne voyais pas bien ce que je pouvais faire d’autre. J’ai inhalé et oui, c’était bon. Pas le moindre de ces toussotements qu’on voit dans les films montrant des gosses allumant leur première cigarette en cachette. Je dois être programmée génétiquement pour fumer. C’était bon d’être là, à cloper avec mon amie retrouvée, et qui m'a tant manqué.  Une revenante, avec qui partager à nouveau les vieux souvenirs et les nouvelles aventures. J’adore l’idée d’être fidèle. Je déteste perdre quelqu’un.
Je ne serai jamais aussi méthodique et parfaite que ma mère, mais au moins je serai peut-être aussi bonne fumeuse qu’elle.
Youpi !



6. Dimanche pourri