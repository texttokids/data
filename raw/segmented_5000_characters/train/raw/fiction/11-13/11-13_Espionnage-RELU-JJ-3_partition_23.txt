« Cher cahier rose, Alice et Pablo faisaient trop de bruit et les devoirs n’ont pas avancé. Je ne suis pas une prima donna des devoirs et j’ai toujours pu travailler avec du bruit, mais là je suppose que je n’en ai pas eu envie. Et puis Brenda est venue se faire expliquer un problème de maths par Charles et j’avais intérêt à m’absenter.
Arthur a téléphoné et j’ai dit à Pablo de dire que je n’étais pas là.
Papa est sorti après le deuxième couscous en deux jours. Moi, je pourrais manger du couscous tous les jours. J’ai rangé la cuisine avec mes frères. Pour ça, nous avons été bien élevés. J’ai esquivé la corvée de lire des livres à Pablo et me suis moi-même mise en route. 
J’ai déambulé dans la vieille ville, j’ai senti différents savons et j’ai mangé une glace chez Fennocchio. Je pensais en ramener pour la famille mais je n’avais pas assez d’argent sur moi. J’y ai pensé quand même.
Je mangeais lentement ma glace en remontant. Les Galeries étaient ouvertes et je me suis dit que nous nous américanisons, avec ces ouvertures des magasins le dimanche. Mais bof, un peu plus, un peu moins…
Cela m’arrangeait d’ailleurs, non que je veuille acheter quoi que ce soit mais juste pour toucher de belles robes et chercher une idée de cadeau de mariage. Un grand magasin est un musée, en quelque sorte. 
Je me suis trouvée au rayon des sous-vêtements. Dans un bac à soldes, j’ai cherché ma taille. C’est galère, les soutiens-gorge. Il s’est fait que le soutien-gorge de mes rêves n’avait pas d’antivol. Un bout de tissu de rien du tout peut atteindre des prix astronomiques, même en solde. Je suis allée avec deux ou trois autres dans la cabine d’essayage et j’ai simplement fourré le bon modèle dans mon pantalon.
Je n’ai pas couru vers la sortie, cool Raoul ! J’ai flâné dans le magasin un bon moment. 
Le crime parfait. Personne ne m’a suivie dans la rue. Et j’étais l’heureuse propriétaire de ce petit morceau de dentelle que j’avais hâte de mettre. Dommage que je n’aie pas pris le slip assorti. C’est tellement facile.
Je n’ai pas suivi la rue principale, j’ai pris par la rue Blacas pour remonter jusqu’à la maison. À mi-chemin, j’ai vu un couple sortir d’un immeuble cossu, un couple enlacé, bouche contre bouche, et je me suis cachée dans le renfoncement d’une porte cochère. Il ne fallait surtout pas qu’ils me voient, car je connais l’homme, et lui aussi me connaît bien.
C’est mon père. 



12.  L’accident