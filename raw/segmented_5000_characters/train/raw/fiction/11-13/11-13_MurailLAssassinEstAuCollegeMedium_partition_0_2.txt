- Police ! Que personne ne sorte !
	On rallume. L’inspecteur interroge tout le monde et il doit deviner qui est le coupable. C’est un jeu qui fait très peur parce qu’on attend dans le noir. On entend un grincement, une respiration. Quelqu’un vous frôle et puis c’est le coup fatal. J’ai été tuée trois fois. J’ai cru vraiment que j’allais mourir, tellement mon cœur battait fort. En partant, j’ai remercié madame Maréchal. Je lui ai dit :
	- J’adore jouer à l’assassin.
	Ma copine a ri et elle m’a dit :
	- Tu te fais toujours assassiner.
- Étrange, marmonnai-je en reposant le devoir.
Elle avait eu 20/20 en chiffres de sang. Qui était Claire Delmas ? Quand je laissai aller ma tête sur l’oreiller, ce soir-là, une phrase me vrilla le cerveau : «Tu te fais toujours assassiner » et je ne pus l’en déloger.
***
Un courant d’air glacial traverse de part en part le plateau de Queutilly comme un coup de poignard.
Par la fenêtre de Saint-Prix, on aperçoit la Doué qui branches nues des arbres.
- L’hiver sera rigoureux, commenta monsieur Agnelle dans mon dos.
Je me retournai.
- Je ne vous ai pas trop fait attendre, monsieur Hazard ? Asseyez-vous… Vous admiriez la vue ?
Une fois derrière son bureau, le directeur de Saint-Prix me dévisagea intensément.
- Je n’ai pas très bien compris, me dit-il. Vous êtes de la police ou vous êtes de la faculté ?
- Mettons que je suis comme vous.
- C’est-à-dire ?
- Un privé.
Une sorte de sourire tordit la bouche du directeur. Les reliefs de son visage étaient si tourmentés qu’on s’attendait presque à voir les os se déplacer, sous la poussée d’obscures forces tectoniques.
- Bien sûr, reprit-il, personne n’est au courant.
Pour tout le monde, vous êtes le remplaçant de monsieur Copa, notre malheureux professeur d’histoire.
Au fait, vous pourrez assurer ses cours ?
J’ignorais jusqu’au contenu des programmes.
- Tout à fait, dis-je.
- La bonté, monsieur Hazard, n’est pas le trait dominant de la jeunesse. Qu’ils viennent ou non de familles aisées, la plupart des jeunes de SaintPrix manquent de repères et de valeurs. Les derniers événements m’ont peiné, mais pas vraiment surpris.
Il faut s’attendre à cela et à pire.
Il me regarda pour savourer l’effet de son préambule. Je lui fis un petit signe de tête encourageant.
Fonce, Alphonse, tu m’intéresses.
- Je ne me serais pas résigné à faire appel à vos services, poursuivit-il, si cette… ordure n’avait pas été glissée sous ma porte.
À bout de bras, il me tendit un papier sur lequel on avait écrit en lettres capitales : 
	AGNELLE, TU T’EN FOUS PLEIN LES FOUILLES.
	TES JOURS SONT COMPTÉS.
	PRENDS GARDE À TA COMPTA.
- C’est anonyme, reprit Agnelle. Mais d’une certaine manière, c’est signé. C’est le « style » des troisièmes 1.
Il froissa le papier dans son poing. région ne tolèrent plus. Vous imaginez de quel rebut il s’agit.
Il en parlait en faisant une lippe dégoûtée.
- Les parents nous demandent de conduire ces jeunes jusqu’au brevet…
Il ricana :
- Jusqu’au brevet ! Combien d’entre eux sont réellement récupérables ? Si sur vingt élèves, nous en sauvons dix, j’estime que nous aurons fait notre devoir. Pour les dix autres… il faut bien que le diable ait sa part. N’est-ce pas ?
- Fifty fifty, dis-je tranquillement.
Un brouhaha se fit alors entendre dans le couloir.
- Monsieur le directeur ! Monsieur le directeur !
La porte du bureau s’ouvrit. C’était Lucien, le concierge un peu simplet, suivi d’un surveillant.
- Un accident ! s’écria Lucien.
Nous nous levâmes d’un même mouvement.
- Elle a sauté par la fenêtre, bredouillait le concierge. C’est une chance. Elle s’est pas tuée.
Dans la cour s’était formé un attroupement que le directeur fendit brutalement, en attrapant les gosses au collet et en les rejetant de côté. une élève à se remettre sur pied. Celle-ci poussa un cri de douleur.
- C’est la cheville, dit monsieur Rémy. Entorse ou fracture. Je vais la porter à l’infirmerie.
Il souleva sans effort la petite fille, une blonde aux cils presque blancs.
- Mais enfin, va-t-on m’expliquer… commença Agnelle, d’un ton rageur.
Il ressortit des explications confuses du concierge que l’enfant avait délibérément sauté dans la cour depuis le vasistas des toilettes, au premier étage.
- Qu’est-ce que je vais raconter aux parents ? marmonna le directeur.
Il se souvint de ma présence et ajouta :
- Excusez-moi, monsieur Hazard, je dois aller téléphoner. Au fait, cette petite sera de vos élèves.
Claire Delmas, 6e 2.
Il me laissa au milieu de la cour, pris dans un tourbillon de pensées. Claire Delmas, 20/20, « on va jouer à l’assassin ».
- L’infirmerie, s’il vous plaît ?
Sur les indications toujours embrouillées du concierge, je montai l’escalier de marbre jusqu’à
- Pourquoi tu as fait ça ? disait une petite voix flûtée.
- Parce qu’il était derrière moi, j’en suis sûre, répondit une voix plus rauque.
- Mais quand même, t’es folle de sauter par la fenêtre !
- J’ai pas réfléchi. J’avais trop… Il y a quelqu’un.