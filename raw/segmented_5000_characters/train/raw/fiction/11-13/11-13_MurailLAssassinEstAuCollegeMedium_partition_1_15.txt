Ainsi la ville de Queutilly et les parents d’élèves horrifiés apprirent-ils ce jour-là que leurs enfants avaient été confiés pendant deux ans à un… maniaque du crime.
- Berthier m’a confirmé qu’avant d’être à SaintPrix, me rapporta Catherine, Agnelle était effectivement dans un collège de la Manche où les mêmes événements se sont produits.
- Que voulez-vous dire ? m’étonnai-je.
- Mais oui. Le coup des copies ! Il l’avait déjà fait.
Des copies d’élèves avaient été subtilisées et notées 0/20. À l’encre rouge, cette fois.
Je hochai la tête.
- Bien joué, marmonnai-je.
Le lendemain midi, l’inspecteur Berthier demanda aux professeurs et aux élèves de 3e 1 de se rassembler dans le réfectoire. On convia aussi Lucien Renard et le surveillant Nicolas Arvet-Dumillon.
- Mesdames, messieurs, commença l’inspecteur, il me revient la pénible obligation de vous apprendre…
L’horreur, l’indignation, la stupéfaction, la révolte, la douleur se lurent sur tous les visages. Un à un, je les examinai. L’un de ces visages mentait.
- Nous aurions dû éviter ce drame, Nils.
Catherine me regardait, désolée et un peu irritée aussi.
- C’était évident que le directeur était fou. Vous ne vouliez pas en convenir. Il était le seul à pouvoir se promener librement à travers le collège. Forcer un casier, scier un barreau, limer un crochet, écrire sur les murs, tout était simple pour lui. La nuit, il était le maître des lieux.
- Le passage par le soupirail ? murmurai-je.
- Ce n’est pas le directeur qui l’empruntait, d’accord. Mais Jules Sampan n’était pas le seul gamin capable de trouver le moyen de fuguer. Interrogez Boussicot, Alcatraz ou Marie Baston !
- Pourquoi le rap d’Axel a-t-il disparu ? murmurai-je à nouveau.
- De quel pré Jules a-t-il voulu me parler ? murmurai-je encore.
Je pris une feuille de papier sur laquelle j’écrivis les dernières paroles de Sampan : « PRÉ AU PRÉ BALL MÉDECINE ».
- « Ball » c’est sans doute la batte de base-ball qui l’a frappé, commenta Catherine, « médecine », c’était plutôt « médecin ». Vous avez mal compris. Il réclamait un médecin.
Je restai un moment dubitatif et mécontent.
- Pourquoi vous cassez-vous la tête ? me reprocha Catherine. C’est trop tard, maintenant. Jules Sampan a payé d’avoir su la vérité avant nous.
- Comment l’a-t-il sue, à votre avis ?
- Mais la nuit où vous êtes allé le rechercher au bord de la Doué, vous l’avez bien laissé au pied du deuxième escalier ?
- Oui. Et alors ?
- Et alors… il a vu Agnelle errant au second étage avec ses yeux de fou. Puis, quand il vous a téléphoné, Agnelle était dans la salle des professeurs et il a tout entendu. Jules avait signé son arrêt de mort. romans policiers. C’est tout à fait le style.
Je baissai les yeux sur mon papier et dans un éclair, je lus :
- Catherine ! Préau ! Préau… Jules parlait du préau et de « médecine-ball ».
- Ces ballons de plusieurs kilos qui servent à la musculation ?
J’acquiesçai.
- Il y en a dans le préau, ajouta Catherine. Ils sont rangés dans un placard.
- Il faut ouvrir ce placard.
Lorsque je me rendis au collège, le vendredi, Saint-Prix avait déjà retrouvé un visage paisible. Les parents avaient d’abord cédé à la panique et téléphoné en nombre pour réclamer leurs enfants. Les professeurs s’étaient efforcés de les calmer. Le criminel étant sous les verrous, le danger était passé. Faure assurait une sorte d’intérim à la direction. Catherine, estimant son rôle terminé, s’était trouvé un successeur aux fourneaux. Lucien était dans sa loge, fidèle au poste, presque à jeun.
- Heu ? La clef ? bredouilla-t-il. La clef du ?…
- Elle est sur le tableau, monsieur Hazard. Tenez !
Je m’éloignai vers le préau. Qu’est-ce que j’espérais trouver ? Un cadavre découpé en morceaux ?
Je tournai la clef dans la serrure et j’ouvris un des panneaux. Aussitôt, un ballon, deux ballons roulèrent des étagères où ils étaient en équilibre. J’en attrapai un qui allait m’écraser le pied et fis un bond pour éviter l’autre. C’étaient des médecine-balls de cinq kilos. J’inspectai les autres étagères. Elles supportaient des haltères de différents poids. Pensivement, je remis les médecine-balls en place, c’està-dire en équilibre instable. Qu’est-ce que Sampan avait voulu me dire ?
- Les haltères, me répondit Catherine. Jules vous a indiqué l’arme du crime.
- Mais pourquoi parler de médecine-ball dans ce cas ?
Catherine haussa les épaules. La question lui paraissait sans intérêt. Puisqu’on avait un coupable et une victime, le roman policier était terminé.
- Pourquoi donnez-vous encore des cours dans ce collège ? s’étonna Catherine.
Je lui répondis par une autre question :
- Mais c’est un fou ! Les fous ont des raisons qui échappent à la raison.
- Pourquoi Agnelle a-t-il manipulé sous mon nez mon coupe-papier s’il l’avait volé dans ma poche ?
- Mais puisque…
- Oui, oui, je sais ! m’exclamai-je. C’est un fou.
Il n’y a que dans les mauvais romans policiers que l’assassin est un fou.