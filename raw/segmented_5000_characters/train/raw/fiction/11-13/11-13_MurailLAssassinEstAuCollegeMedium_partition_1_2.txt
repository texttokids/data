- Écoute, dis-je à Antoine, tu me fous la paix
- Du chantage ? grinça Boussicot.
- Un pacte.
Toute la laideur du monde parut un instant se réfugier dans ce maigre visage aux yeux bordés de sang.
- Bon, vas-y avec ta servante, laissa tomber Boussicot, j’espère que c’est une histoire cochonne.
Les élèves ouvraient des yeux stupéfaits et un peu craintifs. Aucun professeur, même celui qu’ils avaient acculé à la dépression, n’avait encore subi pareille avanie. Leur saisissement était tel qu’ils me laissèrent raconter mon histoire sans trop me chahuter.
- Super, conclut Boussicot. Et celle du Petit Chaperon rouge, tu connais ?
Je sautai de mon perchoir :
- Et mon poing dans la gueule, tu veux connaître ?
Une voix nous avertit :
- 22, la Zagulon.
Boussicot me tendit son livre de géographie :
- Fais le cours. Elle vient te fliquer.
J’ouvris le livre à la page 47 et j’entrepris le commentaire d’une carte. Toc, toc, toc.
- Oui, entrez. craie verte à me prêter ?
- Heu… Oui, voilà…
Ses yeux gourmands me parcoururent des pieds à la tête.
- Ça va ? me glissa-t-elle.
- À merveille.
- Si ça n’allait pas, je suis à côté. Salle 106.
Elle referma la porte.
J’allai reposer le livre sur la table de Boussicot.
Il me dévisageait, les bras croisés, renversé sur sa chaise.
- Mais pourquoi vous ne faites pas le cours ? me demanda-t-il.
- Je n’aime pas la géographie, répondis-je, l’air désolé.
Un frisson de rires parcourut la salle de classe.
- On est les élèves les plus crasses de la Création, me dit Boussicot avec satisfaction. On nous colle le prof le plus nul de la Terre. Normal.
J’acquiesçai. Inutile de lui avouer que j’étais bardé de diplômes et étruscologue distingué.
- Alors, petit prof, reprit avec autorité le Seigneur le couloir. Ce qui se passe ici, ça regarde personne.
Et toi, Alcatraz, travaille !
Juan, docilement, déboucha son stylo-plume et se remit au boulot.
- Pourquoi l’appelez-vous «Alcatraz » ? demandai-je. C’est le nom d’un pénitencier…
- C’est parce qu’on l’a condamné aux travaux forcés, ricana Boussicot. Il fait le travail de tout le monde, les devoirs de maths et les dissertes.
Mes yeux s’agrandirent de stupéfaction :
- Les profs ne se doutent de rien ?
- Non. Alcatraz ne fait pas deux devoirs pareils et il change d’écriture à volonté.
- Ça, c’est fort, murmurai-je, épaté.
Juan prit un air modeste. Il se prit une calotte sur la tête :
- Travaille.
Axel, le guitariste, me fournit aimablement quelques explications supplémentaires tandis que les autres sortaient Walkman et jeux de tarot.
- Pour les interros, Alcatraz nous prépare des antisèches et Marie Baston nous fait des photocopies miniaturisées des bouquins au travail de son père.
Soudain, la tristesse s’empara de moi :
- Mais qu’est-ce que vous espérez avec ce système ?
- Ça fait un moment qu’on n’espère plus rien, me répondit Axel comme si la chose allait de soi.
Il ouvrit son cahier à spirale et, devant moi, écrivit : 
	Tête à rap, cœur cloqué, 
	La vie d’jà t’a débarqué.
	Tu clopes, tu taxes, tu frimes.
	T’as tout perdu, sauf la rime.
- C’est de toi ? murmurai-je, lentement gagné par le désespoir.
- On veut faire un groupe de rap avec mes potes, me dit Axel. Et on se tirera de ce bahut de merde, de cette vie de merde, et on ira claquer leur pognon de merde sous le soleil des Bermudes.
- Un soleil de merde ? suggérai-je.
- Probable.
Depuis combien de temps étais-je à Saint-Prix ?
Vingt-quatre heures ou six mois ? Avais-je été autrefois professeur de faculté, avais-je dans une autre existence écrit des livres, donné des conférences ?
- C’est pas par là le réfectoire des profs, m’avertit Axel auquel j’avais emboîté le pas, à la fin du cours.
- Demi-tour, droite ! me lança Boussicot.
En face de moi, les mains dans les poches, tout de hargne et de hâblerie, se tenait le Seigneur de SaintPrix, un fauve de dix-sept ans se battant les flancs derrière les barreaux de sa prison. « Le diable aura sa part », m’avait dit Agnelle. Et il n’aura pas trop à se fatiguer, pensai-je en m’éloignant, il lui suffira d’attendre à la sortie… Qu’est-ce que martelait Axel, le rappeur ?
	Le sort en est jeté 
	Et tu seras éjecté 
	Des chaises musicales 
	D’la sélection sociale.
- Monsieur Hazard ! me héla Zagulon, la nymphe de ces lieux. Je vous ai gardé une place.
Les professeurs étaient déjà assis. Alban, le prof de gym, me fit un signe de tête amical. Le réfectoire était carrelé de dalles sonores sur lesquelles grinçaient les chaises métalliques des convives. Sur les tables de Formica étaient disposés de grands plats d’aluminium. 
- Carottes râpées, olives noires, m'annonça un petit homme au teint brique, à la brosse de cheveux gris fer. Et vous verrez, demain, ce sera céleri rémoulade, olives vertes !
Il me tendit la main par-dessus le plat :
- Monsieur Faure. How do you do ?
- C’est notre boute-en-train, m’expliqua madame Zagulon, pour le cas où je n’aurais pas tout de suite repéré que Faure tenait le rôle du rigolo.