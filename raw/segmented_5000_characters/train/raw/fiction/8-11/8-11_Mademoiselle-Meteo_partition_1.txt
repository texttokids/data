C’est peut-être parce qu’on l’a nommé Alizée et son nom de famille est Tramontane, mais le fait est que chaque fois qu’un petit vent fait trembler une branche, son cœur se balance avec la brise. Peut-être aussi que la pluie qui bat contre sa vitre est sa plus douce musique. Rien ne l’excite plus qu’un bel orage, un déluge, un raz-de-marée… de loin bien sûr ! Où qu’elle soit, Alizée écoute le bulletin de météo chaque heure et la météo marine au moins une fois par jour, et ça depuis qu’elle avait sept ans. Elle ne l’explique pas autrement que cela : la météo est sa passion.
Elle vient d’avoir 30 ans, célibataire, et, depuis six ans, elle navigue entre le C.P. et le C.M.2, capitaine d’une classe de 25 à 33 élèves, à essayer de faire d’eux des gens civilisés, c’est-à-dire des êtres qui savent lire et écrire… et regarder le ciel !
Mais quelles que soient ses espérances et ses ambitions pour les enfants qui entrent dans son navire, la journée d’école est soumise à un rite incontournable : la météo du jour.
Alizée avait enregistré et montré aux élèves un florilège de météorologues à la télé. « Tous mauvais ! » dit-elle. « Je voudrais que vous fassiez mieux, que vous trouviez une façon originale, dynamique et amusante de nous dire quel temps il fait aujourd’hui. »
- Mais, maîtresse, dit Pierre, il fait toujours beau ici, ce n’est pas intéressant.
- Tu as raison, Pierre. C’est pour ça que vous n’allez pas parler de la météo à Nice. Vous allez chercher un endroit sur terre où les conditions météorologiques sont plus passionnantes. Mais pour commencer, Célia va être la première et elle va quand même débuter par la météo actuellement à Nice.
Alizée n’a pas choisi Célia par hasard. Elle est non seulement la première de la classe, c’est une fille qui réfléchit, qui a des idées, de panache. Elle aurait pu aussi choisir Pierre qui lui tire toutes les fibres de son cœur, mais elle le réserve pour plus tard.
- On démarre donc avec toi Célia, dit Alizée, et tous les jours, à tour de rôle, un de vous va nous donner le compte-rendu de la météo quelque part sur terre.
- Seulement sur terre ? demande Pierre.
- Où veux-tu d’autre ?
Pierre a son idée.
La mère de Célia avait déjà rangé les chaises longues et le parasol, alors elle s’est contentée d’un tapis de plage, un drap de bain. Célia demande à son père de couper une branche du palmier dans son jardin et elle met son bikini sous son short. Son MP3, normalement interdit à l’école, est dans sa poche. Une canette vide de boisson gazeuse avec une paille dans son cartable et un grand chapeau dans un sac en plastique. On ne dirait pas qu’elle va simplement à l’école. On dirait qu’elle déménage.
Tout de suite après l’appel, Alizée fait un geste de la tête vers Célia qui s’installe devant la classe. Elle déroule le tapis, se déshabille, crée du vent avec la branche du palmier, se couche, met les oreillettes du MP3, ferme les yeux et pousse un soupir de bien être. Puis elle s’adresse à la classe : « Ma mère dit que nous n’avons pas besoin d’écouter la météo, on n’a qu’à regarder par la fenêtre. Comme vous voyez, le ciel est bleu azur sur la côte d’azur. Il fait déjà 30 degrés. Ce n‘est pas franchement une journée pour aller à l’école. Je vous conseille de prendre votre maîtresse par la main et de l’amener à la plage. Aujourd’hui nous fêtons les Maurice et nous aurons, tristement, trois minutes de soleil en moins.
Alizée est ravie. Elle savait que Célia ne la décevrait pas et elle aimerait que le suivant conserve le niveau et ainsi de suite. Bien que ce ne soit pas le message de sa formation de professeur d’école, elle sait que c’est son devoir d’exiger l’originalité et l’imagination qui sont des muscles qu’il faut tout simplement … muscler !
Elle a déjà observé que Célia et Carole soient toujours ensemble et que Carole s’efforce à atteindre le niveau de son amie. Carole sera la deuxième présentatrice.
Coup de chance, Carole a déjà son idée. Elle demande à Célia : « Tu me prêtes tes accessoires ? Je les laisserai dans mon casier pour demain. »
- Tu ne peux pas faire la même chose. »
- Non, non, t’inquiètes pas.
- Tu fais quoi ?
- Tu verras.
- Ça va être où ? Célia a peur que son amie la dépasse.
- La surprise !
Carole arrive aussi en classe en bikini sous son paréo, mais au lieu de dérouler le tapis et de s’y installer, elle touche la pierre recouverte du miel noir qu’elle avait traînée soigneusement entourée du papier journal.
- Aie ! C’est dégoûtant ! On ne peut plus se baigner ici pas loin du Nouvelle-Orléans, à Biloxi, Mississippi, avec cette marée noire. Les compagnies pétrolières sont en train de nous abîmer notre planète. Je vais rentrer à mon immeuble nager dans la piscine pleine de chlore. Il fait 98 degrés fahrenheit en Nouvelle-Orléans. On n’y fête pas les saints, mais on écoute le jazz.
- C’est parfait, Carole ! dit Alizée. Qui va faire la météo demain ?
Après les deux meilleurs élèves, personne ne veut se proposer.
- Je vous donne jusqu’à cet après-midi, sinon on va procéder par ordre alphabétique.
Alizée profite de la journée pour parler de la Nouvelle-Orléans, la Louisiane, les marées noires, le président des Etats-Unis. Un sujet mène à un autre et tant pis pour le programme du C.M.1, l’important c’est de les éveiller aux problèmes du monde, non ? se demande la maîtresse. Elle se pose beaucoup de questions quant à provoquer la curiosité de ses élèves.
A la fin de la journée, Alizée n’a pas d’autre solution que de désigner Ulysse Bourgeon, l’élève le plus à la traîne de la classe.
Pour le mettre sur la bonne voie, Alizée dit : « Ulysse, tu pourrais, par exemple, parler de l’origine de ton prénom et présenter la météo en Grèce. »
Ulysse n’avait aucune idée d’où vient son prénom. Il pensait que c’était un acteur, sans doute mort ou un personnage d’une série de télé. Mais par mesure de précaution, il demande à Carole : « Tu peux me laisser tes accessoires ? Je suis sûr qu’il doit faire beau en Grèce aussi.
Il n’est pas bon élève, mais Ulysse a un ordinateur dans sa chambre. Il sait très bien trouver l’information. Sa mère n’aime pas le voir tout le temps devant son écran, mais cette fois il dit la vérité quand il crie « C’est pour l’école, maman. »
A table il demande « Pourquoi m’avez-vous nommé Ulysse ? »
- Ça rime avec « Oh hisse ! »
- J’ai aimé l’Odyssée quand j’étais jeune. Ulysse était mon héros, dit son père.
- Je suis ton héros alors, demande Ulysse.
- C’est certain ! répond son père en ébouriffant ses cheveux.
Ulysse est tout gêné devant la classe à son tour. Mais il se lance :
 « Ulysse est un des  les plus célèbres de la Grèce. Roi d', fils de  et d', il est marié à  dont il a un fils, . Il est célèbre pour sa  (« intelligence rusée »), qui rend son conseil très apprécié dans la  à laquelle il participe. Sa ruse le permet de se débrouiller dans le long périple qu'il connaît au retour de , chanté par  dans son . »
Ulysse sait qu’il a presque tout copié mot par mot d’un site sur internet, mais il espère que la maîtresse n’est pas au courant. Il étale le tapis de plage et annonce « Eh bien oui, pas de chance ! il fait beau en Grèce aussi. Nous sommes encore en Méditerranée. La mer Méditerranée est une  presque entièrement fermée, située entre l’, l’ et l’ et qui s’étend sur une superficie d’environ 2,5 millions de . Son ouverture vers l’ par le  est large de 14 kilomètres. Elle doit son nom au fait qu’elle est littéralement une « mer au milieu des terres ».
Alizée est furieuse. Elle ne sait pas comment faire face à ce fléau d’enfants qui pense se débarrasser de leurs devoirs en volant des morceaux entiers sur internet. Elle se lance dans un discours sur la plagia, sur l’originalité et sur le crime de piquer les mots des autres. « Tu as fait un effort quand même, Ulysse. Tu as appris l’origine de ton prénom, mais il faut faire plus d’effort. Tu peux lire ce qu’il y a sur internet, mais il faut le digérer et le recracher avec tes propres mots et tes propres pensées. » 
Ulysse est surpris que la maîtresse sache qu’il a tout copié.
Alizée est choquée que les enfants puissent tricher. Mais elle en profite quand même pour parler de la Grèce et des dieux grecs. Elle donne à chaque enfant le nom d’un de ces dieux pour qu’ils écrivent un paragraphe chacun sur leur dieu. « Et pas mot par mot volé d’internet ! »
A la fin de la journée, Alizée garde Ulysse auprès d’elle pour lui dire : « Il ne faut pas penser que nous sommes des idiots. Est-ce que je peux compter sur toi pour faire plus d’efforts ? »
Ulysse fait oui de la tête avant que ses larmes ne coulent.