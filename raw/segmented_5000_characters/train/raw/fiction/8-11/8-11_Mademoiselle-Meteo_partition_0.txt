Pour Guy, Shalev, ancien Maître adjoint du port de Menton Garavan, lors de sa bar mitzvah tardive et à Jean-François Rumley, météorologue suisse et à Jenny Sandler à qui ressemble Alizée.



Mademoiselle Météo


« Le ciel est amoureux et offre des nébuleux »
« Situation générale. L’atmosphère ne peut ni conter fleurette ni promettre monts et merveilles, aucun anticyclone ne pointe le bout de ses pressions à l’horizon. Au contraire, les perturbations fourmillent sur l’océan et le continent, l’une d’elles fait d’ailleurs du surplace aux portes de la région.
Prévisions pour la journée. Le ciel pense à vous à l’occasion de la Saint-Valentin et offre un gros bouquet de nébuleux avec quelques brindilles rayonnantes pour l’agrémenter. Il a même prévu des gouttes en fin d’après-midi pour l’arrosage. Il ne faut quand même pas qu’il se fane. Les températures restent assez fleuries, 8 degrés.
Les prochains jours.  Les nuages ont trouvé le chemin, ils vous rendent bien des visites. »
Jean-François Rumley, L’exprès, Neuchâtel



1. Soleil