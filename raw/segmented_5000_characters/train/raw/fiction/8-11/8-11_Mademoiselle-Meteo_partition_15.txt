11. Accalmie

Axel est de bon conseil : « Tu continues ton travail dignement et tu l’ignores. »
- Mais c’est la totalité du monde à l’extérieur de ma petite salle de classe.
- Tiens bon. 
- Je ne sais pas si je suis assez forte. Cette exclusion, même du directeur qui a toujours été un ami, me mine.
- Et si on préparait un beau mariage ?
- On ne se marie pas pour fuir une situation pourrie. Non, il faut que je résolve ce mystère. 
- Mais j’aimerais que le mariage ait lieu en juin.
Alizée devrait lui dire qu’elle ne peut pas se marier comme un marché conclu. Elle a trop envie de se blottir contre lui, mais elle est trop timide pour prendre des initiatives ailleurs que dans sa classe. Elle ne dit rien à Axel de ses doutes. Pourquoi ne veut-il la toucher ?
Elle se plonge dans l’élaboration d’une demande pédagogique pour faire la classe de la mer. Bien sûr, il faut l’autorisation du directeur, des parents et même peut-être de cette Béatrice Morel, mais elle est d’attaque pour faire tout ce qu’il faut pour promener ses élèves en dehors des murs de l’école.
Quand Célia lui dit en pleurant que ses parents ne la laisseraient jamais partir à cause de son asthme, Alizée comprend l’étendu de la complexité de son projet. Mais elle le poursuit. Pour faire n’importe quoi dans la vie, il faut foncer. Elle dépose le dossier chez le directeur qui, comme tout le monde, ne lui parle plus. Elle ne sait toujours pas de quoi elle est accusée.
Elle a enfin trouvé un météorologue pour venir parler à sa classe de son métier. C’est grâce à Doina qui a rencontré ce jeune homme franco-britannique à Londres. Le météorologue suisse d’Alizée avait répondu qu’il n’avait pas le temps en ce moment. 
Roman Johnson et Alizée sont mutuellement impressionnés. Il est aussi beau qu’elle est belle. En plus il a ce charme chaleureux rare qui accompagne les fossettes, les yeux couleur du ciel à Nice et des cheveux aussi noirs que ceux d’Alizée.
- Votre nom est vraiment Alizée Tramontane ? lui demande-t-il devant les élèves qui se sont aperçus du petit tremblement de terre entre leur maîtresse et le météorologue.
- On va renverser les choses : je vous donne la réponse et après vous me dites la question à laquelle j’ai répondu.
- Le météorologue est un spécialiste des phénomènes atmosphériques. Il étudie les pressions, les vents, les températures et tous les mouvements de l’atmosphère, leurs causes et leurs effets. C’est un scientifique de haut niveau. Il établi des prévisions. 
- C’est quoi un météorologue ? disent les enfants en chœur.
- Le météorologue n’est pas le présentateur qui fait la pluie et le beau temps à la télé. Il y a les observateurs, techniciens supérieurs, qui travaillent dans une station météo et qui se chargent de transmettre les données aux ingénieurs des travaux qui les analysent. Quand aux ingénieurs de la météorologie, ils font de la recherche ou dirige des services. Une fois les informations traitées, elles sont diffusées au grand public : transports aériens, maritimes ou terrestres, pêcheurs, agriculteurs, tourisme, santé publique.
- Est-ce que vous êtes à la télé ?
- Les techniciens et ingénieurs de Météo France sont formés principalement à l’Ecole nationale de la météorologie (ENM) basée à Toulouse. Ils sont recrutés sur concours. 
- Quelle formation et éducation ont les météorologues ?
- Les techniciens passent un concours de niveau bac S : maths, physique, informatique, anglais principalement. Une trentaine de postes par an.  
Les enfants marchent très bien dans les formulations des questions.  
- Ces scientifiques utilisent des équipements sophistiqués (satellites, ordinateurs puissants…) pour observer et relever des données terrestres et atmosphériques sur leurs écrans et leurs graphiques. 
Les élèves trouvent toujours la question juste.
- Y-a-t-il une question que je n’ai pas traitée ? demande l’invité.
- Quel est votre statut ? demande Alizée.
- Les météorologues sont des fonctionnaires de Météo France ou travaillent pour l’armée. En règle générale, ils sont en poste dans les centres de calculs ou partent en mission. Les conditions de travail peuvent être rudes. Le technicien météo doit pouvoir enchaîner des horaires en continu (mesures de nuit, jours fériés) et parfois pendant de longues périodes d’isolement.
La question qu’Alizée meurt d’envie de poser mais n’ose pas est « Est-ce que vous pourriez vous dégager pour venir en classe de mer avec nous ? »
Et puis elle trouve son courage et lui pose cette question.
Roman promène ses yeux sur Alizée de la tête aux pieds en passant par ses longues jambes et lui dit : « Oui, ça pourrait être intéressant pour moi aussi de créer un programme « mer et météo ». Oui, alors, quelles sont les dates ? »



12. Tempête