Elle va montrer cet article à ses élèves.
Le soir dans son fauteuil à bascule, elle écrit une lettre à ce météorologue à son journal, lui demandant les conditions qu’il pose pour une éventuelle visite dans sa classe pour parler de son métier. Elle profite pour consulter son téléphone magique et constater qu’il fait moins 44 degrés à Shepherd Bay au Canada et 35 degrés au soleil de Santa Rosa en Argentine.
Elle écoute de la musique en se balançant sur son fauteuil. Voilà encore une invention de tonnerre. Cet Axel a frappé juste avec ce cadeau. Elle ne saurait pas quel cadeau elle pourrait lui faire, même après ses questions en folie. Un accessoire bizarre pour une de ses nombreuses voitures ? Un porte-clefs en forme de cœur ? Ce n’est certainement pas un homme romantique ! Et est-ce qu’elle veut vraiment suivre sa direction, ou la direction de qui que ce soit ?
Il pleut dans la nuit et Alizée reste longtemps éveillée à l’écouter.
Le matin en route pour l’école elle voit un arc-en-ciel qui reste en évidence derrière les fenêtres de la classe. Elle demande qui sait comment le dire en anglais. Même pas Emma ! Elle écrit le mot sur le tableau : rainbow. Et puis elle écrit les paroles de la chanson et essaie de faire chanter ses élèves en chœur :
« Somewhere over the rainbow
Way up high,
There's a land that I heard of
Once in a lullaby.

Somewhere over the rainbow
Skies are blue,
And the dreams that you dare to dream
Really do come true.

Someday I'll wish upon a star
And wake up where the clouds are far
Behind me.
Where troubles melt like lemon drops
Away above the chimney tops
That's where you'll find me.

Somewhere over the rainbow
Bluebirds fly.
Birds fly over the rainbow.
Why then, oh why can't I?

If happy little bluebirds fly
Beyond the rainbow
Why, oh why can't I? »

Avant Noël, elle leur montrera le film « Le magicien d’Oz » avec Judy Garland.
 En plein dans sa chanson, Jennifer fait irruption dans sa classe pour crier « Il y en a qui essaie de travailler ! »  Elle s’adresse aux enfants : « Vous devriez essayer de contrôler votre maîtresse. Elle nous dérange ! Y en a qui ne comprennent rien de l’éducation. Y en a qui n’ont pas d’éducation ! Y en a qui ne savent pas que nous vivons dans un monde compétitif et qu’il faut apprendre de temps en temps. Ne pas jouer tout le temps ! Y en a qui peuvent perdre leur travail pour faute grave ! » Elle sort furieuse en claquant la porte.
Alizée, ébranlée, ne sait pas quoi dire aux enfants à part « Y en a qui ont la colère dans leur sang. »
Après la classe, elle sort de l’école comme un voleur en fuite. Mais c’est qui le voleur exactement ?



8. Temps changeant

Alizée passe pratiquement tout le week-end dans son fauteuil à bascule à couver son malaise. Elle refuse les invitations de ses sœurs et ses parents. Elle écrit des textos en réponse à Axel. Elle écrit surtout un message à sa meilleure amie Doina :
« Doina, pourquoi tu es de l’autre côté de la Manche et pas à mes côtés ? Bon, je comprends, je t’ai quitté il y a si longtemps pour Chicago et tu te venges en partant à Londres ! Il y a justice.
Mais j’ai besoin de tes conseils pour deux problèmes :
1) Jennifer, l’autre maîtresse du C.M.1 me haït, je ne sais pas pourquoi. Je ne suis pas son amie certes, pas d’atomes crochus, mais pourquoi m’a-t-elle désignée comme l’ennemi ? J’aime le directeur, j’aime mes élèves, j’aime mon métier, mais sa haine rend ma vie dans cette école pénible. Je vais essayer de lui parler.
2) Axel D. Bertrand m’a dit qu’il a l’intention de se marier avec moi. Il ne m’a pas à vrai dire demandé en mariage, il m’a juste fait part de sa décision. La principale qualité qu’il cherche en une femme c’est qu’elle suive sa direction !
Je t’entends crier ! On a beaucoup parlé toi et moi de notre homme idéal et voilà, à force de chercher cet homme, nous voilà toutes les deux, comme dirait ma tante, des vieilles filles. Je ne cherche pas à tout prix me marier, je suis bien dans ma vie, (à part cette Jennifer et que tu sois si loin). Le problème c’est que, étrangement, j’aime bien cet Axel D. Bertrand. Un autre problème c’est que je ne renoncerai jamais à mon magnifique nom de famille Tramontane et je sais qu’il n’acceptera pas un nom composé avec le mien. Pour l’instant il est à Singapore, mais quand il est là, il faut que je sois à sa disposition.
Voilà, quand reviens-tu ?
Bisous,
Alizée »