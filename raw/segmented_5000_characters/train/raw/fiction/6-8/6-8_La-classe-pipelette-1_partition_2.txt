L’inspectrice, on s'en doute, est là pour soutenir et pour aider Catherine. 
Elle lui dit et lui répète : « Quand un professeur est intéressant, les enfants l'écoutent et la classe est attentive. Sachez-le. »
Cette affirmation a réussi à chasser le peu de confiance que Catherine avait en elle-même. 
Elle passe pourtant un temps fou à préparer son cours et à chercher tous les moyens de faire en sorte que les enfants l'écoutent et que la classe soit attentive. 
Même le jour où un auteur est venu – et un auteur adoré des enfants ! – ils n’ont pas arrêté de parler entre eux.
 « Vous n'avez qu'à leur imposer les règles que vous aurez définies avec eux. Entre vos élèves et vous, il doit y avoir un contrat », suggère l’inspectrice.
Catherine fait une tentative.
« Aujourd’hui, les enfants, vous et moi allons passer un contrat, annonce-t-elle tout sourire. Nous allons faire ensemble une liste de règles que vous allez choisir, et ces règles, vous les respecterez. Elles seront la loi de la classe. »  
Catherine ajoute : « C'est moi qui fixe la première règle. La voici : 1° Quand je parle, vous vous taisez. »
Emma, qui se considère comme géniale, propose la deuxième règle :
« 2° On lève le doigt quand on veut la parole. »
« 3° On ne répond jamais tous à la fois », ajoute Catherine.
« 4° On se tait pendant la leçon ! » énonce Renaud, dont la spécialité est de parler tout le temps.
D'autres propositions fusent :
« 5° Quand la maîtresse lève la main droite, chaque doigt signifie quelque chose : 1) les yeux regardent !  2) les oreilles écoutent !  3) la bouche se ferme ! 4) les bras restent tranquilles ! 5) les pieds cessent de bouger ! »
« La bouche s'est fermée avant d’entrer en classe », rectifie Angèle.
« Écouter est un défi, on essaie de le relever ! » lance Alice, en plein délire d'hypocrisie.
« Chuchoter et rire, c'est comme bavarder. Il ne faut pas. »
 « On ne doit avoir qu'un seul mot à l'esprit : SILENCE ! » proclame Enzo.
Le brouhaha est général. Comment l'arrêter ? Comment faire taire une classe si bien lancée ?

Enzo
Je ne le fais pas exprès, mais si je n’ouvrais pas ma grande gueule, je pense que la parole me sortirait par les oreilles ou pas le nez. En plus, c’est généralement Renaud qui me parle et ce serait impoli de ma part de ne pas lui répondre. Ma mère me dit d’expliquer à Renaud que nous parlerons pendant la récré et à la cantine, mais que je dois boucler mon clapet pendant les cours. Cela dit, je n'accuse pas Renaud. Comme dit Maman, nous sommes tous responsables de nous-mêmes. Mais c’est quand même la faute de Renaud.


 – Méthode du jeu du silence –

C’est en faisant des œufs à la coque que Catherine a eu l’idée du minuteur.
Elle l’a apporté en classe. « On va faire notre séance habituelle de bruit, après quoi je minuterai votre silence. Celui d'entre vous qui restera sans parler le plus longtemps aura gagné.
– Gagné quoi, maîtresse ? 
– Ma considération. »
Pétrifiés par le tic-tac du minuteur, les élèves restent muets pendant cinq bonnes minutes. 
Tous, même Renaud. 
Catherine va lui serrer sa main : 
« Tu as ma considération. » 
Renaud prend l'air de quelqu'un qui a gagné au Loto.

Angèle
Comme mon nom l'indique, je suis angélique, c'est sûr. Je ne bavarde jamais en classe. À vrai dire, ce n’est pas par choix, c’est juste parce que je n’ai pas le courage de désobéir comme les autres. Et puis je n’ai pas grand-chose à dire. Et je ne veux pas me faire mal voir. Ma mère a assez de problèmes pour nous élever, sans qu'il lui arrive des avertissements de l’école. Les autres, je les admire, je les envie, mais en même temps ils m’énervent parce que j’aimerais pouvoir écouter en paix. En fait, les leçons de la maîtresse m’intéressent. Je pense qu’il n’y a pas de honte à ça.
Si seulement je pouvais lui venir en aide. Mais je n’ai pas non plus le courage de m'opposer aux pires parleurs de la classe.


– Méthode des instruments –

Le mari de Catherine a rassemblé pour elle un bâton de pluie, une cloche, un tambour et une trompette. 
En classe, elle commence en douceur, en secouant le bâton (discret cliquetis). Puis elle finit en fanfare, par une note stridente de trompette.
Elle demande aux enfants de s’asseoir en rond. 
Ils se passent le bâton de main en main. Quand elle dit STOP, l'élève qui tient le bâton a le droit de prendre la parole. Lorsque ça tombe sur Renaud, coup de chance, il n’a rien à dire. Il passe le bâton comme si c’était une patate chaude. Julie, elle, chante Frère Jacques. Angèle récite un poème. Enzo pousse un cri. Alice lâche un rire.
Trêve. 
Miracle ! La méthode des instruments a donné une espèce de résultat !