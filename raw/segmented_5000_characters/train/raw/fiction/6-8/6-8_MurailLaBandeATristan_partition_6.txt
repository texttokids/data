Maman ne veut pas m’acheter un revolver avec silencieux pour Noël. Elle m’a dit :
- Ah non, pas d’armes pour Noël !
Noël, c’est la fête de la paix…
Ah, là, là, les mamans, je te jure ! Ça ne comprend rien. Moi, j’aime mieux mon papa.
Justement, il y a un revolver avec silencieux dans la vitrine du Crocodile bleu. Il est encore mieux que celui qu’Andrès m’a cassé. Mais il est cher. Il vaut vingt euros.
On l’a essayé. La dame du Crocodile bleu a bien voulu. Il est lourd dans la main.
J’en ai tellement envie que j’ai rêvé que je me l’achetais, la nuit dernière.
Je vais garder mon argent de poche.
Comme ça, je me l’achèterai tout seul et je le cacherai sous mon lit.
À l’école, on a fait une rédaction sur Noël. Monsieur Languepin a écrit au tableau : Voici Noël ! Décrivez vos impressions.
On a tous fait des yeux ronds. Qu’estce que c’est, des impressions ?
- Vous n’avez qu’à parler du sapin, des vitrines, a expliqué monsieur Languepin.
Décrivez les jouets dont vous rêvez, montrez comment Noël…
On n’écoutait déjà plus. On se racontait ce qu’on allait demander et on avait plein d’idées.
- Un peu de silence, a dit notre maître, on n’écrit pas avec sa langue.
Moi, j’ai décidé que j’allais faire ma poésie de Noël. Mais j’avais le trac.
Peut-être que je ne sais pas du tout faire de la poésie ? Peut-être que tout le monde va se moquer de moi ?
J’ai écrit ça : Noël, c’est comme une grande lumière dans une forêt très noire.
Noël, c’est comme un oiseau qui chante après la pluie.
Noël, c’est comme les guerres quand elles s’arrêtent.
Pourquoi ce n’est pas Noël toute la vie ?
Ça m’a donné du mal, ma poésie.
J’avais les joues rouges de chaleur. Mais je trouvais que c’était beau ce que j’avais écrit. À la récré, tout le monde se demandait :
- Et toi, qu’est-ce que tu as mis ?
Moi, je n’ai rien dit. J’avais honte. Je suis le seul à avoir écrit une poésie.
Le lendemain, monsieur Languepin a sorti nos cahiers de classe de son cartable.
J’ai vu le mien dessus. Mon cœur s’est mis à battre très fort.
- J’ai lu vos rédactions, a dit le maître.
Vous faites beaucoup trop de fautes !
Parfois, on ne comprend même pas ce que vous avez écrit. Mais il y en a qui m’ont fait de jolies choses. Tristan, par exemple…
Je suis devenu tout chaud comme si j’avais de la fièvre et monsieur Languepin a lu ma poésie. Il l’a lue très bien, avec le ton. Tout le monde a dit que c’était super beau.
- Tristan a raison, a dit monsieur Languepin. Noël, c’est comme les guerres quand elles s’arrêtent. Je sais que vous jouez beaucoup à la guerre dans la cour, et que vous vous bagarrez. Essayez de penser un peu plus à la signification de Noël…
Ça m’embêtait ce qu’il disait. Moi aussi, je joue à la guerre. Mais ce n’est pas la vraie guerre.
Pourquoi les grandes personnes ne voient pas la différence ?
Le dernier cahier de la pile, c’était celui d’Olivier. Olivier est souvent le dernier. Le maître dit qu’il va redoubler s’il continue.
- Tu te fiches du monde, Olivier ?
Qu’est-ce que c’est, ce travail ?
Monsieur Languepin montrait le cahier.
Olivier avait écrit une seule phrase toute petite. J’ai plissé les yeux pour pouvoir lire.
Il avait écrit : Noël, c’est rien.
- Tu vas rester à la récréation pour me refaire ta rédaction, a grondé monsieur Languepin.
Et Olivier pleurait. Pourtant, on le dispute souvent et d’habitude, il ne pleure pas.
Je ne l’aime pas, Olivier. Mais je trouve qu’il n’a pas de chance. Je me demande pourquoi il a écrit que Noël, c’est rien.
Dans la cour, je n’arrivais pas à jouer avec ma bande. Ça me trottait dans la tête. Je pensais qu’Olivier ne doit pas avoir chaud avec son K-way.
- Mais qu’est-ce que tu as ? m’a demandé Karine.
Je lui ai parlé d’Olivier. Elle le déteste.
Elle a trouvé que c’était bien fait pour lui.
Mais je lui ai répondu :
- Peut-être qu’il est très pauvre, Olivier ? Et il n’aura pas de cadeaux pour Noël. C’est pour ça qu’il a écrit : « Noël, c’est rien. » C’est pour se venger contre Noël.
Andrès était d’accord avec moi. J’ai dit :
- On va lui acheter un cadeau de Noël.
- Tu n’es pas un peu fou ! a crié ma sœur. Il nous tape dessus à la récré et il t’a volé ton panda !
Mais moi, quand j’ai mon idée, je ne change pas :
- On va lui acheter le revolver avec le silencieux.
- À vingt euros ! s’est écrié Andrès en faisant ses grands yeux.
On a mis tout notre argent ensemble.
Moi, j’avais mes deux euros d’argent de poche. Karine a donné un euro cinquante.
Andrès dépense toujours son argent. Alors, il n’a rien. Didier est riche. Il a donné quatre euros. Tous ceux de la bande ont apporté quelque chose. Mais c’est des petits. Ils perdaient leur argent ou ils l’oubliaient à la maison. À la fin de la semaine, on avait 17,20 euros. Mais il nous manquait toujours de l’argent pour le revolver avec le silencieux, et la fin du trimestre se rapprochait.
Tous les soirs, on passait devant le Crocodile bleu pour surveiller notre revolver avec le silencieux. On imitait la tête d’Olivier quand il verrait son cadeau.
C’était Andrès qui le faisait le mieux.
Mais il nous fallait encore 2,80 euros. On n’allait pas les voler, quand même !
Et puis, un soir, comme on était devant la vitrine du Crocodile bleu, on a entendu quelqu’un qui disait dans notre dos :
- Super, ce revolver !
On s’est retournés. C’était Pas-deChance, notre voisin.
- On voudrait l’acheter, a dit Karine.
- Mais il nous manque 2,80 euros, a dit Andrès.
- Et il sera pour vous trois, ce revolver ? a demandé Pas-de-Chance.
Alors, je ne sais pas pourquoi, mais je lui ai raconté toutes nos affaires. Les bandes, les épreuves, le panda, Nathalie qui n’est plus ma fiancée, ma poésie de Noël et Olivier. Pas-de-Chance me posait plein de questions. D’habitude, les grandes personnes n’écoutent pas tellement quand on leur raconte nos histoires d’école, mais lui, on voyait que ça l’intéressait. Il a dit :
- Venez dans le magasin, on se gèle dans la rue.
Il a demandé le revolver avec le silencieux. La dame du Crocodile bleu l’a sorti de la vitrine. Pas-de-Chance l’a pris et il s’est tourné vers moi. Il a fait :
- Pan ! T’es mort !
On a ri et la dame aussi. On a mis l’enveloppe avec l’argent sur le comptoir. Il y avait plein de pièces de vingt et de cinquante. Mais la dame du Crocodile bleu répétait :
- Ça ne fait rien. Pensez, les enfants, ça me connaît !
Pas-de-Chance a ajouté les 2 euros et les 80 centimes.
- Le compte est bon ! a dit la dame du Crocodile bleu. Je vais vous faire un beau paquet.
Et maintenant, on a notre cadeau pour Olivier. Je l’ai caché sous mon lit.
- On l’a, notre secret, m’a dit Andrès à la récré.