- Il paraît qu’il mange les enfants et qu’il tond les caniches.
- Dors, Chien Pourri, la route est encore longue jusqu’à Chameaunix.
Chien Pourri s’endort profondément, et ronfle bruyamment. Il rêve à toute la crème Mont-Blanc qu’il mangera au sommet du glacier, mais son sommeil est agité et il se réveille en sursaut.
- Chaplapla et s’il n’y a pas de neige là-haut ?
- On pourra toujours manger un eskimau en bas des pistes, rendorstoi mon ami.
Cela ne rassure pas Chien Pourri qui pense manger un Esquimau dans un igloo.
Le minable homme des neiges C’est l’arrivée à Chameaunix, la Colonie des oubliés descend en premier, Anorak en deuxième parce qu’il a oublié sa cagoule, Chien Pourri et Chaplapla, eux, sont encore à la traîne car Chien Pourri a peur des Esquimaux et ne veut plus descendre du train. Pourtant, un joyeux comité d’accueil l’attend sur le quai. Une petite bonne femme de neige et son mari tiennent une pancarte.
- Ça doit être pour nous, s’enthousiasme Chaplapla.
Mais Chien Pourri se cache derrière un poteau en les apercevant.
« Il est encore plus moche que moi, c’est l’abominable homme des neiges, j’en suis sûr ! Il va me manger comme un enfant et me tondre comme un caniche.»
- Regarde, il a un poireau sur le nez et la petite bonne femme de neige une carotte, s’amuse Chaplapla.
« Ils doivent faire de bonnes soupes », pense Chien Pourri, qui n’est tout de même pas rassuré.
Le petit mari se présente :
- Je suis le minable homme des neiges, dit-il.
- Pourquoi dites-vous ça ? demande Chaplapla.
- Je ne sais même pas faire de chasse-neige, je suis trop minable.
- Il est comme ça depuis qu’il a raté son Ourson, dit la petite bonne femme de neige.
- Au vieux chalet, on vous a réservé une chambre qui donne sur les poubelles, prévient le minable homme des neiges.
- On a l’habitude, le rassure Chaplapla.
La Colonie des oubliés, elle, a oublié de réserver des chambres d’hôtel. Et en pleine saison, autant dire, qu’il n’y a de place nulle part.
- On peut vous dépanner, propose la petite bonne femme de neige.
Personne ne vient jamais dans notre hôtel.
- Moi aussi, je veux y aller, je veux rester avec la colo pourrie, dit Anorak.
- Jamais de la vie je n’irai dormir dans un taudis, dit son papa.
- On y va ou je fais une crise ! prévient Anorak.
- Calme-toi, mon chéri, tu veux un su-sucre ?
« Il aurait dû avoir un chien plutôt qu’un enfant », pense Chien Pourri.
Devant l’entêtement de son bambin, l’inspecteur du guide Mi-chemin accepte.
« Il va nous retirer une étoile, déjà qu’on en avait aucune », soupire le minable homme des neiges.
Chien Pourri prend les choses en main et fait grimper tout le monde dans un vieux cageot où s’entassent Anorak, son papa ainsi que la Colonie des oubliés qui n’a pas oublié d’être bête.
- Je peux le fouetter avec ma frange pour qu’il avance plus vite ? propose le caniche à frange.
- Je peux le mordre ? suggère le pit-bull.
- Je vais le dire à ma maman qu’il traîne mon manteau dans le caniveau, fayote le basset.
Chien Pourri poursuit sa route et, devant la montagne enneigée, laisse éclater sa joie :
- Les vacances aux sports d’hier, c’est aujourd’hui ! Tous huskys !
Le Vieux chat laid Au fond d’une vallée, très loin des pistes, entourée de sapins, de crevasses et de plaques de verglas se trouve une petite cabane en bois, sans chauffage, sans électricité et sans charme. C’est le vieux chalet.
- Ben dis donc, ce n’est pas terrible, fait le caniche à frange.
- Je n’ai jamais vu un hôtel aussi minable, dit le papa d’Anorak. Et j’en ai vu !
- C’est rustique, convient le minable homme des neiges.
À ses côtés, Chien Pourri tremble.
- J’ai peur Chaplapla.
- De quoi Chien Pourri ?
- Du minable homme des neiges.
- Ne t’inquiète pas, tu vois bien qu’il n’est pas méchant.
- Ce n’est pas ça, j’ai peur d’être aussi minable que lui.
Chien Pourri n’est pas au bout de ses peines.
- Les camions-poubelles ne veulent plus passer ici, ils disent que c’est trop moche, confie le minable homme des neiges.
- C’est pour ça qu’on a engagé un chien de traîneau des poubelles, confirme la petite bonne femme de neige. Le vieux chat moche, toi, tu pourras servir d’enseigne à notre chalet, propose-t-elle à Chaplapla, pendant que ton copain ira à la décharge.
- Si vous cloutez Chaplapla sur votre palissade, je dis à mon papa de vous retirer une étoile, dit Anorak.
- De toute façon, on n’en a pas, avoue le minable homme des neiges.
- Calme-toi Anorak, tu vas avoir trop chaud sous ton anorak, dit son papa.
Mais rien ne peut calmer un enfant devant une injustice.
- Le minable homme des neiges est horrible ! crie Anorak.
- Je sais, il est affreux mais il me fait fondre, avoue la petite bonne femme de neige.
Chien Pourri, lui, doit faire vite pour traîner les poubelles s’il veut avoir le temps de se traîner sur les pistes avant la nuit tombée.