Les poilus sont des individus drôles, taquins et souvent mauvais joueurs. Mais ils sont très propres.
Lorsqu’ils voient des poux, ils prennent leurs jambes à leur cou.
Dès qu’il rencontre un ami, un poilu pousse un son bizarre « Arghf !» que personne ne peut vraiment refaire. Puis ils courent l’un vers l’autre pour se frotter amicalement le poil de leur ventre dodu.
Avant de se gratter, un poilu demande toujours la permission en mettant tous ses poils en avant. Sur sa tête se dresse alors une énorme touffe.
Les poilus adorent prendre un bain et se rouler dans l’eau. Quel jeu rigolo! Ca rend leurs poils tout doux !
Très vite, ils se sèchent au vent. Rien de mieux qu’une bonne bourrasque pour se faire un beau brushing!!! Et lorsqu’il n’y a pas de vent? Aucun problème! Il suffit de s’accrocher à une voiture.
Leurs poils se dressent comme les piquants d’un hérisson. Les poilus doivent alors se sauter dessus « à poils joints » pour faire retomber le volume.
Mais la meilleure technique pour dompter leur merveilleuse tignasse c’est, sans nul doute, la toupie.
Soudain, un énorme orage arrive, les poilus sont à nouveau mouillés et leurs cheveux sont retombés.
Pour les faire sécher, il suffit de les essorer.