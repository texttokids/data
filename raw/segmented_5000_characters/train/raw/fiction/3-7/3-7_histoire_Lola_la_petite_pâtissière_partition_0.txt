Depuis toute petite, Lola aime la cuisine, elle adore regarder ses parents préparer à manger.
Mais, ce qu’elle préfère par dessus tout c’est regarder les beaux gâteaux dans les vitrines de la boulangerie.
Très tôt, Lola voulut elle-même s’essayer à la pâtisserie, utilisant son petit frère comme commis de cuisine pour l’aider dans ses préparations.
Aussi, avec l’aide de sa maman, elle réalisa des goûters pour ses copains et copines qui apprécièrent ses gâteaux.
Petit à petit, elle commença à créer seule ses propres recettes qu’elle fit goûter à sa famille. Il y eut des réussites mais aussi quelques échecs…
Pour Noël et son anniversaire, la famille de Lola lui offrit des livres de pâtisserie sachant comme cela lui ferait plaisir.
Puis, un jour, son papa lui apprit qu’il l’avait inscrite à un concours de pâtisserie pour enfants.
Lola dû créer une recette de gâteau originale.
Elle l’envoya et fut sélectionnée pour l’étape suivante du concours, laquelle consistait en la réalisation de cette recette.
Ainsi, peu de temps après, Lola se retrouva parmi quatre autres participants dans une grande cuisine sous l’oeil attentif d’un pâtissier.
Après une heure et demie de travail, chacun présenta son gâteau au jury composé de trois personnes.
Lola était inquiète en voyant l’incroyable travail de ses concurrents qui avaient rivalisé d’inventivité.
Elle n’en menait pas large quand ce fut son tour de passage.
Pourtant, en goûtant son gâteau, les visages des jurés s’illuminèrent.
Et, une demie-heure plus tard, le jury annonça la victoire de Lola ! Son gâteau était simple mais absolument délicieux et sa famille fut très fière d’elle.
Elle repartit avec un trophée et, en cadeau, un an de cours de cuisine. Ainsi, Lola pourra encore se perfectionner et devenir une grande pâtissière !