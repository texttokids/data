Ce weekend, les parents d’Ulysse l’emmènent à Chambord.
Le château de Chambord est l’un des nombreux châteaux qui bordent la Loire : il y en a de très connus, mais aussi pleins d’autres, bien plus petits et bien plus cachés !
L’arrivée à Chambord est impressionnante : le château a plus de 365 cheminées décorées avec des pierres noires et blanches.
Ils vont aller voir un spectacle de théâtre et de musique baroque. En attendant, ils visitent l’escalier à « double révolution », imaginé par Léonard de Vinci: ce sont deux escaliers qui s’entortillent, et on peut monter et descendre en même temps sans se croiser, c’est rigolo !
Le spectacle commence ! Ulysse et ses parents s’installent en face de la scène où un orchestre va jouer de la musique de Lully, sur l’histoire du Chat Botté.
Les ruses du Chat Botté font bien rire Ulysse, et il est très impressionné par les très beaux costumes des acteurs !
Après un heureux dénouement, les acteurs et les spectateurs partent ensemble au jardin, où un magnifique dîner est servi. « C’est décidé, se dit Ulysse, plus tard j’aurais mon propre château! »