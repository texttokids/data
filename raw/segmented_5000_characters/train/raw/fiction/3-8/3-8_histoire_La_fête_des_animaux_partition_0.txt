En plein coeur de la Savane, le Roi Arthur, lion parmi les lions, se désaltère au bord de la rivière.
Le singe Tepee suspendu à une branche lui dit : "Arthur, le jour de la liberté approche et nous n’avons toujours pas décidé ce que nous faisions pour les festivités. Ta fille dansera-t-elle cette année ?"
"La fille d’un roi ne danse pas Tepee, c’est une princesse" répond Arthur. "De plus, il me semble qu’il y aura la parade des singes, le concert des serpents et des hippos et le défilé des lionnes. Ce sera un très beau spectacle. Et toi que prévois-tu pour le spectacle ?" demande Arthur à Tepee.
"Heu, heuu...", Tepee le singe ne répond pas. Arthur lui dit : "Mais enfin paresseux, ne me dis pas que tous les animaux de la savane ont prévu quelque chose pour ce jour de fête et que les singes seront les seuls à ne rien faire ?"
"Ce n’est pas ça", répond le singe, "nous avons été choisis pour danser et nous ne savons pas." Sans plus attendre, Arthur emmène le singe et les membres du clan des singes voir sa fille, Lia.
Arthur dit à sa fille : "Tu dois apprendre aux singes à danser. » Lia se roule par terre en riant, les oiseaux tombent de la branche. Tous les animaux présents se moquent des singes.
Arthur, en chef, s’élève contre ces moqueries. "Comment osez-vous rire de cela ? La parade est le moment le plus important de la Savane. Alors au travail !" dit-il en regardant sa fille.
Lia commence alors des répétitions, les singes essayent de danser, mais personne n’écoute les consignes données par la princesse.
"Et un et deux et trois et un et deux et trois ! Allez Tepee, bouge ton corps !!!" dit-elle. "J’ai faim en plus" répond Tepee.
Voyant l’ampleur de la catastrophe, Lia a soudain une idée. "Bon nous allons prendre les choses que vous aimez manger et nous allons faire une chorégraphie avec cela."
"Oh oui, oh oui !" répondent les singes, heureux d’imaginer un moment de plaisir dans cet instant délicat pour eux. Chacun prend une grande feuille d’arbre, des bananes, des fruits et tout ce que chacun aime.
Lia propose alors en musique que chacun lance aux autres ses fruits, ses aliments et que chacun essaie de les attraper en dansant. La magie opère et tous les animaux se mettent à danser.
Le jour de la parade est arrivé. Le défilé commence, les singes sont fiers de montrer leur prouesse devant le lion, assis sur son trône en tronc d'arbre. Une ovation est réalisée par tous les animaux lors du passage des singes.
Le Lion se lève, félicite Lia et tous les singes avant que tous les animaux ne se retrouvent pour faire la fête.