Le lendemain matin, Flavia entre dans la cour de l’école en faisant tourner les billes de Quentin dans sa poche. La directrice attend la sonnerie pour annoncer à tout le monde :
— Vu le succès des billes à la récréation, nous avons décidé d’organiser un tournoi dans toute l’école. Aujourd’hui, chaque classe devra choisir un champion ou une championne. Et demain, tous les champions s’affronteront lors du tournoi. Bonne chance à tous !
— L’important, c’est de jouer, se répète Flavia avant de se lancer dans une partie avec les élèves de sa classe. 
Avec ses nouvelles billes, elle retrouve confiance en elle. Et maintenant qu’elle sait ce que ça fait de perdre et de se sentir nulle, elle se montre plus gentille avec ses adversaires. Si bien qu’à la fin de la partie, même si Flavia a de nouveau battu tout le monde, tous les perdants sont prêts à encourager la championne de la classe pour le tournoi du lendemain.
Gabriel est déçu de ne pas pouvoir assister au tournoi, alors il offre sa plus belle bille à sa sœur pour lui porter chance. En la quittant devant l’école, il lui fait le V de la victoire. Flavia entre dans la cour comme une conquérante, entourée de ses supporters. Anna chante : « Le jour de gloire est arrivé » et toutes ses sœurs la couvrent de bisous pour l’encourager.
La maîtresse de Flavia lui accroche un badge « CP ». Tous les autres champions sont prêts. Flavia a l’impression de participer à des jeux internationaux. La directrice a un micro et elle annonce l’ordre des rencontres. Puis elle se met à commenter les parties.
Flavia affronte d’abord la championne de CE1. Comme les élèves sont censés soutenir leur champion, les sœurs de Flavia ne peuvent pas rester avec elle. Il n’y a qu’Élisa qui assiste à la partie. Qu’est-ce elle est contente de voir gagner sa petite sœur !
Ensuite, Flavia tombe contre le champion de CE2 qui est aussi le champion de son cœur : Quentin. La cérémonie veut qu’ils se serrent la main. C’est la première fois qu’ils se touchent et Flavia se sent toute flagada. Mais elle se ressaisit : elle ne veut pas manquer l’occasion d’impressionner celui qu’elle aime. Elle le remercie pour son mot et son cadeau et ils commencent à jouer.
La partie est très serrée. Impossible de prédire qui va gagner. 
Mais soudain, Mouad lance à son ami :
— Tu ne vas pas te laisser avoir par cette bébête de CP ! 
Flavia se concentre et joue la bille de Gabriel. C’est le coup qui lui permet de remporter la partie !
Flavia suit les conseils de ses sœurs et félicite Quentin. Ils se serrent de nouveau la main. Flavia n’a pas envie de lâcher celle de Quentin ! Elle lui glisse à son tour une poignée de billes. Malgré sa défaite, Quentin a le sourire.
Elle prend facilement le champion nul du CM1.
Il reste une dernière partie à jouer, contre la championne de CM2. Flavia commence à faiblir, mais elle ne veut pas décevoir sa famille ni sa classe. Bella lui donne discrètement du chocolat pour la requinquer.
Après une belle finale, Flavia est déclarée championne de l’école ! Ses sœurs et les élèves de sa classe l’entourent et tout le monde la complimente. Quentin aussi vient la féliciter. Flavia est sur un petit nuage.
— Est-ce que tu veux… Est-ce que ça te dirait de… bafouille-t-elle.
Ce n’est pas encore aujourd’hui que je le demanderai en mariage, se dit Flavia. Demain, peut-être…
— Est-ce que tu as envie de venir fêter la victoire à la maison ce soir ? propose-t-elle.
— Oui, bien sûr !
Et ce soir-là, chez les Arthur, on improvise un immense circuit de billes dans le salon et tout le monde joue ensemble, sans perdants ni gagnants. Pour Flavia, c’est une des plus belles soirées de sa vie.