Le mercredi, Paul se rend chez sa grand-mère.
Paul adore ce jour car il sait qu’il va lire un livre dans le fauteuil moelleux de son grand-père.
Mais il sait surtout qu’il va pouvoir se régaler en allant faire quelques courses avec sa grand-mère. Sur le chemin, il y a un camion à crêpes, et Paul déguste toujours une crêpe au sucre.
Hummmm… un délice !
Mais le moment qu’il attend avec impatience arrive très vite. Aller au parc et retrouver les copains du mercredi ! Alors en arrivant devant l’entrée, dès qu’il voit le toboggan, Paul observe ceux qui sont présents.
Il y a Lily, Claire, Léon, Tom et plein d’autres copains et copines.
Ensemble ils vont jouer dans le bac à sable, grimper à l’échelle, essayer de s’attraper.
Paul va même jouer au pirate en imaginant qu’il est comme « Peter Le Pirate ».
Et puis vient le moment de rentrer, Paul n’est pas toujours content mais il sait que sa grand-mère lui a préparé son plat préféré.
En arrivant à la maison, son grand-père lui sert des pâtes à la sauce tomate. Hummm un régal. « C’est cool d’être chez ses grands-parents ! » dit-il.