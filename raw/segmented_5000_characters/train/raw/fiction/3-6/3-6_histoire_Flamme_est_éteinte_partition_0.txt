Dylan et Eliott sont amis depuis qu’ils sont tout petits. Ensemble, ils parcourent le monde jour après jour en quête d’aventures. 
Ils ne se séparent jamais de leur amie Flamme, qu’ils ont rencontré au cours d’un de leur périple. Flamme est une petite boule de feu qui leur apporte chaleur et lumière, mais Flamme est aussi une amie très optimiste et elle arrive toujours à voir le bon coté des choses. Surtout quand ils arrivent des mésaventures à ses amis.
Aujourd’hui nos trois amis se trouvent au nord du Canada, dans une immense forêt aux arbres majestueux et verdoyants. 
Alors que nos trois amis se baladent et découvre la flore de cette forêt, ils entendent au loin une énorme plainte entre éternuement et grognement. Leurs curiosité piquée à vif, tout trois se dirigent en courant vers la plainte, qui se répètent encore plusieurs fois.
Au bout de quelques mètres, Dylan, Eliott et Flamme arrive devant une grotte où se trouve un immense ours avec le nez tout rouge et un énorme mouchoir à pois dans la main. 
- Et bien l’ours, que t’arrive-t-il ? Quel est ton nom ? dit Dylan. 
- Je m’appelle Tao, je suis enrhumé depuis des jours, je n’arrive ni à me réchauffer ni à me soigner. Si ce satané rhume ne s’arrête pas, je vais finir par mourir.
Les trois amis sont compatissants devant l’état de Tao qui semble très épuisé. 
Dylan propose alors d’aller dans la forêt chercher des feuilles pour créer un matelas à Tao pour qu’il puisse s’y reposer. 
Eliott, lui, se propose d’aller récolter de la nourriture pour en faire une soupe bien chaude. 
Flamme offre sa chaleur pour que Tao se réchauffe. 
En quelques heures Tao se sent déjà mieux.
Mais alors qu’il se relève de son nouveau matelas, il lance un énorme éternuement et provoque une immense bourrasque de vent. Flamme se trouvant à proximité, en quelques seconde, son feu s’éteint. 
- Olalalala ! s’écrie Tao, Dylan et Eliott. Flamme se retrouve grelottante de froid et n’est plus constituée que de petites braises. 
- Je suis vraiment désolée, s’exclame Tao. 
- Ce n’étais qu’un accident Tao, ne t’inquiète pas, murmure Flamme.
- Comment l’aider maintenant ? Si on attend trop, elle va mourir, dit Eliott. 
C’est alors qu’il vient une idée à Tao. 
-De nombreux campeurs perdent des objets dans la forêt et souvent je ramasse ceux que je trouve jolis ou qui me paraissent utiles. Peut être que l’un d’entre eux pourrait aider votre amie. 
- Où sont ces objets Tao ? demande Dylan. 
Tao se dirige vers le fond de sa caverne.
En effet, le fond de la caverne de Tao est rempli d’objets en tout genre. 
Eliott, Dylan et Tao se mettent à chercher parmi cette caverne d’Alibaba. 
Ils trouvent un thermos, plusieurs casseroles, un couteau-suisse, un chapeau et même une chaise.
- J’ai trouvé ! s’exclame Tao. 
- Qu’est ce que c’est ? 
Tao montre une petite boite au creux de sa patte : c’est une boite d’allumettes. 
Tout trois se précipitent vers Flamme qui a un teint de plus en plus gris. 
Tao frotte l’allumette contre la boite et elle s’enflamme aussitôt. Il la tend à Flamme qui ouvre la bouche et l’avale.
Très vite Dylan en prend une deuxième et lui tend pour être sur que ce plan marchera. 
En quelques instant Flamme reprend des couleurs et s’enflamme de nouveau retrouvant toute sa vitalité et son sourire. 
Les trois autres sont soulagés. 
Flamme sautille partout de joie et remercie Tao.
Tao offre à ses trois nouveaux amis la boîte d’allumettes qui pourrait peut-être, un jour, sauver Flamme de nouveau. 
Tao a gagné un matelas, un nouvelle recette de soupe, la santé mais surtout trois compagnons.