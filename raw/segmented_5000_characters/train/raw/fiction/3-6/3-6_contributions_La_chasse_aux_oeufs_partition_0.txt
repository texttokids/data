Il était une fois une grande famille. Il y avait les grands-parents, les parents, leurs deux enfants, une fille et un garçon, leurs cousins et leur chien Max.
- Les enfants ! Annonça la mère. Savez-vous quel jour nous sommes aujourd’hui? C’est le fameux Lundi de Pâques ! À cette occasion, un mystérieux lapin est venu cette nuit déposer des oeufs magiques partout dans le jardin…
- Mais attention ! Tous les oeufs sont cachés ! Nous allons organiser une chasse au trésor. Le premier qui récoltera le plus d’oeufs colorés aura gagné ! - Vite tous au jardin ! S’écrièrent tous les enfants.
La petite Pauline court au pied de l’arbre et découvre trois magnifiques oeufs rouges.
Le petit Valentin court vers les pots de fleurs et réussit à trouver deux gros oeufs bleus.
Le petit Pascal s’empare du nain de jardin et le secoue pour voir si un oeuf sort de son chapeau. Mais le nain de jardin est vide !
- Alors les enfants, avez-vous trouvé des oeufs magiques? Demande le père. - J’ai trouvé deux oeufs bleus ! S’écrie Valentin. - J’ai trouvé trois oeufs rouges ! S’écrie Pauline. - Bravo Pauline tu as gagné ! Mais Pascal tu n’as rien trouvé ? Demande la mère. - Moi je n’ai rien trouvé… Le nain de jardin était vide !
- Mais le nain de Jardin cachait un oeuf jaune ! Qui a bien pu le manger ?? - C'est Max ! Tout le monde se tourne vers le chien Max qui a le museau recouvert d'un papier jaune. L'oeuf en chocolat est caché sous ses pattes.
- Mais maman pourquoi ce sont des oeufs magiques? - Ouvre ton oeuf et tu verras…
- Et oui le lundi de Pâques, il y a des oeufs en chocolat ! Tous les enfants crient de joie et se mettent à dévorer les chocolats.