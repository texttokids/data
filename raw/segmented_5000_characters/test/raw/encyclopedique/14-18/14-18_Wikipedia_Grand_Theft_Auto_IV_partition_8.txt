GameTrailers Game of the Year Awards 2008 Jeu de l'année Tout


GameSpy Game of the Year 2008 Meilleur scénario Tout système


Nouveau meilleur personnage : Brucie Tout système ^


Giant Bomb Golden Anniversary Year-End Awards Extravaganza


Kotaku's 2008 Games of the Year Awards Jeu de l'année Tout


TeamXbox Game of the Year Awards 2008 Meilleur jeu d'action-aventure


Spike TV Video Game Awards 2008 Jeu de l'année Tout système


Meilleure performance de doublage d'un personnage masculin : Michael Hollick dans le rôle de Niko Bellic Tout système 26^e édition des Golden Joystick Awards BBC 1Xtra, effets


Jeu de l'année (Arvato Digital Services) Xbox 360 G4 G-Phoria 2008 Meilleur nouveau personnage : Niko Bellic


Jeu à longue durée, présenté par Stride Tout système


Time, The Top 10 Everything of 2008: Top 10 Video Games Jeu vidéo 2009 Entertainment Merchants Association, Home Entertainment Awards –


Michael Hollick, voix officielle de Niko Bellic, est récompensé par Spike TV dans la catégorie « Meilleure performance dans


récompensé à plusieurs reprises lors d'importants événements. Il est sacré « Jeu de l'année » par de nombreux médias spécialisés dans les


GameTrailers^ et par des médias libéraux, tels que le


Time^. Le jeu remporte à plus de soixante reprises le prix de « Jeu de l'année », bien plus que tous les autres publiés la même année. Grand Theft Auto IV est également nommé sept fois à la cinquième édition des BAFTA Games Awards^ et trois fois à la neuvième édition des Game Developers Choice Awards^, mais aucune récompense ne lui est décernée. Le site web français Jeuxvideo.com le classe 17^e meilleur jeu


Avant même sa publication, Grand Theft Auto IV devient la cible de nombreuses polémiques. Des personnalités américaines telles que Jack Thompson et Glenn Beck et britanniques telles que George Galloway, ainsi que plusieurs organisations new-yorkaises et l'association Mothers Against Drunk Driving (MADD) qui lutte contre l'alcool au volant, critiquent


possibilité de conduire sous l'influence de l'alcool, la MADD exige de la société Entertainment Software Rating Board (ESRB) le changement de la classification du jeu, qui est de « M » (Mature 17+ ; réservé à un public âgé au-dessus de 17 ans), en « A » (Adults Only ;


Les versions australienne et néo-zélandaise du jeu sont censurées et éditées afin de suivre les critères imposés par le système de classification australien^. Cependant, le jeu est renvoyé dans son intégralité à l'organisation OFLC par Stan Calif, un jeune étudiant de 21 ans, mécontent de la version néo-zélandaise. La version intégrale du jeu est alors rééditée, classée néo-zélandais^. La version non éditée de Microsoft Windows


Europe, plus particulièrement au Royaume-Uni, Grand Theft Auto IV est très largement attaqué pour son contenu jugé


253]. Le premier contenu épisodique, The Lost and Damned, montre un homme intégralement nu, une scène inhabituelle dans les jeux


Selon les presses d'actualités, le jeu influence les actions de joueurs confirmés et du public en général. En avril 2008 à Londres, des violences éclatent entre deux groupes près d'un bar de


selon les spéculations, le jeu aurait suscité cette violence, ce qui est plus tard démenti^. En juin 2008, au New Hyde Park de New York, six adolescents sont appréhendés pour avoir racketté plusieurs individus et pour plusieurs tentatives de vols de voiture^ ; selon la police, les adolescents s'étaient inspirés des événements de Grand Theft Auto IV, ce qui a pour effet de relancer la polémique déjà très présente dans le pays^. En Thaïlande, le jeu est retiré le 18 août 2008 après l'assassinat d'un chauffeur de taxi par un homme qui se disait passionné de Grand Theft Auto IV^. À Lyon en France, un adolescent est appréhendé en septembre 2008 pour avoir brûlé des voitures ; il donne comme explication s'être inspiré du jeu^. En Louisiane, en août 2013, un garçon de huit ans tire à l'arme à feu sur sa grand-mère quelques minutes après avoir