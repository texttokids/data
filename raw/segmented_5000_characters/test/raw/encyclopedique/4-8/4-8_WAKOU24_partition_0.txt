Partir ou rester
De nombreux oiseaux prennent la décision de migrer à cause du manque de nourriture. Cigogne, loriot et faucon hobereau partent en Afrique dévorer de gros criquets. Comment survivent ceux qui restent ? Des insectes et des araignées se cachent encore sous les écorces. Pour les trouver, les mésanges charbonnières et les rouges-gorges inspectent les troncs et... Miam, voilà un moucheron !
Le jaseur boréal voyage dans toute l'Europe pour déguster des baies et des fruits durant tout l'hiver.
Avant d'hiberner, la marmotte est très grosse. A son réveil, en avril, elle sera toute maigre !
Gros dodo
Le bouquetin gratte la neige pour trouver l'herbe cachée dessous. Scronch!
Pour le loir, les chauves-souris et l'ours, une seule solution : dormir jusqu'au printemps. C'est l'hibernation. Quand ils dorment, les animaux dépensent moins d'énergie. Mais ils doivent d'abord faire des réserves de graisse. Ils mangent beaucoup, et hop !, au lit ! Dans un trou d'arbre, une grotte ou un terrier, ils se roulent en boule. La température de leur corps baisse et ils s'endorment.