Les éléphants sont les plus grands animaux terrestres vivant actuellement : l'éléphant d'Afrique mesure en moyenne 4 mètres de haut (à l'épaule) et pèse environ 6 tonnes, pour le mâle (contre 4 chez la femelle). Ce sont des mammifères herbivores à la peau très épaisse. C'est pour cela qu'ils sont aussi appelés pachydermes, (pachy- signifiant épais en grec et -derme la peau). 
La femelle ne donne naissance qu'à un seul petit à la fois, après l'avoir gardé dans son ventre pendant 22 mois. Il entend très bien grâce à ses grandes oreilles, a un bon odorat avec sa trompe, mais la vue est assez faible avec ses petits yeux. Son cri s'appelle le barrissement.


Il existe deux  espèces d'éléphant1 : l'éléphant d'Asie, l'éléphant de savane d'Afrique, et l'éléphant de forêt d'Afrique.
Les deux espèces d'éléphants d'Afrique ont longtemps été considérées comme une seule. Une étude scientifique a prouvé la séparation des deux espèces il y a plusieurs millions d'années, mais des doutes subsistent toujours et certains scientifiques considèrent l'éléphant de savane d'Afrique, et l'éléphant de forêt d'Afrique comme deux sous-espèces d'une seule et même espèce, l'éléphant d'Afrique. 
L'éléphant d'Afrique, peut mesurer jusqu'à 3,70 m au garrot, peser jusqu'à 7 tonnes. Il possède de très grandes oreilles et sa trompe se termine par deux « doigts ». Deux variétés d'éléphants habitent l'Afrique, ceux des forêts et ceux de la savane, les plus grands. Ils se déplacent pour trouver suffisamment de nourriture. Les femelles marchent d'abord, avec les petits, et les mâles restent en arrière et n'interviennent qu'en cas de danger.
L'éléphant d'Asie est plus petit, environ 3 m au garrot, possède des oreilles nettement plus petites et sa trompe se termine par un seul « doigt ». La principale différence est que l'éléphant asiatique, capturé jeune, se laisse apprivoiser s'il est bien soigné. Il est dirigé par son cornac, un homme qui est à la fois son soigneur et son dresseur. L'éléphant vivant très longtemps, c'est presque une union pour la vie entre cet homme et l'animal. 





L'éléphant ne peut sauter compte tenu de sa masse, et ne peut courir également, il peut avancer assez vite (comparé à un homme) lorsqu'il charge par exemple, en se dandinant de gauche à droite (marche à l'amble). Il peut atteindre 15 à 20 km/h en moyenne sur une course de vitesse avec des pointes records à 30 voire 40 km/h maximum. 
Le record est réalisé en 2002 avec un éléphant d'Asie mâle de 3 tonnes qui a atteint 23 km/h en moyenne, mais il n'était pas très gros comparé aux autres, ceux de 5 tonnes n'avancent pas à plus de 17 km/h en moyenne.


La trompe de l'éléphant est l'allongement de son nez et de sa lèvre supérieure. C'est un organe puissant constitué d'environ 15 000 muscles qui lui permet de réaliser des gestes indispensables à sa vie quotidienne. Elle lui sert à sentir, à toucher, à saisir des aliments, des objets ou encore à caresser les membres de sa famille. Elle lui permet d'aspirer jusqu'à 10 L d'eau pour les verser ensuite dans sa bouche, s’asperger lors de sa toilette ou de celle des petits. En s'enroulant autour d'une branche, la trompe peut la casser ou l'écarter. La trompe des éléphanteaux leur permet de s'accrocher à leur mère lors des longs trajets. C'est un véritable outil à tout faire.


Les éléphants ont une excellente mémoire tout comme les dauphins, les singes et les hommes. Ils sont capables de reconnaître leurs semblables même s'ils ne les ont pas vus depuis 20 ans. Des scientifiques ont évalué cette mémoire en plaçant une trentaine d'échantillons d'urine dans plusieurs endroits et ils ont observé. 20 ans après, les éléphants s'en sont souvenus.