La navette spatiale Bourane (Бура́н « tempête de neige » en russe et Buran en anglais), ou aussi OK-1.01, est la première navette spatiale russe. C'est la navette la plus connue du programme Bourane car c'est la seule du programme qui ait connu l'espace et fait un vol orbital. Elle est la première d'une série de 5 navettes (uniquement deux d'entre elles ont été achevées). 
Construite en 1986, elle a effectué son unique vol en mode automatique sans équipage le 15 novembre 1988. Elle a été accidentellement détruite le 12 mai 2002 par suite de l'effondrement du toit de son hangar dont l'architecture n'était pas adaptée à la pluie.


La construction de cette navette commence en 1986 et dure deux ans. Bourane est construite dans sa totalité à Moscou. Au début, elle se nomme Baïkal avant d'être renommée en Bourane peu de temps avant son premier vol1.




C'est l'avion Antonov An-225 Mriya qui permettra le transport de la navette du site de construction au site de lancement2.


La préparation de ce premier vol est longue : commencée en même temps que la construction du projet Bourane (qui voulait attendre que la navette spatiale américaine fasse plusieurs missions avec parmi elles, la mission au cours de laquelle aura lieu la catastrophe de Challenger), elle dure 12 ans.
Officiellement, le décollage a lieu le 29 octobre 1988 mais, 51 secondes avant le décollage, une anomalie se déclenche au niveau du système de guidage. Ce problème retarde le lancement de la navette au 15 novembre 1988.
Le jour venu, les préparatifs se déroulent sans aucun problème. En revanche, les conditions météo sont très défavorables et le service météo envoie un signal avertisseur de tempête aux personnes concernées. En principe, des réacteurs sont censés se trouver sur la navette pour que lors de la descente, Bourane puisse reprendre de l'altitude s'il y avait un problème, mais ils ne sont pas installés pour ce vol. Bourane serait donc obligée de planer à l'atterrissage. Ceci dit, des ingénieurs ayant participé à ce projet de vol assurent que le vol sera un succès, car les conditions limites pour l'atterrissage de la navette ne sont même pas atteintes. Le lancement est finalement décidé.


Le 15 novembre 1988, la navette spatiale Bourane effectue son premier vol qui sera le seul de tout le programme Bourane. Le décollage a lieu de nuit. À 6 h du matin (heure de Moscou), Bourane et le lanceur Energia s'arrachent de la plate-forme de lancement. Dès le décollage, Bourane effectue une rotation comme la navette spatiale américaine (qui utilise le pas de tir d'un ancien lanceur passé et qui a besoin de faire cette rotation pour se mettre sur la bonne orbite). Même si son pas de tir a été construit en même temps que le programme, Bourane fait la même manœuvre que la navette spatiale américaine. En effet, Bourane est censée la concurrencer. 
Un peu plus de deux minutes après le décollage (140 secondes), les quatre boosters (premier étage du lanceur) se séparent en s'éjectant par deux de la partie centrale d'Energia (deuxième étage du lanceur), se séparent à nouveau pour retomber individuellement et des parachutes se déploient : les boosters sont réutilisables. Huit minutes après le décollage, le lanceur Energia se sépare de Bourane. C'est à ce moment-là que le lanceur pourrait larguer un satellite, mais ce ne sera pas le cas lors de ce vol (et donc jamais). 
Contrairement à la navette spatiale américaine où le réservoir externe brûle et se détruit en rentrant dans l'atmosphère, le lanceur Energia tombe dans l'océan Pacifique sans brûler, car il n'est pas prévu pour l'être et mettrait plusieurs jours à brûler. Les moteurs de cette partie du lanceur ne sont pas réutilisables. La navette Bourane se met en orbite avec ses propres moteurs.


Durant son vol orbital, Bourane est inclinée à 51,6°. Elle effectue deux tours du monde (120 000 km), sans aucun problème. Après 1 heure 30 minutes de vol, Bourane se prépare à la manœuvre de désorbitage et à la rentrée dans l'atmosphère.


Au moment de la rentrée dans l'atmosphère, la liaison avec la navette est coupée. Après 20 minutes, la liaison revient. L'approche finale se fait automatiquement. Un avion, un MIG-25, s'envole et s'approche de Bourane lorsqu'elle est encore à 7 km du sol pour la filmer durant l'atterrissage qui est marqué par les figures faites entre la navette et l'avion. Bourane touche doucement la piste d'atterrissage, déploie ses parachutes, se fait survoler par l'avion d'accompagnement avant de s'immobiliser en même temps que lui.