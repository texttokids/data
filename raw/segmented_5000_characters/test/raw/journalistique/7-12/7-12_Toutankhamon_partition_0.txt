N°99 - 29 mars au 4 avril 2019
Toutânkhamon
Bien qu'il ait failli être rayé de l'histoire, Toutânkhamon fait aujourd'hui partie des pharaons les plus célèbres. Une partie du trésor découvert dans son tombeau est exposée en ce moment à Paris. Pendant des semaines, des dizaines de milliers de personnes vont se presser pour admirer ces objets incroyables, présentés en France pour la première fois depuis 50 ans. Qui était Toutânkhamon ? Pourquoi la découverte de son tombeau a-t-elle été si importante ? Comment fonctionnait la civilisation égyptienne ? Accompagne-moi à la découverte de l’Egypte ancienne !

Addison, 12 ans, est fan d’Égypte antique

Addison avait seulement 4 ou 5 ans quand elle a demandé à sa maman de lui acheter son premier livre sur l’Egypte. Elle se rappelle : «Je ne pouvais même pas encore lire, mais je me souviens que j’aimais beaucoup les images parce que c’était très beau et très différent de ce que je voyais d’habitude et donc ça a capté mon attention.»
Cette passionnée d’Egypte de 12 ans a lu beaucoup d’ouvrages sur son sujet préféré. Que ce soient des livres d’histoire, comme ceux qu’elle trouve dans les musées, ou bien des romans. Récemment, elle a lu Mort sur le Nil, un roman policier d’Agatha Christie.
Elle aime tellement explorer sa passion qu’un jour, alors qu’elle visitait la section sur l’Egypte ancienne au musée du Louvre, à Paris, ses parents ont dû la forcer à partir. Ils n’avaient plus beaucoup de temps et sa sœur s’ennuyait trop !
Si la jeune fille est passionnée par toute la civilisation égyptienne, elle est particulièrement intéressée par le processus de momification. C’est-à-dire les traitements spéciaux qui étaient faits sur le corps des Egyptiens quand ils mouraient.
Addison rêve d’aller découvrir la civilisation égyptienne sur place. Depuis longtemps, son papa lui a promis un voyage très spécial pour ses 13 ans (son anniversaire est le 25 avril !). Au début, elle souhaitait aller à Paris. «Mais maintenant que je suis déjà allée à Paris, je veux aller en Egypte voir le Nil, voir les pyramides et découvrir ce que j’ai vu dans les livres», dit-elle.
Mougins, la ville où habite Addison