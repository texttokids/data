Jeudi 5 octobre 2017
Pour avoir un joli sourire, il faut prendre soin de sa bouche et de ses dents.
Mais parfois cela ne suffit pas. Il faut aussi souffrir un peu.
En Islande, dans la ville d'Isafjörður (nord ouest), un passage pour piétons très particulier vient d'être installé. Ce dernier crée une véritable illusion d'optique : il donne l'impression qu'il est en relief.
Pourtant, il s'agit d'un passage pour piétons comme un autre, mais qui a été dessiné façon street art [art de la rue]. Une idée qui a pour but de faire ralentir les automobilistes, surpris par cette vision. Malin et utile !
Les dents sont des outils indispensables dans la vie.
Elles permettent de mâcher sufisamment les aliments pour les blés et les digérer plus facilement. Elles sont également utiles pour parler correctement. Des belles dents permettent enfin d'avoir un joli sourire et ainsi, une bonne image de soi.

En quoi consiste votre travail ?
Le travail d'un orthodontiste est de remettre la bouche en état.
Cela consiste à redresser les dents, à les aligner lorsqu'elles se chevauchent (quand l'une pousse sur l'autre) ou à élargir la mâchoire afin de permettre une bonne croissance, de bien mâcher ou de bien respirer.
Un taxi-drone a réussi son fer essai dans le ciel : de Dubaï (Émirats Arabes Unis).
Il est à mi-chemin entre un drone et un hélicoptère.
L'entreprise allemande e-volo souhaite en faire l'un des modes de transport de demain.
Il permettra de couvrir des trajets ne dépassant pas la demi-heure.
Il suffira d'un simple application mobile pour contacter drone le plus proche de sa position.
Ce n'est donc pas que pour faire joli... Non, l'orthodontie est une discipline médicale [pour être en meilleure santé] qui permet d'avoir une bonne croissance de la mâchoire et de la face [qu'elles grandissent bien].
Ce n'est pas que pour faire joli, pour avoir un beau sourire, même si cela aide aussi.
Pourquoi les jeunes sont-ils plus concernés ? C'est à ce moment que le visage se construit, que la croissance se fait, que l'on peut le mieux agir. Si on attend plus tard, c'est plus compliqué. Même si on peut encore le faire quand on est adulte.

C'est sur l'île de Bali (Indonésie), endroit incontournable pour les surfeurs qu'a été inauguré un ponton flottant.
Il fait 30 mètres de long pour un poids de 500 kilos.

Il arrive parfois les dents poussent de travers parce qu'elles manquent de place ce que la mâchoire n'a pas grandi suffisamment. Dans ces cas-là, il est nécessaire faire appel à un spécialiste : un orthodontiste. Le médecin corrige la position des dents ou de la mâchoire à l'aide d'un appareil dentaire, qu'il faut porter durant plusieurs mois, parfois plusieurs années. Une période pas très agréable, si nécessaire.

On ne va pas chez l'orthodontiste  pour avoir un plus beau sourire. C'est d'abord une question de santé. Porter un appareil aide à avoir une meilleure hygiène, à mieux parler ou encore à mieux respirer.
« Une mâchoire trop étroite entraîne des problèmes pour respirer, notamment la nuit. Des dents trop en avant provoquent quant à elles des problèmes pour parler ou risquent de se casser si on tombe en avant. Avoir les dents bien rangées permet de mieux mâcher, mais aussi de mieux les brosser et donc d'éviter les caries » explique le Dr Claude Bourdillat-Mikol.


Avec lui, plus besoin de ramer, et encore moins de réussir son take-offerte [moment où le surfeur se lève sur la planche pour démarrer sur la vague].
Telle une rampe de lancement, ce pont artificiel permet aux surfeurs d'attraper une vague en plein vol.

Espace Objectif Lune.
Des chatons des sables filmés pour la 1re fois.

Beaucoup astronautes rêvent d'explorer la planète Mars.
En attendant que le rêve devienne réalité, ils pourront toujours s'entraîner en habitant sur la Lune.
Ces habitants feraient fondre la glace des pôles pour avoir de l'eau et mangeraient les plantes cultivées sur le sol lunaire. Ils exploiteront surtout les richesses du sol comme le basalte, une roche volcanique utilisée dans la fabrication des satellites.
Un objectif «réalisable », selon les experts de l'ESA.
A condition toutefois de trouver l'argent nécessaire.