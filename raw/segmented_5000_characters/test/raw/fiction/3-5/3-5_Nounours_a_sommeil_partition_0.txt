C'est le soir.
Edouard est en pyjama.
Il joue avec son ours.
Edouard bâille, bâille, bâille.
Et Nounours a l'air fatigué.
Edouard prend Nounours sur ses genoux.
Il dit tout bas : Tu as sommeil mon Nounours. Viens te coucher !
Edouard couche Nounours doucement.
Il grimpe sur le lit.
Edouard est sous la couverture.
Il est à côté de Nounours.
Il ferme les yeux.
Annie vient éteindre la lumière.
Elle fait un bisou à Nounours, elle fait un bisou à Edouard : Dormez bien tous les deux !