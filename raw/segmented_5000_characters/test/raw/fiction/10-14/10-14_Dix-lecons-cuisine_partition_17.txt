Clara a affiché sur le mur une mappemonde. Et sorti de son sac un paquet de fiches en carton. 
- J’ai fait travailler Simon, qui vous transmet un grand bonjour…
- Est-ce qu’il viendra un jour ? demande Alice.
- Dès que le procès sera fini, c’est promis…
Elle étale devant elle les fiches. 
- Simon a inscrit des noms de plat d’origine étrangère que nous avons adoptés dans notre alimentation. Vous savez, la cuisine a toujours été un moyen de voyager. Vous vous souvenez du poème de Simon, le premier jour ? « Dans ma cuisine, je parle toutes les langues… » C’est vrai. D’ailleurs, on parle et on mange avec la bouche, non ? Et le mot langue…
- On s’embrasse aussi avec la bouche, dit Noah. Et avec la langue.
Pedro éclate de rire. Farid sourit. Hector fronce les sourcils. Alice hoche la tête. Rose rougit. Kev et Marion se regardent.
- Je ne savais pas que tu étais expert en amour, cher Noah, dit Clara.
Et là, tous sourient. Même Noah.
- Je vais vous distribuer ces petits cartons, reprend Clara. Sur chacun il y a un nom de plat ou d’ingrédient qui vient d’ailleurs. Je vous demande de coller l’étiquette sur la carte que j’ai affichée sur le mur… Par exemple, je tire un carton… sushi… tu vas le placer où, Noah ?
- Sur la Chine ! 
- Raté, dit Hector, les sushis, ça vient du Japon.
- Ok, dit Clara, vous avez compris. Parfois, il y a plusieurs cartons avec le même mot. Cela signifie qu’on peut le trouver dans plusieurs pays. Je vous préviens que je n’ai pas choisi de plats italiens ou français, parce que ce serait trop facile…
(NB : ici représenter une mappemonde afin que le lecteur puisse faire l’activité. La « solution » sera présentée sur le site).
Haddock (Angleterre), Sashimi (Japon), Bifteck (Angleterre), Sandwich (Angleterre), Merguez (Maghreb), Couscous (Maghreb), Gaspacho (Espagne), Chips (Angleterre), Cake (Angleterre), Harissa (Maghreb), Pirojki (Russie), Tofu (Chine et Japon), Paella (Espagne), Goulasch (Hongrie), Paprika (Hongrie), Kouglof (Allemagne), Rollmops (Allemagne), Apfelstrudel (Autriche), Sushi (Japon), Kebab (Turquie), Quinoa (Pérou), Ketchup (Indonésie et Etats-Unis), Blini (Russie), Samoussa (Inde et Pakistan), Taco (Mexique), Tortilla (Mexique), Tortilla (Espagne), Nem (Vietnam), Tsatziki (Grèce), Moussaka (Grèce et Turquie), Chili con Carne (Etats-Unis), Empanadas (Espagne et Amérique du Sud).
Il faut plus d’une demi-heure et pas mal de recherches sur Internet pour aboutir. 
- Ça m’a donné faim, déclare Farid en se tapant sur le ventre.
- Alors, on te suit à la cuisine, dit Clara. C’est toi le chef aujourd’hui. À ton tour de présenter ta recette. 
- Ben… euh…, bredouille Farid. Je sais pas par où commencer…
- Comment s’appelle la recette ? demande Rose.
- En arabe ou en français ?
- Donne-nous d’abord le nom arabe, on l’ajoutera à notre carte, dit Clara.
- Hé bien, ça s’appelle « baghrir », c’est une recette marocaine. On l’a…
- Et en français ? demande Noah.
- La « crêpe mille trous ».
- Une crêpe à trous ? s’étonne Hector
- Oui, tu verras il y a plein de trous sur le dessus quand on la cuit, c’est magique. 
- Et qui t’a appris à les faire ? demande Alice.
- Je sais plus. Mon père, je crois. A la maison, tout le monde en fait, c’est pas difficile. On les prépare souvent pendant le Ramadan, mais aussi le reste l’année … Mais parfois, on fait les crêpes françaises...
- Justement dit Clara, je propose que nous fassions deux groupes. Un sous la direction de Farid préparera des baghrir (*). L’autre avec moi fera des crêpes françaises, comme dit Farid. Sauf que les crêpes, ce n’est pas spécialement français. C’est juste un type de galette, comme il y en a un peu partout dans le monde…
- On commence ? l’interrompt Noah.
- Mais t’es pas poli ! s’écrie Pedro.
Clara éclate de rire, et une mèche de son chignon se libère.
- Il a raison, on n’est pas à l’école. Allez, au travail, cuisiniers !

A la fin de la matinée, les baghrir ont été dévorés avec un bel appétit. Mais il reste une belle pile de crêpes « françaises ». 
- Je vais vous donner un devoir à la maison, dit Clara. Vous connaissez les « mille crêpes » ? 
- C’est pas comme les millefeuilles ? demande Alice.
- Pas tout à fait. Il s’agit simplement de gâteaux de crêpes. On va former deux groupes, le premier trouvera une recette de mille crêpes salée et l’autre une recette sucrée.
Les groupes sont tirés au sort : le premier comprend Noah, Alice, Farid et Marion. Le second Rose, Pedro, Kev et Hector. A la surprise de Clara, personne ne proteste.
Rose attend que les autres sortent pour s’approcher de Clara et lui parler à mi-voix. Clara, tête penchée vers elle, l’écoute tandis qu’elle enveloppe dans du papier aluminium trois baghrir destinés à Simon.
- Mais non, emmène-les ici demain ! dit-elle finalement. Je t’assure, ça ne pose pas de problème. Allez, va, je téléphonerai à tes parents.
Elle embrasse Rose et, main sur le ventre, la regarde s’éloigner.



Interlude