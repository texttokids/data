Clara est de retour. Ils lui font une haie d’honneur quand elle sort de sa voiture et se dirige vers l’entrée de l’école. Farid lui porte son panier, Noah son grand sac en toile.
Arrivée à la bibliothèque, Clara va directement vers le panneau punaisé dans le fond. Tantôt s’approchant pour déchiffrer une écriture maladroite, tantôt reculant pour apprécier l’effet d’ensemble, elle lit, commente, questionne. Et puis elle ramène la petite troupe vers la table de réunion.
- Encore désolée pour hier, mais je vois que vous vous en êtes très bien sortis sans moi.
- Tu avais quoi ? demande Noah.
Clara, les regarde, hésitante.
- C’est à cause du bébé ? demande Rose.
Clara sourit. Et soudain, oui, elle est radieuse.
- Tu as deviné ?
Rose rosit. Décidément, ça lui va bien.
- Oui. Je me souviens quand maman attendait mes petits frères…
- Mais alors, tu vas avoir un bébé ? demande Pedro, interloqué. Avec Simon ?
Clara se mord les lèvres pour ne pas rire. 
- C’est un garçon ou une fille ? demande Alice. Vous allez l’appeler comment ?
Etc. Clara doit élever la voix pour rétablir un peu d’ordre.
- Maintenant que vous êtes au courant, on en vient au thème d’aujourd’hui.
- Les empanadas, dit Pedro.
- Oui, mais pas seulement. Tu expliques d’abord ce que c’est, les empanadas, et d’où tu as la recette…
Pedro se redresse sur sa chaise.
- Ben, ça vient du Pérou. C’est là que je suis né. Mais je m’en souviens pas, parce que j’étais trop petit quand mes parents sont venus me chercher. 
- Juste une précision, Pedro, dit Clara. On trouve des empanadas partout en Amérique du Sud, et aussi en Espagne, mais je crois bien que ceux du Pérou sont les meilleurs…
- Avec mes parents, on parle souvent du Pérou, j’ai même commencé à apprendre l’espagnol à la MJC. Un jour, sur le marché à Vesoul, on a vu une dame qui vendait des empanadas. Sur une petite affiche, c’était marqué que ça venait du Pérou. On en a acheté et ma mère a cherché la recette. Depuis, on en fait souvent à la maison.
- Mais c’est quoi, les empanadas ? demande Noah.
- Tu connais les chaussons aux pommes ? répond Pedro. C’est un peu pareil, sauf qu’à l’intérieur, c’est de la viande, du poisson, ou des légumes. Et c’est pas la même pâte, non plus.
- En fait, explique Clara, les empanadas font partie de la Finger Food.
- Les trucs qu’on mange avec les doigts, traduit Hector.
- Exact. Comme les sandwichs, les pizzas, les tacos…
- Les samoussas, les rouleaux de printemps, dit Alice.
- Les hamburgers, dit Noah.
- Les briks et les crostini, dit Farid.
- Les croque-monsieur, dit Rose
- Et cetera, dit Clara. Alors le programme de ce matin : d’abord atelier empanadas avec Pedro.
- Mais on les mange pas tous, on en garde pour Riana ! Elle arrive ce soir à cinq heures…
- Bien sûr, Pedro. Ensuite, vous rechercherez des idées pour le pique-nique d’aujourd’hui. La règle, c’est : que des plats qui se mangent avec les doigts. Les autres groupes du centre aéré seront affamés, alors vous devez être créatifs et productifs ! On y va ?

Trois recettes différentes d’empanadas et dix variétés de finger food. Clara est satisfaite (et épuisée). Les cuisiniers sont très contents d’eux (et rassasiés à force de goûter ceci ou cela et encore ci et encore ça). Alice et Rose ont une idée : écrire la liste des dix variations réalisées et demander aux invités du pique-nique de deviner la recette correspondante. 
- Génial, dit Clara, mais inventez des dénominations originales.
- Nominations ? fait Noah. Ça veut dire quoi ?
- Le nom de la recette, idiot, dit Alice.
- Idiote toi-même, répond Noah du tac au tac.
 C’est le seul incident de la matinée.

Voici la liste des plats présentés ce matin-là :

« Mini-sandwich de la mer
Tramezzino rigolo
Samoussa de la basse-cour
Hamburger du jardinier
Canapé des jours de pluie
Crostini de bonne humeur
Wrap qui plaît à la vache et à son veau
Croque-bébé d’été
Rouleau d’été (sucré)
Petit chou très chou (sucré) » (Cher lecteur, à ton tour, tu peux envoyer tes propositions de recette correspondant à ces dénominations au site : )

A la fin du pique-nique, Clara a rassemblé le groupe.
- Pour fêter la fin de l’atelier, Sylvie et moi avons pensé organiser une fête dans le parc de la mairie pour tous les participants du centre aéré et leurs amis. L’idée est simple : tous ceux qui veulent venir cuisiner apportent une recette et les ingrédients nécessaires. Il y aura des feux de cuisson, des frigos et des ustensiles de cuisine. Je rappelle à Kev et Noah qu’ils n’ont pas présenté leur recette…
- Ah, j’ai oublié, dit Noah en mettant la main sur sa bouche.
Kev ne réagit pas.
- Si vous avez des questions ou des idées pour l’organisation, vous pouvez m’appeler sur mon portable, dit encore Clara. A demain !



Interlude