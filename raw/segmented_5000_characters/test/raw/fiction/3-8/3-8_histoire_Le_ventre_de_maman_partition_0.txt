« Pourquoi Maman a un gros ventre ? »
« Parce qu’elle va se transformer en ballon et s’envoler ? »
« Parce qu’elle veut faire la toupie sur le ventre ? »
« Parce qu’elle mange trop de chocolat ? »
« Parce qu’elle cache tous mes cadeaux de Noël dedans ? »
« Parce qu’elle a avalé le bocal de Bulle mon poisson rouge ? »
« Mais non Léon, dans le ventre de Maman il y a un tout petit bébé. C’est ta petite sœur qui va arriver dans quelques semaines. »
« Elle est petite comment ? - Elle est grosse comme ta tête, mais elle va grandir, et quand elle sera prête, elle sortira. »
« Qu’est-ce qu’elle fait dedans pendant tout ce temps ? - Elle dort beaucoup, elle reste au chaud, et elle écoute ce qui se passe. »
« Pose tes mains sur mon ventre et colle ton oreille. Tu peux lui parler, elle t’entend déjà. » - Coucou bébé, je vais te raconter des histoires et tu ne t’ennuieras pas !