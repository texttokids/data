Qu’est-ce qui se passe quand je dors ?
Tous les gens de la terre font dodo aussi ?
Les animaux aussi dorment pendant que je dors ?
Mon corps s’envole sur un nuage ?
Quand tu dors, il y a d’autres personnes et d’autres animaux qui dorment, mais pas tout le monde en même temps.
Tes yeux se ferment.
Tu restes allongé. Ton petit corps se détend et se repose, pour reprendre des forces avant de retourner courir. Et surtout, tu fais de jolis rêves !
Les animaux, les adultes, les enfants, tout le monde doit dormir chaque jour pour reprendre des forces et être en forme le matin.
Alors je vais beaucoup dormir cette nuit! Pour être plus fort que mes copains au foot demain !