Petit panda se sent tout triste aujourd’hui. Il pleure à gros sanglots. -Bouhou ! Je suis bien malheureux !
-Et pourquoi ne mangerais -tu pas un bon gros poisson ? suggère la grue gloutonne.
Mais petit panda pleure de plus belle. -Bouhou ! Je suis bien malheureux !
-Ssssa va aller, susurre le boa hésitant, sssois patient, ssssss….
Mais petit panda pleure de plus belle. -Bouhou ! Je suis bien malheureux !
-Allons, fais comme moi, prends un peu de hauteur, enseigne le sage éléphant.
Mais petit panda pleure de plus belle. -Bouhou ! Je suis bien malheureux !
-Grrr, c’est pas bientôt fini ce raffut ! rugit le tigre ronchon.
Mais petit panda pleure de plus belle. -Bouhou ! Je suis bien malheureux !
-Blop blop blop, bloubloute la carpe Koï un peu coi.
Mais petit panda pleure de plus belle. -Bouhou ! Je suis bien malheureux !
Maman panda, elle, a compris. Tout ce dont petit panda a besoin, c’est d’un câlin. -Oh, comme je suis heureux ! dit-il en souriant.