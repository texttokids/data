Nous formions maintenant une vraie bande, Craquelin, La Bouillie, Bourguignon, Janvier et moi, pas une bande de grinches, une bande de bons zigs, comme disait La Bouillie.
Grâce à l’argent de Saint-Just, j’avais aussi acheté des béquilles à Craquelin et il s’y suspendait pour faire le chemin de sa chambre à la cour. J’avais mis un banc contre le mur et le petit s’y adossait. Nous apportions alors nos chaises, et La Bouillie se faisait professeur d’arguche. Elle-même savait à peine lire, et comme Janvier s’étonnait de ce qu’elle ait pu rédiger l’avis de recherche, elle lui avoua en rougissant qu’elle avait payé un écolier pour écrire sous sa dictée. Janvier, en rougissant à son tour, lui proposa d’échanger les leçons d’arguche contre des leçons de français. Et ainsi fut fait. Craquelin et Bourguignon, qui savaient tout juste ânonner, suivirent aussi les leçons.
Pendant ce temps, je sortais mon chourin de ma poche et je taillais dans des bouts de bois des sifflets et des petits bateaux pour amuser Craquelin.
Janvier finit par raconter ses peines de cœur à La Bouillie, que ce genre d’histoire intéressait plus que moi. Elle trouva même une ruse pour lui permettre d’échanger du courrier avec mademoiselle Lucie. La Bouillie connaissait la jeune lingère qui lavait, raccommodait et repassait les chemises et les jupons des Desfontaine. Désormais, c’était La Bouillie qui les rapportait dans un grand panier au fond duquel la bonne de mademoiselle Lucie trouvait une lettre de Janvier. La réponse de Lucie partait sous le linge sale, et ainsi de suite. Mais, s’il voulait un jour épouser mademoiselle Desfontaine, Janvier devrait se trouver une situation.
Il avait d’abord tenté de faire publier ses poésies, mais les éditeurs lui avaient claqué la porte au nez. Il s’était donc tourné vers le journalisme et avait écrit quelques articles sur des comédies qu’il avait vues. Puis à force de regarder des pièces, il eut l’idée d’en écrire une, Papa ne veut pas, où il faisait rire les spectateurs de ses propres malheurs.
Dès que le directeur du théâtre lui donna l’argent des premières représentations, il alla le boire puis rentra chez lui en chantant « Poule en haut, poule en bas ».
Un soir, sans crier gare, Jean Saint-Just entra dans ma chambre avec son passe-partout. Craquelin dormait déjà, un pantin de bois de ma fabrication serré contre son cœur.
Sur un signe que me fit Saint-Just, je le rejoignis au-dehors.
- J’ai parlé de toi à un grinche pour un vol à la venterne… Tu sais ce que c’est ?
- Par la fenêtre ?
Saint-Just acquiesça puis me donna quelques précisions.
On allait cambrioler un premier étage d’accès facile par la cour. Les propriétaires, un vieil homme et sa fille, avaient le sommeil lourd.
- J’ai dit à Bobino (c’est le nom du grinche) que tu étais un ramoneur savoyard et que tu n’avais pas ton pareil pour l’escalade. Il t’attend au troisième étage du Lapin.
- Maintenant ? demandai-je, pris de panique.
- Troisième étage, répéta seulement Saint-Just.
Quelque chose de plus fort que moi décidait de ma vie : j’étais un grinche, c’était mon destin.
Sur la pointe des pieds, je grimpai l’escalier en colimaçon du Lapin Volant. Au premier étage, un nuage de fumée flottait déjà au-dessus des buveurs. Au deuxième étage, une partie de cartes était engagée entre quatre joueurs. J’eus le temps de reconnaître de profil un des joueurs avant qu’il ne m’aperçût. Je dégringolai les marches jusqu’en bas, terrorisé.
C’était Riflard ! Il me semblait aussi avoir reconnu Picpoc qui jouait aux cartes contre lui. Je retournai dans la cour où Saint-Just faisait les cent pas.
- Tu as déjà vu Bobino ? s’étonna-t-il.
- Non… Ne vous fâchez pas ! Je n’ai pas pu parce qu’au deuxième il y a un joueur de cartes qui s’appelle Riflard, et j’ai eu peur. C’est un… un mauvais homme.
Le visage de Saint-Just se crispa si fort que ses favoris roux commencèrent à se décoller.
- Riflard est au Lapin Volant ! s’exclama-t-il. Il a fini son temps au bagne, alors ?
Il me regarda avec une lueur sauvage dans les yeux :
- Et pourquoi dis-tu que c’est un « mauvais homme » ?
- Parce qu’il fait partie de la bande du cocher et de Roland.
- Comment le sais-tu ? Tu as des preuves ?
- Non, non, non, fis-je en reculant dans l’ombre.
- Alors, tu accuses au hasard ! s’emporta Saint-Just.
Je crus qu’il allait me secouer par le collet comme il l’avait déjà fait. Au lieu de cela, il prit une inspiration et fit apparaître une pièce d’or au bout de ses doigts :
- Tiens, voilà pour toi. Je te ferai rencontrer Bobino une autre fois.
L’autre fois ne se fit pas attendre. Dès le lendemain, Saint-Just vint me chercher dans ma chambre. Il m’empoigna sans me laisser le temps de réveiller Craquelin et, dans la cour, il me tendit le plan de l’appartement que nous allions cambrioler :
- Tu passeras par la fenêtre de la cuisine. Par ces temps de chaleur, la cuisinière la laisse ouverte. Tu remonteras le couloir jusqu’à l’entrée et tu nous ouvriras.