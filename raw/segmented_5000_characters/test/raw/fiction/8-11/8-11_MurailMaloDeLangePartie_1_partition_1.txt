À la mort de leur papa, les demoiselles de Lange, que je n’appelle pas encore tante Mélanie et tante Amélie parce qu’elles ne m’ont pas adopté à ce moment de mon récit, s’aperçurent de deux choses. Une, qu’elles étaient trop vieilles pour faire le bonheur d’un homme. Deux, qu’elles étaient assez jeunes pour faire le bonheur d’un enfant. Accompagnées de Mariette, leur servante, elles se rendirent chez l’abbé Pigrièche qui dirigeait un hospice pour les orphelins mâles et femelles, rue des Ursulines à Tours.
Mesdemoiselles de Lange expliquèrent dans le détail à l’abbé Pigrièche ce qu’elles voulaient comme genre d’orphelin : pas un nourrisson qui risquait de claquer trop vite (je crois qu’elles dirent la chose autrement) et de leur faire beaucoup de chagrin, mais plutôt un enfant de deux ou trois ans, en bonne santé, propre, sage, intelligent, sans croûtes sur la figure, blond avec des yeux bleus, et bien sûr :
- Une fille !
- J’ai un enfant de deux ans, répondit l’abbé, blond avec des yeux bleus…
- Pas de croûtes ? s’inquiéta Amélie, qui était la cadette.
L’abbé fit signe que l’enfant n’était pas atteint de cette infirmité, mais d’une autre, plus gênante :
- C’est un garçon.
Les demoiselles eurent l’air tellement consterné que sur le moment l’abbé n’insista pas. Il leur fit faire la tournée de la pouponnière côté filles. Il y avait ce jour-là une nouvellenée qui n’allait pas faire la journée, une grosse braillarde aux joues rouges et une idiote à quatre pattes. Profitant de la déception des demoiselles, l’abbé Pigrièche leur proposa d’aller jeter un coup d’œil sur le petit garçon d’à côté.
- Oh, c’est bien inutile, protesta Mélanie, qui était l’aînée.
- Mais puisque nous y sommes, dit-il fermement.
Il montrait la porte qu’il suffisait de pousser, et les demoiselles de Lange, qui avaient reçu une bonne éducation, n’osèrent pas lui dire non.
De l’autre côté de la porte, je dormais bien tranquille dans mon berceau avec un collègue à ma droite et un collègue à ma gauche parce qu’il y avait crise du logement chez les bébés garçons.
- C’est celui du milieu, fit l’abbé Pigrièche, assez sûr de l’effet que je produirais.
J’étais blond comme il l’avait dit, avec une peau de pêche, des cils de soie, des oreilles de satin, et beau comme les Amours tout nus que dessinent les peintres en haut des tableaux, sauf que j’étais habillé. L’inconvénient quand on écrit ses mémoires, c’est qu’on est obligé de dire du bien de soi. Mais la preuve que c’était la vérité, c’est que mademoiselle Amélie s’exclama :
- Quel petit ange !
- On le prend toujours pour une fille, glissa l’abbé.
- Cet enfant a-t-il un nom ? s’informa mademoiselle Mélanie.
- C’est souvent le cas pour les enfants… Il s’appelle Malo.
Mademoiselle Mélanie eut un haut-le-cœur :
- C’est un nom chrétien ?
- On en a même fait une ville, répondit l’abbé.
Il y eut un silence puis mademoiselle Amélie (la cadette, mais c’est la dernière fois que je le précise) se pencha sur mon berceau et prononça distinctement :
- Malo… Malo de Lange.
Et ces mots me firent ouvrir les yeux.
- Bleus, Mélanie ! Regardez comme ils sont bleus !
Je sais bien que je ne peux pas me souvenir de cette scène puisque je n’avais pas deux ans. Mais elle m’a été racontée par la suite. Quand mademoiselle Amélie fit un geste vers moi pour m’enlever du berceau, Mariette, la servante, s’interposa en disant qu’on ne savait rien de moi, que je n’étais peut-être même pas complètement orphelin, et que j’avais sûrement des maladies cachées.
- Je vais vous dire tout ce que je sais, promit l’abbé.
Une dame, le visage caché sous une voilette, m’avait confié en nourrice à une paysanne des environs. Mais un an plus tard, la paysanne mourut, et son mari, qui ne recevait plus d’argent pour mon entretien, m’apporta à l’hospice des Ursulines. Il ne savait pas le nom de ma mère et pensait que j’étais « un enfant de l’amour ». C’est une façon de dire que personne ne veut de vous.
- Qui lui a donné ce nom de Malo ? questionna mademoiselle Mélanie.
- La dame à la voilette.
- Et il n’avait pas sur lui quelque chose qui permette de l’identifier, comme une croix en or ou des langes en dentelle ?
- Pas d’or, pas de dentelle.
- Et le mari de la paysanne, on pourrait l’interroger ?
- On pourrait, oui, mais il est mort.
L’abbé oubliait de révéler à mon sujet une chose très importante. Je pourrais avouer tout de suite ce que c’était, mais je garde toujours le meilleur pour la fin, comme disait le cannibale en se mettant la cervelle de côté.
Le lendemain, le jardinier de l’hospice des Ursulines me conduisit chez les demoiselles de Lange, rue des Cerisiers.
- Eh bien, c’est pas trop tôt, dit-il en me flanquant dans les bras de Mariette.
Du moins, c’est ainsi que Mariette me présenta mon arrivée, mais elle ne m’a jamais aimé. Elle me fit immédiatement prendre un bain dans un grand baquet avant de me remettre à mes tantes adoptives. Elle me dévêtit donc de la tête aux pieds, et c’est alors qu’elle aperçut le signe dont l’abbé Pigrièche n’avait pas voulu parler.
- Jésus Marie ! s’écria-t-elle en me lâchant dans le baquet. C’est quoi c’est-ti, cette horreur ? Mam’zelle Mélanie, à la garde, au secours !
Je me mis à hurler à mon tour. Elle m’attrapa sous le bras et partit en courant vers le petit salon où mes bienfaitrices prenaient le thé.
- Mon Dieu, qu’est-il arrivé à ce pauvre enfant ? s’effraya Amélie, car je hurlais à pleins poumons. Vous l’avez ébouillanté, Mariette ?
- Ça vaudrait mieux, répondit la servante en me posant devant les demoiselles de Lange. On verrait plus c’te horreur !
Comme j’étais tout nu, les demoiselles comprirent de travers.
- Non, pas ça, fit Mariette en me retournant brutalement.
Mes hurlements de rage ne couvrirent pas les exclamations d’effroi de mes bienfaitrices :
- Mon Dieu, qu’est-ce que cela ? Une tache de naissance ? Une brûlure ?
Sur mon épaule droite, je portaise porte toujourse dessin d’une fleur de lys qu’un fer rouge m’avait entré dans la chair.
- Rhabillez-le, ordonna mademoiselle Mélanie, nous allons à l’hospice.
Elle s’imaginait qu’elle pourrait me retourner comme un objet défectueux, mais c’était compter sans le bon abbé Pigrièche, protecteur des orphelins.
- En effet, dit-il après avoir écouté les demoiselles, Malo a une petite brûlure. Mais elle ne se voit pas quand il est habillé.
- Une brûlure, se récria Mélanie, mais c’est la marque du bagne !
- Un bagnard de deux ans ? Voyons, mademoiselle, ça n’existe pas. Et la fleur de lys, c’est aussi l’emblème de la royauté. C’est peut-être hum… un secret d’État. Cet enfant est si beau, si hum… aristocratique.
Bref, l’abbé n’avait pas du tout l’intention de me récupérer parce que donner, c’est donner, et reprendre, c’est voler, comme disait le boucher en laissant son couteau dans le ventre de sa femme.
Quand on écrit ses mémoires, on ne fait pas que raconter ses souvenirs, on retrace aussi son cheminement moral.
Je vais donc sauter quelques années parce que, à deux ans, du point de vue moral, j’étais aussi plat qu’une limace qui viendrait de se faire marcher dessus. Mais à cinq ans, je distinguais déjà le bien du mal. Le bien était un bonhomme en pain d’épice que tante Amélie achetait le dimanche à un vendeur des rues pour récompenser une semaine où je n’avais pas fait de sottises. Le mal était un cabinet noir dans lequel tante Mélanie m’enfournait comme le Boulanger* fait avec ses clients. (* Le Boulanger était le nom du diable dans la langue des voleurs.)
Malgré les efforts de mes tantes pour me donner une bonne éducation, je commis mon premier vol à sept ans.
C’était un pot de confiture que je vidai avec l’aide de La Bouillie, une personne dont je parlerai plus tard. Après avoir raclé jusqu’au couvercle, j’emplis le pot avec de la terre et le replaçai en haut de l’armoire. Au bout de quelques jours, Mariette, en fourrageant dans ses étagères, aperçut ce pot noirâtre qu’elle alla porter à mes tantes en réclamant justice.
Tante Mélanie m’enfourna dans le cabinet sans rien à manger jusqu’au soir. L’apparition d’un bonhomme en pain d’épice dans l’entrebâillement de la porte à l’heure du dîner porta un coup à mon sens moral. C’était comme si le bien venait tenir compagnie au mal.
Quand mes tantes invitaient l’abbé Pigrièche, elles se plaignaient de mes bêtises.
- C’est bien tout ? disait-il comme le monsieur à qui sa femme venait de donner des quintuplés.
- Peut-être faudrait-il lui apprendre à lire le caté-chisme ? s’informa un jour tante Mélanie.
- Vous pourriez même lui apprendre à lire tout court.
Il se tourna alors vers moi et me demanda :
- Est-ce que tu sais seulement écrire ton prénom ?
On me donna une plume, de l’encre et du papier. En m’appliquant de toutes mes forces, je réussis à tracer les trois premières lettres de mon prénom.
- C’est bien tout ? répéta l’abbé.
- C’est tout, dis-je.
L’abbé toussota et ajouta un « O ».
- Petit malin, dit-il en m’observant derrière le buisson noir de ses sourcils.
En quittant les demoiselles de Lange, il leur recommanda de ne jamais me laisser quitter la maison sans surveillance, ce qui m’amène à parler du trou que je fis dans le mur de notre jardin.