Janvier m’avait assuré que monsieur Desfontaine sortait tous les après-midi. Mais j’avais à peine posé ma question que deux hommes parurent devant moi.
- Que se passe-t-il, Lison ?
C’était monsieur Desfontaine qui n’était pas sorti du tout.
- Mais que fait ce petit dégoûtant sur mon palier ? explosa-t-il en m’apercevant. Veux-tu bien te sauver, vaurien, chenapan !
- Méfiez-vous, ces ramoneurs sont souvent des voleurs, fit l’autre homme. Il n’a touché à rien ?
- Eh bien, répondez à monsieur le baron, Lison !
Je n’attendis pas la réponse de la petite bonne, car je venais d’être foudroyé par la plus terrible des révélations. Je savais qui était le baron de Carabas et il ne fallait absolument pas qu’il me reconnût sous mon déguisement. Je dévalai l’escalier quatre à quatre, laissant tomber en route mon échelle, ma corde, ma raclette et mon sac de suie. Dieu merci, je n’avais pas de marmotte, je l’aurais perdue aussi !
Une fois dans la rue, je repris peu à peu mes esprits. Mais j’avais une formidable nouvelle à annoncer à Janvier et à La Bouillie que je réunis au Lapin Volant :
- Le baron de Carabas, c’est Lamproie ! Il porte des habits à la dernière mode de Paris, et c’est le genre de chose qui vous change un homme, comme disait le monsieur qui se transformait en loup-garou tous les samedis. Mais je suis sûr que c’est lui !
- Et qui est ce Lamproie ? s’informa Janvier.
- Mon dab d’antan, c’t un grinche et un frimousseur, lui répondit La Bouillie.
Janvier n’avait pas encore eu le plaisir de parler avec La Bouillie et il lui demanda si elle était en France depuis longtemps. Je me chargeai de la traduction :
- Lamproie était le patron de La Bouillie. C’est un voleur et un tricheur.
- Mais c’est beau-papa qui va être heureux d’apprendre tout cela ! se réjouit François. Donc, il n’est pas plus baron que moi ?
- Plutôt moins.
Janvier se frotta les mains :
- Parfait ! Vous allez venir avec moi chez les Desfontaine pour démasquer le faux baron ?
Nous nous regardâmes, La Bouillie et moi, et fîmes non de la tête. Puis je m’éclipsai pour aller me laver dans le baquet de la cour.
Quelqu’un m’attendait dehors, quelqu’un qui m’attrapa au passage par l’épaule.
- C’est toi, pégriot ? s’étonna Jean Saint-Just qui me reconnaissait à peine sous ma couche de pommade. Mais qu’est-ce que…
Il passa la main sur ma joue :
- Du noir de suageur !
- Non, non, du noir de ramoneur !
- Tu es ramoneur, toi ? Au mois de juin ?
- C’est pour rendre service à Janvier…
- Rendre service à janvier en juin ? !
- Mais écoutez-moi, monsieur Saint-Just ! Je me suis déguisé en ramoneur pour aller porter la lettre de Janvier à mademoiselle Lucie qui ne doit pas épouser le baron de Carabas parce qu’en fait il s’appelle Lamproie, et c’est un grinche.
Jean Saint-Just crispait les traits de son visage comme s’il souffrait en m’écoutant :
- Mais tu racontes n’importe quoi !
- Non, je vous jure, c’est la vérité. Le baron de Carabas est un menteur et un tricheur et un voleur. Il s’appelle Lamproie et il vient de Tours.
- Mais comment peux-tu savoir toutes ces choses ? Tu es passé par toutes les prisons ? Tu connais tous les grinches de France ?
Sa colère tomba aussi brusquement qu’elle était montée.
- Après tout, je m’en moque, marmonna-t-il.
Une pièce d’or brilla au bout de ses doigts, sortie de je ne sais où.
- Voilà pour toi. Tes renseignements étaient bons. Le cambriolage a eu lieu chez les Bonnechose.
Je compris que c’était ma part du butin et je n’eus pas la force de tendre la main.
- Vous avez partagé avec le cocher et Roland ?
Jean Saint-Just ricana :
- Non, je ne partage pas. Mais sois tranquille, tu ne reverras ni l’un ni l’autre au Lapin Volant. Jamais.
Un grand frisson me secoua.
- Qu’est-ce que…
Ma voix s’éteignit. À quoi bon demander des précisions ?
Roland et le cocher avaient été assassinés. Et aussi le pauvre gars qui faisait le guet.
- Alors, tu dis que Carabas s’appelle Lamproie, marmonna Saint-Just comme quelqu’un qui suit ses pensées.
- Mais vous n’allez pas le tuer au moins !
- Ce serait plus intéressant de le faire chanter, tu ne crois pas ?
Il m’attrapa par le collet :
- Je ne sais pas qui tu es, ni d’où tu viens, pégriot. Mais n’oublie pas : si tu trahis, il t’en coûtera une main.
Un éclair bleu était parti de ses yeux. Il lança la pièce d’or sur le pavé et quitta la cour par la porte du fond dont il avait la clé, tout comme la Perlouze. Je ramassai la pièce et la frottai contre mon pantalon comme si elle était souillée de sang. Bien sûr, Roland et le cocher avaient volé, torturé, peut-être tué. C’étaient deux méchantes gens.
Mais la Perlouze et Saint-Just étaient pires encore, et j’étais devenu leur complice. Soudain, je dressai l’oreille. Je venais d’entendre deux coups de sifflet en provenance de la rue François-Miron. C’était Jean Saint-Just qui appelait Picpoc.
Une semaine plus tard, Janvier vint un soir me retrouver dans ma cave. Il était dans une telle joie que je le crus ivre.