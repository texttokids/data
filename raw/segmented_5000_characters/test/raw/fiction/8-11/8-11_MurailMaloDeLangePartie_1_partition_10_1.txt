Je tressaillis en voyant surgir de l’escalier en colimaçon le marin ivre qui s’amusait l’autre soir à jeter des bancs en l’air. Sous son bonnet rouge, il avait un visage de bête fauve, d’énormes favoris roux et une cicatrice lui ouvrant la joue.
Il traînait la jambe droite après lui, comme les bagnards qui ont longuement tiré leur chaîne.
- T’es le pégriot ?
J’ôtai bien poliment ma casquette :
- Oui, monsieur Saint-Just.
Il sortit des poches intérieures de sa vareuse une bouteille de vin, un morceau de pain et un pâté, puis, sans me parler, me désigna un tabouret de la pointe de son couteau. Nous nous assîmes tous les deux et nous mangeâmes, toujours en silence. Quand ce fut fini, Saint-Just balaya les miettes d’un revers de manche puis grimpa sur la table. Du bout de son gourdin, il repoussa la trappe du plafond, et à la force des bras, se hissa au grenier.
- Arrive, m’ordonna-t-il.
Il m’attrapa par la main et me fit monter à mon tour. Il y avait là une table et cinq tabourets, mais il y avait aussi, peu visible dans l’ombre poussiéreuse, un vieux coffre en bois.
- Ils viendront ce soir, me dit-il. Quand t’entendras La Bouillie chanter « Lirlonfa maluré », tu te cacheras dans le coffre et puis t’attendras le temps qu’il faudra. Ils finiront par passer dans le grenier et ils parleront de leur affaire.
T’entraves l’arguche* ? (* Tu comprends l’argot ?)
- Comme un grinche.
- Alors, ouvre bien tes loches, et demain tu réciteras tout ce que t’auras retenu à la Perlouze.
- Oui, monsieur Saint-Just.
- Un conseil : éternue pas. S’ils te trouvent, ils te tuent.
Les larmes me sautèrent aux yeux, peut-être à cause de la poussière du grenier. Je me raclai la gorge avant de répondre une nouvelle fois :
- Oui, monsieur Saint-Just.
Quand la trappe se referma derrière lui, je compris mieux l’horreur de ma situation. J’étais enfermé pour tout l’après-midi, toute la soirée, toute la nuit, sans rien à boire, sans rien à manger, avec pour seule compagnie la cloche voisine sonnant l’heure, le quart et la demie. Ma prison était éclairée par un œil-de-bœuf qui donnait sur la cour. De là, en me contorsionnant, j’apercevais les marches qui menaient à notre cave. La reverrais-je un jour ? Et Craquelin, et Bourguignon, Janvier, La Bouillie, et tous ceux que j’avais laissés derrière moi et dont je m’apercevais que je les aimais ?
Je n’avais encore que douze ans, et pour dire les choses comme elles étaient, la plupart du temps, je ne pensais qu’à moi. D’ailleurs, une envie pressante m’y ramena. Que mes lectrices détournent un instant le regard, car je fus obligé de me soulager dans la cour par l’œil-de-bœuf. Ce fut ma seule distraction de la journée. Enfin, la lumière commença à baisser et j’entendis monter une rumeur sous mes pieds. Le Lapin Volant s’emplissait et je me mis à guetter les allées et venues dans l’escalier en colimaçon. Je finis par m’allonger, l’oreille collée au plancher, ce qui fit que je m’endormis.
Depuis combien de temps les bandits étaient-ils à l’étage du dessous quand j’entendis chanter « Lirlonfa maluré » ? La Bouillie leur servait à boire et à manger, tout en chantant.
- T’vas fermer ça ? lui ordonna une voix brutale.
Je tremblais si fort que je préférai ne pas me relever et j’allai jusqu’à mon coffre à quatre pattes. Je soulevai le couvercle et me faufilai à l’intérieur. Je ne serais protégé du regard des bandits que par une mince épaisseur de bois. Le coffre était très petit et je ne pouvais y tenir que recroquevillé. Il sentait fort le moisi et je ne pus m’empêcher au bout de cinq minutes de soulever le couvercle pour happer un peu d’air frais. Au même instant, j’entendis cogner dans le plafond. Quelqu’un repoussait la trappe. Je me repliai à l’intérieur de mon coffre, et pour la première fois depuis que j’avais quitté mes tantes, je me souvins de mes prières. S’il y avait un Dieu pour les pégriots, c’était le moment pour lui de prendre du service. J’entendis les bandits s’installer sur les tabourets. Ils avaient apporté le vin avec eux. Ils étaient si bruyants qu’ils auraient pu être dix, mais j’identifiai bientôt seulement trois voix différentes, une brutale, une autre un peu flûtée qui devait appartenir à un tout jeune homme, et la dernière enfin, traînante, et que je reconnus fort bien.
C’était celle de Roland, le suageur qui avait assommé Craquelin. Après avoir bu plusieurs verres, les trois grinches en vinrent au sujet de leur réunion.
- C’est quoi, ton chopin* ? demanda Roland. (* ton vol)
Ce fut le brutal qui répondit et il prit dans la nuit de mon coffre l’apparence du cocher au visage enduit de graisse noire.
- Le larbin, c’est un bon zig. Il m’a fait l’emplâtre et v’là la carouble**. (** Le domestique, c’est un copain. Il a fait l’empreinte de la serrure et voilà la fausse clé.)
J’entendis le cocher qui plaquait une clé sur le bois de la table.
- Alors, ça, c’est rudement bien goupiné ! le félicita la voix maniérée.
- C’est z’où, ton affaire ? demanda Roland, le ton méfiant.