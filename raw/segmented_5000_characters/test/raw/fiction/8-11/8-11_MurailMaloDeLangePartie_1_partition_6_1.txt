- Montons, dis-je en poussant Craquelin devant moi.
- Chez madame Massepain ? s’informa le petit qui n’y comprenait plus rien.
Nous allâmes jusqu’au dernier étage où de simples chambres s’alignaient de part et d’autre d’un couloir. Doucement, je tournai chaque poignée de porte et l’une d’elles céda. L’odeur de la pipe qui régnait dans la pièce me fit supposer que nous étions chez le jeune homme. Comme il n’avait rien à voler à part deux volumes de poésie, il ne prenait pas la peine de fermer à clé.
- As-tu perdu les pommes, Craquelin ?
- Non, monsieur Malo.
Ce fut notre repas. Puisque le jeune homme était sorti, nous étions à l’abri pour un moment, le temps que nos poursuivants se lassent.
- Ce pauvre Bourguignon, se souvint alors Craquelin.
Va-t-on l’envoyer au bagne ?
- Non, il n’avait rien volé, lui…
Pour ma part, j’avais lâché le mouchoir en route. Il y a des souvenirs dont il vaut mieux ne pas s’encombrer, comme disait le monsieur qui avait scié sa femme en morceaux.
Pour passer le temps, j’ouvris la lucarne et m’aperçus, en m’y accoudant, qu’à l’étage inférieur la fenêtre était grande ouverte. Me suspendre à la gouttière et prendre pied sur le balcon du dessous me parut un jeu d’enfant, à moi qui avais si souvent escaladé les murs des jardins de Tours. Craquelin était plus petit que moi et je dus l’attraper par les jambes tandis qu’il se suspendait dans le vide. Puis je le tirai vers moi et nous roulâmes tous deux sur le tapis d’une chambre à coucher.
- Qu’est-ce que nous faisons là, monsieur Malo ? s’informa timidement Craquelin.
Je ne le savais pas moi-même. Avais-je agi de la sorte parce que La Bouillie m’avait appris comment font les venterniers* ? Je regardai autour de moi. Il y avait là plein de jolies choses à voler, un éventail, un châle, un livre relié, une bourse en filet. Mais étais-je un voleur ? (* Ce sont des voleurs qui s’introduisent par les fenêtres.)
- Monsieur Malo, vous réfléchissez ? Faites vite alors parce qu’il y a des gens dans la maison…
En effet, j’entendis des pas et un sifflotement qui se rapprochaient de la porte fermée de la chambre à coucher.
- Sous le lit, Craquelin !
Nous nous jetâmes à plat ventre. Pour le coup, Craquelin se faufila plus facilement que moi sous le bois de lit. Quand la porte s’ouvrit, nous étions tous les deux bien cachés.
- Oh, monsieur François, dit une voix de jeune fille, vous n’auriez pas dû venir. Si papa nous surprenait !
- Je vous aime, Lucie. Votre père est un monstre.
- Ne dites pas cela ! Il ne veut que mon bonheur, mais…
- Vous l’aimez plus que moi ?
- Monsieur François…
- Embrassez-moi !
- Non, ce n’est pas bien.
- Vous ne m’aimez pas.
- Mais si !
Qu’ils étaient sciants, tous les deux ! Pendant ce temps-là, nous étouffions sous le lit, Craquelin et moi.
- Oh, mon Dieu ! s’écria la demoiselle. J’entends mon père. Il est rentré !
- Cachez-moi, cachez-moi, supplia monsieur François.
Je vis le moment où nous serions trois sous le bois de lit. Je décidai de sortir de ma cachette et de tirer monsieur François d’embarras. Mademoiselle Lucie faillit tourner de l’œil en me voyant ramper sur son tapis, bientôt suivi par Craquelin.
- Mais qu’est-ce que vous faites là ? s’étonna monsieur François, oubliant qu’il avait plus urgent à régler.
- Si vous voulez échapper au papa, suivez-moi !
Monsieur François ne se le fit pas dire deux fois et il passa comme moi de la fenêtre à la lucarne. Puis nous tendîmes les bras à Craquelin pour le hisser jusqu’à nous. Une fois dans la chambre, les jambes cassées par l’émotion, nous nous assîmes tous les trois, Craquelin et moi sur le lit, et monsieur François sur l’unique chaise.
- Donc, vous êtes des voleurs, remarqua-t-il.
- On dit qu’on est des grinches dans le métier, rectifia Craquelin.
Je lui filai un coup de pied.
- Nous pensions trouver notre tante à cet étage, expliquai-je.
- Et vous l’attendiez sous le lit ?
Devant notre silence, monsieur François haussa les épaules.
- Je ne suis pas suffisamment l’ami des gendarmes pour vous livrer à eux.
Il me tendit la main :
- François Janvier.
- Malo.
- Pas tout à fait un nom de fille, hein ?
Je soupirai. Dix fois, j’avais failli me tuer en me prenant les pieds dans cette robe.
- On m’a volé mes vêtements.
- La vie est dure chez les voleurs, ironisa François Janvier.
- Oui, et on a faim, dit naïvement le pauvre Craquelin.
Janvier avait bon cœur, il partagea avec nous le peu qui lui restait à manger. Une fois attablé, je lui demandai s’il connaissait un endroit où nous pourrions trouver des vêtements.
- Avec de l’argent, oui. Sans argent, non.
Je sortis de ma poche la tabatière de monsieur Lamproie.
Janvier la soupesa, puis la frotta du revers de la manche pour la faire reluire.
- C’est la tabatière de votre oncle Briquebœuf ?
- En plein dedans.
- Vous pourriez en tirer cinquante francs en la vendant à un honnête bijoutier. Mais étant donné les… hum…