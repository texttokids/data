- Pourquoi tu as apporté du fromage ? Ce n’est pas prévu.
- Regarde quel fromage.
- Brillat-Savarin, je n’en ai jamais goûté.
- C’est un , doux au palais, qui se mange jeune et frais. Il est fabriqué dans les régions de  et de .
- Matière grasse ?
- 35 % de matière grasse, à partir de , c'est un fromage , d’un poids moyen de 500 grammes, qui se présente sous la forme d’un disque plat d’environ 13 cm de diamètre et 3,5 cm d’épaisseur.
- Tu as avalé Wikipédia ou quoi ?
- Je fais souvent ça, j’apprends un article par cœur et me voilà grand savant. C’est juste une introduction au nom de notre école.
- Et je vois que tu as apporté son livre « La physiologie du goût ». Tu l’as lu ?
- Un peu …
Devant notre collège si ordinaire, si calme, si banal, il y a une foule, des camions de la télé, des journalistes comme s’il y avait un meurtre avant notre arrivée.
Un homme que je reconnais parle au micro. C’est Jamie Oliver. Il n’est pas venu seul !
Mademoiselle Laurent vient vers moi. « Tu savais ? »
- Pas du tout. L’invitation était pour lui. Tout à fait intime.
- Il va falloir que j’aille parler avec le directeur. Il faut des autorisations, aie aie aie.
- J’irai le prendre en charge avec Carl.
Je vais vers la horde sauvage. Je me présente : « I am Bonnie Bonnet, I was the one who invited you. It is so nice of you to come speak to us. But in my head, it was a private little meeting. »
- It grew ! Is this a problem ?
- The French administration may not allow this. There is a state of emergency in France, Vigipirate, you understand ?
- I’m sorry. 
Bien que le principal ne le connaisse pas, ni d’Eve, ni d’Adam, ce Jamie Oliver, il sort l’accueillir comme s’il était le roi de Siam. Il baragouine même quelques mots en anglais. Et il accepte la télé, la radio, et qui veut entrer dans son moulin. Il aurait pris des extraterrestres, des farfadets et des lutins s’ils étaient accompagnés de la télé.
Jamie Oliver est content que la cuisine soit occupée pour préparer le repas du midi. Il va de casserole en casserole. Il donne même des conseils aux cuistots. Les cameramen et women s’installent. C’est toute une population voire une armée et ils vont mettre longtemps à prendre leurs marques.
Et pendant ce temps Omama est à la chimio avec Josiane. Le directeur qui semble vouloir l’exclusivité de Jamie Oliver, envoie Carl et moi dans notre classe. « Il n’est pas encore prêt, on va lui laisser faire son travail. »
Et nous on se tape les maths.
Je vois que Carl apprend son discours sur Brillat Savarin et moi je tourne les pouces, je me gratte, je me peigne avec mes doigts, je triture ma montre Patek Philippe, je m’assois sur une fesse, puis l’autre, je tape des pieds, je regard à gauche et à droit, mais surtout pas le tableau. La leçon est interminable et je pense à tout ce qui se passe dans la cuisine.
Quand enfin nous nous y rendons, Jamie Oliver est en plein dans son film. Il dit qu’il est au collège Brillat Savarin à Paris où le déjeuner pour les élèves est en préparation. Il approuve le menu car il y a la salade, du poisson (surgélé), des épinards (surgelés). « I even saw organic fresh fruit and vegetables. This school deserves to be called Brillat Savarin. »
Ce que je pensais être une rencontre sympathique avec un Brillat Savarin moderne est devenu un voyage mégalomédiatique. Notre classe, selon les instructions de Mademoiselle Laurent, mange ensemble pour être filmé ensemble. Jamie Oliver mange aussi et analyse le goût de chaque plat devant les caméras. Il trouve que nous sommes beaucoup plus sophistiqués que nos homologues américains. Merci ! Le club va suivre le déjeuner. On nettoie les tables et nous restons assis. 
Quand on a le feu vert, Carl se lève et récite son discours sur Brillat Savarin. Il est un orateur avec du charme et du savoir. Je traduis pour Jamie. J’apprends qu’il ouvre un nouveau restaurant italien à Paris (ainsi mon invitation lui tombe pile au service de sa pub). Ce qui n’empêche pas qu’il soit excessivement sympathique.
Mademoiselle Laurent nous fait déménager dans la cuisine et lui passe les ingrédients pour son curry d’agneau.
On branche les téléphones ; on débranche les cerveaux, ainsi on va apprendre du maître. Il commence par citer notre Brillat Savarin (que je ne connaissais même pas il y a une semaine) « You are what you eat ! »
Il coupe, il fait dorer, il fait mijoter, ça sent bon, on vient de manger mais on a de nouveau faim, c’est de la magie. 
Pendant que ça cuit, il nous demande « If you are what you eat, what are you ? »
- I’m chocolat !  dit Dorélie.
- I’m pirogi ! je dis pensant à ma grand-mère.
- Je suis frites !
- Je suis hamburger !
- I’m ice cream, dit Carl qui est entièrement dévoué à la crème glacée.
- Je suis fraises tagada !
- I’m chumous, dit Omar.
Ainsi tout le monde annonce sa nourriture préférée. Jamie montre sa consternation avec une expression de dégoût. Il nous parle de l’obésité, du diabète, des artères, des maladies qui tuent, montre un de ses films sur son téléphone, parle des clefs de la santé. Il réussit à convaincre.
Le temps de sa plaidoirie et son curry est prêt. Il le met dans une assiette et nous passe des fourchettes. Inutile de dire qu’une bouchée ne suffit pas.
Son attaché de presse nous distribue à chacun le nouveau livre de cuisine dédicacé par l’auteur Jamie Oliver. C’est vraiment Noël et nous sommes tous ravis.
Avant de dire au revoir, il me prend à côté pour me dire combien il apprécie mon invitation. Le film de la rencontre sera sur son site web et aussi au journal télévision à 20 heures. Il me tend aussi deux invitations pour l’ouverture de son restaurant. J’irai avec Omama.
Il part avec son armée. Mademoiselle Laurent est en tête à tête avec le principal.
Je pars aussi avec Carl et Dorélie. Omar nous rattrape.
- Je peux rentrer avec toi ? demande-t-il à Dorélie.
- On y va, répond-elle.
On se sépare d’eux et Carl revient chez moi.