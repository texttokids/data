Maman a apporté le courrier que j’ai oublié dans sa boîte en bas dans ma précipitation à voir Omama. C’est une belle enveloppe calligraphiée adressée à Mademoiselle Bonnie Bonnet.
Je l’ouvre devant tout le monde pour découvrir une invitation au mariage de Julie (dans ma robe !) Elle est de parole ! C’est un mariage en mai alors il y a le temps. Peut-être Omar pourrait me faire une belle robe rose. Sans cousins, sans cousines, dans un cercle restreint d’amis, je ne suis jamais allée à un mariage. Mon père ne jugeait pas bon de m’inviter au sien avec Catherine, la mère des monstres. J’aimerais obtenir une invitation pour Omama aussi. Elle au moins a de l’expérience. Elle a organisé le mariage de maman avec papa. Le sien aussi avec mon grand-père mystère.
La deuxième enveloppe est aussi adressée à Bonnie Bonnet et famille. C’est une convocation aux portes ouvertes du foyer d’Omar avec un défilé de mode et dégustation des plats exotiques. Comme la vie est pétillante ponctuée par des fêtes, des mariages, des gens positifs. Mamie Colette qui ne l’est pas est partie en nous promettant de nous inviter à son hôtel palais. Il va falloir qu’elle surmonte son dégoût de ce sacré cancer qu’elle doit penser est contagieux.
La troisième enveloppe est de Catherine, l’ex-femme de mon père, pour Omama. On est surprises qu’elle écrive, mais malheureusement pas surprises par ce qu’elle écrit :  
« Aïe ! Quelle poisse ! Comme tu dois te sentir misérable ! 
La vie est brutale et te fait payer cher le travail que tu as fait. Tu as néanmoins démontré une force hors du commun pour ton âge et on peut vivement espérer qu'avec un juste repos, les choses vont rentrer dans l'ordre.
Catherine »

C’est elle la poisse !
La quatrième enveloppe n’a pas de timbre. C’est de Carl qui m’invite pendant les vacances de Pâques au ski encore en Suisse. Je lui téléphone.
- Mon petit Carl chéri adoré, tu es peut-être au courant de ma phobie de vitesse, de neige, de chaussures de ski.
- Il faut surmonter !
- Qu’est-ce que tu détestes le plus dans la vie ?
- L’immobilité !
- Et c’est justement ce que j’aime le plus ! Il faut rester immobile pour écrire. 
- Il faut bouger pour avoir de quoi écrire ! Mais je resterais bien immobile pour te tenir compagnie, alors tu pourrais bien bouger pour me faire plaisir, n’est-ce pas ?
- N’importe quoi, Carl, mais pitié, pas du ski.
- Rien que le matin ! L’après-midi tu peux cocooner. 
- Je vais réfléchir …
- C’est une chance. Plus on aime faire des choses, plus on est heureux. Regarde le tandem ! Tu as vaincu ta peur.
- Un tandem de ski alors ?
- Tu as aimé l’hôtel !
- J’ai aimé ta compagnie.
- Alors tu comprends que je veuille la tienne. Mon père insiste !
- On verra. Ma vie est imprévisible en ce moment avec Omama.
- Si tu veux, je peux l’accompagner ce vendredi. Chacun son tour.
- C’est gentil, mais je pense qu’elle aime aller avec moi. Je lui demanderai. Tu as eu l’invitation pour voir le foyer d’Omar. C’est la semaine prochaine.
- Oui, on peut y aller ensemble. Avec Dorélie aussi. On a de la chance de voir cela de près. Sinon, « réfugié » c’est juste un mot sans l’humanité derrière.
- J’ai toujours été un peu perplexe. Bien sûr ce serait mieux que ces personnes déplacées puissent rester chez eux dans de bonnes conditions, sans guerre, sans famine, mais ils ne peuvent pas. Alors il faut les accueillir ici. Ma propre grand-mère est venue d’ailleurs. 
- Heureusement qu’elle est venue !
Quand Je demande à Omama si elle veut bien que Carl l’accompagne à la chimio, elle accepte sans problème.
C’est donc Carl qui regarde les têtes couvertes par des perruques, des casquettes et des turbans, c’est Carl qui s’assoit à côté du lit pour lire à Omama un livre qu’il a apporté, le livre que nous lisons en classe « La vipère au poing » de Hervé Bazin. A mon avis ce livre est trop dur pour Omama dans son état, mais elle aime qu’on lui lise et trouve de la matière à discussion. Ceci dit, elle s’endort rapidement et sa voisine est assez fermée à la causette. Le temps va vite, Carl continue sa lecture, Omama poursuit ses doux ronflements avant d’être débranchée de la chimio. Elle trouve les infirmières tellement jolies et souriantes et l’atmosphère si chaleureux qu’elle quitte presqu’à regret.
Je rentre du collège pour tester la recette d’agneau aux pois chiches avant de la faire dans le club. Omama se ressert deux fois. La chimio ne détruit pas son appétit. Contrairement à ce que pense Mamie Colette et Catherine, Omama n’inspire pas pitié du tout.


Le foyer