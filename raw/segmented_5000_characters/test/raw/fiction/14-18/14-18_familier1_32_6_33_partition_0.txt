Le bruit de notre converse éveille la mère Pâquerette (en l'eau cul rance ce serait plutôt un dahlia qu'elle évoquerait). Ma présence la trouble comme une goutte d'eau trouble le Ricard le plus pur. Elle se séante et ses vingt kilogrammes de nichons plouffent sur son ventre à replis. Du coup, les lunettes tatouées adoptent un regard de myope.
- Salut, bouffie, la salue-je galamment ; sois gentille : cache ta triperie ! Le matin, les abats me portent au cœur !

Elle relève un bout de drap qui traînait par là sur sa poitrine gélatineuse.

Une docile. Plutôt une soumise. La môme idéale pour devenir pute professionnelle. Elle se trouve dans l'antichambre de la prostitution, Pâquerette. Un pas de plus et elle met le pied dedans, comme toi dans une merde quand tu vas acheter le journal.
- Donc, reviens-je à Ted, il s'agit d'une blague ? Tu m'as vu à la Rose d'Or. Tu te dis : "Tiens, ce salaud de flic à qui je dois la belle cicatrice qui ajoute tant à mon charme, prend du bon temps avec une gerce, je vais lui jouer un tour". Tu tires ton mouchoir, tu écris ce message mystérieux et inquiétant. Tu charges ta rombiasse de me le faire tenir sans m'alerter. Cette grosse charrette, pas sûre d'elle, transmet le flambeau à la copine délurée qui vous accompagne. Et, effectivement, ta babille sur fil d'Écosse arrive à bon port. O.K? C'est la version qu'on enregistre, tout est bon, y a pas de virgule à changer dans le texte ?
- Bon, O.K., alors saboulez-vous, les deux, et suivez-moi.
- Pas une partouze, rassure-toi, ton brancard me ferait dégoder. Faut être english pour pouvoir s'embourber ce catafalque de bidoche pas nette !

J'ai débité la dernière réplique en anglais, pas désobliger la grosse. Tu connais ma galanterie légendaire ?

Le couple se lève, sans trop de pudeur, et se loque avec mornitude. Ted est blafard sous sa rouquinerie. Il a des cils de porc, comme ceux dont se servent les artistes chinois pour peindre sur un grain de riz la conquête de Pékin par les Mandchous en 1644. En passant son jean, je le vois qui en palpe les vagues.
- Non, tu ne l'as pas perdu, lui fais-je ; c'est moi qui l'ai.

Une qui ouvre des vasistas grand comme l'entrée principale de Saint-Pierre de Rome en nous voyant radiner tous les trois, c'est ma miss Lola.

Elle me questionne du regard.
- Ce sont les amis qui m'ont carré dans la braguette le message que tu sais !

Du coup, la v'là qu'ébullitionne. Elle louche sur Pâquerette.
- Non, une de ses amies beaucoup plus souple.
- Commander à bouffer, ils ont un room service а l'hôtel. J'ai lu le menu dans l'ascenseur, je serais assez pour de la viande des grisons et des filets de perche meunière, pas vous, mes amis ? C'est bon et léger pour le déjeuner. Ça ne vous abîme pas l'après-midi. Un coup de fendant pour arroser le tout et nous serons en pleine forme pour faire quelques parties de rami dans l'après-midi.

Lola pige de moins en moins.
- Non, rassure-toi. On reste ensemble jusqu'au spectacle de ce soir. Nous irons à la Rose d'Or tous les quatre ; et ensuite, si nous sommes encore vivants, nous nous séparerons. O.K., Teddy ?

Il me regarde et hausse les épaules.

TOUJOURS A MOI QUE ÇA ARRIVE !

Ces dames refusant de jouer aux cartes, nous fîmes un poker, Ted et moi. Il trichait à la grecque, ce qui est rare pour un natif de la Grande Albion de mes fesses et m'épongea cinq cents francs. Et cinq cents vrais francs : pas des français ni des belges, des suisses. Tu avoueras qu'il n'est pas commun qu'un flic se fasse secouer sa fraîche par le malfrat qu'il surveille. Mais je ne suis pas n'importe quel poulet, tu l'auras déjà pressenti.

La journée se dérouta dans une torpeur un peu cafardeuse, sous un ciel où le soleil se laissait biter par des floconneries de nuages. Les cris acides des mouettes ajoutaient à la mélancolie ambiante. J'avais déjà vécu des moments de ce tonneau au (long) cours de veillées funèbres consacrées à des gens qui ne me touchaient pas de trop près. Entre autres, après le décès de la mère Dunkerque, une voisine presque impotente qui se prénommait Rose (car c'est la rose l'impotente).

M'man s'était occupée d'elle sur la fin de ses jours. Elle ressemblait à une baleine échouée sur la grève de son plumard, la mère Dunkerque. Des bajoues à n'en plus finir, des nichons plein le lit, un ventre qui foirait tout azimut. Elle matait sa téloche toute la sainte journée, en actionnant constamment, les boutons de la télécommande, sans jamais se fixer sur un programme. Une butineuse d'ondes hertziennes ! Et puis elle était clamsée gentiment, un après-midi d'automne (c'était peut-être le printemps, mais quand tu meurs, c'est toujours l'automne). On l'avait veillée en compagnie d'un autre voisin serviable.