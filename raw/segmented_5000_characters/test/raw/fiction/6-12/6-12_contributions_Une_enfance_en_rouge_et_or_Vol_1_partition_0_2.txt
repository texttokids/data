Il fallait en arriver de temps à autre à ces punitions pour qu’elle réalise que la discipline était applicable à tous et que seul le temps de la récréation était dévolu à la distraction.
UNE NOUVELLE INATTENDUE 
Au tout début de l’année où elle allait fêter ses dix ans, les parents d’Anna vinrent la voir dans sa chambre pendant qu’elle jouait et lui demandèrent, l’air cérémonieux, de les écouter attentivement. 
«  Anna, ma chérie, nous avons quelque chose d’important à te dire, annonça la maman. 
Le papa se mit à poursuivre, jetant un coup d’œil furtif à la maman : 
- Oui, Anna, tu es grande maintenant. C’est pourquoi nous avons pensé, maman et moi, qu’il serait intéressant que tu aies de la compagnie pendant les vacances. 
- Comment cela ? questionna l’enfant.
- Ce que veut dire papa, reprit à son tour la maman, c’est que dans le cadre de notre travail, nous avons la possibilité, grâce à une association, de permettre à des enfants que nous soignons, ou avons déjà soignés par le passé, de connaître de meilleurs moments de vie, le temps des vacances. Ce sont des enfants dont les parents ne peuvent plus s’occuper et qui peuvent alors être accueillis temporairement dans d’autres familles. 
- Voilà, donc, ce qu’essaie de te faire comprendre ta maman, continua le père, c’est que nous envisageons d’accueillir un enfant pour les vacances de février ; nous verrons comment cela va se passer et si l’essai est concluant, il pourra probablement revenir aux vacances de Pâques ; et si tout continue à très bien se passer entre tous, il pourra vraisemblablement rester avec toi chez Nanette et Papy Germain qui prendront le relais l’été prochain. 
La fillette écoutait sans broncher. Surprise mais intriguée.
Il y eut un petit silence, puis la mère enchaîna à nouveau : 
- Anna, tu te souviens qu’on a déjà parlé ensemble que nous devrions bientôt nous absenter, ton père et moi, pendant une période de deux mois en mission médicale à l’étranger ? Alors, c’est confirmé, c’est pour l’été qui vient. 
- Ah ! fit simplement Anna. 
- Bon ! s’exclama le papa. Nous n’allons pas y aller par quatre chemins. Tout cela pour te dire que nous avons contacté l’association en relation avec notre équipe médicale déjà sur place et un jeune garçon pourrait venir en février prochain afin de se familiariser avec nous tous. Qu’en penses-tu ? 
- J’aurais préféré que vous restiez cet été !
- Mais Anna, reprit le père dans un léger soupir, tu sais très bien que quoiqu’il en soit, tu vas passer tout l’été avec Nanette et Papy. Il était prévu que nous ne pouvions pas prendre de repos à cette période. 
- Oui, je sais, répondit-elle, dans un murmure. 
- Alors, dit-il, nous aimerions bien que tu comprennes que c’est important aussi bien pour toi que pour lui. 
- Pourquoi ? questionna-t-elle, étonnée.
- Eh bien, pour ne pas vous sentir seuls l’un et l’autre pour les grandes vacances ! De plus, tu passes toujours de très bons moments à la campagne, mais lui, ne sait pas ce que signifie passer de belles vacances. Aussi, après un long séjour à l’hôpital, ce ne peut être que bénéfique pour lui. 
- O.k., marmonna-t-elle, pas vraiment convaincue qu’un étranger vienne fouler ses plates-bandes. 
Le statut d’enfant unique lui convenait très bien et elle se demanda vraiment ce qui avait pu faire penser le contraire à ses parents. Elle ajouta quand même : 
- J’aurais préféré que ce soit une fille qui vienne, alors !
- Bien sûr, trésor, renchérit le papa, nous comprenons, mais pour la période où nous serons absents, seuls des garçons de ton âge, ou à peu près, ont la possibilité d’être accueillis. Les filles qui restent sur place sont beaucoup trop petites pour partir si loin. Tu as l’habitude de tes copains du centre de loisirs pourtant, tu joues beaucoup avec eux. 
- C’est vrai, dit la fillette, mais il dormira où ? 
- Ne te tracasse pas pour cela, la rassura sa maman, ce n’est pas très important. Nous avons suffisamment de place ici et il en est de même à la campagne chez tes grands-parents. Vous aurez chacun votre espace. 
- Bon, alors d’accord ! finit par dire Anna. 
- À la bonne heure ! conclut le père.
Anna repartit dans sa chambre et avec l’imagination débordante que chacun lui connaît, mit en scène une histoire qu’elle conta à ses amies les peluches alignées sur des étagères. Elle leur mima même des détails comme si elles étaient en mesure de la comprendre. 
Il est vrai que cette rencontre à venir était un bon sujet pour élaborer un nouveau scénario. 
Et puis, finalement, elle était déjà heureuse de partir l’été prochain chez ses grands-parents Germain et Anna, dits Papy et Nanette. Elle les adorait et en plus, ils habitaient un endroit superbe dans la jolie vallée de la Têt, dans le Conflent ! 
Que d’aventures en perspective !