C’est la fin de la journée, Lily rentre de l’école, toute triste. Papa et Maman s’inquiètent et lui demandent : “Qu’est-ce qui ne va pas ma petite Lily ?”
La petite regarde ses parents en disant : “La maîtresse commence à nous apprendre à lire, mais je trouve ça trop difficile. Je bute sur les mots, et les copains se moquent de moi…”
Lily est bien chagrinée, mais Maman et Papa ont la solution. Pour apprendre à lire plus facilement, Maman sort des petites images. “Viens là Lily, et dis moi ce que tu vois sur chacune des images” explique sa Maman.
A la première, la fillette dit “Un robot”. “Exactement ! Et que fait ta bouche quand tu dis le mot “robot” ? demande Maman.
Lily répète le mot “robot” plusieurs fois. Elle s’aperçoit alors que sa bouche forme la lettre O.
“Et bien voilà”, explique Maman, “maintenant quand tu vois le mot “robot”, tu vois qu’il y a deux fois la lettre “O”, comme la forme de ta bouche.” La petite Lily comprend ce que lui explique Maman.
Maman et Papa lui montrent plein de petites astuces drôles pour réussir à lire les sons. Lily s’amuse tout en apprenant, et cela fait plaisir à Papa et Maman.
A l’école, la petite fille est plus rassurée. Grâce aux méthodes de son Papa et sa Maman, Lily arrive à lire beaucoup mieux qu’auparavant.