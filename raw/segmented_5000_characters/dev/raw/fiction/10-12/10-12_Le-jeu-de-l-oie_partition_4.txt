Soudain, j’ai entendu une petite voix appeler. Une voix enfantine, craintive et tremblante.
Je me suis avancé, cherchant d’où elle provenait. Peu à peu, j’ai reconnu des mots : « Victor, Victor, s’il te plaît… ». Pas de doute, cela venait des WC. Et pourtant ce n’était pas la voix sèche et rauque, rongée par la fumée de milliers de cigares, de ma tante Michèle. C’était la voix d’une petite fille perdue dans l’obscurité.
- J’ai peur du noir, Victor, a dit la voix.
J’ai répondu :
- Ferme les yeux, et tu ne verras plus le noir.
Ce n’était pas pour me moquer. C’est ce que maman me disait quand j’étais petit et que je l’appelais, assis sur mon lit, entouré du noir de la nuit et de ses fantômes.
Silence de l’autre côté de la porte. J’ai ajouté :
- Tu peux chanter aussi. Moi, je chantais toujours « Maman, les petits bateaux qui vont sur l’eau ». Ça marchait…
En prononçant ces mots, sans savoir pourquoi, j’ai pensé : j’aurais bien aimé chanter « Papa, les petits bateaux qui vont sur l’eau ». Mais je n’ai pas de papa. Il existe, bien sûr, mais je ne l’ai jamais vu. Il s’appelle Bertrand et il est ingénieur informatique. Quand maman est tombée enceinte de moi, il l’a quittée et n’a jamais voulu me voir. Souvent, je me demande où il habite, s’il a une femme, des enfants…
J’ai secoué la tête pour ne plus penser à ça, pour chasser les mots qui s’embrouillaient dans ma tête.
Dans les WC, la petite voix chantait : « A la claire fontaine, m’en allant promener… ». J’ai souri. C’est la rengaine de ma mère. Elle la chante à tue-tête quand elle passe l’aspirateur ou cire les meubles.
- Bon, ben moi, je continue à jouer, ai-je lancé bien fort. Voilà, je jette les dés. Quatre et trois, sept. J’arrive sur la case numéro 26. Et c’est encore à moi. Deux et quatre, six, ça m’amène donc sur la case 32. Ouf, j’ai eu chaud. Tu as entendu, tatie Michèle ? Un point de moins et c’était moi qui tombais dans le puits.
Sans répondre, elle a continué à chantonner « …pour un bouquet de roses que je lui refusais ». J’ai commencé à m’inquiéter. J’ai frappé légèrement à la porte, j’ai demandé :
- Hé, ça va ?
La chanson s’est arrêtée, brusquement, et de sa voix normale, grave et enfumée, mais adoucie encore par d’anciens souvenirs, Michèle a parlé :
- J’adorais cette chanson quand j’étais gamine. Elle me faisait toujours penser à mon père, ne me demande pas pourquoi…
Elle s’est tue un instant. Je n’ai rien dit, j’ai attendu qu’elle continue. Elle a donné un coup de poing sur la porte.
- Victor, tu es là ?
- Non, non, je suis pas là, ai-je répondu en prenant une toute petite voix.
Elle a tambouriné sur la porte.
- Laisse-moi sortir ! Laisse-moi sortir du puits !
J’ai entamé une sorte de danse de pélican ivre en tirant la langue et roulant des yeux. J’étais tranquille : elle ne pouvait pas me voir.
- Victor, je t’en supplie ! a-t-elle gémi.
J’ai poussé un soupir à décoiffer mon copain Erwan qui use pourtant d’un pot de gel par jour et j’ai dit :
- Ben, je peux pas !
- Pourquoi ?
- Tu ne peux sortir que si un joueur vient te remplacer. Mais j’ai déjà dépassé la case 31 et nous ne sommes que deux joueurs... C’est pas drôle, je sais, mais je dois continuer à jouer tout seul jusqu’à ce que j’arrive à la case 63.
Elle a gratté avec ses ongles à la porte. Puis plus rien. Je suis allé dans ma chambre et je me suis jeté sur le lit pour lire un manga. Mais je le connaissais par cœur, et ça m’a vite ennuyé.



7