- C’est moi qui commence, a-t-elle décidé.
Elle a lancé les dés, déplacé son pion. Puis c’était mon tour. J’ai lancé les dés, déplacé mon pion. Et ainsi de suite. Mortellement ennuyeux. Mais ça amusait Michèle. Surtout quand je tombais sur les mauvaises cases. Elle, elle trichait ouvertement en déplaçant son pion à toute vitesse pour l’arrêter juste avant les cases dangereuses.
Je ne protestais pas, je voulais que ça se termine le plus vite possible. Mais quand la première partie a été finie, Michèle s’est écriée :
- Pauvre Loulou, il a perdu ! Allez, va, je t’offre une revanche ! A toi de jouer…
J’ai joué. Deuxième partie. Perdue. Troisième partie. Encore perdue. Les joues rouges, les cheveux ébouriffés, Michèle riait de plaisir. Je ne comprenais pas comment ce jeu débile pouvait la passionner à ce point. Dans le cendrier, le cigare brûlait lentement, oublié. 
- Tu sais, a dit Michèle tout à coup, j’adorais le jeu de l’oie quand j’étais petite. Mais je devais toujours laisser gagner ta mère parce qu’elle était plus petite.
Sans transition, elle a ajouté :
- Va me chercher à boire, j’ai soif ! Un verre de vin, du rouge.
J’ai obéi. Je suis allé à la cuisine, j’ai versé un verre de vin. J’ai eu la tentation d’ajouter du sel et du poivre. J’ai renoncé au dernier moment. Je suis retourné au salon, j’ai posé le verre sur la table.
Michèle l’a bu d’un trait. Puis elle m’a fixé d’un air inquiétant. Je n’avais pourtant rien fait, promis, juré ! Lisait-elle dans mes pensées ? Non. Ses traits se sont soudain détendus. Elle s’est écriée :
- J’ai une idée !
J’ai sursauté. Qu’avait-elle encore inventé ?
- Pour que ce soit plus amusant, a-t-elle annoncé, on va faire un jeu de l’oie grandeur nature à travers l’appartement !
Je n’ai rien compris, mais je n’ai pas posé de questions. Surtout pas.
Elle s’est précipitée sur un bloc de papier à lettres. Au feutre, elle a tracé de grands chiffres sur les feuilles, de un à cinquante-deux. Elle a scotché la plupart sur le sol, en partant de la porte d’entrée. Mais elle a collé le numéro 6 sur le lavabo de la salle de bain, le 12 sur la baignoire, puis le 19 sur la table de la cuisine et le 31 sur la lunette des WC. Ensuite elle a dispersé les chaises dans la salle à manger et posé le 42 sur l’une d’elles. Pour finir, elle a collé le numéro 52 sur la porte du placard de ma chambre.
Et elle m’a expliqué :
- Le 6 et le 12, ce sont les ponts. Le 19, c’est l’hôtellerie. Le 31, c’est le puits. Le 42, c’est le labyrinthe et le 52 la prison. Comme dans le jeu traditionnel.
Je ne voyais toujours pas où elle voulait en venir. Elle a pris les dés et m’a traîné jusqu’à la porte d’entrée.
- Le paillasson servira de case départ, a-t-elle déclaré.
- Mais pourquoi on ne continue pas à jouer normalement ? ai-je osé demander.
- Je te l’ai dit, Loulou : ce sera plus amusant. 
Et elle a lancé les dés. Un cinq et un trois. Elle a déplacé son encombrante personne jusqu’à la case numéro huit, au fond du couloir.
C’était à moi. J’ai fait un six. Michèle a frappé des mains comme une gamine.
- Six ! Loulou a fait un six ! C’est le pont, c’est le pont !
Et alors ? Je ne comprenais vraiment pas pourquoi elle s’excitait à ce point. On jouait au jeu de l’oie depuis plus de deux heures, et ce n’était pas la première fois que je tombais sur la case numéro 6. Je connaissais la règle par cœur : « Qui fera 6, où il y a un pont, paiera le prix convenu et se placera au nombre 12 pour se noyer sous le second pont. »
- Qu’est-ce que je fais ? ai-je demandé avec un soupir ennuyé.
- Eh bien, tu vas au 6.
Sans me presser, j’ai cherché la feuille de papier marquée du numéro 6. Elle était collée sur le lavabo dans la salle de bain.
- Et maintenant ?
Tante Michèle a éclaté de rire :
- Au numéro 12, Loulou, au numéro 12 !
Qu’y avait-il de drôle là-dedans ? Mystère. La case 12 était la baignoire. Michèle s’est précipitée dans la salle de bain et a ouvert en grand le robinet d’eau froide.
- Qu’est-ce que tu fais ? ai-je demandé, vaguement inquiet.
- Tu vas te noyer sous le pont ! C’est la règle, Loulou…



5