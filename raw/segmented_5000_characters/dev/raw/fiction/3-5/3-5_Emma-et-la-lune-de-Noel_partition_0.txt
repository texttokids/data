Emma et la lune de Noël



Ce n'est pas au Père Noël qu’Emma donne sa liste. 

Quand Emma voit la lune si loin dans le ciel et si près d'elle, elle sait que sa liste va aller tout droit vers elle. Elle sait que la lune est son amie. Elle pense même que la lune lui fait un grand sourire.

La lune n'est pas toujours là-haut, perchée dans la nuit, mais quand elle est là, Emma lui parle : "Je ne t'écris pas, maman lune, parce que je ne sais pas encore écrire, mais je vais te dire ma liste de cadeaux : ma liste est courte parce que tout ce que je veux c'est toi, c'est la lune."

Emma a l'impression que la lune lui fait un clin d'œil et lui répond : "Voilà ! C'est fait ! Je me donne à toi !"

Elle ne se fait pas de soucis. C'est promis. Emma aura la lune.

Emma voit les paquets que le Père Noël a déposé dans l'armoire de l'entrée parce que sa hutte est trop lourde le soir de Noël. Et Emma sait que dans l'un de ces paquets il y aura un grand ballon blanc tout doux, tout mou, qui sera son doudou magique.

Sa maman fait du pain d'épices en forme de bonhommes et Emma aimerait croquer la lune.

Nono lui répète sa liste tous les jours. C'est une liste qui n'est qu'un long bouchon de camions, de voitures, d'avions, de trains. Comment peut-on désirer des camions quand on peut avoir la lune ?

Emma compte les jours. Nono n'arrête pas de lui parler des camions rouges et verts, grands et petits. Et le jour de Noël arrive avec sa dinde et ses marrons et Emma est confiante. Elle aura la lune. La lune le lui a dit.

Elle commence par les plus petits paquets. Pas de lune. C'est normal. Les paquets grandissent avec son espoir. L'avant dernier paquet n'est que l'avion de Barbie. Il ne reste plus qu'un. Emma voit que c'est rond. Elle sait que maman lune tient ses promesses. 

Elle regarde le ciel et elle voit que la lune n'est plus là, disparue du ciel pour atterrir chez elle.

Elle ouvre le paquet. Emma ne peut pas cacher sa déception. Elle fait une grimace. "C'est quoi ça ?"

"C'est un globe. Avec ça, tu sais où tu te trouves parmi tous les pays."

"Mais ce n'est pas lune …"

"Non, c'est la terre !"

Emma pense qu'il faut y aller petit à petit, d'abord la terre et après la lune.

Ce sera pour l'an prochain la lune !