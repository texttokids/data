Monsieur Personne laissa s’écouler quelques secondes de silence. « Il a deviné, pensai-je, il sait qu’elle ment.» J’attribue parfois à mon père des facultés extraordinaires, alors qu’il est un homme d’une intelligence médiocre.
- Lieutenant, au rapport.
J’obéis en prenant à mon compte le récit de Gaby.
- Tu me dis que tu n’as pas vu ces bandits, mais tu les as entendus, remarqua mon père. N’y avait-il rien de particulier dans leurs voix ?
Je secouai la tête mais, dans le même temps, Gaby s’écria :
- Si ! Un des hommes bégayait !
Mon père la regarda en silence et elle se mit à rougir, puis à bafouiller :
- C’est… c’est Malo qui me l’a dit. Tu… tu te rappelles, Malo ?
- C’est exact, il bégayait.
Un éclair bleu partit des yeux sombres de monsieur Personne :
- Je connais un habitué du Lapin Volant qui bégaie. On l’appelle Quiqui. C’est un venternier. Je vais demander à Frédo de le surveiller à partir de demain.
- Et si le meurtre a lieu cette nuit ? le questionna Gaby.
- On arrêtera les assassins.
- On ferait mieux d’empêcher le crime ! Pourquoi ne pas surveiller la rue des Prouvaires cette nuit ?
- Quelle maison ? Quel étage ? Quel appartement ?
Gaby, même si elle avait peur du chef, lui tenait parfois tête :
- La rue n’est pas très grande. Malo et moi, on pourrait…
Blam ! Monsieur Personne tapa des deux poings sur son bureau, nous faisant sauter en l’air, Gaby, moi et l’encrier.
- Tu ne discutes pas mes ordres ! Et tu ne quittes pas ton poste pour aller te réchauffer !
En fait, mon papa est vraiment quelqu’un d’extraordinaire.
- Et toi, me dit-il, couche-toi de bonne heure. Tu as une sale mine.
Nous nous quittâmes, Gaby et moi, à l’entrée de la galerie.
Elle me jeta un regard de chien battu :
- Je te paie un verre, veux-tu ?
- Non, je rentre chez moi. Ma mauvaise humeur me tient compagnie.
- Il n’aime personne.
- Qui ça ? Le chef ?
Elle eut un triste sourire :
- Personne n’aime personne.
Je regardai s’éloigner ce pauvre moineau de Gaby, sautillant entre les flaques de boue. Savait-elle seulement où elle allait passer la nuit ? Moi, en revanche, j’avais un projet, un projet qui me mena tout droit rue des Prouvaires.
Gaby avait raison : cette rue n’était pas longue. En passant devant le volet baissé d’une boucherie chevaline, je pensai soudain à ce conte où le chasseur doit rapporter à la méchante reine le foie et les poumons de Blanche-Neige comme preuves de sa mort. Où vivait l’enfant dont on voulait arracher le cœur ? Dans cette maison bourgeoise ?
Derrière cette fenêtre éclairée ?
J’avais décidé de monter la garde jusqu’au matin. Comme la neige s’était remise à tomber à gros flocons, je m’abritai sous un porche en face de la boucherie. Les longues stations solitaires dans la nuit et le froid, ce n’est pas le meilleur aspect du métier, comme disait l’Éventreur du Châtelet. Je songeai à la tiédeur du Lapin Volant, au tintement amical des verres, au bon souper de la mère Gargoton, et je me décollai du mur. Bah, une nuit pareille, même les assassins restent chez eux.
Comme je m’engageais rue de la Petite-Truanderie, je fus bousculé par une femme en châle qui semblait surgie de nulle part. Si je ne l’avais pas entendue venir, c’étaitussi incroyable que cela parûtarce qu’elle marchait pieds nus.
- Ouch ! C’est des façons avec les dames ! s’écria-t-elle en se frottant l’épaule.
- Mais c’est plutôt vous…
À la lueur du réverbère, je vis tout de suite à qui j’avais affaire. Une bohémienne, des anneaux d’or aux oreilles.
- Viens, viens, me dit-elle en me tirant par le bras, je vais te lire les lignes de la main.
- Ouais, c’est ça ! Tu veux me faire les poches, répliquai-je en la repoussant.
- Allons, ne fais pas le méchant, je te dis l’avenir contre un verre d’eau d’aff.
J’avais froid, le Lapin Volant était encore loin, et les bohémiens peuvent vous apprendre beaucoup de choses. C’est ce que mon père prétend. Voyant que j’étais tenté, la fille me reprit le bras :
- Viens chez Polidori, c’est à côté. Il a du parfait-amour, c’est bon comme si les anges te pissaient dans la bouche.
La drôlesse savait trouver les mots qui touchent.
Cinq minutes plus tard, nous étions attablés chez Polidori devant un verre d’une étrange liqueur violette. J’en pris une lampée et je fis claquer ma langue. Fameux, le parfait-amour.
Une boule de feu qui vous descend jusque dans le ventre.
La fille qui me faisait face était une vraie bohémienne au teint cuivré, pouilleuse et effrontée. Lavée et correctement habillée, elle aurait pu être belle, mais elle aurait toujours senti l’enfer. Je lui tendis ma main droite.
- En voilà une main de paresseux ! Bien douce, bien lisse !
- Je suis un employé de bureau, ricanai-je.
De la main gauche, je m’envoyai une autre lampée dans le gosier.
- Ta ligne de cœur est longue, me dit la fille.
Elle suivit la ligne du bout de son ongle sale et je ne pus m’empêcher d’imaginer que Léonie de Bonnechose m’attendait au bout de ce sillon.
- Tu es fidèle en amour, c’est une bonne chose.
Malgré moi, je tressaillis.