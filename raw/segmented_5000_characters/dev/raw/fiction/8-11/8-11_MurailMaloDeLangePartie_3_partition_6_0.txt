La brigade de Sûreté eut fort à faire durant tout le mois de février avec une bande de pickpockets à laquelle je m’étais associé pour les prendre sur le fait. Il y eut quinze arrestations.
Grâce à Gaby, qui surveillait toujours l’hôtel rue du Roi-deSicile, nous arrêtâmes aussi Tellier, le faux-monnayeur. Pauvre gars ! Il serait sans doute guillotiné. Frédo nous signala encore quelques meurtres de chiens jusqu’à ce que monsieur Personne, un jour de mars, m’indique une piste :
- Il y a un endroit où on peut se procurer des chiens…
- La fourrière ! m’exclamai-je, étonné de ne pas y avoir pensé.
La fourrière se trouvait rue de Pontoise. De l’extérieur, avec son porche monumental et son entrée pavée, elle ressemblait à une caserne. Devant la loge du concierge, un pinson, auquel on avait crevé les yeux, chantait à qui mieux mieux.
- Monsieur Joseph ? demandai-je.
C’était le nom d’un ancien prisonnier de Bicêtre à qui monsieur Personne avait trouvé une place de gardien à la fourrière.
- Il donne la pâtée aux bêtes, me répondit le concierge.
Les jappements me guidèrent jusqu’au chenil, qui s’étirait des deux côtés d’une allée. Dans chaque cage grillagée, il y avait un chien, chien de race tout surpris d’être là ou pauvre corniaud habitué à la méchanceté humaine. Quelques chiots se jetèrent gaiement contre le grillage à mon passage, quémandant une caresse ou bien la liberté.
- Monsieur Joseph ?
Un solide gaillard à grosses moustaches me fit face, l’air renfrogné.
- Je viens de la part de monsieur Personne.
- Présent, fit le gaillard avec un salut militaire.
Mon père avait un peu partout dans Paris des hommes prêts à tout pour lui. Mais les chiens aboyaient de rage à la vue du seau où leur nourriture attendait.
- Donnez-leur à manger, dis-je. Nous parlerons après.
Tandis que monsieur Joseph terminait sa tournée, je parcourus l’allée jusqu’au bout. J’eus une petite émotion devant une des cages où un roquet reposait sa fine tête noire sur ses pattes avant.
- Fiston ?
Ses oreilles frémirent. Était-ce lui ? Si ce n’était pas lui, c’était son frère…
- C’est un jeune, fit la voix de monsieur Joseph dans mon dos. Il aura pas vu grand-chose de la vie.
- Qu’est-ce qui va lui arriver ?
- C’est son jour d’être pendu.
- Pendu !
J’avais eu un recul d’horreur.
- C’est pas moi qui fais le règlement, mon petit monsieur, se justifia Joseph. On garde les chiens trois jours, et si on vient pas nous les réclamer, on les pend, tenez, à cette poutre où y a des crochets !
Je me souvins alors de ma mission :
- Est-ce qu’il n’y a pas quelqu’un qui est venu réclamer un chien… plusieurs chiens ces derniers temps ?
- C’est Personne qui veut savoir ça ? Hum…
Je compris que monsieur Joseph hésitait à m’avouer quelque chose.
- Peut-être j’aurais pas dû… Mais c’étaient des chiens sans collier.
Il avait sans doute reçu un peu d’argent en échange.
- Vous avez bien fait, le rassurai-je. Après tout, ces chiens allaient être pendus.
Leur destinée n’avait pas été meilleure, mais je n’en dis rien à Joseph. Je voulais l’encourager à parler :
- À quoi ressemblait ce monsieur ?
- C’était pas un monsieur. Plutôt un sauvage.
- Un bohémien ?
Le gardien acquiesça.
- Il ne vous a pas dit son nom ?
- C’est pas un bavard.
- Quand est-il venu pour la dernière fois ?
- Y a bien dix jours de ça.
- Il vous a pris des chiens ?
- Deux. Il dit qu’il les dresse et qu’il les revend comme chiens savants.
Tout en parlant, Joseph regardait autour de lui comme s’il avait peur de voir surgir le « sauvage ».
- S’il revenait, dis-je, vous pourriez essayer d’en savoir un peu plus long sur lui ?
Nous étions à présent sous l’auvent où avaient lieu les pendaisons. Je levai la tête vers les gros crochets rouillés et j’imaginai Fiston, pendant tout flasque au bout de la corde.
Je l’ai dit, je ne fais pas de sentiment avec les bêtes. Mais tout de même, c’était Fiston.
- Et pour le jeune roquet… La patronne du Lapin Volant a perdu un chien dans ce genre. Ça lui ferait plaisir si…
- Oh, prenez-le, prenez-le ! s’empressa Joseph. Je vous donne même la laisse et le collier.
Il sortit le chien de la cage par la peau du cou et le coinça entre ses genoux pour lui passer le collier. Fiston couinait de frayeur.
- Tenez-le ferme. Il est à vous, me dit Joseph, la mine satisfaite, en me tendant la laisse.
Dès qu’il changea de main, Fiston se débattit en aboyant.
- Bottez-y le cul, me conseilla Joseph qui, voyant que je n’osais pas, joignit le geste à la parole.
Fiston s’aplatit en gémissant, et j’eus la certitude à cet instant que je n’avais pas fait une bonne acquisition.
Tandis que je tirais sur la laisse pour remettre Fiston debout, Joseph se pencha vers moi et me chuchota précipitamment :
- Il s’appelle Orson. Il vit avec sa tribu après la barrière Saint-Denis. Mais faites-y attention, on dit qu’il s’est vendu au diable.
- Merci du renseignement, répondis-je, plus pré-occupé par mon roquet que par les âmes damnées. Mais debout, crétin, je viens de te sauver la vie !