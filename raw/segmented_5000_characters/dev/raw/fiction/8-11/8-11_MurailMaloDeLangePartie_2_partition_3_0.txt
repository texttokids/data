À peine remis de mes émotions, je partis rue Bridaine, où logeaient Trois-Guiboles et sa famille. Pour que je puisse surveiller le caroubleur, monsieur Personne m’avait loué la chambre à côté de sa mansarde. Le précédent locataire y avait fait un trou dans le mur pour se rincer l’œil quand la fille Guibole se rinçait tout court.
Trois-Guiboles n’avait pas toujours été malhonnête. Il avait commencé en gagnant sa vie comme son père qui était serrurier. Il était plutôt beau gars, blond aux yeux doux, mais avec une patte tordue qui l’obligeait à s’aider d’une canne. Il aimait les filles et il s’était marié de bonne heure avec la Louise, qui n’avait pas tardé à lui pondre un mioche par an. Mais ils avaient de la malchance et les enfants mouraient l’un après l’autre, sauf Fanny qui leur était restée. Puis Trois-Guiboles était allé un peu trop souvent au cabaret, il y avait rencontré des mauvais zigs qui semblaient avoir toujours de l’or plein les poches. C’était une bande de cambrioleurs et, pour un peu d’alcool, Trois-Guiboles leur avait fabriqué des clés. Dès qu’ils avaient été arrêtés par monsieur Personne, les cambrioleurs avaient dénoncé le caroubleur, en espérant que cela leur vaudrait un allègement de leur peine. Ils avaient tous eu droit à quinze ans de bagne.
Pendant ce temps, la femme Guibole était tombée dans la misère et la fille Guibole sur le trottoir.
Ce soir-là, la famille était au complet. Louise, qui n’avait pas plus de quarante ans, avait déjà les cheveux entièrement blancs. Les dames de la paroisse lui donnaient parfois un petit travail de couture et c’était le seul argent qui rentrait dans la maison. Trois-Guiboles se tenait le plus souvent devant son établi, comme tout de suite, les bras ballants, attendant de l’ouvrage qui ne venait pas. La fille Guibole avait seize ans et un bébé. Elle était jolie mais elle avait les coins de la bouche en berne, comme si elle était toujours sur le point de pleurer. On l’appelait Nini la farceuse. Sa petite était née deux ans plus tôt, mais elle ne marchait toujours pas, et à cet instant même, elle rampait sur le carrelage glacé. On était au mois de janvier, et le seul chauffage, c’était la chandelle qui permettait à la Louise de coudre à dix heures passées. Des coups frappés à la porte vinrent troubler cet aimable tableau de famille. Les parents échangèrent un regard effrayé.
- Faut-i ouvrir ? demanda Nini à mi-voix.
Sans attendre de permission, Lanturlu entra :
- Salut, la compagnie ! Te v’là donc sorti du bagne, Guibole ?
- Et vous, vous allez sortir de chez nous, répondit la femme d’une voix que la colère faisait trembler.
- Vous proutez pas*, ma bonne dame. Je sors, je sors ! (* Ne vous fâchez pas !)
Lanturlu se tourna vers Trois-Guiboles :
- Viens t’envoyer un coup de paff ’ !
- Père, n’y va pas ! s’écria Nini. Tu sais ce qu’on dit de lui.
Lanturlu fit deux petits pas sautillants pour se rapprocher de la jeune fille :
- Je serais curieux de savoir ce que les autres bavent sur moi.
Nini s’était reculée, mais elle eut tout de même le courage de répondre :
- On dit que vous étiez le chef des chauffeurs d’Orgères. On dit que personne vous a dénoncé parce que vous tuez ceux qui en savent trop.
- Est-ce qu’on dit que j’agrandis la bouche de ceux qui parlent trop ?
Lanturlu avait pris le ton de la plaisanterie, mais il avait sorti le couteau.
- Laisse, laisse-la, laisse-les ! s’affola Trois-Guiboles. Ah, malheur ! Je serai jamais tranquille !
Il attrapa sa canne, résigné à suivre l’assassin.
- Tu retourneras au bagne, lui murmura sa femme, tentant de le retenir par la manche.
- Eh bien, au moins, vous aurez la paix.
Lanturlu et lui allaient sortir. Je quittai mon poste d’observation. Il y avait une taverne rue Bridaine. Je fis le pari que c’était là qu’ils iraient boire et je courus m’y installer avant eux. J’étais propre et bien mis. Si Lanturlu avait aperçu Tortillard la nuit passée, il ne le reconnaîtrait pas dans Malo.
J’avais vu juste. J’étais à peine assis dans la taverne que Lanturlu et Trois-Guiboles y entrèrent. Ils s’installèrent trop loin de moi pour que je distingue leurs paroles. Mais je vis Lanturlu pousser un petit paquet sur la table vers TroisGuiboles, qui le regarda sans le prendre. Ah, s’il l’empochait, c’en était fini de son honnêteté. Mais aussi de sa vie.
Lentement, Trois-Guiboles approcha la main du paquet qui contenait l’empreinte à la cire. Lanturlu fit mine de s’en désintéresser et commanda à boire. Le paquet disparut dans la poche du caroubleur. Alors, je pensai à sa famille, et surtout à Nini la farceuse. Elle avait tenu tête à Lanturlu et il s’en souviendrait.
- Prévenir le chef, fis-je entre mes dents.
Je ne devais pas revoir monsieur Personne avant le mardi suivant. Mais d’ici là, la carouble serait fabriquée, le cambriolage effectué et Trois-Guiboles supprimé. Aussi, le lendemain matin, je me rendis à la librairie de la galerie Véro-Dodat et la traversai tranquillement en direction du bureau de monsieur Personne.