- Pour le bal des Tuileries, je mettrai le Golconde !
- Oui, oui, il y aura du monde, chevrota le duc.
- Non, le Golconde ! ! !
Monsieur Personne m’avait appris que le duc possédait une collection de diamants qui faisait rêver tous les voleurs de Paris. Le plus beau, monté en sautoir, pesait 140 carats et s’appelait le Golconde. C’était un diamant bleu qu’un lord anglais avait rapporté des Indes et qui coûtait à lui seul douze millions de francs. D’après les racontars des domestiques, les soirs où madame portait le Golconde, elle devait le rendre au duc après le bal et il le gardait avec lui dans sa chambre, attendant le lendemain matin pour le faire reporter à son notaire, qui l’enfermait dans un coffre. Pour voler le Golconde, Lanturlu devrait donc choisir un soir comme celui-ci. Et pourquoi pas ce soir même ?
Ma maîtresse étant partie, je me crus libre de circuler dans l’hôtel d’Écourlieu, que je visitai depuis les combles jusqu’aux écuries. En l’absence du duc, je me faufilai dans ses appartements. Sa chambre était au rez-de-chaussée, peutêtre parce qu’il ne pouvait plus monter les escaliers. Les fenêtres, qui donnaient sur la rue, étaient grillagées. Je jetai un regard à travers les barreaux et j’aperçus de l’autre côté, sous un porche, un mendiant auquel manquait un bras. Que faisait-il là ? Comment pouvait-il espérer gagner quelque aumône dans une rue si peu passante ? J’aurais voulu me persuader que c’était un homme de la bande de Lanturlu en train de faire le guet. Mais j’avais reconnu ce que mon père appelle « le coup du manchot » et qu’il m’avait enseigné : on plaque le bras gauche contre la poitrine et on l’attache avec une ceinture, puis on met quelques chiffons dans la partie supérieure de la manche vide pour figurer le moignon.
Ce faux mendiant sous le porche était un agent de la Sûreté.
Et je savais très bien pourquoi il était là. Pour me protéger !
Mon père continuait de me traiter comme le petit garçon sur lequel il devait veiller.
- Son lieutenant, tu parles ! fis-je entre mes dents.
Je ressortis des appartements du duc et restai un moment devant sa porte à ruminer ma colère.
- Mais que faites-vous ici ? s’écria une rude voix.
C’était Antonin, le valet de chambre de monsieur le duc.
Je balbutiai que ma maîtresse était sortie.
- Et alors ? Avez-vous rangé les affaires de madame, avez-vous nettoyé ses brosses, avez-vous remis dans ses plis sa robe de bal ?
Je n’aurais jamais imaginé que jouer à la poupée pût occuper toute une journée. J’avais à peine fini mes rangements que madame me sonnait. Elle me jeta à la figure son feutre et sa cravache. Puis je dus m’agenouiller pour lui ôter ses chaussures crottées. Ensuite, ce fut le bain de madame, et de nouveau, le coiffeur de madame, l’habillage de madame.
Un jeune groom vint porter le Golconde dans son écrin de la part de monsieur le duc.
- Attache le fermoir, Suzanne.
Je m’exécutai tout en admirant dans le miroir l’énorme diamant bleu entre les seins de ma maîtresse.
- Eh bien, as-tu fini de me regarder ?
- Ce n’est pas mon regard qui vous usera la peau, comme disait le squelette à l’écorché.
Pendant un instant, j’avais oublié que j’étais Suzanne. Je m’attirai le reflet, dans le miroir, d’un sourire carnassier. Puis du bout des doigts, madame me congédia. Mais il ne faut pas croire que la journée d’une femme de chambre s’achève au moment où sa maîtresse part s’amuser dans le monde.
Quand elle revint du bal, madame me sonna pour que je la déshabille. Puis elle remit le Golconde dans son écrin et me demanda d’aller le reporter au duc d’Écourlieu.
Quand j’entrai dans sa chambre, le vieux bonhomme était déjà en chemise et bonnet de nuit.
- Ah, le Golconde ! s’écria-t-il en apercevant l’écrin.
Mets-le dans le tiroir de ma table de nuit, petite… heu…
quel est ton nom ?
- Suzanne, monsieur, fis-je avec mon invariable révérence.
- Suzanne. C’est bien joli. Celle qui était là avant toi s’appelait aussi Suzanne. Une brave enfant, très gentille avec moi… Allons, ne te sauve pas si vite !
Son regard était trouble et son grand nez plus enflammé que jamais. Même si je ne risquais pas de tomber enceinte comme la Suzanne précédente, je n’avais pas l’intention de m’attarder.
- Bonne nuit, monsieur.
- Ah, petite coquine, ne veux-tu pas gagner une belle pièce d’or ? fit-il en tendant vers moi ses bras débiles.