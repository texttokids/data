Nous arrivâmes au dépôt de Pont-à-Lezen le 18 février 1834. Je fus libéré de mes fers, plongé avec Mouchique dans une grande cuve d’eau tiède, puis examiné par un médecin.
Enfin, on me rasa la tête et on me donna des vêtements marqués aux lettres GAL pour galérien : une casaque de laine rouge, une chemise de toile blanche, un pantalon jaune et des souliers ferrés. Mouchique eut droit au bonnet vert des condamnés à perpétuité et moi au bonnet rouge des condamnés pour un temps. L’argousin forgeron revint alors pour me fixer la manicle à la cheville droite. C’était un cercle de fer auquel pendait une chaîne. On m’inscrivit dans le registre du bagne comme suit : « Quincampoix Malo, 18 ans (nom et âge supposés), blond, yeux bleus, marqué à la fleur de lys. » Je reçus le matricule 21 793, et avec cent quarante-cinq gars pareillement rasés, pareillement vêtus, traînant la chaîne de la jambe droite, je franchis les portes du bagne de Brest entre deux canons pointés vers l’océan. Derrière les murs de cette forteresse qui était comme une ville avec ses officiers, ses gardes, ses médecins, son aumônier, et plus de quatre mille prisonniers, je découvris la vie des forçats, la mienne désormais.
- Lever, cinq heures, coucher, neuf heures. Repas midi et soir : pain noir, fèves et légumes, la viande et le vin sont réservés à ceux qui vont à la grande fatigue.
Nous étions rassemblés dans la cour principale tandis que le commissaire du bagne nous débitait le règlement sur le ton d’une directrice de pensionnat :
- Ceux qui tentent une évasion : trois ans de bagne supplémentaires. Ceux qui se battent, qui refusent d’obéir : bastonnade. Ceux qui jurent ou oublient de saluer les chefs : cachot. Si vous vous conduisez bien, vous aurez droit à un matelas et une couverture.
Je me tenais au milieu des forçats du premier cordon qui s’étaient regroupés d’eux-mêmes, et je tâchais de les imiter, jambes écartées et bras croisés sur la poitrine. Ils savaient que, dès le lendemain, ils iraient à la « grande fatigue », au déchargement des bateaux ou au travail dans les carrières.
S’ils pouvaient voler, ils voleraient. S’ils pouvaient s’évader, ils s’évaderaient. S’ils devaient tuer, ils tueraient.
Après le discours du commissaire, ce fut le moment du mariage, comme disaient les anciens en ricanant. On allait nous accoupler deux par deux par la manicle. À qui mon sort serait-il lié jour et nuit ? J’aurais voulu que ce fût Mouchique. Je sentis que quelqu’un me tirait par la manche.
J’eus un sursaut de dégoût en voyant que c’était un garde.
- Je viens de la part de Personne, me dit-il le plus bas qu’il put.
Enfin ! Enfin, mon père se souvenait de moi.
- Je peux te mettre à la manicle avec un gars tranquille.
C’était tout ce qu’il avait à m’offrir ?
- Je veux être avec Mouchique, répliquai-je.
- Mouchique ? L’empoisonneur ?
- Oui.
- Mais… il est dangereux.
- Je n’ai pas trouvé mieux, comme disait la demoiselle à qui le curé demandait si elle voulait prendre pour époux monsieur Martin ici présent.
Le garde resta un moment interloqué, avant de se décider :
- Après tout, c’est ton affaire.
Tandis que le forgeron reliait nos deux chaînes par trois anneaux de fer, j’observai Mouchique à la dérobée.
Il mâchonnait un brin de paille, l’air indifférent. Sur le registre, au moment où l’on m’inscrivait, j’avais pu lire ceci : « Tinchebray Raphaël dit Mouchique, 22 ans, brun, yeux noirs, dragon tatoué sur l’omoplate. » J’avais été stupéfait de lui savoir huit années de plus que moi. Sa petite taille, ses épaules tombantes, ses bras fluets lui faisaient le corps d’un jeune garçon.
- Je te préviens, me dit-il de sa voix chantante, je n’ai pas l’intention d’être entretenu trop longtemps par la nation.
- Tu veux t’évader ?
Il ricana.
- Je ne te demande pas de m’aider, mais de te taire. Tu as vu comment finissent les moutons ?
- Pourquoi tu me menaces ?
- Ce n’est pas toi qui as voulu te rendre aux argousins ?
Voilà donc ce qu’il pensait de moi. Que j’étais un lâche.
Je me baissai et sortis de mon soulier le sou creux qui contenait un louis d’or.
- Avec ça, dis-je, on pourra acheter une lime et des vêtements. Tout s’achète ici.
C’était ce que m’avait raconté mon père, qu’il suffisait de payer les gardes pour s’assurer de leur complicité.
- Donne-moi cet argent !
Mouchique tendit la main comme s’il était sûr que j’allais lui obéir. Et d’ailleurs, c’est ce que je fis. Avec l’or que m’avait donné mon père, Mouchique commença par nous acheter un « ménage » : deux gamelles de bois, un tonnelet de vin, deux couvertures et deux « serpentins », qui sont des matelas à dérouler sur la planche du lit. Ainsi, nous avions l’air de deux braves garçons seulement soucieux de nous installer. Nous devions faire oublier notre réputation de fortes têtes.
Il nous fallut attendre le lendemain pour savoir si nous serions employés à la grande ou à la petite fatigue. Le commissaire avait entendu parler des poupées de paille de Mouchique et il lui demanda d’en fabriquer une.