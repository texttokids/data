- Oui, c’est ce que je lis sur le rapport du commissaire Jacquot. Quincampoix Malo, demeurant 6 rue Bridaine, soupçonné d’avoir gardé pour lui le Golconde qu’il devait remettre à monsieur le duc d’Écourlieu. Alors, qu’en avezvous fait ?
- J’y ai dit : bas les pattes ! Et je me suis sauvé.
Le juge Dumortier fit un petit bond sur sa chaise :
- De quoi parles-tu ?
- De m’sieur le duc. C’est un vieux cochon.
- Et c’est pour ça que tu l’as tué ?
- Ben, non, il s’a pendu comme on pend les andouilles dans la ferme à mon père.
Le juge se mit à ricaner, comme s’il avait décidé de s’amuser de la situation, mais ses mains étaient en train de faire de la charpie avec le procès-verbal du commissaire Jacquot.
- Tu as tort de te payer ma tête. Je n’avais pas l’intention de t’accuser d’assassinat. Mais d’après le médecin, ce suicide n’est pas vraisemblable. Donc, voilà comment je vois les choses : tu as fait semblant de mettre le diamant dans le tiroir de la table de nuit, mais tu n’as mis en fait que l’écrin.
Le duc s’est aperçu du tour que tu voulais lui jouer. Alors, tu l’as étranglé avec sa cravate, puis suspendu à la fenêtre…
- Et pis j’ai fermé le verrou et j’ai passé z’à travers le mur, dis-je avec l’air d’admirer mon exploit.
Le juge changea de tactique et m’attaqua sur mon déguisement : n’était-ce pas la preuve d’une préméditation ?
- C’est la faute à madame l’intendante, répondis-je. Elle voulait pas d’un valet, mais elle voulait bien d’une chambrière.
- Et ce pistolet, ce couteau, pour quoi faire ?
- On m’a dit à la ferme : méfie-toi des brigands à Paris.
Et vous voyez qu’ils avaient raison. Y avait un brigand cette nuit chez m’sieur le duc.
Au bout d’une heure d’interrogatoire, le juge Dumortier parut renoncer à tirer de moi quoi que ce soit.
Le lendemain matin, j’eus droit à la promenade des prisonniers.
- Tu vas voir, y a du beau monde dans la grande cour, me dit le concierge avec ce respect bizarre qu’il avait pour les bandits. Y a le petit Griffon qu’a déjà crevé les z’œils à deux collègues et y a Bec-Fin qu’est gerbé à vioque*. (* emprisonné à vie)
Il ajouta, de l’air content d’un hôtelier qui vient d’accueillir un client distingué :
- Ah, et nous avons le beau Guédon qu’est arrivé d’hier !
Le beau Guédon, ce monstre borgne qui étranglait les gens de son unique main !
- C’est sûr que, cette fois, il est bon pour l’abbaye de Monte-à-Regret. Il a étranglé deux vieux qu’étaient riches comme des Crésus.
J’avais beau me répéter que mon père savait où j’étais et qu’il veillait sur moi, je me sentis bien misérable en entrant dans la cour où retentissaient des cris, des rires et des chants qui n’avaient rien de joyeux. Je connaissais par cœur ce genre d’histoires où un pauvre gars, qui a volé trois artichauts dans un champ pour ne pas mourir de faim, se retrouve livré dans la prison à des grinches féroces. Ses vêtements, on les lui vole à la nuit tombée, s’il a une boucle à l’oreille, on la lui arrache avec l’oreille, s’il se plaint, on le bat, et s’il ose dénoncer ses bourreaux, on le retrouve pendu aux barreaux de la cellule.
- Veux-tu que je te présente au capitaine Trompe-laMort ? me proposa le concierge. Il sort demain de prison. Si tu lui dis ousque t’as planqué le Golconde, il va te le chercher et vous faites part à deux !
Je remerciai vivement le concierge pour son offre, puis je lui fis comprendre que je préférais rester seul. Je m’éloignai de lui au plus vite et je choisis le coin le plus tranquille de la cour pour me donner des tapes dans le dos, car il faisait un froid piquant. Mais je sentais sur moi quelques regards de curiosité.
- T’entraves l’arguche, momacque ? fit une voix dans mon dos.
Je me retournai et vis à trois pas de moi un jeune gars, court sur pattes et très sale. Devais-je lui répondre que je comprenais l’argot ou continuer à jouer mon rôle d’idiot ?
Voyant que j’hésitais, le gars crut m’avoir impressionné :
- T’as taf de mézigue* ? (* T’as peur de moi ?)
Le gars n’était pas venu me faire la conversation, il cherchait la bagarre. C’était sans doute le prix à payer quand on était nouveau venu. Mieux valait celui-là qu’un autre, car il n’était pas bien gros. Soudain, il s’élança vers moi, les mains en avant, repliées comme des serres. Il avait des ongles immenses et noirs, taillés en pointe.
- Vas-y, Griffon ! l’encouragea un collègue. Crèves-y un œil !
Griffon n’avait sûrement jamais entendu parler de l’art de la savate, mais moi, je m’y entraînais depuis deux ans.
Il parut très surpris quand mon pied, l’atteignant en plein ventre, le repoussa d’un bon mètre. Il serait tombé à la renverse si le camarade ne l’avait rattrapé par les épaules.
- Tu veux-ti de l’aide ? lui proposa le nommé Bec-Fin, qui faisait bien trois fois mon poids.
- Pas de ça ! dit une voix aussi agréable à entendre qu’une poulie qui grince. C’est un combat d’homme à homme.