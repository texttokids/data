Mais la servante de mademoiselle de Bonnechose ne se déridait pas.
- Enfin, Margote, qu’est-ce qu’il y a ?
- Pour ainsi dire, il s’est déclaré, m’sieur Malo.
- Quoi ?
J’avais rugi. Margote se recula.
- Oh, je savais ben que ça vous ferait pas plaisir. Mais c’est vot’ faute, aussi ! Six mois que vous êtes parti… Ma maîtresse, elle a pleuré que c’en était une vraie misère. Tous les jours, elle me demandait : «Alors, on a-ti des nouvelles ? » Mais je n’écoutais pas cette pleurnicheuse de Margote.
J’étais fou de rage :
- C’est ce pantin de Furme d’Aubert, c’est ça ? Il a parlé d’amour à Léonie, c’est ça ?
- Dame ! C’est son cousin. Il l’aime depuis toujours, qu’il dit.
- Et elle ? Elle ? criai-je. Qu’est-ce qu’elle dit ?
- Elle dit qu’il doit patienter.
Léonie n’avait que quinze ans, mais comme me l’apprit Margote, monsieur et madame de Bonnechose voyaient d’un bon œil se nouer cette idylle entre leur fille et leur neveu. Monsieur Furme d’Aubert était le très honorable préfet de police de Paris et, surtout, sa femme, née d’Écourlieu, venait d’hériter la fabuleuse fortune de son oncle. Car même sans le Golconde, qui avait définitivement disparu, l’héritage du duc d’Écourlieu restait considérable.
- Margote, je t’en supplie, Margote, il faut que je voie Léonie. Je lui expliquerai…
Mais que pouvais-je lui expliquer ? Que j’avais été condamné à vingt ans de travaux forcés, que j’étais un bagnard en rupture de ban, et l’ami de gens distingués tels que Coco l’haricot et Mouchique l’empoisonneur ? Je priai Margote de me donner de quoi écrire et je rédigeai le message suivant : « Ma chère Léonie, pouvez-vous me retrouver demain à 10 heures sous le Gladiateur mourant ? Je vous expliquerai tout.» Cela me laissait le temps d’inventer une histoire à l’usage d’une jeune fille de la bonne société et d’aller chercher quelques affaires que j’avais laissées rue Bridaine. C’était la seule mission dont mon père m’avait chargé.
Car il ne voulait plus, m’avait-il dit, que je me mêle à la faune des agents.
Je m’aperçus tout de suite en revenant à mon poste d’observation que la pièce d’à côté était occupée. Par simple désœuvrement, je collai mon œil au trou dans le mur et j’eus la surprise de voir Nini la farceuse qui partageait un plat de pommes de terre avec sa petite Mouzette. Mon père m’avait-il menti en me disant qu’il avait envoyé la famille Guibole à Saint-Cloud ?
- Tiens, monsieur Malo, vous aussi, vous êtes revenu ? me dit Nini en m’ouvrant la porte.
Elle m’expliqua qu’elle voulait conduire sa petite à l’« hôpital des pauvres », comme le lui avait conseillé le médecin de Saint-Cloud qui avait refusé de la soigner.
- Mais regardez, monsieur Malo, elle marche maintenant !
Mouzette fit quelques pas vacillants sur ses deux jambes arquées avant de se laisser tomber sur les fesses. Nini ne semblait pas en meilleure santé que sa fille. Ses grands yeux vert d’eau lui mangeaient toute la figure.
- Et où c’est que vous étiez passé ? voulut-elle savoir.
Je me fis la réflexion que Nini était à peine plus âgée que Léonie et que je pouvais faire l’essai sur elle d’un récit de mon invention. Mais, curieusement, ce fut la vérité qui sortit de ma bouche, et bien m’en prit, car soudain Nini m’interrompit :
- Moïra ? J’ai connu une fille qui s’appelait comme ça.
Elle était servante dans un hôtel de rendez-vous pour des messieurs de la haute. Et voleuse comme pas deux !
À coup sûr, c’était elle. Il n’était pas difficile d’imaginer ce que ce vieux polisson de duc avait fait : ayant remarqué la beauté de la petite servante, il l’avait retirée de ce mauvais lieu qu’il fréquentait. Un ou deux professeurs avaient donné à Moïra un petit vernis d’instruction et de bonnes manières, et après quelques années passées chez une « parente » de province, la baronne de Feuillère était arrivée à Paris. Elle était désormais suffisamment bien élevée pour que le duc en fasse sa maîtresse, mais d’une origine si misérable qu’il n’en avait pas fait sa femme.
- Et toi, Nini, que vas-tu devenir à Paris ?
- Oh, moi, je vais me lever un riche Anglais sur les boulevards et je serai sa milady !
Le plus vraisemblable était qu’elle allait attraper une vilaine maladie ou tomber enceinte, ou même les deux, et finir avec sa Mouzette à l’hôpital des pauvres. Je lui tendis le louis d’or que monsieur Personne m’avait donné la veille.
Elle rougit, me remercia et commença d’ouvrir son corsage.
Je rougis à mon tour.
- Eh, non, non, non ! Je suis fiancé !
En fait, je n’en étais plus si sûr. Pour être fiancé, on a besoin d’être deux, comme disait Robinson à son perroquet, qui n’avait pas l’air intéressé.