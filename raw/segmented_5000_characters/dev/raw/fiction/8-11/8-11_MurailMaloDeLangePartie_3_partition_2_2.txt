Côme de la Trimbaldière avait trente ans et une réputation de séducteur. Depuis quelques semaines, il offrait des partitions de piano à Léonie, il lui faisait des compliments idiotsn vers. D’après Nini, Léonie faisait semblant d’être aimable avec lui pour ne pas fâcher ses parents. Mais à force de faire semblant, elle finirait peut-être par l’aimer vraiment… Il y a une chose pire qu’un rival qu’on connaît, c’est un rival qu’on imagine. Il fallait que je voie ce Trimbaldière, que je me rassure en lui trouvant un défaut, une voix de canard enroué ou un nez en pied de marmite. Car personne n’est parfait, comme disait le cyclope en avouant qu’il était myope.
Les Bonnechose donnaient une réception tous les vendredis. Je pris des gants blancs dans mon armoire de déguisements. Je me collai des favoris pour me vieillir, je glissai un petit coussin sous mon gilet, et je mis une perruque brune sur mes boucles blondes. J’étais méconnaissable, surtout que, quand un domestique vous tend son plateau, c’est le plateau qu’on regarde.
Nini m’accueillit à l’office. Elle m’expliqua que les trois salons avaient été ouverts pour les besoins de la réception.
Je pris un plateau d’argent et j’entrai dans le premier salon, le plus brillamment éclairé. J’offris un verre d’orangeade à quelques dames tout en cherchant celle de mes pensées. Et je la vis : Léonie menue, modelée dans un nuage, Un rire moqueur aux lèvres, une rose au corsage…
Mon Dieu, cela devient grave, je fais des vers, moi aussi !
Bref, Léonie avait toujours ses yeux de jais étincelants, et ils étaient une douzaine à se presser autour d’elle comme des mouches autour d’un pot de miel. Mais ce n’étaient que de très jeunes gens, aucun d’eux ne pouvait être le baron Côme de la Trimbaldière. Pour m’éviter d’aller les boxer, je passai dans le salon suivant où des vieillards jouaient au whist pendant que des grosses mamans aux jambes fatiguées se reposaient devant la cheminée. Toujours pas de Trimbaldière.
Le dernier salon était plongé dans la pénombre et je crus d’abord que personne ne s’y était aventuré. Mais un bourdonnement de voix me détrompa. M’approchant de la porte, j’entraperçus un cercle de messieurs tous semblables dans leurs habits noirs et je reconnus la voix de monsieur de Bonnechose qui disait :
- … et donc, entre la poire et le fromage, il lui glissa : « Je suis bonne poire. Coupons la poire en deux.» Des éclats de rire saluèrent ce qui devait être une histoire drôle ridiculisant Louis-Philippe et sa tête en forme de poire.
- Mais dites-moi, n’est-ce point l’heure de retourner au grand salon ? fit une voix chevrotante.
- Vous avez raison, monsieur de Liechtemberg, répondit le maître de maison. Trimbaldière doit nous amener… qui vous savez. Retournons au grand salon. Liechtemberg, prenez mon bras.
Je me dépêchai de partir en cuisine et je chargeai mon plateau de glaces et de biscuits. Je revins au moment où un domestique annonçait d’une voix solennelle :
- Monsieur de la Trimbaldière ! Monseigneur le duc de Normandie !
Il y eut quelques murmures d’étonnement : « Qui ça ?
Le duc de quoi ? » Le monseigneur annoncé était un gros rougeaud d’une cinquantaine d’années, marchant les cuisses écartées. Moi, c’était Côme de la Trimbaldière qui m’intéressait. L’œil de braise avec des cernes de débauché, la bouche cruellement dessinée, il me dépassait de la tête et des épaules. Mon père avait raison de me traiter en enfant.
Trimbaldière, lui, était un homme. Il s’avança vers le cercle de graves messieurs en habit noir et dit :
- J’ai le privilège de vous présenter monseigneur LouisCharles, dauphin de France.
Ce ne furent pas ces mots, pourtant étranges, qui me pétrifièrent, mais l’apparition de monsieur de Liechtemberg au bras de monsieur de Bonnechose. C’était un vieillard à demi paralysé, portant culotte courte et bas blancs. En un mot : papa. S’il posait le regard sur moi, mes favoris se décolleraient et mon ventre postiche glisserait à terre, j’en étais certain. Je lâchai mon plateau et m’enfuis en courant.
Le lendemain, je remâchai ma colère. Mon père aurait dû me prévenir qu’il menait une enquête sous le toit de ma fiancée !
- Je suis son lieutenant et il ne me dit pas la moitié de ce qu’il fait.
Gaby m’écoutait, le nez baissé sur son verre. Nous étions tous deux assis à une table du Lapin Volant.
- C’est un sournois, m’approuva Gaby. Tiens, savais-tu qu’il a une bonne amie ?
- Papa ! ?
- C’est une ponante* du Palais-Royal. (* prostituée)
Je ricanai :
- C’est le métier qui veut ça. Les filles lui donnent des informations sur les mauvais coups en préparation.
- Non, celle-là, c’est sa bonne amie, s’entêta Gaby. Elle s’appelle Caramel. Il paraît qu’elle a du sang de négresse dans les veines. Ça doit lui plaire. Moi, j’ai du sang de navet.