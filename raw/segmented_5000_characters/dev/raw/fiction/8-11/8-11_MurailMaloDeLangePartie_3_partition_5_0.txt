Après avoir vu tant de prodiges dans la même soirée, plus rien n’aurait dû m’étonner. Pourtant, je poussai un cri de surprise quand j’entrai dans ma chambre. Une forme rouge et noire avait surgi de derrière mon armoire de déguisements. Je me baissai pour récupérer le poignard lacé contre mon mollet.
- Attends, n’aie pas peur ! s’écria la bohémienne.
- C’est plutôt toi qui devrais avoir peur de moi, voleuse, empoisonneuse !
D’un même mouvement, la fille cambra les reins et rejeta en arrière les lourdes boucles noires de sa chevelure :
- Je suis cigaine*. Est-ce que tu vaux mieux que moi ? (* tsigane)
Je me revis en train de voler le billet d’entrée dans la poche du bourgeois et je me mis à rire. La fille joignit son rire au mien et me montra mon armoire de déguisements :
- Tu es voleur ou policier ?
- Moitié-moitié.
Ma colère était retombée. Elle s’assit au bord du lit, et moi aussi.
- Écoute, me dit-elle, j’ai volé ce cœur d’enfant parce qu’on m’a promis de l’argent.
- Qui ça ?
Elle fit le signe de la croix, puis répondit très vite :
- Je ne le connais pas. Il porte un masque. Il est très grand. Il mange les cœurs. C’est le benk.
- Le benk ?
De ses index dressés, la cigaine se fit des cornes de diable.
- Je ne veux plus lui obéir. Mais il a d’autres serviteurs, aussi mauvais que lui. Il y a celui qui lui apporte des chiens.
- Qui est-ce ?
- Il se fait passer pour aveugle.
- Dis-moi son nom.
- Orson.
- Il est cigain, lui aussi ?
- Oui, et c’est un dresseur d’ours. Les animaux, il les connaît.
- Où est-ce que je peux le trouver ?
La fille se signa de nouveau :
- Chez le benk.
- Et où il est, ton benk ?
- En enfer.
Elle se leva, jugeant m’en avoir assez dit. Je la saisis par le poignet :
- Comment t’appelles-tu ?
- Zina.
- Où est-ce que je peux te trouver ?
- Nulle part. Je reprends la route avec les miens.
Nous nous regardâmes au fond des yeux.
- Laisse-moi partir. Je suis voleuse, c’est vrai, mais je ne veux pas faire le Mal. Je t’ai prévenu. Maintenant, tu sais qui est ton Ennemi.
Je relâchai son poignet.
- Que sainte Rita te protège, me souffla-t-elle à l’oreille.
Une fois sur mon seuil, elle se retourna :
- Méfie-toi d’une fille du Palais-Royal. Elle s’appelle Caramel.
- Hein ? Pourquoi ?
Mais Zina s’enfuit, rouge et noire dans la nuit. Cette visite nocturne me laissa abasourdi. Le benk, Orson, Caramel… Et qu’est-ce que mon père venait faire dans tout cela ? La seule personne à qui je pouvais en parler, c’était Gaby.
- Ah, tu vois, triompha-t-elle, je savais que cette Caramel était mauvaise !
- Tu ne savais rien du tout, tu étais jalouse, ricanai-je.
Ce qu’il faudrait à présent, c’est prendre des renseignements sur elle.
- C’est fait… De son vrai nom, elle s’appelle Alberte Brunet, elle a l’accent du Midi. Elle a d’abord été une fille à ouvriers. Puis elle a fait la connaissance du vieux marquis Victor de Montval, qui l’a sortie de sa misère et qui l’a installée au 161 de la galerie Valois.
- Nom d’unch ! Le 161, c’est à côté du théâtre du magicien !
Oh, comme c’était étrange, étrange et menaçant…
- Il faut mettre ton père en garde. Il voit cette fille tous les jeudis après-midi. Il faut lui dire qu’elle est dangereuse.
- Mais il le sait sûrement. C’est une moucharde, cette Caramel. D’un côté, elle est au service du benk, et de l’autre, elle donne des renseignements à mon père. C’est le métier, Gaby.
Elle soupira et, soudain, comme si elle prenait une décision, elle ôta sa casquette. Ses fins cheveux tombèrent en pluie dans son cou et le long de ses joues.
- Tu les laisses repousser ? m’étonnai-je.
- Et je me suis privée de pain pour acheter une robe et des bas de soie.
- À quoi cela va te servir ?
- À me faire belle. Pour moi toute seule, devant le miroir de ma chambre.
Ses yeux s’emplirent de larmes.
- Je pourrais avoir des souliers, des rubans comme cette Caramel…
- … si tu faisais le même métier qu’elle. C’est ce que tu veux, Gaby ?
Une larme roula sur sa joue sans qu’elle me répondît.
Mon père parut content de me voir quand je passai en fin de matinée dans son bureau de la galerie Véro-Dodat.
- Eh bien, qu’as-tu appris sur le docteur Pelletan ?
- Rien. Mais je sais qui est le propriétaire du singe savant.
Je lui racontai ma soirée au théâtre du Palais-Royal et je conclus :
- Pourquoi Wizzard a-t-il remplacé le singe par un enfant ? Parce que Monsieur Jones est mort.
Mon père m’approuva d’un signe de tête. La langue me démangeait de lui parler aussi de Caramel. Mais je me demandais comment il prendrait mon indiscrétion. J’allais tout de même m’y décider lorsque je reconnus les deux coups légers frappés à la porte. C’était Frédo, l’oiseau de mauvais augure, qui annonça :
- Chef, un autre cabe refroidi !
Il en riait, l’abruti. Moi, je m’étais levé d’un bond en pensant : Fiston ! Mais pourquoi le benk l’aurait-il choisi, lui, parmi les milliers de chiens errant dans Paris ? Frédo avait fait porter le cadavre à la morgue.