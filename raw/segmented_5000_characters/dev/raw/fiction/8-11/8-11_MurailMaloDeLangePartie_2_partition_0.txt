PARTIE 2 : Fils de Personne 

Les faits que je vais vous révéler se sont déroulés de janvier à juillet 1834. À quatorze ans, j’étais le plus jeune agent secret de France. Comme tous ceux qui travaillent à la brigade de sûreté de Paris sous les ordres de monsieur Personne, je dois rester muet sur mes activités. Mais on ne peut pas toujours tout garder pour soi, comme disait le petit garçon trop gourmand avant de vomir sur ses parents.

1. Trucs et astuces d'un agent secret. - Cloche-Perce et crève-cœur.