Un moineau, à la rigueur ». Janvier et sa femme vinrent dire qu’ils me devaient leur bonheur et que j’étais le meilleur des amis, même s’ils ne me voyaient plus si souvent.
J’avais une affreuse envie de pleurer et d’aller me jeter dans les bras de ma tante ou dans ceux de Janvier. Enfin, par la petite porte de côté, on fit entrer un dernier témoin en ma faveur.
- Mademoiselle Léonie de Bonnechose. Approchez de la barre, mademoiselle.Vous jurez de parler sans haine et sans crainte, de dire toute la vérité, rien que la vérité ?
- Je le jure.
Elle était pâle et abattue, mais elle dit d’une voix ferme :
- Monsieur de Lange est un ami d’enfance. Il est incapable d’avoir fait ce dont on l’accuse. Depuis qu’il s’est évadé du bagne, il n’a eu de cesse de retrouver le véritable coupable.
- Diriez-vous, mademoiselle, fit maître Chanterelle d’une voix doucereuse, que monsieur de Lange n’a jamais rien volé ?
- Mais je…
Elle tourna vers moi le regard. Je lui avais raconté en riant comment j’avais pillé Le Vieil Elbeuf.
- Prenez garde, mademoiselle, de ne pas vous parjurer.
- Monsieur de Lange est innocent de tout ce dont on l’accuse.
- J’entends bien, mademoiselle, mais vous ne répondez pas à ma question.
Léonie porta la main à son front, balbutia quelques mots indistincts, et glissa à terre. Je fis un bond sur mon siège, mais, connaissant Léonie, je ne m’inquiétai pas trop. Elle avait trouvé la solution pour ne pas répondre à la question.
On appela les huissiers et on évacua ma fiancée. Les dames de l’assistance semblaient très émues, certaines portaient leur mouchoir à leurs yeux. Mais déjà l’avocat de madame Furme d’Aubert réclamait la comparution du témoin suivant, témoin à charge.
- Monsieur Hauchecorne. Avancez jusqu’à la barre, monsieur.
Cette fois-ci, j’étais perdu. Cet homme jura qu’il me reconnaissait. Il reconnaissait mes yeux, il reconnaissait mon nez. J’étais venu dans sa boutique déguisé en vieillard, mais il m’avait vu détaler comme un lapin, emportant avec moi une pièce de tissu en satin. J’agissais avec deux complices, mâle et femelle. Monsieur Hauchecorne en tremblait encore d’indignation. Cette farce allait me coûter ma tête, car il devenait évident dans l’esprit des jurés que j’étais un voleur récidiviste.
Dans la plaidoirie qui s’ensuivit, maître Chanterelle démontra que j’étais le voleur du Golconde, l’assassin du duc, un forçat en rupture de ban et le chef d’une bande de malfaiteurs. Il ne m’accorda aucune circonstance atténuante :
- Car, contrairement à ce que mon honorable collègue, maître Flambard, essaiera peut-être de vous faire croire, monsieur de Lange n’est nullement un enfant abandonné à la charité publique, mais un petit protégé de la Providence qui a été adopté par deux personnes très estimables, les demoiselles de Lange à Tours, et qui a choisi, oui, choisi, messieurs les jurés, de fuguer à l’âge de douze ans pour s’en aller vivre au Lapin Volant, un repaire de voleurs et d’assassins.
Tout ce que disait maître Chanterelle ressemblait tant à l’histoire de ma vie que j’en venais à douter de qui j’étais vraiment. Maître Flambard se leva à son tour et fit sa plaidoirie, dans laquelle il fut question de violette et d’ange déchu, mais pas de mon innocence. Quand vint son tour de parler, l’avocat général eut beau jeu de souligner que maître Flambard n’avait pas cherché à me défendre parce que j’étais indéfendable :
- Le jeune âge de monsieur de Lange ne peut être une excuse aux crimes odieux dont il s’est rendu coupable. La société doit parfois retrancher l’un de ses propres membres pour sa sécurité. Je demande la peine de mort pour l’accusé.
Des sanglots éclatèrent dans l’assistance, mais j’étais trop accablé pour en chercher la provenance. J’avais l’impression confuse d’être la victime d’une machination dont rien ni personne ne pourrait me sauver.
- Accusé, me demanda le président, avez-vous quelque chose à ajouter pour votre défense ?
À travers les larmes qui me brouillaient la vue, je le vis qui grimaçait en se tâtant l’estomac. Il avait hâte d’en finir.
J’aurais voulu demander pardon à monsieur Hauchecorne pour le tour que je lui avais joué, mais c’eût été tendre ma tête au bourreau.
- Bien, si vous n’avez rien à dire, je vais clore les débats…
- Un instant, monsieur le président ! clama une voix.