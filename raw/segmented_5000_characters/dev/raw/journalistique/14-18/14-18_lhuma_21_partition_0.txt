Fe. N.

médias télé

Dans l'actualité

Audiovisuel extérieur

Fusion... sur le Net ?

Alors que l'on attend pour la mi-novembre les conclusions demandées par Sarkozy sur la rationalisation

de l'audiovisuel public extérieur, Antoine Schwarz,

le président de RFI, s'est désolidarisé d'un projet évoqué

en interne de fusion de la radio internationale

avec la chaîne France 24 pour prôner, si ce n'est un holding chapeautant RFI, France 24 et TV5 Monde, du moins

la mise en place d'un site Internet unique.

Presse économique

Les CE haussent le ton

Ça se tend aux Échos : les élus du CE veulent engager des poursuites au pénal après avoir appris

que la direction avait rencontré les experts mandatés par ces derniers pour leur dire qu'elle contestait

et refusait le droit d'alerte déclenché début

octobre. En conséquence, ils réclament la tenue

d'un CE extraordinaire lundi sur le sujet.

Et, à la Tribune, les élus, face aux « zones d'ombre » des trois offres « fermes » de reprise, demandent,

eux, à ce que celle de Fabrice Larue, ancien patron

de DI Groupe (la filiale médias de LVMH), soit prise

en compte alors qu'elle n'est pour l'heure

que « conditionnelle ».

Reuters

Prolongation de l'enquête

Bruxelles rendra le 10 mars 2008, et non plus

le 25 février, les conclusions de son enquête

sur le rachat de Reuters par son concurrent Thomson en vue de la constitution du numéro un mondial de l'information financière.

médias télé

En télé, la vérité, c'est off ?

Et soudain, Pascal Bataille ! Non, le comparse de Laurent Fontaine ne veut pas nous parler de sa nouvelle émission C'est off, où, sur Jimmy, le duo, déguisé en Christophe Hondelatte, dévoile, pêle-mêle, les coulisses de Roland-Garros, les liens entre Ségolène Royal et Mitterrand ou la face cachée de LO. Même si, dix minutes auparavant, un ponte de leur société de production nous assurait que les animateurs n'avaient rien à nous dire, Bataille nous aura parlé une petite heure de 20 Minutes de bonheur, le documentaire d'Oren Nataf et d'Isabelle Friedmann sur les coulisses de l'émission qu'ils animaient jadis sur TF1, Y a que la - vérité qui compte. Un documentaire « collector » puis-que, après avoir été diffusé fin 2006 à Belfort, il vient d'être déprogrammé alors qu'il était à l'affiche le 9 novembre du festival organisé par les Cahiers du cinéma.

Les collaborateurs de Bataille et Fontaine (avec l'appui de ces derniers) figurant dans le documentaire ont en effet assigné en référé le magazine ainsi que le site d'Arrêt sur images (qui en diffuse des extraits) pour empêcher sa diffusion. L'audience doit avoir lieu lundi. Bataille, après nous avoir rappelé gentiment que les animateurs avaient déjà eu un « problème » avec l'Humanité il y a quelques années, dénonce en substance un film qui ne serait qu'à charge : « Si certains ont pu nous dire qu'on était fous parce qu'il ne faut jamais laisser personne entrer en cuisine, nous, on l'a fait parce qu'on n'avait rien à cacher. On a donc laissé quelqu'un nous suivre en cuisine. Le problème, c'est qu'au prétexte d'avoir vu un apprenti cuistot tremper son doigt dans la sauce pour la goûter, il ne va montrer que ça pour dire que, chez nous, tout le monde trempe son doigt dans la sauce et que c'est dégueulasse. Face à cela, il n'y a pas grand-chose à dire sinon stop », explique-t-il. Et de s'ériger en défendeur du droit à l'image...

Reste qu'après avoir visionné ce documentaire, nous n'arrivons pas à comprendre, à l'instar du réalisateur Oren Nataf, ce que les animateurs

et leurs collaborateurs reprochent au film : « Le fond de l'histoire, c'est que Bataille et Fontaine n'assument pas le film. Peut-être n'assument-ils pas l'image qu'il peut donner d'eux, une image sans fard, sans maquillage. Mais, même si on partait avec un a priori négatif, ce n'est pas un Strip-tease totalement délirant. On n'a fait ni un film polémique ni un film à charge, mais un documentaire jetant un regard froid et dépassionné sur ce qu'est la télévision populaire. Où l'on voit comment les gens bossent. Des gens concernés, qui prennent leur travail au - sérieux et qui étaient très contents qu'on les suive. Pour, au dernier moment, nous dire qu'ils refusaient que l'on exploite leur image ! Et ce alors que nous avons les autorisations de tous les témoins, c'est-à-dire de tous ceux qui ont vu leur vie privée dévoilée dans cette émission... »

Pour le réalisateur, il y a désormais peu de chances que le film puisse être diffusé. Dommage. Pour le public mais aussi pour les principaux intéressés. Puisque ne resteront de ce film qu'un ou deux extraits fugaces sur la Toile et quelques phrases, comme celle de Fontaine, discutant du contenu d'un numéro de Y a que la - vérité qui compte, lâchant : « Moi, je ne passerais pas les homos un lundi de Pâques. »

Sébastien Homer

médias

Les nouveaux supports

Depuis l'apparition de la TNT dans
le paysage audiovisuel français, la tendance n'a cessé de s'accélérer. Alors que l'on comptait, en France, six chaînes gratuites analogiques, les nouvelles technologies