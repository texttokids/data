Michel Biard, historien

Les lecteurs en direct

Mots

Martine Carnaroli, (par Internet)

Guy Môquet aurait, dit-on, donné sa vie... Non, il n'a pas donné sa vie, car lorsqu'on est si jeune, si beau, si combatif, on ne donne pas sa vie, on se la garde pour la suite. Il n'a pas donné sa vie, on la lui a prise. Et ça fait une très grande différence. Ceux qui voulaient aujourd'hui défendre l'esprit de résistance et se démarquer de l'entreprise de banalisation-récupération présidentielle ont raté le coche en gardant les mêmes mots. Et, parfois, les mots tuent...

événement

MEDEF, UIMM : lobbies, et plus si affinités...

Patronat . L'« affaire Gautier-Sauvagnac » jette une lumière crue sur le lobbying en direction des parlementaires. L'Humanité dresse un premier inventaire de ce travail contre la démocratie.

Recrutés à la hâte depuis début octobre par le MEDEF et l'UIMM, les supercracks de la « communication de crise » méritent sans aucun doute leurs salaires mirobolants. 30 000 euros par mois, dit-on, c'est le tarif pour transformer les vessies en lanternes... Sur information du service antiblanchiment du ministère des Finances (Tracfin), lui-même rencardé par des banquiers de BNP-Paribas dès 2004, le parquet de Paris lance en septembre dernier une enquête préliminaire sur les retraits de sommes importantes en liquide par Denis Gautier-Sauvagnac et ses proches. Entre 2000 et 2007, celui qui cumule aujourd'hui les fonctions de président et de délégué général de la principale fédération patronale française, qui siège encore à la direction du MEDEF, qui préside, au nom de l'organisation patronale et en alternance avec la CFDT, l'assurance chômage, a, valise après valise, retiré une vingtaine de millions d'euros, au total, sur différents comptes de l'UIMM. Dès les premières investigations, la brigade financière tombe en fait sur un « trésor de guerre » évalué à 600 millions d'euros. Il s'agit, explique tranquillement Gautier-Sauvagnac devant une poignée de journalistes, d'un fond antigrève, baptisé « entraide pour les - industries métallurgiques » (EPIM) constitué à partir de 1972 grâce à des « surcotisations » volontaires des entreprises (0,02 % de la masse salariale, puis 0,04 % depuis 2001). Selon lui, en trente-cinq ans, 170 millions d'euros ont servi à indemniser des patrons qui n'ont rien lâché devant les revendications sociales et « qui sollicitaient notre secours après un conflit » ; le reste, 120 millions, aurait été « bien placé », comme dans une corne d'abondance, afin de se transformer en 600 millions d'euros aujourd'hui.

Du « fluide »

pour le lobbying ?

Et devant ce scandale, que se passe-t-il ? En utilisant une expression parfaitement ambiguë et taillée sur mesure par la « com' de crise » - cet argent aurait servi à « fluidifier les relations sociales » -, banalisée au fil des déclarations et déclinée dans la presse régionale par les responsables locaux de l'UIMM comme du MEDEF - ce sont bien souvent les mêmes -, le patronat a réussi à renverser la charge de la preuve, à focaliser l'attention sur le « financement des syndicats » et à - occulter ses opérations de lobbying intense en direction des parlementaires. Dernier exemple en date, quand, mercredi dernier, Libération publie le témoignage d'un syndicaliste CFTC selon lequel un membre de l'UIMM a commis en 1998 une tentative de corruption à son encontre... Jusqu'ici, les responsables patronaux de la métallurgie ont été contraints d'avouer aujourd'hui que des « salariés » de l'UIMM touchent des « primes » en liquide, issues de cette « caisse noire », et que certains retraités de la fédération passent une fois par mois retirer leur complément dans des enveloppes. Prédécesseur de Gautier-Sauvagnac, Daniel Dewavrin vient, lui, de reconnaître qu'il lui est arrivé de puiser son « argent de poche » avant de partir en vacances...

La façade du patronat se fissure. Pour sa défense, Denis Gautier-Sauvagnac continue d'insinuer qu'une partie de l'argent aurait servi à « participer au financement de diverses organisations de notre vie sociale », mais récuse fermement toute suspicion de corruption : « Je n'ai jamais donné d'argent à un parlementaire. » Le patron de l'UIMM, qui emploie, d'après une estimation de l'Express, plus de 400 juristes spécialisés (soit la moitié de ses salariés), évoque un « lobbying tout à fait classique » qui « n'a rien d'occulte », afin de faire passer des amendements sur les textes que son organisation juge « mauvais ». De son côté, en janvier 2006, quelques mois après son élection à la tête du MEDEF, à l'occasion de l'assemblée générale à Arc-et-Senans (Doubs), Laurence Parisot se félicite ouvertement de l'aménagement de l'ISF et de la suppression d'une hausse anticipée de la taxe d'apprentissage, obtenus par le patronat à l'Assemblée nationale : « Nous sommes engagés - résolument dans le lobbying - parlementaire et politique », lance-t-elle sous les vivats.

Les « succès législatifs du MEDEF »