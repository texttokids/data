Quels sont mes droits ?

En France, tout le monde a des devoirs et est obligé de respecter la loi. Mais on a aussi des droits et des libertés, qu’il faut protéger.
En voici quelques exemples :
La liberté d’expression : c’est le droit de dire ce qu’on pense, même si certains ne sont pas d’accord (mais tant qu’on respecte la loi, comme pour toutes les libertés qu’on a).
La liberté de réunion : c’est ce qui permet notamment de manifester.
La liberté de circulation : c’est le droit de se déplacer en France, de quitter le pays et d’y revenir.
La liberté de culte : c’est le droit de pratiquer la religion qu’on veut.
Le droit au respect de la vie privée : grâce à ça, la police ne peut entrer chez les gens que dans certains cas définis par la loi, le médecin n’a pas le droit de dire à quelqu’un d’autre de quoi ses patients sont malades, il faut demander l’autorisation avant de publier la photo de quelqu’un dans un journal, etc.
Et il y en a encore beaucoup d’autres !
Depuis que l’on a mis en place l’état d’urgence, les policiers ont un peu plus de pouvoir et les citoyens un peu moins de droits. C’est une situation qui est autorisée par la loi, mais parfois cela entraîne des excès :
A Nice, une petite fille de 6 ans a été blessée parce que des policiers ont cassé la porte de son appartement pour entrer. Son père a été arrêté. Sauf que la police s’était trompée de porte et voulait arrêter leur voisin.
La police a annulé le passeport d’un musicien qui voyage beaucoup pour faire des concerts, parce qu’elle avait peur qu’il soit dangereux. Mais ce n’était pas du tout un terroriste !
Dans le sud-ouest de la France, la maison d’un couple qui cultive des légumes a été fouillée. Les policiers ont dit que c’était parce qu’ils cherchaient des gens liés au terrorisme ou des armes, mais en fait c’était plutôt parce que le couple est engagé contre des projets qui ne respectent pas l’environnement.
D’autres personnes ont aussi été embêtées à tort par la police ces dernières semaines, dont beaucoup de Français musulmans. Mais si des policiers font des choses illégales, les citoyens ont le droit de porter plainte.