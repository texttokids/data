N°59 - 25 au 31 mai 2018
Roland-Garros
Comme chaque année au printemps, les meilleures joueuses et les meilleurs joueurs de tennis du monde se retrouvent à Paris pour le tournoi de Roland-Garros. Du 27 mai au 10 juin, ils vont s'affronter pour devenir les nouveaux champions de la terre battue. A quoi ressemble ce tournoi ? Pourquoi est-il particulier ? Comment marche un match de tennis ? Enfile tes baskets et suis-moi sur le court.

Paul, 14 ans, est ramasseur de balles

Paul, 14 ans, participe cette année à Roland-Garros. Il fait partie des 250 ramasseurs de balles, ces personnes qui distribuent et ramassent les balles de tennis pour les joueuses et joueurs pendant les matchs. Les ramasseurs de balles leur apportent aussi leur serviette quand ils en ont besoin, parce qu’on transpire beaucoup lorsqu’on joue.
Pour être sélectionné, Paul a dû passer plusieurs étapes. La première était au mois d’octobre. Il y avait 3 000 candidats ! C’était beaucoup, alors il a fallu passer des épreuves pour savoir qui étaient les meilleurs. «Il y avait plein d’exercices, on devait courir, ramasser la balle le plus vite possible», explique Paul. A la fin de ce test, il ne restait plus que 370 ramasseurs de balles.
La seconde étape a eu lieu au mois de février. Pendant une semaine, Paul a été testé pour savoir s’il était «vraiment très bon pour ramasser les balles», raconte-t-il. Il a appris qu’il irait à Roland-Garros à la fin de cette semaine de stage. «J’ai eu du mal à y croire. C’est presque un rêve qui se réalise car j’ai toujours aimé le tennis.»
Paul joue au tennis depuis l’âge de 9 ans, mais il a commencé à s’entraîner il y a deux ans pour être ramasseur de balles. Il a voulu participer aux sélections de Roland-Garros pour «vivre de beaux moments et être au plus près des joueurs».
L’adolescent est un peu stressé : «J’ai peur de mal faire les choses parce que Roland-Garros est un grand tournoi.» Il rêve de se retrouver sur le même terrain de tennis que son joueur préféré, l’Espagnol Rafael Nadal. Ce serait un grand moment pour lui.
Neuville-aux-Bois, la ville où habite Paul, et Paris, où a lieu le tournoi de Roland-Garros