La Légion d'honneur ne se demande pas.
Chaque ministère suggère des candidats, proposés ensuite au chef de l'État.
La Légion d'honneur est attribuée 3 fois par an (1er janvier, Pâques, 14 juillet).
Elle compte quelque 92 000 membres vivants.
Il y a deux moins, Sayeeda et Sultan vivaient encore au milieu des combats, dans le zoo d'Alep, en Syrie.
Grâce à l'association Four Paws [Quatre pattes en français], les deux tigres ont pu être évacués vers la Turquie avec 11 autres animaux.
Après un long voyage qui les a fait passer par la Turquie, les deux félins viennent d'arriver au centre d'accueil pour félins de Nijeberkoop, au nord des Pays-Bas.
Ils vont y subir des soins, Amaigris et déshydratés [lorsqu'on manque d'eau], ils ont déjà repris un peu de poids.
Mais la guérison des traumatismes [blessures et chocs] qu'ils ont subis durant la guerre sera longue, estiment les soigneurs.
Exactement comme pour les humains.
La Légion d'honneur est la plus haute décoration honorifique française [la plus grande récompense]. Elle est décernée [remise] par un grand chancelier et un grand maître. Ils dirigent l'ordre national de la Légion d'honneur.
Jusqu'ici 3 000 personnes étaient distinguées chaque année. Pour la période de 2018-2020, le nombre de décorés civils [qui ne sont pas des militaires] sera réduit de moitié. La décision du chef de l'État fera passer donc le nombre à moins de 2000, soit environ 1000 décorés de moins par an.
Madagascar
Les enfants retournent enfin à l'école

Justice
Que s'est-il passé au procès Merah ?

Abdelkader Merah est le frère de Mohamed Merah. Ce dernier a commis une terrible attaque à Toulouse et Montauban en mars 2012. Il a tué 7 personnes dont 3 enfants avant d'être abattu par la police.
La rentrée scolaire devait avoir lieu le 2 octobre dernier. Mais les écoles sont restées fermées à cause d'une épidémie de peste qui a fait 131 morts sur l'île.
Ces derniers jours, la situation s'est améliorée sur l'île. Les malades sont moins nombreux. Le 6 novembre, les cours ont donc pu reprendre dans les écoles primaires, les collèges et les lycées. Les élèves sont toutefois obligés de suivre des règles strictes. Avant le début du cours, chaque élève est examiné dans une salle d'isolement afin de vérifier qu'il ne présente des symptômes signes] de la maladie.
Pas complice
Abdelkader Merah était jugé pour savoir quel avait été son rôle dans l'attaque. Était-il un complice de son frère ? Non, ont décidé les juges réunis en une cour [un tribunal spéciale]. Selon eux, Abdelkader Merah ne savait pas ce que son frère allait faire. Cependant, il partageait ses idées de haine et de terrorisme
Ce verdict [cette décision] des juges a été contesté par de nombreuses personnes, qui espéraient qu'Abdelkader Merah serait condamné à la prison à vie. Le parquet general [nom donné aux personnes qui représentent l'Etat dans les procès] a fait appel. C'est-à-dire qu'il a demandé un nouveau procès, notamment pour examiner à nouveau la question de la complicité.