On trouve en Europe de nombreux lézards, comme le lézard vert : il en existe en fait plusieurs espèces, qui vivent dans des régions différentes d'Europe, et qui se ressemblent beaucoup. Il est assez difficile de les reconnaître. 
Le lézard des souches est un proche parent du lézard vert. Les mâles se ressemblent beaucoup, et sont également de couleur verte, alors que les femelles sont beaucoup plus brunes. Le Lézard ocellé est le plus grand lézard d'Europe, et peut mesurer jusqu'à 70 cm.
Le lézard des murailles est nettement plus petit, et mesure environ 20 cm. Il a de nombreux cousins, qui lui ressemblent beaucoup, notamment dans les îles de Méditerranée, comme le lézard des ruines, qui vit en Sicile, le lézard d'Espagne, le lézard de Crète, ou le lézard de Tauride.
Le lézard vivipare vit dans le nord de l'Europe et dans les montagnes du sud de l'Europe, comme les Pyrénées, par exemple. Il présente une particularité originale pour un lézard : selon son milieu de vie, les femelles pondent des œufs ou mettent au monde des petits déjà vivants.
Les orvets sont des reptiles sans pattes, mais ce ne sont pas des serpents. Selon les scientifiques, il pourraient s'agir de proches cousins des lézards, ou de proches cousins des serpents. Comme les lézards, les orvets peuvent se casser leur queue pour échapper à leurs prédateurs : c'est ce que l'on appelle l'autotomie. L'orvet fragile est ainsi appelé pour cette raison. On le surnomme aussi « Serpent de verre ». Il est très commun. L'orvet doré, son cousin, vit en Grèce. Ils peuvent mesurer tous les deux une quarantaine de cm environ. L'orvet géant est un cousin éloigné, beaucoup plus grand (comme l'indique son nom), qui peut atteindre 1,30 m. On le confond facilement avec un serpent.
On trouve dans le sud de l'Europe certains reptiles qui vivent plutôt dans les régions plus chaudes d'Afrique et d'Asie, comme les geckos ou les caméléons. La tarente de Maurétanie est un petit gecko, le seul d'Europe, que l'on trouve notamment en Espagne, au Portugal, en France et en Italie. Le caméléon commun est le seul caméléon d'Europe. Il vit en Afrique du Nord, mais aussi en Espagne et en Italie.

-Un lézard vert de l'Est, en République Tchèque

-Le lézard ocellé est le plus grand lézard d'Europe










La tortue d'Hermann, ou tortue des Maures, est une tortue terrestre, présente dans le sud de l'Europe, notamment en France, et dans les Balkans. La tortue grecque, sa cousine, lui ressemble beaucoup. La tortue bordée a des écailles sur le bord de la carapace qui forment une sorte de jupe. Elle vit en Grèce et en Albanie.
Mais on trouve aussi des tortues aquatiques : La cistude d'Europe, également appelée tortue des marais, est la plus connue. Elle est malheureusement en voie de disparition. On la trouve dans presque toute les régions d'Europe. Sa cousine, la cistude de Sicile, vit uniquement dans cette île. L'émyde lépreuse vit principalement en Espagne et au Portugal. Elle est très rare en France.
La tortue de Floride était autrefois une espèce appréciée comme animal de compagnie, avant qu'elle ne soit interdite dans de nombreux pays d'Europe. Originaire d'Amérique du Nord, les petites tortues de Floride étaient élevées dans des aquarium. Mais, quand elles devenaient trop grandes, les gens les relâchaient dans la nature. Malheureusement, les tortues de Floride se sont rapidement multipliées en Europe, et elles ont chassées les cistudes et les émydes, qui du coup sont en voie de disparition.










Les grenouilles vertes sont répandues dans toute l'Europe. Il s'agit en fait d'un groupe d'espèces, et de leurs hybrides. Il est assez difficile de les distinguer les unes des autres. La grenouille rieuse est la plus grande, mais il existe d'autres espèces, avec lesquelles elle peut s'hybrider, comme la petite grenouille verte, la grenouille de Berger en Italie, et la grenouille de Perez en Espagne.





De nombreuses espèces de grenouilles vertes vivent dans différentes régions de la Méditerranée. La petite grenouille de Karpathos est minuscule ; c'est aussi la plus rare, car elle ne vit que dans la petite île de Karpathos, en Grèce. La grenouille de Chypre ne vit qu'à Chypre, et la grenouille de Crète... qu'en Crète. La grenouille épirote vit en Grèce, et en Albanie.



Les grenouilles brunes sont leurs cousines. Il en existe aussi plusieurs espèces : la grenouille rousse est la plus connue, mais il en existe beaucoup d'autres, comme la grenouille agile. La plupart apprécient les milieux forestiers, et les mares cachées dans les bois. Certaines espèces de grenouilles brunes ne se rencontrent que dans certaines zones de l'Europe, comme la grenouille ibérique, la grenouille italienne, la grenouille grecque, ou la grenouille des Pyrénées.