-Une grenouille grecque, au Mont Olympe, en Grèce
Les rainettes ressemblent à des grenouilles. Elles ont des doigts collants, qui leur permettent de grimper et de s'accrocher sur les branches. La rainette verte est la plus connue. On pensait autrefois qu'elle était répandue dans pratiquement toute l'Europe, mais on a découvert récemment que certaines rainettes que l'on prenait jusque là pour des rainettes vertes appartenaient en réalité à des espèces à part, comme la rainette méridionale, qui vit dans le sud de l'Europe (et notamment de la France), la rainette italienne, qui vit en Italie, et la rainette sarde, qui vit notamment en Corse et en Sardaigne.


Mais on trouve aussi de nombreux autres anoures en Europe, qui appartiennent à des espèces différentes, et qui sont, selon les cas, couramment appelés « crapauds » ou « grenouilles ».
Les « vrais » crapauds sont représentés, en Europe, notamment par le crapaud commun, le plus courant, qui est également présent en Asie et en Afrique du Nord. Le crapaud vert est présent dans le centre de l'Europe, ainsi qu'en Asie. Il a plusieurs cousins en Méditerranée, comme le crapaud sicilien, qui vit en Sicile, et le crapaud des Baléares, qui vit aux Baléares et en Italie. Le crapaud calamite, qui est présent depuis le Portugal jusqu'en Pologne, leur ressemble un peu.
L'alyte accoucheur, parfois appelé crapaud accoucheur, ressemble un peu à un crapaud. On le rencontre dans la péninsule ibérique, et en France, jusqu'en Allemagne. Il est appelé ainsi car le mâle transporte avec lui son chapelets d’œufs pour veiller sur eux après l'accouplement.




-Un alyte accoucheur mâle, avec ses œufs, en France

Le pélobate brun est un amphibien nocturne, peu commun, mais répandu dans presque toute l'Europe, et jusqu'en Sibérie. Cette espèce recherche des sols mous pour y creuser son terrier : on le rencontrait autrefois fréquemment dans les champs d'asperges. Son cousin, le pélobate cultripède, est beaucoup plus rare. Il fréquente les mares temporaires du sud-ouest de l'Europe.
Le sonneur à ventre de feu est un petit amphibien qui pratique le camouflage : son dos est tacheté de brun, pour éviter d'être repéré par les prédateurs. Mais, si malgré tout, un prédateur tente de s'en prendre à lui, le sonneur a un autre moyen de défense : il est empoisonné ! Pour ne pas se faire manger (ce serait dommage que le prédateur meure empoisonné après avoir mangé le sonneur, car le sonneur serait mort de toutes façons...), il faut donc que les prédateurs sachent qu'il n'est pas comestible. Lorsque le sonneur se fait repérer par un prédateur, il se retourne sur le dos, et montre son ventre tacheté de noir et rouge : c'est un avertissement, c'est ce que l'on appelle l'aposématisme.
Le sonneur à ventre jaune ressemble beaucoup à son cousin, mais son ventre est jaune tacheté de noir, et non pas rouge. Le sonneur à pieds épais, lui, ne vit qu'en Italie.


-Un sonneur à ventre jaune, en Autriche, relève ses pattes et arc-boute son dos, pour montrer son ventre.


La salamandre de feu est très facile à reconnaître, avec son corps noir tacheté de jaune. Elle mesure environ 20 à 25 cm de long, et est répandue dans toute l'Europe. Sa cousine, la salamandre corse, lui ressemble beaucoup, mais elle est plus rare, et ne se trouve qu'en Corse. La salamandre noire est plus petite et n'a pas de taches. Elle vit dans les Alpes.
Les salamandres sont plutôt des amphibiens terrestres, qui apprécient les sols humides, en montagne ou dans les forêts, mais qui ne recherchent des points d'eau que pour se reproduire. Le chioglosse portugais vit au Portugal et au nord-ouest de l'Espagne. Il est à peu près aussi grand que la salamandre noire.