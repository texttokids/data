Cette section est vide, insuffisamment détaillée ou incomplète.


L'île, fertile, attira l'attention des Anglais dès le XVI^e siècle Anglais, Néerlandais, Français et même par le duché de Courlande. Pour certains, Tobago demeure, dans la littérature, l'île de Robinson Crusoé, œuvre écrite par Daniel Defoe et


Les Anglais et Français furent les premiers à tenter la colonisation des Petites Antilles, dont deux tentatives éphémères à


Anglais. De 1628 à 1678, les Néerlandais considéraient Tobago comme leur colonie sous le nom de Nieuw Walcheren. Lors de l'arrivée des Néerlandais, l'île avait été octroyée (ainsi que l'ensemble des Antilles) plusieurs fois par le roi Jacques I^er d'Angleterre, dont une fois en 1610 à Jakob Kettler, fils du duc de Courlande. Pour les Néerlandais, Tobago demeura durant cette période, un site de


Jan de Moor demanda à Jacob Maerssen pour qu'on lui octroie la permission de coloniser Tobago sous l'auspice de la Compagnie néerlandaise des Indes occidentales (WIC) fondée en 1621 et détenant un monopole colonial et commercial sur toutes terres à l'ouest du Cap de Bonne-Espérance. Une expédition de colons s'y établit seulement en 1628 et fut ravitaillée en 1629 par l'amiral Pater. En 1632, un second contingent de colons néerlandais quitte le port de Flessingue et s'y établit - la colonie comporte maintenant plus ou moins 200 colons. Aux débuts des années 1630, certaines escadres de la compagnie eurent comme instructions de s'arrêter à Tobago pour ravitailler les colons. Entre 1634-37, la colonie fut éradiquée sans qu'on en sache exactement les causes. Il est probable qu'une expédition espagnole depuis l'île de la Trinité ait attaqué la colonie. Les Néerlandais auraient été traités comme des criminels de guerre, contre toutes conventions établies entre les deux nations durant la trêve de 1609-21. Les Néerlandais répliquèrent à Trinité et mirent à sac San


Au même moment, Willem Usselincx puis Charles I^er d'Angleterre tentèrent d'intéresser Friederich Kettler, duc de Courlande Jakob en mission à Amsterdam, il déniche le capital nécessaire pour financer une première expédition de 212 colons vers l'île^. Les colons, pour la plupart zélandais, ne s'acclimatèrent pas et désertèrent rapidement. Une seconde entreprise courlandaise en 1637 connut probablement le même sort. Les années 1630, semble-t-il, sont assez difficiles à décortiquer étant donné les nombreuses


En 1639, des Anglais de la Barbade tentent de nouveau d'occuper l'île sous la direction du capitaine Massam mais les attaques continuelles des Amérindiens Caraïbes de l'île voisine de Saint-Vincent découragent toute occupation permanente. Une troisième tentative en 1642 dirigée par le capitaine Marshall de la Barbade ne porte fruits que quelques années avant d'être poussé à émigrer vers le Suriname par les affrontements autochtones incessants. À ce moment, Jacob Kettler, qui avait succédé à son père à la tête du duché de Courlande, décida de retenter sa chance et de financer une nouvelle tentative de colonisation à Tobago. Éconduits par les Caraïbes, les colons courlandais trouvèrent refuge aux établissements zélandais du


Après avoir tenté d'intéresser les Néerlandais dans une entreprise coloniale conjointe et essuyé des refus, Jakob décide de s'y reprendre seul en 1653 alors que les Néerlandais et les Anglais s'affrontent pendant la Première guerre anglo-néerlandaise. Le 20 mai 1654. une expédition de 80 familles dirigée par un capitaine hollandais engagé par le duc jette l'ancre à Tobago et renomme l'île Nouvelle-Courlande. Rapidement, les Courlandais construisirent le fort Jakobus.


En 1654, des Juifs fuyant la Nouvelle-Hollande repassée aux mains portugaises tentent de convaincre les États généraux de leur octroyer Tobago, mais les héritiers De Moor, les frères Lampsins, d'éminents Zélandais, cherchaient au même moment à remettre sur pied la


Les Zélandais reprenant contrôle de la colonie, la première expédition zélandaise, avec un bateau de 50 Juifs de Zélande, arrive à Tobago en septembre 1654, quatre mois après les Courlandais et fonde une colonie du côté opposé de l'île, nommée Nieuw-Walcheren.


Becquard, chef de l'expédition néerlandaise, visite Saint-Eustache afin d'y recruter des colons potentiels. Lorsque Becquard revint à Tobago, la présence courlandaise était maintenant connue et un traité de partition fut ratifié. La dotation aux Lampsins a été rédigée de manière que les États généraux néerlandais conservent une partie