Il refuse d'abord de s'engager dans la lutte qui oppose Girondins et Montagnards, mais, lié d'amitié avec Robespierre^, il prend position en sa faveur quand il est attaqué, par Barbaroux en octobre et Louvet en novembre, et finit par rejoindre les bancs de la Montagne devant les attaques répétées de la Gironde contre la Commune de Paris et les menaces fédéralistes^. Il indique à la tribune des Jacobins que la prépondérance des Girondins au sein des institutions, et en particulier au sein du Comité de Constitution, lui a « dessillé les yeux »^. Lors du procès de Louis XVI, il vote la peine de mort sans appel ni


Le 26 novembre 1792, il est envoyé en mission dans le Loiret pour y rétablir l'ordre et la circulation des grains. Le


décret, avec Goupilleau de Montaigu et Michel, d'organiser la réunion à la France de la principauté de Salm, intégrée dans le département des Vosges. Rappelés le 30 avril suivant, les trois députés sont de retour à Paris au plus tard le


Girondins mais demande que l'on use de modération à l'égard des vaincus et se propose comme otage pour tranquilliser Bordeaux sur le sort de ses députés. Il est également l'un des rédacteurs de la


Mandat d'arrêt de Danton et de ses amis, signé par les membres du Comité de Salut public et du Comité de Sûreté générale le 30


Couthon à la Convention nationale en 1793, dessin de Vivant Denon,


Adjoint au Comité de salut public le 31 mai et chargé de la correspondance générale, il est nommé membre du comité lors du renouvellement du 10 juillet. Le lendemain, il présente un rapport sur la révolte de Lyon dans lequel il réclame des mesures rébellion », ne voulant pas qu'on confonde les bons citoyens avec les mauvais. Défenseur des paysans, il fait voter, via le décret du 17 juillet 1793, comme il l'avait demandé en février 1792 à l'assemblée législative, l'abolition complète, sans indemnité, des droits féodaux et le brûlement des titres féodaux. De même, le 20 août, il prend un arrêté contre les anciens privilèges prévoyant notamment, dans son premier article, la destruction de « tous les châteaux-forts, donjons, tours et autres monuments de la féodalité », ainsi que le comblement des « fossés qui les environnent », des citernes et des « souterrains pratiqués auprès », dans son


Le 21 août, il est envoyé en mission à l'armée des Alpes et dans le Rhône-et-Loire avec Châteauneuf-Randon et Maignet, afin de faire rentrer Lyon dans le rang. Le 1^er septembre, sa mission est élargie à la Lozère, mais il ne s'y rend pas^. Après s'être assuré du Puy-de-Dôme, où il lève des troupes, il prend la tête d'une armée de 10 000 hommes et fait le siège de Lyon, dont les autorités ont passé outre les tentatives de conciliation en faisant guillotiner Chalier. Entré dans la ville le 9 octobre, il mène une répression modérée : n'appliquant qu'en partie le décret de la Convention qui prescrit sa destruction, il ne fait abattre que quelques maisons. Il est rappelé à


1793)^ ; la répression deviendra extrêmement violente


Fouché^. À son arrivée, il reçoit les félicitations de


Reprenant ses travaux au Comité de salut public, il est élu


intervient fréquemment sur les questions militaires, fait décréter d'accusation le général Westermann et contribue à la chute des


trémoussoir ou élastique » emprunté au Mobilier national, qui avait appartenu à la comtesse d'Artois et se trouvait à Versailles ; celui-ci est actuellement exposé au musée


Convention décide, par décret, de nommer une commission parlementaire ont été rendues jusqu'à ce jour, en supprimant celles qui sont devenues


Couthon est élu le 3 floréal (22 avril 1794), avec Cambacérès et Merlin de Douai, tous deux députés de la


Une autre commission étant, quant à elle, « chargée de rédiger un corps d'instruction civile propre à conserver les mœurs et l'esprit de la liberté », il fait adopter le principe qu'elle sera choisie par le Comité de salut public, indiquant qu'« un membre du comité », en l'occurrence Saint-Just, « s'est déjà occupé du


Le 18 floréal (7 mai 1794), il défend le décret présenté par Robespierre d'après lequel la République française reconnaît l'Être suprême et propose que son discours soit traduit dans toutes les langues et diffusé dans tout l'univers.


Rapporteur de la loi du 22 prairial (10 juin 1794) corédigée avec Robert Lindet, loi dite de « Grande Terreur » qui réorganise le Tribunal révolutionnaire, il déclare devant la Convention: « Le délai pour punir les ennemis de la patrie ne doit être que le temps de les reconnaître ; il s’agit moins de les punir que de les anéantir… Il n’est pas question de donner quelques exemples, mais d’exterminer les implacables satellites de la tyrannie ou de périr avec