une grande urbanisation : on note une grande activité pastorale en altitude, les estives couvrant 575 000 ha, soit près d'un tiers de la surface du massif. Côté agriculture, le versant méditerranéen est propice à la culture de la vigne au niveau collinéen et dans les plaines l'irrigation de vergers et de champs agricoles. On note aussi la présence d'une filière


XX^e siècle, avec des évolutions contrastées actuellement : l'industrie lourde tend à reculer tandis que les activités artisanales (avec plus de 334 activités différentes) se


tourisme occupe une partie importante dans l'économie actuelle du massif : outre les stations de sports d'hiver, on note un nombre important de stations thermales et d'hôtelleries. La haute montagne attire les randonneurs pour ses paysages et son aspect sauvage, tandis que le piémont est plus visité pour les lieux chargés d'histoire (chemins de Saint-Jacques de Compostelle,


plus de 50 stations de sports d'hiver réparties tout le long de la


En 1999, les statistiques concernant la répartition socio-professionnelle (côté français)^ étaient les suivantes :


Carte administrative et des transports des Pyrénées.


L'administration du territoire est bien sûr différente suivant les pays. En France, le territoire est découpé en régions, départements, arrondissements et cantons ; en Espagne, le découpage se fait en communautés autonomes, provinces et comarques ; en Andorre, la


Côté français, l'espace pyrénéen est défini et délimité


janvier 1985 : le massif pyrénéen est constitué par « chaque zone de montagne et les zones qui lui sont immédiatement contiguës et qui forment avec elle une même entité géographique, économique et sociale » (Art.5L n^o 85-30). C'est une unité d'aménagement de l'espace et de programmation. L'aménagement du territoire y vise le regroupement pays (voir l'article Pays des Pyrénées), ainsi que le désenclavement de la zone massif avec la construction de voie rapides ou d'autoroutes sur chaque versant ou transnationales (voir l'article


Le réseau routier comprend l'autoroute A64 (la Pyrénéenne) qui compte 90 km dans la zone massif, 500 km de routes nationales et 2 000 km de routes départementales^. Les autoroutes A9 et AP-7 permettent de traverser les Pyrénées orientales, l'A63 et l'AP-8 les Pyrénées occidentales ; l'autoroute A66 permettra à terme de relier Toulouse et Foix à Barcelone


Le réseau ferré quant à lui comprend 350 km dont un pôle d'échange


Pyrénées-Orientales) avec l'Espagne et l'Andorre.


Du Pays basque à l’Ariège, en passant par le Béarn et la Bigorre, 35 Commissions Syndicales du massif Pyrénéen, des structures intercommunales créées par l’ordonnance royale du 18 juillet 1837, ont mission de gérer et développer le patrimoine naturel d’un territoire en montagne (forêts, espaces montagnards, faune et flore). Mêmes si elles sont bien présentes dans le code des Collectivités Territoriales (art L 5222-1 et suivants du code général des collectivités territoriales), les Commissions Syndicales sont peu connues au niveau du public et des


Article détaillé : Espaces protégés des Pyrénées.


La faune et la flore de la partie centrale des Pyrénées sont protégées par le parc national des Pyrénées, versant français, et par deux parcs nationaux, le parc national d'Aigüestortes et lac Saint-Maurice en "Encantats" et le parc national d'Ordesa et du Mont-Perdu, versant espagnol. À cela, s'ajoute le parc naturel régional des Pyrénées catalanes, le Parc naturel régional des Pyrénées ariégeoises et des réserves naturelles nationales comme celle du Néouvielle, du Soussouéou dans la vallée d'Ossau dans les Pyrénées occidentales, ou les nombreuses réserves naturelles catalanes La Massane). Il existe enfin des réserves naturelles régionales en Ariège (Embeyre), dans les Pyrénées-Orientales (Nyer) et dans les Hautes-Pyrénées (Pibeste). Les nombreux sites naturels classés au titre de la loi sur la protection des paysages et les arrêtés préfectoraux de protection de biotope, les réserves biologiques et les réserves de faune sauvage témoignent également de l'intérêt écologique du massif