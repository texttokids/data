Durant le Commonwealth anglais, Jakob Kettler ne put continuer à jouir du soutien politique anglais. Les frères Lampsins s'attelèrent donc à développer leur colonie comme bon leur semblait. Les Zélandais et les Courlandais furent ravitaillés pendant ces quelques années, mais lorsque la Guerre du Nord éclata et que le duc fut pris en otage par les Suédois, les Néerlandais décidèrent de passer à l'offensive. Encouragés par leurs seigneurs, la WIC et les États généraux, les colons néerlandais commencèrent à saisir les navires de ravitaillement


En 1658 il n'y avait plus que 40 Courlandais capables de porter les armes, contre 500 Zélandais, rejoints par des Français^.


En raison du refus par Holtzbruch, commandant de la colonie, d'une offre néerlandaise de reddition, les colons courlandais se mutinèrent et la colonie capitula le 11 décembre 1659. Le commandant néerlandais promit aux Courlandais de les ramener en Europe et de rendre l'enclave au duc une fois celui-ci libre des geôles suédoises. Les frères Lampsins modifièrent le traité pour ajouter que les Courlandais avaient abdiqué volontairement leur souveraineté sur leur territoire de


Avec la domination néerlandaise complétée, la colonie prospéra et un contingent de planteurs français s'ajouta au nombre dans le « Quartier des Trois Rivières » et Louis XIV, éleva Cornelius Lampsins au


Libéré au traité de Oliva, Jakob Kettler entreprit des démarches devant les institutions néerlandaises afin que le traité original soit respecté, mais la WIC ne s'y plia pas. Le duc tenta même de racheter Tobago pour 200 000 florins mais malgré l'intérêt marqué des frères Lampsins, les négociations échouèrent. Après le traité de 1662 entre les Provinces-Unies et Louis XIV, Cornelis Lampsins chercha à faire reconnaître ses droits par la couronne française, qui par décret, déclara Tobago comme baronnie française. Entre 1660-65, on estime que la colonie néerlandaise comptait entre 1000 et 1500 colons et plus de 7000 esclaves, elle produisait du rhum, du cacao et du sucre ; six


Un représentant des granas, les Juifs de Livourne, Paulo Jacomo Pinto, entama des négociations à Amsterdam et obtint le transport en 1658 et 1659 de deux groupes de juifs, le second de 120 personnes, en passant par la Zélande. Un troisième groupe de 152 juifs de Livourne arrive sur le Monte de Cisne le 20 juillet 1660, alors que sa destination devait être Cayenne. Parmi eux, le célèbre poète juif espagnol Daniel Levi de Barrios, alias Miguel de Barrios, dont la femme Debora meurt à Tobago et qui repart à Bruxelles. En janvier 1661, Paulo Jacomo Pinto se préoccupe du sort des Juifs déviés pour une raison inconnue à Tobago et laissés dans


Charles II d'Angleterre décida d'octroyer Tobago à son cousin Jakob Kettler, duc de Courlande, en échange des postes de traite négrière courlandais au Gambie. Le roi décida même d'avertir le gouverneur de Barbade et les Néerlandais qu'il considérait Tobago comme possession anglaise et fief de Courlande. Après les attaques anglaises et l'occupation française durant cette guerre, Crijnssen débarque sur une île désertée en 1667. La paix de Breda restitua Tobago aux Néerlandais, mais n'élimina pas toutes revendications françaises et courlandaises. Les réfugiés néerlandais reprirent rapidement pied à Tobago dès le retour à la paix mais dans un état d'abjecte pauvreté.


En décembre 1668, le duc de Courlande s'entêta à réclamer la possession de Tobago et finança une nouvelle expédition. Les Néerlandais sur l'île offrirent aux colons courlandais deux options : rester et s'intégrer à la colonie de Nieuw-Walcheren ou partir ; ils choisirent la deuxième solution. Le duc de Courlande tenta donc de reprendre le contrôle de l'île en plaidant sa cause devant les États généraux néerlandais mais ceux-ci se rangèrent dans le camp des Lampsins qui reçurent une confirmation officielle de leur possession de seigneurie jusqu'en 1700


En 1672, Charles II décida qu'il devait reprendre Tobago et supporta entièrement les prétentions courlandaises. Le gouverneur de la Barbade, William Willoughby, prit en charge une partie importante des dépenses pour capturer Tobago. La conquête de Tobago fut le premier acte belliqueux commis dans les Caraïbes par les Européens durant la guerre de Hollande, entre les 18 et 22 décembre 1672. Les 400 colons néerlandais furent transportés à la Barbade et les infrastructures de la colonie furent rasées par les conquérants. Deux jours plus tard, une frégate française réclama l'île afin d'en prendre possession. Au traité de Westminster, Tobago fut retournée aux Provinces-Unies par les Anglais, mais les hostilités entre Français et


Lors de la fondation de la nouvelle WIC en 1674, Tobago ne fut pas intégré à la charte et demeura sur papiers propriété des frères Lampsins. Malgré les protestations zélandaises, la chambre amstellodamoise de la WIC décida de repeupler l'île sous la direction de Jacob Binckes, un amiral frison qui s'était distingué en 1673