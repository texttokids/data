Super cerveau !
Le cerveau est l'ordinateur de ton corps. Il reçoit des milliers d'informations, les analyse et indique à tes organes ou à tes muscles ce qu'ils doivent faire.
Bien protégé sous les os de ton crâne, se cache ton cerveau. Mais comment fonctionne-t-il ?

Ton cerveau est divisé en deux : l'hémisphère gauche et l'hémisphère droit.
Pour reconnaître les sensations.
(le toucher, la douleur, etc.)
Dans ton cerveau se trouve ta mémoire. Grâce à elle, tu te souviens de tes vacances ou de tes leçons.
Le lobe frontal.
Pour reconnaître les formes ou les visages.
Pour parler bouger, penser et réfléchir.
19 200.
Le lobe temporal
Pour reconnaître les sons et ce qu'ils
veulent dire.
Ton cerveau ne se repose jamais : même quand tu ne penses à rien ou
que tu dors, il continue de travailler !