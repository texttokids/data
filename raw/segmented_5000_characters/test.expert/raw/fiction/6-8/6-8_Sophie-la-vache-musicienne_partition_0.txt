Sophie vit à la campagne. Très musicienne, elle adore chanter et charmer sa famille et tous ses amis par ses petits concerts.
Un jour, un grand concours de musique est organisé. Tous les orchestres du pays sont conviés à y participer. « J’ai envie de tenter ma chance », déclare Sophie à ses amis. « Je parviendrai peut-être à me faire engager dans une orchestre. »
« Tu veux partir pour la grande ville ? » s’inquiète sa mère. 
« Tu veux nous quitter ! » s’exclame son père.
« Et nous concerts du soir ? » s’attristent ses amis.
« Ecoutez, dit Georges le vieux cheval, nous sommes tous un peu inquiets. Mais Sophie a raison : elle doit essayer, elle a du talent et elle réussira. » 
Georges a été convaincant. Le matin du départ, tous accompagnent Sophie à la gare… Enfin la grande ville !
Sophie achète un journal et un plan de la ville, s’installe à la terrasse d’un bistrot et consulte les petites annonces : beaucoup d’orchestres recherchent des musiciens. 
« Voyons, voyons. Hum, celui-ci, Le Grand Orchestre du Sourire Etincelant, est tout près d’ici. Quel drôle de nom ! Eh bien, allons-y… »
« Vous venez pour la place ? En principe, nous n’engageons pas d’herbivores, mais entrez, entrez… »
Sophie s’enfuit à toutes jambes. « Je dois faire plus attention aux noms d’orchestres, se dit-elle. Ah ! Les herbivores Mélomanes. Je suis herbivore, allons-y… »
« Vous venez pour la place ? Désolé, ma chère, vous ne faites pas le poids ! »
« Qu’est-ce que le poids viens faire avec la musique, s’exclame Sophie. Hum ! Harmonie Royale des Ruminants. Je rumine, allons-y… » 
« Vous venez pour la place ? Désolée, ma chère, je crains que vous ne soyez pas à la hauteur »
« Qu’est-ce que la taille vient faire avec la musique, s’indigne Sophie. Ah ! Cercle Musical des Bêtes à Cornes. J’ai des cornes, allons-y… »
« Vous venez pour la place ? Désolé, ma chère, il y a cornes et cornes… »
« Qu’est-ce que les cornes viennent faire avec la musique, se fâche Sophie. Ensemble Orchestral Bovin. Je suis une bovine, allons-y… »
« Pouah ! Pas de vaches brunes chez nous ! »
« Qu’est-ce que la couleur vient faire avec la musique, s’emporte Sophie. Ah ! Les Bovidés Musiciens. Je suis une bovidée, allons-y… »
« Vous venez pour la place ? Naavrée maa chèère, je crains que vous ne soyez pas assez chic pour nôtre orchestre. »
« Quelle bande de snobinardes, fulmine Sophie. Qu’est-ce que l’élégance vient faire avec la musique ! » Elle reprend son journal. « Ah, Grand Orchestre des Vaches Folles. Je suis une vache et je suis folle de rage ! Allons-y… »
« Vous venez pour la place ? Entrez, entrez, plus on est de fous, plus on rit… » « Heu, je crois que je me suis trompée d’adresse », bredouille Sophie.
Sophie est découragée. Orchestre Royal Canin, Les Matous Ronronnants, ce n’est même pas la peine d’essayer… « Il ne me reste plus qu’à retourner chez moi. »
Toute triste, elle s’assoit à la terrasse du café de la gare. « Eh bien, ma petite dame, ça n’a pas l’air d’aller ? » s’inquiète le garçon.
Sophie lui raconte ses malheurs. « Oh ! Ça ne m’étonne pas, ma petite dame. Tous ces orchestres sont nuls, ils n’aiment pas vraiment la musique. Moi-même, qui suis musicien, j’ai connu ça : je n’avais pas les poils assez longs ou assez courts ; j’avais les oreilles trop pendantes, le museau trop pointu ; je n’avais pas la bonne taille, la conne couleur, le bon pedigree… »
« Alors, pourquoi ne pas former un orchestre ensemble ? Nous n’engagerions les gens que sur leur talent ! Je me présente : Sophie »
« Topez là, ma petite dame ! Moi, c’est Douglas. » 
« Ne m’appelez plus ‘ma petite dame’, Douglas, et ce sera parfait. »
Sophie et Douglas ont passé une annonce dans le journal, et les candidats affluent.
Tous deux prennent le temps de les écouter très attentivement. Bientôt, ils engagent … Quatre excellents musiciens.
«  Je ne trouvais pas de place sous prétexte que je suis maigrichon ! »
« Moi c’est à cause de ma jambe de bois. »
« Les autres loups me trouvent trop sentimental ! »
Sophie a baptisé leur groupe Les Amis de la Musique. Eh, bien évidemment, ils ont gagné le concours. »